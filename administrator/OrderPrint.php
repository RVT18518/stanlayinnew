<?php
include("../includes/modulefunction.php");
include("../includes/function_lib.php"); 
include("session_check.php");
$pcode       = $_REQUEST["pcode"];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $_SERVER['HTTP_HOST'];?>:Your Order</title>
<link href='css/style.css' rel='stylesheet' type='text/css' />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="javascript" type="text/javascript">
function printerX()
{
	window.print();
}
</script>
<style>
.pagehead {
	color: #333333;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight: bold;
	background-color:#FFFFFF;
}
.head {
	color: #333333;
	font-size: 12px;
	font-weight: bold;
	background-color:#FFFFFF;
}
.tblBorder {
	border: 0px solid #fff;
}
.tblBorder_invoice {
	border: 1px solid #333;
}

.tblBorder_invoice_bottom {
	border-bottom: 1px solid #333;
}

</style>
</head>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="tblBorder">
  <tr>
    <td width="12%">&nbsp;</td>
    <td>&nbsp;</td>
    <td width="44%" align="right">&nbsp;</td>
  </tr>
  <tr class="pagehead">
    <td colspan="3"><table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td height="35" align="left"><!--<img src="assets/logo.png" alt="Invoice Order" title="Invoice Order" onerror="style.display='none';" />--></td>
          <td align="center"><h2 style="text-decoration:underline;">Invoice</h2></td>
          <td align="right">&nbsp;<!--<img src="assets/logo.png" alt="Invoice Order" title="Invoice Order" onerror="style.display='none';" />--></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td width="12%">&nbsp;</td>
    <td>&nbsp;</td>
    <td width="44%" align="right">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" ><table width="100%" border="0" cellpadding="0" cellspacing="1" class="tblBorder_invoice">
        <tr>
          <td><table width="100%" border="0" cellpadding="4" cellspacing="0" >
              <tr>
                <td><strong>STMR Global Investments LLC<br />
					DBA shanktimantra.com<br />
                  PO Box 494<br />
                  Aldie, VA 20105<br />
                  866-719-8006<br />
                  888-987-2737<br />
                  <a href="mailto:info@shaktimantra.com">info@shaktimantra.com</a><br />
                  <a href="http://www.shaktimantra.com">www.shaktimantra.com</a></strong></td>
              </tr>
              <tr>
                <td></td>
              </tr>
              <tr>
                <td></td>
              </tr>
            </table></td>
          <td valign="top" width="50%"><?php $s->order_gen_info_invoice($pcode);?></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" align="center"><table width="100%" border="0" cellpadding="0" cellspacing="1" class="tblBorder_invoice">
        <tr>
          <td width="50%" valign="top" align="left" ><?php $s->order_billing_address_invoice($pcode);?></td>
          <td valign="top" align="left"><?php $s->order_shipping_address_invoice($pcode);?></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td width="44%">&nbsp;</td>
    <td valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" class="tblBorder_invoice" ><?php $s->OrderItemsInfo_invoice($pcode);?></td>
  </tr>
  <tr>
    <td colspan="2">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" align="right" ><table width="100%" border="0" cellpadding="0" cellspacing="0" class="tblBorder_invoice">
        <tr>
          <td width="51%">&nbsp;</td>
          <td align="right"><?php $s->OrderTotalInfo_invoice($pcode);///////// order total?></td>
        </tr>
      </table></td>
  </tr>
</table>
<p align="center">Please make  All checks and money orders payable to STMR Global Investments LLC.<br />
  Thank You  For Your Business<br />
  Please visit  our website <a href="http://www.shaktimantra.com">www.shaktimantra.com</a> for terms and conditions</p>
</body>
</html>
