<?php
	$data_action = $_REQUEST["action"];
	$pcode 		 = $_REQUEST["pcode"];
	$pcode_emp	 = $_REQUEST["pcode_emp"];

if($_REQUEST['action']=='ChangeStatus')
	{
		$rs_status 		= $s->getData_with_condition('tbl_comp_emp','comp_emp_id',$pcode_emp);
		if(mysqli_num_rows($rs_status)>0)
		{
			$row_status = mysqli_fetch_object($rs_status);
			if($row_status->status == 'active')
			{
				$fileArray["status"] = 'inactive';
			}
			else if($row_status->status == 'inactive')
			{
				$fileArray["status"] = 'active';
			}
		
			$result     = $s->editRecord('tbl_comp_emp',$fileArray,'comp_emp_id',$pcode_emp);
			$s->pageLocation("index.php?pagename=add_company&action=edit&result=$result&pcode=$pcode");
		}
	}

if($data_action=='delete')
		{
//			$result = $s->delete_table_withCondition('tbl_comp','id',$pcode);
			$result = $s->delete_table_withCondition('tbl_comp_emp','comp_emp_id',$pcode_emp);	
//			$result      = $s->editRecord('tbl_comp_emp',$fileArray,'comp_emp_id',$pcode_emp);
			$s->pageLocation("index.php?pagename=add_company&action=edit&result=$result&pcode=$pcode");
			if($result)
			{
				echo "<p class='success'>".record_delete."</p><br />";	
			}
			else 
			{
				echo "<p class='error'>".record_not_delete."</p><br />";	
			}
		}

	if($_REQUEST["action"] == "update" || $_REQUEST["action"] == "insert")
	{
		$dataArray["comp_name"]		= $_REQUEST["comp_name"];
		$dataArray["fname"]			= $_REQUEST["fname"];
		$dataArray["lname"]			= $_REQUEST["lname"];
		$dataArray["no_of_emp"]		= $_REQUEST["no_of_emp"];
		$dataArray["email"]			= addslashes($_REQUEST["email"]);
		$dataArray["address"]		= addslashes($_REQUEST["address"]);
		$dataArray["city"]			= $_REQUEST["city"];
		$dataArray["country"]		= $_REQUEST["country"];
		$dataArray["state"]			= addslashes($_REQUEST["state"]);
		$dataArray["zip"]			= $_REQUEST["zip"];
		$dataArray["telephone"]		= $_REQUEST["tele_no"];
		$dataArray["fax_no"]		= $_REQUEST["fax_no"];
		$dataArray["ref_source"]	= $_REQUEST["ref_source"];
		$dataArray["cust_segment"]	= $_REQUEST["cust_segment"];
		$dataArray["acc_manager"]	= $_REQUEST["acc_manager"];
		$dataArray["designation_id"]= $_REQUEST["designation"];
		$dataArray["mobile_no"]		= $_REQUEST["mobile_no"];
		$dataArray["description"]	= addslashes($_REQUEST["descr"]);
		$dataArray["status"]		= $_REQUEST["status"];
	}
	if($_REQUEST["action"] == "insert")
	{
		if($_REQUEST["status"]!="" && $_REQUEST["status"]!='0')
		{
			$result = $s->insertRecord('tbl_comp',$dataArray);
			$s->pageLocation("index.php?pagename=manager&action=$data_action&result=$result");
		}
		$error = 1;
		$data_action = 'add_new';
	}
	if($_REQUEST["action"] == "update")
	{
		if($_REQUEST["status"]!="" && $_REQUEST["status"]!='0')
		{
			$result = $s->editRecord('tbl_comp',$dataArray,'id',$pcode);
			$s->pageLocation("index.php?pagename=manager&action=$data_action&result=$result");
		}
		$error = 1;
		$data_action = 'edit';
	}
	if($data_action=="edit")
	{
		$rs		  		= $s->getData_with_condition('tbl_comp','id',$pcode);
		$row	  		= mysqli_fetch_object($rs);
		$comp_name 		= $row->comp_name;
		$fname 	  		= $row->fname;
		$lname    		= $row->lname;
		$address  		= stripslashes($row->address);
		$city			= $row->city;
		$no_of_emp		= $row->no_of_emp;
		$ref_source		= $row->ref_source;
		$cust_segment	= $row->cust_segment;
		$country		= $row->country;
		$state			= stripslashes($row->state);
		$zip			= $row->zip;
		$tele_no		= $row->telephone;
		$fax_no	    	= $row->fax_no;
		$descr			= stripslashes($row->description);
		$status			= $row->status;
		$designation	= $row->designation_id;
		$acc_manager	= $row->acc_manager;
		$email			= $row->email;
		$mobile_no		= $row->mobile_no;
		$comp_website	= $row->comp_website;
		$comp_revenue	= $row->comp_revenue;
		
		
		$data_action 	= "update";
	}
	if($data_action == "add_new")
	{
		/*$fname	= $_REQUEST["fname"];
		$lname		= $_REQUEST["lname"];
		$address	= $_REQUEST["address"];*/
		/*$city		= $_REQUEST["city"];
		$country	= $_REQUEST["country"];
		$state		= $_REQUEST["state"];
		$zip		= $_REQUEST["zip"];
		$tele_no	= $_REQUEST["tele_no"];*/
		/*$descr	= $_REQUEST["descr"];
		$status		= $_REQUEST["status"];
		$role		= $_REQUEST["role"];
		$email		= $_REQUEST["email"];
		$pass		= $_REQUEST["password"];*/
		$data_action 	= "insert";
	}	
?>
<script language="javascript" src="../js/zone_state.js" type="text/javascript"></script>

<form name="frx1" id="frx1" action="index.php?pagename=company_details&action=<?php echo $data_action;?>&pcode=<?php echo $pcode;?>" method="post">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="5">
          <tr>
            <td width="46%" class="pageheadTop">Company Manager</td>
            <td width="47%" class="headLink" align="right"><ul>
                <li><a href="index.php?pagename=company_manager" >Back</a></li>
              </ul></td>
            <td width="7%" align="right"><input type="submit" name="add" id="add" class="inputton" value="Save"  /></td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td class="pHeadLine"></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><table width="100%" border="0" cellpadding="5" cellspacing="0" class="tblBorder">
          <tr>
            <td colspan="5" class="pagehead">Company Details</td>
          </tr>
          <tr class="text">
            <td align="left" valign="top"  class="pad">Company Name<span class="redstar"> *</span></td>
            <td align="left" valign="top"  ><input name="comp_name" type="text" class="inpuTxt"  value="<?php echo $comp_name;?>"></td>
            <td valign="top" >&nbsp;</td>
            <td align="left" valign="top" >No. of Employees</td>
            <td valign="top" ><input type="text" class="inpuTxt" name="no_of_emp" id="no_of_emp" value="<?php echo $no_of_emp;?>" />
            <?php /*?><select class="inpuTxt" name="no_of_emp" id="no_of_emp">
                <option value="0" <?php if($no_of_emp=='0'){ echo "selected='selected'";}?>>Select No. of Employees</option>
                <option value="1-20" <?php if($no_of_emp=='1-20'){ echo "selected='selected'";}?>>1-20</option>
                <option value="20-50" <?php if($no_of_emp=='20-50'){ echo "selected='selected'";}?>>20-50</option>
                <option value="50-100" <?php if($no_of_emp=='50-100'){ echo "selected='selected'";}?>>50-100</option>
                <option value="101" <?php if($no_of_emp=='101'){ echo "selected='selected'";}?>> >100</option>
              </select><?php */?></td>
          </tr>
          <tr class="text">
            <td width="20%" valign="top" class="pad" >First Name<span class="redstar"> *</span></td>
            <td width="25%" valign="top" ><input type="text" name="fname" id="fname" class="inpuTxt"  value="<?php echo $fname;?>"></td>
            <td width="5%" valign="top" >&nbsp;</td>
            <td width="20%" align="left" valign="top" >Last Name</td>
            <td width="25%" valign="top" ><input type="text" name="lname" id="lname" class="inpuTxt"  value="<?php echo $lname;?>" /></td>
          </tr>
          <tr class="text">
            <td valign="top" class="pad">Designation</td>
            <td valign="top"><select name="designation" class="inpuTxt" id="designation" onchange="designation_chage(this.value)">
                <option value="0">Select Designation</option>
                <?php
		$query   	= "deleteflag='active' and designation_status='active' order by designation_name";
		$rs_desig   = $s->selectwhere('tbl_designation',$query);
		while($row_desig = mysqli_fetch_object($rs_desig))
		{
		?>
                <option  value="<?php echo $row_desig->designation_id; ?>"
	   <?php
		   if($row_desig->designation_id == $designation)
		   {
				echo "selected='selected'";
		   }
	   ?> ><?php echo stripslashes($row_desig->designation_name); ?></option>
                <?php } ?>
              </select>
              
              <script type="text/javascript">
			  var val1=0;
			  	function designation_chage(val1)
				 {
					 if(val1==14)
					  {
						  document.getElementById("designation1").style.display='inline';
					  }
				 }
			  </script>
              <style type="text/css">
			  	#designation1 {
					 display:none;
				}
			  </style>
              
              <p>
              <input type="text" name="designation" class="inpuTxt" value="<?php echo stripslashes($row_desig->designation_name); ?>"  id="designation1" /></p>
              </td>
            <td valign="top">&nbsp;</td>
            <td width="20%" align="left" valign="top">Email</td>
            <td width="25%" align="left" valign="top"><input type="text" name="email" id="email" class="inpuTxt"  value="<?php echo $email;?>" /></td>
          </tr>
          <tr class="text">
            <td width="20%" align="left" valign="top" class="pad">Zip/Postal Code <span class="redstar"> *</span></td>
            <td width="25%" align="left" valign="top"><input name="zip" type="text" class="inpuTxt" id="zip" value="<?php echo $zip;?>"></td>
            <td width="5%" valign="top">&nbsp;</td>
            <td width="20%" align="left" valign="top">Customer Segment <span class="redstar">*</span></td>
            <td width="25%" valign="top"><select name="cust_segment" id="cust_segment" class="inpuTxt">
                <option value="0" <?php if($cust_segment=='0' ){ echo "selected='selected'"; }?>>Select Customer Segment</option>
                 <option value="mes" <?php if($cust_segment=='mes' ){ echo "selected='selected'"; }?>>MES</option>
                <option value="govt" <?php if($cust_segment=='govt' ){ echo "selected='selected'"; }?>>Govt</option>
                <option value="Railway_SET" <?php if($cust_segment=='Railway_SET' ){ echo "selected='selected'"; }?>>Railway S&T</option>
                <option value="Railway_SET_Contractor" <?php if($cust_segment=='Railway_SET_Contractor' ){ echo "selected='selected'"; }?>>Railway S&T Contractor</option>
                <option value="Railway_Engineering" <?php if($cust_segment=='Railway_Engineering' ){ echo "selected='selected'"; }?>>Railway Engineering </option>
                <option value="Pvt_Infra" <?php if($cust_segment=='Pvt_Infra' ){ echo "selected='selected'"; }?>>Pvt Infra </option>
                <option value="Pvt_Building_Construction" <?php if($cust_segment=='Pvt_Building_Construction' ){ echo "selected='selected'"; }?>>Pvt Building Construction</option>
                <option value="bro" <?php if($cust_segment=='bro' ){ echo "selected='selected'"; }?>> BRO</option>
                <option value="Road_Contractor" <?php if($cust_segment=='Road_Contractor' ){ echo "selected='selected'"; }?>>Road Contractor</option>
                <option value="Educational_Research_Institute" <?php if($cust_segment=='Educational_Research_Institute' ){ echo "selected='selected'"; }?>>Educational & Research Institute</option>
                <option value="Reseller" <?php if($cust_segment=='Reseller' ){ echo "selected='selected'"; }?>>Reseller</option>
                <option value="Manufacturing" <?php if($cust_segment=='Manufacturing' ){ echo "selected='selected'"; }?>>Manufacturing</option>
                
                <option value="Consultant" <?php if($cust_segment=='Consultant' ){ echo "selected='selected'"; }?>>Consultant</option>
                <option value="Railway_Construction" <?php if($cust_segment=='Railway_Construction' ){ echo "selected='selected'"; }?>>Railway Construction</option>
                <option value="Railway_Engineering" <?php if($cust_segment=='Railway_Engineering' ){ echo "selected='selected'"; }?>>Railway Engineering</option>
                <option value="PWD_R_B" <?php if($cust_segment=='PWD_R_B' ){ echo "selected='selected'"; }?>>PWD R&B</option>
                <option value="CPWD" <?php if($cust_segment=='CPWD' ){ echo "selected='selected'"; }?>>CPWD</option>
                <option value="Govt_Others" <?php if($cust_segment=='Govt_Others' ){ echo "selected='selected'"; }?>>Govt. Others</option>
              </select></td>
          </tr>
          <tr class="text">
            <td width="20%" align="left" valign="top" class="pad">City<span class="redstar"> *</span></td>
            <td width="25%" align="left" valign="top" ><input name="city" type="text" class="inpuTxt" id="city" value="<?php echo $city;?>"></td>
            <td width="5%" align="left" valign="top" >&nbsp;</td>
            <td width="20%" align="left" valign="top" >Account Manager <span class="redstar">*</span></td>
            <td width="25%" align="left" valign="top" ><select name="acc_manager" class="inpuTxt" id="acc_manager">
                <option value="0" <?php if($acc_manager=='0' ){ echo "selected='selected'"; }?>>Select Account Manager</option>
                <?php
            $query_acc_manager   	= "deleteflag='active' and admin_status='active' order by admin_fname";
            $rs_desig_acc_manager   = $s->selectwhere('tbl_admin',$query_acc_manager);
            while($row_desig_acc_manager = mysqli_fetch_object($rs_desig_acc_manager))
            {
            ?>
                <option  value="<?php echo $row_desig_acc_manager->admin_id; ?>"
	   <?php  if($row_desig_acc_manager->admin_id == $acc_manager){ echo "selected='selected'";}?>><?php echo stripslashes($row_desig_acc_manager->admin_fname)."&nbsp;".stripslashes($row_desig_acc_manager->admin_lname); ?></option>
                <?php } ?>
              </select></td>
          </tr>
          <tr class="text">
            <td width="20%" align="left" valign="top" class="pad"> Country<span class="redstar"> *</span></td>
            <td width="25%" align="left" valign="top"><select name="country" id="country" class="inpuTxt" onchange="htmlData('zone_state.php', 'ch='+this.value);"  >
                <option value="">Select Country</option>
                <?php
	$sql_country="select * from tbl_country order by country_name";
	$rs_co=mysqli_query($GLOBALS["___mysqli_ston"],$sql_country);
	if(mysqli_num_rows($rs_co)>0)
	{
		while($row_country=mysqli_fetch_object($rs_co))
		{	
?>
                <option value="<?php echo $row_country->country_id;?>" <?php if($row_country->country_id==99 ){ echo "selected='selected'"; }?>><?php echo ucfirst($row_country->country_name);?></option>
                <?php
		}
	}
?>
              </select></td>
            <td width="5%"  align="left" valign="top">&nbsp;</td>
            <td align="left" valign="top">Referral Source</td>
            <td valign="top"><select name="ref_source" id="ref_source" class="inpuTxt">
                <option value="0" <?php if($ref_source=='0' ){ echo "selected='selected'"; }?>>Select Referral Source</option>
                <option value="visit_made" <?php if($ref_source=='visit_made' ){ echo "selected='selected'"; }?>>Visit Made</option>
                <option value="phone_in" <?php if($ref_source=='phone_in' ){ echo "selected='selected'"; }?>>Phone In</option>
                <option value="website_google" <?php if($ref_source=='website_google' ){ echo "selected='selected'"; }?>>Website Google</option>
                <option value="just_dial" <?php if($ref_source=='just_dial' ){ echo "selected='selected'"; }?>> Just Dial</option>
                <option value="referral" <?php if($ref_source=='referral' ){ echo "selected='selected'"; }?>> Referral</option>
                <option value="emal_fax" <?php if($ref_source=='emal_fax' ){ echo "selected='selected'"; }?>> Email/Fax(General)</option>
                <option value="trade_show" <?php if($ref_source=='trade_show' ){ echo "selected='selected'"; }?>> Trade Show</option>
                <option value="published_tender" <?php if($ref_source=='published_tender' ){ echo "selected='selected'"; }?>> Published Tender</option>
                <option value="advertisement" <?php if($ref_source=='advertisement' ){ echo "selected='selected'"; }?>> Advertisement</option>
                <option value="website_other" <?php if($ref_source=='website_other' ){ echo "selected='selected'"; }?>> Website other</option>
              </select></td>
          </tr>
          <tr class="text">
            <td width="20%" align="left" valign="top" class="pad">State/Province <span class="redstar"> *</span></td>
            <td width="25%" align="left" valign="top" id="txtResult"><?php	
	if(is_numeric($state))
	{
		$rs_module = $s->getData_with_condition('tbl_zones','zone_id',$state);
		if(mysqli_num_rows($rs_module)>0)
		{
			$row_state = mysqli_fetch_object($rs_module);
	?>
              <input type="text" name="state" id="state" value="<?php echo $row_state->zone_name;?>" class="inpuTxt" />
              <?php
		}
	}
	else
	{
?>
              <input type="text" name="state" id="state" value="<?php echo stripslashes($state);?>" class="inpuTxt" />
              <?php	
	}
	if($error == 1)
	{
		echo "<span class='redstar'>Please fill the State.</span>";
		$error = -1;
	}
?></td>
            <td width="5%" align="left" valign="top" id="txtResult">&nbsp;</td>
            <td width="20%" align="left" valign="top" id="txtResult">Website</td>
            <td width="25%" align="left" valign="top" id="txtResult"><input type="text" name="comp_website" id="comp_website" class="inpuTxt"  value="<?php echo $comp_website;?>" /></td>
          </tr>
          <tr class="text">
            <td width="20%" valign="top" class="pad">Address <span class="redstar">*</span></td>
            <td width="25%" valign="top"><textarea name="address" id="address" class="inpuTxt"  rows="6"><?php echo $address;?></textarea></td>
            <td width="5%" align="left" valign="top">&nbsp;</td>
            <td width="20%" align="left" valign="top">Revenue</td>
            <td width="25%" align="left" valign="top"><input type="text" name="comp_revenue" id="comp_revenue"  class="inpuTxt"  value="<?php echo $comp_revenue;?>" /></td>
          </tr>
          <tr class="text">
            <td width="20%" valign="top" class="pad">Telephone No. <span class="redstar"> </span></td>
            <td width="25%" valign="top"><input type="text" name="tele_no" id="tele_no" class="inpuTxt"  value="<?php echo $tele_no;?>"></td>
            <td width="5%" valign="top">&nbsp;</td>
            <td width="20%" align="left" valign="top">&nbsp;</td>
            <td width="25%" valign="top">&nbsp;</td>
          </tr>
          <tr class="text">
            <td valign="top" class="pad">Mobile No</td>
            <td valign="top"><input type="text" name="mobile_no" id="mobile_no" class="inpuTxt"  value="<?php echo $mobile_no;?>"></td>
            <td valign="top">&nbsp;</td>
            <td align="left" valign="top">&nbsp;</td>
            <td valign="top">&nbsp;</td>
          </tr>
          <tr class="text">
            <td width="20%" valign="top" class="pad">Fax No. </td>
            <td width="25%" valign="top"><input type="text" name="fax_no" id="fax_no" class="inpuTxt"  value="<?php echo $fax_no;?>"></td>
            <td width="5%" valign="top">&nbsp;</td>
            <td width="20%" align="left" valign="top">&nbsp;</td>
            <td width="25%" valign="top">&nbsp;</td>
          </tr>
          <tr class="text">
            <td width="20%"  valign="top" class="pad">Description</td>
            <td width="25%" valign="top"><textarea name="descr" id="descr" class="inpuTxt" rows="6"><?php echo $descr;?></textarea></td>
            <td width="5%" valign="top">&nbsp;</td>
            <td width="20%" align="left" valign="top">&nbsp;</td>
            <td width="25%" valign="top">&nbsp;</td>
          </tr>
          <tr class="text">
            <td width="20%" valign="top" class="pad">Status</td>
            <td width="25%" valign="top"><select name="status" id="status" class="inpuTxt">
                <option value="active" <?php if($status == "active"){ echo "selected='selected'";}?>>Active</option>
                <option value="inactive" <?php if($status == "inactive"){ echo "selected='selected'";}?>>Inactive</option>
              </select></td>
            <td width="5%" valign="top">&nbsp;</td>
            <td width="20%" align="left" valign="top">&nbsp;</td>
            <td width="25%" valign="top">&nbsp;</td>
          </tr>
          <?php if($_REQUEST["action"] != 'edit')
	{?>
          <tr class="text">
            <td class="redstar pad" > * Required Fields </td>
            <td valign="top"><input name='add2' id='add2' type='submit' class='inputton' value='Save' /></td>
            <td valign="top">&nbsp;</td>
            <td align="left" valign="top">&nbsp;</td>
            <td valign="top">&nbsp;</td>
          </tr>
          <?php } //php role table mapp here ?>
        </table></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <?php
   $sql_rs	= "SELECT * from tbl_comp_emp where company_id='$pcode'  and deleteflag='active'";
   $rs_emp	= mysqli_query($GLOBALS["___mysqli_ston"],$sql_rs);
   $num_emp	= mysqli_num_rows($rs_emp);
   if($num_emp>0)
   {
  ?>
    <tr class="text">
      <td ><table width="100%" border="0" cellpadding="4" cellspacing="0" class="tblBorder">
          <tr class="pagehead">
            <td colspan="5" class="pad">Person Contact Details</td>
          </tr>
          <tr class="head">
            <td class="pad">Name</td>
            <td>Phone</td>
            <td>Email</td>
            <td>Status</td>
            <td>Action</td>
          </tr>
          <?php
while($row_emp=mysqli_fetch_object($rs_emp))
{
  ?>
          <tr>
            <td class="pad"><?php echo $row_emp->fname;?></td>
            <td><?php echo $row_emp->phone;?></td>
            <td><?php echo $row_emp->email;?></td>
            <td><?php 
	if($row_emp->status == "active")
	{
?>
              <img src="images/green.gif" title="Active" border="0" alt=""  /> &nbsp; &nbsp; <a href="index.php?pagename=add_company&action=ChangeStatus&pcode_emp=<?php echo $row_emp->comp_emp_id;?>&pcode=<?php echo $pcode;?>"><img src="images/red_light.gif" title="Inactive" border="0" alt=""  /></a>
              <?php
	}
	else if($row_emp->status == "inactive")
	{
?>
              <a href="index.php?pagename=add_company&action=ChangeStatus&pcode_emp=<?php echo $row_emp->comp_emp_id;?>&pcode=<?php echo $pcode;?>"><img src="images/green_light.gif" title="Active" border="0"  alt="" /></a> &nbsp; &nbsp; <img src="images/red.gif" title="Inactive" border="0" alt=""  />
              <?php		
	}
?></td>
            <td><a href="index.php?pagename=add_product&action=edit&pcode=<?php echo $row->id;?>"></a> <!--<a href="index.php?pagename=add_company&action=edit&pcode=<?php echo $row->id;?>"> <img src="images/e.gif" border="0"  alt="Edit"/></a> --> &nbsp; &nbsp;<a href="index.php?pagename=add_company&action=delete&pcode_emp=<?php echo $row_emp->comp_emp_id;?>&pcode=<?php echo $row->id;?>" onclick='return del();'> <img src="images/x.gif" border="0" alt="Delete" /></a></td>
          </tr>
          <?php }?>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
        </table></td>
    </tr>
    <?php 
   }
	if($_REQUEST["action"] == 'edit')
	{?>
    <tr>
      <td><table width="100%" border="0" cellpadding="0" cellspacing="0" class="tblBorder">
          <tr class="pagehead">
            <td colspan="2" class="pad" > Person Contact No. &amp; Details</td>
          </tr>
          
          <tr>
            <td colspan="2"><table width="100%"  cellpadding="0" cellspacing="0" class="tblBorder" border="0">
                <tr>
                  <td colspan="2"><table width="100%" cellpadding="5" cellspacing="0" >
                      <tr class="text">
                        <td class="pad" valign="top">Name </td>
                        <td><input type="text" id="cust_emp_fname[]" name="cust_emp_fname[]" class="inpuTxt" 
		        value="<?php echo $image_name['cust_emp_fname'];?>"   width="60"></td>
                      </tr>
                      <tr class="text">
                        <td class="pad" valign="top">Phone </td>
                        <td><input type="text" id="cust_emp_phone[]" name="cust_emp_phone[]" class="inpuTxt" 
		        value="<?php echo $image_name['cust_emp_phone'];?>"   width="60"></td>
                      </tr>
                      <tr class="text">
                        <td class="pad" width="20%">Email</td>
                        <td><input type="text" id="cust_emp_email[]" name="cust_emp_email[]" class="inpuTxt" 
		        value="<?php echo $image_name['cust_emp_email'];?>"   width="60"></td>
                      </tr>
                      <tr class="text">
                        <td></td>
                        <td ><input name='add_image' id='add_image' type='button' class='inputton' value='Add More'  onclick="toggle_it('pr1')"/></td>
                      </tr>
                    </table></td>
                </tr>
                <tr id="pr1" style="display:none">
                  <td colspan="2"><table width="100%" cellpadding="5" cellspacing="0" >
                      <tr class="text">
                        <td class="pad" valign="top">Name </td>
                        <td><input type="text" id="cust_emp_fname[]" name="cust_emp_fname[]" class="inpuTxt" 
		        value="<?php echo $image_name['cust_emp_fname'];?>"   width="60"></td>
                      </tr>
                      <tr class="text">
                        <td class="pad" valign="top">Phone </td>
                        <td><input type="text" id="cust_emp_phone[]" name="cust_emp_phone[]" class="inpuTxt" 
		        value="<?php echo $image_name['cust_emp_phone'];?>"   width="60"></td>
                      </tr>
                      <tr class="text">
                        <td class="pad" width="20%">Email</td>
                        <td><input type="text" id="cust_emp_email[]" name="cust_emp_email[]" class="inpuTxt" 
		        value="<?php echo $image_name['cust_emp_email'];?>"   width="60"></td>
                      </tr>
                      <tr class="text">
                        <td></td>
                        <td ><input name='add_image' id='add_image' type='button' class='inputton' value='Add More'  onclick="toggle_it('pr2')"/></td>
                      </tr>
                    </table></td>
                </tr>
                <tr id="pr2" style="display:none">
                  <td colspan="2"><table width="100%" cellpadding="5" cellspacing="0" >
                      <tr class="text">
                        <td class="pad" valign="top">Name </Td>
                        <td><input type="text" id="cust_emp_fname[]" name="cust_emp_fname[]" class="inpuTxt" 
		        value="<?php echo $image_name['cust_emp_fname'];?>"   width="60"></td>
                      </tr>
                      <tr class="text">
                        <td class="pad" valign="top">Phone </Td>
                        <td><input type="text" id="cust_emp_phone[]" name="cust_emp_phone[]" class="inpuTxt" 
		        value="<?php echo $image_name['cust_emp_phone'];?>"   width="60"></td>
                      </tr>
                      <tr class="text">
                        <td class="pad" width="20%">Email</Td>
                        <td><input type="text" id="cust_emp_email[]" name="cust_emp_email[]" class="inpuTxt" 
		        value="<?php echo $image_name['cust_emp_email'];?>"   width="60"></td>
                      </tr>
                      <tr class="text">
                        <td></td>
                        <td ><input name='add_image' id='add_image' type='button' class='inputton' value='Add More'  onclick="toggle_it('pr3')"/></td>
                      </tr>
                    </table></td>
                </tr>
                <tr id="pr3" style="display:none">
                  <td colspan="2"><table width="100%" cellpadding="5" cellspacing="0" >
                      <tr class="text">
                        <td class="pad" valign="top">Name </td>
                        <td><input type="text" id="cust_emp_fname[]" name="cust_emp_fname[]" class="inpuTxt" 
		        value="<?php echo $image_name['cust_emp_fname'];?>"   width="60"></td>
                      </tr>
                      <tr class="text">
                        <td class="pad" valign="top">Phone </td>
                        <td><input type="text" id="cust_emp_phone[]" name="cust_emp_phone[]" class="inpuTxt" 
		        value="<?php echo $image_name['cust_emp_phone'];?>"   width="60"></td>
                      </tr>
                      <tr class="text">
                        <td class="pad" width="20%">Email</td>
                        <td><input type="text" id="cust_emp_email[]" name="cust_emp_email[]" class="inpuTxt" 
		        value="<?php echo $image_name['cust_emp_email'];?>"   width="60"></td>
                      </tr>
                      <tr class="text">
                        <td></td>
                        <td><input name='add_image' id='add_image' type='button' class='inputton' value='Add More' onclick="toggle_it('pr4')"/></td>
                      </tr>
                    </table></td>
                </tr>
                <tr id="pr4" style="display:none">
                  <td colspan="2"><table width="100%" cellpadding="5" cellspacing="0" >
                      <tr class="text">
                        <td class="pad" valign="top">Name </td>
                        <td><input type="text" id="cust_emp_fname[]" name="cust_emp_fname[]" class="inpuTxt" 
		        value="<?php echo $image_name['cust_emp_fname'];?>"   width="60"></td>
                      </tr>
                      <tr class="text">
                        <td class="pad" valign="top">Phone </td>
                        <td><input type="text" id="cust_emp_phone[]" name="cust_emp_phone[]" class="inpuTxt" 
		        value="<?php echo $image_name['cust_emp_phone'];?>"   width="60"></td>
                      </tr>
                      <tr class="text">
                        <td class="pad" width="20%">Email</Td>
                        <td><input type="text" id="cust_emp_email[]" name="cust_emp_email[]" class="inpuTxt" 
		        value="<?php echo $image_name['cust_emp_email'];?>"   width="60"></td>
                      </tr>
                      <tr class="text">
                        <td></td>
                        <td><input name='add_image' id='add_image' type='button' class='inputton' value='Add More' /></td>
                      </tr>
                    </table></td>
                </tr>
                <tr class="text">
                  <td colspan="2" class="pad"><input name='add' id='add' type='submit' class='inputton' value='Save' /></td>
                </tr>
                <tr class="text">
                  <td class="redstar pad" colspan="2"> * Required Fields </td>
                </tr>
              </table></td>
          </tr>
        </table></td>
    </tr>
    <?php }?>
  </table>
</form>
<script language="javascript" type="text/javascript">
 var frmvalidator = new Validator("frx1");
 frmvalidator.addValidation("fname","req","Please enter First Name.");
// frmvalidator.addValidation("tele_no","req","Please enter Telephone No.");
 frmvalidator.addValidation("address","req","Please enter address.");
 frmvalidator.addValidation("city","req","Please enter city.");
 frmvalidator.addValidation("country","dontselect=0","Please select country.");
 frmvalidator.addValidation("state","req","Please enter state.");
 frmvalidator.addValidation("zip","req","Please enter zip/postal code.");
 frmvalidator.addValidation("cust_segment","dontselect=0","Please select Customer Segment.");
 frmvalidator.addValidation("acc_manager","dontselect=0","Please select Account Manager.");
 //frmvalidator.addValidation("email","req","Please enter email address.");
 //frmvalidator.addValidation("email","email","Please enter valid email address.");
 //frmvalidator.addValidation("password","req","Please enter password.");
 //frmvalidator.addValidation("confirm_pass","req","Please enter confirm password.");
 //frmvalidator.setAddnlValidationFunction("DoCustomValidation");
 /*function DoCustomValidation()
{
  var frm = document.forms["frx1"];
  if(frm.password.value != frm.confirm_pass.value)
  {
    alert('The Password and confirm password does not match!');
    return false;
  }
  else
  {
    return true;
  }
}*/
</script>