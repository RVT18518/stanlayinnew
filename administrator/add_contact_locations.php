<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<?php 
	$data_action = $_REQUEST["action"];
	$pcode		 = $_REQUEST["pcode"];
	
	if($data_action == "delete_image")
	{
		$rs_del    			= $s->getData_with_condition('tbl_contact_us_page_location','id',$pcode);
		$row_del   			= mysqli_fetch_object($rs_del);
		$del_image_large  	= "../".$row_del->image_path;
		if(file_exists($del_image_large)>0)
		{
			unlink($del_image_large);
		}
		$sql_remove = "update tbl_contact_us_page_location SET image_path = '' where id = $pcode";
		$result     = mysqli_query($GLOBALS["___mysqli_ston"],$sql_remove);
		if($result == 0)
		{
			$Msz  = 0; 
		}
		else if($result == 1)
		{ 
			$Msz  = 1;
		}
		$data_action = "edit";
	}
	if($data_action == "delete_logo")
	{
		$rs_del    			= $s->getData_with_condition('tbl_contact_us_page_location','id',$pcode);
		$row_del   			= mysqli_fetch_object($rs_del);
		$del_image_large  	= "../".$row_del->logo_path;
		if(file_exists($del_image_large)>0)
		{
			unlink($del_image_large);
		}
		$sql_remove = "update tbl_contact_us_page_location SET logo_path = '' where pages_id = $pcode";
		$result     = mysqli_query($GLOBALS["___mysqli_ston"],$sql_remove);
		if($result == 0)
		{
			$Msz  = 0; 
		}
		else if($result == 1)
		{ 
			$Msz  = 1;
		}
		$data_action = "edit";
	}

	if($data_action=="edit")
	{
		$rs  			 = $s->getData_with_condition('tbl_contact_us_page_location','id',$pcode);
		$row 			 = mysqli_fetch_object($rs);
		$location		 = $row->location;
		$address     	 = $row->address;
		
		$google_code     = $row->google_code;
		$image_path		 = $row->image_path;
		$sort_order		 = $row->sort_order;
		$data_action     = "update";
		
	
	}
	if($data_action == "add_new")
	{
		$data_action = "insert";
	}	
		
?>
<form name="frx1" id="frx1" action="index.php?pagename=contact_us_location_manager&action=<?php echo $data_action;?>&pcode=<?php echo $pcode;?>" method="post" enctype="multipart/form-data">
  <table width="100%"  align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="49%" class="pageheadTop">Location Manager</td>
            <td width="43%" class="headLink"><!--<ul>
                <li><a href="#" onclick="back_page();">Back</a></li>
            </ul>--></td>
            <td width="8%"><?php if($_SESSION["AdminLoginID_SET"]!='131') {?><input name="save"  type="submit" class="inputton" id="save" value="Save"><?php }?></td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td class="pHeadLine"></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><table width="100%" cellpadding="0" cellspacing="0" class="tblBorder">
          <tr>
            <td colspan="3"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td class="pagehead"> Location Details</td>
                </tr>
              </table></td>
          </tr>
          <tr class="text">
            <td width="12%"  class="pad">Location <span class="redstar">* </span></td>
            <td colspan="2"><input name="location" type="text" class="inpuTxt" id="location" value="<?php  echo $location; ?>" /></td>
          </tr>
        
        
          <tr class="text">
            <td  class="pad">&nbsp;</td>
            <td colspan="2">&nbsp;</td>
          </tr>
          <tr class="text">
            <td width="12%"  class="pad">Address <span class="redstar">* </span></td>
            <td colspan="2">
            <?php
					//$sBasePath = $_SERVER['PHP_SELF'] ;
					//$sBasePath = substr( $sBasePath, 0, strpos( $sBasePath, "file" ) ) ;
					$sBasePath  = "../fckeditor/";
					$oFCKeditor = new FCKeditor('address') ;
					$oFCKeditor->BasePath = $sBasePath;
					$oFCKeditor->Value		=  stripslashes($address); 
					$oFCKeditor->Width  = '40%' ;
					$oFCKeditor->Height = '200' ;
					$oFCKeditor->ToolbarSet = 'Basic';
					$oFCKeditor->Create() ;
				?>
                
             
            <?php /*?><textarea name="address" id="address" rows="10" cols="33" ><?php  echo $address; ?></textarea><?php */?></td>
          </tr>
        
         
          <tr class="text">
            <td  class="pad">&nbsp;</td>
            <td colspan="2">&nbsp;</td>
          </tr>
          <tr class="text">
            <td width="12%"  class="pad">Location Map <span class="redstar">* </span></td>
            <td colspan="2"><textarea  name="google_code" class="inpuTxt" rows="10" id="google_code" ><?php  echo $google_code; ?></textarea></td>
          </tr>
        
         
          <tr class="text" style="height:5px;">
            <td colspan="3">&nbsp;</td>
          </tr>
      
     
          <tr class="text">
            <td class="pad">Location Image</td>
            <td colspan="2"><input type="file" name="location_image"  class="form-control name_list" />
              &nbsp;(Size: 1344px X 282px)
              <?php if($image_path != ""){?>
              <img src="../<?php echo $image_path;?>" width="100" />
              <a href="download.php?action=downloadfile&file=<?php echo "../".$image_path;?>">Download File</a> &nbsp; | &nbsp; <a href="index.php?pagename=add_contact_locations&action=delete_image&pcode=<?php echo $pcode;?>">Remove File</a>
              <?php }?></td>
          </tr>
          
          <!--<tr class="text"><td class="pad">Add Locations</td><td colspan="2">
          <div class="input_fields_container"><div><input type="text" name="product_name[]">
      <input type="text" name="product_name2[]"><input type="text" name="product_name3[]">
<button class="btn btn-sm btn-primary add_more_button">Add More Fields</button></div></div>
</td></tr>
           -->
           
          <?php /*
		 <tr class="text">
		  <td class="pad">Page Small Image </td>
		  <td ><input type="file" name="logo_path" id="logo_path" /><?php if($logo_path != ""){?>
		<a href="download.php?action=downloadfile&file=<?php echo "../".$logo_path;?>">Download File</a> &nbsp; | &nbsp; <a href="index.php?pagename=add_contact_locations&action=delete_logo&pcode=<?php echo $pcode;?>">Remove File</a><?php }?></td>
		  <td width="56%">This image will be displayed in the Small box on the home page </td>
		  </tr>
*/ ?>
          <?php /*?><tr class="text">
            <td class="pad"> Add as a Quick Link</td>
            <td colspan="2"><select name="quick_link" class="inpuTxt" id="quick_link">
                <option value="yes" <?php  if( $quick_link == "yes"){ echo "selected='selected'";}?>>Yes</option>
                <option value="no"  <?php  if( $quick_link == "no") { echo "selected='selected'";}?>>No</option>
              </select></td>
          </tr><?php */?>
          <?php /*?><tr class="text">
	  <td align="left" valign="top" class="pad">Display Order </td>
	  <td align="left"><input name="sort_order" type="text" class="inpuTxt" id="sort_order" value="<?php echo $sort_order; ?>"></td>
	  </tr><?php */?>
          <tr class="text">
            <td class="pad">&nbsp;</td>
            <td colspan="2">&nbsp;</td>
          </tr>
          <tr class="text">
            <td class="pad"> Status</td>
            <td colspan="2"><select name="page_status" class="inpuTxt" id="page_status">
                <option value="active"   <?php  if( $page_status == "active"){ echo "selected='selected'";}?> >Active</option>
                <option value="inactive" <?php  if( $page_status == "inactive"){ echo "selected='selected'";}?>>Inactive</option>
              </select></td>
          </tr>
          <tr class="text">
            <td class="pad"></td>
            <td colspan="2">&nbsp;</td>
          </tr>
          <tr class="text">
            <td class="pad"></td>
            <td colspan="2"><?php if($_SESSION["AdminLoginID_SET"]!='131') {?><input name="save"  type="submit" class="inputton" id="save" value="Save"><?php }?></td>
          </tr>
          <tr class="text">
            <td class="redstar pad" colspan="3"> * Required Fields </td>
          </tr>
        </table></td>
    </tr>
  </table>
</form>
<script language="JavaScript" type="text/javascript">
 var frmvalidator = new Validator("frx1");
 frmvalidator.addValidation("location","req","Please enter location "); 
</script> 

