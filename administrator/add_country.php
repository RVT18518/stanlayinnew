<?php 
if(isset($_REQUEST['save']) || isset($_REQUEST['edit']))
{
	
	$dataArray['country_name']   = $_REQUEST['country_name'];
	$dataArray['country_code2']  = $_REQUEST['country_code2'];
	$dataArray['country_code3']  = $_REQUEST['country_code3'];
	$dataArray['country_status'] = $_REQUEST['country_status'];

	if(isset($_REQUEST['save']))
	{
	 $country_add = $s->insertRecord(tbl_country, $dataArray);
	
		if($country_add == 0)
		{ 	$s->javascriptRedirect("index.php?pagename=manage_country&msg=add"); }
		else 	{ 	$_SESSION['msg'] = record_not_added; }	
	}
	else if(isset($_REQUEST['edit']))
	{
		$id = $_REQUEST['id_'];	
		$edit = $s->editRecord(tbl_country, $dataArray, country_id , $id);
	
		if($edit == 0)
		{ 	$s->javascriptRedirect("index.php?pagename=manage_country&msg=edit"); }
		else { 	$_SESSION['msg'] == record_not_update; 	}
	}
}


if($_REQUEST['action'] == 'edit')
{
 $id 	 = $_REQUEST['id'];
 $query  = mysqli_query($GLOBALS["___mysqli_ston"],"select * from tbl_country where country_id ='$id'");
 $result = mysqli_fetch_array($query);
?>
<form name="frx1" id="frx1" action="" method="post">
<table width="100%"  cellspacing="0" cellpadding="0">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="24%" class="pageheadTop">Country Manager</td>
          <td width="76%" class="headLink"><ul>
              <li><a href="index.php?pagename=manage_country">Back</a> </li></ul>
		 </td>
		  <td width="76%" class="headLink"><input type="submit" name="edit" id="edit" class="inputton" value="Save" />
		 </td>
	  </tr>
		
    </table></td>
  </tr>
  <tr>
    <td class="pHeadLine"></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>

<tr><td valign="top" class="pagecontent">

<?php
if($_SESSION['msg'] != "")
{
	if ($_SESSION['msg'] == record_not_added)
	{
		echo "<p class='error'>".record_not_added."</p><br />";
	}
	else if ($_SESSION['msg'] == record_not_update)
	{
		echo "<p class='error'>".$_SESSION['msg']."</p> <br />";
	}
}
?>
<table width="100%"  border="0" cellpadding="0" cellspacing="0" class="tblBorder">
  <tr align="left">
    <td colspan="2" ><table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr> <td class="pagehead">Country</td>  </tr>
   </table></td>
  </tr>

<input type="hidden" id="id_" name="id_" value="<?php echo $id;?>" />
  <tr class="text" align="left">
    <td width="19%" class="pad" >Country Name<span class="redstar"> *</span></td>
    <td width="81%"><input name="country_name" id="country_name" type="text" class="inpuTxt" value="<?php echo $result['country_name']; ?>" /></td>
  </tr>
  <tr class="text" align="left">
    <td width="19%" class="pad">Country ISO Code (2 char)<span class="redstar"> *</span></td>
    <td ><input name="country_code2" id="country_code2" type="text" class="inpuTxt" value="<?php echo $result['country_code2']; ?>" /></td>
  </tr>
  <tr class="text" align="left">
    <td width="19%" class="pad">Country ISO Code (3 char)<span class="redstar"> *</span></td>
    <td><input name="country_code3" id="country_code3" type="text" class="inpuTxt" value="<?php echo $result['country_code3']; ?>" /></td>
  </tr>
  <tr class="text" align="left">
    <td width="19%" class="pad">Status</td>
    <td><select name="country_status" id="country_status" class="inpuTxt">
      <option value="active" <?php if($result['country_status']=="active"){ echo "selected";}?>>Active</option>
      <option value="inactive"  <?php if($result['country_status']=="inactive"){ echo "selected";}?>>Inactive</option>
    </select>    </td>
  </tr>
  <tr class="text" align="left">
    <td class="pad"></td>
    <td width="81%"><input  type="submit" name="edit" id="edit" class="inputton" value="Save" /> &nbsp; </td>
  </tr>
  <tr class="text"><td class="redstar pad" colspan="2"> * Required Fields </td></tr>  
</table>
</td>
</tr>
</table>
</form>
<?php }else if($_REQUEST['action'] != 'edit')
{?>
<form name="frx1" id="frx1	" action="" method="post">
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="24%" class="pageheadTop">Country Manager</td>
          <td width="76%" class="headLink"><ul>
              <li><a href="index.php?pagename=manage_country">Back</a></li>
          </ul></td>
          <td width="76%" class="headLink">
		  <input  type="submit" name="save" id="save" class="inputton" value="Save" border="0" />
          </td>
        </tr>
    </table></td>
  </tr>
  <tr>
    <td class="pHeadLine"></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
<tr><td valign="top" class="pagecontent">
<?php
if($_SESSION['msg'] != "")
{
	if ($_SESSION['msg'] == record_not_added)
	{
		echo "<p class='error'>".$_SESSION['msg']."</p><br />";
	
	}else if ($_SESSION['msg'] == record_added){ 
	
		echo "<p class='success'>".$_SESSION['msg']."</p> <br/>";       
	
	}else if ($_SESSION['msg'] == record_not_update)
	{
	
		echo "<p class='error'>".$_SESSION['msg']."</p> <br />";
			
	}else if ($_SESSION['msg'] == record_update){ 
	
		echo "<p class='success'>".$_SESSION['msg']."</p><br />";
	}
}
?>
<table width="100%"  border="0" cellpadding="0" cellspacing="0" class="tblBorder">
  <tr align="left">
    <td colspan="2" ><table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr class="pagehead"><td class="pad"> Country</td> 
  </tr>
   </table></td>
  </tr>
  <tr class="text" align="left">
    <td width="19%" class="pad" >Country Name<span class="redstar"> *</span></td>
    <td width="81%"><input name="country_name" id="country_name" type="text" class="inpuTxt" value="" /></td>
  </tr>
  <tr class="text" align="left">
    <td width="19%" class="pad">Country ISO Code (2 char)<span class="redstar"> *</span></td>
    <td ><input name="country_code2" id="country_code2" type="text" class="inpuTxt" value="" /></td>
  </tr>
  <tr class="text" align="left">
    <td width="19%" class="pad">Country ISO Code (3 char)<span class="redstar"> *</span></td>
    <td><input name="country_code3" id="country_code3" type="text" class="inpuTxt" value="" /></td>
  </tr>
  <tr class="text" align="left">
    <td width="19%" class="pad">Status</td>
    <td><select name="country_status" id="country_status" class="inpuTxt">
	  <option value="active" selected >Active</option>
      <option value="inactive">Inactive</option>
    </select>    </td>
  </tr>
  <tr class="text" align="left">
    <td class="pad"></td>
    <td width="81%"><input  type="submit" name="save" id="save" class="inputton" value="Save" />&nbsp;</td>
  </tr>
  <tr class="text"><td class="redstar pad" colspan="2"> * Required Fields </td></tr>  
<?php }  ?>
</table>
</td>
</tr></table>
</form>
<!-- This function will validate the form -->
<script language="JavaScript" type="text/javascript">
	var frmvalidator = new Validator("frx1");
	frmvalidator.addValidation("country_name","req","Please enter Country Name.");
	frmvalidator.addValidation("country_code2","req","Please enter Country Code");
	frmvalidator.addValidation("country_code3","req","Please enter Country ISO Code3");
	frmvalidator.addValidation("country_status","req","Please Select Country Status");
	
//	function V2validateData(strValidateStr,objValue,strError) 
// 	var frmvalidator = new V2validateData("maxlength", "country_code2", "strError");
	frmvalidator.addValidation("country_code2","maxlen=2");
	frmvalidator.addValidation("country_code3","maxlen=3");
</script>