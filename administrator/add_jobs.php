<link href="css/style.css" rel="stylesheet" type="text/css" />
<?php 
	$data_action = $_REQUEST["action"];
	$pcode		 = $_REQUEST["pcode"];
	if($data_action=="edit")
	{
		$rs  			= $s->getData_with_condition('tbl_jobs','job_id',$pcode);
		$row 			= mysqli_fetch_object($rs);
		$job_code		= $row->job_code;
		$job_title     	= $row->job_title;
		$location      	= $row->location;
		$experience    	= $row->experience;
		$no_of_posts   	= $row->no_of_posts;
		
		
		$date_from 		= $s->getDateformate($row->date_from,'ymd','ymd');
		$date_to		= $s->getDateformate($row->date_to,'ymd','ymd');
		$jobdescription	= $row->jobdescription;
		$status		 	= $row->status;
		$data_action    = "update";
	}
	if($data_action == "add_new")
	{
		$data_action = "insert";
	}	
?>
<SCRIPT language="JavaScript">
var cal2 = new CalendarPopup("testdiv2");
cal2.setReturnFunction("showDate1");
cal2.showYearNavigation();
var cal1 = new CalendarPopup("testdiv1");
cal1.setReturnFunction("showDate2");
cal1.showYearNavigation();
</SCRIPT>
<SCRIPT language="JavaScript">
document.write(cal2.getStyles());
document.write(cal1.getStyles());</SCRIPT>
<SCRIPT language="JAVASCRIPT">
// Function to get input back from calendar popup
function showDate1(y,m,d) {
document.frx1.date_from.value   = y + "-" + m + "-" + d;
}		
function showDate2(y,m,d) {
document.frx1.date_to.value   = y + "-" + m + "-" + d;
}		
</script>

<form name="frx1" id="frx1" action="index.php?pagename=manager_job&action=<?php echo $data_action;?>&pcode=<?php echo $pcode;?>" method="post" enctype="multipart/form-data">
<table width="100%"  align="center" cellpadding="0" cellspacing="0">
	<tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="24%" class="pageheadTop">Job Manager</td>
            <td width="76%" class="headLink"><ul>
                <li><a href="index.php?pagename=manager_job" >Back</a></li>
            </ul></td><td><input name="save"  type="submit" class="inputton" id="save" value="Save"></td>
          </tr>
      </table></td>
    </tr>
	<tr>
      <td class="pHeadLine"></td>
    </tr>
	<tr>
      <td>&nbsp;</td>
    </tr>
	
	<tr><td>
	<table width="100%" cellpadding="0" cellspacing="0" class="tblBorder">
	<tr>
	  <td colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr><td class="pagehead">  Page Details</td>
	</tr>
      </table></td>
	  </tr>
	<tr class="text">
	  <td width="12%"  class="pad">Job Code <span class="redstar">* </span></td>
	  <td width="88%"><input name="job_code" type="text" class="inpuTxt" id="job_code" value="<?php  echo $job_code; ?>" /></td></tr>
<tr class="text">
	  <td width="12%"  class="pad">Job Title  <span class="redstar">* </span></td>
	  <td width="88%"><input name="job_title" type="text" class="inpuTxt" id="job_title" value="<?php  echo stripslashes($job_title); ?>" /></td></tr>

	<tr class="text">
	  <td class="pad">Location <span class="redstar">* </span></td>
	  <td><input name="location" type="text" class="inpuTxt" id="location" value="<?php  echo $location; ?>" /></td>
	  </tr>
      
      <tr class="text">
	  <td class="pad"> Experience<span class="redstar">* </span></td>
	  <td><input name="experience" type="text" class="inpuTxt" id="location" value="<?php  echo $experience; ?>" /></td>
	  </tr>
      
            <tr class="text">
	  <td class="pad"> No. of posts<span class="redstar">* </span></td>
	  <td><input name="no_of_posts" type="text" class="inpuTxt" id="no_of_posts" value="<?php  echo $no_of_posts; ?>" /></td>
	  </tr>
      
      
      
	<tr class="text">
	  <td class="pad">From Date <span class="redstar">* </span></td>
	  <td> <input name="date_from" type="text" class="inpuTxt" id="date_from" value="<?php echo $date_from;?>" readonly="true">
<a id="anchor1" onClick="cal2.showCalendar('anchor1'); return false;" href="#" name="anchor1">&nbsp;
<img alt="" src="images/cal.gif" border="0"></a><div id="testdiv2" style="Z-INDEX: 102; VISIBILITY: hidden; POSITION: absolute"></div></td>
	  </tr>
	<tr class="text">
	  <td class="pad">To  Date  <span class="redstar">* </span></td>
	  <td><input name="date_to" type="text" class="inpuTxt" id="date_to" value="<?php echo $date_to ;?>" readonly="true">
<a id="anchor2" onClick="cal1.showCalendar('anchor2'); return false;" href="#" name="anchor2">&nbsp;
<img alt="" src="images/cal.gif" border="0"></a><div id="testdiv1" style="Z-INDEX: 102; VISIBILITY: hidden; POSITION: absolute"></div></td>
	  </tr>
	<tr class="text">
	  <td class="pad" valign="top" nowrap>Job Description </td>
	  <td><?php
					$sBasePath  			= "../../fckeditor/";
					$oFCKeditor 			= new FCKeditor('jobdescription');
					$oFCKeditor->BasePath 	= $sBasePath;
					$oFCKeditor->Value		=  stripslashes($jobdescription); 
					$oFCKeditor->Width  	= '80%' ;
					$oFCKeditor->Height 	= '400' ;
					$oFCKeditor->Create() ;
				?></td>
	 
	  
	  <tr class="text"><td class="pad"> Status</td><td><select name="status" class="inpuTxt" id="status">
	<option value="active"   <?php  if( $status == "active"){ echo "selected='selected'";}?> >Active</option>
	<option value="inactive" <?php  if( $status == "inactive"){ echo "selected='selected'";}?>>Inactive</option>
	</select></td></tr>
	<tr class="text"><td class="pad"></td><td><input name="save"  type="submit" class="inputton" id="save" value="Save"></td>
	</tr>
	<tr class="text"><td class="redstar pad" colspan="2"> * Required Fields </td></tr>  
	</table>
		</td></tr>
</table>
</form>
<script language="JavaScript" type="text/javascript">
 var frmvalidator = new Validator("frx1");
 frmvalidator.addValidation("job_code","req","Please enter Job Code.");
 frmvalidator.addValidation("job_title","req","Please enter Job Title.");
 frmvalidator.addValidation("location","req","Please enter Job Location.");
 frmvalidator.addValidation("date_from","req","Please enter From Date.");
 frmvalidator.addValidation("date_to","req","Please enter To Date.");
</script>