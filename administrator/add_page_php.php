<link href="../css/style.css" rel="stylesheet" type="text/css" />
<?php 
	$data_action = $_REQUEST["action"];
	$pcode		 = $_REQUEST["pcode"];
	if($data_action == 'add_new')
	{
		$data_action='insert';
	}
	if($data_action == 'edit')
	{
		$rs			 	= $s->getData_with_condition('tbl_website_page','page_id',$pcode);
		$row		 	= mysqli_fetch_object($rs);
		$module_id    	= $row->module_id;
		$page_title 	= $row->page_title;
		$page_name		= $row->page_name;
		$display_order  = $row->display_order;
		$page_type		= $row->page_type;
		$manager_page	= $row->manager_page_id;
		$helpText		= $row->help_text;
		$data_action 	= 'update';
	}
?>
<script language="javascript" src="../js/zone_state.js" type="text/javascript"></script>
<form name="frx1" id="frx1" action="index.php?pagename=manage_website_page&action=<?php echo $data_action;?>&pcode=<?php echo $pcode;?>" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="pagecontent">
	<tr>
	  <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="13%" nowrap class="pageheadTop">Website PHP Page Manager</td>
		   <td width="83%" class="headLink">
		   <ul> <li><a href="index.php?pagename=manage_website_page" >Back</a></li> </ul></td>
          <td width="4%" class="headLink" align="right">		  
		  <input type="submit" name="save " id="save" class="inputton" value="Save" />
		  </td>
        </tr>
      </table></td>
    </tr>
	<tr>
	  <td class="pHeadLine"></td>
    </tr>
	<tr>
	  <td>&nbsp;</td>
    </tr>
	
	<tr>
	  <td><table width="100%" border="0" cellpadding="0" cellspacing="0" class="tblBorder">
        <tr>
          <td colspan="2" class="pagehead"> Page Details</td>
        </tr>
		<tr class="text">
		  <td class="pad">Module<span class="redstar"> * </span></td><td>
		<select name="module_id" id="module_id" class="inpuTxt" onChange="htmlData('web_page_module_php.php', 'module='+this.value);">
		<option value="">Please select module</option>
		<option value="1"<?php if($module_id==1){echo "selected='selected'";}?>>Configuration</option>
		<option value="2"<?php if($module_id==2){echo "selected='selected'";}?>>Catalog</option>
		<option value="3"<?php if($module_id==3){echo "selected='selected'";}?>>Modules</option>
		<option value="4"<?php if($module_id==4){echo "selected='selected'";}?>>Customer/Order</option>
		<option value="5"<?php if($module_id==5){echo "selected='selected'";}?>>Locations/Taxs</option>
		<option value="6"<?php if($module_id==6){echo "selected='selected'";}?>>Localization</option>
		<option value="7"<?php if($module_id==7){echo "selected='selected'";}?>>Reports</option>
		<option value="8"<?php if($module_id==8){echo "selected='selected'";}?>>Tools</option>
		<option value="9"<?php if($module_id==9){echo "selected='selected'";}?>>My Account</option>
		</select></td></tr>
        <tr class="text">
          <td class="pad">Page Type <span class="redstar">*</span> </td>
          <td nowrap>
		  <select name="page_type" id="page_type" class="inpuTxt">
		  <option value="" selected> Please Select Page Type</option>
		  <option value="0" <?php if($page_type==0){ echo "selected='selected'";}?>> Manager Page</option>
		  <option value="1" <?php if($page_type==1){ echo "selected='selected'";}?>> manager Sub Page</option>
		  </select></td>
        </tr >
		<tr class="text"><td nowrap class="pad">Manager Page Name</td>
		<td id="txtResult">
		<select name="manager_page" id="manager_page" class="inpuTxt">
		  <option value=""> Please Select Manager Page </option>
		  <option value="0" <?php if( $manager_page == 0) { echo "selected='selected'"; }?> > Manager Page</option>
<?php
			$sql_module = "select * from tbl_website_page where module_id = '$module_id' and page_type = '0'";
			$rs_module  = mysqli_query($GLOBALS["___mysqli_ston"],$sql_module);
			while($row_page = mysqli_fetch_object($rs_module))
			{
?>
				<option value="<?php echo $row_page->page_id;?>" <?php if($row_page->page_id == $manager_page){ echo "selected='selected'";}?>><?php echo $row_page->page_title;?></option>
<?php
			}
?>
		  </select>
		</td>
		</tr>
        <tr class="text">
          <td class="pad">Page Title <span class="redstar">* </span></td>
          <td><input name="page_title" id="page_title" value="<?php echo $page_title;?>" class="inpuTxt">
          </td>
        </tr>
        <tr class="text">
          <td class="pad">Page Name <span class="redstar">*</span> </td>
          <td><input name="page_name" id="page_name" value="<?php echo $page_name;?>" class="inpuTxt"></td>
        </tr>
        <tr class="text">
          <td class="pad">Display Order<span class="redstar"> * </span></td>
          <td><input name="display_order" id="display_order" value="<?php echo $display_order;?>" class="inpuTxt"></td>
        </tr>
		
		 <tr class="text">
          <td class="pad" valign="top">Help Text<br> (applicable with manager pages)</td>
          <td valign="top"><textarea name="helpText" id="helpText" rows="6" cols="50"><?php echo $helpText;?></textarea> [for the new line puropse use '\n']</td>
        </tr>
        <tr class="text">
          <td width="17%" class="pad">&nbsp;</td>
          <td width="83%"><input type="submit" name="save " id="save" class="inputton" value="Save" /> &nbsp; 
			</td>
        </tr>
<tr class="text"><td class="redstar pad" colspan="2"> * Required Fields </td></tr>      
</table></td>
    </tr>

</table>
</form>
<script language="JavaScript" type="text/javascript">
 var frmvalidator = new Validator("frx1");
 frmvalidator.addValidation("module_id","dontselect=0","Please select the module");
 frmvalidator.addValidation("page_title","req","Please enter  page title");
 frmvalidator.addValidation("page_name","req","Please enter page name");
 frmvalidator.addValidation("display_order","req","Please enter  display order");
</script>
