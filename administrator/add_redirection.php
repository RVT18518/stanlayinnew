<?php 
	$data_action = $_REQUEST["action"];
	$pcode		 = $_REQUEST["pcode"];
	
	if($data_action=="edit")
	{
		$rs			= $s->getData_with_condition('tbl_redirection ','id',$pcode);
		$row		= mysqli_fetch_object($rs);
		$old_url	= $row->old_url;
		$new_url	= $row->new_url;
		$status		= $row->status;
		
		$data_action = "update";
	}
	if($data_action == "add_new")
	{
		$data_action = "insert";
	}	
?>

<form name="frx1" id="frx1" action="index.php?pagename=redirection_module&action=<?php echo $data_action;?>&pcode=<?php echo $pcode;?>" method="post" enctype="multipart/form-data">
  <table width="100%"  align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="34%" class="pageheadTop"><?php if($_REQUEST["action"] == "add_new")	{ echo "Add New";} else {echo "Edit";}?>
              Redirection </td>
            <td width="61%" class="headLink"><ul>
                <li><a href="index.php?pagename=redirection_module">Back</a></li>
              </ul></td>
            <td width="5%"><input name="save"  type="submit" class="inputton" id="save" value="Save"></td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td class="pHeadLine"></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><table width="100%" cellpadding="0" cellspacing="0" class="tblBorder">
          <tr>
            <td colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr class="pagehead">
                  <td class="pad"> Redirection Details </td>
                </tr>
              </table></td>
          </tr>
          <tr class="text">
            <td colspan="2"  class="pad">&nbsp;</td>
          </tr>
          <tr class="text">
            <td width="25%"  class="pad">Old URL <span class="redstar"> * </span></td>
            <td ><textarea name="old_url" rows="3" cols="75" class="inpuTxtfreewidth" id="old_url" required="required" ><?php  echo trim($old_url); ?>
</textarea></td>
          </tr>
          <tr class="text">
            <td colspan="2"  class="pad">&nbsp;</td>
          </tr>
          <tr class="text">
            <td   class="pad">New URL <span class="redstar"> * </span></td>
            <td ><textarea name="new_url" type="text" class="pad inpuTxtfreewidth" id="new_url" rows="3" cols="75" required="required"  ><?php  echo trim($new_url); ?>
</textarea></td>
          </tr>
          <tr class="text">
            <td colspan="2" class="pad">&nbsp;</td>
          </tr>
          <tr class="text">
            <td class="pad"> Status</td>
            <td><select name="status" class="inpuTxt" id="status">
                <option value="active"   <?php  if( $status == "active"){ echo "selected='selected'";}?> >Active</option>
                <option value="inactive" <?php  if( $status == "inactive"){ echo "selected='selected'";}?>>Inactive</option>
              </select></td>
          </tr>
          <tr class="text">
            <td class="pad"></td>
            <td>&nbsp;</td>
          </tr>
          <tr class="text">
            <td class="pad"></td>
            <td><input name="save"  type="submit" class="inputton" id="save" value="Save"></td>
          </tr>
          <tr class="text">
            <td class="redstar pad" colspan="2"> * Required Fields </td>
          </tr>
        </table></td>
    </tr>
  </table>
</form>
