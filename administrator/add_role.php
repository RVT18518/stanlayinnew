<?php 
	$data_action = $_REQUEST["action"];
	$pcode		 = $_REQUEST["pcode"];
	if($data_action == 'add_new')
	{
		$data_action='insert';
	}
	if($data_action == 'edit')
	{
		$rs_role_name	= $s->getData_with_condition('tbl_admin_role_type','admin_role_id',$pcode);
		$row_role_name 	= mysqli_fetch_object($rs_role_name);
		$data_action  	= 'update';
	}
?>

<form name="frx1" id="frx1" action="index.php?pagename=role_manager&action=<?php echo $data_action;?>&pcode=<?php echo $pcode;?>" method="post">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr >
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="24%" class="pageheadTop">Role Manager </td>
            <td width="76%" class="headLink"><ul>
                <li><a href="index.php?pagename=role_manager">Back</a></li>
              </ul></td>
            <td><input type="submit" name="add " id="add" class="inputton" value="Save" /></td>
          </tr>
        </table></td>
    </tr>
    <tr >
      <td class="pHeadLine"></td>
    </tr>
    <tr >
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td valign="top"><table width="100%" border="0" cellpadding="5" cellspacing="0" class="tblBorder">
          <tr class="pagehead" >
            <td colspan="2"  >Add New Role</td>
          </tr>
          <tr class="text" align="left">
            <td width="11%" class="pad" >Role Name<span class="redstar">*</span></td>
            <td width="89%" ><input type="text" name="role_name" id="role_name" class="inpuTxt"  value="<?php echo stripslashes($row_role_name->admin_role_name);?>">
              &nbsp; </td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td height="5px" colspan="2"></td>
    </tr>
    <tr>
      <td><table width="100%" border="0" cellpadding="5" cellspacing="0" class="tblBorder" >
          <tr class="pagehead">
            <td colspan="8" >Module/Form Access</td>
          </tr>
          <tr valign="top" class="text">
            <td width="15%"><table width="100%" cellpadding="3" cellspacing="0" class="tblBorder">
                <tr class="head">
                  <td width="12%" nowrap>Configuration</td>
                  <td width="4%">Grant</td>
                </tr>
                <?php 
	$sql_config = "select * from tbl_website_page where module_id ='1' and page_type ='0' and deleteflag='active'";
	$rs_config	= mysqli_query($GLOBALS["___mysqli_ston"],$sql_config);
	if(mysqli_num_rows($rs_config)>0)
	{
		while($row_config = mysqli_fetch_object($rs_config))
		{
?>
                <tr class="text" align="left">
                  <td width="82%" class="pad"><?php echo $row_config->page_title;?></td>
                  <td width="18%"><input name="<?php echo $row_config->page_id;?>" type="checkbox" id="<?php echo $row_config->page_id;?>" value="yes"
	<?php 
		$rs_access    = mysqli_query($GLOBALS["___mysqli_ston"],"select * from tbl_admin_access where role_id='$pcode' and page_id='$row_config->page_id'");
		if(mysqli_num_rows($rs_access)>0)
		{
			echo "checked='checked'";
		}
	?>  /></td>
                </tr>
                <?php			
		}
	}
	else
	{
?>
                <tr class='text'>
                  <td colspan='2' class='redstar'>&nbsp; No record present in database.</td>
                </tr>
                <?php	
	}
?>
              </table></td>
            <td width="15%" valign="top"><table width="100%" cellpadding="3" cellspacing="0" class="tblBorder">
                <tr class="head">
                  <td width="82%" nowrap>Catalog</td>
                  <td width="18%">Grant</td>
                </tr>
                <?php 
	$sql_config = "select * from tbl_website_page where module_id ='2' and page_type ='0' and deleteflag='active'";
	$rs_config	= mysqli_query($GLOBALS["___mysqli_ston"],$sql_config);
	if(mysqli_num_rows($rs_config)>0)
	{
		while($row_config = mysqli_fetch_object($rs_config))
		{
?>
                <tr class="text" align="left">
                  <td width="82%" class="pad"><?php echo $row_config->page_title;?></td>
                  <td width="18%"><input name="<?php echo $row_config->page_id;?>" type="checkbox" id="<?php echo $row_config->page_id;?>" value="yes"  
	<?php 
		$rs_access    = mysqli_query($GLOBALS["___mysqli_ston"],"select * from tbl_admin_access where role_id='$pcode' and page_id='$row_config->page_id'");
		if(mysqli_num_rows($rs_access)>0)
		{
			echo "checked='checked'";
		}
	?>
	/></td>
                </tr>
                <?php			
		}
	}
	else
	{
?>
                <tr class='text'>
                  <td colspan='2' class='redstar'>&nbsp; No record present in database.</td>
                </tr>
                <?php	
	}
?>
              </table></td>
            <td width="15%" ><table width="100%" cellpadding="3" cellspacing="0" class="tblBorder">
                <tr class="head">
                  <td width="12%" nowrap>Modules</td>
                  <td width="4%">Grant</td>
                </tr>
                <?php 
	$sql_config = "select * from tbl_website_page where module_id ='3' and page_type ='0' and deleteflag='active'";
	$rs_config	= mysqli_query($GLOBALS["___mysqli_ston"],$sql_config);
	if(mysqli_num_rows($rs_config)>0)
	{
		while($row_config = mysqli_fetch_object($rs_config))
		{
?>
                <tr class="text" align="left">
                  <td width="82%" class="pad"><?php echo $row_config->page_title;?></td>
                  <td width="18%"><input name="<?php echo $row_config->page_id;?>" type="checkbox" id="<?php echo $row_config->page_id;?>" value="yes"  
	<?php 
		$rs_access    = mysqli_query($GLOBALS["___mysqli_ston"],"select * from tbl_admin_access where role_id='$pcode' and page_id='$row_config->page_id'");
		if(mysqli_num_rows($rs_access)>0)
		{
			echo "checked='checked'";
		}
	?>
	/></td>
                </tr>
                <?php			
		}
	}
	else
	{
?>
                <tr class='text'>
                  <td colspan='2' class='redstar'>&nbsp; No record present in database.</td>
                </tr>
                <?php	
	}
?>
              </table></td>
            <td width="15%" ><table width="100%" cellpadding="3" cellspacing="0" class="tblBorder">
                <tr class="head">
                  <td width="12%" nowrap>Customer/Order</td>
                  <td width="4%">Grant</td>
                </tr>
                <?php 
	$sql_config = "select * from tbl_website_page where module_id ='4' and page_type ='0' and deleteflag='active'";
	$rs_config	= mysqli_query($GLOBALS["___mysqli_ston"],$sql_config);
	if(mysqli_num_rows($rs_config)>0)
	{
		while($row_config = mysqli_fetch_object($rs_config))
		{
?>
                <tr class="text" align="left">
                  <td width="82%" class="pad"><?php echo $row_config->page_title;?></td>
                  <td width="18%"><input name="<?php echo $row_config->page_id;?>" type="checkbox" id="<?php echo $row_config->page_id;?>" value="yes" 
	<?php 
		$rs_access    = mysqli_query($GLOBALS["___mysqli_ston"],"select * from tbl_admin_access where role_id='$pcode' and page_id='$row_config->page_id'");
		if(mysqli_num_rows($rs_access)>0)
		{
			echo "checked='checked'";
		}
	?>
	 /></td>
                </tr>
                <?php			
		}
	}
	else
	{
?>
                <tr class='text'>
                  <td colspan='2' class='redstar'>&nbsp; No record present in database.</td>
                </tr>
                <?php	
	}
?>
              </table></td>
            <td width="14%"><table width="100%" cellpadding="3" cellspacing="0" class="tblBorder">
                <tr class="head">
                  <td width="12%" nowrap>Locations/Taxes </td>
                  <td width="4%">Grant</td>
                </tr>
                <?php 
	$sql_config = "select * from tbl_website_page where module_id ='5' and page_type ='0' and deleteflag='active'";
	$rs_config	= mysqli_query($GLOBALS["___mysqli_ston"],$sql_config);
	if(mysqli_num_rows($rs_config)>0)
	{
		while($row_config = mysqli_fetch_object($rs_config))
		{
?>
                <tr class="text" align="left">
                  <td width="82%" class="pad"><?php echo $row_config->page_title;?></td>
                  <td width="18%"><input name="<?php echo $row_config->page_id;?>" type="checkbox" id="<?php echo $row_config->page_id;?>" value="yes"  
	<?php 
		$rs_access    = mysqli_query($GLOBALS["___mysqli_ston"],"select * from tbl_admin_access where role_id='$pcode' and page_id='$row_config->page_id'");
		if(mysqli_num_rows($rs_access)>0)
		{
			echo "checked='checked'";
		}
	?>
	/></td>
                </tr>
                <?php 			
		}
	}
	else
	{
?>
                <tr class='text'>
                  <td colspan='2' class='redstar'>&nbsp; No record present in database.</td>
                </tr>
                <?php	
	}
?>
              </table></td>
            <?php	/*
	<td width="11%"><table width="100%" cellpadding="3" cellspacing="0" class="tblBorder">
	<tr class="head"><td width="12%" nowrap>Localization </td>
	<td width="4%">Grant</td>
	</tr>
	<?php 
	$sql_config = "select * from tbl_website_page where module_id ='6' and page_type ='0'";
	$rs_config	= mysqli_query($GLOBALS["___mysqli_ston"],$sql_config);
	if(mysqli_num_rows($rs_config)>0)
	{
		while($row_config = mysqli_fetch_object($rs_config))
		{
?>
<tr class="text" align="left"><td width="82%" class="pad"><?php echo $row_config->page_title;?></td><td width="18%">
	<input name="<?php echo $row_config->page_id;?>" type="checkbox" id="<?php echo $row_config->page_id;?>" value="yes"  
	<?php 
		$rs_access    = mysqli_query($GLOBALS["___mysqli_ston"],"select * from tbl_admin_access where role_id='$pcode' and page_id='$row_config->page_id'");
		if(mysqli_num_rows($rs_access)>0)
		{
			echo "checked='checked'";
		}
	?>
	/></td>
	</tr>
<?php			
		}
	}
	else
	{
?>
<tr class='text'><td colspan='2' class='redstar'> &nbsp; No record present in database.</td></tr> 
<?php	
	}
?>
	</table>			 </td>
	*/ ?>
            <td width="13%"><table width="100%" cellpadding="3" cellspacing="0" class="tblBorder">
                <tr class="head">
                  <td width="12%" nowrap>Reports</td>
                  <td width="4%">Grant</td>
                </tr>
                <?php 
	$sql_config = "select * from tbl_website_page where module_id ='7' and page_type ='0' and deleteflag='active'";
	$rs_config	= mysqli_query($GLOBALS["___mysqli_ston"],$sql_config);
	if(mysqli_num_rows($rs_config)>0)
	{
		while($row_config = mysqli_fetch_object($rs_config))
		{
?>
                <tr class="text" align="left">
                  <td width="82%" class="pad"><?php echo $row_config->page_title;?></td>
                  <td width="18%"><input name="<?php echo $row_config->page_id;?>" type="checkbox" id="<?php echo $row_config->page_id;?>" value="yes" 
	<?php 
		$rs_access    = mysqli_query($GLOBALS["___mysqli_ston"],"select * from tbl_admin_access where role_id='$pcode' and page_id='$row_config->page_id'");
		if(mysqli_num_rows($rs_access)>0)
		{
			echo "checked='checked'";
		}
	?>
	 /></td>
                </tr>
                <?php			
		}
	}
	else
	{
?>
                <tr class='text'>
                  <td colspan='2' class='redstar'>&nbsp; No record present in database.</td>
                </tr>
                <?php	
	}
?>
              </table></td>
            <td width="15%"><table width="100%" cellpadding="3" cellspacing="0" class="tblBorder">
                <tr class="head">
                  <td width="12%" nowrap>Tools</td>
                  <td width="4%">Grant</td>
                </tr>
                <?php 
	$sql_config = "select * from tbl_website_page where module_id ='8' and page_type ='0' and deleteflag='active'";
	$rs_config	= mysqli_query($GLOBALS["___mysqli_ston"],$sql_config);	if(mysqli_num_rows($rs_config)>0)
	{
		while($row_config = mysqli_fetch_object($rs_config))
		{
?>
                <tr class="text" align="left">
                  <td width="82%" class="pad"><?php echo $row_config->page_title;?></td>
                  <td width="18%"><input name="<?php echo $row_config->page_id;?>" type="checkbox" id="<?php echo $row_config->page_id;?>" value="yes"   
	<?php 
		$rs_access    = mysqli_query($GLOBALS["___mysqli_ston"],"select * from tbl_admin_access where role_id='$pcode' and page_id='$row_config->page_id'");
		if(mysqli_num_rows($rs_access)>0)
		{
			echo "checked='checked'";
		}
	?>
	/></td>
                </tr>
                <?php			
		}
	}
	else
	{
?>
                <tr class='text'>
                  <td colspan='2' class='redstar'>&nbsp; No record present in database.</td>
                </tr>
                <?php	
	}
?>
              </table></td>
          </tr>
          <tr class="text" align="left">
            <td colspan="8" valign="top" ><table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr class="text" align="left">
                  <td class="pad" >Grant to all
                    <input name="all" type="checkbox" id="all" value="yes" 
		  <?php 
			$rs_access    = mysqli_query($GLOBALS["___mysqli_ston"],"select * from tbl_admin_access where role_id='$pcode' and page_id='0'");
			if(mysqli_num_rows($rs_access)>0)
			{
				echo "checked='checked'";
			}
		?>
		  ></td>
                </tr>
                <tr class="text" align="left">
                  <td class="pad"><input type="submit" name="add " id="add" class="inputton" value="Save" /></td>
                </tr>
                <tr class="text">
                  <td class="redstar pad" colspan="2"> * Required Fields </td>
                </tr>
              </table></td>
          </tr>
        </table></td>
    </tr>
  </table>
</form>
<script language="JavaScript" type="text/javascript">
 var frmvalidator = new Validator("frx1");
 frmvalidator.addValidation("role_name","req","Please enter  Role Name");
</script>