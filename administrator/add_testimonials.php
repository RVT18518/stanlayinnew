<?php 
	$data_action=$_REQUEST['action'];
	$pcode=$_REQUEST["pcode"];
	if($data_action=="delete_image")
	{
		$rs_del    = $s->getData_with_condition('tbl_testimonials','testimonials_id',$pcode);
		$row_del   = mysqli_fetch_object($rs_del);
		$del_image_large  = $row_del->testimonials_logo_large;
		$del_image_medium = $row_del->testimonials_logo_medium;
		$del_image_small  = $row_del->testimonials_logo_small;
		if(file_exists($del_image_large)>0)
		{
			unlink($del_image_large);
		}
		if(file_exists($del_image_medium)>0)
		{
			unlink($del_image_medium);
		}
		if(file_exists($del_image_small)>0)
		{
			unlink($del_image_small);
		}

		$sql_remove="update tbl_testimonials SET testimonials_logo_large = '' , testimonials_logo_medium = '', testimonials_logo_small = ''  where testimonials_id = $pcode";
		mysqli_query($GLOBALS["___mysqli_ston"],$sql_remove);
		$data_action="edit";
	}
	if($data_action=="edit")
	{
		$rs=$s->getData_with_condition('tbl_testimonials','testimonials_id',$pcode);
		$row=mysqli_fetch_object($rs);
		$testimonials_name        = stripslashes($row->testimonials_name);
		$testimonials_status      = $row->testimonials_status;
		$testimonials_description = stripslashes($row->testimonials_description);
		$testimonials_logo        = $row->testimonials_logo_small;
		$tax_class_id		       = $row->tax_class_id;
		$est_arrival_min		   = $row->est_arrival_min;
		$est_arrival_max		   = $row->est_arrival_max;	
		
		$data_action = "update";
	}
	if($data_action == "add_new")
	{
		$data_action = "insert";
	}
	
	
////////////////// 09-01-2012 /////////////////////////////////////////////////////
/*echo	strtoupper($testimonials_name);

echo "TEST <br>".$sql="SELECT * from tbl_testimonials where deleteflag='active' and status='active'";
$rs=mysqli_query($GLOBALS["___mysqli_ston"],$sql);
$num_row=mysqli_num_rows($rs);

while(mysqli_fetch_array($row));
{
	
	}*/
////////////////////////////////////////////////////////////////////

?>

<form name="frx1" id="frx1" action="index.php?pagename=manage_testimonials&action=<?php echo $data_action;?>&pcode=<?php echo $pcode;?>" method="post" enctype="multipart/form-data">
  <table width="100%" align="center" cellpadding="0" cellspacing="0" class="pagecontent" >
    <tr>
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="38%" class="pageheadTop">Testimonials Manager</td>
            <td width="52%" class="headLink"><ul>
                <li><a href="index.php?pagename=manage_testimonials">Back</a></li>
              </ul></td>
            <td width="10%" align="right"><input name="save"  type="submit" class="inputton" id="save" value="Save"></td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td class="pHeadLine"></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><table width="100%" cellpadding="5" cellspacing="0" class="tblBorder">
          <tr>
            <td class="pagehead" colspan="2"> Testimonials</td>
          </tr>
          <tr>
            <td colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              </table></td>
          </tr>
          
          <tr class="text">
            <td width="27%" class="pad"> Name <span class="redstar">*</span></td>
            <td width="84%"><input name="testimonials_name" type="text" class="inpuTxt" id="testimonials_name" value="<?php echo $testimonials_name;?>"></td>
          </tr>
          <tr class="text">
	  <td class="pad">Image<br /><span class="redstar"><strong>W:843px*H:446px</strong></span></td>
	  <td><input name="testimonials_logo" type="file"  id="testimonials_logo" value=""></td>
	</tr>
          <tr class="text">
            <td valign="top" class="pad"> Description</td>
            <td><textarea name="testimonials_description"  rows="4" class="inpuTxt" id="testimonials_description" ><?php echo $testimonials_description;?></textarea></td>
          </tr>
        <?php
	  if(trim($testimonials_logo)!=""){
	  ?>
          <tr class="text">
            <td class="pad"><img src="../<?php echo $testimonials_logo ;?>" onerror="style.display='none';" /></td>
            <td><a href="index.php?pagename=add_manu&action=delete_image&pcode=<?php echo $pcode;?>">Remove Image</a></td>
            <?php
	  }
	  ?>
          </tr>
          <tr class="text">
            <td width="27%" class="pad">&nbsp;</td>
            <td width="84%">&nbsp;&nbsp;&nbsp;</td>
          </tr>
          <tr class="text">
            <td class="pad"> Status</td>
            <td><select name="testimonials_status" class="inpuTxt" id="testimonials_status">
                <option value="active" <?php if( $testimonials_status =="active"){ echo "selected='selected'";}?>>Active</option>
                <option value="inactive" <?php if($testimonials_status =="inactive"){ echo "selected='selected'";}?>>Inactive</option>
              </select>
              </select></td>
          </tr>
          <tr class="text">
            <td class="pad"></td>
            <td><input name="save"  type="submit" class="inputton" id="save" value="Save"></td>
          </tr>
          <tr class="text">
            <td class="redstar pad" colspan="2"> * Required Fields </td>
          </tr>
        </table></td>
    </tr>
  </table>
</form>
<script language="JavaScript" type="text/javascript">
 var frmvalidator = new Validator("frx1");
 frmvalidator.addValidation("testimonials_name","req","Please enter Testimonials  Name");
</script>