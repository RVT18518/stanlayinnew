<?php
	$data_action = $_REQUEST["action"];
	$pcode 		 = $_REQUEST["pcode"];
	$pcode_emp	 = $_REQUEST["pcode_emp"];


	if($_REQUEST["action"] == "update" || $_REQUEST["action"] == "insert")
	{
		$dataArray["C_Name"]			= addslashes($_REQUEST["C_Name"]);
		$dataArray["AddressName"]		= addslashes($_REQUEST["AddressName"]);
		$dataArray["Number"]			= $_REQUEST["Number"];
		$dataArray["Email"]				= $_REQUEST["Email"];
		$dataArray["Fax"]				= $_REQUEST["Fax"];
		$dataArray["Contact_1"]			= $_REQUEST["Contact_1"];
		$dataArray["Contact_2"]			= $_REQUEST["Contact_2"];
		$dataArray["Country"]			= $_REQUEST["Country"];
		$dataArray["Currency"]			= $_REQUEST["Currency"];
		$dataArray["Price_Basis"]		= $_REQUEST["Price_Basis"];
		$dataArray["Payment_Terms"]		= $_REQUEST["Payment_Terms"];
		$dataArray["status"]			= $_REQUEST["status"];
	}



	if($_REQUEST["action"] == "insert")
	{
		
		if($_REQUEST["status"]!="" && $_REQUEST["status"]!='0')
		{
			$result = $s->insertRecord('vendor_master',$dataArray);
			
			$s->pageLocation("index.php?pagename=vendor_manager&action=$data_action&result=$result");
		}
		
		$error = 1;
		$data_action = 'add_new';
	
	}
	if($_REQUEST["action"] == "update")
	{
		if($_REQUEST["status"]!="" && $_REQUEST["status"]!='0')
		{
			$result = $s->editRecord('vendor_master',$dataArray,'ID',$pcode);
			$s->pageLocation("index.php?pagename=vendor_manager&action=$data_action&result=$result");
		}
		$error = 1;
		$data_action = 'edit';
	}
	if($data_action=="edit")
	{
		$rs		  		= $s->getData_with_condition('vendor_master','ID',$pcode);
		$row	  		= mysqli_fetch_object($rs);
		$C_Name 		= $row->C_Name;
		$AddressName 	= stripslashes($row->AddressName);
		$Number    		= $row->Number;
		$Email  		= $row->Email;
		$Fax  			= $row->Fax;
		$Contact_1		= $row->Contact_1;
		$Contact_2		= $row->Contact_2;
		$Country		= $row->Country;
		$Currency		= $row->Currency;
		$Price_Basis	= $row->Price_Basis;
		$Payment_Terms	= $row->Payment_Terms;
		$status			= $row->status;
		
		
		$data_action 	= "update";
	}
	if($data_action == "add_new")
	{
		$data_action 	= "insert";
	}	
?>
<script language="javascript" src="../js/zone_state.js" type="text/javascript"></script>

<form name="frx1" id="frx1" action="index.php?pagename=add_vendor&action=<?php echo $data_action;?>&pcode=<?php echo $pcode;?>" method="post">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="5">
          <tr>
            <td width="46%" class="pageheadTop">Vendor Manager</td>
            <td width="47%" class="headLink" align="right"><ul>
                <li><a href="index.php?pagename=vendor_manager" >Back</a></li>
              </ul></td>
            <td width="7%" align="right"><input type="submit" name="add" id="add" class="inputton" value="Save"  /></td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td class="pHeadLine"></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><table width="100%" border="0" cellpadding="5" cellspacing="0" class="tblBorder">
          <tr>
            <td colspan="5" class="pagehead">Vendor Details</td>
          </tr>
          <tr class="text">
            <td align="left" valign="top"  class="pad">Company Name<span class="redstar"> *</span></td>
            <td align="left" valign="top"  ><input name="C_Name" id="C_Name" type="text" class="inpuTxt"  value="<?php echo $C_Name;?>"></td>
            <td valign="top" >&nbsp;</td>
              <td width="20%" valign="top" class="pad">Telephone No. <span class="redstar"> </span></td>
            <td width="25%" valign="top"><input type="text" name="Number" id="Number" class="inpuTxt"  value="<?php echo $Number;?>"></td>
           
          </tr>
        
          <tr class="text">
            <td valign="top" class="pad" rowspan="4">Address</td>
            <td valign="top" rowspan="4"><textarea name="AddressName" id="AddressName" class="inpuTxt" rows="6"><?php echo $AddressName;?></textarea></td>
            <td valign="top">&nbsp;</td>
           <td width="20%" align="left" valign="top" class="pad">Email</td>
            <td width="25%" align="left" valign="top"><input type="text" name="Email" id="Email" class="inpuTxt"  value="<?php echo $Email;?>" /></td>
          </tr>
          <tr class="text">
           <td width="5%" valign="top">&nbsp;</td>
            <td width="20%" valign="top" class="pad">Fax No. </td>
            <td width="25%" valign="top"><input type="text" name="Fax" id="Fax" class="inpuTxt"  value="<?php echo $Fax;?>"></td>
           
          </tr>
          <tr class="text">
           <td width="5%" valign="top">&nbsp;</td>
            <td width="20%" valign="top" class="pad">Contact Person 1</td>
            <td width="25%" valign="top"><input type="text" name="Contact_1" id="Contact_1" class="inpuTxt"  value="<?php echo $Contact_1;?>"></td>
           
          </tr>
           <tr class="text">
           <td width="5%" valign="top">&nbsp;</td>
            <td width="20%" valign="top" class="pad">Contact Person 2</td>
            <td width="25%" valign="top"><input type="text" name="Contact_2" id="Contact_2" class="inpuTxt"  value="<?php echo $Contact_2;?>"></td>
           
          </tr>
          
          <tr class="text">
            <td width="20%" valign="top" class="pad">Country</td>
            <td width="25%" valign="top"><select name="Country" id="Country" class="inpuTxt">
              <option value="">Select Country</option>
              <?php
	$sql_country="select * from tbl_country order by country_name";
	$rs_co=mysqli_query($GLOBALS["___mysqli_ston"],$sql_country);
	if(mysqli_num_rows($rs_co)>0)
	{
		while($row_country=mysqli_fetch_object($rs_co))
		{	
?>
              <option value="<?php echo $row_country->country_name;?>" <?php if($Country == $row_country->country_name){ echo "selected='selected'";}?>><?php echo ucfirst($row_country->country_name);?></option>
              <?php
		}
	}
?>
            </select></td>
            <td width="5%" valign="top">&nbsp;</td>
           <td width="20%" valign="top" class="pad">Currency</td>
            <td width="25%" valign="top"><select name="Currency" id="Currency" class="inpuTxt">
              <option value="">Select Currency</option>
              <option value="Euro" <?php if($Currency == "Euro"){ echo "selected='selected'";}?>>Euro</option>
              <option value="USD" <?php if($Currency == "USD"){ echo "selected='selected'";}?>>USD</option>
              <option value="GBP" <?php if($Currency == "GBP"){ echo "selected='selected'";}?>>GBP</option>
              <option value="CHF" <?php if($Currency == "CHF"){ echo "selected='selected'";}?>>CHF</option>
              <option value="INR" <?php if($Currency == "INR"){ echo "selected='selected'";}?>>INR</option>
            </select></td>
          </tr>
          
          
          
           <tr class="text">
            <td width="20%" valign="top" class="pad">Price Basis</td>
            <td width="25%" valign="top"><select name="Price_Basis" id="Price_Basis" class="inpuTxt">
              <option value="">Select Price Basis</option>
              <option value="Ex Works" <?php if($Price_Basis == "Ex Works"){ echo "selected='selected'";}?>>Ex Works</option>
              <option value="FOB" <?php if($Price_Basis == "FOB"){ echo "selected='selected'";}?>>FOB</option>
              <option value="FCA" <?php if($Price_Basis == "FCA"){ echo "selected='selected'";}?>>FCA</option>
              <option value="CIP" <?php if($Price_Basis == "CIP"){ echo "selected='selected'";}?>>CIP</option>
            </select></td>
            <td width="5%" valign="top">&nbsp;</td>
           <td width="20%" valign="top" class="pad">Payment Terms</td>
            <td width="25%" valign="top"><select name="Payment_Terms" id="Payment_Terms" class="inpuTxt">
              <option value="">Select Payment Terms</option>
              <option value="10% Advance & Balance Net 30" <?php if($Payment_Terms == "10% Advance & Balance Net 30"){ echo "selected='selected'";}?>>10% Advance & Balance Net 30</option>
              <option value="25% Advance & Balance Net 30" <?php if($Payment_Terms == "25% Advance & Balance Net 30"){ echo "selected='selected'";}?>>25% Advance & Balance Net 30</option>
              <option value="50% Advance & Balance Net 30" <?php if($Payment_Terms == "50% Advance & Balance Net 30"){ echo "selected='selected'";}?>>50% Advance & Balance Net 30</option>
              <option value="Net 30 Days" <?php if($Payment_Terms == "Net 30 Days"){ echo "selected='selected'";}?>>Net 30 Days</option>
              <option value="Net 60 Days" <?php if($Payment_Terms == "Net 30 Days"){ echo "selected='selected'";}?>>Net 60 Days</option>
              <option value="Net 90 Days" <?php if($Payment_Terms == "Net 90 Days"){ echo "selected='selected'";}?>>Net 90 Days</option>
            </select></td>
          </tr>
          
          
          
          
          <tr class="text">
            <td width="20%" valign="top" class="pad">Status</td>
            <td width="25%" valign="top"><select name="status" id="status" class="inpuTxt">
                <option value="active" <?php if($status == "active"){ echo "selected='selected'";}?>>Active</option>
                <option value="inactive" <?php if($status == "inactive"){ echo "selected='selected'";}?>>Inactive</option>
              </select></td>
            <td width="5%" valign="top">&nbsp;</td>
            <td width="20%" align="left" valign="top">&nbsp;</td>
            <td width="25%" valign="top">&nbsp;</td>
          </tr>
          <?php if($_REQUEST["action"] != 'edit')
	{?>
          <tr class="text">
            <td class="redstar pad" > * Required Fields </td>
            <td valign="top"><input name='add2' id='add2' type='submit' class='inputton' value='Save' /></td>
            <td valign="top">&nbsp;</td>
            <td align="left" valign="top">&nbsp;</td>
            <td valign="top">&nbsp;</td>
          </tr>
          <?php } //php role table mapp here ?>
        </table></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
   
	</table>
</form>
<script language="javascript" type="text/javascript">
 var frmvalidator = new Validator("frx1");
 //frmvalidator.addValidation("fname","req","Please enter First Name.");
// frmvalidator.addValidation("tele_no","req","Please enter Telephone No.");
 //frmvalidator.addValidation("address","req","Please enter address.");
 //frmvalidator.addValidation("city","req","Please enter city.");
 //frmvalidator.addValidation("country","dontselect=0","Please select country.");
// frmvalidator.addValidation("state","req","Please enter state.");
 //frmvalidator.addValidation("zip","req","Please enter zip/postal code.");
 //frmvalidator.addValidation("cust_segment","dontselect=0","Please select Customer Segment.");
 //frmvalidator.addValidation("acc_manager","dontselect=0","Please select Account Manager.");

</script>