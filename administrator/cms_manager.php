<?php
/*echo "<pre>";
print_r($_REQUEST);
*/
$addmore_links=@implode(",",array_unique($_REQUEST["addmore"]));
//exit;

	$data_action	= $_REQUEST['action'];
	$pcode			= $_REQUEST["pcode"];
	$order 			= $_REQUEST["order"];
	$order_by		= $_REQUEST["orderby"]; 
	if(strlen(trim($order))<=0)
	{
		$order		= "asc";
	}
	if(strlen(trim($order_by))<=0)
	{
		$order_by	= "pages_name";
	}

	$result			= $_REQUEST["result"]; 
	$max_results	= 10;
	if(!isset($_GET['pageno']))
	{ 
		$page		= 1; 
	}
	else 
	{ 
		$page 		= $_GET['pageno']; 
	} 
	if(!isset($_GET['records']))
	{
    	$max_results = 20; 
	}
	else 
	{
    	$max_results = $_GET['records']; 
	}
	$from = (($page * $max_results) - $max_results);  
	
	if($_REQUEST['action']=='update' || $_REQUEST['action']=='insert')
	{
		$file_array["pages_name"]  		= addslashes($_REQUEST["page_name"]);
		$file_array["page_title"]  		= addslashes($_REQUEST["page_title"]);
		$file_array["addmore_links"]  	= ($addmore_links);
				
		if(strlen(trim($_REQUEST["permalink"]))>0 )
		{
			$file_array["permalink"]= addslashes(str_replace(" ","-",str_replace("'","",str_replace("/","_",strtolower($_REQUEST["permalink"])))));
			//$file_array["permalink"]= addslashes(str_replace("'","",str_replace("/","_",$_REQUEST["permalink"])));
		}
		else
		{
			$file_array["permalink"]= addslashes(str_replace(" ","-",str_replace("'","",str_replace("/","_",strtolower("'","",$_REQUEST["page_name"])))));
		}		
		
		$file_array["menu_id"]   		= "0";//$_REQUEST["InMenu"];
		$file_array["sort_desc"]  		= addslashes($_REQUEST["sort_desc"]);
		$file_array["pages_html_text"]  = trim(addslashes($_REQUEST["pages_html_text"]));
	
		if($_FILES["image_path"]["name"] != "")
		{
			$bannerPath = $s->fileUpload("uploads/cms_images/", "image_path", "CMSBANNER_");
			//$bannerPath = $s->ImageUpload("uploads/cms_images/", "image_path", "CMSBANNER_", "1387", "438");//commneted on 16-11-2019 to free height width of banner image by Rumit
			if($bannerPath != -1)
			{
				$file_array["image_path"] = $bannerPath;
			}
		}
		
		if($_FILES["logo_path"]["name"] != "")
		{
			$imagePath = $s->ImageUpload("uploads/cms_logo/", "logo_path","Logo_Small_", "92","92");
			if($imagePath != -1)
			{
				$file_array["logo_path"] = $imagePath;
			}
		}
		$file_array["lock_status"]  	= $_REQUEST["locked"];
		$file_array["page_status"] 		= $_REQUEST["page_status"];
		//$file_array["menu_view"] 		= $_REQUEST["menu_view"];
		if($_REQUEST["menu_header"]!='')
		{
			$file_array["menu_header"] = $_REQUEST["menu_header"];
		}
		else
		{
			$file_array["menu_header"]="no";
		}
		if($_REQUEST["menu_footer"]!='')
		{
			$file_array["menu_footer"] = $_REQUEST["menu_footer"];
		} 
		else
		{
			$file_array["menu_footer"]="no";
		}
		//============================panel================================//
		if($_REQUEST["left_panel"]!='')
		{
			$file_array["left_panel"] = $_REQUEST["left_panel"];
		}
		else
		{
			$file_array["left_panel"]="no";
		}
		if($_REQUEST["right_panel"]!='')
		{
			$file_array["right_panel"] = $_REQUEST["right_panel"];
		} 
		else
		{
			$file_array["right_panel"]="no";
		}
		//========================panel=================================//
				
		$file_array["meta_content"] 	= addslashes($_REQUEST["meta_content"]);
		$file_array["meta_desc"] 		= addslashes($_REQUEST["meta_desc"]);
		$file_array["quick_link"] 		= "no";//$_REQUEST["quick_link"];
		$file_array["sort_order"] 		= "0";//$_REQUEST["sort_order"];
	}
	if($_REQUEST['action']=='ChangeStatus')
	{
		$rs_status = $s->getData_with_condition('tbl_cms_pages','pages_id',$pcode);
		if(mysqli_num_rows($rs_status)>0)
		{
			$row_status = mysqli_fetch_object($rs_status);
			if($row_status->page_status == 'active')
			{
				$fileArray["page_status"] = 'inactive';
			}
			else if($row_status->page_status == 'inactive')
			{
				$fileArray["page_status"] = 'active';
			}
			$result      = $s->editRecord('tbl_cms_pages',$fileArray,'pages_id',$pcode);
			$s->pageLocation("index.php?pagename=cms_manager&action=ChangeStatusDone&result=$result"); 
		}
	}
	if($_REQUEST['action']=='update')
	{
		$result = $s->editRecord('tbl_cms_pages',$file_array,'pages_id',$pcode);
		$data_action = "updateDone";
		$s->pageLocation("index.php?pagename=cms_manager&action=$data_action&pcode=$pcode&result=$result"); 
	}
	if($_REQUEST['action']=='insert')
	{
		$result = $s->insertRecord('tbl_cms_pages',$file_array);
		$data_action = "insertDone";
		$s->pageLocation("index.php?pagename=cms_manager&action=$data_action&pcode=$pcode&result=$result"); 
	}
	function SubCMSPageRecords($parentID, $space)
	{
		$space = $space . "&nbsp;&nbsp;";
		$sql = "select * from tbl_cms_pages where deleteflag = 'active' and menu_id = $parentID order by pages_name asc";
		$rs  = mysqli_query($GLOBALS["___mysqli_ston"],$sql);
		if(mysqli_num_rows($rs)>0)
		{
			while($row = mysqli_fetch_object($rs))
			{
?>
<tr class="text" onmouseover="bgr_color(this, '#EAB9BA')" onMouseOut="bgr_color(this, '')">
  <td align="center" valign="middle"><?php echo $row->pages_id; ?></Td>
  <td class="pad" valign="middle" ><?php echo $space."&raquo; ".ucfirst(stripslashes($row->pages_name)) ; ?></td>
  <td class="pad" valign="middle"><?php echo ucfirst(stripslashes($row->page_title)) ; ?></td>
  <td align="center" valign="middle"><?php 
	if($row->page_status =="active")
	{
?>
    <img src="images/green.gif" title="Active" border="0"  /> &nbsp; &nbsp; <a href="index.php?pagename=cms_manager&action=ChangeStatus&pcode=<?php echo $row->pages_id;?>&pageno=<?php echo $page;?>&records=<?php echo $max_results;?>"><img src="images/red_light.gif" title="Inactive" border="0"  /></a>
    <?php
	}
	else if($row->page_status =="inactive")
	{
?>
    <a href="index.php?pagename=cms_manager&action=ChangeStatus&pcode=<?php echo $row->pages_id;?>&pageno=<?php echo $page;?>&records=<?php echo $max_results;?>"><img src="images/green_light.gif" title="Active" border="0"  /></a> &nbsp; &nbsp; <img src="images/red.gif" title="Inactive" border="0"  />
    <?php		
	}
?></td>
  <td align="center" valign="middle"><div align="center"><a href="index.php?pagename=add_cms&action=edit&pcode=<?php echo $row->pages_id;?>"> <img src="images/e.gif" border="0"  alt="Edit"/></a> &nbsp; &nbsp;
      <?php	if($row->lock_status == "inactive")
		{
?>
      <a href="index.php?pagename=cms_manager&action=delete&pcode=<?php echo $row->pages_id;?>" onclick='return del();'> <img src="images/x.gif" border="0" alt="Delete" /></a>
      <?php 
		}
		else
		{
?>
      <img src="images/protect.gif" border="0" alt="Locked Data" />
      <?php		
		}
?>
    </div></td>
</tr>
<?php		
				SubCMSPageRecords($row->pages_id, $space);		
			}
		}
	}
?>
<script type="text/javascript">
function OnSelect() 
{
	window.location = document.frx1.records.value;
}
function OnSelectPages()
{
	window.location = document.frx1.pages_select.value;
}
</script>
<form name="frx1" id="frx1" action="#" method="post">
  <table width="100%" align="center" cellpadding="0" cellspacing="0" class="pagecontent">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="38%" class="pageheadTop">CMS Manager</td>
            <td width="62%" class="headLink"><ul>
                <li><a href="index.php?pagename=add_cms&action=add_new">Add New Page</a></li>
              </ul></td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td class="pHeadLine"></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><?php 
		if($_REQUEST['action']=='ChangeStatusDone')
		{
			if($result==0)
			{
				echo "<p class='success'>Status Change Successfully</p><br />";	
			}
			else if($result==1)
			{
				echo "<p class='error'>Status Changing Fails</p><br />";	
			}
		}	
		if($data_action=='delete')
		{
			$sql_delete	= "delete from tbl_cms_pages where pages_id = $pcode";
			$result		= mysqli_query($GLOBALS["___mysqli_ston"],$sql_delete);
			//$result = $s->delete_table_withCondition('tbl_cms_pages','pages_id',$pcode);	
			if($result)
			{
				echo "<p class='success'>".record_delete."</p><br />";	
			}
			else 
			{
				echo "<p class='error'>".record_not_delete."</p><br />";	
			}
		}
		if($_REQUEST['action']=='updateDone')
		{
			 if($result==0)
			{
				echo "<p class='success'>".record_update."</p><br />";	
			}
			else if($result==1)
			{
				echo "<p class='error'>".record_not_update."</p><br />";	
			}
		}
		else if($_REQUEST['action']=='insertDone')
		{
			 if($result==0)
			{
				echo "<p class='success'>".record_added."</p><br />";	
			}
			else if($result==1)
			{
				echo "<p class='error'>".record_not_added."</p><br />";	
			}
			
		}
	?></td>
    </tr>
    <tr>
      <td width="100%"><table width="100%" cellpadding="0" cellspacing="0" class="tblBorder">
          <tr class="pagehead">
            <td colspan="4" class="pad" nowrap="nowrap" > CMS Page Details </td>
            <td align="center" nowrap="nowrap">Records View  &nbsp;
              <select name="records" onchange="OnSelect();"  >
                <option <?php if($max_results==20){ echo "selected='selected'";} ?>
value="index.php?pagename=cms_manager&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=20">20</option>
                <option <?php if($max_results==50){ echo "selected='selected'";} ?>
value="index.php?pagename=cms_manager&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=50">50</option>
              </select></td>
          </tr>
          <?php
	$searchRecord = " (menu_id = 0 or menu_id = -1)";
	
	$rs = $s->getData_withPages('tbl_cms_pages',$order_by, $order, $from, $max_results, $searchRecord);
	
	if(mysqli_num_rows($rs)==0 && $page!=1)
	{
		$page=1;
		$s->pageLocation("index.php?pagename=cms_manager&orderby=pages_id&order=$order&pageno=$page&records=$max_results"); 
	}
	
	if($order == "asc")
	{
		$order_new = "desc";
	}
	else if($order == "desc")
	{
		$order_new = "asc";
	}
	
	$i=1;
	if(mysqli_num_rows($rs)!=0)
	{		
?>
          <tr class="head">
            <td width="14%" align="center" ><a href="index.php?pagename=cms_manager&orderby=pages_id&order=<?php echo $order_new; ?>&pageno=<?php echo $page;?>&records=<?php echo $max_results;?>">ID</a></td>
            <td width="40%" class="pad"><a href="index.php?pagename=cms_manager&orderby=pages_name&order=<?php echo $order_new; ?>&pageno=<?php echo $page;?>&records=<?php echo $max_results;?>">Page Name</a></td>
            <td width="14%" class="pad"><a href="index.php?pagename=cms_manager&orderby=page_title&order=<?php echo $order_new; ?>&pageno=<?php echo $page;?>&records=<?php echo $max_results;?>">Page Title</a></td>
            <td width="12%" align="center" > Status </td>
            <td width="20%"  align="center">Action</td>
          </tr>
          <?php
			while($row = mysqli_fetch_object($rs))
			{
?>
          <tr class="text" onmouseover="bgr_color(this, '#EAB9BA')" onMouseOut="bgr_color(this, '')">
            <td align="center" valign="middle"><?php echo $row->pages_id; ?></Td>
            <td class="pad" valign="middle" ><?php echo ucfirst(stripslashes($row->pages_name)) ; ?></td>
            <td class="pad" valign="middle"><?php echo ucfirst(stripslashes($row->page_title)) ; ?></td>
            <td align="center" valign="middle"><?php 
	if($row->page_status =="active")
	{
?>
              <img src="images/green.gif" title="Active" border="0"  /> &nbsp; &nbsp; <a href="index.php?pagename=cms_manager&action=ChangeStatus&pcode=<?php echo $row->pages_id;?>&pageno=<?php echo $page;?>&records=<?php echo $max_results;?>"><img src="images/red_light.gif" title="Inactive" border="0"  /></a>
              <?php
	}
	else if($row->page_status =="inactive")
	{
?>
              <a href="index.php?pagename=cms_manager&action=ChangeStatus&pcode=<?php echo $row->pages_id;?>&pageno=<?php echo $page;?>&records=<?php echo $max_results;?>"><img src="images/green_light.gif" title="Active" border="0"  /></a> &nbsp; &nbsp; <img src="images/red.gif" title="Inactive" border="0"  />
              <?php		
	}
?></td>
            <td align="center" valign="middle"><a href="index.php?pagename=add_cms&action=edit&pcode=<?php echo $row->pages_id;?>"> <img src="images/e.gif" border="0"  alt="Edit"/></a> &nbsp; &nbsp;
              <?php	if($row->lock_status == "inactive")
		{
?>
<?php if($_SESSION["AdminLoginID_SET"]!='131') {?>
              <a href="index.php?pagename=cms_manager&action=delete&pcode=<?php echo $row->pages_id;?>" onclick='return del();'> <img src="images/x.gif" border="0" alt="Delete" /></a>
              <?php }?>
              <?php 
		}
		else
		{
?>
              <img src="images/protect.gif" border="0" alt="Locked Data" />
              <?php		
		}
?></td>
          </tr>
          <?php 
		
				SubCMSPageRecords($row->pages_id, "&nbsp;");
			}
	
?>
          <tr class='head headLink'>
            <td colspan="8" align="right" nowrap="nowrap"><table width="100%">
                <tr>
                  <td  nowrap="nowrap" align="right"><?php						
		 	$total_pages = $s->getTotal_pages('tbl_cms_pages', $order_by, $order, $max_results, $searchRecord);
			if($page > 1)
			{ 
				$prev = ($page - 1); 
			echo "<ul><li><a href='index.php?pagename=cms_manager&orderby=$order_by&order=$order&pageno=$prev&records=$max_results'> Previous</a></li></ul>"; 
			} 
?></td>
                  <td width="43" nowrap="nowrap" align="center"><select name="pages_select" onchange="OnSelectPages();">
                      <?php
			for($i = 1; $i <= $total_pages; $i++)
			{ 
?>
                      <option <?php if($page==$i){ echo "selected='selected'";} ?> 
value="index.php?pagename=cms_manager&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $i;?>&records=<?php echo $max_results;?>"><?php echo $i;?></option>
                      <?php
			} 
?>
                    </select></td>
                  <td width="59" nowrap="nowrap"  align="left"><?php		
			if($page < $total_pages)
			{ 
				$next = ($page + 1); 
				echo "<ul><li><a href='index.php?pagename=cms_manager&orderby=$order_by&order=$order&pageno=$next&records=$max_results'>Next </a></li></ul>";
			} 
?></td>
                </tr>
              </table></td>
          </tr>
          <?php 
		}
		else
		{	
?>
          <tr class='text'>
            <td colspan='8' class='redstar'>&nbsp; No record present in database.</td>
          </tr>
          <?php
		}
?>
        </table></td>
    </tr>
  </table>
</form>