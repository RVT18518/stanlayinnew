<?php
	include("../includes/function_lib.php");
	$comp_name   = $_REQUEST["comp"];
	$rs_module = $s->getData_with_condition('tbl_comp','id',$comp_name);
	if(mysqli_num_rows($rs_module )>0)
	{
		$row_module  = mysqli_fetch_object($rs_module);
		$no_of_emp=$row_module->no_of_emp;
		$comp_website=$row_module->comp_website;
		$comp_revenue=$row_module->comp_revenue;
		$cust_segment=$row_module->cust_segment;
		/*
?>
<select name="state" id="state" class="inpuTxt" >
<?php
	while($row_module  = mysqli_fetch_object($rs_module))
	{
	
?>
 <option value="<?php echo $row_module->zone_id ;?>"><?php echo $row_module->zone_name;?></option>
<?php
	}
?>
</select>
<?php 
	}
	else
	{
?>	
<input type="text" name="state" id="state" value="" class="inpuTxt">
<?php 
	}
?>*/
?>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr class="text">
    <td width="20%" align="left" valign="top"  class="pad"><strong>Company Name</strong></td>
    <td width="22%" align="left" valign="top"  ><strong>Website</strong></td>
    <td width="18%" valign="top" ><strong>Industry</strong></td>
    <td width="20%" align="left" valign="top" ><strong>Annual Revenues</strong></td>
    <td width="20%" valign="top" ><strong>No. of Employees</strong></td>
  </tr>
  <tr class="text">
    <td align="left" valign="top"  class="pad"><select name="comp_name" class="inpuTxt" id="comp_name" onChange="htmlData_comp_details('comp_details_ajax.php', 'comp='+this.value);">
        <option value="0" <?php if($comp_name=='0' ){ echo "selected='selected'"; }?>>Select Company </option>
        <?php
            $query_comp_manager   	= "deleteflag='active' and status='active' order by comp_name";
            $rs_desig_comp_manager   = $s->selectwhere('tbl_comp',$query_comp_manager);
            while($row_desig_comp_manager = mysqli_fetch_object($rs_desig_comp_manager))
            {
            ?>
        <option  value="<?php echo $row_desig_comp_manager->id; ?>"
	   <?php  if($row_desig_comp_manager->id == $comp_name){ echo "selected='selected'";}?>><?php echo stripslashes($row_desig_comp_manager->comp_name); ?></option>
        <?php } ?>
         <option value="other">Other</option>
      </select>
      <span class="redstar"> *</span></td>
    <td align="left" valign="top"  ><input type="text" name="comp_website" id="comp_website" value="<?php echo $comp_website;?>" /></td>
    <td valign="top" ><select name="cust_segment" id="cust_segment" class="inpuTxt">
        <option value="0" <?php if($cust_segment=='0' ){ echo "selected='selected'"; }?>>Select Customer Segment</option>
        <option value="cons_road" <?php if($cust_segment=='cons_road' ){ echo "selected='selected'"; }?>>Construction & Road</option>
        <option value="eng_ind_inst" <?php if($cust_segment=='eng_ind_inst' ){ echo "selected='selected'"; }?>>Engineering/Industrial/Institutional</option>
        <option value="surveying_cons" <?php if($cust_segment=='surveying_cons' ){ echo "selected='selected'"; }?>>Surveying Consultant</option>
        <option value="telecom" <?php if($cust_segment=='telecom' ){ echo "selected='selected'"; }?>> Telecommunications</option>
        <option value="railways" <?php if($cust_segment=='railways' ){ echo "selected='selected'"; }?>> Railways</option>
        <option value="railways_cont" <?php if($cust_segment=='railways_cont' ){ echo "selected='selected'"; }?>> Railways Contractor</option>
        <option value="defense" <?php if($cust_segment=='defense' ){ echo "selected='selected'"; }?>> Defense</option>
        <option value="bro" <?php if($cust_segment=='bro' ){ echo "selected='selected'"; }?>> BRO</option>
        <option value="mes" <?php if($cust_segment=='mes' ){ echo "selected='selected'"; }?>> MES</option>
        <option value="govt_cont" <?php if($cust_segment=='govt_cont' ){ echo "selected='selected'"; }?>> Govt Contractor</option>
        <option value="industrial" <?php if($cust_segment=='industrial' ){ echo "selected='selected'"; }?>> Industrial</option>
        <option value="power" <?php if($cust_segment=='power' ){ echo "selected='selected'"; }?>> Power</option>
        <option value="pipeline" <?php if($cust_segment=='pipeline' ){ echo "selected='selected'"; }?>> Pipeline</option>
        <option value="govt_others" <?php if($cust_segment=='govt_others' ){ echo "selected='selected'"; }?>> Govt Others</option>
        <option value="govt_supp_reseller" <?php if($cust_segment=='govt_supp_reseller' ){ echo "selected='selected'"; }?>> Govt Supplier/ Reseller</option>
      </select></td>
    <td align="left" valign="top" ><input type="text" name="comp_revenue" id="comp_revenue" value="<?php echo $comp_revenue;?>" /></td>
    <td valign="top" ><input type="text" name="no_of_emp" id="no_of_emp" value="<?php echo $no_of_emp;?>" /></td>
  </tr>
</table>
<?php
}
if ($comp_name=='other' || $comp_name==0)
{
?>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr class="text">
    <td width="20%" align="left" valign="top"  class="pad"><strong>Company Name</strong></td>
    <td width="22%" align="left" valign="top"  ><strong>Website</strong></td>
    <td width="18%" valign="top" ><strong>Industry</strong></td>
    <td width="20%" align="left" valign="top" ><strong>Annual Revenues</strong></td>
    <td width="20%" valign="top" ><strong>No. of Employees</strong></td>
  </tr>
  <tr class="text">
    <td align="left" valign="top"  class="pad"><input type="text" name="comp_name" id="comp_name" value="" />
      <span class="redstar"> *</span></td>
    <td align="left" valign="top"  ><input type="text" name="comp_website" id="comp_website" value="<?php echo $comp_website;?>" /></td>
    <td valign="top" ><select name="cust_segment" id="cust_segment" class="inpuTxt">
        <option value="0" <?php if($cust_segment=='0' ){ echo "selected='selected'"; }?>>Select Customer Segment</option>
        <option value="cons_road" <?php if($cust_segment=='cons_road' ){ echo "selected='selected'"; }?>>Construction & Road</option>
        <option value="eng_ind_inst" <?php if($cust_segment=='eng_ind_inst' ){ echo "selected='selected'"; }?>>Engineering/Industrial/Institutional</option>
        <option value="surveying_cons" <?php if($cust_segment=='surveying_cons' ){ echo "selected='selected'"; }?>>Surveying Consultant</option>
        <option value="telecom" <?php if($cust_segment=='telecom' ){ echo "selected='selected'"; }?>> Telecommunications</option>
        <option value="railways" <?php if($cust_segment=='railways' ){ echo "selected='selected'"; }?>> Railways</option>
        <option value="railways_cont" <?php if($cust_segment=='railways_cont' ){ echo "selected='selected'"; }?>> Railways Contractor</option>
        <option value="defense" <?php if($cust_segment=='defense' ){ echo "selected='selected'"; }?>> Defense</option>
        <option value="bro" <?php if($cust_segment=='bro' ){ echo "selected='selected'"; }?>> BRO</option>
        <option value="mes" <?php if($cust_segment=='mes' ){ echo "selected='selected'"; }?>> MES</option>
        <option value="govt_cont" <?php if($cust_segment=='govt_cont' ){ echo "selected='selected'"; }?>> Govt Contractor</option>
        <option value="industrial" <?php if($cust_segment=='industrial' ){ echo "selected='selected'"; }?>> Industrial</option>
        <option value="power" <?php if($cust_segment=='power' ){ echo "selected='selected'"; }?>> Power</option>
        <option value="pipeline" <?php if($cust_segment=='pipeline' ){ echo "selected='selected'"; }?>> Pipeline</option>
        <option value="govt_others" <?php if($cust_segment=='govt_others' ){ echo "selected='selected'"; }?>> Govt Others</option>
        <option value="govt_supp_reseller" <?php if($cust_segment=='govt_supp_reseller' ){ echo "selected='selected'"; }?>> Govt Supplier/ Reseller</option>
      </select></td>
    <td align="left" valign="top" ><input type="text" name="comp_revenue" id="comp_revenue" value="<?php echo $comp_revenue;?>" /></td>
    <td valign="top" ><input type="text" name="no_of_emp" id="no_of_emp" value="<?php echo $no_of_emp;?>" /></td>
  </tr>
</table>
<?php
}?>