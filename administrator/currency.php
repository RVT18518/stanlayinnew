<?php 
	$data_action = $_REQUEST["action"];
	$pcode		 =$_REQUEST["pcode"];
	$order 		 = $_REQUEST["order"];
	$order_by    = $_REQUEST["orderby"]; 
	if(!isset($_GET['pageno']))
	{ 
    	$page = 1; 
	} else { 
    	$page = $_GET['pageno']; 
	} 
	if(!isset($_GET['records']))
	{ 
    	$max_results = 10; 
	} else { 
   		$max_results = $_GET['records']; 
	} 
	$from = (($page * $max_results) - $max_results);  
	
	if($_REQUEST['action']=='update' ||$_REQUEST['action']=='insert')
	{
				
		$fileArray["currency_title"]  =$_REQUEST["title"];
		$fileArray["currency_code"]	  =$_REQUEST["code"];
		$fileArray["currency_symbol"] =	$_REQUEST["symbol"];
		$fileArray["currency_value"]	=$_REQUEST["value"];
		$currency_status	= $_REQUEST["default"];
	}
	if($_REQUEST['action']=='update')
	{
		if($currency_status=='yes')
		{
			$sql1="update tbl_currencies set currency_status='no'";
			mysqli_query($GLOBALS["___mysqli_ston"],$sql1);
			$fileArray["currency_status"] = 'yes';
		}
		else
		{
				$fileArray["currency_status"] = 'no';	
		}
		$result = $s->editRecord('tbl_currencies ',$fileArray,'currency_id',$pcode);
		$sql_def="select * from tbl_currencies where currency_status='yes'";
		$result_def = mysqli_query($GLOBALS["___mysqli_ston"],$sql_def);
		if(mysqli_num_rows($result_def)<=0)
		{
			$sql_sel="update tbl_currencies set currency_status='yes' where currency_super_default ='yes'";
			mysqli_query($GLOBALS["___mysqli_ston"],$sql_sel);
		}
	}
	if($_REQUEST['action']=='insert')
	{
		if($currency_status=='yes')
		{
			$sql1="update tbl_currencies set currency_status='no'";
			mysqli_query($GLOBALS["___mysqli_ston"],$sql1);
			$fileArray["currency_status"] = 'yes';
		}
		else
		{
				$fileArray["currency_status"] = 'no';	
		}
		$result = $s->insertRecord('tbl_currencies',$fileArray);
	}
	
?>
<script type="text/javascript">
function OnSelect() 
{
	window.location = document.frx1.records.value;
}
function OnSelectPages()
{
	window.location = document.frx1.pages_select.value;
}
</script>
<form name="frx1" id="frx1" action="#" method="post">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="pagecontent">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="24%" class="pageheadTop">Currency Manager</td>
          <td width="76%" class="headLink"><ul>
              <li><a href="index.php?pagename=add_edit_currency&amp;action=add_new">Add New Currency</a></li>
          </ul></td>
        </tr>
    </table></td>
  </tr>
  <tr>
    <td class="pHeadLine"></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
<tr><td>
	<?php 
		if($data_action=='delete')
		{
			$sql_del="select currency_id from tbl_currencies where currency_super_default ='yes'";
			$rs_del=mysqli_query($GLOBALS["___mysqli_ston"],$sql_del);
			if(mysqli_num_rows($rs_del)>0)
			{
				$r_del=mysqli_fetch_object($rs_del);
			if($r_del->currency_id != $pcode) 
			{
				$result = $s->delete_table_withCondition('tbl_currencies','currency_id',$pcode);	
				$sql_def="select * from tbl_currencies where currency_status='yes'";
				$result_def = mysqli_query($GLOBALS["___mysqli_ston"],$sql_def);
				if(mysqli_num_rows($result_def)<=0)
				{
					$sql_sel="update tbl_currencies set currency_status='yes' where currency_super_default ='yes'";
					mysqli_query($GLOBALS["___mysqli_ston"],$sql_sel);
				}
			}
			else
			{
				echo "<p class='error'> Super Default Currency ! This record not deleted.</p><br />";
				$result=-3;
			}
			}
			if($result==1)
			{
				echo "<p class='success'>".record_delete."</p><br />";	
			}
			else if($result==0)
			{
				echo "<p class='error'>".record_not_delete."</p><br />";	
			}
		}
		if($_REQUEST['action']=='update')
		{
			 if($result==0)
			{
				echo "<p class='success'>".record_update."</p><br />";	
			}
			else if($result==1)
			{
				echo "<p class='error'>".record_not_update."</p><br />";	
			}
		}
		else if($_REQUEST['action']=='insert')
		{
			 if($result==0)
			{
				echo "<p class='success'>".record_added."</p><br />";	
			}
			else if($result==1)
			{
				echo "<p class='error'>".record_not_added."</p><br />";	
			}
		}
?>
	
</td></tr>
  <tr>
    <td><table width="100%" border="0" cellpadding="0" cellspacing="0" class="tblBorder">
      <tr class="pagehead">
        <td colspan="5" class="pad" >Currenciess</td>
		<td>
		  <div align="center">Records View &nbsp; 
              <select name="records" onchange="OnSelect();"  >
                <option <?php if($max_results==10){ echo "selected='selected'";} ?> 
value="index.php?pagename=currency&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=10">10</option>
                <option <?php if($max_results==20){ echo "selected='selected'";} ?>
value="index.php?pagename=currency&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=20">20</option>
                <option <?php if($max_results==50){ echo "selected='selected'";} ?>
value="index.php?pagename=currency&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=50">50</option>
                <option <?php if($max_results==100){ echo "selected='selected'";} ?>
value="index.php?pagename=currency&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=100">100</option>
              </select>
		      </div></td>
      </tr>
<?php 
	if($order_by == '')
	{
		$order_by = 'currency_id';	
	}
	$rs = $s->getData_withPages('tbl_currencies',$order_by, $order,$from,$max_results);
	if($_REQUEST["order"] == 'asc')
	{
		$order = 'desc';
	}
	else if($_REQUEST["order"] == 'desc')
	{
		$order = 'asc';
	}
	else
	{
		$order = 'asc';
	}
	$i=1;
	if(mysqli_num_rows($rs)==0 && $page != 1)
	{
		$page=1;
		$s->pageLocation("index.php?pagename=currency&orderby=currency_title&order=$order&pageno=$page&records=$max_results"); 

	}
	if(mysqli_num_rows($rs)>0)
	{
?>	
  <tr class="head">
        <td width="7%" align="center">ID</td>
        <td width="13%">
		<a href="index.php?pagename=currency&orderby=currency_title&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=<?php echo $max_results;?>"> Currency</a></td>
        <td width="11%" align="center">
		<a href="index.php?pagename=currency&orderby=currency_code&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=<?php echo $max_results;?>">Code</a></td>
        <td width="13%" align="center">Symbol</td>
        <td width="12%" align="center">
		<a href="index.php?pagename=currency&orderby=currency_value&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=<?php echo $max_results;?>">Value</a></td>		
        <td width="19%" align="center" nowrap="nowrap">Action</td>
      </tr>
    
<?php	
	while($row = mysqli_fetch_object($rs))
	{
?>
	  <tr class="text" onmouseover="bgr_color(this, '#EAB9BA')" onmouseout="bgr_color(this, '')">
        <td align="center"><?php echo $row->currency_id; ?></td>
        <td><?php echo $row->currency_title; ?> <?php if($row->currency_status=="yes"){ echo "<b>(Default)</b>"; } ?></td>
        <td align="center"><?php echo strtoupper($row->currency_code); ?></td>
		<td align="center"><?php echo $row->currency_symbol; ?></td>
        <td align="center"><?php printf("%.2f", $row->currency_value); ?></td>        
        <td align="center">
		  <a href="index.php?pagename=add_edit_currency&action=edit&pcode=<?php echo $row->currency_id; ?>">
		      <img src="../images/e.gif" alt="Edit" border="0" /></a> &nbsp; &nbsp;  
		      <a href="index.php?pagename=currency&action=delete&pcode=<?php echo $row->currency_id; ?>&orderby=currency_title&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=<?php echo $max_results;?>"onclick='return del();' >
            <img src="../images/x.gif" alt="Delete" border="0"  /></a></td>
      </tr>
  <?php
  	$i++;
  	} 
?>
<tr class='head headLink'><td colspan="6" align="right" nowrap="nowrap">
<table width="100%">
<tr><td width="1092" nowrap="nowrap">
  <div align="right">
    <?php						
		 	$total_pages = $s->getTotal_pages('tbl_currencies',$order_by, $order,$max_results );
			if($page > 1)
			{ 
				$prev = ($page - 1); 
			echo "<ul><li><a href='index.php?pagename=currency&orderby=$order_by&order=$order&pageno=$prev&records=$max_results'>< Previous</a></li></ul>"; 
			} 
?>
  </div></td>
<td width="43" nowrap="nowrap" align="center">
			  <select name="pages_select" onchange="OnSelectPages();"  >
                <?php
			for($i = 1; $i <= $total_pages; $i++)
			{ 
?>
			            <option <?php if($page==$i){ echo "selected='selected'";} ?> 
value="index.php?pagename=currency&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $i;?>&records=<?php echo $max_results;?>"><?php echo $i;?></option>
			    
                <?php
				
			} 
?>
              </select>
			</td><td width="59" nowrap="nowrap" >
              <div align="left">
                <?php		
			if($page < $total_pages)
			{ 
				$next = ($page + 1); 
				echo "<ul><li><a href='index.php?pagename=currency&orderby=$order_by&order=$order&pageno=$next&records=$max_results'>Next ></a></li></ul>";
			} 
			 
		?>
              </div></td>
</tr></table>
</td></tr>
<?php
  }
  else
  {
?> 
<tr class='text'><td colspan='7' class='redstar'> &nbsp; No record present in database</td></tr>
<?php
  }
  ?>

   
    </table></td>
  </tr>
</table>
</form>
