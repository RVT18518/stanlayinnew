<?php
require('pdf/fpdf.php');
$d=date('d_m_Y');

class PDF extends FPDF
{

function Header()
{
    //Logo
	$name="Export PDF";
    $this->SetFont('Arial','B',15);
    //Move to the right
    $this->Cell(80);
    //Title
	$this->SetFont('Arial','B',9);
    //Line break
    $this->Ln(20);
}

//Page footer
function Footer()
{
   
}

//Load data
function LoadData($file)
{
	//Read file lines
	$lines=file($file);
	$data=array();
	foreach($lines as $line)
		$data[]=explode(';',chop($line));
	return $data;
}

//Simple table
function BasicTable($header,$data)
{ 

$this->SetFillColor(0,255,0);
$this->SetDrawColor(128,0,0);
$w=array(30,65,50,10,10,10,10,10,15,15,15,15,15);

$this->Cell(30,6,"Asian contec Limited",4);
$this->Ln();
$this->Cell(30,6,"Asian Centre,",4);
$this->Ln();
$this->Cell(30,6,"B-28, Okhla Industrial Area, Phase - 1,",4);
$this->Ln();
$this->Cell(30,6,"New Delhi - 110020.",4);
$this->Ln();
$this->Cell(30,6,"Tel : +91-11-41860000 (100 lines)",4);
$this->Ln();
$this->Cell(30,6,"Fax : +91-11-41860066",4);
$this->Ln();
$this->Cell(30,6,"Sales helpline : +91-11-41406926",4);
$this->Ln();
$this->Cell(30,6,"Email : sales@stanlay.com",4);
$this->Ln();
$this->Cell(30,6,"Web : www.stanlay.in | www.stanlay.com",4);
$this->Ln();
$this->Ln();
	//Header
	for($i=0;$i<count($header);$i++)
		$this->Cell($w[$i],7,$header[$i],1);
	$this->Ln();
	//Data
	foreach ($data as $eachResult) 
	{ //width
		$this->Cell(30,6,$eachResult["admin_id"],1);
		$this->Cell(65,6,$eachResult["admin_fname"],1);
		$this->Cell(50,6,$eachResult["admin_email"],1);
		$this->Cell(10,6,$eachResult["admin_address"],1);
		$this->Cell(10,6,$eachResult["admin_telephone"],1);
		$this->Ln();
	}
}

//Better table
}

$pdf=new PDF();

		
$header=array('Admin ID','Admin Name','Email','Address','Telephone');
//Data loading
//*** Load MySQL Data ***//
$objConnect = mysqli_connect("localhost","root","root") or die("Error:Please check your database username & password");
$objDB = mysqli_select_db("stanlay");
$strSQL = "SELECT admin_id, admin_fname,admin_email, admin_address, admin_telephone  FROM tbl_admin";
$objQuery = mysqli_query($GLOBALS["___mysqli_ston"],$strSQL);
$resultData = array();
for ($i=0;$i<mysqli_num_rows($objQuery);$i++) {
	$result = mysqli_fetch_array($objQuery);
	array_push($resultData,$result);
}
//************************//


function forme()

{
$d=date('d_m_Y');
echo "PDF generated successfully. To download document click on the link >> <a href=".$d.".pdf>DOWNLOAD</a>";
}


$pdf->SetFont('Arial','',12);

//*** Table 1 ***//
$pdf->AddPage();
$pdf->Ln(5);
$pdf->BasicTable($header,$resultData);
forme();
$pdf->Output("$d.pdf","F");

?>