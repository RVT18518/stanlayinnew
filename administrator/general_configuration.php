<link href="css/style.css" rel="stylesheet" type="text/css" />
<?php
	$data_action = $_REQUEST["action"];
	$rs 		 = $s->getData_without_condition('tbl_general_configuraction');
	if(mysqli_num_rows($rs)!=0)
	{
		$row	 = mysqli_fetch_object($rs);
		$pcode	 = $row->gen_config_id;
	}
	if($_REQUEST['action'] == 'delete_image')
	{
		unlink("../".$row->store_logo);
		$DelLogo["store_logo"] = '';
		$DelLogo["display_title"] = "name";
		$result      = $s->editRecord('tbl_general_configuraction',$DelLogo,'gen_config_id',$pcode);
		$s->pageLocation("index.php?pagename=general_configuration&action=edit");
	}
	if($_REQUEST['action'] == 'update' && $_REQUEST["add"]=='Save')
	{
		$fileArray["website_title"] = addslashes($_REQUEST["title"]);
		$fileArray["store_name"]   	= addslashes($_REQUEST["store_name"]);
		$fileArray["admin_email"]  	= $_REQUEST["admin_email"];
		$fileArray["meta_content"] 	= $_REQUEST["meta_content"];
		$fileArray["meta_desc"]  	= $_REQUEST["meta_desc"];
		$fileArray["webtitlecont"]  = $_REQUEST["webtitlecont"];
		$fileArray["footer_category"]  = $_REQUEST["footer_category"];
		
		if($_FILES["logo"]["name"] != '')
		{
			unlink("../".$row->store_logo);
			$StoreLogoImage 		 = $s->ImageUpload('uploads/storelogo/','logo','STORELOGO','257');
			$fileArray["store_logo"] = $StoreLogoImage;
		}
		if($_REQUEST["display_title"] == "")
		{
			$fileArray["display_title"] = $_REQUEST["displayTitle"];
		}
		if($_REQUEST["cont_email"] == "")
		{
			$fileArray["contact_email"] = $_REQUEST["admin_email"];
		}
		else
		{
			$fileArray["contact_email"] = $_REQUEST["cont_email"];
		}
		if($_REQUEST["customer_support"] == "")
		{
			$fileArray["customer_support__email"] = $_REQUEST["admin_email"];
		}
		else
		{
			$fileArray["customer_support__email"] = $_REQUEST["customer_support"];
		}
		if($_REQUEST["general_contact"] == "")
		{
			$fileArray["gen_contact_email"]  = $_REQUEST["admin_email"];
		}
		else
		{
			$fileArray["gen_contact_email"] = $_REQUEST["general_contact"];
		}
		
		//////////////
		
	}
	if($data_action =='update')
	{
		$result      = $s->editRecord('tbl_general_configuraction',$fileArray,'gen_config_id',$pcode);
		$s->pageLocation("index.php?pagename=general_configuration&action=edit&setX=1");
	}
	if($data_action =='edit')
	{
		$rs  = $s->getData_with_condition('tbl_general_configuraction','gen_config_id',$pcode);
		$row = mysqli_fetch_object($rs);
		$data_action = 'update';
	}
?>

<form name="frx1" id="frx1" action="index.php?pagename=general_configuration&action=<?php echo $data_action;?>&pcode=<?php echo $pcode;?>" method="post" enctype="multipart/form-data">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="90%" class="pageheadTop">General Configuration </td>
        <td width="9%" class="headLink" align="right"><?php if($_SESSION["AdminLoginID_SET"]!='131') {?><input name="add" type="submit" class="inputton" id="add" value="Save"> <?php }?>&nbsp; </td>
		<td width="1%"  class="pad" align="right"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td class="pHeadLine"></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr><td>
<?php 
$_REQUEST['setX']=0;
	if($_REQUEST['setX']=='1')
	{
	 	if($result==0)
		{
			echo "<p class='success'>".record_update."</p><br />";	
		}
		else if($result==1)
		{
			echo "<p class='error'>".record_not_update."</p><br />";	
		}
	}
		
?>
</td></tr>

<tr><td>
<table width="100%" border="0" cellspacing="0" cellpadding="3">
<tr><td valign="top" class="pagecontent">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="tblBorder">
<tr class="pagehead"><td   class="pad">Store Configuration</td>
</tr>
<tr><td >
<table width="100%" border="0" cellpadding="3" cellspacing="0">
<tr class="head">
  <td  class="pad" colspan="2">Website Configuration</td>
</tr>
 
  <tr class="text">
    <td class="pad">Company Name <span class="redstar">*</span> </td>
    <td><input type="text" name="store_name" id="store_name" value="<?php echo stripslashes($row->store_name);?>" class="inpuTxt" />    </td>
  </tr>
  
<tr class="head">
  <td  class="pad" colspan="2">Email Configuration</td>
</tr>
<tr class="text">
<td width="19%" class="pad">Admin Email <span class="redstar">*</span> </td>
<td width="81%"><input type="text" name="admin_email" id="admin_email" value="<?php echo $row->admin_email;?>" class="inpuTxt" /></td>
</tr>

<tr class="head">
  <td  class="pad" colspan="2">Website Content</td>
</tr>
<tr class="text">
    <td valign="top" class="pad">Enter Website Home Content<span class="redstar"></span></td>
    <td><?php
					$sBasePath  			 = "../fckeditor/";
					$oFCKeditor 			 = new FCKeditor('webtitlecont') ;
					$oFCKeditor->BasePath 	 = $sBasePath;
					$oFCKeditor->Value		 =  stripslashes($row->webtitlecont); 
					$oFCKeditor->Width 		 = '600' ;
					$oFCKeditor->Height 	 = '300' ;
					$oFCKeditor->Create() ;
?></td>
  </tr>
  
  
  <tr class="text">
    <td valign="top" class="pad">Footer Categories<span class="redstar"></span></td>
    <td><?php
					$sBasePath  			 = "../fckeditor/";
					$oFCKeditor 			 = new FCKeditor('footer_category') ;
					$oFCKeditor->BasePath 	 = $sBasePath;
					$oFCKeditor->Value		 =  stripslashes($row->footer_category); 
					$oFCKeditor->Width 		 = '600' ;
					$oFCKeditor->Height 	 = '300' ;
					$oFCKeditor->Create() ;
?></td>
  </tr>
  
  
  
<tr class="head">
  <td  class="pad" colspan="2">SEO Configuration</td>
</tr>
 <tr class="text">
    <td class="pad">Website Title <span class="redstar">*</span></td>
    <td><input type="text" name="title" id="title" value="<?php echo stripslashes($row->website_title);?>" class="inpuTxt" /> </td>
  </tr>
<tr class="text">
  <td class="pad" valign="top">Meta Content</td>
  <td align="left"><textarea class="inpuTxtSelect" cols="35" name="meta_content" id="meta_content" rows="6" ><?php echo stripslashes($row->meta_content);?></textarea></td>
</tr>
<tr class="text" valign="top">
  <td class="pad">Meta Description</td>
  <td align="left"><textarea class="inpuTxtSelect" cols="35" name="meta_desc" id="meta_desc" rows="6" ><?php echo stripslashes($row->meta_desc);?></textarea></td>
</tr>
<tr class="text">
<td class="pad">&nbsp;</td>
<td align="left"><?php if($_SESSION["AdminLoginID_SET"]!='131') {?><input name="add" type="submit" class="inputton" id="add" value="Save"> <?php }?></td>
</tr>
<tr class="text"><td class="redstar pad" colspan="2"> * Required Fields </td></tr>  
</table>
</td></tr>
</table>
</td></tr>
</table>
</td></tr>
</table>
</form>
<script language="JavaScript" type="text/javascript">
 var frmvalidator = new Validator("frx1");
 frmvalidator.addValidation("title","req","Please enter website title.");
 frmvalidator.addValidation("store_name","req","Please enter Website Name.");
 frmvalidator.addValidation("admin_email","req","Please enter Admin Email.");
 frmvalidator.addValidation("admin_email","email","Please enter valid Admin Email Address.");
/* frmvalidator.addValidation("cont_email","email","Please enter valid Contact Us Email Address.");
 frmvalidator.addValidation("customer_support","email","Please enter valid Customer Support Email Address.");
 frmvalidator.addValidation("general_contact","email","Please enter valid General Contact Email Address.");*/
</script>
