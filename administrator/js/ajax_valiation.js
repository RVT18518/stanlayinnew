// JavaScript Document
$.validator.setDefaults({
	submitHandler: function() 
	{
		
formsubmit();
//frm.submit('index.php?pagename=add_test');
	},
	highlight: function(input) {
		$(input).addClass("ui-state-highlight");
	},
	unhighlight: function(input) {
		$(input).removeClass("ui-state-highlight");
	}
});
 
$().ready(function() {
	$.fn.themeswitcher && $('<div/>').css({
		position: "absolute",
		right: 10,
		top: 10
	}).appendTo(document.body).themeswitcher();
	
			
	// validate signup form on keyup and submit
	$("#frx1").validate({
		rules: {
			firstname: "required",
			newstitle: "required",
			event_title: "required",
			news_date: "required",
			sort_date: "required",
			expiry_date: "required",
			testimonialstitle: "required",
			albumtitle: "required",
			newsletter_cate_name: "required",
			newsletter_title: "required",
			job_code: "required",
			job_title: "required",
			location: "required",
			date_from: "required",
			date_to: "required",
			
			title: "required",
			store_name: "required",
			admin_email: "required",
			
			page_name: "required",
			
			old_pass: "required",
			new_pass: "required",
			confirm_pass: "required",
			
			admin_fname: "required",
			admin_lname: "required",
			
			large_img_width: "required",
			small_img_width: "required",
			medium_img_width: "required",
			large_img_height: "required",
			small_img_height: "required",
			medium_img_height: "required",
			
			
			//newsletter_path: "required",
			
			username: {
				required: true,
				minlength: 2
			},
			
			newsletter_path: {
				required: false,
				accept: false
			},
			
			
			
			large_img_width: {
				required: true,
				number: true
			},
			
			
			
			medium_img_width: {
				required: true,
				number: true
			},
			
			
			small_img_width: {
				required: true,
				number: true
			},
			
			
			
			
			
			/* */
			
				large_img_height: {
				required: true,
				number: true
			},
			
			
			
			medium_img_height: {
				required: true,
				number: true
			},
			
			
			small_img_height: {
				required: true,
				number: true
			},
			
			
			
			testimonials_date: {
				required: true
				/*,
				date: true*/
			},
			
			new_pass: {
				required: true,
				minlength: 5,
				maxlength: 20
			},
			confirm_pass: {
				required: true,
				minlength: 5,
				maxlength: 20,
				equalTo: "#new_pass"
			},
			
			admin_email: {
				required: true,
				email: true
			},
			
			email: {
				//required: true,
				email: true
			},
			topic: {
				required: "#newsletter:checked",
				minlength: 2
			},
			agree: "required"
		},
		messages: {
			newstitle: "Please enter news title",
			event_title: "Please enter event title",
			sort_date: "Please enter start date",
			expiry_date: "Please enter end date",
			testimonialstitle: "Please enter testimonial title",
			albumtitle: "Please enter album title",
			newsletter_cate_name: "Please enter category title",
			newsletter_title: "Please enter title",
			
			job_code: "Please enter job code.",
			job_title: "Please enter job title.",
			location: "Please enter job location.",
			date_from: "Please enter from date.",
			date_to: "Please enter to date.",
			
			title: "Please enter website title.",
			store_name: "Please enter store name.",
			page_name:"Please enter page name.",
			//admin_email: "Please enter Admin Email.",
			old_pass:"Please enter your old password",
			
			admin_fname: "Please enter your first name",
			admin_lname: "Please enter your last name",
			
			//newsletter_path:"Please select File",
			
//			date: "Please enter date",
			//date: "Please enter a valid date.",
			lastname: "Please enter your lastname",
			username: {
				required: "Please enter a username",
				minlength: "Your username must consist of at least 2 characters"
			},
			new_pass: {
				required: "Please provide a password",
				minlength: "Your password must be at least 5 characters long"
			},
			confirm_pass: {
				required: "Please provide a password",
				minlength: "Your password must be at least 5 characters long",
				equalTo: "Please enter the same password as above"
			},
			email: true,
			agree: "Please accept our policy"
		}
	});
	
	// propose username by combining first- and lastname
	$("#username").focus(function() {
		var firstname = $("#firstname").val();
		var lastname = $("#lastname").val();
		if(firstname && lastname && !this.value) {
			this.value = firstname + "." + lastname;
		}
	});
	
	$("#frx1 input:(:submit)").addClass("ui-widget-content");
	
	$(":submit").button();
});