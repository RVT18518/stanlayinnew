<?php 
//session_start();
	$data_action = $_REQUEST['action'];
	$pcode		 = $_REQUEST["pcode"];
	$order 		 = $_REQUEST["order"];
	$order_by    = $_REQUEST["orderby"];

	if(!isset($_GET['pageno']))
	{ 
    	$page = 1; 
	} 
	else 
	{ 
    $page = $_GET['pageno']; 
	} 
	if(!isset($_GET['records']))
	{ 
    	$max_results = 25; 
	} 
	else 
	{ 
    $max_results = $_GET['records']; 
	} 
	$from = (($page * $max_results) - $max_results); 
	
	
	if($_REQUEST['action']=='update' ||$_REQUEST['action']=='insert')
	{
		$fileArray["application_name"]      = addslashes($_REQUEST["application_name"]);
		$fileArray["application_name2"]     = addslashes($_REQUEST["application_name2"]);
		$fileArray["application_heading"]   = addslashes($_REQUEST["application_heading"]);
		$fileArray["application_heading2"]  = addslashes($_REQUEST["application_heading2"]);
		$fileArray["sort_order"]  		= addslashes($_REQUEST["sort_order"]);
		
		$fileArray['parent_id1'] 		= $_REQUEST["parent_id1"];
		$fileArray['parent_id2'] 		= $_REQUEST["parent_id2"];
		$fileArray['parent_id3'] 		= $_REQUEST["parent_id3"];

		
		
		$fileArray["login_id"]      	= $_SESSION["AdminLoginID_SET"];
		$fileArray["show_as_list"]      = $_REQUEST["show_as_list"];
		$fileArray["application_status"]    = $_REQUEST["application_status"];
		
		$fileArray["application_description"] = addslashes($_REQUEST["application_description"]);
		$application_added_by = "1";// use session variable for login id 
		$img_result = "";
		if($_FILES["application_logo"]["name"] != "")
		{
			$large				= 450;//$s->fetchGeneral_config('largeimg');
			$medium				= 400;//$s->fetchGeneral_config('mediumimg');
			$small				= $s->fetchGeneral_config('smallimg');	
			
//			$result   = $s->fileUpload('uploads/product_catalog/','catalog_file','product_catalog');
			
			$img_result_large   = $s->ImageUpload('uploads/application_logos/large/','application_logo','application',$large);	
			$img_result_medium  = $s->ImageUpload('uploads/application_logos/medium/','application_logo','application',$medium);	
			$img_result_small   = $s->fileUpload('uploads/application_logos/small/','application_logo','application');	
			if($img_result_large != -1)
			{
				$fileArray["application_logo_large"]  = $img_result_large;
			}
			if($img_result_medium != -1)
			{
				$fileArray["application_logo_medium"] = $img_result_medium;
			}
			if($img_result_small  != -1)
			{
				$fileArray["application_logo_small"] = $img_result_small;
			}	
		}
				
		
		if($_FILES["application_logo_top"]["name"] != "")
		{
			$top			= $s->fetchGeneral_config('smallimg');	
			
			$img_result_top = $s->fileUpload('uploads/application_logos/small/','application_logo_top','applicationTOP');	
			if($img_result_top != -1)
			{
				$fileArray["application_logo_top"]  = $img_result_top;
			}
		}		
		
	}
	if($_REQUEST['action'] == 'ChangeStatus')
	{
		$rs_status = $s->getData_with_condition('tbl_application','application_id',$pcode);
		if(mysqli_num_rows($rs_status)>0)
		{
			$row_status = mysqli_fetch_object($rs_status);
			if($row_status->application_status == 'active')
			{
				$fileArray["application_status"] = 'inactive';
			}
			else if($row_status->application_status == 'inactive')
			{
				$fileArray["application_status"] = 'active';
			}
			$result = $s->editRecord('tbl_application',$fileArray,'application_id',$pcode);
		}
	}
	if( $_REQUEST['action'] == 'update' )
	{
			
		if($_FILES["application_logo"]["name"] !="")
		{
			$rs_del    			= $s->getData_with_condition('tbl_application','application_id',$pcode);
			$row_del   			= mysqli_fetch_object($rs_del);
			$del_image_large  	= $row_del->application_logo_large;
			$del_image_medium 	= $row_del->application_logo_medium;
			$del_image_small  	= $row_del->application_logo_small;
			if(file_exists("../".$del_image_large)>0)
			{
				@unlink("../".$del_image_large);
			}
			if(file_exists("../".$del_image_medium)>0)
			{
				@unlink("../".$del_image_medium);
			}
			if(file_exists("../".$del_image_small)>0)
			{
				@unlink("../".$del_image_small);
			}
		}		
		
		if($_FILES["application_logo_top"]["name"] !="")
		{
			$rs_del    			= $s->getData_with_condition('tbl_application','application_id',$pcode);
			$row_del   			= mysqli_fetch_object($rs_del);
			$del_image_top  	= $row_del->application_logo_top;
			if(file_exists("../".$del_image_top)>0)
			{
				@unlink("../".$del_image_top);
			}
		}
		
		
		$result = $s->editRecord('tbl_application',$fileArray,'application_id',$pcode);
			
	}
	if($_REQUEST['action'] == 'insert')
	{
			if($img_result != -1 )
			{
				$result = $s->insertRecord('tbl_application' ,$fileArray);
			}
	}
?>
<script type="text/javascript">
function OnSelect() 
{
	window.location = document.frx1.records.value;
}
function OnSelectPages()
{
	window.location = document.frx1.pages_select.value;
}
</script>

<form name="frx1" id="frx1" action="#" method="post">
  <table width="100%" align="center" cellpadding="0" cellspacing="0" class="pagecontent">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="46%" class="pageheadTop">Application Manager</td>
            <td width="54%" class="headLink"><ul>
                <li><a href="index.php?pagename=add_application&action=add_new">Add New Application</a></li>
              </ul></td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td class="pHeadLine"></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><?php
		if($_REQUEST['action']=='ChangeStatus')
		{
			if($result==0)
			{
				echo "<p class='success'>Status Change Successfully</p><br/>";	
			}
			else if($result==1)
			{
				echo "<p class='error'>Status Changing Fails</p><br/>";	
			}
			$data_action = "Changed";
		}
		if($data_action=='delete')
		{
			$rs_del    			= $s->getData_with_condition('tbl_application','application_id',$pcode);
			$row_del   			= mysqli_fetch_object($rs_del);
			$del_image_large  	= $row_del->application_logo_large;
			$del_image_medium 	= $row_del->application_logo_medium;
			$del_image_small  	= $row_del->application_logo_small;
			$result = $s->delete_table_withCondition('tbl_application','application_id',$pcode);	
			if($result)
			{
				if(file_exists($del_image_large)>0)
				{
					@unlink($del_image_large);
				}
				if(file_exists($del_image_medium)>0)
				{
					@unlink($del_image_medium);
				}
				if(file_exists($del_image_small)>0)
				{
					@unlink($del_image_small);
				}
				echo "<p class='success'>".record_delete."</p><br/>";	
			}
			else 
			{
				echo "<p class='error'>".record_not_delete."</p><br/>";	
			}
		}
		if($_REQUEST['action']=='update')
		{
			 if($result==0)
			{
				echo "<p class='success'>".record_update."</p><br/>";	
			}
			else if($result==1)
			{
				echo "<p class='error'>".record_not_update."</p><br/>";	
			}
		}
		else if($_REQUEST['action']=='insert')
		{
			 if($result==0)
			{
				echo "<p class='success'>".record_added."</p><br/>";	
			}
			else if($result==1)
			{
				echo "<p class='error'>".record_not_added."</p><br/>";	
			}
		}
?></td>
    </tr>
    <tr>
      <td valign="top"><table width="100%" cellpadding="5" cellspacing="0" class="tblBorder" border="0">
          <tr class="pagehead">
            <td colspan="2" class="pad"> Application Details</td>
            <td align="right" colspan="4">Records View &nbsp;
              <select name="records" onchange="OnSelect();"  >
                <option <?php if($max_results==25){ echo "selected='selected'";} ?>
value="index.php?pagename=manage_application&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=25"> 25</option>
                <option <?php if($max_results==50){ echo "selected='selected'";} ?>
value="index.php?pagename=manage_application&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=50"> 50</option>
                <option <?php if($max_results==100){ echo "selected='selected'";} ?>
value="index.php?pagename=manage_application&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=100"> 100</option>
              </select></td>
          </tr>
          <?php
	$rs = $s->getData_without_condition('tbl_application');
	if($order_by == '')
	{
		$order_by = 'application_id';	
	}
	$rs = $s->getData_withPages('tbl_application',$order_by, $order,$from,$max_results);
	if($_REQUEST["order"] == 'asc')
	{
		$order = 'desc';
	}
	else if($_REQUEST["order"] == 'desc')
	{
		$order = 'asc';
	}
	else
	{
		$order = 'asc';
	}
	if(mysqli_num_rows($rs)== 0 && $page != 1)
	{
		$page=1;
		$s->pageLocation("index.php?pagename=manage_application&orderby=application_name&order=$order&pageno=$page&records=$max_results"); 
	}
	$i=1;
	if(mysqli_num_rows($rs)!=0)
	{
?>
          <tr class="head">
            <td width="5%" align="center">ID</td>
            <td width="15%"><a href="index.php?pagename=manage_application&orderby=application_name&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=<?php echo $max_results;?>"> Name</a></td>
            <td width="10%" align="center" ><a href="index.php?pagename=manage_application&orderby=sort_order&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=<?php echo $max_results;?>">Display Order</a></td>
            <td width="15%" align="center">Status</td>
            <td width="15%" align="center">Add Products</td>
            <td width="15%" align="center">Action</td>
          </tr>
          <?php
	while($row = mysqli_fetch_object($rs))
	{
?>
          <tr  class="text" onmouseover="bgr_color(this, '#FFFF99')" onmouseout="bgr_color(this, '')" bgcolor="#660099">
            <td align="center"><?php echo $row->application_id;?></td>
            <td><span style="color:#FF0000"><?php echo stripslashes($row->application_name);?></span> <span style="color:#333"><?php echo stripslashes($row->application_name2);?></span></td>
            <td  align="center" nowrap><?php echo stripslashes($row->sort_order);?></td>
            <td  align="center" nowrap><?php 
	if($row->application_status =="active")
	{
?>
              <img src="images/green.gif" title="Active" border="0" alt=""  /> &nbsp; &nbsp; <a href="index.php?pagename=manage_application&action=ChangeStatus&pcode=<?php echo $row->application_id;?>&pageno=<?php echo $page;?>&records=<?php echo $max_results;?>"><img src="images/red_light.gif" title="Inactive" border="0"  /></a>
              <?php
	}
	else if($row->application_status =="inactive")
	{
?>
              <a href="index.php?pagename=manage_application&action=ChangeStatus&pcode=<?php echo $row->application_id;?>&pageno=<?php echo $page;?>&records=<?php echo $max_results;?>"><img src="images/green_light.gif" title="Active" border="0"  /></a> &nbsp; &nbsp; <img src="images/red.gif" title="Inactive" border="0"  />
              <?php		
	}
?></td>
            <td align="center"><input type="button" value="Add Products" onclick="window.location.href='index.php?pagename=g2_index_matching_add_pro&activeOP=group&action=edit&pcode=<?php echo $row->application_id;?>'" class="inputton" /></td>
            <td align="center">&nbsp; &nbsp;<a href="index.php?pagename=add_application&action=edit&pcode=<?php echo $row->application_id;?>"><img src="images/e.gif"  border="0"  alt="Edit"/></a> &nbsp; &nbsp;
              <?php
	  if($_SESSION["AdminLoginID_SET"]=='4' || $_SESSION["AdminLoginID_SET"]=='32')
				  {
				  ?>
              <a href="index.php?pagename=manage_application&action=delete&pcode=<?php echo $row->application_id;?>" onclick='return del();'><img src="images/x.gif"  border="0"  alt="Edit"/></a>
              <?php }?></td>
          </tr>
          <?php 
		$i++;
	}
?>
          <tr class='head headLink'>
            <td colspan="6" align="right" nowrap="nowrap"><table width="100%">
                <tr>
                  <td  nowrap="nowrap" align="right"><?php						
		 	$total_pages = $s->getTotal_pages('tbl_application',$order_by, $order,$max_results );
			if($page > 1)
			{ 
				$prev = ($page - 1); 
			echo "<ul><li><a href='index.php?pagename=manage_application&orderby=$order_by&order=$order&pageno=$prev&records=$max_results'>< Previous</a></li></ul>"; 
			} 
?></td>
                  <td width="43" nowrap="nowrap" align="center"><select name="pages_select" onchange="OnSelectPages();"  >
                      <?php
			for($i = 1; $i <= $total_pages; $i++)
			{ 
?>
                      <option <?php if($page==$i){ echo "selected='selected'";} ?> 
value="index.php?pagename=manage_application&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $i;?>&records=<?php echo $max_results;?>"><?php echo $i;?></option>
                      <?php
			} 
?>
                    </select></td>
                  <td width="59" nowrap="nowrap" align="left"><?php		
			if($page < $total_pages)
			{ 
				$next = ($page + 1); 
				echo "<ul><li><a href='index.php?pagename=manage_application&orderby=$order_by&order=$order&pageno=$next&records=$max_results'>Next ></a></li></ul>";
			} 
			 
?></td>
                </tr>
              </table></td>
          </tr>
          <?php
}
else
{
?>
          <tr class='text'>
            <td colspan='6' class='redstar'>&nbsp; No record present in database</td>
          </tr>
          <?php 
}
?>
        </table></td>
    </tr>
  </table>
</form>
