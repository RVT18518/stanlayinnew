
<link href="css/style.css" rel="stylesheet" type="text/css" />
<?php
	$data_action = $_REQUEST['action'];
	$pcode		 = $_REQUEST["pcode"];
	$result	     = $_REQUEST["result"]; 
	$order		= $_REQUEST["order"];
	$order_by	= $_REQUEST["orderby"];
	if(strlen(trim($order))<=0)
	{
		$order		= 'asc';
	}
	if(strlen(trim($order_by))<=0)
	{
		$order_by	= 'video_id';
	}
	
	if(!isset($_GET['pageno']))
	{ 
    	$page = 1; 
	} 
	else 
	{ 
    	$page = $_GET['pageno']; 
	} 
	if(!isset($_GET['records']))
	{ 
    	$max_results = 10; 
	} 
	else
	{ 
    	$max_results = $_GET['records']; 
	} 
	$from = (($page * $max_results) - $max_results);  
	
	if($_REQUEST['action']=='update' || $_REQUEST['action']=='insert')
	{
		$file_array["video_title"]		= addslashes($_REQUEST["video_title"]);
		$file_array["date"]				= $_REQUEST["date"];
		$file_array["sort_order"]		= addslashes($_REQUEST["sort_order"]);
		$file_array["meta_content"]		= addslashes($_REQUEST["meta_content"]);
		$file_array["meta_desc"]		= addslashes($_REQUEST["meta_desc"]);
		$file_array["sort_description"]	= addslashes($_REQUEST["sort_description"]);
		$file_array["description"]		= addslashes($_REQUEST["description"]);
		$file_array["emb_video"]		= addslashes($_REQUEST["emb_video"]);		
		////////////////////////////////////////
		if($_FILES["image_path"]["name"] != "")
		{
			$imagePath = $s->ImageUpload("uploads/videos/video_related_small_images/", "image_path","VIDEO_Small_", "100","80");
			if($imagePath != -1)
			{
				$file_array["image_path"] = $imagePath;
			}
		}
		/////////////////////////////////////////

		////////////////////////////////////////
		if($_FILES["video_path"]["name"] != "")
		{
			$filePath = $s->fileUpload("uploads/videos/", "video_path", "VIDEO_");
			if($filePath != -1)
			{
				$file_array["video_path"] = $filePath;
			}			
		}
		/////////////////////////////////////////

		$file_array["status"] 	= $_REQUEST["status"];
	}
	if($_REQUEST['action']=='ChangeStatus')
	{
		$rs_status = $s->getData_with_condition('tbl_corporate_videos','video_id',$pcode);
		if(mysqli_num_rows($rs_status)>0)
		{
			$row_status = mysqli_fetch_object($rs_status);
			if($row_status->status == 'active')
			{
				$fileArray["status"] = 'inactive';
			}
			else if($row_status->status == 'inactive')
			{
				$fileArray["status"] = 'active';
			}
			$result      = $s->editRecord('tbl_corporate_videos',$fileArray,'video_id',$pcode);
			$s->pageLocation("index.php?pagename=manage_corporate_videos&action=ChangeStatusDone&result=$result"); 
		}
	}
	if($_REQUEST['action']=='update')
	{
		$result = $s->editRecord('tbl_corporate_videos',$file_array,'video_id',$pcode);
		$data_action = "updateDone";
		$s->pageLocation("index.php?pagename=manage_corporate_videos&action=$data_action&pcode=$pcode&result=$result"); 
	}
	if($_REQUEST['action']=='insert')
	{
		$file_array['date'] = date("Y-m-d") ;
		$result = $s->insertRecord('tbl_corporate_videos',$file_array);
		$data_action = "insertDone";
		$s->pageLocation("index.php?pagename=manage_corporate_videos&action=$data_action&pcode=$pcode&result=$result"); 
	}
?>
<script type="text/javascript">
function OnSelect() 
{
	window.location = document.frx1.records.value;
}
function OnSelectPages()
{
	window.location = document.frx1.pages_select.value;
}
</script>
<form name="frx1" id="frx1" action="#" method="post">
<table width="100%" align="center" cellpadding="0" cellspacing="0" class="pagecontent">
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="27%" class="pageheadTop">Video Manager</td>
					<td width="73%" class="headLink"><ul><li><a href="index.php?pagename=add_corporate_videos&action=add_new">Add New Video </a></li></ul></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr><td class="pHeadLine"></td></tr>
	<tr><td height="21">
<?php 
		if($_REQUEST['action']=='ChangeStatusDone')
		{
			if($result==0)
			{
				echo "<p class='success'>Status Change Successfully</p><br />";	
			}
			else if($result==1)
			{
				echo "<p class='error'>Status Changing Fails</p><br />";	
			}
		}	
		if($data_action=='delete')
		{
			$result = $s->delete_table_withCondition('tbl_corporate_videos','video_id',$pcode);	
			if($result)
			{
				echo "<p class='success'>".record_delete."</p><br />";	
			}
			else 
			{
				echo "<p class='error'>".record_not_delete."</p><br />";	
			}
		}
		if($_REQUEST['action']=='updateDone')
		{
			 if($result==0)
			{
				echo "<p class='success'>".record_update."</p><br />";	
			}
			else if($result==1)
			{
				echo "<p class='error'>".record_not_update."</p><br />";	
			}
		}
		else if($_REQUEST['action']=='insertDone')
		{
			 if($result==0)
			{
				echo "<p class='success'>".record_added."</p><br />";	
			}
			else if($result==1)
			{
				echo "<p class='error'>".record_not_added."</p><br />";	
			}
			
		}
	?></td>
	</tr>
	<tr>
		<td width="100%">
			<table width="100%" cellpadding="0" cellspacing="0" class="tblBorder">
				<tr class="pagehead">
					<td colspan="5" class="pad" nowrap="nowrap" > Videos Details </td>
					<td align="center" nowrap="nowrap">Records View  &nbsp; 
						<select name="records" onchange="OnSelect();">
							<option <?php if($max_results==10){ echo "selected='selected'";} ?>	value="index.php?pagename=manage_corporate_videos&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=10">10</option>
							<option <?php if($max_results==30){ echo "selected='selected'";} ?> value="index.php?pagename=manage_corporate_videos&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=30">30</option>
          <option <?php if($max_results==50){ echo "selected='selected'";} ?>
value="index.php?pagename=manage_corporate_videos&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=50">50</option>
          <option <?php if($max_results==100){ echo "selected='selected'";} ?>
value="index.php?pagename=manage_corporate_videos&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=100">100</option>
        </select> </td>
		  </tr>
<?php
	$searchRecord = " 1 = 1 ";
	$rs = $s->getData_withPages('tbl_corporate_videos',$order_by, $order,$from,$max_results, $searchRecord);
	
	if(mysqli_num_rows($rs)==0 && $page!=1)
	{
		$page=1;
		$s->pageLocation("index.php?pagename=manage_corporate_videos&orderby=video_id&order=$order&pageno=$page&records=$max_results"); 
	}
	
	if($order == "asc")
	{
		$order_new = "desc";
	}
	else if($order == "desc")
	{
		$order_new = "asc";
	}
	
	$i=1;
	if(mysqli_num_rows($rs)!=0)
	{		
?>		
		<tr class="head">
		  <td width="7%" align="center" ><a href="index.php?pagename=manage_corporate_videos&orderby=video_id&order=<?php echo $order_new; ?>&pageno=<?php echo $page;?>&records=<?php echo $max_results;?>">ID</a></td>
		  <td width="40%" class="pad"> <a href="index.php?pagename=manage_corporate_videos&orderby=video_title&order=<?php echo $order_new; ?>&pageno=<?php echo $page;?>&records=<?php echo $max_results;?>">Video Title</a></td>
		  <td width="12%" class="pad"> <!--Date--></td>
		  <td width="15%" class="pad" align="center">Download</td>
		  <td width="11%" align="center" > Status		  </td>
		  <td width="15%"  ><div align="center">Action</div></td></tr>
<?php
		while($row = mysqli_fetch_object($rs))
		{
?>
		<tr class="text" onmouseover="bgr_color(this, '#EAB9BA')" onMouseOut="bgr_color(this, '')">
		<td align="center" valign="middle"><?php echo $row->video_id; ?></Td>
		<td class="pad" valign="middle"><?php echo ucfirst(stripslashes($row->video_title)) ; ?></td>
		<td class="pad" valign="middle"><?php /*?><?php echo $s->getDateformate($row->date,"mdy",'dmy', "-", "-") ; ?><?php */?></td>
		<td class="pad" valign="middle" align="center"><?php if($row->video_path != ""){?>
		<a href="download.php?action=downloadfile&file=<?php echo "../".$row->video_path;?>">Download Video</a> <?php }?></td>
		<td align="center" valign="middle">
<?php 
	if($row->status == "active")
	{
?>
<img src="images/green.gif" title="Active" border="0"  /> &nbsp; &nbsp;
<a href="index.php?pagename=manage_corporate_videos&action=ChangeStatus&pcode=<?php echo $row->video_id;?>&pageno=<?php echo $page;?>&records=<?php echo $max_results;?>"><img src="images/red_light.gif" title="Inactive" border="0"  /></a>
<?php
	}
	else if($row->status == "inactive")
	{
?>
<a href="index.php?pagename=manage_corporate_videos&action=ChangeStatus&pcode=<?php echo $row->video_id;?>&pageno=<?php echo $page;?>&records=<?php echo $max_results;?>"><img src="images/green_light.gif" title="Active" border="0"  /></a> &nbsp; &nbsp;
<img src="images/red.gif" title="Inactive" border="0"  />
<?php		
	}
?>		</td>
		<td align="center" valign="middle">
		  <div align="center"><a href="index.php?pagename=add_corporate_videos&action=edit&pcode=<?php echo $row->video_id;?>">
	      <img src="images/e.gif" border="0"  alt="Edit"/></a> &nbsp; &nbsp; 
	   <a href="index.php?pagename=manage_corporate_videos&action=delete&pcode=<?php echo $row->video_id;?>" onclick='return del();'>
          <img src="images/x.gif" border="0" alt="Delete" /></a>		  </div></td></tr>
<?php 
			}
?>
<tr class='head headLink'><td colspan="6" align="right" nowrap="nowrap">
<table width="100%">
<tr><td  nowrap="nowrap">
  <div align="right">
<?php						
	$total_pages = $s->getTotal_pages('tbl_corporate_videos', $order_by, $order, $max_results, $searchRecord);
	if($page > 1)
	{ 
		$prev = ($page - 1); 
		echo "<ul><li><a href='index.php?pagename=manage_corporate_videos&orderby=$order_by&order=$order&pageno=$prev&records=$max_results'> Previous</a></li></ul>"; 
	} 
?>
  </div></td>
<td width="43" nowrap="nowrap" align="center">
			  <select name="pages_select" onchange="OnSelectPages();">
<?php
	for($i = 1; $i <= $total_pages; $i++)
	{ 
?>
<option <?php if($page==$i){ echo "selected='selected'";} ?> 
value="index.php?pagename=manage_corporate_videos&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $i;?>&records=<?php echo $max_results;?>">
<?php echo $i;?></option>
<?php
	} 
?>
              </select>			  </td><td width="59" nowrap="nowrap" >
              <div align="left">
<?php		
			if($page < $total_pages)
			{ 
				$next = ($page + 1); 
				echo "<ul><li><a href='index.php?pagename=manage_corporate_videos&orderby=$order_by&order=$order&pageno=$next&records=$max_results'>Next </a></li></ul>";
			} 
?>
              </div></td>
</tr></table>
</td></tr>
<?php 
		}
		else
		{	
?>	
<tr class='text'><td colspan='8' class='redstar'> &nbsp; No record present in database.</td></tr> 
<?php
		}
?>	
	</table>
		</td></tr>
</table>
</form>
