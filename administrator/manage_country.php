<!-- Author  Rupesh kumar	 Date: 16-12-2008 -->
<link href="css/style.css" rel="stylesheet" type="text/css" />
<?php

 if($_REQUEST['msg']=='del')
{
 $s->delete_table_withCondition('tbl_country', 'country_id', $_REQUEST['id']);

	if($x)
	{ 	$_SESSION['msg']= record_delete ; }
	else
	{ 	$_SESSION['msg']= record_delete ; }
} 

// This will check the status of country and change its status
else if($_REQUEST['msg']=='stat')
{

	if($_REQUEST['status'] == 'active')
	{
	$_REQUEST['status'] = "Inactive";
	}
	else if($_REQUEST['status'] == 'inactive')
	{
	$_REQUEST['status'] = "Active";
	}

	$dataArray['country_status']= $_REQUEST['status'];
	$x =  $s->editRecord('tbl_country', $dataArray ,  'country_id', $_REQUEST['id']);
	
	if($x == 0)
	{ 	$_SESSION['msg']= record_update ; }
	else
	{ 	$_SESSION['msg']= record_not_update ; }

}


?>
<form name="frx1" id="frx1" action="" method="post">
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td colspan="2">
			  
			</td>
          </tr>
        <tr>
          <td width="24%" class="pageheadTop">Country Manager</td>
          <td width="76%" class="headLink"><ul>
              <li><a href="index.php?pagename=add_country">Add New Country</a></li>
          </ul></td>
        </tr>
    </table></td>
  </tr>  
  
  <tr>
    <td class="pHeadLine"></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  
<tr><td valign="top" class="pagecontent">

<?php
				if($_SESSION['msg'] != "")
				{
					if ($_SESSION['msg'] == record_not_delete)
					{
						echo "<p class='error'>".$_SESSION['msg']."</p><br />";
				  
					}else if ($_SESSION['msg'] == record_delete){
						echo "<p class='success'>".$_SESSION['msg']."</p><br />";
				  
					}	 
				}
				if($_REQUEST['msg'] == 'edit')
				{
				echo "<p class='success'>".record_update."</p><br />";
				}
				else if($_REQUEST['msg'] == 'add')
				{
				echo "<p class='success'>".record_added."</p><br />";
				} 
				else if ($_SESSION['msg'] == record_not_update)
				{
					echo "<p class='error'>".$_SESSION['msg']."</p><br />";
			  
				}else if ($_SESSION['msg'] == record_update){
					echo "<p class='success'>".$_SESSION['msg']."</p><br />";
			  
				}	 
				
	?>
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="tblBorder">
  <tr>
    <td colspan="6" class="pagehead">Manage Country</td>
  </tr>
  <tr class="head">
    <td width="12%" align="center">ID</td>
    <td width="32%"  > 
	<?php
	if($_REQUEST['action'] == 'desc')
	{
		echo "<a href='index.php?pagename=manage_country&action=asc&fn=country_name'>Country asc</a>";
	} else if($_REQUEST['action'] == 'asc')
	{
	 	echo "<a href='index.php?pagename=manage_country&action=desc&fn=country_name'>Country des</a>";
	}else
	{
	echo "<a href='index.php?pagename=manage_country&action=asc&fn=country_name'>Country</a>";
	
	}
	?>
	
	
	</td>
    <td width="12%">
	<?php
		if($_REQUEST['action'] == 'desc')
		{
			echo "<a href='index.php?pagename=manage_country&action=asc&fn=country_code2'>ISO Code (2 char) asc</a>";
		} else if($_REQUEST['action'] == 'asc')
		{
			echo "<a href='index.php?pagename=manage_country&action=desc&fn=country_code2'>ISO Code (2 char) des</a>";
		}else
		{
		echo "<a href='index.php?pagename=manage_country&action=asc&fn=country_code2'>ISO Code (2 char)</a>";
		}
	?>
	</td>
    <td width="11%">
	<?php
		if($_REQUEST['action'] == 'desc')
		{
			echo "<a href='index.php?pagename=manage_country&action=asc&fn=country_code3'>ISO Code (3 char) asc</a>";
		} else if($_REQUEST['action'] == 'asc')
		{
			echo "<a href='index.php?pagename=manage_country&action=desc&fn=country_code3'>ISO Code (3 char) des</a>";
		}else
		{
		echo "<a href='index.php?pagename=manage_country&action=asc&fn=country_code3'>ISO Code (3 char)</a>";
		}
	?>
	</td>
    <td width="16%"  align="center">
	
	<?php
		if($_REQUEST['action'] == 'desc')
		{
			echo "<a href='index.php?pagename=manage_country&action=asc&fn=country_status'>Status asc</a>";
		} else if($_REQUEST['action'] == 'asc')
		{
			echo "<a href='index.php?pagename=manage_country&action=desc&fn=country_status'>Status des</a>";
		}else
		{
		echo "<a href='index.php?pagename=manage_country&action=asc&fn=country_status'>Status</a>";
		}
	?>
	
	</td>
    <td width="17%"  align="center">Action</td>
  </tr>
<?php
	
	$i=1;
//  Sorting the data
	if( $_REQUEST['action'] )
	{
		$country_query = $s->getData_with_sorting(tbl_country  , $_REQUEST['fn'], $_REQUEST['action']);
		
	}else 
	{
		$country_query = $s->getData_without_condition(tbl_country);
	}
	while($result = $s->getNextRow($country_query))
	{
?>
  <tr class="text" onmouseover="bgr_color(this, '#EAB9BA')" onmouseout="bgr_color(this, '')">
    <td align="center"><?php echo $result['country_id']; ?></td>
    <td nowrap><?php echo $result['country_name']; ?></td>
    <td><?php echo $result['country_code2']; ?></td>
    <td><?php echo $result['country_code3']; ?></td>
    <td  align="center">
<?php 
	if($result['country_status'] == "active")
	{
?>
<img src="images/green.gif" title="Active" border="0"  /> &nbsp; &nbsp;
<a href="index.php?pagename=manage_country&msg=stat&status=<?php  echo $result['country_status']; ?>&id=<?php  echo $result['country_id']; ?>"  ><img src="images/red_light.gif" title="Inactive" border="0"  /></a>
<?php
	}
	else if($result['country_status'] == "inactive")
	{
?>
<a href="index.php?pagename=manage_country&msg=stat&status=<?php  echo $result['country_status']; ?>&id=<?php  echo $result['country_id']; ?>"  ><img src="images/green_light.gif" title="Active" border="0"  /></a> &nbsp; &nbsp;
<img src="images/red.gif" title="Inactive" border="0"  />
<?php		
	}
?>	
	</td>
    <td  align="center"><a href="index.php?pagename=add_country&action=edit&id=<?php echo $result['country_id']; ?>">
	<img src="images/e.gif"  border="0"  title="Edit"/></a> &nbsp; &nbsp;  
	<a   href="index.php?pagename=manage_country&msg=del&id=<?php  echo $result['country_id']; ?>"  onclick="return del();">
	<img src="images/x.gif"  border="0" title="Delete" /></a> 
	</td>
  </tr>
 <?php  $i++;}?> 

	
	
</table>
</td>
</tr>
</table>
</form>
<br />

