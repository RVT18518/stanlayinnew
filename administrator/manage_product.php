<?php
	$data_action = $_REQUEST['action'];
	$pcode		 = $_REQUEST["pcode"];
	$order		 = $_REQUEST["order"];
	$order_by	 = $_REQUEST["orderby"];

	if(strlen(trim($order))<=0)
	{
	$order 	 	 = 'asc';
	}
	if(strlen(trim($order_by))<=0)
	{
	$order_by 	 = 'pro_title';
	}	
	$result	     = $_REQUEST["result"]; 
	if(!isset($_GET['pageno']))
	{ 
    	$page = 1; 
	} 
	else 
	{ 
    	$page = $_GET['pageno']; 
	} 
	if(!isset($_GET['records']))
	{ 
    	$max_results = 500; 
	} 
	else 
	{ 
    	$max_results = $_GET['records']; 
	} 
	$from = (($page * $max_results) - $max_results);  
	
	if($_REQUEST['action']=='update' ||$_REQUEST['action']=='insert')
	{
		$cat_parent_id					= $s->FieldValue("tbl_category", "parent_id", "cate_id = ".$_REQUEST['category_id']);
		if($cat_parent_id == 0)
		{
			$s->pageLocation("index.php?pagename=manage_product&cat=parent"); 
		}
		else
		{
			$file_array["cate_id"]  	= $_REQUEST["category_id"];	
			$file_array["pro_title"]  	= addslashes($_REQUEST["pro_title"]);	
			//$file_array["seo_title"]	= addslashes($_REQUEST["seo_title"]);
			
			if(strlen(trim($_REQUEST["pro_permalink"]))>0 )
			{
			$file_array["pro_permalink"]= addslashes(str_replace("'","",str_replace("/","_",$_REQUEST["pro_permalink"])));
			}
			else
			{
			$file_array["pro_permalink"]= addslashes(str_replace("'","",str_replace("/","_",$_REQUEST["pro_title"])));
			}
			$file_array["meta_content"] 	= addslashes($_REQUEST["meta_content"]);
			$file_array["meta_desc"]		= addslashes($_REQUEST["meta_desc"]);
			$file_array["pro_details"]		= addslashes($_REQUEST["pro_details"]);
			$file_array["pro_short_desc"] 	= addslashes($_REQUEST["pro_short_desc"]);
			$file_array["applications"]  	= addslashes($_REQUEST["applications"]);
			$file_array["specification"]  	= addslashes($_REQUEST["specification"]);
			$file_array["transducer_selection"]  	= addslashes($_REQUEST["transducer_selection"]);
			
			
			$file_array["features"]  		= addslashes($_REQUEST["features"]);
			$file_array["features_details"] = addslashes($_REQUEST["features_details"]);
			
			$file_array["accessories"]  	= addslashes($_REQUEST["accessories"]);
			$file_array["measures"]  		= addslashes($_REQUEST["measures"]);
			$file_array["gen_char"]  		= addslashes($_REQUEST["gen_char"]);
		
		
		if($_FILES["pro_tech_specs"]["name"] != "")
			{
				$filePath = $s->fileUpload("uploads/products/technical_specs/", "pro_tech_specs", "TECH_");
				if($filePath != -1)
				{
					$file_array["pro_tech_specs"] = $filePath;
				}			
			}
			////////////////////////////////////////
			/*
			///////////////////////////////////////
			if($_FILES["pro_literature"]["name"] != "")
			{
				$filePath = $s->fileUpload("uploads/products/literature/", "pro_literature", "LITER_");
				if($filePath != -1)
				{
					$file_array["pro_literature"] = $filePath;
				}			
			}
			////////////////////////////////////////
			if($_FILES["pro_eng_manual"]["name"] != "")
			{
				$filePath = $s->fileUpload("uploads/products/eng_manual/", "pro_eng_manual", "MANU_");
				if($filePath != -1)
				{
					$file_array["pro_eng_manual"] = $filePath;
				}			
			}*/
			////////////////////////////////////////////
			if($_FILES["pro_image"]["name"] != "")
			{
				$img_result_large = $s->fileUpload('uploads/pro_image/banner_image/','pro_image','PROIMG_');		
				if($img_result_large != -1)
				{
					$file_array['pro_image']  	=  $img_result_large;
				}
			}
			
			
				if($_FILES["pro_image_small"]["name"] != "")
			{
				$img_result_large_small = $s->fileUpload('uploads/pro_image/featured_image/','pro_image_small','PROIMG_small_');		
				if($img_result_large_small != -1)
				{
					$file_array['pro_image_small']  	=  $img_result_large_small;
				}
			}
			
			/////////////////////////////////////////
			$file_array["product_url"] 	= $_REQUEST["product_url"];
			$file_array["sort_order"]  	= $_REQUEST["sort_order"];
			$file_array["status"] 		= $_REQUEST["status"];
		}
	}
	if($_REQUEST['action']=='ChangeStatus')
	{
		$rs_status = $s->getData_with_condition('tbl_products_1','pro_id',$pcode);
		if(mysqli_num_rows($rs_status)>0)
		{
			$row_status = mysqli_fetch_object($rs_status);
			if($row_status->status == 'active')
			{
				$fileArray["status"] = 'inactive';
			}
			else if($row_status->status == 'inactive')
			{
				$fileArray["status"] = 'active';
			}
		
			$result      = $s->editRecord('tbl_products_1',$fileArray,'pro_id',$pcode);
			$s->pageLocation("index.php?pagename=manage_product&action=ChangeStatusDone&result=$result");
		}
	}
	if($_REQUEST['action']=='update')
	{
		$result = $s->editRecord('tbl_products_1',$file_array,'pro_id',$pcode);
		$data_action = "updateDone";
		$s->pageLocation("index.php?pagename=manage_product&action=$data_action&pcode=$pcode&result=$result"); 
	}
	
	
	
	if($_REQUEST['action']=='save_sort_order')
	{	
	/*echo "<pre>";	
	print_r($_REQUEST);
	*/

	$sort_order_ctr=count($_REQUEST["sort_order"]);
//echo	$_REQUEST["cate_id"];
for($s=0; $s<$sort_order_ctr; $s++)
{
$sort_pro_id=$_REQUEST["pro_id"][$s];
$sort_order=$_REQUEST["sort_order"][$s];

$file_sort_array["sort_order"] 	= $_REQUEST["sort_order"][$s];

$update_sort_order_sql="UPDATE tbl_products_1 set sort_order='$sort_order' where pro_id='$sort_pro_id'";
$result=mysqli_query($GLOBALS["___mysqli_ston"],$update_sort_order_sql); //exit;
//echo $result = $s->editRecord('tbl_products_1',$file_sort_array,'pro_id',$_REQUEST["pro_id"][$s]);
}
$data_action = "updateDone";?>
<script>window.location = '<?php echo "index.php?pagename=manage_product&action=updateDone&cate_id=".$_REQUEST["cate_id"]."&result=0";?>';
</script>
<?php
	}
	
	if($_REQUEST['action']=='insert')
	{
		$result = $s->insertRecord('tbl_products_1',$file_array);
		$pcode=mysqli_insert_id(); //exit;
		$data_action = "insertDone";
		
		/*$sql			= "select max(pro_id) as pro_id from tbl_products_1 where status = 'active' and deleteflag = 'active'";
		$rs_add_pro		= mysqli_query($GLOBALS["___mysqli_ston"],$sql);
		if(mysqli_num_rows($rs_add_pro)>0)
		{
			$row_add_pro	= mysqli_fetch_object($rs_add_pro);
			$pcode			= $row_add_pro->pro_id;*/
			$s->pageLocation("index.php?pagename=add_general_prodcuts&subpagename=product_general_detail&action=edit&pcode=$pcode"); 
		//}
	}
?>
<script type="text/javascript">
function OnSelect() 
{
	window.location = document.frx1.records.value;
}
function OnSelectPages()
{
	window.location = document.frx1.pages_select.value;
}
</script>

<table width="100%" align="center" cellpadding="0" cellspacing="0" class="pagecontent">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="40%" class="pageheadTop">Product Manager</td>
          <td width="60%" class="headLink"><ul>
              <li><a href="index.php?pagename=add_product&action=add_new">Add New Product</a></li>
            </ul></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td class="pHeadLine"></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><?php 
		if($_REQUEST['action']=='ChangeStatusDone')
		{
			if($result==0)
			{
				echo "<p class='success'>Status Change Successfully</p><br />";	
			}
			else if($result==1)
			{
				echo "<p class='error'>Status Changing Fails</p><br />";	
			}
		}	
		if($data_action=='delete')
		{
			$result = $s->delete_table_withCondition('tbl_products_1','pro_id',$pcode);	
			if($result)
			{
				echo "<p class='success'>".record_delete."</p><br />";	
			}
			else 
			{
				echo "<p class='error'>".record_not_delete."</p><br />";	
			}
		}
		if($_REQUEST['action']=='updateDone')
		{
			 if($result==0)
			{
				echo "<p class='success'>".record_update."</p><br />";	
			}
			else if($result==1)
			{
				echo "<p class='error'>".record_not_update."</p><br />";	
			}
		}
		else if($_REQUEST['action']=='insertDone')
		{
			if($result==0)
			{
				echo "<p class='success'>".record_added."</p><br />";	
			}
			else if($result==1)
			{
				echo "<p class='error'>".record_not_added."</p><br />";	
			}
		}
		if($_REQUEST["cat"] == "parent")
		{
			echo "<p class='error'>"."Please select sub category"."</p><br />";	
		}
	?></td>
  </tr>
  <tr>
    <td width="100%"><table width="100%" cellpadding="5" cellspacing="0" class="tblBorder">
        <tr class="pagehead">
          <td colspan="3" class="pad" nowrap="nowrap" > Product Details </td>
          <td align="right" nowrap="nowrap">&nbsp;</td>
          <td align="right" nowrap="nowrap"><form name="frx1" id="frx1" action="#" method="post">
              Select Category  &nbsp;
              <?php
                $sql_cateid="select cate_id,cate_name from tbl_category where parent_id!='0' order by cate_name";
                $rs_cateid=mysqli_query($GLOBALS["___mysqli_ston"],$sql_cateid);
				?>
              <select name="cate_id" onchange="this.form.submit();"  >
                <option value="">Select Category</option>
                <?php
                while($row_cateid=mysqli_fetch_array($rs_cateid))
                  {	?>
<option value="<?php echo $row_cateid['cate_id']; ?>" <?php if($row_cateid['cate_id']==$_REQUEST['cate_id']) { echo "selected"; } ?> ><?php echo $row_cateid['cate_name']; ?></option>
                <?php }?>
              </select>
            </form></td>
        </tr>
        <?php
	$searchRecord = " 1 = 1 ";
	//$rs = $s->getData_withPages('tbl_products_1',$order_by, $order,$from,$max_results, $searchRecord);
	$table_name	= "tbl_products_1 ";
	$search		= "where tbl_products_1.deleteflag = 'active' AND status='active' AND cate_id='".$_REQUEST['cate_id']."'";
	$star		= "tbl_products_1.*";
	
	$rs 		= $s->getData_withPagesJoin($table_name, $order_by, $order, $from, $max_results, $search, $star);	
	if(mysqli_num_rows($rs)==0 && $page!=1)
	{
		$page=1;
		$s->pageLocation("index.php?pagename=manage_product&orderby=pro_id&order=$order&pageno=$page&records=$max_results"); 
	}
	
	if($order == "asc")
	{
		$order_new = "desc";
	}
	else if($order == "desc")
	{
		$order_new = "asc";
	}
	
	$i=1;
	
	$search		= "select tbl_products_1.pro_id,tbl_products_1.sort_order,tbl_products_1.pro_title, tbl_products_1.status from tbl_products_1 inner join tbl_products_categories on tbl_products_categories.pro_id=tbl_products_1.pro_id where tbl_products_1.deleteflag = 'active' AND tbl_products_categories.cate_id='".$_REQUEST['cate_id']."' group by tbl_products_1.pro_id order by tbl_products_1.sort_order asc";
	
	if($_REQUEST['cate_id']=='')
	    {
			$search		= "select tbl_products_1.pro_id,tbl_products_1.sort_order,tbl_products_1.pro_title, tbl_products_1.status from tbl_products_1 where tbl_products_1.deleteflag = 'active' group by tbl_products_1.pro_id order by tbl_products_1.pro_id desc limit 0,15";
		}
	
//	echo $search;
	$rs=mysqli_query($GLOBALS["___mysqli_ston"],$search);
	if(mysqli_num_rows($rs)!=0)
	{
?>
        <form name="save_sort_order" id="save_sort_order" action="index.php?pagename=manage_product&action=save_sort_order"  method="post">
          <tr class="head">
            <td width="12%" align="center" ><a href="index.php?pagename=manage_product&orderby=pro_id&order=<?php echo $order_new; ?>&pageno=<?php echo $page;?>&records=<?php echo $max_results;?>">ID</a></Td>
            <td width="23%" class="pad"><a href="index.php?pagename=manage_product&orderby=pro_title&order=<?php echo $order_new; ?>&pageno=<?php echo $page;?>&records=<?php echo $max_results;?>">Product Name </a></td>
            <td width="9%" align="center">Status</td>
            <td width="18%"   align="center">Display Order</td>
            <td width="18%"   align="center">Action</td>
          </tr>
          <?php
			while($row = mysqli_fetch_object($rs))
			{
?>
          <tr class="text" onmouseover="bgr_color(this, '#EAB9BA')" onMouseOut="bgr_color(this, '')">
            <td align="center" valign="middle"><?php echo $row->pro_id; ?></Td>
            <td class="pad"    valign="middle"><?php echo ucfirst(stripslashes($row->pro_title)) ; ?></td>
            <td align="center" valign="middle"><?php  
	if($row->status == "active")
	{
?>
              <img src="images/green.gif" title="Active" border="0"  /> &nbsp; &nbsp; <a href="index.php?pagename=manage_product&action=ChangeStatus&pcode=<?php echo $row->pro_id;?>&pageno=<?php echo $page;?>&records=<?php echo $max_results;?>"><img src="images/red_light.gif" title="Inactive" border="0"  /></a>
              <?php
	}
	else if($row->status == "inactive")
	{
?>
              <a href="index.php?pagename=manage_product&action=ChangeStatus&pcode=<?php echo $row->pro_id;?>&pageno=<?php echo $page;?>&records=<?php echo $max_results;?>"><img src="images/green_light.gif" title="Active" border="0"  /></a> &nbsp; &nbsp; <img src="images/red.gif" title="Inactive" border="0"  />
              <?php		
	}
?></td>
            <td align="center" valign="middle"><input type="text" size="3" name="sort_order[]" value="<?php echo $row->sort_order;?>" />
              <input type="hidden" size="3" name="pro_id[]" value="<?php echo $row->pro_id;?>" />
              <input  type="hidden" value="<?php echo $_REQUEST['cate_id'];?>" name="cate_id" size="3" />
              <input name="save" type="submit" value="Save" class="inputton" /></td>
            <td align="center" valign="middle"><a href="index.php?pagename=add_product&action=edit&pcode=<?php echo $row->pro_id;?>"></a> <a href="index.php?pagename=add_general_prodcuts&subpagename=product_general_detail&action=edit&pcode=<?php echo $row->pro_id;?>"> <img src="images/e.gif" border="0"  alt="Edit"/></a>
              <?php
	  if($_SESSION["AdminLoginID_SET"]=='4')
				  {
				  ?>
              &nbsp; &nbsp; <a href="index.php?pagename=manage_product&action=delete&pcode=<?php echo $row->pro_id;?>" onclick='return del();'> <img src="images/x.gif" border="0" alt="Delete" /></a>
              <?php }?></td>
          </tr>
          <?php 
			}
?>
        </form>
        <tr class='head headLink'>
          <td colspan="5" align="right" nowrap="nowrap"><table width="100%" style="display:none">
              <tr>
                <td  nowrap="nowrap"  align="right"><?php
	//$total_pages = $s->getTotal_pagesJoin($table_name, $order_by, $order, $max_results, $search, $star);
	if($page > 1)
	{ 
		$prev = ($page - 1); 
		echo "<ul><li><a href='index.php?pagename=manage_product&orderby=$order_by&order=$order&pageno=$prev&records=$max_results'> Previous</a></li></ul>"; 
	} 
?></td>
                <td width="43" nowrap="nowrap" align="center"><select name="pages_select" onchange="OnSelectPages();">
                    <?php
	for($i = 1; $i <= $total_pages; $i++)
	{ 
?>
                    <option <?php if($page==$i){ echo "selected='selected'";} ?> 
value="index.php?pagename=manage_product&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $i;?>&records=<?php echo $max_results;?>"> <?php echo $i;?></option>
                    <?php
	} 
?>
                  </select></td>
                <td width="59" nowrap="nowrap" align="left"><?php		
			if($page < $total_pages)
			{ 
				$next = ($page + 1); 
				echo "<ul><li><a href='index.php?pagename=manage_product&orderby=$order_by&order=$order&pageno=$next&records=$max_results'>Next </a></li></ul>";
			} 
?></td>
              </tr>
            </table></td>
        </tr>
        <?php 
		}
		else
		{	
?>
        <tr class='text'>
          <td colspan='8' class='redstar'>&nbsp; No record present in database.</td>
        </tr>
        <?php
		}
?>
      </table></td>
  </tr>
</table>
