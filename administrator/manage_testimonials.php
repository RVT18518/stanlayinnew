<?php 
	$data_action = $_REQUEST['action'];
	$pcode		 = $_REQUEST["pcode"];
	$order 		 = $_REQUEST["order"];
	$order_by    = $_REQUEST["orderby"]; 
	if(!isset($_GET['pageno']))
	{ 
    	$page = 1; 
	} else { 
    $page = $_GET['pageno']; 
	} 
	if(!isset($_GET['records']))
	{ 
    	$max_results = 50; 
	} else { 
    $max_results = $_GET['records']; 
	} 
	$from = (($page * $max_results) - $max_results); 
	
	
	if($_REQUEST['action']=='update' ||$_REQUEST['action']=='insert')
	{
		$fileArray["testimonials_name"]        = addslashes($_REQUEST["testimonials_name"]);
		//$fileArray["testimonials_status"]      = $_REQUEST["testimonials_status"];
		$fileArray["testimonials_description"] = addslashes($_REQUEST["testimonials_description"]);
		$testimonials_added_by = "1";// use session variable for login id 
		$img_result = "";
		if($_FILES["testimonials_logo"]["name"] != "")
		{
			$large				= 843;//$s->fetchGeneral_config('largeimg');
			$medium				= 300;//$s->fetchGeneral_config('mediumimg');
			$small				= 285;//$s->fetchGeneral_config('smallimg');	
			$img_result_large   = $s->ImageUpload('uploads/testimonials_logos/large/','testimonials_logo','TSTLOGO',$large,446);	
			$img_result_medium  = $s->ImageUpload('uploads/testimonials_logos/medium/','testimonials_logo','TSTLOGO',$medium,300);	
			$img_result_small   = $s->ImageUpload('uploads/testimonials_logos/small/','testimonials_logo','TSTLOGO',$small,150);	
			if($img_result_large != -1)
			{
				$fileArray["testimonials_logo_large"]  = $img_result_large;
			}
			if($img_result_medium != -1)
			{
				$fileArray["testimonials_logo_medium"] = $img_result_medium;
			}
			if($img_result_small  != -1)
			{
				$fileArray["testimonials_logo_small"] = $img_result_small;
			}	
		}
	}
	if($_REQUEST['action'] == 'ChangeStatus')
	{
		$rs_status = $s->getData_with_condition('tbl_testimonials','testimonials_id',$pcode);
		if(mysqli_num_rows($rs_status)>0)
		{
			$row_status = mysqli_fetch_object($rs_status);
			if($row_status->testimonials_status == 'active')
			{
				$fileArray["testimonials_status"] = 'inactive';
			}
			else if($row_status->testimonials_status == 'inactive')
			{
				$fileArray["testimonials_status"] = 'active';
			}
			$result = $s->editRecord('tbl_testimonials',$fileArray,'testimonials_id',$pcode);
		}
	}
	if( $_REQUEST['action'] == 'update' )
	{
			
		if($_FILES["testimonials_logo"]["name"] !="")
		{
			$rs_del    = $s->getData_with_condition('tbl_testimonials','testimonials_id',$pcode);
			$row_del   = mysqli_fetch_object($rs_del);
			$del_image_large  = $row_del->testimonials_logo_large;
			$del_image_medium = $row_del->testimonials_logo_medium;
			$del_image_small  = $row_del->testimonials_logo_small;
			if(file_exists($del_image_large)>0)
			{
				unlink($del_image_large);
			}
			if(file_exists($del_image_medium)>0)
			{
				unlink($del_image_medium);
			}
			if(file_exists($del_image_small)>0)
			{
				unlink($del_image_small);
			}
		}
		
	
	$fileArray['login_id'] 	 	= $_SESSION["AdminLoginID_SET"];
	$fileArray['testimonials_added_by'] 	= $_SESSION["AdminLoginID_SET"];
	
//	$fileArray['tax_class_id'] 	 	= $_REQUEST['tax_class_id'];// now class id is rates id
	//$fileArray['est_arrival_min'] 	= $_REQUEST['est_arrival_min'];// now class id is rates id
	//$fileArray['est_arrival_max'] 	= $_REQUEST['est_arrival_max'];// now class id is rates id

	
	//$fileArray['pro_rap_amount'] 	= $_REQUEST['pro_rap_amount'];
		$result = $s->editRecord('tbl_testimonials',$fileArray,'testimonials_id',$pcode);
				
			
	}
	if($_REQUEST['action'] == 'insert')
	{
			if($img_result != -1 )
			{
				$fileArray['login_id'] 	 	= $_SESSION["AdminLoginID_SET"];
	$fileArray['testimonials_added_by'] 	= $_SESSION["AdminLoginID_SET"];
				$result = $s->insertRecord('tbl_testimonials' ,$fileArray);
			}
	}
?>
<script type="text/javascript">
function OnSelect() 
{
	window.location = document.frx1.records.value;
}
function OnSelectPages()
{
	window.location = document.frx1.pages_select.value;
}
</script>

<form name="frx1" id="frx1" action="#" method="post">
  <table width="100%" align="center" cellpadding="0" cellspacing="0" class="pagecontent">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="53%" class="pageheadTop">Testimonials Manager</td>
            <td width="47%" class="headLink"><ul>
                <li><a href="index.php?pagename=add_testimonials&action=add_new">Add New Testimonials</a></li>
              </ul></td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td class="pHeadLine"></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><?php
		if($_REQUEST['action']=='ChangeStatus')
		{
			if($result==0)
			{
				echo "<p class='success'>Status Change Successfully</p><br/>";	
			}
			else if($result==1)
			{
				echo "<p class='error'>Status Changing Fails</p><br/>";	
			}
			$data_action = "Changed";
		}
		if($data_action=='delete')
		{
			$rs_del    = $s->getData_with_condition('tbl_testimonials','testimonials_id',$pcode);
			$row_del   = mysqli_fetch_object($rs_del);
			$del_image_large  = $row_del->testimonials_logo_large;
			$del_image_medium = $row_del->testimonials_logo_medium;
			$del_image_small  = $row_del->testimonials_logo_small;
			$result = $s->delete_table_withCondition('tbl_testimonials','testimonials_id',$pcode);	
			if($result)
			{
				if(file_exists($del_image_large)>0)
				{
					unlink($del_image_large);
				}
				if(file_exists($del_image_medium)>0)
				{
					unlink($del_image_medium);
				}
				if(file_exists($del_image_small)>0)
				{
					unlink($del_image_small);
				}
				echo "<p class='success'>".record_delete."</p><br/>";	
			}
			else 
			{
				echo "<p class='error'>".record_not_delete."</p><br/>";	
			}
		}
		if($_REQUEST['action']=='update')
		{
			 if($result==0)
			{
				echo "<p class='success'>".record_update."</p><br/>";	
			}
			else if($result==1)
			{
				echo "<p class='error'>".record_not_update."</p><br/>";	
			}
		}
		else if($_REQUEST['action']=='insert')
		{
			 if($result==0)
			{
				echo "<p class='success'>".record_added."</p><br/>";	
			}
			else if($result==1)
			{
				echo "<p class='error'>".record_not_added."</p><br/>";	
			}
		}
?></td>
    </tr>
    <tr>
      <td width="100%"><table width="100%" cellpadding="5" cellspacing="0" class="tblBorder">
          <tr class="pagehead">
            <td colspan="2" class="pad"> Testimonials Details</td>
            <td width="15%" align="center">Records View &nbsp;
              <select name="records" onchange="OnSelect();"  >
                <option <?php if($max_results==10){ echo "selected='selected'";} ?> 
value="index.php?pagename=manage_testimonials&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=10"> 10</option>
                <option <?php if($max_results==20){ echo "selected='selected'";} ?>
value="index.php?pagename=manage_testimonials&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=20"> 20</option>
                <option <?php if($max_results==50){ echo "selected='selected'";} ?>
value="index.php?pagename=manage_testimonials&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=50"> 50</option>
                <option <?php if($max_results==100){ echo "selected='selected'";} ?>
value="index.php?pagename=manage_testimonials&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=100"> 100</option>
              </select></td>
          </tr>
          <?php
	$rs= $s->getData_without_condition('tbl_testimonials');
	if($order_by == '')
	{
		$order_by = 'testimonials_id';	
	}
	$rs = $s->getData_withPages('tbl_testimonials',$order_by, $order,$from,$max_results);
	if($_REQUEST["order"] == 'asc')
	{
		$order = 'desc';
	}
	else if($_REQUEST["order"] == 'desc')
	{
		$order = 'asc';
	}
	else
	{
		$order = 'asc';
	}
	if(mysqli_num_rows($rs)== 0 && $page != 1)
	{
		$page=1;
		$s->pageLocation("index.php?pagename=manage_testimonials&orderby=testimonials_name&order=$order&pageno=$page&records=$max_results"); 
	}
	$i=1;
	if(mysqli_num_rows($rs)!=0)
	{
?>
          <tr class="head">
            <Td width="9%" align="center">ID</Td>
            <td width="57%"><a href="index.php?pagename=manage_testimonials&orderby=testimonials_name&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=<?php echo $max_results;?>"> Testimonials Name</a></td>
            <!-- <td width="30%" align="center">
		  <a href="index.php?pagename=manage_testimonials&orderby=testimonials_status&order=<?php //echo $order;?>&pageno=<?php //echo $page;?>&records=<?php //echo $max_results;?>">
		  Status</a></td>-->
            <td align="center">Action</td>
          </tr>
          <?php
	while($row = mysqli_fetch_object($rs))
	{
?>
          <tr class="text" onmouseover="bgr_color(this, '#FFFF99')" onmouseout="bgr_color(this, '')">
            <td align="center"><?php echo $row->testimonials_id;?></td>
            <td><?php echo stripslashes($row->testimonials_name);?></td>
            <td align="center"><a href="index.php?pagename=add_testimonials&action=edit&pcode=<?php echo $row->testimonials_id;?>"><img src="images/e.gif"  border="0"  alt="Edit"/></a> &nbsp; &nbsp;
             
              <a href="index.php?pagename=manage_testimonials&action=delete&pcode=<?php echo $row->testimonials_id;?>" onclick='return del();'><img src="images/x.gif"  border="0"  alt="Edit"/></a>
             </td>
          </tr>
          <?php 
		$i++;
	}
?>
          <tr class='head headLink'>
            <td colspan="3" align="right" nowrap="nowrap"><table width="100%">
                <tr>
                  <td  nowrap="nowrap" align="right"><?php						
		 	$total_pages = $s->getTotal_pages('tbl_testimonials',$order_by, $order,$max_results );
			if($page > 1)
			{ 
				$prev = ($page - 1); 
			echo "<ul><li><a href='index.php?pagename=manage_testimonials&orderby=$order_by&order=$order&pageno=$prev&records=$max_results'>< Previous</a></li></ul>"; 
			} 
?></td>
                  <td width="43" nowrap="nowrap" align="center"><select name="pages_select" onchange="OnSelectPages();"  >
                      <?php
			for($i = 1; $i <= $total_pages; $i++)
			{ 
?>
                      <option <?php if($page==$i){ echo "selected='selected'";} ?> 
value="index.php?pagename=manage_testimonials&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $i;?>&records=<?php echo $max_results;?>"><?php echo $i;?></option>
                      <?php
			} 
?>
                    </select></td>
                  <td width="59" nowrap="nowrap" align="left"><?php		
			if($page < $total_pages)
			{ 
				$next = ($page + 1); 
				echo "<ul><li><a href='index.php?pagename=manage_testimonials&orderby=$order_by&order=$order&pageno=$next&records=$max_results'>Next ></a></li></ul>";
			} 
			 
?></td>
                </tr>
              </table></td>
          </tr>
          <?php
}
else
{
?>
          <tr class='text'>
            <td colspan='3' class='redstar'>&nbsp; No record present in database</td>
          </tr>
          <?php 
}
?>
        </table></td>
    </tr>
  </table>
</form>
