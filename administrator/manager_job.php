<?php
	$data_action = $_REQUEST['action'];
	$pcode		 = $_REQUEST["pcode"];
	$order 		 = 'asc';
	$order_by    = 'job_id';
	$result	     = $_REQUEST["result"]; 
	if(!isset($_GET['pageno']))
	{ 
    	$page = 1;
	}
	else 
	{ 
    	$page = $_GET['pageno']; 
	} 
	if(!isset($_GET['records']))
	{ 
    	$max_results = 10; 
	} 
	else 
	{ 
    	$max_results = $_GET['records']; 
	} 
	$from = (($page * $max_results) - $max_results);  
	
	if($_REQUEST['action']=='update' ||$_REQUEST['action']=='insert')
	{
		$file_array["job_code"]  		= $_REQUEST["job_code"];
		$file_array["job_title"]  		= addslashes($_REQUEST["job_title"]);
		$file_array["location"]  		= $_REQUEST["location"];
		$file_array["experience"]  		= $_REQUEST["experience"];
		$file_array["no_of_posts"]  	= $_REQUEST["no_of_posts"];
		
		if($_REQUEST["date_from"] !="" || $_REQUEST["date_from"] != "--")
		{
			$file_array["date_from"] = $s->getDateformate($_REQUEST["date_from"],"ymd","ymd");
		}
		if($_REQUEST["date_to"] !="" || $_REQUEST["date_to"] != "--")
		{
			$file_array["date_to"] = $s->getDateformate($_REQUEST["date_to"],"ymd","ymd");
		}
		$file_array["jobdescription"]   = addslashes($_REQUEST["jobdescription"]);
		$file_array["status"]  			= $_REQUEST["status"];
	}
	if($_REQUEST['action']=='ChangeStatus')
	{
		$rs_status = $s->getData_with_condition('tbl_jobs','job_id',$pcode);
		if(mysqli_num_rows($rs_status)>0)
		{
			$row_status = mysqli_fetch_object($rs_status);
			if($row_status->status == 'active')
			{
				$fileArray["status"] = 'inactive';
			}
			else if($row_status->status == 'inactive')
			{
				$fileArray["status"] = 'active';
			}
			$result      = $s->editRecord('tbl_jobs',$fileArray,'job_id',$pcode);
			$s->pageLocation("index.php?pagename=manager_job&action=ChangeStatusDone&result=$result"); 
		}
	}
	if($_REQUEST['action']=='update')
	{
		$result = $s->editRecord('tbl_jobs',$file_array,'job_id',$pcode);
		$data_action = "updateDone";
		$s->pageLocation("index.php?pagename=manager_job&action=$data_action&pcode=$pcode&result=$result"); 
	}
	if($_REQUEST['action']=='insert')
	{
		$result = $s->insertRecord('tbl_jobs',$file_array);
		$data_action = "insertDone";
		$s->pageLocation("index.php?pagename=manager_job&action=$data_action&pcode=$pcode&result=$result"); 
	}
?>	
<script type="text/javascript">
function OnSelect() 
{
	window.location = document.frx1.records.value;
}
function OnSelectPages()
{
	window.location = document.frx1.pages_select.value;
}
</script>
<form name="frx1" id="frx1" action="#" method="post">
<table width="100%" align="center" cellpadding="0" cellspacing="0" class="pagecontent">
	
	 <tr>
       <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
           <tr>
             <td width="24%" class="pageheadTop">Job Manager</td>
             <td width="76%" class="headLink"><ul>
                 <li><a href="index.php?pagename=add_jobs&action=add_new">Add New Job</a></li>
             </ul></td>
           </tr>
       </table></td>
    </tr>
	 <tr>
       <td class="pHeadLine"></td>
    </tr>
	 <tr>
       <td>&nbsp;</td>
    </tr>
	<tr><td>
<?php 
		if($_REQUEST['action']=='ChangeStatusDone')
		{
			if($result==0)
			{
				echo "<p class='success'>Status Change Successfully</p><br />";	
			}
			else if($result==1)
			{
				echo "<p class='error'>Status Changing Fails</p><br />";	
			}
		}	
		if($data_action=='delete')
		{
			$result = $s->delete_table_withCondition('tbl_jobs','job_id',$pcode);	
			if($result)
			{
				echo "<p class='success'>".record_delete."</p><br />";	
			}
			else 
			{
				echo "<p class='error'>".record_not_delete."</p><br />";	
			}
		}
		if($_REQUEST['action']=='updateDone')
		{
			 if($result==0)
			{
				echo "<p class='success'>".record_update."</p><br />";	
			}
			else if($result==1)
			{
				echo "<p class='error'>".record_not_update."</p><br />";	
			}
		}
		else if($_REQUEST['action']=='insertDone')
		{
			 if($result==0)
			{
				echo "<p class='success'>".record_added."</p><br />";	
			}
			else if($result==1)
			{
				echo "<p class='error'>".record_not_added."</p><br />";	
			}
			
		}
	?>	</td></tr>	<tr><td width="100%">
	<table width="100%" cellpadding="0" cellspacing="0" class="tblBorder">
		<tr class="pagehead">
		  <td colspan="7" class="pad" nowrap="nowrap" > Job Details </td>
		  <td align="center" nowrap="nowrap">Records View  &nbsp; 
            <select name="records" onchange="OnSelect();"  >
          <option <?php if($max_results==10){ echo "selected='selected'";} ?> 
value="index.php?pagename=manager_job&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=10">10</option>
          <option <?php if($max_results==20){ echo "selected='selected'";} ?>
value="index.php?pagename=manager_job&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=20">20</option>
          <option <?php if($max_results==50){ echo "selected='selected'";} ?>
value="index.php?pagename=manager_job&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=50">50</option>
<option <?php if($max_results==100){ echo "selected='selected'";} ?>
value="index.php?pagename=manager_job&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=100">100</option>
        </select> </td>
		  </tr>
<?php
	$searchRecord = " 1 = 1 ";
	$rs = $s->getData_withPages('tbl_jobs',$order_by, $order,$from,$max_results, $searchRecord);
	
	if(mysqli_num_rows($rs)==0 && $page!=1)
	{
		$page=1;
		$s->pageLocation("index.php?pagename=manager_job&orderby=job_id&order=$order&pageno=$page&records=$max_results"); 
	}
	$i=1;
	if(mysqli_num_rows($rs)>0)
	{		
?>		
		<tr class="head">
		  <Td width="12%" align="center" >ID</Td>
		  <td width="8%" class="pad"> Job Code</td>
		  <td width="15%" class="pad">Jobe Title</td>
		  <td width="15%" class="pad" >Job Location</td>
          <td width="15%" class="pad" >Job Experience</td>
          <td width="15%" class="pad" >No Of Posts</td>
		  <td width="10%" align="center" >Status</td>
          
		  <td width="15%"  ><div align="center">Action</div></td></tr>
<?php
			while($row = mysqli_fetch_object($rs))
			{
?>
		<tr class="text" onmouseover="bgr_color(this, '#FFFF99')" onMouseOut="bgr_color(this, '')">
		<td align="center" valign="middle"><?php echo $row->job_id; ?></Td>
		<td class="pad" valign="middle" ><?php echo $row->job_code; ?></td>
		<td class="pad" valign="middle"><?php echo ucfirst(stripslashes($row->job_title)) ; ?></td>
		<td class="pad" valign="middle"><?php echo $row->location; ?></td>
        <td class="pad" valign="middle"><?php echo $row->experience; ?></td>
        <td class="pad" valign="middle"><?php echo $row->no_of_posts; ?></td>
        
        
		<td align="center" valign="middle">
<?php 
	if($row->status =="active")
	{
?>
<img src="images/green.gif" title="Active" border="0"  /> &nbsp; &nbsp;
<a href="index.php?pagename=manager_job&action=ChangeStatus&pcode=<?php echo $row->job_id;?>&pageno=<?php echo $page;?>&records=<?php echo $max_results;?>"><img src="images/red_light.gif" title="Inactive" border="0"  /></a>
<?php
	}
	else if($row->status =="inactive")
	{
?>
<a href="index.php?pagename=manager_job&action=ChangeStatus&pcode=<?php echo $row->job_id;?>&pageno=<?php echo $page;?>&records=<?php echo $max_results;?>"><img src="images/green_light.gif" title="Active" border="0"  /></a> &nbsp; &nbsp;
<img src="images/red.gif" title="Inactive" border="0"  />
<?php		
	}
?>		</td>
		<td align="center" valign="middle">
		  <div align="center"><a href="index.php?pagename=add_jobs&action=edit&pcode=<?php echo $row->job_id;?>">
	      <img src="images/e.gif" border="0"  alt="Edit"/></a> &nbsp; &nbsp; 
		   <a href="index.php?pagename=manager_job&action=delete&pcode=<?php echo $row->job_id;?>" onclick='return del();'>
          <img src="images/x.gif" border="0" alt="Delete" /></a>		  </div></td></tr>
		  
<?php 
		
			}
	
?>
<tr class='head headLink'><td colspan="9" align="right" nowrap="nowrap">
<table width="100%">
<tr><td  nowrap="nowrap">
  <div align="right">
    <?php						
		 	$total_pages = $s->getTotal_pages('tbl_jobs', $order_by, $order, $max_results, $searchRecord);
			if($page > 1)
			{ 
				$prev = ($page - 1); 
			echo "<ul><li><a href='index.php?pagename=manager_job&orderby=$order_by&order=$order&pageno=$prev&records=$max_results'> Previous</a></li></ul>"; 
			} 
?>
  </div></td>
<td width="43" nowrap="nowrap" align="center">
			  <select name="pages_select" onchange="OnSelectPages();">
                <?php
			for($i = 1; $i <= $total_pages; $i++)
			{ 
?>
<option <?php if($page==$i){ echo "selected='selected'";} ?> 
value="index.php?pagename=manager_job&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $i;?>&records=<?php echo $max_results;?>">
<?php echo $i;?></option>
<?php
			} 
?>
              </select>			  </td><td width="59" nowrap="nowrap" >
              <div align="left">
<?php		
			if($page < $total_pages)
			{ 
				$next = ($page + 1); 
				echo "<ul><li><a href='index.php?pagename=manager_job&orderby=$order_by&order=$order&pageno=$next&records=$max_results'>Next </a></li></ul>";
			} 
?>
              </div></td>
</tr></table>
</td></tr>
<?php 
		}
		else
		{	
?>	
<tr class='text'><td colspan='9' class='redstar'> &nbsp; No record present in database.</td></tr> 
<?php
		}
?>	
	</table>
		</td></tr>
</table>
</form>
