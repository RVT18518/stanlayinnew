
<link href="css/style.css" rel="stylesheet" type="text/css" />
<?php
	$data_action = $_REQUEST['action'];
	$pcode		 = $_REQUEST["pcode"];
	$order_by 	 = 'module_config_id';
	$order    	 = 'asc'; 
	if(!isset($_GET['pageno']))
	{ 
    	$page = 1; 
	} 
	else 
	{ 
    	$page = $_GET['pageno']; 
	} 
	if(!isset($_GET['records']))
	{ 
    	$max_results = 10; 
	} 
	else 
	{ 
    	$max_results = $_GET['records']; 
	} 
	$from = (($page * $max_results) - $max_results);  
	if($_REQUEST['action']=="uninstall")
	{
		$fileArray["module_config_status"] 	= "inactive";
		$result 	  						= $s->editRecord('tbl_module_config_master' ,$fileArray,'module_config_id',$pcode);
		$rs_module_un						= $s->getData_with_condition('tbl_module_config_master','module_config_id',$pcode);
		$row_module_un						= mysqli_fetch_object($rs_module_un);
		$rs_subtable						= $s->getData_without_condition($row_module_un->data_table_name);
		$row_subtable						= mysqli_fetch_object($rs_subtable);
		if($row_subtable->link_table_name   != 'none')
		{
			$sql_unistall_link				= "truncate table ".$row_subtable->link_table_name;
			$result_link	 				= mysqli_query($GLOBALS["___mysqli_ston"],$sql_unistall_link);
		}
		$sql_unistall						= "truncate table ".$row_module_un->data_table_name;
		$result		 						= mysqli_query($GLOBALS["___mysqli_ston"],$sql_unistall);
		
	}
?>
<script type="text/javascript">
function OnSelect() 
{
	window.location = document.frx1.records.value;
}
function OnSelectPages()
{
	window.location = document.frx1.pages_select.value;
}
</script>
<form name="frx1" id="frx1" action="#" method="post">
<table width="100%" align="center" cellpadding="0" cellspacing="0" class="pagecontent">
	
	 <tr>
       <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
           <tr>
             <td width="24%" nowrap class="pageheadTop">Payment Module Manager </td>
               <td width="76%" class="headLink"></td>
           </tr>
       </table></td>
    </tr>
	 <tr>
       <td class="pHeadLine"></td>
    </tr>
	 <tr>
       <td>&nbsp;</td>
    </tr>
	<tr><td>
<?php 
		if($data_action=='uninstall')
		{
			if($result)
			{
				echo "<p class='success'> Module Uninstalled Successfully</p><br />";	
			}
			else 
			{
				echo "<p class='error'> Module NOT Uninstalled Successfully.</p><br />";	
			}
		}
		if($_REQUEST['action']=='updateDone')
		{
			 if($result==0)
			{
				echo "<p class='success'>".record_update."</p><br />";	
			}
			else if($result==1)
			{
				echo "<p class='error'>".record_not_update."</p><br />";	
			}
		}
		if($_REQUEST['action']=='insertDone')
		{
			 if($result==0)
			{
				echo "<p class='success'>".record_added."</p><br />";	
			}
			else if($result==1)
			{
				echo "<p class='error'>".record_not_added."</p><br />";	
			}
			
		}
	?>	</td></tr>	<tr><td width="100%">
	<table width="100%" cellpadding="0" cellspacing="0" class="tblBorder">
		<tr class="pagehead">
		  <td colspan="3" class="pad" > Payment Module Details </td>
		  <td align="right">Records View &nbsp; 
        <select name="records" onchange="OnSelect();"  >
          <option <?php if($max_results==10){ echo "selected='selected'";} ?> 
value="index.php?pagename=payment_module&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=10">10</option>
          <option <?php if($max_results==20){ echo "selected='selected'";} ?>
value="index.php?pagename=payment_module&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=20">20</option>
          <option <?php if($max_results==50){ echo "selected='selected'";} ?>
value="index.php?pagename=payment_module&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=50">50</option>
          <option <?php if($max_results==100){ echo "selected='selected'";} ?>
value="index.php?pagename=payment_module&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=100">100</option>
        </select>
 </td>
		  </tr>
<?php
	$recordSet = " 1=1 ";
	$recordSet = $recordSet . " and module_config_type ='payment' ";
	$rs 	   = $s->getData_withPages('tbl_module_config_master',$order_by, $order,$from,$max_results,$recordSet);
	
	if(mysqli_num_rows($rs)==0 && $page!=1)
	{
		$page=1;
		$s->pageLocation("index.php?pagename=payment_module&orderby=$order_by&order=$order&pageno=$page&records=$max_results"); 
	}
	if(mysqli_num_rows($rs)>0)
	{		
?>		
		<tr class="head">
		  <Td width="116" align="center" >ID</Td>
		  <td width="739" class="pad">Payment Module Name</td>
		  
		  <td width="203" align="center">&nbsp; </td>
		  <td width="203"  ><div align="center"> Action</div></td></tr>
<?php
			$i=1;
			while($row = mysqli_fetch_object($rs))
			{
?>
		<tr class="text" onmouseover="bgr_color(this, '#EAB9BA')" onMouseOut="bgr_color(this, '')">
		<td align="center" valign="middle"><?php echo $row->module_config_id; ?></td>
		<td class="pad" valign="middle" ><?php echo $row->module_config_name;?></td>
		
		<td width='203' align='center'> 
		 </td>
		<td align="center" valign="middle">
	      <?php if($row->module_config_status=='inactive'){?>
		  <a href="index.php?pagename=<?php echo  $row->pagename;?>&action=edit&pcode=<?php echo $row->module_config_id;?>">
		  <img src="images/add.gif" border="0"  alt="Install"/></a>
		  <?php } else if($row->module_config_status=='active'){?>
		  <a href="index.php?pagename=<?php echo  $row->pagename;?>&action=edit&pcode=<?php echo $row->module_config_id;?>">
		  <img src="images/e.gif" border="0"  alt="Edit"/></a>
		  &nbsp; &nbsp; 
		  <a href="index.php?pagename=payment_module&action=uninstall&pcode=<?php echo $row->module_config_id;?>" onclick='return uninstall();'>
          <img src="images/x.gif" border="0" alt="Uninstall" /></a>
		  <?php }?>
		 </td></tr>
<?php 
			}
?>
<tr class='head headLink'><td colspan="4" align="right" nowrap="nowrap">
<table width="100%">
<tr><td  nowrap="nowrap">
  <div align="right">
    <?php						
		 	$total_pages = $s->getTotal_pages('tbl_module_config_master',$order_by, $order,$max_results,$recordSet);
			if($page > 1)
			{ 
				$prev = ($page - 1); 
			echo "<ul><li><a href='index.php?pagename=payment_module&orderby=$order_by&order=$order&pageno=$prev&records=$max_results'>< Previous</a></li></ul>"; 
			} 
?>
  </div></td>
<td width="43" nowrap="nowrap" align="center">
			  <select name="pages_select" onchange="OnSelectPages();">
                <?php
			for($i = 1; $i <= $total_pages; $i++)
			{ 
?>
<option <?php if($page==$i){ echo "selected='selected'";} ?> 
value="index.php?pagename=payment_module&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $i;?>&records=<?php echo $max_results;?>">
<?php echo $i;?></option>
<?php
			} 
?>
              </select>
			</td><td width="59" nowrap="nowrap" >
              <div align="left">
                <?php		
			if($page < $total_pages)
			{ 
				$next = ($page + 1); 
				echo "<ul><li><a href='index.php?pagename=payment_module&orderby=$order_by&order=$order&pageno=$next&records=$max_results'>Next ></a></li></ul>";
			} 
		?>
              </div></td>
</tr></table>
</td></tr>
<?php 
		}
		else
		{	
?>	
<tr class='text'>
  <td colspan='4' class='redstar'> &nbsp; No Payment Module.</td>
</tr> 
<?php
		}
?>	
	</table>
		</td></tr>
</table>
</form>
