
<link href="../css/style.css" rel="stylesheet" type="text/css" />
<?php 
	$data_action = $_REQUEST["action"];
	$pcode 		 = $_REQUEST["pcode"];
	$run		 = '';
	if($_REQUEST["action"] == "update" || $_REQUEST["action"] == "insert")
	{
		$dataArray["business_email"] = $_REQUEST["business_email"];
		$dataArray["gateway_server"] = $_REQUEST["gatewayserver"];
		$dataArray["page_style"]	 = $_REQUEST["page_logo"];
	}
	if($_REQUEST["action"] == "insert")
	{
		
		$result = $s->insertRecord('tbl_paypal_standard',$dataArray);
		$data_action = 'insertDone';
$s->pageLocation("index.php?pagename=payment_module&action=$data_action&result=$result");
	}
	if($_REQUEST["action"] == "update")
	{
		$result = $s->editRecord('tbl_paypal_standard',$dataArray,'standard_id','1');
		$data_action = 'updateDone';

$s->pageLocation("index.php?pagename=payment_module&action=$data_action&result=$result");

	}
	if($_REQUEST["action"] == "edit")
	{
		
		
		$rsModule		= $s->getData_with_condition('tbl_module_config_master','module_config_id','4');
		$rowModule 		= mysqli_fetch_object($rsModule);
		$moduleStatus	= $rowModule->module_config_status;

		if($moduleStatus == 'inactive')
		{
			$dataArrayModule['module_config_status'] = 'active';
			$result 	  = $s->editRecord('tbl_module_config_master',$dataArrayModule,'module_config_id','4');
			$data_action = "insert";
			$run		 = 'install';
		}
		else
		{
			$rsPPS  		= $s->getData_with_condition('tbl_paypal_standard','standard_id','1');
			$rowPPS 		= mysqli_fetch_object($rsPPS);
			$business_email = $rowPPS->business_email;
			$gatewayserver  = $rowPPS->gateway_server;
			$page_logo  	= $rowPPS->page_style;
			$data_action 	= "update";
		}
		
	}
?>
<form name="frx1" id="frx1" action="index.php?pagename=paypal_standard_payment_manager&action=<?php echo $data_action;?>&pcode=<?php echo 1;?>" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
	  <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="27%" nowrap class="pageheadTop">PayPal Website Standard Payment</td>
          <td width="73%" class="headLink"><ul>     
		  <li><a href="index.php?pagename=admin_manager" >Save</a></li>           
              <li><a href="#" onclick="back_page();">Back</a></li>
              
          </ul></td>
        </tr>
      </table></td>
    </tr>
	<tr>
	  <td class="pHeadLine"></td>
    </tr>
	<tr><td>&nbsp;</td></tr>
	<tr><td>
<?php
	if($run == 'install')
	{
		 if($result == 0)
		{
			echo "<p class='success'> PayPal Website Payments Standard installed Successfully.</p><br />";	
		}
		else if($result == 1)
		{
			echo "<p class='error'>PayPal Website Payments Standard not installed Successfully.</p><br />";	
		}
	}
?>
</td>  </tr>
	
	<tr><td>
	<table width="100%" border="0" cellpadding="0" cellspacing="0" class="tblBorder">
		<tr class="pagehead" >
		  <td class="pad" colspan="2" >Pay Pal Standard Details </td>
		  </tr>
		<tr class="text">
		  <td width="15%" class="pad" > Business E-Mail Address </strong> <span class="redstar">*</span></td>
		  <td width="85%" ><input type="text" name="business_email" id="business_email" class="inpuTxt"  value="<?php echo $business_email;?>">
		[ The PayPal seller e-mail address to accept payments]
		</td>
		</tr>
  <tr class="text">
    <td class="pad" align="left">Gateway Server <span class="redstar"> *</span></td>
    <td align="left"><select name="gatewayserver" id="gatewayserver" class="inpuTxt" >
      <option value="live" <?php if($gatewayserver =='live'){echo "selected='selected'";}?>>Live</option>
      <option value="sandbox" <?php if($gatewayserver =='sandbox'){echo "selected='selected'";}?>>Sandbox</option>
	</select>
    [ Use the testing (sandbox) or live gateway server for transactions ?]</td>
  </tr>
		
		<tr class="text">
		<td class="pad">Page Style  <span class="redstar"> </span></td>
		<td><input type="text" name="page_logo" id="page_logo" class="inpuTxtSelect" size="80"  value="<?php echo $page_logo;?>">
		  [Store Logo URL e.g. http://www.abc.com/images/logo.jpg]
		</td>
		</tr>

		<!--<tr class="text">
		  <td class="pad">Debug E-Mail Address  <span class="redstar"> *</span></td>
		  <td><input type="text" name="admin_fname" id="admin_fname" class="inpuTxt"  value="<?php //echo $admin_fname;?>">
		  [All parameters of an Invalid IPN notification will be sent to this email address if one is entered.]
		</td>
		</tr>-->
		
		<tr class="text">
          <td width="15%" class="pad">&nbsp;</td>
          <td width="85%"><input type="submit" name="add " id="add" class="inputton" value="Save" />
            &nbsp;</td>
        </tr><tr class="text"><td class="redstar pad" colspan="2"> * Required Fields </td></tr>
</table>
</td></tr>
	
	
</table>
</form>
<script language="JavaScript" type="text/javascript">
var frmvalidator = new Validator("frx1");
frmvalidator.addValidation("business_email","req","Please enter your Business Email.");
frmvalidator.addValidation("business_email","email","Please enter your right Business Email.");
</script>
