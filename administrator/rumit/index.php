<?php 
	include("../includes/function_lib.php");
	include("session_check.php");
	$admin_id=$_SESSION["AdminLoginID_SET"];
	if(strlen(trim($_SESSION["AdminLoginID_SET"])>0))
		{
			include("../fckeditor/fckeditor.php");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Backoffice</title>
<link rel="stylesheet" type="text/css" href="../css/ddlevelsmenu-base.css" />
<link rel="stylesheet" type="text/css" href="../css/ddlevelsmenu-topbar.css" />
<link rel="stylesheet" type="text/css" href="../css/ddlevelsmenu-sidebar.css" />
<link rel="stylesheet" type="text/css" href="../backoffice/css/epoch_styles.css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/ddlevelsmenu.js"></script>
<script language="JavaScript" src="../js/gen_validatorv2.js" type="text/javascript"></script>
<SCRIPT language="JavaScript" src="../js/AnchorPosition.js" type="text/javascript"></SCRIPT>
<SCRIPT language="JavaScript" src="../js/PopupWindow.js" type="text/javascript"></SCRIPT>
<SCRIPT language="JavaScript" src="../js/CalendarPopup.js" type="text/javascript"></SCRIPT>
<!--<script src="../ckeditor/ckeditor.js"></script>-->
<!--NEW CALENDAR-->
<link rel="stylesheet" type="text/css" media="all" href="jsDatePick_ltr.min.css" />
<!--<script type="text/javascript" src="jquery.1.4.2.js"></script>-->
<script type="text/javascript" src="jsDatePick.jquery.min.1.3.js"></script>

<!--NEW CALENDAR-->
<script type="text/javascript">
function bgr_color(obj, color) 
{
    obj.style.backgroundColor=color
}
</script>

<!--Graph-->
    <!--[if lte IE 8]><script language="javascript" type="text/javascript" src="../excanvas.min.js"></script><![endif]-->
    <script language="javascript" type="text/javascript" src="chart/jquery.js"></script>
    <script language="javascript" type="text/javascript" src="chart/jquery.flot.js"></script>
<!--Graph-->

<script language="javascript">  
function back_page()
{
	window.history.back(-1);
}
</script>
<script type="text/javascript" src="js/jquery-1.3.2.js"></script>
<!--<link rel="stylesheet" href="css/lightbox.css" type="text/css" media="screen" />
<script language="JavaScript" type="text/javascript" src="js/prototype.js" ></script>
<script language="JavaScript" type="text/javascript" src="js/scriptaculous.js?load=effects" ></script>
<script language="JavaScript" type="text/javascript" src="js/lightbox.js" ></script>
--></head>
<link href="css/style.css" rel="stylesheet" type="text/css" />
</head><body>
<!--<div id="dhtmltooltip"></div>--> 
<!--script language="JavaScript" src="../js/HelpTextJS.js" type="text/javascript"></script-->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="100%"><table width="100%" border="0" cellspacing="0" cellpadding="0" id="pageheader">
        <tr>
          <td ><!--<h1>Stanlay</h1>--> 
         
           <a href="index.php?pagename=general_configuration" ><img src="../images/logo.png" width="180" height="51" alt="Stanlay" border="0" /></a>
          </td>
             <td  align="center" ><h1 style="color:#ff0000">Admin Control Panel</h1></td>
          <?php if($_SESSION["AdminLoginID_SET"]!=''){?>
          <td width="39%" class="topRight">Welcome <?php echo $_SESSION["AdminName_SET"];?><span class="saperator">|</span> <?php echo date("l F d, Y")?> <span class="saperator">|</span> <a href="logout.php">Log Out</a></td>
          <?php }?>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td width="100%"><?php include("includes/main_top_menu.php");?></td>
  </tr>
  
  <tr>
    <td height="450px" valign="top"><table width="100%" border="0" cellspacing="1" cellpadding="0">
        <tr>
          <td width="3%"><?php //include("includes/left-menu.php");?></td>
          <td><?php 
	//error_reporting(0);
	
	//include("resize_function.php");
	include("../fckeditor/fckeditor.php");
	//include	("includes/config.php");
	$_SESSION['msg']="";
	
			if(isset($_REQUEST['pagename']))
			{
				include($_REQUEST['pagename'].".php");
			}
		
	//*/
	
	/*if(isset($_REQUEST['pagename'])){
		include($_REQUEST['pagename'].".php");
	}*/
	?></td>
          <td width="3%">&nbsp;</td>
        </tr>
      </table></td>
  </tr>
  
  <?php /*?><tr>
    <td height="450px" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="3%">&nbsp;</td>
          <td><?php
	if(isset($_REQUEST['pagename']))
	{
		include($_REQUEST['pagename'].".php");
	}
?></td>
          <td width="3%">&nbsp;</td>
        </tr>
      </table></td>
  </tr><?php */?>
  <tr>
    <td height="20"></td>
  </tr>
  
  <tr>
    <td ><?php include("includes/footer.php");?></td>
  </tr>
</table>
</body>
</html>
<?php
	}
	else if(!isset($_SESSION["AdminLoginID_SET"]))
	{
		$s->pageLocation("admin_login.php");
	}
?>