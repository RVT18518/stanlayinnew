<?php
	$data_action = $_REQUEST['action'];
	$pcode		 = $_REQUEST["pcode"];
	$order		 = $_REQUEST["order"];
	$order_by	 = $_REQUEST["orderby"];
	
/*	echo "<pre>";
	
	print_r($_SESSION);*/
	$acc_manager = $_SESSION["AdminLoginID_SET"];
	
	if(strlen(trim($order))<=0)
	{
	$order 	 	 = 'asc';
	}
	if(strlen(trim($order_by))<=0)
	{
	$order_by 	 = 'ID';
	}	
	$result	     = $_REQUEST["result"]; 
	if(!isset($_GET['pageno']))
	{ 
    	$page = 1; 
	} 
	else 
	{ 
    	$page = $_GET['pageno']; 
	} 
	if(!isset($_GET['records']))
	{ 
    	$max_results = 100; 
	} 
	else 
	{ 
    	$max_results = $_GET['records']; 
	} 
	$from = (($page * $max_results) - $max_results);  
	
	
	if($_REQUEST['action']=='ChangeStatus')
	{
		$rs_status = $s->getData_with_condition('vendor_master','ID',$pcode);
		if(mysqli_num_rows($rs_status)>0)
		{
			$row_status = mysqli_fetch_object($rs_status);
			if($row_status->status == 'active')
			{
				$fileArray["status"] = 'inactive';
			}
			else if($row_status->status == 'inactive')
			{
				$fileArray["status"] = 'active';
			}
		
			$result      = $s->editRecord('vendor_master',$fileArray,'ID',$pcode);
			$s->pageLocation("index.php?pagename=vendor_manager&action=ChangeStatusDone&result=$result");
		}
	}
/*	if($_REQUEST['action']=='update')
	{
		$result = $s->editRecord('vendor_master',$file_array,'id',$pcode);
		$data_action = "updateDone";
		$s->pageLocation("index.php?pagename=company_manager&action=$data_action&pcode=$pcode&result=$result"); 
	}*/

?>
<script type="text/javascript">
function OnSelect() 
{
	window.location = document.frx1.records.value;
}
function OnSelectPages()
{
	window.location = document.frx1.pages_select.value;
}
</script>

<form name="frx1" id="frx1" method="post" action="#">
  <table width="100%" align="center" cellpadding="0" cellspacing="0" class="pagecontent">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="40%" class="pageheadTop">Vendor Master</td>
            <td width="60%" class="headLink"><ul>
                <li><a href="index.php?pagename=add_vendor&action=add_new">Add New Vendor</a></li>
              </ul></td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td class="pHeadLine"></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><?php 
		if($_REQUEST['action']=='ChangeStatusDone')
		{
			if($result==0)
			{
				echo "<p class='success'>Status Change Successfully</p><br />";	
			}
			else if($result==1)
			{
				echo "<p class='error'>Status Changing Fails</p><br />";	
			}
		}	
		if($data_action=='delete')
		{
			$result = $s->delete_table_withCondition('vendor_master','id',$pcode);	
			if($result)
			{
				echo "<p class='success'>".record_delete."</p><br />";	
			}
			else 
			{
				echo "<p class='error'>".record_not_delete."</p><br />";	
			}
		}
		if($_REQUEST['action']=='update')
		{
			 if($result==0)
			{
				echo "<p class='success'>".record_update."</p><br />";	
			}
			else if($result==1)
			{
				echo "<p class='error'>".record_not_update."</p><br />";	
			}
		}
		else if($_REQUEST['action']=='insert')
		{
			if($result==0)
			{
				echo "<p class='success'>".record_added."</p><br />";	
			}
			else if($result==1)
			{
				echo "<p class='error'>".record_not_added."</p><br />";	
			}
		}
		if($_REQUEST["cat"] == "parent")
		{
			echo "<p class='error'>"."Please select sub category"."</p><br />";	
		}
	?></td>
    </tr>
    <tr>
      <td width="100%"><table width="100%" cellpadding="5" cellspacing="0" class="tblBorder">
          <tr class="pagehead">
            <td colspan="4" class="pad" nowrap="nowrap" > Vendor Details </td>
            <td align="right" nowrap="nowrap">
            </td>
          </tr>
          <?php
	$searchRecord = " 1 = 1 ";
	//$rs = $s->getData_withPages('vendor_master',$order_by, $order,$from,$max_results, $searchRecord);
	$table_name	= "vendor_master";
	
	if($_SESSION["AdminLoginID_SET"]!=4)
	{
	$search		= "where deleteflag = 'active'";
	}
	else
	{
	$search		= "where deleteflag = 'active' ";
	}
	$star		= "*";
	
 	$rs 		= $s->getData_withPagesJoin($table_name, $order_by, $order, $from, $max_results, $search, $star);	
	if(mysqli_num_rows($rs)==0 && $page!=1)
	{
		$page=1;
		$s->pageLocation("index.php?pagename=vendor_manager&orderby=id&order=$order&pageno=$page&records=$max_results"); 
	}
	
	if($order == "asc")
	{
		$order_new = "desc";
	}
	else if($order == "desc")
	{
		$order_new = "asc";
	}
	
	$i=1;
	if(mysqli_num_rows($rs)!=0)
	{
?>
          <tr class="head">
            <td width="12%" align="center" ><a href="index.php?pagename=vendor_manager&orderby=ID&order=<?php echo $order_new; ?>&pageno=<?php echo $page;?>&records=<?php echo $max_results;?>">ID</a></td>
            <td width="23%" class="pad"><a href="index.php?pagename=vendor_manager&orderby=C_Name&order=<?php echo $order_new; ?>&pageno=<?php echo $page;?>&records=<?php echo $max_results;?>">Company Name </a></td>
            <td width="9%" align="center">Country</td>
            <td width="9%" align="center">Status</td>
            <td width="18%"   align="center">Action</td>
          </tr>
          <?php
			while($row = mysqli_fetch_object($rs))
			{
?>
          <tr class="text" onmouseover="bgr_color(this, '#EAB9BA')" onMouseOut="bgr_color(this, '')">
            <td align="center" valign="middle"><?php echo $row->ID; ?></Td>
            <td class="pad"    valign="middle"><?php echo ucfirst(stripslashes($row->C_Name)) ; ?></td>
            <td align="center" valign="middle"><?php echo ucfirst(stripslashes($row->Country)) ; ?></td>
            <td align="center" valign="middle"><?php 
	if($row->status == "active")
	{
?>
              <img src="images/green.gif" title="Active" border="0"  /> &nbsp; &nbsp; <a href="index.php?pagename=vendor_manager&action=ChangeStatus&pcode=<?php echo $row->ID;?>&pageno=<?php echo $page;?>&records=<?php echo $max_results;?>"><img src="images/red_light.gif" title="Inactive" border="0"  /></a>
              <?php
	}
	else if($row->status == "inactive")
	{
?>
              <a href="index.php?pagename=vendor_manager&action=ChangeStatus&pcode=<?php echo $row->ID;?>&pageno=<?php echo $page;?>&records=<?php echo $max_results;?>"><img src="images/green_light.gif" title="Active" border="0"  /></a> &nbsp; &nbsp; <img src="images/red.gif" title="Inactive" border="0"  />
              <?php		
	}
?></td>
            <td align="center" valign="middle"><a href="index.php?pagename=add_product&action=edit&pcode=<?php echo $row->ID;?>"></a> <a href="index.php?pagename=add_vendor&action=edit&pcode=<?php echo $row->ID;?>"> <img src="images/e.gif" border="0"  alt="Edit"/></a> &nbsp; &nbsp; <a href="index.php?pagename=vendor_manager&action=delete&pcode=<?php echo $row->ID;?>" onclick='return del();'> <img src="images/x.gif" border="0" alt="Delete" /></a></td>
          </tr>
          <?php 
			}
?>
          <tr class='head headLink'>
            <td colspan="5" align="right" nowrap="nowrap"><table width="100%">
                <tr>
                  <td  nowrap="nowrap"  align="right"><?php
	$total_pages = $s->getTotal_pagesJoin($table_name, $order_by, $order, $max_results, $search, $star);
	if($page > 1)
	{ 
		$prev = ($page - 1); 
		echo "<ul><li><a href='index.php?pagename=vendor_manager&orderby=$order_by&order=$order&pageno=$prev&records=$max_results'> Previous</a></li></ul>"; 
	} 
?></td>
                  <td width="43" nowrap="nowrap" align="center"><select name="pages_select" onchange="OnSelectPages();">
                      <?php
	for($i = 1; $i <= $total_pages; $i++)
	{ 
?>
                      <option <?php if($page==$i){ echo "selected='selected'";} ?> 
value="index.php?pagename=company_manager&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $i;?>&records=<?php echo $max_results;?>"> <?php echo $i;?></option>
                      <?php
	} 
?>
                    </select></td>
                  <td width="59" nowrap="nowrap" align="left"><?php		
			if($page < $total_pages)
			{ 
				$next = ($page + 1); 
				echo "<ul><li><a href='index.php?pagename=vendor_manager&orderby=$order_by&order=$order&pageno=$next&records=$max_results'>Next </a></li></ul>";
			} 
?></td>
                </tr>
              </table></td>
          </tr>
          <?php 
		}
		else
		{	
?>
          <tr class='text'>
            <td colspan='8' class='redstar'>&nbsp; No record present in database.</td>
          </tr>
          <?php
		}
?>
        </table></td>
    </tr>
  </table>
</form>
