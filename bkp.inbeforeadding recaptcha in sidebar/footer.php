<?php $css_path="https://www.stanlay.in/"; ?>
<footer class"animate-box" data-animate-effect="fadeInUp animated-fast">
  <div class="container">
    <div class="row clearfix">
      <div class="col-md-6 col-sm-12">
        <div class="col-md-4 col-sm-4">
          <div class="blk-heading">Quick Links</div>
          <ul>
            <li><a href="<?php echo $css_path; ?>about-us.php">About Us</a></li>
            <li><a href="<?php echo $css_path; ?>career.php">Career</a></li>
            <li><a href="<?php echo $css_path; ?>blog/">Blog</a></li>
            <li><a href="<?php echo $css_path; ?>news.php">News and Events</a></li>
            <li><a href="https://www.stanlay.com" target="_blank">Stanlay.com</a></li>
          </ul>
        </div>
        <div class="col-md-4 col-sm-4">
          <div class="blk-heading">Video &amp; Training</div>
          <ul>
            <li><a href="<?php echo $css_path; ?>video.php">Videos</a></li>
            <li><a href="<?php echo $css_path; ?>catalogues.php">Catalogues</a></li>
            <li><a href="<?php echo $css_path; ?>support-&-traning.php">Support &amp; Training</a></li>
            <li><a href="<?php echo $css_path; ?>warranty_support/" target="_blank">Warranty Support</a></li>
          </ul>
        </div>
        <div class="col-md-4 col-sm-4">
          <div class="blk-heading">Achievement</div>
          <ul>
            <li><a href="<?php echo $css_path; ?>awards-&-prizes.php">Award &amp; Prizes</a></li>
            <li><a href="<?php echo $css_path; ?>trade-fairs-&-events.php">Trade Fairs &amp; Events</a></li>
          </ul>
        </div>
      </div>
      <div class="col-md-6 col-sm-12">
        <div class="col-md-5 col-sm-4 sm-icons">
          <div class="blk-heading">Follow Us</div>
          <a href="https://www.facebook.com/Stanlay-164626704400/" target="_blank" title="See our social activities" class="sm-fb">Facebook</a> <a href="#;" class="sm-tw">Twitter</a> <a href="#;" class="sm-linkedin">Linked In</a> <a href="https://www.youtube.com/user/Stanlayvideo/" target="_blank" title="See our videos and tutorials" class="sm-ytube">Youtube</a>
          <div class="sales-helpline">
            <div>Sales Helpline</div>
            <strong>Email:</strong> <a href="mailto:sales@stanlay.com">sales@stanlay.com</a><br>
            <strong>Tel:</strong> 011-41860000 </div>
        </div>
        <div class="col-md-7 col-sm-8 signup">
          <div class="blk-heading">Sign up for Email Notification</div>
          <form>
            <input type="text" name=""  placeholder="Enter your email address and press enter">
          </form>
        </div>
      </div>
    </div>
  </div>
  <div class="container-fluid copyright">
    <div class="container">
      <div class="copyright-lt">&copy; Copyright 2019 Stanlay . All rights reserved.</div>
      <div class="copyright-rt"></div>
    </div>
  </div>
</footer>
<?php mysql_close();?>
<link href="<?php echo BASE_URL;?>css/lightslider.css" rel="stylesheet"  >
<link rel="stylesheet" href="<?php echo BASE_URL;?>css/animate.css"  >
<link href="<?php echo BASE_URL;?>css/prettyPhoto.css" rel="stylesheet"  >
<link href="<?php echo BASE_URL;?>css/responsive.css" rel="stylesheet"  >
<link href="<?php echo BASE_URL;?>css/pgwslider.css" rel="stylesheet"  >
<link rel="stylesheet" type="text/css" media="all" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"  >
<!--/header menu--> 
<script src="<?php echo $css_path; ?>js/jquery.js"></script> 
<script async="async" src="<?php echo $css_path; ?>js/bootstrap.min.js"></script> 
<script async="async" defer="defer" src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script> 
<script async="async" src="<?php echo $css_path; ?>js/megamenu.js"></script> 
<script src="<?php echo $css_path; ?>js/responsive-tabs.js"></script> 
<script src="<?php echo $css_path; ?>js/pgwslider.js"></script> 
<script async="async" src="<?php echo $css_path; ?>js/jquery.expander.js"></script> 
<script src="<?php echo $css_path; ?>js/jquery.prettyPhoto.js"></script> 
<script src="<?php echo $css_path; ?>js/jquery.isotope.min.js"></script> 
<script src="<?php echo $css_path; ?>js/wow.min.js"></script> 
<script src="<?php echo $css_path; ?>tabs/easy-responsive-tabs.js"></script> 
<script src="<?php echo $css_path; ?>js/slick.js"></script> 
<script src="<?php echo $css_path; ?>js/lightslider.js"></script> 
<script src="<?php echo $css_path; ?>js/jquery.simplyscroll.min.js"></script>
<?php /*?><script src="<?php echo $css_path; ?>ddmenu2/rem.min.js"></script> 
<script src="<?php echo $css_path; ?>ddmenu2/nav.jquery.min.js"></script> 
<script src="<?php echo $css_path; ?>ddmenu3/menu-support.js"></script> 
<?php */?>
<!--<script>
   $('.nav').nav();
</script>
--><!-- 09June2018 starts--> 
<script type="text/javascript" src="<?php echo $css_path; ?>js/stellarnav.min.js"></script> 
<script type="text/javascript">
	jQuery(document).ready(function($) {
		jQuery('.stellarnav').stellarNav({
			theme: 'light'
		});
		$(".subcat1").hover(
			function(){ // Mouse Over
			$(this).parent().addClass("active-parent");
			},
			function(){ // Mouse Out
			$(this).parent().removeClass("active-parent");
			}
		);

	$(".subcat2").hover(
			function(){ // Mouse Over
			$(this).parent().addClass("active-parent");
			},

		function(){ // Mouse Out
			$(this).parent().removeClass("active-parent");
			}
		);
	$(".dropdown").hover(            
        function() {
            $('.dropdown-menu', this).not('.in .dropdown-menu').stop(true,true).slideDown("400");
            $(this).toggleClass('open');        
        },
        function() {
            $('.dropdown-menu', this).not('.in .dropdown-menu').stop(true,true).slideUp("400");
            $(this).toggleClass('open');       
        }
    );
});
</script> 
<!-- 09June2018 ends--> 
<script async="async" defer="defer" src="<?php echo $css_path; ?>js/main.js"></script>
<link rel="stylesheet"  href="<?php echo BASE_URL;?>css/lightslider.css"/>
<?php include_once("analyticstracking.php") ?>