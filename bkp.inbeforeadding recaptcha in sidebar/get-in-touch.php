<section class="getintouch">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <div class="head">Get In Touch</div>
        <div class="want-know">Want to Know More About our Products ?</div>
        <p>Contact us today to find out more. We have our sales representative ready to help you with any subject matter related to our products</p>
      </div>
      <div class="col-md-6"> 
        <form role="form" action="<?php echo BASE_URL;?>mail-contact-captcha.php" name="TagPopup_Form" id="TagPopup_Form" method="post" onsubmit="return validateform();">
          <input type="text" name="name" placeholder="Name"  required="required" >
          <input  name="mobile" type="tel" pattern="[7-9]{1}[0-9]{9}" class="formfooter" placeholder="Mobile No." oninvalid="this.setCustomValidity('Please enter your valid mobile number.')" oninput="setCustomValidity('')"  required="required">
          <input type="email" name="email" placeholder="Email"  class="formfooter" required="required" style="width:98%" >
          <textarea name="message" cols="" rows="" placeholder="Message"  required="required" ></textarea>
<!--          <div class="g-recaptcha" data-sitekey="6LerrmAUAAAAAPEfS2Ix86b5schmXXslYTxpTMpo"></div>-->
          <div class="g-recaptcha" data-sitekey="6Lf9LWgUAAAAAIHNSulDkx2uL1zDDt_vYmSpatyF"></div>
          <input type="submit" name="submit" value="Submit">
        </form>
      </div>
    </div>
  </div>
</section>
<style>

.formfooter{

width: 48%;

    margin: 0 10px 10px 0;

    float: left;}

</style>
<!--<script src='https://www.google.com/recaptcha/api.js'></script> -->
<script type="text/javascript" language="javascript">

function validateform(){

var captcha_response = grecaptcha.getResponse();

if(captcha_response.length == 0)

{

	alert("Please click on the reCAPTCHA box.");

    // Captcha is not Passed

    return false;

}

else

{

    // Captcha is Passed

    return true;

}

}

// ]]></script>