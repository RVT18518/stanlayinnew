<a href="#;" class="btn-letus" data-toggle="modal" data-target="#myModal"><img src="<?php echo $css_path;?>images/btn-let-us-contact.jpg"  alt=""></a> <!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm"> 
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <div class="modal-title"><strong>Let Us Contact You</strong></div>
        </div>
        <div class="modal-body">
          <form role="form" action="<?php echo $css_path; ?>mail-contact1.php" name="TagPopup_Form" id="TagPopup_Form" method="post">
            <input type="hidden" name="_token" value="">
            <div class="form-group">
              <input type="text" name="name1" placeholder="Name" class="form-control input-sm"  required="required" >
            </div>
            <div class="form-group">
              <input type="email" name="email1" placeholder="Email" class="form-control input-sm" required="required" >
            </div>
            <div class="form-group">
              <input  name="mobile1" type="tel" pattern="[7-9]{1}[0-9]{9}" placeholder="Mobile No." class="form-control input-sm" oninvalid="this.setCustomValidity('Please enter your valid mobile number.')" oninput="setCustomValidity('')"  required="required">
            </div>
            <div class="form-group">
              <textarea name="message1" cols="" rows="" placeholder="Message" class="form-control input-sm" required="required" ></textarea>
            </div>
            <div class="form-group">
              <input type="checkbox"  title="Click to verify you are not robot!!!" required="required" />
               <!--<div class="g-recaptcha" data-sitekey="6LerrmAUAAAAAPEfS2Ix86b5schmXXslYTxpTMpo"></div>-->
            </div>
             
            <div class="form-group">
              <div>
                <button type="submit" class="btn btn-login">Submit</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>