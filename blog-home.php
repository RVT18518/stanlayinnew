<?php
include_once('blog/wp-config.php');
$lastposts = get_posts( array(
    'posts_per_page' => 3
) );
?>

<section class="blog-awards animate-box" data-animate-effect="fadeInRight animated-fast">
  <div class="container">
    <div class="col-md-9 col-sm-12">
      <div class="head">Blog</div>
      <?php if ( $lastposts ) {
   foreach ( $lastposts as $post ) :
       setup_postdata( $post ); ?>
      <?php //$url = wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'medium' ); ?>
      <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'medium' );
	  $url=$image["0"];
//	  print_r($image);
	   //the_post_thumbnail('medium'); ?>
      <div class="col-md-4 col-sm-4 blog-container"><img class="lazy" data-src="<?php echo $url; ?>" style="height:138px;width:100%" alt="image" />
        <div class="blog-update"> <span class="blog-date"><?php echo date('d M Y', strtotime($post->post_date));?></span>
          <div class="blog-head twolinetext"><a href="<?php the_permalink(); ?>">
            <?php if(strlen(get_the_title())>61) { echo substr(get_the_title(),0,58)."...";} else {echo the_title();}?>
            </a></div>
        </div>
      </div>
      <?php
   endforeach; 
   wp_reset_postdata();
	  }
?>
    </div>
    <div class="col-md-3 col-sm-12 awards">
      <div class="head">Award &amp; Prizes</div>
      <div class="awards-logos slider" style="margin-top:0px; padding-bottom:20px;">
        <div class="slide"><img class="lazy" data-src="images/quality-assured.jpg" alt="quality-assured"></div>
        <div class="slide"><img class="lazy" data-src="images/asian-contec-certificate.jpg" alt="asian-contec-certificate"></div>
        <div class="slide"><img class="lazy" data-src="images/1stprize-big.jpg" alt="1stprize"></div>
      </div>
    </div>
  </div>
</section>
