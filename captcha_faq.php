<?php
session_start();
$text_faq = rand(10000,99999);
$_SESSION["vercode_faq"] = $text_faq;

$height = 22; //CAPTCHA image height
$width = 60; //CAPTCHA image width

$image_p = imagecreate($width, $height);
$black = imagecolorallocate($image_p, 169, 0, 2); //red
$white = imagecolorallocate($image_p, 255, 255, 255);
$font_size = 12; 
imagestring($image_p, $font_size, 5, 3, $text_faq, $white);
imagejpeg($image_p, null, 80);
?>