<?php include('header-cms.php'); ?>
<style>
table tr.heading td {
	border: 1px solid #CECECE!important;
	padding: 10px 10px!important;
	font-family:'Open Sans', Arial, sans-serif!important;
	font-weight:700!important;
	font-size: 14px!important;
	background: #ed3237!important;
	color: #fff!important;
}
table tr.sub_content td {
	font-family:'Open Sans', Arial, sans-serif!important;
	font-weight: 700!important;
	border: 1px solid #CECECE!important;
	display: table-cell!important;
	padding: 10px 10px!important;
	font-size: 14px!important;
}
</style>

<body class="homepage">
<!--header menu-->
<?php include('header-menu.php');  ?>
<!--/header menu-->
<div class="page-banner"> <img src="images/page-banner.jpg" alt="">
  <ul class="breadcrumb">
    <li><a href="<?php echo $css_path;?>">Home</a></li>
    <li><?php echo stripslashes($front->getContentpage($page ,'pages_name'));?></li>
  </ul>
  <h1><?php echo stripslashes($front->getContentpage($page ,'pages_name'));?></h1>
</div>
<div class="page-content">
  <div class="container-fluid"> <?php echo stripslashes($front->getContentpage($page ,'pages_html_text')); ?>
    <div style="max-width:100%; overflow-x:auto;">
      <div class="DatalTable">
      <?php
$sql_career="Select * from tbl_jobs where deleteflag='active' and status='active' order by date_from asc ";
$rs_career=mysqli_query($GLOBALS["___mysqli_ston"], $sql_career);
$num_rows=mysqli_num_rows($rs_career);

if($num_rows>0){
?>
        <div class="DatalTableBody">
          <div class="DataTableRow">
            <div class="DataTableCell" style="background: #ed3237; color:#fff;"><strong>Sr. No.</strong></div>
            <div class="DataTableCell" style="background: #ed3237; color:#fff;"><strong>Job Title</strong></div>
            <div class="DataTableCell" style="background: #ed3237; color:#fff;"><strong>Location</strong></div>
            <div class="DataTableCell" style="background: #ed3237; color:#fff;"><strong>Experience</strong></div>
            <div class="DataTableCell" style="background: #ed3237; color:#fff;"><strong>No of Post</strong></div>
            <div class="DataTableCell" style="background: #ed3237; color:#fff;"><strong>Full Details</strong></div>
            <p></p>
          </div>

<?php
/*$sql_career="Select * from tbl_jobs where deleteflag='active' and status='active' order by date_from asc ";
$rs_career=mysqli_query($GLOBALS["___mysqli_ston"],$sql_career);*/
while($row_career=mysqli_fetch_object($rs_career))
{

?>
          <div class="DataTableRow">
            <div class="DataTableCell">1</div>
            <div class="DataTableCell"><?php echo $row_career->job_title;?></div>
            <div class="DataTableCell"><?php echo $row_career->location;?></div>
            <div class="DataTableCell"><?php echo $row_career->experience;?></div>
            <div class="DataTableCell"><?php echo $row_career->no_of_posts;?></div>
            <div class="DataTableCell"><a href="job_details.php?pos=<?php echo $row_career->job_id;?>">More</a></div>
            <p></p>
          </div>
 <?php }?>
         
          
          <p>&nbsp;</p>
        </div>
        <?php }?>
      </div>
    </div>
  </div>
</div>
<?php include('sidebar-contact.php');  ?>
<?php include('get-in-touch.php');  ?>
<?php include('browse-by-category.php');  ?>
<!--done-->
<?php include('footer-top-categories.php');  ?>
<!--done-->
<?php include('footer.php');  ?>
</body>
</html>
