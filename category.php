<?php
include('header-category.php'); 
include('header-menu.php'); 

/*echo "<pre>";
print_r($_SERVER);*/

?>
<!--/header menu-->
<div class="page-banner">
  <div class="breadcrumb_pagehead category-header">
    <h1 class="head"><?php echo @$cate_name; ?></h1>
    <ul class="breadcrumb">
      <li><a href="<?php echo $css_path;?>">Home</a></li>
      <?php 
   if(@$p_c>0) {   ?>
      <li><a href="<?php echo $css_path.$front->Cat_URL_Main($p_c); ?>"><?php echo $front->category_name($p_c); ?></a></li>
      <li><a href="<?php echo @$css_path.$front->Cat_URL_Main($p_c).@$front->Cat_URL_Main($cat_id); ?>"><?php echo @$cate_name; ?></a></li>
      <?php } else {?>
      <li><a href="<?php echo $css_path.$front->Cat_URL_Main($cat_id); ?>"><?php echo @$cate_name; ?></a></li>
      <?php } ?>
    </ul>
  </div>
  <?php /*?>  <img src="<?php echo $css_path_images;?><?php echo @$banner_image_new; ?>" alt="<?php echo @$cate_name; ?>" ><?php */?>
  <img class="lazy img-responsive" src="<?php echo $css_path_images;?>/images/blank.png" data-src="<?php echo $css_path_images.@$banner_image_new; ?>" alt="<?php echo @$cate_name; ?>" >
  <div class="container category-header">
    <?php 
   if(@$p_c>0) { 
    ?>
    <h2 style=" text-align:  left;"><?php echo @$heading_2; ?>
      <?php //echo $cate_name; ?>
    </h2>
    <?php } else {?>
    <h2 ><?php echo @$heading_2; ?>
      <?php //echo @$cate_name; ?>
    </h2>
    <?php } ?>
  </div>
</div>

<div class="page-content">
  <div class="container">
    <div class="row">
    <div class="col-md-12 padtpbtm30 table-responsive">
<!--      <div class="col-md-12 padtpbtm30">-->
       <?php echo @$cate_description; ?>
      </div>
    </div>
  </div>
</div>
<?php if(@$p_c>0) {  ?>
<section class="products-hp">
  <div class="container">
    <div class="explore_heading"><?php echo $sub_cat_heading 		= stripslashes($front->section_heading("sub_cat_heading"));?><!--Explore the Products--></div>
    <div class="product-listhp">
      <?php
			$sql_sub_cat_pro	 = "select tbl_products_1.pro_title, tbl_products_1.pro_short_desc, tbl_products_1.pro_id from tbl_products_1 
			left join tbl_products_categories
			on tbl_products_categories.pro_id = tbl_products_1.pro_id
			where tbl_products_categories.cate_id ='".$cat_id."' AND 
			tbl_products_1.deleteflag = 'active' and tbl_products_1.status='active' order by tbl_products_1.sort_order asc";
			$rs_sub_cat_pro  	 = mysqli_query($GLOBALS["___mysqli_ston"],$sql_sub_cat_pro);
			$s=0;
			while($rs_cat_pro_num_sub  = mysqli_fetch_object($rs_sub_cat_pro)) {
			$s++;
		  ?>
     <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="moreBox"><a href="<?php echo $css_path.$front->Pro_URL($rs_cat_pro_num_sub->pro_id); ?>">
          <?php 
	 //echo $rs_cat_pro_num_sub->pro_id;
//	  echo "aaa".$front->pro_featured_image($rs_cat_pro_num_sub->pro_id);
	  if($front->pro_featured_image($rs_cat_pro_num_sub->pro_id)=='') {?>
          <img class="lazy img-responsive" src="<?php echo $css_path_images;?>/images/blank.png" data-src="<?php echo $css_path_images; ?><?php echo "uploads/pro_image/small/".$front->get_PROImages($rs_cat_pro_num_sub->pro_id); ?>" alt="<?php echo $rs_cat_pro_num_sub->pro_title; ?>" border="0" >
          <?php /*?>        <img src="<?php echo $css_path_images; ?><?php echo "uploads/pro_image/small/".$front->get_PROImages($rs_cat_pro_num_sub->pro_id); ?>" alt="<?php echo $rs_cat_pro_num_sub->pro_title; ?>" border="0" ><?php */?>
          <?php } else {?>
          <img class="lazy img-responsive" src="<?php echo $css_path_images;?>/images/blank.png" data-src="<?php echo $css_path_images; ?><?php echo $front->pro_featured_image($rs_cat_pro_num_sub->pro_id); ?>" alt="<?php echo $rs_cat_pro_num_sub->pro_title; ?>" border="0" >
          <?php /*?>      <img src="<?php echo $css_path_images; ?><?php echo $front->pro_featured_image($rs_cat_pro_num_sub->pro_id); ?>" alt="<?php echo $rs_cat_pro_num_sub->pro_title; ?>" border="0" ><?php */?>
          <?php } ?>
          </a>
          <div class="prd-details-hp">
            <h3 class="head"><a href="<?php echo $css_path.$front->Pro_URL($rs_cat_pro_num_sub->pro_id); ?>"><?php echo $rs_cat_pro_num_sub->pro_title; ?></a></h3>
            <p class="twolinetext"><?php echo strip_tags($rs_cat_pro_num_sub->pro_short_desc); ?></p>
          </div>
        </div>
      </div>
      <?php 
       if($s%2==0)
	  {
	  echo '<div class="clear"></div>';
		  }}?>
    </div>
  </div>
</section>
<?php } else { ?>
<section class="products-hp">
  <div class="container">
    <div class="explore_heading"><?php echo $cat_heading 		= stripslashes($front->section_heading("cat_heading"));?><!--Explore the Categories--></div>
    <div class="product-listhp">
      <?php
			$sql_sub_cat_pro	 = "select featured_image,short_desc,cate_name,banner_image,cat_permalink_n,cate_id from tbl_category where parent_id ='".@$cat_id."' AND cate_status='active' AND deleteflag='active' order by sort_order";
			$rs_sub_cat_pro  	 = mysqli_query($GLOBALS["___mysqli_ston"],$sql_sub_cat_pro);
			$c=0;
			while($rs_cat_pro_num_sub  = mysqli_fetch_object($rs_sub_cat_pro)) {$c++;
			if($rs_cat_pro_num_sub->featured_image==NULL){
$cat_featured_image=$rs_cat_pro_num_sub->banner_image;
}
else
{
$cat_featured_image=$rs_cat_pro_num_sub->featured_image;
}
  ?>
      <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="moreBox">
          <?php /*?><a href="<?php echo $css_path.$pid."/".$rs_cat_pro_num_sub->cat_permalink_n;  ?>/"><img src="<?php //echo $css_path;?><?php echo $css_path_images.$cat_featured_image; ?>" alt="<?php echo $rs_cat_pro_num_sub->cate_name; ?>" border="0" ></a><?php */?>
          <a href="<?php echo $css_path.$pid."/".$rs_cat_pro_num_sub->cat_permalink_n;  ?>/"><img class="lazy img-responsive"   data-src="<?php //echo $css_path;?><?php echo $css_path_images.$cat_featured_image; ?>" alt="<?php echo $rs_cat_pro_num_sub->cate_name; ?>" border="0" ></a>
          <div class="prd-details-hp">
            <h3 class="head"><a href="<?php echo $css_path.$pid."/".$rs_cat_pro_num_sub->cat_permalink_n;  ?>/"><?php echo $rs_cat_pro_num_sub->cate_name; ?></a></h3>
            <p class="twolinetext"><?php echo strip_tags($rs_cat_pro_num_sub->short_desc); ?></p>
          </div>
        </div>
      </div>
      <?php 
 if($c%2==0)
{
 echo '       <div class="clear"></div>';
 }}?>
    </div>
  </div>
</section>
<?php }?>
<?php include('sidebar-contact.php');  ?>
<?php include('get-in-touch.php');  ?>
<?php include('browse-by-category.php');  ?>
<!--done-->
<?php include('footer-top-categories.php');  ?>
<!--done-->
<?php include('footer.php');  ?>
</body>
</html>
<?php
/*
######## Your Website Content Ends here Category Page#########
if (!is_dir($cache_folder)) { //create a new folder if we need to
    mkdir($cache_folder);
}
if(!$ignore){
   $fp = fopen($cache_file, 'w');  //open file for writing
fwrite($fp, ob_get_contents()); //write contents of the output buffer in Cache file
    fclose($fp); //Close file pointer
	}
ob_end_flush(); //Flush and turn off output buffering

*/
?>