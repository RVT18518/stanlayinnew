<?php include('header-cms.php'); ?>
<style>
table tr.heading td {
	border: 1px solid #CECECE!important;
	padding: 10px 10px!important;
	font-family:'Open Sans', Arial, sans-serif!important;
	font-weight:700!important;
	font-size: 14px!important;
	background: #ed3237!important;
	color: #fff!important;
}
table tr.sub_content td {
	font-family:'Open Sans', Arial, sans-serif!important;
	font-weight: 700!important;
	border: 1px solid #CECECE!important;
	display: table-cell!important;
	padding: 10px 10px!important;
	font-size: 14px!important;
}
</style>

<body class="homepage">

<!--header menu-->

<?php include('header-menu.php');  ?>
<?php 
$addmore_links=$front->getContentpage($page ,'addmore_links');
$addmore_links_explode=explode(",",$addmore_links);
$addmore_links_count=count($addmore_links_explode);
?>
<?php $banner_img_path=$front->getContentpage($page ,'image_path');
if($banner_img_path=='')
{
$banner_img_path="images/page-banner.jpg";//$front->getContentpage($page ,'image_path');
}
else
{
$banner_img_path=$css_path_images.$front->getContentpage($page ,'image_path');
}
?>
<!--/header menu-->

<div class="page-banner"> <img src="<?php echo $banner_img_path;?>" alt="image">
  <ul class="breadcrumb">
    <li><a href="<?php echo $css_path;?>">Home</a></li>
    <li><?php echo stripslashes($front->getContentpage($page ,'pages_name'));?></li>
  </ul>
  <h1><?php echo stripslashes($front->getContentpage($page ,'pages_name'));?></h1>
</div>
<?php if($pid=='contact-us')
{
	$query 			= "deleteflag = 'active' and status = 'active' ";
	$rs   = $front->selectWhere("tbl_contact_us_page_location", $query);
	//$rs  			 = $front->selectWhere('tbl_contact_us_page_location','id');
	
	if(mysqli_num_rows($rs)>0)
	{
		while($row[]  = mysqli_fetch_object($rs))
		{
			
		/*$location[]		= $row->location;
		$address[]		= $row->address;
		$image_path[]		= $row->image_path;
		$google_code[]	= $row->google_code;
		$sort_order	= $row->sort_order;*/
		}
		//echo "<pre>";
		$locations=$row;
		//print_r($locations);
	}
	$ctr=count($locations);
	?>
<!--contactus starts-->
<div class="page-content">
  <div class="bg-light">
    <div class="container">
      <div class="clearfix contact-blocks">
        <div class="col-md-6 col-sm-6">
          <h2><strong><?php echo $locations["0"]->location;?></strong></h2>
          <div class="col-md-6 stanlay-building"> <img src="<?php echo $css_path_images.$locations["0"]->image_path;?>" alt=""> </div>
          <div class="col-md-6 stanlay-address"> <!--<strong>Asian Contec Limited</strong><br>
           Asian Centre,<br>
            B-28, Okhla Industrial Area, <br>
            Phase-1, New Delhi &ndash; 110020.<br>
            Tel: +91-11-41860000 (100 lines) <br>
            Fax: +91-11-41860066<br>
            Sales Helpline: +91-11-41406926 <br>
            Email: sales@stanlay.com<br>
            <strong>Web</strong>: <a href="http://www.stanlay.in" target="_blank">www.stanlay.in</a> | <a href="http://www.stanlay.com" target="_blank">www.stanlay.com</a>--> <?php echo $locations["0"]->address;?></div>
        </div>
        <div class="col-md-6 col-sm-6 stanlay-map"> 
          
          <!-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3505.2711693824976!2d77.27717231467759!3d28.531567982457716!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390ce1599cb27a7d%3A0xab3370fe886793fd!2sAsian+Contec+Ltd!5e0!3m2!1sen!2sin!4v1521989107239" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>--><?php echo $locations["0"]->google_code;?> </div>
      </div>
      
      <!--Head Office Ends-->
      
      <?php 
?>
      <div class="clearfix contact-blocks">
        <div class="col-md-6 col-sm-6">
          <h2><strong><?php echo $locations["1"]->location;?>:</strong></h2>
          <div class="col-md-6 stanlay-building"> <img src="<?php echo $css_path_images.$locations["1"]->image_path;?>" alt=""> </div>
          <div class="col-md-6 stanlay-address"> <!--<strong>Asian Contec Limited</strong><br>
            44A, E.P.I.P, Phase &ndash; 1,<br>
			Jharmajri Baddi,<br>
			Dist. Solan,<br>
			Himachal Pradesh--><?php echo $locations["1"]->address;?></div>
        </div>
        <div class="col-md-6 col-sm-6 stanlay-map"> 
          
          <!-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3422.76836339465!2d76.82643651474271!3d30.921098181571864!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390f8acc8bc88747%3A0xfd759c8df7f32b99!2sJharmajri+Bus+Stand!5e0!3m2!1sen!2sin!4v1524040180420" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>--><?php echo $locations["1"]->google_code;?> </div>
      </div>
      
      <!--Works Office Ends--> 
      
    </div>
  </div>
  <div class="bg-light">
    <div class="container regional-block">
      <h2><strong>Regional Offices:</strong></h2>
      <?php for($l=2; $l<$ctr-1; $l++){?>
      <div class="clearfix contact-blocks">
        <div class="col-md-6 col-sm-6">
          <div class="col-md-6 stanlay-building"> <img src=" <?php echo $css_path_images.$locations[$l]->image_path;?>" alt=""> </div>
          <div class="col-md-6 stanlay-address">
            <h3><?php echo $locations[$l]->location;?></h3>
            <?php $address_locations=$locations[$l]->address;?>
            <?php $address_locations1=preg_replace('/font-family.+?;/', "", $address_locations);?>
            <?php $address_locations2=preg_replace('/font-size.+?;/', "", $address_locations1);?>
            <?php $address_locations3=str_replace("style=","style=", $address_locations2);?>
            <?php echo $address_locations4=str_replace("MsoNormalTable","table-responsive table-width100per", $address_locations3);?> </div>
        </div>
        <div class="col-md-6 col-sm-6 stanlay-map"> <?php echo $locations[$l]->google_code;?> </div>
      </div>
      <hr>
      <?php //echo $l;
	  }?>
    </div>
  </div>
</div>

<!--contactus ends-->

<?php } else {?>
<div class="page-content">
  <div class="container-fluid"> <?php echo stripslashes($front->getContentpage($page ,'pages_html_text')); ?> </div>
  <p>&nbsp;</p>
</div>
<?php }?>
<div class="page-content">
  <div class="container-fluid">
    <?php
	if($addmore_links_count>0)
	{
	?>
    <p>&nbsp;</p>
    <div class="accordion">
      <?php
	for($i=0; $i<$addmore_links_count;$i++)
	{
		if($addmore_links_explode[$i]!='0')
		{
?>
      <h4 class="accordion-toggle"><?php echo stripslashes($front->getContentpage($addmore_links_explode[$i] ,'pages_name')); ?></h4>
      <div class="accordion-content">
        <p>
          <?php 
					
		if($addmore_links_explode[$i]=='5')
		{
			
	$query 			= "deleteflag = 'active' ";
	$rs   = $front->selectWhere("tbl_contact_us_page_location", $query);
	//$rs  			 = $front->selectWhere('tbl_contact_us_page_location','id');
	
	if(mysqli_num_rows($rs)>0)
	{
		while($row[]  = mysqli_fetch_object($rs))
		{
			
		/*$location[]		= $row->location;
		$address[]		= $row->address;
		$image_path[]		= $row->image_path;
		$google_code[]	= $row->google_code;
		$sort_order	= $row->sort_order;*/
		}
		//echo "<pre>";
		$locations=$row;
		//print_r($locations);
	}
	$ctr=count($locations);

	?>
          
          <!--contactus starts-->
          
        <div class="page-content">
          <div class="bg-light">
            <div class="container">
              <div class="clearfix contact-blocks">
                <div class="col-md-6 col-sm-6">
                  <h2><strong><?php echo $locations["0"]->location;?></strong></h2>
                  <div class="col-md-6 stanlay-building"> <img src="<?php echo $css_path_images.$locations["0"]->image_path;?>" alt=""> </div>
                  <div class="col-md-6 stanlay-address"> <!--<strong>Asian Contec Limited</strong><br>
           Asian Centre,<br>
            B-28, Okhla Industrial Area, <br>
            Phase-1, New Delhi &ndash; 110020.<br>
            Tel: +91-11-41860000 (100 lines) <br>
            Fax: +91-11-41860066<br>
            Sales Helpline: +91-11-41406926 <br>
            Email: sales@stanlay.com<br>
            <strong>Web</strong>: <a href="http://www.stanlay.in" target="_blank">www.stanlay.in</a> | <a href="http://www.stanlay.com" target="_blank">www.stanlay.com</a>--> <?php echo $locations["0"]->address;?></div>
                </div>
                <div class="col-md-6 col-sm-6 stanlay-map"> 
                  
                  <!-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3505.2711693824976!2d77.27717231467759!3d28.531567982457716!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390ce1599cb27a7d%3A0xab3370fe886793fd!2sAsian+Contec+Ltd!5e0!3m2!1sen!2sin!4v1521989107239" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>--><?php echo $locations["0"]->google_code;?> </div>
              </div>
              
              <!--Head Office Ends-->
              
              <?php 
?>
              <div class="clearfix contact-blocks">
                <div class="col-md-6 col-sm-6">
                  <h2><strong><?php echo $locations["1"]->location;?>:</strong></h2>
                  <div class="col-md-6 stanlay-building"> <img src="<?php echo $css_path_images.$locations["1"]->image_path;?>" alt=""> </div>
                  <div class="col-md-6 stanlay-address"> <!--<strong>Asian Contec Limited</strong><br>
            44A, E.P.I.P, Phase &ndash; 1,<br>
			Jharmajri Baddi,<br>
			Dist. Solan,<br>
			Himachal Pradesh--><?php echo $locations["1"]->address;?></div>
                </div>
                <div class="col-md-6 col-sm-6 stanlay-map"> 
                  
                  <!-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3422.76836339465!2d76.82643651474271!3d30.921098181571864!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390f8acc8bc88747%3A0xfd759c8df7f32b99!2sJharmajri+Bus+Stand!5e0!3m2!1sen!2sin!4v1524040180420" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>--><?php echo $locations["1"]->google_code;?> </div>
              </div>
              
              <!--Works Office Ends--> 
              
            </div>
          </div>
          <div class="container regional-block">
            <h2><strong>Regional Offices:</strong></h2>
            <?php for($l=2; $l<$ctr-1; $l++){?>
            <div class="clearfix contact-blocks">
              <div class="col-md-6 col-sm-6">
                <div class="col-md-6 stanlay-building"> <img src=" <?php echo $css_path_images.$locations[$l]->image_path;?>" alt=""> </div>
                <div class="col-md-6 stanlay-address">
                  <h3><?php echo $locations[$l]->location;?></h3>
                  
                  <!--  <strong>Asian Contec Limited</strong><br>
		  Regus Prince Infocity II,<br>
		  Level - 1,283/3 &amp; 283/4 Rajiv Gandhi Road (OMR), Kandanchavady, Chennai 600 096 Tamilnadu India	--> 
                  
                  <?php echo $locations[$l]->address;?> </div>
              </div>
              <div class="col-md-6 col-sm-6 stanlay-map"> 
                
                <!--<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3806.4057988587833!2d78.48765031487711!3d17.44028098804691!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x194258bffa4bd8c2!2sAsian+Contec+Ltd!5e0!3m2!1sen!2sin!4v1524040245100" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>--> 
                
                <?php echo $locations[$l]->google_code;?> </div>
            </div>
            <hr>
            <?php }?>
          </div>
        </div>
        
        <!--contactus ends-->
        
        <?php }
		//career page
		if($addmore_links_explode[$i]=='11')
		{
			echo stripslashes($front->getContentpage($addmore_links_explode[$i] ,'pages_html_text'));
			?>
        <div style="max-width:100%; overflow-x:auto;">
          <?php

$sql_career="Select * from tbl_jobs where deleteflag='active' and status='active' order by date_from asc ";
$rs_career=mysqli_query($GLOBALS["___mysqli_ston"],$sql_career);

$num_rows=mysqli_num_rows($rs_career);
if($num_rows>0){

?>
          <div class="DatalTable">
            <div class="DatalTableBody">
              <div class="DataTableRow">
                <div class="DataTableCell" style="background: #ed3237; color:#fff;"><strong>Sr. No.</strong></div>
                <div class="DataTableCell" style="background: #ed3237; color:#fff;"><strong>Job Title</strong></div>
                <div class="DataTableCell" style="background: #ed3237; color:#fff;"><strong>Location</strong></div>
                <div class="DataTableCell" style="background: #ed3237; color:#fff;"><strong>Experience</strong></div>
                <div class="DataTableCell" style="background: #ed3237; color:#fff;"><strong>No of Post</strong></div>
                <div class="DataTableCell" style="background: #ed3237; color:#fff;"><strong>Full Details</strong></div>
                <p></p>
              </div>
              <?php
$sql_career="Select * from tbl_jobs where deleteflag='active' and status='active' order by date_from asc ";
$rs_career=mysqli_query($GLOBALS["___mysqli_ston"],$sql_career);
while($row_career=mysqli_fetch_object($rs_career))
{

?>
              <div class="DataTableRow">
                <div class="DataTableCell">1</div>
                <div class="DataTableCell"><?php echo $row_career->job_title;?></div>
                <div class="DataTableCell"><?php echo $row_career->location;?></div>
                <div class="DataTableCell"><?php echo $row_career->experience;?></div>
                <div class="DataTableCell"><?php echo $row_career->no_of_posts;?></div>
                <div class="DataTableCell"><a href="job_details.php?pos=<?php echo $row_career->job_id;?>">More</a></div>
                <p></p>
              </div>
              <?php }?>
              <p>&nbsp;</p>
            </div>
          </div>
          <?php }?>
        </div>
        <?php
		}
		else{
					echo stripslashes($front->getContentpage($addmore_links_explode[$i] ,'pages_html_text'));
		}?>
        </p>
      </div>
      <?php
	}}
	?>
      
      <!--<h4 class="accordion-toggle">Quality</h4>
			<div class="accordion-content">
				<p>
					Show your content here...
				</p>
			</div>
			
			<h4 class="accordion-toggle">Warrenty Programs</h4>
			<div class="accordion-content">
				<p>
					Show your content here...
				</p>
			</div>
			
			<h4 class="accordion-toggle">Stocking</h4>
			<div class="accordion-content">
				<p>
					Show your content here...
				</p>
			</div>
			<h4 class="accordion-toggle">Manufacturing</h4>
			<div class="accordion-content">
				<p>
					Show your content here...
				</p>
			</div>
			<h4 class="accordion-toggle">Contact Us</h4>
			<div class="accordion-content">
				<p>
					Show your content here...
				</p>
			</div>--> 
      
    </div>
    <p>&nbsp;</p>
    <?php

	}
	?>
  </div>
</div>
<?php include('sidebar-contact.php');  ?>
<?php include('get-in-touch.php');  ?>
<?php include('browse-by-category.php');  ?>

<!--done-->

<?php include('footer-top-categories.php');  ?>

<!--done-->

<?php include('footer.php');  ?>
<script language="javascript">
		$(document).ready(function() {
	$('.accordion').find('.accordion-toggle').click(function() {
		$(this).next().slideToggle('600');
		$(".accordion-content").not($(this).next()).slideUp('600');
	});
	$('.accordion-toggle').on('click', function() {
		$(this).toggleClass('active').siblings().removeClass('active');
	});
});
</script>
</body>
</html>
<?php
/*
######## Your Website Content Ends here Product Page#########
if (!is_dir($cache_folder)) { //create a new folder if we need to
    mkdir($cache_folder);
}

if(!$ignore){
    $fp = fopen($cache_file, 'w');  //open file for writing
   fwrite($fp, ob_get_contents()); //write contents of the output buffer in Cache file
    fclose($fp); //Close file pointer
}
ob_end_flush(); //Flush and turn off output buffering
*/
?>