<!-- (B) PERIOD SELECTOR -->
<script language="javascript" src="../js/zone_state.js" type="text/javascript"></script>
<div id="calPeriod">
  <?php
      // (B1) MONTH SELECTOR
      // NOTE: DEFAULT TO CURRENT SERVER MONTH YEAR
      $months = [
        1 => "January", 2 => "Febuary", 3 => "March", 4 => "April",
        5 => "May", 6 => "June", 7 => "July", 8 => "August",
        9 => "September", 10 => "October", 11 => "November", 12 => "December"
      ];
      $monthNow = date("m");
      echo "<select id='calmonth'>";
      foreach ($months as $m=>$mth) {
        printf("<option value='%s'%s>%s</option>",
          $m, $m==$monthNow?" selected":"", $mth
        );
      }
      echo "</select>";

      // (B2) YEAR SELECTOR
      echo "<input type='number' id='calyear' value='".date("Y")."'/>";
    ?>
</div>

<!-- (C) CALENDAR WRAPPER -->
<div id="calwrap"></div>

<!-- (D) EVENT FORM -->
<div id="calblock">
  <form id="calform">
    <input type="hidden" id="evtid"/>
    <table width="100%" border="0">
      <tr>
        <td colspan="4"><h4 class="modal-title">Add Task</h4></td>
      </tr>
      <tr>
        <td><label for="start">Date Start</label></td>
        <td><input type="date" id="evtstart" class="inpuTxt" required/>
          <input type="hidden"  class="inpuTxt" id="account_manager" value="<?php echo $_SESSION["AdminLoginID_SET"];?>" /></td>
        <td><label for="end">Date End</label></td>
        <td><input type="date" id="evtend" class="inpuTxt" required/></td>
      </tr>
      <tr>
        <td><label for="txt">Customer</label></td>
        <td><?php 
	$query1	= " deleteflag = 'active' and parent_id = 0 order by comp_name asc";	
	$rsCate1 = $s->selectWhere('tbl_comp', $query1);
?>

          <select  name="customer"  id="customer" class="inpuTxtSelect" style="width:170px" required  onchange="htmlData('calender_co_offers.php', 'ch='+this.value);">
            <option value=''>Select Company</option>
            <?php 
				 //$CateParent
	if(mysqli_num_rows($rsCate1)>0)
	{    
//	$i=1;
		while($rowCate1 = mysqli_fetch_object($rsCate1))
		{
		
?>
            <option value='<?php echo $rowCate1->id;?>' <?php if($rowCate1->id == $CateParent){echo "selected='selected'";}?>><?php echo stripslashes($rowCate1->comp_name);?></option>
            <?php //showSubCompany($rowCate1->id,'&nbsp;',$CateParent);    
		}
	}
?>
          </select></td>
          
          <td><label for="lead_type"> Type</label></td>
        <td id="txtResult">
          <select id="lead_type" name="lead_type" class="inpuTxtSelect" required >
            <option value="">Select</option>
            
            <option value='Lead' >Lead</option>
            <option value='Opportunity' >Opportunity</option>
            
          </select></td>
          </tr>
          <tr>
          
          
                  <td><label for="status"> Status</label></td>
        <td>
          <select id="status" class="inpuTxtSelect" required >
           
            <option value='Pending' >Pending</option>
            <option value='Completed' >Completed</option>
            
          </select></td>
          
          
        <td><label for="txt">Task Type</label></td>
        <td><?php 
	$query2	= " deleteflag = 'active'   order by tasktype_name";	
	$rsCate2 = $s->selectWhere('tbl_tasktype_master', $query2);
?>
          <select id="evttxt" class="inpuTxtSelect" required >
            <option value="">Select</option>
            <?php 
 //$CateParent
	if(mysqli_num_rows($rsCate2)>0)
	{    
		while($rowCate2 = mysqli_fetch_object($rsCate2))
		{        
?>
            <option value='<?php echo $rowCate2->tasktype_abbrv;?>' ><?php echo stripslashes($rowCate2->tasktype_name);?></option>
            <?php
				//showSubApplicationCategories($rowCate1->application_id,'&nbsp;',$app_cat_id_search);    
		}
	}
?>
          </select></td>
      </tr>
      <tr>
        <td><label for="txt">Product <br />Category</label></td>
        <td id="txtResultCity" ><?php 
	$query1	= " deleteflag = 'active' and parent_id1 = 0 order by application_name";	
	$rsCate1 = $s->selectWhere('tbl_application', $query1);
?>
          <select name="product_category" class="inpuTxtSelect" id="product_category"  style="max-width:170px;" required    >
            <option value="">Select Product Group</option>
            <?php 
 //$CateParent
	if(mysqli_num_rows($rsCate1)>0)
	{    
		while($rowCate1 = mysqli_fetch_object($rsCate1))
		{        
?>
            <option value='<?php echo $rowCate1->application_id;?>' ><?php echo stripslashes($rowCate1->application_name);?></option>
            <?php
				//showSubApplicationCategories($rowCate1->application_id,'&nbsp;',$app_cat_id_search);    
		}
	}
?>
          </select></td>
        <td><label for="txt">Remarks</label></td>
        <td><textarea id="task_type" class="inpuTxt" required></textarea></td>
      </tr>
      <tr>
        <td><label for="txt">Opportunity <br />Value</label></td>
        <td><?php 
	$query3	= " deleteflag = 'active'   order by opportunity_value_name";	
	$rsCate3 = $s->selectWhere('tbl_opportunity_value_master', $query3);
?>
          <select id="opportunity_value" class="inpuTxtSelect" required>
            <option value="">Select</option>
            <!-- <option value="1">1 to 500000</option>
            <option value="2">500000 to 5000000</option>-->
            <?php 
 //$CateParent
	if(mysqli_num_rows($rsCate3)>0)
	{    
		while($rowCate3 = mysqli_fetch_object($rsCate3))
		{        
?>
            <option value='<?php echo $rowCate3->opportunity_value_id;?>' ><?php echo stripslashes($rowCate3->opportunity_value_name);?></option>
            <?php
				//showSubApplicationCategories($rowCate1->application_id,'&nbsp;',$app_cat_id_search);    
		}
	}
?>
          </select></td>
        <td><label for="color">Color</label></td>
        <td><input type="color" id="evtcolor" class="inpuTxt" value="#e4edff" required/></td>
      </tr>
      <tr>
        <td><input type="submit" id="calformsave" value="Save"/></td>
        <td><input type="button" id="calformdel" value="Delete"/></td>
        <td><input type="button" id="calformcx" value="Cancel"/></td>
        <td>&nbsp;</td>
      </tr>
    </table>
  </form>
</div>

</body></html>