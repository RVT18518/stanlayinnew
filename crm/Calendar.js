function converts(str) {
  var date = new Date(str),
    mnth = ("0" + (date.getMonth() + 1)).slice(-2),
    day = ("0" + date.getDate()).slice(-2);
  return [day, mnth, date.getFullYear()].join("-");
}

function convertstartdate(str) {
	var now     = new Date(); 
    var date = new Date(str),
        mnth = ("0" + (date.getMonth()+1)).slice(-2),
        day  = ("0" + date.getDate()).slice(-2);
        hourss  = ("0" + now.getHours()).slice(-2);
        minutess = ("0" + now.getMinutes()).slice(-2);
	//	alert(hourss);
    return [ day, '-',mnth,'-', date.getFullYear(),' ', hourss,':', minutess,':', '00' ].join("");
}

function convertenddate(str) {
	var now     = new Date(); 
    var date = new Date(str),
        mnth = ("0" + (date.getMonth()+1)).slice(-2),
        day  = ("0" + date.getDate()).slice(-2);
        hourss  = ("0" + (now.getHours()+1)).slice(-2);
        minutess = ("0" + now.getMinutes()).slice(-2);
		//alert(hourss);
    return [ day, '-',mnth,'-', date.getFullYear(),' ', hourss,':', minutess,':', '00' ].join("");
}

document.addEventListener('DOMContentLoaded', function() {
    var url ='./';
    $('body').on('click', '.datetimepicker', function() {
        $(this).not('.hasDateTimePicker').datetimepicker({
            controlType: 'select',
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd-mm-yy",
           // timeFormat: 'HH:mm:ss',/*removed seconds option on 1 Nov 2021*/
            yearRange: "1900:+10",
            showOn:'focus',
            firstDay: 1
        }).focus();
    });
	$('#startDate')
        .datetimepicker({
			showTodayButton: true,
			format: 'dd-mm-yy',
			
        });
		
	
$('#endDate')
 .datetimepicker({
			showTodayButton: true,
			format: 'dd-mm-yy'
    });
		$("#startDate").on("datetimepicker.change", function (e) {
        $('#endDate').data("datetimepicker").minDate(e.date);
    });

    	$("#endDate").on("datetimepicker.change", function (e) {
        $('#startDate').data("datetimepicker").maxDate(e.date);
 });
/*current date time starts */	
var d = new Date();
var yearss = d.getFullYear();
var month = d.getMonth();
var day = d.getDate();
var hours = d.getHours()+1;
var mints = d.getMinutes();	

//	$("#startDate").datepicker().datepicker('setDate', new Date());
	
	//$("#endDate").datepicker().datepicker('setDate', new Date(yearss,month,day,hours,mints));	
//var selectedDate = $('#endDate').data('date');

//	 $('#StartDate').val("Y-MM-DD HH:mm:ss");
  // $('#EndDate').val("Y-MM-DD HH:mm:ss");
/*current date time ends */		
    $(".colorpicker").colorpicker();
    var calendarEl = document.getElementById('calendar');
    var calendar = new FullCalendar.Calendar(calendarEl, {
        plugins: ['interaction', 'dayGrid', 'timeGrid', 'list'],
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'dayGridMonth,timeGridWeek,timeGridDay,listMonth'
        },
        navLinks: true, // can click day/week names to navigate views
        businessHours: true, // display business hours
        editable: true,
			/*onclick add new event on day click starts */
			/*ref: link for this https://jsfiddle.net/milz/fotLoLy9/*/
		selectable: true,
        selectHelper: true,
	
		select: function(event, start, end) {
//			format: 'MM/DD/YYYY'
			  var start = ($("#startDate"), event.start);
			//  alert($('#startDate').val());
//alert(start);
//alert(convertenddate(start));
$("#startDate").val(convertstartdate(start));
$("#endDate").val(convertenddate(start));
//console.log(convert(start))
//$('#addeventmodal').find('#startDate').val(event.startDate);
              $('#addeventmodal').modal('show');
            },
			/*onclick add new event on day click ends*/
        //uncomment to have a default date
        //defaultDate: '2020-04-07',
        events: url+'api/load.php',
        eventDrop: function(arg) {
            var start = arg.event.start.toDateString()+' '+arg.event.start.getHours()+':'+arg.event.start.getMinutes()+':'+arg.event.start.getSeconds();
            if (arg.event.end == null) {
                end = start;
            } else {
                var end = arg.event.end.toDateString()+' '+arg.event.end.getHours()+':'+arg.event.end.getMinutes()+':'+arg.event.end.getSeconds();
            }

            $.ajax({
              url:url+"api/update.php",
              type:"POST",
              data:{id:arg.event.id, start:start, end:end},
            });
        },
        eventResize: function(arg) {
            var start = arg.event.start.toDateString()+' '+arg.event.start.getHours()+':'+arg.event.start.getMinutes()+':'+arg.event.start.getSeconds();
            var end = arg.event.end.toDateString()+' '+arg.event.end.getHours()+':'+arg.event.end.getMinutes()+':'+arg.event.end.getSeconds();

            $.ajax({
              url:url+"api/update.php",
              type:"POST",
              data:{id:arg.event.id, start:start, end:end},
            });
        },
		
	
		 eventClick: function(arg) {
            var id = arg.event.id;
            
            $('#editEventId').val(id);
            $('#deleteEvent').attr('data-id', id); 

            $.ajax({
              url:url+"api/getevent.php",
              type:"POST",
              dataType: 'json',
              data:{id:id},
              success: function(data) {
                    $('#editEventTitle').val(data.title);
                    $('#editStartDate').val(data.start);
                    $('#editEndDate').val(data.end);
                    $('#editproduct_category').val(data.product_category);
                    $('#editaccount_manager').val(data.account_manager);
					$('#editcustomer').val(data.customer);
					$('#editevttxt').val(data.evttxt);
					$('#editlead_type').val(data.lead_type);
					$('#editopportunity_value').val(data.opportunity_value);
					$('#editstatus').val(data.status);
                    $('#editColor').val(data.color);
                    $('#editTextColor').val(data.textColor);
                    $('#editeventmodal').modal();
                }
            });

            $('body').on('click', '#deleteEvent', function() {
                if(confirm("Are you sure you want to remove it?")) {
                    $.ajax({
                        url:url+"api/delete.php",
                        type:"POST",
                        data:{id:arg.event.id},
                    }); 

                    //close model
                    $('#editeventmodal').modal('hide');

                    //refresh calendar
                    calendar.refetchEvents();         
                }
            });
			

            
           // calendar.refetchEvents();
        }
    });

    calendar.render();

    $('#createEvent').submit(function(event) {

        // stop the form refreshing the page
        event.preventDefault();

        $('.form-group').removeClass('has-error'); // remove the error class
        $('.help-block').remove(); // remove the error text

        // process the form
        $.ajax({
            type        : "POST",
            url         : url+'api/insert.php',
            data        : $(this).serialize(),
            dataType    : 'json',
            encode      : true
        }).done(function(data) {

            // insert worked
            if (data.success) {
                
                //remove any form data
                $('#createEvent').trigger("reset");

                //close model
                $('#addeventmodal').modal('hide');

                //refresh calendar
                calendar.refetchEvents();

            } else {

                //if error exists update html
                if (data.errors.date) {
                    $('#date-group').addClass('has-error');
                    $('#date-group').append('<div class="help-block">' + data.errors.date + '</div>');
                }

                if (data.errors.title) {
                    $('#title-group').addClass('has-error');
                    $('#title-group').append('<div class="help-block">' + data.errors.title + '</div>');
                }

            }

        });
    });

    $('#editEvent').submit(function(event) {

        // stop the form refreshing the page
        event.preventDefault();

        $('.form-group').removeClass('has-error'); // remove the error class
        $('.help-block').remove(); // remove the error text

        //form data
        var id = $('#editEventId').val();
        var title = $('#editEventTitle').val();
        var start = $('#editStartDate').val();
        var end = $('#editEndDate').val();
		var product_category = $('#editproduct_category').val();
		var account_manager = $('#editaccount_manager').val();
		var customer = $('#editcustomer').val();
		var evttxt = $('#editevttxt').val();
		var lead_type = $('#editlead_type').val();
		var opportunity_value = $('#editopportunity_value').val();
		var status = $('#editstatus').val();
        var color = $('#editColor').val();
        var textColor = $('#editTextColor').val();
		

        // process the form
        $.ajax({
            type        : "POST",
            url         : url+'api/update.php',
            data        : {
                id:id, 
                title:title, 
                start:start,
                end:end,
                product_category:product_category,
                account_manager:account_manager,
				customer:customer,
				evttxt:evttxt,
				lead_type:lead_type,
				opportunity_value:opportunity_value,
				status:status,
                color:color,
                text_color:textColor
            },
            dataType    : 'json',
            encode      : true
        }).done(function(data) {

            // insert worked
            if (data.success) {
                
                //remove any form data
                $('#editEvent').trigger("reset");

                //close model
                $('#editeventmodal').modal('hide');

                //refresh calendar
                calendar.refetchEvents();

            } else {

                //if error exists update html
                if (data.errors.date) {
                    $('#date-group').addClass('has-error');
                    $('#date-group').append('<div class="help-block">' + data.errors.date + '</div>');
                }

                if (data.errors.title) {
                    $('#title-group').addClass('has-error');
                    $('#title-group').append('<div class="help-block">' + data.errors.title + '</div>');
                }

            }

        });
    });
});
