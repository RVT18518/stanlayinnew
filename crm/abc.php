
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <title></title>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <meta name="robots" content="noindex, nofollow">
  <meta name="googlebot" content="noindex, nofollow">
  <meta name="viewport" content="width=device-width, initial-scale=1">


  <script
    type="text/javascript"
    src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"
    
  ></script>

    <link rel="stylesheet" type="text/css" href="/css/result-light.css">

      <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
      <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
      <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">

  <style id="compiled-css" type="text/css">
      .preview-image-container {
  display: inline-block;
  position: relative;
  width: 100%;
  max-width: 100%;
  border: 2px dotted #bbb;
  border-radius: 5px;
}

.preview-image-dummy {
  margin-top: 75%;
}

.preview-image-url {
  display: block;
  margin: auto;
  max-width: 100%;
  max-height: 100%;
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
}

.preview-image-container .close {
  position: absolute;
  top: 2px;
  right: 2px;
  z-index: 2;
  background-color: #FFF;
  padding: 5px 2px 2px;
  color: #000;
  font-weight: bold;
  cursor: pointer;
  opacity: .2;
  text-align: center;
  font-size: 22px;
  line-height: 10px;
  border-radius: 50%;
}

.preview-image-container:hover .close {
  opacity: 1;
}

.preview-image-instruction {
  text-align: center;
  display: block;
  margin: auto;
  max-width: 100%;
  max-height: 100%;
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

.preview-image-instruction > i {
  margin-top: 15%;
}

.preview-image-btn-browse {
  display: table;
  margin: auto;
}

    /* EOS */
  </style>

  <script id="insert"></script>


    <script>
      const customConsole = (w) => {
        const pushToConsole = (payload, type) => {
          w.parent.postMessage({
            console: {
              payload: payload,
              type:    type
            }
          }, "*")
        }

        w.onerror = (message, url, line, column) => {
          // the line needs to correspond with the editor panel
          // unfortunately this number needs to be altered every time this view is changed
          line = line - 70
          if (line < 0){
            pushToConsole(message, "error")
          } else {
            pushToConsole(`[${line}:${column}] ${message}`, "error")
          }
        }

        let console = (function(systemConsole){
          return {
            log: function(){
              let args = Array.from(arguments)
              pushToConsole(args, "log")
              systemConsole.log.apply(this, args)
            },
            info: function(){
              let args = Array.from(arguments)

              pushToConsole(args, "info")
              systemConsole.info.apply(this, args)
            },
            warn: function(){
              let args = Array.from(arguments)
              pushToConsole(args, "warn")
              systemConsole.warn.apply(this, args)
            },
            error: function(){
              let args = Array.from(arguments)
              pushToConsole(args, "error")
              systemConsole.error.apply(this, args)
            },
            system: function(){
              let args = Array.from(arguments)
              pushToConsole(args, "system")
            }
          }
        }(window.console))

        window.console = console

        console.system("Running fiddle")
      }

      if (window.parent){
        customConsole(window)
      }
    </script>
</head>
<body>
    <div class="preview-image-container" style="cursor: default;">
  <input type="file" name="File" id="File" class="image-url form-control" accept="image/*" style="display:none;">
  <div class="preview-image-dummy"></div><img class="preview-image-url">
  <div class="preview-image-instruction" style="background-color: inherit; cursor: default;"><i class="fa fa-4x fa-cloud-upload" aria-hidden="true"></i>
    <div class="preview-image-btn-browse btn btn-primary">Browse</div>
    <p>or drop a file here.</p>
  </div>
</div>


    <script type="text/javascript">//<![CDATA[


function readURL(input) {
  if (input.files && input.files[0]) {
    ShowThumbnail(input.files[0], input);
  }
}

$("input.image-url:file").change(function() {
  readURL(this);
  var i = $("input.image-url:file").files.length;
  console.log(i);
});

$(".preview-image-container").on('dragover', dragover_handler);
$(".preview-image-container").on('dragleave', dragleave_handler);
$(".preview-image-container").on('drop', drop_handler);
$(".image-url-delete").on('click', delete_handler);

$('.preview-image-btn-browse').click(function() {
  $(this).closest('.preview-image-container').find('.image-url').trigger('click');
});

function dragleave_handler(ev) {
  $(ev.target).css("backgroundColor", "");
  $(ev.target).css("cursor", "default");
}

function dragover_handler(ev) {
  // Prevent default select and drag behavior
  ev.preventDefault();
  var dt = ev.originalEvent.dataTransfer;
  dt.dropEffect = "move";

  if (dt.types && (dt.types.indexOf ? dt.types.indexOf('Files') != -1 : dt.types.contains('Files'))) {
    $(ev.target).css("backgroundColor", "#87CEFA");
    $(ev.target).css("cursor", "pointer");
  }
}

function drop_handler(ev) {
  ev.preventDefault();
  var target = $(ev.target);
  target.css("backgroundColor", "inherit");
  target.css("cursor", "default");
  HideInstruction(target);
  // If dropped items aren't files, reject them
  var dt = ev.originalEvent.dataTransfer;
  if (dt.items && dt.items[0]) {
    // Use DataTransferItemList interface to access the file(s)
    if (dt.items[0].kind == "file") {
      var f = dt.items[0].getAsFile();
      ShowThumbnail(f, target);
    }
  } else {
    // Use DataTransfer interface to access the file(s)
    if (dt.files && dt.files[0]) {
      ShowThumbnail(dt.files[0], target);
    }
  }
}


function ShowThumbnail(file, previewDOM) {
  var reader = new FileReader();

  reader.onload = function(e) {
    var imagePreview = $(previewDOM).siblings('.preview-image-url');
    imagePreview.attr('src', e.target.result);
    HideInstruction($(previewDOM).siblings(".preview-image-instruction"));
    ShowDeleteBtn(imagePreview);
  }

  reader.readAsDataURL(file);
}

function HideInstruction(target) {
  $(target).closest(".preview-image-instruction").hide();
}

function ShowInstruction(target) {
  $(target).closest(".preview-image-instruction").show();
}

function ShowDeleteBtn(img) {
  $(img).siblings(".close").show();
}

function HideDeleteBtn(img) {
  $(img).siblings(".close").hide();
}

function delete_handler(ev) {
  var imagePreview = $(ev.target).siblings('.preview-image-url');
  imagePreview.removeAttr('src');
  HideDeleteBtn(imagePreview);
  imagePreview.siblings(".preview-image-instruction").show();
  var fileInput = $(ev.target).siblings("input.image-url:file");

  console.log(fileInput[0].files.length);
  fileInput.val(null);
  console.log(fileInput[0].files.length);
  if ($("input.image-url:file")[0].files.length == 0) {
    console.log("no files selected");
  }
}

// call initialization file
// Is FileAPI available?
if (window.File && window.FileList && window.FileReader) {
  Init();
}

function Init() {

}



  //]]></script>

  <script>
    // tell the embed parent frame the height of the content
    if (window.parent && window.parent.parent){
      window.parent.parent.postMessage(["resultsFrame", {
        height: document.body.getBoundingClientRect().height,
        slug: "12q709b4"
      }], "*")
    }

    // always overwrite window.name, in case users try to set it manually
    window.name = "result"
  </script>

    <script>
      let allLines = []

      window.addEventListener("message", (message) => {
        if (message.data.console){
          let insert = document.querySelector("#insert")
          allLines.push(message.data.console.payload)
          insert.innerHTML = allLines.join(";\r")

          let result = eval.call(null, message.data.console.payload)
          if (result !== undefined){
            console.log(result)
          }
        }
      })
    </script>

</body>
</html>
