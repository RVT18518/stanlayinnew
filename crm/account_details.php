<?php
	$data_action = $_REQUEST["action"];
	$pcode 		 = $_REQUEST["pcode"];
	if($_REQUEST["action"] == "update" || $_REQUEST["action"] == "insert")
	{
		
		$dataArray["seller_salutation"]		= $_REQUEST["salutation"];
		$dataArray["seller_fname"]			= addslashes($_REQUEST["seller_fname"]);
		$dataArray["seller_lname"]			= addslashes($_REQUEST["seller_lname"]);
		$dataArray["seller_desc"]			= addslashes($_REQUEST["seller_descr"]);
		$dataArray["status"]				= $_REQUEST["status"];
		$dataArray["seller_email"]			= $email = $_REQUEST["seller_email"];
		$dataArray["percent_commission"]	= $_REQUEST["percent_commission"];
		$dataArray["created_by"]			= $_SESSION["AdminLoginID_SET"];
	}
	if($_REQUEST["action"] == "update")
	{
		/*if($_REQUEST["state"]!="" && $_REQUEST["state"]!='0')
		{*/
			$result = $s->editRecord('tbl_seller',$dataArray,'seller_id',$pcode);
			if($result == 0)
			{
				$msg ='record_update';
			}
			else if($result == 1)
			{
				$msg ='record_not_update';
			}
			$s->pageLocation("index.php?pagename=add_general_information&subpagename=account_details&action=edit&pcode=$pcode&result=$result");
		//}
		$error = 1;
		$data_action = 'edit';
	}
	if($data_action=="edit")
	{
		$rs				 = $s->getData_with_condition('tbl_seller','seller_id',$pcode);
		$row			 = mysqli_fetch_object($rs);
		$seller_salutation = $row->seller_salutation;
		$seller_fname 	 = stripslashes($row->seller_fname);
		$seller_lname    = stripslashes($row->seller_lname);
		$seller_address  = nl2br(stripslashes($row->seller_address));
		$city		     = $row->seller_city;
		$country		 = $row->seller_country;
		$state		   	 = $row->seller_state;
		$zip		 	 = $row->seller_zip;
		$seller_phone	 = $row->seller_phone;
		$seller_desc	 = nl2br(stripslashes($row->seller_desc));
		$seller_status	 = $row->seller_status;
		$seller_role	 = $row->seller_role_id;
		$seller_email	 = $row->seller_email;
		$seller_percent	 = $row->percent_commission ;

		$data_action 	= "update";
	}
	
?>
<form name="frx1" id="frx1" action="index.php?pagename=add_general_information&subpagename=account_details&action=<?php echo $data_action;?>&pcode=<?php echo $pcode;?>" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="0">

	<tr>
	  <td class="pHeadLine"></td>
    </tr>

	<tr><td>
	<table width="100%" border="0" cellpadding="0" cellspacing="0" class="tblBorder">
		<tr>
		  <td colspan="2" class="pagehead" >Sales Rep Details</td>
		  </tr>
		
		  <tr class="text"><td width="15%" class="pad" >Salutation <span class="redstar">*</span></td>
		<td width="85%" > 
		 <select name="salutation" id="salutation" class="inpuTxt">
		<option value="mr" <?php if($seller_salutation == 'mr'){ echo 'selected=selected';} ?>>Mr.</option>
		<option value="ms" <?php if($seller_salutation == 'ms'){ echo 'selected=selected';} ?> >Ms.</option>
		<option value="mrs" <?php if($seller_salutation == 'mrs'){ echo 'selected=selected';} ?>>Mrs.</option>
	  </select>	  </td></tr>
	  
		<tr class="text"><td width="15%" class="pad" >First Name<span class="redstar">*</span></td>
		<td width="85%" ><input type="text" name="seller_fname" id="seller_fname" class="inpuTxt"  value="<?php echo $seller_fname;?>"></td></tr>

		<tr class="text">
		  <td class="pad">Last Name</td>
	 <td><input type="text" name="seller_lname" id="seller_lname" class="inpuTxt"  value="<?php echo $seller_lname;?>"></td></tr>
	
 <tr class="text">
          <td class="pad">Email [username]<span class="redstar"> *</span></td>
          <td><input type="text" name="seller_email" id="seller_email" class="inpuTxt" value="<?php echo $seller_email;?>" <?php if($_REQUEST['action'] == 'edit'){ echo 'readonly=readonly';}?>/></td>
        </tr>
	     <tr class="text">
          <td class="pad">Percent Commission</td>
          <td>
<input type="text" name="percent_commission" id="percent_commission" class="inpuTxt" value="<?php echo $seller_percent;?>" /></td>
        </tr>

		<tr class="text"><td width="12%" valign="top" class="pad">Description</td>
	<td width="88%"><textarea name="seller_descr" id="seller_descr" class="inpuTxt"  rows="6"><?php echo stripslashes($seller_desc);?></textarea></td></tr>

		<tr class="text"><td class="pad">Status</td>
		<td>
		<select name="status" id="status" class="inpuTxt">
			<option value="active" <?php if($status == "active"){ echo "selected='selected'";}?>>Active</option>
			<option value="inactive" <?php if($status == "inactive"){ echo "selected='selected'";}?>>Inactive</option>
		</select>		</td></tr><?php //php role table mapp here ?>
	
	<tr class="text"><td  colspan="2" height="5px"></td></tr> 
	<tr class="text">
          <td class="pad">&nbsp;</td>
          <td><input type="submit" name="add " id="add" class="inputton" value="Save" />
            &nbsp;</td>
        </tr>	
		<tr class="text"><td class="redstar pad" colspan="2"> * Required Fields </td></tr> 	
</table>
</td></tr>

	
</table>
</form>
<script language="JavaScript" type="text/javascript">
 var frmvalidator = new Validator("frx1");

 frmvalidator.addValidation("seller_fname","req","Please enter First Name.");
 frmvalidator.addValidation("seller_email","req","Please enter email address.");
 frmvalidator.addValidation("seller_email","email","Please enter valid email address.");


</script>
