<?php 
//this new dynamic module for add calibration master added by rumit on dated 28Jan 2020
		$data_action			= $_REQUEST['action'];
		$pcode					= $_REQUEST["pcode"];
	if($data_action=="edit")
	{
		$rs						= $s->getData_with_condition('tbl_calibration_master','calibration_id',$pcode);
		$row					= mysqli_fetch_object($rs);
		$calibration_name        	= stripslashes($row->calibration_name);
		$calibration_abbrv       	= stripslashes($row->calibration_abbrv);
		//$calibration_manager     = stripslashes($row->calibration_manager);
		//$sub_calibration_lead    = stripslashes($row->sub_calibration_lead);
		//$sub_calibration_lead2   = stripslashes($row->sub_calibration_lead2);
		//$sub_calibration_lead3   = stripslashes($row->sub_calibration_lead3);
		$calibration_status      	= $row->calibration_status;
		if($row->calibration_description==''){
		$calibration_description 	= str_replace(' ', '_',$row->calibration_name) ;//stripslashes($row->calibration_description);		
		}
		else
		{
		$calibration_description 	= str_replace(' ', '_',$row->calibration_description) ;//stripslashes($row->calibration_description);					
		}
		$data_action = "update";
	}
	if($data_action == "add_new")
	{
		$data_action = "insert";
	}
?>
<form name="frx1" id="frx1" action="index.php?pagename=manage_calibration_master&action=<?php echo $data_action;?>&pcode=<?php echo $pcode;?>" method="post" enctype="multipart/form-data">
  <table width="100%" align="center" cellpadding="0" cellspacing="0" class="pagecontent" >
    <tr>
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="38%" class="pageheadTop">Add Calibration</td>
            <td width="52%" class="headLink"><ul>
                <li><a href="index.php?pagename=manage_calibration_master">Back</a></li>
              </ul></td>
            <td width="10%" align="right"><input name="save"  type="submit" class="inputton" id="save" value="Save"></td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td class="pHeadLine"></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><table width="100%" cellpadding="5" cellspacing="0" class="tblBorder">
          <tr>
            <td class="pagehead" colspan="2"> Calibration</td>
          </tr>
          <tr class="text">
            <td width="18%" class="pad"> Name <span class="redstar">*</span></td>
            <td width="24%"><input name="calibration_name" type="text" class="inpuTxt" id="calibration_name" value="<?php echo $calibration_name;?>" required="required"  pattern="[a-zA-Z0-9]+[a-zA-Z0-9 ]+" /></td>
          </tr>
          <?php /*?><tr class="text">

	  <td class="pad">Logo</td>

	  <td><input name="calibration_logo" type="file"  id="calibration_logo" value=""></td>

	</tr><?php */?>
          <tr class="text" style="display:none">
            <td width="18%" valign="top" class="pad"> Description</td>
            <td width="24%">
            <input name="calibration_description" type="text" class="inpuTxt" id="calibration_description" value="<?php echo $calibration_description;?>" />
           </td>
          </tr>
          <tr class="text" style="display:none">
            <td width="18%" class="pad">Abbreviation </td>
            <td width="24%"><input name="calibration_abbrv" type="text" class="inpuTxt" id="calibration_abbrv" value="<?php echo $calibration_abbrv;?>" maxlength="2" /></td>
          </tr>
          <tr class="text">
            <td width="18%" class="pad"> Status </td>
            <td width="24%"><select name="calibration_status" class="inpuTxt" id="calibration_status">
                <option value="active" <?php if( $calibration_status =="active"){ echo "selected='selected'";}?>>Active</option>
                <option value="inactive" <?php if($calibration_status =="inactive"){ echo "selected='selected'";}?>>Inactive</option>
              </select></td>
          </tr>
          <tr class="text">
            <td width="18%" class="pad"></td>
            <td width="24%"><input name="save"  type="submit" class="inputton" id="save" value="Save"></td>
          </tr>
          <tr class="text">
            <td class="redstar pad" colspan="2">* Required Fields</td>
          </tr>
        </table></td>
    </tr>
  </table>
</form>