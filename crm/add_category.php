<?php
	$pcode				= $_REQUEST['pcode'];
	$data_action 		= $_REQUEST['action'];
if($data_action == "delete_image")
{
	$rs_del    			= $s->getData_with_condition('tbl_category','cate_id',$pcode);
	$row_del   			= mysqli_fetch_object($rs_del);
	$del_image_large  	= "../".$row_del->cate_image;
	if(file_exists($del_image_large)>0)
	{
		unlink($del_image_large);
	}
	$sql_remove = "update tbl_category SET cate_image = '' where cate_id = $pcode";
	$result     = mysqli_query($GLOBALS["___mysqli_ston"],$sql_remove);
	if($result == 0)
	{ 
		$_SESSION['msg'] = "Category image deleted"; 
	}
	else 
	{
		$_SESSION['msg'] = "record_not_updated";   
	}
	$data_action = "edit";
}

if($data_action == "delete_banner")
{
	$rs_del_banner		= $s->getData_with_condition('tbl_category','cate_id',$pcode);
	$row_del_banner 	= mysqli_fetch_object($rs_del_banner);
	$del_banner_large  	= "../".$row_del_banner->banner_image;
	if(file_exists($del_banner_large)>0)
	{
		unlink($del_banner_large);
	}
	$sql_remove_banner	= "update tbl_category SET banner_image = '' where cate_id = $pcode";
	$result    			= mysqli_query($GLOBALS["___mysqli_ston"],$sql_remove_banner);
	if($result == 0)
	{ 
		$_SESSION['msg']= "Category image deleted"; 
	}
	else 
	{
		$_SESSION['msg']= "record_not_updated";
	}
	$data_action = "edit";
}
if($data_action == "delete_manuals")
{
	$rs_del_manual		= $s->getData_with_condition('tbl_category','cate_id',$pcode);
	$row_del_manual		= mysqli_fetch_object($rs_del_manual);
	$del_manual		  	= "../".$row_del_manual->eng_manual;
	if(file_exists($del_manual)>0)
	{
		unlink($del_manual);
	}
	$sql_remove_manual	= "update tbl_category SET eng_manual = '' where cate_id = $pcode";
	$result    			= mysqli_query($GLOBALS["___mysqli_ston"],$sql_remove_manual);
	if($result == 0)
	{ 
		$_SESSION['msg'] = "Category image deleted"; 
	}
	else 
	{
		$_SESSION['msg'] = "record_not_updated";
	}
	$data_action = "edit";
}

if($data_action == 'edit')
{
	$_SESSION['msg'] = "";
	$query			 = " deleteflag = 'active' and cate_id = $pcode";	
	$rsCate 		 = $s->selectWhere('tbl_category', $query);
	if(mysqli_num_rows($rsCate)>0)
	{
		$rowCate	  		= mysqli_fetch_object($rsCate);
		$CateImage 	  		= $rowCate->cate_image;
		$banner_image 		= $rowCate->banner_image;
		$CateName 	  		= stripslashes($rowCate->cate_name);
		$seo_title			= stripslashes($rowCate->seo_title);
		$cat_permalink		= stripslashes($rowCate->cat_permalink);		
		$CateParent   		= $rowCate->parent_id;
		$sort_order   		= $rowCate->sort_order;
		$meta_content		= stripslashes($rowCate->meta_content);		
		$meta_desc			= stripslashes($rowCate->meta_desc);
		$short_desc			= stripslashes($rowCate->short_desc);
		$CateDesc     		= stripslashes($rowCate->cate_description);
		$CateStatus			= $rowCate->cate_status;
		$eng_manual   		= $rowCate->eng_manual;
		$eng_manual_tilte   = stripslashes($rowCate->eng_manual_tilte);
		$cate_status		= $rowCate->cate_status;
	}
	$data_action = "update";
}
if($data_action == "add_new")
{
	$_SESSION['msg'] = "";
	$data_action 	 = "insert";
}
if($_FILES["cate_image_name"]["name"] !="")
{
	//$bannerPath 			= $s->ImageUpload("uploads/cms_images/", "image_path", "CMSBANNER_", "965", "284");
	$img_result_large 		= $s->fileUpload('uploads/category_images/','cate_image_name','CATE_');
	if($img_result_large != -1)	
	{
		$dataArray['cate_image'] = $img_result_large;
	}
}
if($_FILES["banner_image"]["name"] != "")
{
	$banner_result_large     = $s->fileUpload('uploads/category_banner/','banner_image','CATE_BANNER_');
	if($banner_result_large != -1)
	{
		$dataArray['banner_image']  =  $banner_result_large;
	}
}
//////////////////For Engineering Manual//////////////////////
if($_FILES["eng_manual"]["name"] != "")
{
	$filePath = $s->fileUpload("uploads/category_eng_manuals/", "eng_manual", "MANU_");
	if($filePath != -1)
	{
		$dataArray["eng_manual"] = $filePath;
	}
}
////////////////////////////////////////////
// For adding  or editing record
if($_REQUEST['action'] == "update" || $_REQUEST['action'] == "insert")
{
	$dataArray['cate_name'] 		= addslashes($_REQUEST["cate_name"]);
	$dataArray['seo_title']			= addslashes($_REQUEST["seo_title"]);
//	$dataArray['perma_link']		= addslashes(str_replace("","",$_REQUEST["perma_link"]));
	
	
	if(strlen(trim($_REQUEST["cat_permalink"]))>0 )
			{
			$dataArray["cat_permalink"]= addslashes(str_replace(" ","-",str_replace("'","",str_replace("/","_",strtolower($_REQUEST["cat_permalink"])))));
			}
			else
			{
			$dataArray["cat_permalink"]= addslashes(str_replace(" ","-",str_replace("'","",str_replace("/","_",strtolower($_REQUEST["cate_name"])))));
			}
//$dataArray['cat_permalink']		= addslashes(str_replace("/","_",$_REQUEST["cat_permalink"]));

	if($pcode !=  $_REQUEST["parent_id"])
	{
		$dataArray['parent_id'] 	= $_REQUEST["parent_id"];
	}
	$dataArray['sort_order'] 		= $_REQUEST["sort_order"];
	$dataArray['meta_content']		= addslashes($_REQUEST['meta_content']);
	$dataArray['meta_desc']			= addslashes($_REQUEST['meta_desc']);
	$dataArray['short_desc']		= addslashes($_REQUEST['short_desc']);
	$dataArray['cate_description'] 	= addslashes($_REQUEST["cate_description"]);
	$dataArray["eng_manual_tilte"] 	= addslashes($_REQUEST["eng_manual_tilte"]);
	$dataArray["cate_status"] 		= addslashes($_REQUEST["cate_status"]);
}
if($_REQUEST['action'] == "insert")
{
	$dataArray['cate_status'] = "active";
	$result 				  = $s->insertRecord( 'tbl_category' , $dataArray);
	if($result == 0)
	{ 	
		$s->pageLocation("index.php?pagename=manage_category&msg=add"); 
	} 
	else
	{ 
		$_SESSION['msg'] = record_not_added;   
	}
}
if($_REQUEST['action'] == "update")
{
	$result= $s->editRecord( 'tbl_category' , $dataArray ,'cate_id' , $pcode );	 //exit;
	if($result == 0)
	{ 
		$s->pageLocation("index.php?pagename=manage_category&msg=edit"); 
	}
	else 
	{
		$_SESSION['msg'] = record_not_updated;   
	}
}
//////////////////// Manuals //////////////////////////////////
if($data_action == "delete_manuals")
{
	$rs_del    			= $s->getData_with_condition('tbl_products','pro_id',$pcode);
	$row_del   			= mysqli_fetch_object($rs_del);
	$del_image_large  	= "../".$row_del->eng_manual;
	if(file_exists($del_image_large)>0)
	{
		unlink($del_image_large);
	}
	$sql_remove = "update tbl_products SET eng_manual = '' where pro_id = $pcode";
	$result     = mysqli_query($GLOBALS["___mysqli_ston"],$sql_remove);
	if($result == 0)
	{ 
		$Msz = 0; 
	}
	else if($result == 1)
	{ 
		$Msz  = 1;
	}
	$data_action = "edit";
}
//////////////////////////////////////////////// 
function showSubCategories($cat_id, $dashes = '',$check_pid)
{    
	$dashes .= '&nbsp;&nbsp;&nbsp;';    
	$rsSub = mysqli_query($GLOBALS["___mysqli_ston"],"SELECT cate_id ,  cate_name FROM tbl_category WHERE parent_id=" . $cat_id ." and deleteflag='active' and cate_status='active'") or die(mysql_error());    
	
	if(mysqli_num_rows($rsSub) >= 1)
	{        
		while($rows_sub = mysqli_fetch_array($rsSub))
		{            
			if($rows_sub['cate_id'] == $check_pid)
			{
				echo "<option value='$check_pid' selected='selected'>". $dashes . stripslashes($rows_sub['cate_name']) . " </option>";
			}
			else
			{
				echo "<option value='".$rows_sub['cate_id']."'>".$dashes . stripslashes($rows_sub['cate_name']) . "</option><br />";
			}
				showSubCategories($rows_sub['cate_id'], $dashes,$check_pid);        
		}    
	}
}
?>
<script>
function ViewPrintX1(orderID)
{
  var path;
  path = "map_category1.php?pcode="+orderID;
  newwindow=window.open(path,'name',"menubar=1,resizable=1,status=1,toolbar=1,scrollbars=1");
	if (window.focus) {newwindow.focus();}
}
</script>

<form name="frx1" action="index.php?pagename=add_category&action=<?php echo $data_action;?>&pcode=<?php echo $pcode;?>" method="post"  enctype="multipart/form-data">
  <table width="100%" align="center"  cellpadding="0" cellspacing="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="86%" class="pageheadTop">Category Manager </td>
            <td width="9%" class="headLink"><ul>
                <li> <a href="#" onclick='ViewPrintX1(<?php echo $pcode;?>)'>Map Category</a></li>
              </ul></td>
            <td width="5%" class="headLink"><input name='edit' id='edit' type='submit' class='inputton' value='Save'  ></td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td class="pHeadLine"></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><?php
if($_SESSION['msg'] != "")
{
	if ($_SESSION['msg'] == record_not_added)
	{
		echo "<p class='error'>".$_SESSION['msg']."</p><br />";
	
	}else if ($_SESSION['msg'] == record_added){ 
	
		echo "<p class='success'>".$_SESSION['msg']."</p> <br/>";       
	
	}else if ($_SESSION['msg'] == record_not_update)
	{
	
		echo "<p class='error'>".$_SESSION['msg']."</p> <br />";
			
	}else if ($_SESSION['msg'] == record_update){ 
	
		echo "<p class='success'>".$_SESSION['msg']."</p><br />";
	}else
	{
		echo "<p class='error'>".$_SESSION['msg']."</p><br />";
	
	}
	if($_SESSION['msg'] == "Category image deleted")
	{
			echo "<p class='success'>".$_SESSION['msg']."</p><br />";
	}
}
?></td>
    </tr>
    <tr>
      <td><table width="100%" cellpadding="0" cellspacing="0" class="tblBorder">
          <tr>
            <td colspan="2" class="pagehead" >Category </td>
          </tr>
          <tr class="text">
            <td width="19%" class="pad" >Category Name <span class="redstar">*</span></td>
            <td width="81%" align="left"><input name="cate_name" type="text" class="inpuTxt" id="cate_name" value="<?php echo htmlentities($CateName); ?>"></td>
          </tr>
          <tr class="text">
            <td align="left" class="pad">Parent Name <span class="redstar">*</span></td>
            <td align="left"><?php 
	$query1	= " deleteflag = 'active' and parent_id = 0";	
	//if($_REQUEST["action"] == "edit")
	//{
	//	$query1 = $query1 . "and  cate_id <> $pcode";
//	}
	$rsCate1 = $s->selectWhere('tbl_category', $query1);
	
?>
              <select  name="parent_id" class="inpuTxt"  id="parent_id">
                <option value='0'>Root</option>
                <?php 
 //$CateParent
	if(mysqli_num_rows($rsCate1)>0)
	{    
		while($rowCate1 = mysqli_fetch_object($rsCate1))
		{        
?>
                <option value='<?php echo $rowCate1->cate_id;?>' <?php if($rowCate1->cate_id == $CateParent){echo "selected='selected'";}?>><?php echo stripslashes($rowCate1->cate_name);?></option>
                <?php
				showSubCategories($rowCate1->cate_id,'&nbsp;',$CateParent);    
		}
	}
?>
              </select>
          </tr>
          <tr class="text">
            <td align="left" class="pad" valign="top">Upload Image</td>
            <td align="left"><input name="cate_image_name" id="cate_image_name" type="file" >
              &nbsp;&nbsp; &nbsp;
              <?php if($CateImage != "")
{?>
              <img src="../<?php echo $CateImage; ?>"  width="100" onerror="style.display='none';" />&nbsp; &nbsp; <a href="index.php?pagename=add_category&action=delete_image&pcode=<?php echo $pcode;?>">Remove Image</a>
              <?php } ?></td>
          </tr>
          <tr class="text">
            <td align="left" class="pad" valign="top">Upload Banner</td>
            <td><input name="banner_image" id="banner_image" type="file" >
              &nbsp;&nbsp; &nbsp;
              <?php if($banner_image != "")
{?>
              <img src="../<?php echo $banner_image; ?>"  width="100" onerror="style.display='none';" />&nbsp; &nbsp; <a href="index.php?pagename=add_category&action=delete_banner&pcode=<?php echo $pcode;?>">Remove banner</a>
              <?php } ?></td>
          </tr>
          <tr class="text">
            <td class="pad" nowrap>Page Title </td>
            <td><input name="seo_title" type="text" class="inpuTxt" id="seo_title" value="<?php  echo stripslashes($seo_title); ?>" /></td>
          </tr>
          <tr class="text">
            <td class="pad" nowrap>Permalink</td>
            <td><input name="cat_permalink" type="text" class="inpuTxt" id="cat_permalink" value="<?php  echo stripslashes($cat_permalink); ?>" />
              (Do not use '/' 'etc and special characters)</td>
          </tr>
          <tr class="text">
            <td class="pad" valign="top" nowrap>Meta Keywords</td>
            <td><textarea name="meta_content"  rows="6" cols="50"  id="meta_content" ><?php  echo stripslashes($meta_content); ?>
</textarea></td>
          </tr>
          <tr class="text" style="height:5px;">
            <td colspan="2">&nbsp;</td>
          </tr>
          <tr class="text" style="height:5px;">
            <td class="pad" valign="top" nowrap>Meta Description</td>
            <td><textarea name="meta_desc"  rows="6" cols="50" id="meta_desc"  ><?php  echo stripslashes($meta_desc); ?>
</textarea></td>
          </tr>
          <tr class="text" style="height:5px;">
            <td colspan="2">&nbsp;</td>
          </tr>
          <tr class="text">
            <td class="pad" valign="top" nowrap>Short Description</td>
            <td><?php
	$sBasePath  			= "../fckeditor/";
	$oFCKeditor 			= new FCKeditor('short_desc') ;
	$oFCKeditor->BasePath 	= $sBasePath;
	$oFCKeditor->Value		= stripslashes($short_desc); 
	$oFCKeditor->Width  	= '80%';
	$oFCKeditor->Height 	= '200';
	$oFCKeditor->Create();
?></td>
          </tr>
          <tr class="text" style="height:5px;">
            <td colspan="2">&nbsp;</td>
          </tr>
          <tr class="text">
            <td align="left" valign="top" class="pad"> Description</td>
            <td align="left"><?php
	$sBasePath  			= "../fckeditor/";
	$oFCKeditor 			= new FCKeditor('cate_description') ;
	$oFCKeditor->BasePath 	= $sBasePath;
	$oFCKeditor->Value		=  stripslashes($CateDesc); 
	$oFCKeditor->Width  	= '80%';
	$oFCKeditor->Height 	= '400';
	$oFCKeditor->Create();
?></td>
          </tr>
          <tr class="text">
            <td align="left" valign="top" class="pad">Display Order </td>
            <td align="left"><input name="sort_order" type="text" class="inpuTxt" id="sort_order" value="<?php echo $sort_order; ?>"></td>
          </tr>
          <tr class="text">
            <td align="left" class="pad">Status </td>
            <td align="left"><select  name="cate_status" class="inpuTxt"  id="cate_status">
                <option value='active' <?php  if( $cate_status == "active"){ echo "selected='selected'";}?> >Active</option>
                <option value='inactive' <?php  if( $cate_status == "inactive"){ echo "selected='selected'";}?> >Inactive</option>
              </select></td>
          </tr>
          <?php /*?><tr class="text">
            <td class="pad">Datasheet Title <span class="redstar">*</span></td>
            <td><input name="eng_manual_tilte" id="eng_manual_tilte" type="text" value="<?php echo $eng_manual_tilte ; ?>" /></td>
          </tr>
          <tr class="text">
            <td class="pad">Upload Datasheet <span class="redstar">*</span></td>
            <td><input name="eng_manual" id="eng_manual" type="file"  value="" />
              &nbsp; &nbsp;
              <?php if($eng_manual != ""){?>
              <a href="download.php?action=downloadfile&file=<?php echo "../".$eng_manual;?>">Download File</a> &nbsp; | &nbsp; <a href="index.php?pagename=add_category&action=delete_manuals&pcode=<?php echo $pcode;?>">Remove File</a>
              <?php }?></td>
          </tr><?php */?>
          <?php 
	/*<tr class="text"><td align="left" valign="top" class="pad"> Meta Keyword</td><td align="left"><textarea name="cate_meta_keyword"  rows="4" class="inpuTxt" id="cate_meta_keyword" ><?php echo stripslashes($cat_edit_result['cate_meta_keyword']); ?></textarea></td></tr>
	
	<tr class="text"><td align="left" valign="top" class="pad"> Meta Tag</td><td align="left"><textarea name="cate_meta_tag" rows="4" class="inpuTxt" id="cate_meta_tag"><?php echo stripslashes($cat_edit_result['cate_meta_tag']); ?></textarea></td></tr>
<tr class="text"><td align="left" class="pad"> Status</td><td align="left">
<select name="cate_status" class="inpuTxt" id="cate_status">
	<option value="active"   <?php if($CateStatus  == 'active')  { echo "selected = 'selected'"; }?>>Active</option>
	<option value="inactive" <?php if($CateStatus  == 'inactive'){ echo "selected = 'selected'"; }?>>Inactive</option>
</select>
</td></tr>
	*/ 
?>
          <tr class="text">
            <td class="pad"></td>
            <td><input name='edit' id='edit' type='submit' class='inputton' value='Save'  ></td>
          </tr>
          <tr class="text">
            <td class="redstar pad" colspan="2"> * Required Fields </td>
          </tr>
        </table></td>
    </tr>
  </table>
</form>
<!-- This function will validate the form --> 
<script language="JavaScript" type="text/javascript">
	var frmvalidator = new Validator("frx1");
	frmvalidator.addValidation("cate_name","req","Please enter Category Name.");
	frmvalidator.addValidation("parent_id","req","Please select Parent Category.");
	//frmvalidator.addValidation("eng_manual_tilte","req","Please enter Engineering Manuals Title .");
<?php 
	if($_REQUEST["action"]=="add_new")
	{
?>
	//frmvalidator.addValidation("eng_manual","req","Please select Engineering Manuals.");
<?php 
	}
?>
	//frmvalidator.addValidation("cate_short_description","req","Please enter short description");
	
	
/*	frmvalidator.addValidation("cate_image_name","req","Please upload category image");
	frmvalidator.addValidation("cate_description","req","Please enter category description.");
	frmvalidator.addValidation("cate_meta_keyword","req","Please enter meta keyword.");
	frmvalidator.addValidation("cate_meta_tag","req","Please enter meta tag.");
	*/
</script>