<?php 
	$data_action = $_REQUEST["action"];
	$pcode		 = $_REQUEST["pcode"];
	if($_REQUEST["action"] == "edit")
	{
		$rs 		 = $s->getData_with_condition('all_cities','city_id',$pcode);
		$row		 = mysqli_fetch_object($rs);
		$zone_id  	 = $row->state_code;
		$city_name	 = $row->city_name;
		$city_code	 = $row->city_code;
		$data_action = "update";
	}
	if($_REQUEST["action"] == "add_new")
	{
			$data_action = "insert";
	}
?>

<form name="frx1" id="frx1" action="index.php?pagename=cities_manager&action=<?php echo $data_action;?>&pcode=<?php echo $pcode;?>" method="post">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0" >
          <tr>
            <td width="13%" class="pageheadTop">City </td>
            <td width="83%" class="headLink"><ul>
                <li><a href="index.php?pagename=cities_manager">Back</a></li>
              </ul></td>
            <td width="4%" class="headLink"><input  type="submit" name="edit" id="edit" class="inputton" value="Save" /></td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td class="pHeadLine"></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td valign="top" class="pagecontent"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="tblBorder">
          <tr>
            <td colspan="2" ><table width="100%" border="0" cellspacing="0" cellpadding="0" class="tblBorder">
                <tr>
                  <td class="pagehead"> City Details </td>
                </tr>
              </table></td>
          </tr>
          <tr class="text">
            <td class="pad" align="left"> State<span class="redstar"> *</span></td>
            <td align="left"><select name="state" id="state" class="inpuTxt"  required >
                <option value="">Select State</option>
                <?php
	//$sql_state = "select * from tbl_zones order by zone_name";
	$rs_co		 = $s->getData_without_condition('tbl_zones','zone_name');//mysqli_query($GLOBALS["___mysqli_ston"],$sql_state);
	if(mysqli_num_rows($rs_co)>0)
	{
		while($row_state=mysqli_fetch_object($rs_co))
		{	
?>
                <option value="<?php echo $row_state->zone_id;?>" <?php if($row_state->zone_id==$zone_id ){ echo "selected='selected'"; }?>><?php echo ucfirst($row_state->zone_name);?></option>
                <?php
		}
	}
?>
              </select></td>
          </tr>
          <tr class="text" align="left">
            <td width="13%" class="pad">City Name<span class="redstar"> *</span></td>
            <td ><input name="city_name" id="city_name" type="text" class="inpuTxt" value="<?php echo $city_name;?>" required="required" /></td>
          </tr>
          <tr class="text" align="left">
            <td width="13%" class="pad" >City Code<span class="redstar"> *</span></td>
            <td width="87%"><input name="city_code" id="city_code" type="text" class="inpuTxt" value="<?php echo $city_code;?>" required="required" /></td>
          </tr>
          <tr class="text" align="left" >
            <td ></td>
            <td><input  type="submit" name="edit" id="edit" class="inputton" value="Save" />
              &nbsp;</td>
          </tr>
          <tr class="text">
            <td class="redstar pad" colspan="2"> * Required Fields </td>
          </tr>
        </table></td>
    </tr>
  </table>
</form>
<!-- This function will validate the form --> 
<script language="JavaScript" type="text/javascript">
	var frmvalidator = new Validator("frx1");
	frmvalidator.addValidation("state","dontselect=0","Please select Country.");
	frmvalidator.addValidation("city_code","req","Please enter City Code");
	frmvalidator.addValidation("city_name","req","Please enter City Name");
</script>