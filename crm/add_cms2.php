<link href="css/style.css" rel="stylesheet" type="text/css" />
<?php 
	$data_action = $_REQUEST["action"];
	$pcode		 = $_REQUEST["pcode"];
	
	if($data_action == "delete_image")
	{
		$rs_del    			= $s->getData_with_condition('tbl_cms_pages','pages_id',$pcode);
		$row_del   			= mysqli_fetch_object($rs_del);
		$del_image_large  	= "../".$row_del->image_path;
		if(file_exists($del_image_large)>0)
		{
			unlink($del_image_large);
		}
		$sql_remove = "update tbl_cms_pages SET image_path = '' where pages_id = $pcode";
		$result     = mysqli_query($GLOBALS["___mysqli_ston"],$sql_remove);
		if($result == 0)
		{
			$Msz  = 0; 
		}
		else if($result == 1)
		{ 
			$Msz  = 1;
		}
		$data_action = "edit";
	}
	if($data_action == "delete_logo")
	{
		$rs_del    			= $s->getData_with_condition('tbl_cms_pages','pages_id',$pcode);
		$row_del   			= mysqli_fetch_object($rs_del);
		$del_image_large  	= "../".$row_del->logo_path;
		if(file_exists($del_image_large)>0)
		{
			unlink($del_image_large);
		}
		$sql_remove = "update tbl_cms_pages SET logo_path = '' where pages_id = $pcode";
		$result     = mysqli_query($GLOBALS["___mysqli_ston"],$sql_remove);
		if($result == 0)
		{
			$Msz  = 0; 
		}
		else if($result == 1)
		{ 
			$Msz  = 1;
		}
		$data_action = "edit";
	}

	if($data_action=="edit")
	{
		$rs  			 = $s->getData_with_condition('tbl_cms_pages','pages_id',$pcode);
		$row 			 = mysqli_fetch_object($rs);
		$page_name		 = $row->pages_name;
		$pages_title     = $row->page_title;
		$menu_id      	 = $row->menu_id;
		//$sort_desc	 = $row->sort_desc;
		$pages_html_text = $row->pages_html_text;
		$image_path		 = $row->image_path;
		$logo_path		 = $row->logo_path;
		$lock_status	 = $row->lock_status;
		$page_status	 = $row->page_status;
		$menu_header 	 = $row->menu_header;
		$menu_footer 	 = $row->menu_footer;
		
		$left_panel 	 = $row->left_panel;
		$right_panel 	 = $row->right_panel;
		
		//$menu_view		 = $row->menu_view;
		$meta_content	 = $row->meta_content;
		$meta_desc		 = $row->meta_desc;
		$quick_link		 = $row->quick_link;
		$data_action     = "update";
	}
	if($data_action == "add_new")
	{
		$data_action = "insert";
	}	
	function SubCMSPageRecord($ManuIDRecords, $selected, $space)
	{
		$space  = $space . "&nbsp;&nbsp;";
		$sqlCms = "select * from tbl_cms_pages where deleteflag = 'active' and menu_id = $ManuIDRecords";
		$rsCms  = mysqli_query($GLOBALS["___mysqli_ston"],$sqlCms);
		if(mysqli_num_rows($rsCms) >0)
		{
			while($rowCms = mysqli_fetch_object($rsCms))
			{
?>
<option value="<?php echo $rowCms->pages_id;?>" <?php if($rowCms->pages_id == $selected){ echo "selected='selected'";}?>><?php echo $space."&raquo; ".ucfirst($rowCms->pages_name);?>	</option>
<?php
				SubCMSPageRecord($rowCms->pages_id, $selected, $space);
			}
		}
	}	
?>
<form name="frx1" id="frx1" action="index.php?pagename=cms_manager&action=<?php echo $data_action;?>&pcode=<?php echo $pcode;?>" method="post" enctype="multipart/form-data">
<table width="100%"  align="center" cellpadding="0" cellspacing="0">

	<tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="24%" class="pageheadTop">CMS Manager</td>
            <td width="76%" class="headLink"><!--<ul>
                <li><a href="#" onclick="back_page();">Back</a></li>
            </ul>--></td><td><input name="save"  type="submit" class="inputton" id="save" value="Save"></td>
          </tr>
      </table></td>
    </tr>
	<tr>
      <td class="pHeadLine"></td>
    </tr>
	<tr>
      <td>&nbsp;</td>
    </tr>
	
	<tr>
		<td>
		<table width="100%" cellpadding="0" cellspacing="0" class="tblBorder">
			<tr>
				<td colspan="3">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr><td class="pagehead">  Page Details</td></tr>
				    </table>				</td>
			</tr>
			<tr class="text">
				<td width="12%"  class="pad">Page Name <span class="redstar">* </span></td>
				<td colspan="2"><input name="page_name" type="text" class="inpuTxt" id="page_name" value="<?php  echo stripslashes($page_name); ?>" /></td>
			</tr>
			<tr class="text">
				<td class="pad">Page Title</td>
				<td colspan="2"><input name="page_title" type="text" class="inpuTxt" id="page_title" value="<?php  echo stripslashes($pages_title); ?>" /></td>
			</tr>
			<tr class="text">
				<td class="pad">Page Menu </td>
				<td colspan="2">
					<select name="InMenu" id="InMenu" class="inpuTxtSelect">
						<option value="0" <?php if(0 == $menu_id){ echo "selected='selected'";}?>>Main Page </option>
						<option value="-1" <?php if(-1 == $menu_id){ echo "selected='selected'";}?>>No Display in Menu </option>
<?php
	$sqlMenu = "select * from tbl_cms_pages where deleteflag = 'active' and menu_id = 0";	
	$rsMenu	 = mysqli_query($GLOBALS["___mysqli_ston"],$sqlMenu);
	if(mysqli_num_rows($rsMenu)>0)
	{
		while($rowMenu = mysqli_fetch_object($rsMenu))
		{
			
?>
						<option value="<?php echo $rowMenu->pages_id;?>" <?php if($rowMenu->pages_id == $menu_id){ echo "selected='selected'";}?>><?php echo ucfirst($rowMenu->pages_name);?></option>
<?php			
			SubCMSPageRecord($rowMenu->pages_id, $menu_id, "&nbsp;");
		}	
	}
?>
					</select>				</td>
			</tr>
			<tr class="text">
		  <td class="pad">Panel Position </td>
		  <td colspan="2">
		  
		  
		  
		  
	 <input type="checkbox" name="left_panel"  value="yes" <?php if($left_panel=='yes'){ echo 'checked';}?>/>Show Left Panel&nbsp;<input type="checkbox" name="right_panel"  value="yes" <?php if($right_panel=='yes'){ echo 'checked';}?>/>Show Right Panel&nbsp;	  </td></tr>
			<tr class="text" style="height:5px;"><td colspan="3">&nbsp;</td></tr>
			<tr class="text">
				<td class="pad" valign="top" nowrap>Meta Content</td>
				<td colspan="2"><textarea name="meta_content"  rows="10" cols="50"  id="meta_contet" ><?php  echo stripslashes($meta_content); ?></textarea></td>
			</tr>
	  		<tr class="text" style="height:5px;"><td colspan="3">&nbsp;</td></tr>
	  		<tr class="text" style="height:5px;">
				<td class="pad" valign="top" nowrap>Meta Description</td>
				<td colspan="2"><textarea name="meta_desc"  rows="10" cols="50" id="meta_desc"  ><?php  echo stripslashes($meta_desc); ?></textarea></td>
			</tr>
	 		<tr class="text" style="height:5px;"><td colspan="3">&nbsp;</td></tr>
	<!--tr class="text">
	  <td class="pad" valign="top" nowrap>Short Content</td>
	  <td><?php
					//$sBasePath = $_SERVER['PHP_SELF'] ;
					//$sBasePath = substr( $sBasePath, 0, strpos( $sBasePath, "file" ) ) ;
			/*		$sBasePath  = "../fckeditor/";
					$oFCKeditor 		  = new FCKeditor('sort_desc') ;
					$oFCKeditor->BasePath = $sBasePath;
					$oFCKeditor->Value		=  stripslashes($sort_desc); 
					$oFCKeditor->Width  = '80%' ;
					$oFCKeditor->Height = '200' ;
					$oFCKeditor->Create() ;
				*/?>	
	  </tr-->
	


	<tr class="text">
	  <td class="pad" valign="top" nowrap>Page Content</td>
	  <td colspan="2"><?php
					//$sBasePath = $_SERVER['PHP_SELF'] ;
					//$sBasePath = substr( $sBasePath, 0, strpos( $sBasePath, "file" ) ) ;
					$sBasePath  = "../fckeditor/";
					$oFCKeditor = new FCKeditor('pages_html_text') ;
					$oFCKeditor->BasePath = $sBasePath;
					$oFCKeditor->Value		=  stripslashes($pages_html_text); 
					$oFCKeditor->Width  = '80%' ;
					$oFCKeditor->Height = '400' ;
					$oFCKeditor->Create() ;
				?>	 </td>
	  </tr>
	  <tr class="text">
		  <td class="pad">Menu Position </td>
		  <td colspan="2">
	 <?php /* <input type="checkbox" name="menu_header"  value="yes" <?php if($menu_header=='yes'){ echo 'checked';}?>/>Header Menu&nbsp ;*/?><input type="checkbox" name="menu_footer"  value="yes" <?php if($menu_footer=='yes'){ echo 'checked';}?>/>Footer Menu	  </td></tr>
	    <tr class="text">
		  <td class="pad">Top Banner</td>
		  <td colspan="2"><input type="file" name="image_path" id="image_path" /><?php if($image_path != ""){?>
		<a href="download.php?action=downloadfile&file=<?php echo "../".$image_path;?>">Download File</a> &nbsp; | &nbsp; <a href="index.php?pagename=add_cms&action=delete_image&pcode=<?php echo $pcode;?>">Remove File</a><?php }?></td>
		</tr>
<?php /*
		 <tr class="text">
		  <td class="pad">Page Small Image </td>
		  <td ><input type="file" name="logo_path" id="logo_path" /><?php if($logo_path != ""){?>
		<a href="download.php?action=downloadfile&file=<?php echo "../".$logo_path;?>">Download File</a> &nbsp; | &nbsp; <a href="index.php?pagename=add_cms&action=delete_logo&pcode=<?php echo $pcode;?>">Remove File</a><?php }?></td>
		  <td width="56%">This image will be displayed in the Small box on the home page </td>
		  </tr>
*/?> 
	  <tr class="text">
	    <td class="pad"> Locked </td>
	    <td colspan="2">
	<?php 
	if($lock_status == "active")
	{
		echo "Page Locked ";
?>	
<select name="locked" class="inpuTxt" id="locked" style="visibility:hidden">
<option value="active" selected="selected">Active</option>
</select>	
<?php
	}
	else
	{
	?>
		<select name="locked" class="inpuTxt" id="locked">
		<option value="active">Active</option>
		<option value="inactive" selected="selected">Inactive</option>
		</select>
	<?php
	}
	?>	</td></tr>
	<tr class="text"><td class="pad"> Add as a Quick Link</td><td colspan="2"><select name="quick_link" class="inpuTxt" id="quick_link">
	<option value="yes" <?php  if( $quick_link == "yes"){ echo "selected='selected'";}?>>Yes</option>
	<option value="no"  <?php  if( $quick_link == "no") { echo "selected='selected'";}?>>No</option>
	</select></td></tr>
	  <tr class="text"><td class="pad"> Status</td><td colspan="2"><select name="page_status" class="inpuTxt" id="page_status">
	<option value="active"   <?php  if( $page_status == "active"){ echo "selected='selected'";}?> >Active</option>
	<option value="inactive" <?php  if( $page_status == "inactive"){ echo "selected='selected'";}?>>Inactive</option>
	</select></td></tr>
	<tr class="text"><td class="pad"></td><td colspan="2"><input name="save"  type="submit" class="inputton" id="save" value="Save"></td>
	</tr>
	<tr class="text"><td class="redstar pad" colspan="3"> * Required Fields </td></tr>  
	</table>
		</td></tr>
</table>
</form>
<script language="JavaScript" type="text/javascript">
 var frmvalidator = new Validator("frx1");
 frmvalidator.addValidation("page_name","req","Please enter Page Name");
</script>
