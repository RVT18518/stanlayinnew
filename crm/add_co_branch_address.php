<?php 
	$data_action=$_REQUEST['action'];
	$pcode=$_REQUEST["pcode"];
	
	if($data_action=="edit")
	{
		$rs=$s->getData_with_condition('tbl_company_branch_address','id',$pcode);
		$row=mysqli_fetch_object($rs);
		$location       = stripslashes($row->location);
		$company_name   = stripslashes($row->company_name);
		$address        = stripslashes($row->address);
		$email_id     	= stripslashes($row->email_id);
		$pan_no    		= stripslashes($row->pan_no);
		$gst_no    		= stripslashes($row->gst_no);
		
		/*$bank_name      = stripslashes($row->bank_name);
		$bank_acc_no    = stripslashes($row->bank_acc_no);
		$bank_address   = stripslashes($row->bank_address);
		$ifsc_code    	= stripslashes($row->ifsc_code);		*/
		
		$sort_order		= $row->sort_order;
		$status      	= $row->status;
		$data_action = "update";
	}
	if($data_action == "add_new")
	{
		$data_action = "insert";
	}
	
?>

<form name="frx1" id="frx1" action="index.php?pagename=manage_co_branch_address&action=<?php echo $data_action;?>&pcode=<?php echo $pcode;?>" method="post" enctype="multipart/form-data">
  <table width="100%" align="center" cellpadding="0" cellspacing="0" class="pagecontent" >
    <tr>
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="41%" class="pageheadTop">Branch Details</td>
            <td width="55%" class="headLink"><ul>
                <li><a href="index.php?pagename=manage_co_branch_address">Back</a></li>
              </ul></td>
            <td width="5%" align="right"><input name="save"  type="submit" class="inputton" id="save" value="Save"></td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td class="pHeadLine"></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><table width="100%" cellpadding="5" cellspacing="0" class="tblBorder">
          <tr class="text">
            <td width="27%" class="pad">Branch Name <span class="redstar">*</span></td>
            <td width="73%"><input name="location" type="text" class="inpuTxt" size="13" id="location" value="<?php echo $location;?>" />
              &nbsp;</td>
          </tr>
          <tr class="text">
            <td class="pad">Company Name</td>
            <td><input name="company_name" type="text" class="inpuTxt"  id="company_name" value="<?php echo $company_name;?>" required="required" /></td>
          </tr>
          <tr class="text">
            <td class="pad">Email</td>
            <td><input name="email_id" type="text" class="inpuTxt"  id="email_id" value="<?php echo $email_id;?>" required="required" /></td>
          </tr>
          <tr class="text">
            <td class="pad" >Address </td>
            <td><textarea name="address" id="address"  class="inpuTxt" ><?php echo $address;?></textarea></td>
          </tr>
          <tr class="text">
            <td class="pad">PAN No.</td>
            <td><input name="pan_no" type="text"  id="pan_no" value="<?php echo $pan_no;?>" class="inpuTxt" required="required"  /></td>
          </tr>
          <tr class="text">
            <td class="pad">GST No.</td>
            <td><input name="gst_no" type="text"  id="gst_no" value="<?php echo $gst_no;?>" class="inpuTxt" required="required"  /></td>
          </tr>
          <tr class="text">
            <td colspan="2" class="pad head">Bank Details</td>
          </tr>
         
          <tr class="text">
            <td class="pad">Display Order</td>
            <td><input name="sort_order" type="text"  id="sort_order" value="<?php echo $sort_order;?>" class="inpuTxt"  /></td>
          </tr>
          <tr class="text">
            <td class="pad"> Status</td>
            <td><select name="status" class="inpuTxt" id="status">
                <option value="active" <?php if( $status =="active"){ echo "selected='selected'";}?>>Active</option>
                <option value="inactive" <?php if($status =="inactive"){ echo "selected='selected'";}?>>Inactive</option>
              </select></td>
          </tr>
          <tr class="text">
            <td class="pad"></td>
            <td><input name="save"  type="submit" class="inputton" id="save" value="Save"></td>
          </tr>
          <tr class="text">
            <td class="redstar pad" colspan="2"> * Required Fields </td>
          </tr>
        </table></td>
    </tr>
  </table>
</form>
<!--<script language="JavaScript" type="text/javascript">
 var frmvalidator = new Validator("frx1");
 frmvalidator.addValidation("location","req","Please enter Name");
  frmvalidator.addValidation("application_heading","req","Please enter Heading Name");
</script>-->