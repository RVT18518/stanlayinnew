<script language="javascript" src="../js/pro_name.js" type="text/javascript"></script>
<?php 
	$data_action=$_REQUEST['action'];
	$pcode=$_REQUEST["pcode"];

	if($data_action=="edit")
	{
		$rs					= $s->getData_with_condition('tbl_demo_stock','stock_id',$pcode);
		$row				= mysqli_fetch_object($rs);
		
		
		$office_location_id = stripslashes($row->office_location_id);
		$category_id    	= stripslashes($row->cat_id);
		$pro_id   			= stripslashes($row->pro_id);
		$qty    			= stripslashes($row->qty);		
		
		$serial_no			= $row->serial_no;
		$status      		= $row->status;
		$stock_available	= $s->product_qty($row->pro_id);
		$data_action 		= "update";
		
		if($stock_available<=0)
{
	$class="style='background-color:#ffa08a'";
	}
else
{
	$class="style='background-color:#D1E3D1'";
}
	}
	if($data_action == "add_new")
	{
		$data_action = "insert";
	}
	
?>

<form name="frx1" id="frx1" action="index.php?pagename=demo_stock_manager&action=<?php echo $data_action;?>&pcode=<?php echo $pcode;?>" method="post" enctype="multipart/form-data" onsubmit="return validatePassword();">
  <table width="100%" align="center" cellpadding="0" cellspacing="0" class="pagecontent" >
    <tr>
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="41%" class="pageheadTop">Add Demo Stock </td>
            <td width="55%" class="headLink"><ul>
                <li><a href="index.php?pagename=demo_stock_manager">Back</a></li>
              </ul></td>
           <?php if($_REQUEST['action']!='edit'){?> <td width="5%" align="right">
			<input name="save"  type="submit" class="inputton" id="save" value="Save"></td><?php }?>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td class="pHeadLine"></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td valign="top"><table width="100%" cellpadding="5" cellspacing="0" class="tblBorder">
         
          <tr class="text">
            <td colspan="2" class="pad head">Add Demo Stock</td>
          </tr>
      
          <tr class="text">
            <td class="pad">Office Location</td>
            <td><select name="office_location" id="office_location" class="inpuTxt" required   >
                <?php
	echo '<option value="" selected="selected" >Select Office Location</option>';
	$query_off_locations			 = " deleteflag = 'active' order by location";
	//echo $query;
	//$rs_role = $s->getData_without_condition('tbl_admin');
	$rs_off_locations 		 = $s->selectWhere('tbl_location', $query_off_locations);
	if(mysqli_num_rows($rs_off_locations)>0)
	{
		while($row_off_locations = mysqli_fetch_object($rs_off_locations))
		{	

?>
                <option value="<?php echo $row_off_locations->id;?>" <?php if($office_location_id ==$row_off_locations->id) { echo "selected";}?> ><?php echo $row_off_locations->location;?></option>
                <?php
		}
	}
?>
              </select></td>
          </tr>
          <tr class="text">
            <td class="pad">Select Product Category</td>
            <td><?php 
	$sqlCate = "select * from tbl_application where deleteflag = 'active' order by application_name asc";
	$rsCate  = mysqli_query($GLOBALS["___mysqli_ston"], $sqlCate);
	//echo "<br>".	$selected_c->pro_id;
if(mysqli_num_rows($rsCate)>0)
	{
?>
              <select name="category_id" id="category_id" onChange="htmlData_comp_details('demo_stock_product_ajax.php', 'tm='+this.value);"  class="inpuTxt" required>
                <option value="" <?php if($rowCate->application_id == $category_id) {	echo "selected='selected'"; }?>>All Category</option>
                <?php	while($rowCate = mysqli_fetch_object($rsCate))
		{
?>
                <option value="<?php echo $rowCate->application_id;?>" <?php if($rowCate->application_id == $category_id){echo "selected='selected'";}?>><?php echo $rowCate->application_name;?></option>
                <?php			
		}?>
              </select>
              <?php
	}

?></td>
          </tr>
          <tr class="text">
            <td class="pad" >Select Product</td>
            <td id="txtresultcomp1"><select name="product_add_DO"  class="inpuTxt" required>
                <option value="">Select Product</option>
                <?php  
//		  $category_id   = $_REQUEST["tm"];



						$SQLDOProduct1 	= "
Select tbl_products.pro_title,tbl_products.status,tbl_products.pro_id, tbl_index_g2.match_pro_id_g2 FROM tbl_products INNER JOIN tbl_index_g2 ON tbl_index_g2.match_pro_id_g2=tbl_products.pro_id where tbl_products.deleteflag='active' and tbl_index_g2.pro_id='$category_id' order by pro_title asc ";
						$SQLDOProductRS1	 	= mysqli_query($GLOBALS["___mysqli_ston"], $SQLDOProduct1);  
						while($SQLDOProductROW1=mysqli_fetch_object($SQLDOProductRS1)) {

					?>
                <option value="<?php echo $SQLDOProductROW1->pro_id; ?>" <?php if($SQLDOProductROW1->pro_id == $pro_id) {	echo "selected='selected'"; }?>><?php echo $SQLDOProductROW1->pro_title; ?></option>
                <?php } ?>
              </select></td>
          </tr>
          <tr class="text">
            <td class="pad" >Warehouse Stock </td>
            <td id="avail_qty"><input name="avail_qty" type="text"  id="available_qty" value="<?php echo $stock_available;?>" <?php echo $class;?> class="inpuTxt" required="required" readonly="readonly"  /></td>
          </tr>
          <tr class="text">
            <td class="pad">Demo Stock Quantity</td>
            <td><input name="qty" type="number" min="1"  id="qty" value="<?php echo $qty;?>" class="inpuTxt" required="required"  /></td>
          </tr>
          <tr class="text">
            <td class="pad">Serial No</td>
            <td><input name="serial_no" type="text"  id="serial_no" value="<?php echo $serial_no;?>" class="inpuTxt" required="required"  /></td>
          </tr>
          <tr class="text" style="display:none">
            <td class="pad"> Status</td>
            <td><select name="status" class="inpuTxt" id="status">
                <option value="active" <?php if( $status =="active"){ echo "selected='selected'";}?>>Active</option>
                <option value="inactive" <?php if($status =="inactive"){ echo "selected='selected'";}?>>Inactive</option>
              </select></td>
          </tr>
          <?php if($_REQUEST['action']!='edit'){?>
          <tr class="text">
            <td class="pad"></td>
            <td><input name="save"  type="submit" class="inputton" id="save" value="Save"></td>
          </tr>
          <?php }?>
          <tr >
            <td class="redstar pad" colspan="2"> * Required Fields </td>
          </tr>
          
          
          
           <tr >
            <td class=" pad" colspan="2" align="center"> <strong><?php //echo $s->stock_office_location_name($office_location_id);?></strong>
			
			<?php 
			
			
			/*function old_stock_chart($pcode){
$sql_t="Select stock_id,transf_stock_id,remarks from tbl_demo_stock where stock_id='$pcode' ";
			$rs_t=mysqli_query($GLOBALS["___mysqli_ston"],$sql_t);
			$rs_t_num=mysqli_num_rows($rs_t);
			if($rs_t_num>0)
			{
//			print_r($row_t);?>
<?php  $row_t=mysqli_fetch_object($rs_t);?>
<?php  $transf_stock_id_get=$row_t->transf_stock_id;?>
<ul align="center">
<li ><img src="images/truck.png" height="50px" /></li>
<li><?php echo $s->stock_remarks($transf_stock_id_get);//$row_t->remarks; ?></li>

</ul>
<?php
 			 old_stock_chart($transf_stock_id_get);
			}
			return $transf_stock_id_get;
			}
			echo old_stock_chart($pcode);*/
			
					echo $s->old_stock_chart($pcode);?>
            </td>
          </tr>
          
        </table></td>
    </tr>
  </table>
</form>
<script id="rendered-js">

function validatePassword(){
	var available_qty = document.getElementById("available_qty");
var qty = document.getElementById("qty");
	//alert(available_qty.value);
	//alert(qty.value);
  if(parseInt(available_qty.value) < parseInt(qty.value)) {
    qty.setCustomValidity("Available qty less than required qty");
		return false;
  } else {
    qty.setCustomValidity('');
	return true;
  }
}

available_qty.onchange = validatePassword;
qty.onkeyup = validatePassword;
</script>
<!--<script language="JavaScript" type="text/javascript">
 var frmvalidator = new Validator("frx1");
 frmvalidator.addValidation("location","req","Please enter Name");
  frmvalidator.addValidation("application_heading","req","Please enter Heading Name");
</script>-->