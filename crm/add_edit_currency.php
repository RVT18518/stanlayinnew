<link href="../css/style.css" rel="stylesheet" type="text/css" />
<?php
	$data_action=$_REQUEST["action"];
	$pcode=$_REQUEST["pcode"];
	if($data_action=="edit")
	{
		$rs   	 = $s->getData_with_condition("tbl_currencies","currency_id",$pcode);
		$row  	 = mysqli_fetch_object($rs);
		$title   = $row->currency_title;
		$code    = $row->currency_code;
		$symbol  = $row->currency_symbol;
		$value   = $row->currency_value;
		$default = $row->currency_status;
		
		$data_action="update";
	}
	if($data_action=="add_new")
	{
		$data_action="insert";
	}	
?>

<form id="frx1" name="frx1" method="post" action="index.php?pagename=currency&action=<?php echo $data_action;?>&pcode=<?php echo $pcode;?>" onsubmit="return isnum();">
  <table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="24%" class="pageheadTop">Currency Manager</td>
            <td width="76%" class="headLink"><ul>
                <li><a href="#" onclick="back_page();">Back </a></li>
              </ul></td>
            <td><input type="submit"name="add"id="add"class="inputton"value="Save"/></td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td class="pHeadLine"></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><table width="100%" border="0" cellpadding="0" cellspacing="0" class="tblBorder">
          <tr>
            <td colspan="2" class="pagehead">Currencies</td>
          </tr>
          <tr class="text">
            <td width="10%" class="pad">Titles<span class="redstar"> *</span></td>
            <td width="90%"><input name="title" type="text" class="inpuTxt" id="title" value="<?php echo $title;?>" /></td>
          </tr>
          <tr class="text" >
            <td class="pad">Code<span class="redstar"> *</span></td>
            <td><input name="code" type="text" class="inpuTxt" id="code" value="<?php echo $code;?>" /></td>
          </tr>
          <tr class="text"  >
            <td class="pad">Symbol<span class="redstar"> *</span></td>
            <td><input name="symbol" type="text" class="inpuTxt" id="symbol" value="<?php echo $symbol;?>" /></td>
          </tr>
          <tr class="text"  >
            <td class="pad">Value<span class="redstar"> *</span></td>
            <td><input name="value" type="text" class="inpuTxt" id="value" value="<?php echo $value;?>" /></td>
          </tr>
          <tr class="text"  >
            <td class="pad">Set as Default</td>
            <td><?php if($default=='yes')
			{
		?>
              <input name="default" type="checkbox" id="default" value="yes" checked="checked"/>
              <?php 
			}
			else
			{
		?>
              <input name="default" type="checkbox" id="default" value="yes"/>
              <?php 
			}
		?></td>
          </tr>
          <tr class="text"  >
            <td class="pad">&nbsp;</td>
            <td><input name="Submit" type="submit" class="inputton" value="Save"/></td>
          </tr>
          <tr class="text">
            <td class="redstar pad" colspan="2"> * Required Fields </td>
          </tr>
        </table></td>
    </tr>
  </table>
</form>
<script language="JavaScript" type="text/javascript">
 var frmvalidator = new Validator("frx1");
 frmvalidator.addValidation("title","req","Please enter Currency Name");
 frmvalidator.addValidation("code","req","Please enter Currency Code");
 frmvalidator.addValidation("code","maxlen=3","Please enter 3 Char Currency Code ");
 frmvalidator.addValidation("code","minlen=3","Please enter 3 Char Currency Code");
 frmvalidator.addValidation("symbol","req","Please enter Currency Symbol");
 frmvalidator.addValidation("value","req","Please enter Currency Value");
 frmvalidator.addValidation("value","regexp=^[0-9.0-9]","Please enter Currency as numeric Value"); 

</script>