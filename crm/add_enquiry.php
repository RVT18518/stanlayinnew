<?php 
//this new dynamic module for enq remarks added by rumit on dated 18Jun 2018
	$data_action=$_REQUEST['action'];
	$pcode=$_REQUEST["pcode"];
	
	if($data_action=="edit")
	{
		$rs=$s->getData_with_condition('tbl_web_enq','ID',$pcode);
		$row=mysqli_fetch_object($rs);
		$Cus_name        = stripslashes($row->Cus_name);


		$Cus_email        = stripslashes($row->Cus_email);

		$Cus_mob        = stripslashes($row->Cus_mob);

		$Cus_msg        = stripslashes($row->Cus_msg);

		$ref_source        = stripslashes($row->ref_source);
		$Cus_status      = $row->Cus_status;
		$Cus_description = stripslashes($row->Cus_description);		
		$data_action = "update";
	}
	if($data_action == "add_new")
	{
		$data_action = "insert";
	}
	
	
	if($_REQUEST['action']=='update' ||$_REQUEST['action']=='insert')
	{
		$fileArray["enq_remark"]        = addslashes($_REQUEST["enq_remark"]);

		$result_enq_team_updated = $s->editRecord('tbl_web_enq',$fileArray,'ID',$pcode);//exit;
		$s->pageLocation("index.php?pagename=add_enquiry&action=edit&pcode=$pcode&result=$result_enq_team_updated"); 
	}

?>
<form name="frx1" id="frx1" action="index.php?pagename=add_enquiry&action=<?php echo $data_action;?>&pcode=<?php echo $pcode;?>" method="post" enctype="multipart/form-data">

  <table width="100%" align="center" cellpadding="0" cellspacing="0" class="pagecontent" >

    <tr>

    <tr>

      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

          <tr>

            <td width="38%" class="pageheadTop">Enquiry Manager</td>

            <td width="52%" class="headLink"><ul>

                <li><a href="index.php?pagename=web_enq_manager">Back</a></li>

              </ul></td>

            <td width="10%" align="right"><input name="save"  type="submit" class="inputton" id="save" value="Save"></td>

          </tr>

        </table></td>

    </tr>

    <tr>

      <td class="pHeadLine"></td>

    </tr>

    <tr>

      <td>&nbsp;</td>

    </tr>
    
    
    <tr>
    <td><?php
		if($_REQUEST['action']=='ChangeStatus')
		{
			if($result==0)
			{
				echo "<p class='success'>Status Change Successfully</p><br/>";	
			}
			else if($result==1)
			{
				echo "<p class='error'>Status Changing Fails</p><br/>";	
			}
			$data_action = "Changed";
		}
		
		
		else if($_REQUEST['result']=='0')
		{
			 if($result==0)
			{
				echo "<p class='success'>".record_update."</p><br/>";	
			}
			else if($result==1)
			{
				echo "<p class='error'>".record_update."</p><br/>";	
			}
		}
?></td>
  </tr>
    <tr>

      <td><table width="100%" cellpadding="5" cellspacing="0" class="tblBorder">

          <tr>

            <td class="pagehead" colspan="2"> Enquiry</td>

          </tr>

          <tr class="text">

            <td width="18%" class="pad"> Name <span class="redstar">*</span></td>

            <td width="24%"><input type="text" value="<?php echo ucfirst(stripslashes($row->Cus_name)) ; ?>" name="enq_cust_name" readonly="readonly" /></td>

          </tr>

          <?php /*?><tr class="text">
	  <td class="pad">Logo</td>
	  <td><input name="Cus_logo" type="file"  id="Cus_logo" value=""></td>
	</tr><?php */?>

          <tr class="text">

            <td width="18%" valign="top" class="pad"> Email</td>

            <td width="24%"><input type="text" value="<?php echo ucfirst(stripslashes($row->Cus_email)) ; ?>" name="enq_cust_email" readonly="readonly" /></td>

          </tr>

          <tr class="text">

            <td width="18%" class="pad">Mobile</td>

            <td width="24%"><input type="text" value="<?php echo ucfirst(stripslashes($row->Cus_mob)) ; ?>" name="enq_cust_mobile" readonly="readonly" /></td>

          </tr>

          <?php

		  if($Cus_msg!='0')

		  {

			  $Cus_msg_style="";

			  $Cus_msg_style_add_more="none";

		  }

		  else

		  {

			    $Cus_msg_style="none";

			  $Cus_msg_style_add_more="";

		  }

		  
		  ?>

          

          <?php

		  if($ref_source!='0')

		  {

			  $ref_source_style="";

			  $ref_source_style_add_more="none";

		  }

		  else

		  {

			    $ref_source_style="none";

				$ref_source_style_add_more="";

		  }

		  ?>

          <tr class="text" id="ref_sourcebox" style="display:<?php echo $ref_source_style;?>">

            <td class="pad"> Enquiry Source<span class="redstar"> * </span></td>

            <td><select name="ref_source" id="ref_source" class="inpuTxtSelect" disabled="disabled" >
                              <option value="" >Select Enquiry Source</option>
                              <?php
	$rs_role = $s->getData_without_condition('tbl_enq_source','enq_source_name');
	if(mysqli_num_rows($rs_role)>0)
	{
		while($row_role = mysqli_fetch_object($rs_role))
		{	
?>
                              <option value="<?php echo $row_role->enq_source_description;?>" <?php if($row_role->enq_source_description==$ref_source){ echo "selected='selected'";} ?> ><?php echo $row_role->enq_source_name;?></option>
                              <?php
		}
	}
?>
                            </select></td>

          </tr>

          <tr class="text">

            <td width="18%" class="pad"> Remark </td>

            <td width="24%"><textarea rows="5" name="enq_remark" required="required" ><?php echo ucfirst(stripslashes($row->enq_remark)) ; ?></textarea></td>

          </tr>

          <tr class="text">

            <td width="18%" class="pad"></td>

            <td width="24%"><input name="save"  type="submit" class="inputton" id="save" value="Save"></td>

          </tr>

          <tr class="text">

            <td class="redstar pad" colspan="2"> * Required Fields </td>

          </tr>

        </table></td>

    </tr>

  </table>

</form>
