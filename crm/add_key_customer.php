<?php 
//this new dynamic module for add mode master added by rumit on dated 27Jul 2019
	$data_action=$_REQUEST['action'];
	$pcode=$_REQUEST["pcode"];
	if($data_action=="edit")
	{
		$rs					= $s->getData_with_condition('tbl_key_customer_master','key_customer_id',$pcode);
		$row				= mysqli_fetch_object($rs);
		$key_customer_name        	= stripslashes($row->key_customer_name);
		$key_customer_abbrv       	= stripslashes($row->key_customer_abbrv);
		$min_value     = stripslashes($row->min_value);
		$max_value    = stripslashes($row->max_value);
		$date_range   = stripslashes($row->date_range);
		//$sub_key_customer_lead3   = stripslashes($row->sub_key_customer_lead3);
		$key_customer_status      	= $row->key_customer_status;
		if($row->key_customer_description==''){
		$key_customer_description 	= str_replace(' ', '_',$row->key_customer_name) ;//stripslashes($row->key_customer_description);		
		}
		else
		{
		$key_customer_description 	= str_replace(' ', '_',$row->key_customer_description) ;//stripslashes($row->key_customer_description);					
		}
		$data_action = "update";
	}
	if($data_action == "add_new")
	{
		$data_action = "insert";
	}
?>

<form name="frx1" id="frx1" action="index.php?pagename=manage_key_customer_master&action=<?php echo $data_action;?>&pcode=<?php echo $pcode;?>" method="post" enctype="multipart/form-data">
  <div class="panel panel-default">
    <div class="row">&nbsp;</div>
    <div class="row">
      <div class="col-xs-10 col-sm-6 col-md-4 col-lg-12">
        <fieldset>
          <legend class="pseudo_border"> Key Customer</legend>
        </fieldset>
        <div class="form-group col-xs-10 col-sm-6 col-md-6 col-lg-6">
          <label for="exampleInputEmail1"> Name<span class="redstar"> *</span></label>
          <input name="key_customer_name" type="text" class="form-control"  value="<?php echo $key_customer_name;//str_replace("PVT. LTD."," ",$comp_name);?>" required="required"   autocomplete="nope" placeholder="Enter Key Customer Name" >
          <!--Underscores and hyphen allowed with small brackets--> 
        </div>
        <div class="form-group col-xs-10 col-sm-4 col-md-4 col-lg-6">
          <label for="exampleInputEmail1">Abbreviation<span class="redstar"> *</span></label>
          <input name="key_customer_abbrv" type="text" class="form-control"  value="<?php echo $key_customer_abbrv;//str_replace("PVT. LTD."," ",$comp_name);?>" required="required"   autocomplete="nope" placeholder="Enter Key Customer Abbreviation" >
        </div>
        <div class="form-group col-xs-10 col-sm-4 col-md-4 col-lg-6" >
          <label for="exampleInputEmail1">Min Value<span class="redstar"> *</span></label>
          <input type="text" name="min_value" class="form-control" placeholder="Enter Min value"  value="<?php echo $min_value;?>" required="required" />
        </div>
        <div class="form-group col-xs-10 col-sm-6 col-md-6 col-lg-6" >
          <label for="exampleInputEmail1">Max Value<span class="redstar"> *</span></label>
          <input type="text" name="max_value" class="form-control" placeholder="Enter Max Value"   value="<?php echo $max_value;?>" required="required" />
        </div>
        <div class="form-group col-xs-10 col-sm-4 col-md-4 col-lg-6" >
          <label for="exampleInputEmail1">Description</label>
          <input type="text" name="key_customer_description" class="form-control" placeholder="Description"  value="<?php echo $key_customer_description;?>" />
        </div>
        <div class="form-group col-xs-10 col-sm-6 col-md-6 col-lg-6">
          <label for="exampleInputEmail1">Date Range<span class="redstar"> *</span></label>
          <input type="text" name="date_range" class="form-control" placeholder="Date range in months"   value="<?php echo $date_range;?>" required="required" />
        </div>
        <div class="form-group col-xs-10 col-sm-6 col-md-6 col-lg-6">
          <label for="exampleInputEmail1">Status</label>
          <select name="key_customer_status" id="key_customer_status" class="form-control">
            <option value="active" <?php if($key_customer_status == "active"){ echo "selected='selected'";}?>>Active</option>
            <option value="inactive" <?php if($key_customer_status == "inactive"){ echo "selected='selected'";}?>>Inactive</option>
          </select>
        </div>
        <div class="row col-xs-10 col-sm-4 col-md-4 col-lg-6">&nbsp;</div>
      </div>
      <!--        <div class="col-xs-10 col-sm-4 col-md-4 col-lg-1">blank </div>--> 
      
    </div>
    <div class="clearfix"></div>
    <div class="col-xs-10 col-sm-4 col-md-4 col-lg-4">
      <input type="submit" name='save' id='save' class="btn btn-danger" value='Save' />
    </div>
    <div class="clearfix"></div>
    <br />
    <br />
  </div>
</form>
