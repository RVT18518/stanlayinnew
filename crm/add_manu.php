<?php 
	$data_action=$_REQUEST['action'];
	$pcode=$_REQUEST["pcode"];
	if($data_action=="delete_image")
	{
		$rs_del    = $s->getData_with_condition('tbl_manufacturers','manufacturers_id',$pcode);
		$row_del   = mysqli_fetch_object($rs_del);
		$del_image_large  = $row_del->manufacturers_logo_large;
		$del_image_medium = $row_del->manufacturers_logo_medium;
		$del_image_small  = $row_del->manufacturers_logo_small;
		if(file_exists($del_image_large)>0)
		{
			unlink($del_image_large);
		}
		if(file_exists($del_image_medium)>0)
		{
			unlink($del_image_medium);
		}
		if(file_exists($del_image_small)>0)
		{
			unlink($del_image_small);
		}

		$sql_remove="update tbl_manufacturers SET manufacturers_logo_large = '' , manufacturers_logo_medium = '', manufacturers_logo_small = ''  where manufacturers_id = $pcode";
		mysqli_query($GLOBALS["___mysqli_ston"],$sql_remove);
		$data_action="edit";
	}
	if($data_action=="edit")
	{
		$rs=$s->getData_with_condition('tbl_manufacturers','manufacturers_id',$pcode);
		$row=mysqli_fetch_object($rs);
		$manufacturers_name        = stripslashes($row->manufacturers_name);
		$manufacturers_status      = $row->manufacturers_status;
		$manufacturers_description = stripslashes($row->manufacturers_description);
		$manufacturers_logo        = $row->manufacturers_logo_small;
		$tax_class_id		       = $row->tax_class_id;
		$est_arrival_min		   = $row->est_arrival_min;
		$est_arrival_max		   = $row->est_arrival_max;	
		
		$data_action = "update";
	}
	if($data_action == "add_new")
	{
		$data_action = "insert";
	}
	
	
////////////////// 09-01-2012 /////////////////////////////////////////////////////
/*echo	strtoupper($manufacturers_name);

echo "TEST <br>".$sql="SELECT * from tbl_manufacturers where deleteflag='active' and status='active'";
$rs=mysqli_query($GLOBALS["___mysqli_ston"],$sql);
$num_row=mysqli_num_rows($rs);

while(mysqli_fetch_array($row));
{
	
	}*/
////////////////////////////////////////////////////////////////////

?>

<form name="frx1" id="frx1" action="index.php?pagename=manage_manu&action=<?php echo $data_action;?>&pcode=<?php echo $pcode;?>" method="post" enctype="multipart/form-data">
  <table width="100%" align="center" cellpadding="0" cellspacing="0" class="pagecontent" >
    <tr>
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="38%" class="pageheadTop">Manufacturer Manager</td>
            <td width="52%" class="headLink"><ul>
                <li><a href="index.php?pagename=manage_manu">Back</a></li>
              </ul></td>
            <td width="10%" align="right"><input name="save"  type="submit" class="inputton" id="save" value="Save"></td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td class="pHeadLine"></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><table width="100%" cellpadding="5" cellspacing="0" class="tblBorder">
          <tr>
            <td class="pagehead" colspan="2"> Manufacturer</td>
          </tr>
          <tr>
            <td colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              </table></td>
          </tr>
          <?php
	  if(trim($manufacturers_logo)!=""){
	  ?>
          <tr class="text">
            <td class="pad"><img src="../<?php echo $manufacturers_logo ;?>" onerror="style.display='none';" /></td>
            <td><a href="index.php?pagename=add_manu&action=delete_image&pcode=<?php echo $pcode;?>">Remove Image</a></td>
            <?php
	  }
	  ?>
          </tr>
          <tr class="text">
            <td width="27%" class="pad"> Name <span class="redstar">*</span></td>
            <td width="73%"><input name="manufacturers_name" type="text" class="inpuTxt" id="manufacturers_name" value="<?php echo $manufacturers_name;?>"></td>
          </tr>
          <?php /*?><tr class="text">
	  <td class="pad">Logo</td>
	  <td><input name="manufacturers_logo" type="file"  id="manufacturers_logo" value=""></td>
	</tr><?php */?>
          <tr class="text">
            <td valign="top" class="pad"> Description</td>
            <td><textarea name="manufacturers_description"  rows="4" class="inpuTxt" id="manufacturers_description" ><?php echo $manufacturers_description;?></textarea></td>
          </tr>
          <tr class="text">
            <td class="pad" >Tax </td>
            <td><input type="text" name="tax_class_id" id="tax_class_id" value="<?php echo $tax_class_id;?>" />
              
              <!--<select name="tax_class_id" id="tax_class_id" class="inpuTxt">
          <option value="0">Select Tax Class</option>-->
              
              <?php /*?> <?php
    //$tax_query = $s->getData_without_condition('tbl_tax_class');
	$query       =  "deleteflag ='active'  and  tax_class_status ='active'";
	$tax_query   =  $s->selectwhere('tbl_tax_class',$query);
    while($tax_result = $s->getRecord($tax_query))
	{
?>
          <option value="<?php echo $tax_result['tax_class_id'] ; ?>"	 <?php if( $rowProPrice->tax_class_id == $tax_result['tax_class_id']){ echo selected; }?>><?php echo $tax_result['tax_class_name'];?></option>
          <?php 
	} 
?>
        </select><?php */?></td>
          </tr>
          <tr class="text">
            <td class="pad">Estimated Delivery </td>
            <td valign="top"><table width="25%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                  <td>&nbsp;</td>
                  <td><strong>Min</strong></td>
                  <td>&nbsp;</td>
                  <td><strong>Max</strong></td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td></td>
                  <td><input name="est_arrival_min" type="text" class="inpuTxtsmall" id="est_arrival_min" value="<?php echo $est_arrival_min; ?>" /></td>
                  <td></td>
                  <td><input name="est_arrival_max" type="text" class="inpuTxtsmall" id="est_arrival_max" value="<?php echo $est_arrival_max; ?>" /></td>
                  <td><strong>days</strong></td>
                </tr>
              </table></td>
          </tr>
          <tr class="text">
            <td width="16%" class="pad">&nbsp;</td>
            <td width="84%">&nbsp;&nbsp;&nbsp;</td>
          </tr>
          <tr class="text">
            <td class="pad"> Status</td>
            <td><select name="manufacturers_status" class="inpuTxt" id="manufacturers_status">
                <option value="active" <?php if( $manufacturers_status =="active"){ echo "selected='selected'";}?>>Active</option>
                <option value="inactive" <?php if($manufacturers_status =="inactive"){ echo "selected='selected'";}?>>Inactive</option>
              </select>
              </select></td>
          </tr>
          <tr class="text">
            <td class="pad"></td>
            <td><input name="save"  type="submit" class="inputton" id="save" value="Save"></td>
          </tr>
          <tr class="text">
            <td class="redstar pad" colspan="2"> * Required Fields </td>
          </tr>
        </table></td>
    </tr>
  </table>
</form>
<script language="JavaScript" type="text/javascript">
 var frmvalidator = new Validator("frx1");
 frmvalidator.addValidation("manufacturers_name","req","Please enter Manufacturer  Name");
</script>