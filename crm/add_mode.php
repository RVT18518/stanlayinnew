<?php 
//this new dynamic module for add mode master added by rumit on dated 27Jul 2019
	$data_action=$_REQUEST['action'];
	$pcode=$_REQUEST["pcode"];
	if($data_action=="edit")
	{
		$rs					= $s->getData_with_condition('tbl_mode_master','mode_id',$pcode);
		$row				= mysqli_fetch_object($rs);
		$mode_name        	= stripslashes($row->mode_name);
		$mode_abbrv       	= stripslashes($row->mode_abbrv);
		//$mode_manager     = stripslashes($row->mode_manager);
		//$sub_mode_lead    = stripslashes($row->sub_mode_lead);
		//$sub_mode_lead2   = stripslashes($row->sub_mode_lead2);
		//$sub_mode_lead3   = stripslashes($row->sub_mode_lead3);
		$mode_status      	= $row->mode_status;
		if($row->mode_description==''){
		$mode_description 	= str_replace(' ', '_',$row->mode_name) ;//stripslashes($row->mode_description);		
		}
		else
		{
		$mode_description 	= str_replace(' ', '_',$row->mode_description) ;//stripslashes($row->mode_description);					
		}
		$data_action = "update";
	}
	if($data_action == "add_new")
	{
		$data_action = "insert";
	}
?>
<form name="frx1" id="frx1" action="index.php?pagename=manage_mode_master&action=<?php echo $data_action;?>&pcode=<?php echo $pcode;?>" method="post" enctype="multipart/form-data">
  <table width="100%" align="center" cellpadding="0" cellspacing="0" class="pagecontent" >
    <tr>
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="38%" class="pageheadTop">Add Mode </td>
            <td width="52%" class="headLink"><ul>
                <li><a href="index.php?pagename=manage_mode_master">Back</a></li>
              </ul></td>
            <td width="10%" align="right"><input name="save"  type="submit" class="inputton" id="save" value="Save"></td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td class="pHeadLine"></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><table width="100%" cellpadding="5" cellspacing="0" class="tblBorder">
          <tr>
            <td class="pagehead" colspan="2"> Mode</td>
          </tr>
          <tr class="text">
            <td width="18%" class="pad"> Name <span class="redstar">*</span></td>
            <td width="24%"><input name="mode_name" type="text" class="inpuTxt" id="mode_name" value="<?php echo $mode_name;?>" required="required"></td>
          </tr>
          <?php /*?><tr class="text">

	  <td class="pad">Logo</td>

	  <td><input name="mode_logo" type="file"  id="mode_logo" value=""></td>

	</tr><?php */?>
          <tr class="text" style="display:non">
            <td width="18%" valign="top" class="pad"> Permalink</td>
            <td width="24%">
            <input name="mode_description" type="text" class="inpuTxt" id="mode_description" value="<?php echo $mode_description;?>" />
           </td>
          </tr>
          <tr class="text" style="display:none">
            <td width="18%" class="pad">Abbreviation </td>
            <td width="24%"><input name="mode_abbrv" type="text" class="inpuTxt" id="mode_abbrv" value="<?php echo $mode_abbrv;?>" maxlength="2" /></td>
          </tr>
          <tr class="text">
            <td width="18%" class="pad"> Status </td>
            <td width="24%"><select name="mode_status" class="inpuTxt" id="mode_status">
                <option value="active" <?php if($mode_status =="active"){ echo "selected='selected'";}?>>Active</option>
                <option value="inactive" <?php if($mode_status =="inactive"){ echo "selected='selected'";}?>>Inactive</option>
              </select></td>
          </tr>
          <tr class="text">
            <td width="18%" class="pad"></td>
            <td width="24%"><input name="save"  type="submit" class="inputton" id="save" value="Save"></td>
          </tr>
          <tr class="text">
            <td class="redstar pad" colspan="2"> * Required Fields </td>
          </tr>
        </table></td>
    </tr>
  </table>
</form>