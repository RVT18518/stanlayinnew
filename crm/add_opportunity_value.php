<?php 
//this new dynamic module for add opportunity_value master added by rumit on dated 06Oct 2021
		$data_action			= $_REQUEST['action'];
		$pcode					= $_REQUEST["pcode"];
	if($data_action=="edit")
	{
		$rs						= $s->getData_with_condition('tbl_opportunity_value_master','opportunity_value_id',$pcode);
		$row					= mysqli_fetch_object($rs);
		$opportunity_value_name        	= stripslashes($row->opportunity_value_name);
		$opportunity_value_abbrv       	= stripslashes($row->opportunity_value_abbrv);
		$display_order       			= stripslashes($row->display_order);
		
		//$opportunity_value_manager     = stripslashes($row->opportunity_value_manager);
		//$sub_opportunity_value_lead    = stripslashes($row->sub_opportunity_value_lead);
		//$sub_opportunity_value_lead2   = stripslashes($row->sub_opportunity_value_lead2);
		//$sub_opportunity_value_lead3   = stripslashes($row->sub_opportunity_value_lead3);
		$opportunity_value_status      	= $row->opportunity_value_status;
		if($row->opportunity_value_description==''){
		$opportunity_value_description 	= str_replace(' ', '_',$row->opportunity_value_name) ;//stripslashes($row->opportunity_value_description);		
		}
		else
		{
		$opportunity_value_description 	= str_replace(' ', '_',$row->opportunity_value_description) ;//stripslashes($row->opportunity_value_description);					
		}
		$data_action = "update";
	}
	if($data_action == "add_new")
	{
		$data_action = "insert";
	}
?>
<form name="frx1" id="frx1" action="index.php?pagename=manage_opportunity_value_master&action=<?php echo $data_action;?>&pcode=<?php echo $pcode;?>" method="post" enctype="multipart/form-data">
  <table width="100%" align="center" cellpadding="0" cellspacing="0" class="pagecontent" >
    <tr>
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="38%" class="pageheadTop">Add Opportunity Value</td>
            <td width="52%" class="headLink"><ul>
                <li><a href="index.php?pagename=manage_opportunity_value_master">Back</a></li>
              </ul></td>
            <td width="10%" align="right"><input name="save"  type="submit" class="inputton" id="save" value="Save"></td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td class="pHeadLine"></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><table width="100%" cellpadding="5" cellspacing="0" class="tblBorder">
          <tr>
            <td class="pagehead" colspan="2"> Opportunity Value</td>
          </tr>
          <tr class="text">
            <td width="18%" class="pad"> Name <span class="redstar">*</span></td>
            <td width="24%"><input name="opportunity_value_name" type="text" class="inpuTxt" id="opportunity_value_name" value="<?php echo $opportunity_value_name;?>" required="required"   /></td>
          </tr>
          <?php /*?><tr class="text">

	  <td class="pad">Logo</td>

	  <td><input name="opportunity_value_logo" type="file"  id="opportunity_value_logo" value=""></td>

	</tr><?php */?>
          <tr class="text" style="display:none">
            <td width="18%" valign="top" class="pad"> Description</td>
            <td width="24%">
            <input name="opportunity_value_description" type="text" class="inpuTxt" id="opportunity_value_description" value="<?php echo $opportunity_value_description;?>" />
           </td>
          </tr>
          <tr class="text"  >
            <td width="18%" class="pad">Abbreviation<span class="redstar">*</span> </td>
            <td width="24%"><input name="opportunity_value_abbrv" type="text" class="inpuTxt" id="opportunity_value_abbrv" value="<?php echo $opportunity_value_abbrv;?>"  required="required" /></td>
          </tr>
          
          
            <tr class="text"  >
            <td width="18%" class="pad">Display Order<span class="redstar">*</span> </td>
            <td width="24%"><input name="display_order" type="text" class="inpuTxt" id="display_order" value="<?php echo $display_order;?>"  required="required" /></td>
          </tr>
          
          <tr class="text">
            <td width="18%" class="pad"> Status </td>
            <td width="24%"><select name="opportunity_value_status" class="inpuTxt" id="opportunity_value_status">
                <option value="active" <?php if( $opportunity_value_status =="active"){ echo "selected='selected'";}?>>Active</option>
                <option value="inactive" <?php if($opportunity_value_status =="inactive"){ echo "selected='selected'";}?>>Inactive</option>
              </select></td>
          </tr>
          <tr class="text">
            <td width="18%" class="pad"></td>
            <td width="24%"><input name="save"  type="submit" class="inputton" id="save" value="Save"></td>
          </tr>
          <tr class="text">
            <td class="redstar pad" colspan="2">* Required Fields</td>
          </tr>
        </table></td>
    </tr>
  </table>
</form>