<?php 
//this new dynamic module for add mode master added by rumit on dated 26 Nov 2020
	$data_action=$_REQUEST['action'];
	$pcode=$_REQUEST["pcode"];
	if($data_action=="edit")
	{
		$rs					= $s->getData_with_condition('tbl_product_type_master','product_type_id',$pcode);
		$row				= mysqli_fetch_object($rs);
		$product_type_name        	= stripslashes($row->product_type_name);
		$product_type_abbrv       	= stripslashes($row->product_type_abbrv);
		//$product_type_manager     = stripslashes($row->product_type_manager);
		//$sub_product_type_lead    = stripslashes($row->sub_product_type_lead);
		//$sub_product_type_lead2   = stripslashes($row->sub_product_type_lead2);
		//$sub_product_type_lead3   = stripslashes($row->sub_product_type_lead3);
		$product_type_status      	= $row->product_type_status;
		if($row->product_type_description==''){
		$product_type_description 	= str_replace(' ', '_',$row->product_type_name) ;//stripslashes($row->product_type_description);		
		}
		else
		{
		$product_type_description 	= str_replace(' ', '_',$row->product_type_description) ;//stripslashes($row->product_type_description);					
		}
		$data_action = "update";
	}
	if($data_action == "add_new")
	{
		$data_action = "insert";
	}
?>
<form name="frx1" id="frx1" action="index.php?pagename=manage_product_type_master&action=<?php echo $data_action;?>&pcode=<?php echo $pcode;?>" method="post" enctype="multipart/form-data">
  <table width="100%" align="center" cellpadding="0" cellspacing="0" class="pagecontent" >
    <tr>
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="38%" class="pageheadTop">Add Product Type </td>
            <td width="52%" class="headLink"><ul>
                <li><a href="index.php?pagename=manage_product_type_master">Back</a></li>
              </ul></td>
            <td width="10%" align="right"><input name="save"  type="submit" class="inputton" id="save" value="Save"></td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td class="pHeadLine"></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><table width="100%" cellpadding="5" cellspacing="0" class="tblBorder">
          <tr>
            <td class="pagehead" colspan="2"> Product Type</td>
          </tr>
          <tr class="text">
            <td width="18%" class="pad"> Name <span class="redstar">*</span></td>
            <td width="24%"><input name="product_type_name" type="text" class="inpuTxt" id="product_type_name" value="<?php echo $product_type_name;?>" required="required"></td>
          </tr>
          <?php /*?><tr class="text">

	  <td class="pad">Logo</td>

	  <td><input name="product_type_logo" type="file"  id="product_type_logo" value=""></td>

	</tr><?php */?>
          <tr class="text" style="display:none">
            <td width="18%" valign="top" class="pad"> Permalink</td>
            <td width="24%">
            <input name="product_type_description" type="text" class="inpuTxt" id="product_type_description" value="<?php echo $product_type_description;?>" />
           </td>
          </tr>
          <tr class="text" style="display:none">
            <td width="18%" class="pad">Abbreviation </td>
            <td width="24%"><input name="product_type_abbrv" type="text" class="inpuTxt" id="product_type_abbrv" value="<?php echo $product_type_abbrv;?>" maxlength="2" /></td>
          </tr>
          <tr class="text">
            <td width="18%" class="pad"> Status </td>
            <td width="24%"><select name="product_type_status" class="inpuTxt" id="product_type_status">
                <option value="active" <?php if($product_type_status =="active"){ echo "selected='selected'";}?>>Active</option>
                <option value="inactive" <?php if($product_type_status =="inactive"){ echo "selected='selected'";}?>>Inactive</option>
              </select></td>
          </tr>
          <tr class="text">
            <td width="18%" class="pad"></td>
            <td width="24%"><input name="save"  type="submit" class="inputton" id="save" value="Save"></td>
          </tr>
          <tr class="text">
            <td class="redstar pad" colspan="2"> * Required Fields </td>
          </tr>
        </table></td>
    </tr>
  </table>
</form>