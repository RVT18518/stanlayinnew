<?php 
//this new dynamic module for add mode master added by rumit on dated 27Nov 2020
	$data_action=$_REQUEST['action'];
	$pcode=$_REQUEST["pcode"];
	if($data_action=="edit")
	{
		$rs					= $s->getData_with_condition('tbl_product_type_class_master','product_type_class_id',$pcode);
		$row				= mysqli_fetch_object($rs);
		$product_type_class_name        	= stripslashes($row->product_type_class_name);
		$product_type_class_show       	= stripslashes($row->product_type_class_show);
		//$product_type_class_manager     = stripslashes($row->product_type_class_manager);
		//$sub_product_type_class_lead    = stripslashes($row->sub_product_type_class_lead);
		//$sub_product_type_class_lead2   = stripslashes($row->sub_product_type_class_lead2);
		//$sub_product_type_class_lead3   = stripslashes($row->sub_product_type_class_lead3);
		$product_type_class_status      	= $row->product_type_class_status;
		if($row->product_type_class_description==''){
		$product_type_class_description 	= str_replace(' ', '_',$row->product_type_class_name) ;//stripslashes($row->product_type_class_description);		
		}
		else
		{
		$product_type_class_description 	= str_replace(' ', '_',$row->product_type_class_description) ;//stripslashes($row->product_type_class_description);					
		}
		$data_action = "update";
	}
	if($data_action == "add_new")
	{
		$data_action = "insert";
	}
?>
<form name="frx1" id="frx1" action="index.php?pagename=manage_product_type_class_master&action=<?php echo $data_action;?>&pcode=<?php echo $pcode;?>" method="post" enctype="multipart/form-data">
  <table width="100%" align="center" cellpadding="0" cellspacing="0" class="pagecontent" >
    <tr>
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="38%" class="pageheadTop">Add Product Type Class</td>
            <td width="52%" class="headLink"><ul>
                <li><a href="index.php?pagename=manage_product_type_class_master">Back</a></li>
              </ul></td>
            <td width="10%" align="right"><input name="save"  type="submit" class="inputton" id="save" value="Save"></td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td class="pHeadLine"></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><table width="100%" cellpadding="5" cellspacing="0" class="tblBorder">
          <tr>
            <td class="pagehead" colspan="2"> Product Type</td>
          </tr>
          <tr class="text">
            <td width="18%" class="pad"> Name <span class="redstar">*</span></td>
            <td width="24%"><input name="product_type_class_name" type="text" class="inpuTxt" id="product_type_class_name" value="<?php echo $product_type_class_name;?>" required="required"></td>
          </tr>
          <?php /*?><tr class="text">

	  <td class="pad">Logo</td>

	  <td><input name="product_type_class_logo" type="file"  id="product_type_class_logo" value=""></td>

	</tr><?php */?>
          <tr class="text" style="display:none">
            <td width="18%" valign="top" class="pad"> Permalink</td>
            <td width="24%">
            <input name="product_type_class_description" type="text" class="inpuTxt" id="product_type_class_description" value="<?php echo $product_type_class_description;?>" />
           </td>
          </tr>
          <tr class="text" style="display:nonfe">
            <td width="18%" class="pad">Show on PQV </td>
            <td width="24%">
            <select name="product_type_class_show" class="inpuTxt" id="product_type_class_show">
                <option value="yes" <?php if($product_type_class_show =="yes"){ echo "selected='selected'";}?>>Yes</option>
                <option value="no" <?php if($product_type_class_show =="no"){ echo "selected='selected'";}?>>No</option>
              </select>
            
            </td>
          </tr>
          <tr class="text">
            <td width="18%" class="pad"> Status </td>
            <td width="24%"><select name="product_type_class_status" class="inpuTxt" id="product_type_class_status">
                <option value="active" <?php if($product_type_class_status =="active"){ echo "selected='selected'";}?>>Active</option>
                <option value="inactive" <?php if($product_type_class_status =="inactive"){ echo "selected='selected'";}?>>Inactive</option>
              </select></td>
          </tr>
          <tr class="text">
            <td width="18%" class="pad"></td>
            <td width="24%"><input name="save"  type="submit" class="inputton" id="save" value="Save"></td>
          </tr>
          <tr class="text">
            <td class="redstar pad" colspan="2"> * Required Fields </td>
          </tr>
        </table></td>
    </tr>
  </table>
</form>