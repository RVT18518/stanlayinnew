<?php 
//this new dynamic module for team added by rumit on dated 6Jan 2017
	$data_action=$_REQUEST['action'];
	$pcode=$_REQUEST["pcode"];
	if($data_action=="edit")
	{
		$rs=$s->getData_with_condition('tbl_seller','seller_id',$pcode);
		$row=mysqli_fetch_object($rs);
		$seller_name        		= stripslashes($row->seller_name);

		$seller_company_name        = stripslashes($row->seller_company_name);

		$seller_bank_name        	= stripslashes($row->seller_bank_name);

		$seller_bank_account_number = stripslashes($row->seller_bank_account_number);

		$seller_bank_address        = stripslashes($row->seller_bank_address);

		$seller_rtgs_neft_code      = stripslashes($row->seller_rtgs_neft_code);
		$seller_status      		= $row->seller_status;
		$seller_description 		= stripslashes($row->seller_description);		
		$data_action = "update";
	}
	if($data_action == "add_new")
	{
		$data_action = "insert";
	}

?>

<form name="frx1" id="frx1" action="index.php?pagename=manage_seller&action=<?php echo $data_action;?>&pcode=<?php echo $pcode;?>" method="post" enctype="multipart/form-data">
  <table width="100%" align="center" cellpadding="0" cellspacing="0" class="pagecontent" >
    <tr>
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="38%" class="pageheadTop">Seller Manager</td>
            <td width="52%" class="headLink"><ul>
                <li><a href="index.php?pagename=manage_seller">Back</a></li>
              </ul></td>
            <td width="10%" align="right"><input name="save"  type="submit" class="inputton" id="save" value="Save"></td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td class="pHeadLine"></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><table width="100%" cellpadding="5" cellspacing="0" class="tblBorder">
          <tr>
            <td class="pagehead" colspan="2"> Seller</td>
          </tr>
          <tr class="text">
            <td width="18%" class="pad"> Name <span class="redstar">*</span></td>
            <td width="24%"><input name="seller_name" type="text" class="inpuTxt" id="seller_name" value="<?php echo $seller_name;?>"></td>
          </tr>
          <?php /*?><tr class="text">
	  <td class="pad">Logo</td>
	  <td><input name="seller_logo" type="file"  id="seller_logo" value=""></td>
	</tr><?php */?>
         
          <tr class="text">
            <td width="18%" class="pad">Company Name</td>
            <td width="24%"><input name="seller_company_name" type="text" class="inpuTxt" id="seller_company_name" value="<?php echo $seller_company_name;?>"></td>
          </tr>
          
          
           <tr class="text">

                        <td colspan="1" align="left" valign="top" class="pad head_crm">Bank Account Details <br>

                          </td>

                        <td colspan="2" align="left" valign="top" class="pad head_crm"></td>

                      </tr>
           <tr class="text">
            <td width="18%" class="pad">Bank Name</td>
            <td width="24%"><input name="seller_bank_name" type="text" class="inpuTxt" id="seller_bank_name" value="<?php echo $seller_bank_name;?>"></td>
          </tr>
          
          
           <tr class="text">
            <td width="18%" class="pad">Account No</td>
            <td width="24%"><input name="seller_bank_account_number" type="text" class="inpuTxt" id="seller_bank_account_number" value="<?php echo $seller_bank_account_number;?>"></td>
          </tr>
          
           <tr class="text">
            <td width="18%" valign="top" class="pad"> Bank Address</td>
            <td width="24%"><textarea name="seller_bank_address"  rows="4" class="inpuTxt" id="seller_bank_address" ><?php echo $seller_bank_address;?></textarea></td>
          </tr>
          
          
           <tr class="text">
            <td width="18%" class="pad">RTGS/NEFT IFSC Code</td>
            <td width="24%"><input name="seller_rtgs_neft_code" type="text" class="inpuTxt" id="seller_rtgs_neft_code" value="<?php echo $seller_rtgs_neft_code;?>"></td>
          </tr>
         
          <tr class="text">
            <td width="18%" class="pad"> Status </td>
            <td width="24%"><select name="seller_status" class="inpuTxt" id="seller_status">
                <option value="active" <?php if( $seller_status =="active"){ echo "selected='selected'";}?>>Active</option>
                <option value="inactive" <?php if($seller_status =="inactive"){ echo "selected='selected'";}?>>Inactive</option>
              </select>
              </select></td>
          </tr>
          <tr class="text">
            <td width="18%" class="pad"></td>
            <td width="24%"><input name="save"  type="submit" class="inputton" id="save" value="Save"></td>
          </tr>
          <tr class="text">
            <td class="redstar pad" colspan="2"> * Required Fields </td>
          </tr>
        </table></td>
    </tr>
  </table>
</form>
<script language="JavaScript" type="text/javascript">
 var frmvalidator = new Validator("frx1");
 frmvalidator.addValidation("seller_name","req","Please enter  Name");
 

</script> 
