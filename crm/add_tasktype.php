<?php 
//this new dynamic module for add tasktype master added by rumit on dated 06Oct 2021
		$data_action			= $_REQUEST['action'];
		$pcode					= $_REQUEST["pcode"];
	if($data_action=="edit")
	{
		$rs						= $s->getData_with_condition('tbl_tasktype_master','tasktype_id',$pcode);
		$row					= mysqli_fetch_object($rs);
		$tasktype_name        	= stripslashes($row->tasktype_name);
		$tasktype_abbrv       	= stripslashes($row->tasktype_abbrv);
		//$tasktype_manager     = stripslashes($row->tasktype_manager);
		//$sub_tasktype_lead    = stripslashes($row->sub_tasktype_lead);
		//$sub_tasktype_lead2   = stripslashes($row->sub_tasktype_lead2);
		//$sub_tasktype_lead3   = stripslashes($row->sub_tasktype_lead3);
		$tasktype_status      	= $row->tasktype_status;
		if($row->tasktype_description==''){
		$tasktype_description 	= str_replace(' ', '_',$row->tasktype_name) ;//stripslashes($row->tasktype_description);		
		}
		else
		{
		$tasktype_description 	= str_replace(' ', '_',$row->tasktype_description) ;//stripslashes($row->tasktype_description);					
		}
		$data_action = "update";
	}
	if($data_action == "add_new")
	{
		$data_action = "insert";
	}
?>
<form name="frx1" id="frx1" action="index.php?pagename=manage_tasktype_master&action=<?php echo $data_action;?>&pcode=<?php echo $pcode;?>" method="post" enctype="multipart/form-data">
  <table width="100%" align="center" cellpadding="0" cellspacing="0" class="pagecontent" >
    <tr>
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="38%" class="pageheadTop">Add Task Type</td>
            <td width="52%" class="headLink"><ul>
                <li><a href="index.php?pagename=manage_tasktype_master">Back</a></li>
              </ul></td>
            <td width="10%" align="right"><input name="save"  type="submit" class="inputton" id="save" value="Save"></td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td class="pHeadLine"></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><table width="100%" cellpadding="5" cellspacing="0" class="tblBorder">
          <tr>
            <td class="pagehead" colspan="2"> Task Type</td>
          </tr>
          <tr class="text">
            <td width="18%" class="pad"> Name <span class="redstar">*</span></td>
            <td width="24%"><input name="tasktype_name" type="text" class="inpuTxt" id="tasktype_name" value="<?php echo $tasktype_name;?>" required="required"  pattern="[a-zA-Z0-9]+[a-zA-Z0-9 ]+" /></td>
          </tr>
          <?php /*?><tr class="text">

	  <td class="pad">Logo</td>

	  <td><input name="tasktype_logo" type="file"  id="tasktype_logo" value=""></td>

	</tr><?php */?>
          <tr class="text" style="display:none">
            <td width="18%" valign="top" class="pad"> Description</td>
            <td width="24%">
            <input name="tasktype_description" type="text" class="inpuTxt" id="tasktype_description" value="<?php echo $tasktype_description;?>" />
           </td>
          </tr>
          <tr class="text"  >
            <td width="18%" class="pad">Abbreviation<span class="redstar">*</span> </td>
            <td width="24%"><input name="tasktype_abbrv" type="text" class="inpuTxt" id="tasktype_abbrv" value="<?php echo $tasktype_abbrv;?>"  required="required" /></td>
          </tr>
          <tr class="text">
            <td width="18%" class="pad"> Status </td>
            <td width="24%"><select name="tasktype_status" class="inpuTxt" id="tasktype_status">
                <option value="active" <?php if( $tasktype_status =="active"){ echo "selected='selected'";}?>>Active</option>
                <option value="inactive" <?php if($tasktype_status =="inactive"){ echo "selected='selected'";}?>>Inactive</option>
              </select></td>
          </tr>
          <tr class="text">
            <td width="18%" class="pad"></td>
            <td width="24%"><input name="save"  type="submit" class="inputton" id="save" value="Save"></td>
          </tr>
          <tr class="text">
            <td class="redstar pad" colspan="2">* Required Fields</td>
          </tr>
        </table></td>
    </tr>
  </table>
</form>