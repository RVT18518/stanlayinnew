<?php 

//this new dynamic module for team added by rumit on dated 6Jan 2017

	$data_action=$_REQUEST['action'];

	$pcode=$_REQUEST["pcode"];

	

	if($data_action=="edit")

	{

		$rs=$s->getData_with_condition('tbl_team','team_id',$pcode);

		$row=mysqli_fetch_object($rs);

		$team_name        = stripslashes($row->team_name);
		$team_abbrv        = stripslashes($row->team_abbrv);
		$team_manager        = stripslashes($row->team_manager);
		$sub_team_lead        = stripslashes($row->sub_team_lead);
		$sub_team_lead2        = stripslashes($row->sub_team_lead2);
		$sub_team_lead3        = stripslashes($row->sub_team_lead3);

		$team_status      = $row->team_status;

		$team_description = stripslashes($row->team_description);		

		$data_action = "update";

	}

	if($data_action == "add_new")

	{

		$data_action = "insert";

	}



?>

<form name="frx1" id="frx1" action="index.php?pagename=manage_team&action=<?php echo $data_action;?>&pcode=<?php echo $pcode;?>" method="post" enctype="multipart/form-data">
  <table width="100%" align="center" cellpadding="0" cellspacing="0" class="pagecontent" >
    <tr>
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="38%" class="pageheadTop">Team Manager</td>
            <td width="52%" class="headLink"><ul>
                <li><a href="index.php?pagename=manage_team">Back</a></li>
              </ul></td>
            <td width="10%" align="right"><input name="save"  type="submit" class="inputton" id="save" value="Save"></td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td class="pHeadLine"></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><table width="100%" cellpadding="5" cellspacing="0" class="tblBorder">
          <tr>
            <td class="pagehead" colspan="2"> Team</td>
          </tr>
          <tr class="text">
            <td width="18%" class="pad"> Name <span class="redstar">*</span></td>
            <td width="24%"><input name="team_name" type="text" class="inpuTxt" id="team_name" value="<?php echo $team_name;?>"></td>
          </tr>
          <?php /*?><tr class="text">

	  <td class="pad">Logo</td>

	  <td><input name="team_logo" type="file"  id="team_logo" value=""></td>

	</tr><?php */?>
          <tr class="text">
            <td width="18%" valign="top" class="pad"> Description</td>
            <td width="24%"><textarea name="team_description"  rows="4" class="inpuTxt" id="team_description" ><?php echo $team_description;?></textarea></td>
          </tr>
          <tr class="text">
            <td width="18%" class="pad">Team Abbreviation </td>
            <td width="24%"><input name="team_abbrv" type="text" class="inpuTxt" id="team_abbrv" value="<?php echo $team_abbrv;?>" maxlength="2" /></td>
          </tr>
          <tr class="text">
            <td class="pad">Team Lead<span class="redstar"> *</span></td>
            <td><select name="team_manager" id="team_manager" class="inpuTxt">
                <option value="0" <?php if($team_manager == "0"){ echo "selected='selected'";}?>>Select Team Manager</option>
                <?php //this new dynamic module for team added by rumit on dated 6Jan 2017

	$rs_team = $s->getData_without_condition('tbl_admin','admin_fname','asc');

	if(mysqli_num_rows($rs_team)>0)

	{

		while($row_team = mysqli_fetch_object($rs_team))

		{	

?>
                <option value="<?php echo $row_team->admin_id?>" <?php if($row_team->admin_id == $team_manager ){ echo "selected='selected'"; }?>><?php echo $row_team->admin_fname.''.$row_team->admin_lname;?></option>
                <?php

		}

	}

?>
              </select></td>
          </tr>


          <?php
		  if($sub_team_lead2!='0')
		  {
			  $sub_team_lead2_style="";
			  $sub_team_lead2_style_add_more="none";
		  }
		  else
		  {
			    $sub_team_lead2_style="none";
			  $sub_team_lead2_style_add_more="";
		  }
		  

		  ?>
          
          <?php
		  if($sub_team_lead3!='0')
		  {
			  $sub_team_lead3_style="";
			  $sub_team_lead3_style_add_more="none";
		  }
		  else
		  {
			    $sub_team_lead3_style="none";
				$sub_team_lead3_style_add_more="";
		  }
		  ?>
          <tr class="text">
            <td class="pad">Team Manager 1<span class="redstar"> *</span></td>
            <td><select name="sub_team_lead" id="sub_team_lead" class="inpuTxt">
                <option value="0" <?php if($sub_team_lead == "0"){ echo "selected='selected'";}?>>None</option>
                <?php //this new dynamic module for team added by rumit on dated 6Jan 2017

	$rs_team_lead = $s->getData_without_condition('tbl_admin','admin_fname','asc');

	if(mysqli_num_rows($rs_team_lead)>0)

	{

		while($row_team_lead = mysqli_fetch_object($rs_team_lead))

		{	

?>
                <option value="<?php echo $row_team_lead->admin_id?>" <?php if($row_team_lead->admin_id == $sub_team_lead ){ echo "selected='selected'"; }?>><?php echo $row_team_lead->admin_fname.''.$row_team_lead->admin_lname;?></option>
                <?php

		}

	}

?>
              </select> <a href="#;" onclick="document.getElementById('sub_team_lead2box').style.display=''; document.getElementById('add_more2').style.display='none' " id="add_more2" style=" display:<?php echo $sub_team_lead2_style_add_more;?>">Add More </a></td>
          </tr>
          

          <tr class="text" id="sub_team_lead2box" style="display:<?php echo $sub_team_lead2_style;?>">
            <td class="pad"> Team Manager 2<span class="redstar"> * </span></td>
            <td><select name="sub_team_lead2" id="sub_team_lead2" class="inpuTxt">
                <option value="0" <?php if($sub_team_lead2 == "0"){ echo "selected='selected'";}?>>None</option>
                <?php //this new dynamic module for team added by rumit on dated 6Jan 2017

	$rs_team_lead2 = $s->getData_without_condition('tbl_admin','admin_fname','asc');

	if(mysqli_num_rows($rs_team_lead2)>0)

	{

		while($row_team_lead2 = mysqli_fetch_object($rs_team_lead2))

		{	

?>
                <option value="<?php echo $row_team_lead2->admin_id?>" <?php if($row_team_lead2->admin_id == $sub_team_lead2 ){ echo "selected='selected'"; }?>><?php echo $row_team_lead2->admin_fname.''.$row_team_lead2->admin_lname;?></option>
                <?php

		}

	}

?>
              </select> <a href="#;" onclick="document.getElementById('sub_team_lead3box').style.display=''; document.getElementById('add_more3').style.display='none'" id="add_more3" style=" display:<?php echo $sub_team_lead3_style_add_more;?>">Add More </a></td>
          </tr>
          
          
            
          <tr class="text" id="sub_team_lead3box" style="display:<?php echo $sub_team_lead3_style;?>">
            <td class="pad"> Team Manager 3<span class="redstar"> * </span></td>
            <td><select name="sub_team_lead3" id="sub_team_lead3" class="inpuTxt">
                <option value="0" <?php if($sub_team_lead3 == "0"){ echo "selected='selected'";}?>>None</option>
                <?php //this new dynamic module for team added by rumit on dated 6Jan 2017

	$rs_team_lead3 = $s->getData_without_condition('tbl_admin','admin_fname','asc');

	if(mysqli_num_rows($rs_team_lead3)>0)

	{

		while($row_team_lead3 = mysqli_fetch_object($rs_team_lead3))

		{	

?>
                <option value="<?php echo $row_team_lead3->admin_id?>" <?php if($row_team_lead3->admin_id == $sub_team_lead3 ){ echo "selected='selected'"; }?>><?php echo $row_team_lead3->admin_fname.''.$row_team_lead3->admin_lname;?></option>
                <?php

		}

	}

?>
              </select></td>
          </tr>
          <tr class="text">
            <td width="18%" class="pad"> Status </td>
            <td width="24%"><select name="team_status" class="inpuTxt" id="team_status">
                <option value="active" <?php if( $team_status =="active"){ echo "selected='selected'";}?>>Active</option>
                <option value="inactive" <?php if($team_status =="inactive"){ echo "selected='selected'";}?>>Inactive</option>
              </select>
              </select></td>
          </tr>
          <tr class="text">
            <td width="18%" class="pad"></td>
            <td width="24%"><input name="save"  type="submit" class="inputton" id="save" value="Save"></td>
          </tr>
          <tr class="text">
            <td class="redstar pad" colspan="2"> * Required Fields </td>
          </tr>
        </table></td>
    </tr>
  </table>
</form>
<script language="JavaScript" type="text/javascript">

 var frmvalidator = new Validator("frx1");

 frmvalidator.addValidation("team_name","req","Please enter Team  Name");

 

 function check_team()

   {

	   	var value_role=document.getElementById('admin_role').value;

		if(value_role=="Select Role")

		  {

				alert(value_role);

				return false;

		  }

		

   }

</script> 
