<?php
	$data_action = $_REQUEST["action"];
	$pcode 		 = $_REQUEST["pcode"];
	$pcode_emp	 = $_REQUEST["pcode_emp"];




	if($_REQUEST["action"] == "update" || $_REQUEST["action"] == "insert")
	{
		
		if($_FILES["tnd_SD_Scanned_Copy"]["name"] != "")
			{
				$filePath = $s->fileUpload("uploads/tender/", "tnd_SD_Scanned_Copy", "Tender_");
				if($filePath != -1)
				{
					 $dataArray["tnd_SD_Scanned_Copy"] = $filePath;
					// exit;
				}
			else {
					$msg = 'Pl Check Invoice Name! Invoice Not Uploaded';
			}
			}
			
			
	
		$dataArray["tnd_winning_status"]	= $_REQUEST["tnd_winning_status"];
		$dataArray["tnd_SD_APP"]			= $_REQUEST["tnd_SD_APP"];
        $dataArray["tnd_SD_TYPE"]			= $_REQUEST["tnd_SD_TYPE"];
  		$dataArray["tnd_DS_Date"]			= $_REQUEST["tnd_DS_Date"];
		$dataArray["tnd_SD_Value"]			= $_REQUEST["tnd_SD_Value"];
		$dataArray["tnd_SD_Bank"]			= $_REQUEST["tnd_SD_Bank"];
		$dataArray["tnd_SD_Return"]			= $_REQUEST["tnd_SD_Return"];
		$dataArray["tnd_EMD_SD"]			= $_REQUEST["tnd_EMD_SD"];
		$dataArray["tnd_SD_Details"]		= $_REQUEST["tnd_SD_Details"];
		
	}
	
	if($_REQUEST["action"] == "update")
	{
			$result = $s->editRecord('tbl_tender',$dataArray,'id',$pcode);
			if($_REQUEST["tnd_SD_APP"]!='') {
				$s->pageLocation("index.php?pagename=view_open_tender&action=edit&pcode=$pcode");
			}
			
			$error = 1;
			$data_action = 'edit';
	}
	if($data_action=="edit")
	{
		$rs		  		= $s->getData_with_condition_ten('tbl_tender','ID',$pcode);
		$row	  		= mysqli_fetch_object($rs);
		
		$tnd_winning_status 		= $row->tnd_winning_status;
		$tnd_SD_APP 				= $row->tnd_SD_APP;
		$tnd_SD_TYPE  				= $row->tnd_SD_TYPE;
		$tnd_DS_Date  				= $row->tnd_DS_Date;
		$tnd_SD_Value				= $row->tnd_SD_Value;
		$tnd_SD_Bank				= $row->tnd_SD_Bank;
		$tnd_SD_Return				= $row->tnd_SD_Return;
		$tnd_EMD_SD					= $row->tnd_EMD_SD;
		$tnd_SD_Scanned_Copy		= $row->tnd_SD_Scanned_Copy;
		$tnd_SD_Details				= $row->tnd_SD_Details;

		$data_action 	= "update";
	}
	if($data_action == "add_new")
	{
		$data_action 	= "insert";
	}	
?>
<script language="javascript" src="../js/zone_state.js" type="text/javascript"></script>
<script>
function showHint(str) {
    if (str.length == 0) { 
        document.getElementById("txtHint").innerHTML = "";
        return;
    } else {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("txtHint").innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "getcompanyexits.php?q=" + str, true);
        xmlhttp.send();
    }
}
</script>



<!--<script language="javascript" src="../js/comp_details.js" type="text/javascript"></script>-->
<script type="text/javascript">
	window.onload = function(){
		g_globalObject = new JsDatePick({
			useMode:2,
			target:"date_opened",
			dateFormat:"%Y-%m-%d"
		});
		g_globalObject1 = new JsDatePick({
			useMode:2,
			target:"date_opened1",
			dateFormat:"%m-%d-%Y"
		});
		
		g_globalObject2 = new JsDatePick({
			useMode:2,
			target:"date_opened2",
			dateFormat:"%m-%d-%Y"
		});
		
		
		
	};
</script>

<form name="frx1" id="frx1" action="index.php?pagename=add_tender_emd&action=<?php echo $data_action;?>&pcode=<?php echo $pcode;?>" method="post" enctype="multipart/form-data">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="5">
          <tr>
            <td width="46%" class="pageheadTop">Tendering &raquo; SD</td>
            <td width="47%" class="headLink" align="right"></td>
            <td width="7%" align="right"></td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td class="pHeadLine"></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td style="border:1px solid #CCC"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="">
          <tr>
            <td colspan="5" class="pagehead"><h5>Security Deposit Details (Mandatory) <span class="redstar"> *</span></h5></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr class="text">
            <td align="left" valign="top" class="pad">Tender Status<span class="redstar"> *</span></td>
            <td align="left" valign="top" ><select name="tnd_winning_status" onchange="this.form.submit()">
                <option value="">Select Tender Status</option>
                <option value="Won" <?php if($tnd_winning_status=="Won") { echo "selected"; } ?>>Won</option>
                <option value="Lost" <?php if($tnd_winning_status=="Lost") { echo "selected"; } ?>>Lost</option>
              </select></td>
              
               <td>&nbsp;</td>
               <td align="left" valign="top" class="pad">EMD Received<span class="redstar"> *</span></td>
            <td align="left" valign="top" ><select name="tnd_EMD_SD" required>
            	<option value="">Select</option>
                <option value="Yes" <?php if($tnd_EMD_SD=="Yes") { echo "selected"; } ?>>Yes</option>
                <option value="No" <?php if($tnd_EMD_SD=="No") { echo "selected"; } ?>>No</option>
                <option value="EMD to SD" <?php if($tnd_EMD_SD=="EMD to SD") { echo "selected"; } ?>>EMD to SD</option>
              </select></td>
              
              
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <?php if($tnd_winning_status=="Won") {  ?>
          <tr class="text">
            <td align="left" valign="top" class="pad">SD Applicable<span class="redstar"> *</span></td>
            <td align="left" valign="top" ><select name="tnd_SD_APP">
               <option value="">Select</option>
                <option value="Yes" <?php if($tnd_SD_APP=="Yes") { echo "selected"; } ?>>Yes</option>
                <option value="No" <?php if($tnd_SD_APP=="No") { echo "selected"; } ?>>No</option>
              </select></td>
            <td align="left" valign="top" >&nbsp;</td>
            <td align="left" valign="top"  class="pad">SD (If SD Applicable Yes)<span class="redstar"> *</span></td>
            <td align="left" valign="top"  ><select name="tnd_SD_TYPE">
               
               <option value="">Select</option>
                <option value="DD" <?php if($tnd_SD_TYPE=="DD") { echo "selected"; } ?>>DD</option>
                <option value="FDR" <?php if($tnd_SD_TYPE=="FDR") { echo "selected"; } ?>>FDR</option>
                <option value="BG" <?php if($tnd_SD_TYPE=="BG") { echo "selected"; } ?>>BG</option>
                    <option value="Online" <?php if($tnd_SD_TYPE=="Online") { echo "selected"; } ?>>Online</option>
                <option value="NEFT-RTGS" <?php if($tnd_SD_TYPE=="NEFT-RTGS") { echo "selected"; } ?>>NEFT/RTGS</option>
              </select></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          
           
    
      <tr class="text">
            <td align="left" valign="top"  class="pad">SD Scanned Copy <span class="redstar"> *</span></td>
            <td align="left" valign="top"  ><input type="file" name="tnd_SD_Scanned_Copy"  />
            
            </td>
            
            
                
           <td align="left" valign="top" >&nbsp;</td>
             <td align="left" valign="top" class="pad">SD Details<span class="redstar"> *</span></td>
           <td align="left" valign="top" >
           <textarea name="tnd_SD_Details" style="width:189px; height:43px"  required="required"><?php echo $tnd_SD_Details; ?></textarea>
            </td>
           
          </tr>
          
            <tr>
            <td>&nbsp;</td>
          </tr>
          
          
          <tr class="text">
            <td align="left" valign="top" class="pad">SD Dated<span class="redstar"> *</span></td>
            <td align="left" valign="top" ><input type="text" name="tnd_DS_Date" id="date_opened" class="inpuTxt" style="width:189px; margin-left:0px; margin-right:15px"  required="required" value="<?php echo $tnd_DS_Date; ?>" autocomplete="off" /></td>
            <td align="left" valign="top" >&nbsp;</td>
            <td align="left" valign="top" class="pad">SD Value<span class="redstar"> *</span></td>
            <td align="left" valign="top" ><input type="text" name="tnd_SD_Value" class="inpuTxt" style="width:189px; margin-left:0px; margin-right:15px"  required="required" value="<?php echo $tnd_SD_Value; ?>" autocomplete="off" /></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            
            <td align="left" valign="top" class="pad">Drawn On (Bank)<span class="redstar"> *</span></td>
            <td align="left" valign="top" ><select name="tnd_SD_Bank">
               <option value="">Select</option>
                <option value="PNB Bank" <?php if($tnd_SD_Bank=="PNB Bank") { echo "selected"; } ?>>PNB Bank</option>
                <option value="J&amp;K Bank Ltd" <?php if($tnd_SD_Bank=="J&K Bank Ltd") { echo "selected"; } ?>>J&amp;K Bank Ltd</option>
                <option value="HDFC Bank" <?php if($tnd_SD_Bank=="HDFC Bank") { echo "selected"; } ?>>HDFC Bank</option>
                <option value="Standard Chartered Bank" <?php if($tnd_SD_Bank=="Standard Chartered Bank") { echo "selected"; } ?>>Standard Chartered Bank</option>
                 <option value="SBI Bank" <?php if($tnd_SD_Bank=="SBI Bank") { echo "selected"; } ?>>SBI Bank</option>
              </select></td>
           <td>&nbsp;</td>
            <td align="left" valign="top" class="pad">Return By<span class="redstar"> *</span></td>
            <td align="left" valign="top" ><input type="text" name="tnd_SD_Return" id="date_opened1" class="inpuTxt" style="width:189px; margin-left:0px; margin-right:15px"  required="required" value="<?php echo $tnd_SD_Return; ?>" autocomplete="off" /></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <?php }  ?>
         
          <tr class="text">
            <td class="redstar pad" > * Required Fields </td>
            <td valign="top" colspan="4" align="right" style="padding-right:5.5%"><input name='add2' id='add2' type='submit' class='inputton' value='Save & Finish' style="width:200px" /></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
  </table>
</form>
<script language="javascript" type="text/javascript">
 var frmvalidator = new Validator("frx1");
 //frmvalidator.addValidation("fname","req","Please enter First Name.");
// frmvalidator.addValidation("tele_no","req","Please enter Telephone No.");
 frmvalidator.addValidation("address","req","Please enter address.");
 frmvalidator.addValidation("city","req","Please enter city.");
 frmvalidator.addValidation("country","dontselect=0","Please select country.");
 frmvalidator.addValidation("state","req","Please enter state.");
 //frmvalidator.addValidation("zip","req","Please enter zip/postal code.");
 frmvalidator.addValidation("cust_segment","dontselect=0","Please select Customer Segment.");
 //frmvalidator.addValidation("acc_manager","dontselect=0","Please select Account Manager.");
 //frmvalidator.addValidation("email","req","Please enter email address.");
 //frmvalidator.addValidation("email","email","Please enter valid email address.");
 //frmvalidator.addValidation("password","req","Please enter password.");
 //frmvalidator.addValidation("confirm_pass","req","Please enter confirm password.");
 //frmvalidator.setAddnlValidationFunction("DoCustomValidation");
 /*function DoCustomValidation()
{
  var frm = document.forms["frx1"];
  if(frm.password.value != frm.confirm_pass.value)
  {
    alert('The Password and confirm password does not match!');
    return false;
  }
  else
  {
    return true;
  }
}*/
</script>