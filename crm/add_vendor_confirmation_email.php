<?php 
//this new dynamic module for add mode master added by rumit on dated 3Dec 2020
	$data_action=$_REQUEST['action'];
	$pcode=$_REQUEST["pcode"];
	if($data_action=="edit")
	{
		$rs							= $s->getData_with_condition('tbl_vendor_confirm_mail','vendor_conf_mail_id',$pcode);
		$row						= mysqli_fetch_object($rs);
		$vendor_master_type        	= stripslashes($row->vendor_master_type);
		$vendor_product_type       	= stripslashes($row->vendor_product_type);
		$config_mail_id     		= stripslashes($row->config_mail_id);
		//$sub_hvc_lead    			= stripslashes($row->sub_hvc_lead);
		//$sub_hvc_lead2   = stripslashes($row->sub_hvc_lead2);
		//$sub_hvc_lead3   = stripslashes($row->sub_hvc_lead3);
		$status      				= $row->status;
		
		$data_action = "update";
	}
	if($data_action == "add_new")
	{
		$data_action = "insert";
	}
?>
<form name="frx1" id="frx1" action="index.php?pagename=manage_vendor_confirmation_email&action=<?php echo $data_action;?>&pcode=<?php echo $pcode;?>" method="post" enctype="multipart/form-data">
  <table width="100%" align="center" cellpadding="0" cellspacing="0" class="pagecontent" >
    <tr>
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="38%" class="pageheadTop">Add Vendor Confirmation mail </td>
            <td width="52%" class="headLink"><ul>
                <li><a href="index.php?pagename=manage_vendor_confirmation_email">Back</a></li>
              </ul></td>
            <td width="10%" align="right"><input name="save"  type="submit" class="inputton" id="save" value="Save"></td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td class="pHeadLine"></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><table width="100%" cellpadding="5" cellspacing="0" class="tblBorder">
          <tr>
            <td class="pagehead" colspan="2">Vendor Confirmation Email</td>
          </tr>
          <tr class="text">
            <td width="18%" class="pad"> Master Type<span class="redstar">*</span></td>
            <td width="24%"><select name="vendor_master_type" class="inpuTxt" id="vendor_master_type">
           <option value="master" <?php if($vendor_master_type =="master"){ echo "selected='selected'";}?>>Master</option>
                <option value="product" <?php if($vendor_master_type =="product"){ echo "selected='selected'";}?>>Product</option>
              </select></td>
          </tr>
          <?php /*?><tr class="text">

	  <td class="pad">Logo</td>

	  <td><input name="hvc_logo" type="file"  id="hvc_logo" value=""></td>

	</tr><?php */?>
          <tr class="text" style="display:non">
            <td width="18%" valign="top" class="pad"> Product Type</td>
            <td width="24%">
            <select name="vendor_product_type" class="inpuTxt" id="vendor_product_type">
                <option value="import" <?php if($vendor_product_type =="import"){ echo "selected='selected'";}?>>Import</option>
                <option value="domestic" <?php if($vendor_product_type =="domestic"){ echo "selected='selected'";}?>>Domestic</option>
              </select>
           </td>
          </tr>
          <tr class="text"  >
            <td width="18%" class="pad">Email Id </td>
            <td width="24%"><input name="config_mail_id" type="text" class="inpuTxt" id="config_mail_id" value="<?php echo $config_mail_id;?>"  /></td>
          </tr>
          <tr class="text">
            <td width="18%" class="pad"> Status </td>
            <td width="24%"><select name="status" class="inpuTxt" id="status">
                <option value="active" <?php if($status =="active"){ echo "selected='selected'";}?>>Active</option>
                <option value="inactive" <?php if($status =="inactive"){ echo "selected='selected'";}?>>Inactive</option>
              </select></td>
          </tr>
          <tr class="text">
            <td width="18%" class="pad"></td>
            <td width="24%"><input name="save"  type="submit" class="inputton" id="save" value="Save"></td>
          </tr>
          <tr class="text">
            <td class="redstar pad" colspan="2"> * Required Fields </td>
          </tr>
        </table></td>
    </tr>
  </table>
</form>