<?php 
//this new dynamic module for customer segment added by rumit on dated 24Mar 2020
	$data_action=$_REQUEST['action'];
	$pcode=$_REQUEST["pcode"];
	if($data_action=="edit")
	{
		$rs					  = $s->getData_with_condition('tbl_vendor_payment_terms_master','vendor_payment_terms_id',$pcode);
		$row				  = mysqli_fetch_object($rs);
		$vendor_payment_terms_name         = stripslashes($row->vendor_payment_terms_name);
		$vendor_payment_terms_abbrv        = stripslashes($row->vendor_payment_terms_abbrv);
		$tracking_link        = stripslashes($row->tracking_link);
		//$vendor_payment_terms_manager    = stripslashes($row->vendor_payment_terms_manager);
		//$sub_vendor_payment_terms_lead   = stripslashes($row->sub_vendor_payment_terms_lead);
		//$sub_vendor_payment_terms_lead2  = stripslashes($row->sub_vendor_payment_terms_lead2);
		//$sub_vendor_payment_terms_lead3  = stripslashes($row->sub_vendor_payment_terms_lead3);
		$vendor_payment_terms_status       = $row->vendor_payment_terms_status;
		if($row->vendor_payment_terms_description==''){
		$vendor_payment_terms_description  = str_replace(' ', '_',$row->vendor_payment_terms_name) ;//stripslashes($row->vendor_payment_terms_description);		
		}
		else
		{
		$vendor_payment_terms_description = str_replace(' ', '_',$row->vendor_payment_terms_description) ;//stripslashes($row->vendor_payment_terms_description);					
		}
		$data_action = "update";
	}
	if($data_action == "add_new")
	{
		$data_action = "insert";
	}
?>
<form name="frx1" id="frx1" action="index.php?pagename=manage_vendor_payment_terms&action=<?php echo $data_action;?>&pcode=<?php echo $pcode;?>" method="post" enctype="multipart/form-data">
  <table width="100%" align="center" cellpadding="0" cellspacing="0" class="pagecontent" >
    <tr>
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="38%" class="pageheadTop">Add Vendor payment terms  </td>
            <td width="52%" class="headLink"><ul>
                <li><a href="index.php?pagename=manage_vendor_payment_terms">Back</a></li>
              </ul></td>
            <td width="10%" align="right"><input name="save"  type="submit" class="inputton" id="save" value="Save" /></td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td class="pHeadLine"></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
     <td><table width="100%" cellpadding="5" cellspacing="0" class="tblBorder">
          <tr>
            <td class="pagehead" colspan="2"> Supply Order Payment Terms</td>
          </tr>
          <tr class="text">
            <td width="18%" class="pad"> Name <span class="redstar">*</span></td>
            <td width="24%"><input name="vendor_payment_terms_name" type="text" class="inpuTxt" id="vendor_payment_terms_name" value="<?php echo $vendor_payment_terms_name;?>" required="required" /></td>
          </tr>
          <?php /*?><tr class="text">

	  <td class="pad">Logo</td>

	  <td><input name="vendor_payment_terms_logo" type="file"  id="vendor_payment_terms_logo" value=""></td>

	</tr><?php */?>
          <tr class="text" style="display:non">
            <td width="18%" class="pad">Value </td>
            <td width="24%"><input name="vendor_payment_terms_description" type="text" class="inpuTxt" id="vendor_payment_terms_description" value="<?php echo $vendor_payment_terms_description;?>" maxlength="2" /></td>
          </tr>
          <tr class="text">
            <td width="18%" class="pad"> Status </td>
            <td width="24%"><select name="vendor_payment_terms_status" class="inpuTxt" id="vendor_payment_terms_status">
                <option value="active" <?php if( $vendor_payment_terms_status =="active"){ echo "selected='selected'";}?>>Active</option>
                <option value="inactive" <?php if($vendor_payment_terms_status =="inactive"){ echo "selected='selected'";}?>>Inactive</option>
              </select>
              </select></td>
          </tr>
          <tr class="text">
            <td width="18%" class="pad"></td>
            <td width="24%"><input name="save"  type="submit" class="inputton" id="save" value="Save" /></td>
          </tr>
          <tr class="text">
            <td class="redstar pad" colspan="2"> * Required Fields </td>
          </tr>
        </table></td>
    </tr>
  </table>
</form>