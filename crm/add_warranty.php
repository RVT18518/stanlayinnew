<?php 
//this new dynamic module for add warranty master added by rumit on dated 28Jan 2020
		$data_action			= $_REQUEST['action'];
		$pcode					= $_REQUEST["pcode"];
	if($data_action=="edit")
	{
		$rs						= $s->getData_with_condition('tbl_warranty_master','warranty_id',$pcode);
		$row					= mysqli_fetch_object($rs);
		$warranty_name        	= stripslashes($row->warranty_name);
		$warranty_abbrv       	= stripslashes($row->warranty_abbrv);
		//$warranty_manager     = stripslashes($row->warranty_manager);
		//$sub_warranty_lead    = stripslashes($row->sub_warranty_lead);
		//$sub_warranty_lead2   = stripslashes($row->sub_warranty_lead2);
		//$sub_warranty_lead3   = stripslashes($row->sub_warranty_lead3);
		$warranty_status      	= $row->warranty_status;
		if($row->warranty_description==''){
		$warranty_description 	= str_replace(' ', '_',$row->warranty_name) ;//stripslashes($row->warranty_description);		
		}
		else
		{
		$warranty_description 	= str_replace(' ', '_',$row->warranty_description) ;//stripslashes($row->warranty_description);					
		}
		$data_action = "update";
	}
	if($data_action == "add_new")
	{
		$data_action = "insert";
	}
?>
<form name="frx1" id="frx1" action="index.php?pagename=manage_warranty_master&action=<?php echo $data_action;?>&pcode=<?php echo $pcode;?>" method="post" enctype="multipart/form-data">
  <table width="100%" align="center" cellpadding="0" cellspacing="0" class="pagecontent" >
    <tr>
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="38%" class="pageheadTop">Add Warranty</td>
            <td width="52%" class="headLink"><ul>
                <li><a href="index.php?pagename=manage_warranty_master">Back</a></li>
              </ul></td>
            <td width="10%" align="right"><input name="save"  type="submit" class="inputton" id="save" value="Save"></td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td class="pHeadLine"></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><table width="100%" cellpadding="5" cellspacing="0" class="tblBorder">
          <tr>
            <td class="pagehead" colspan="2"> Warranty</td>
          </tr>
          <tr class="text">
            <td width="18%" class="pad"> Name <span class="redstar">*</span></td>
            <td width="24%"><input name="warranty_name" type="text" class="inpuTxt" id="warranty_name" value="<?php echo $warranty_name;?>" required="required"  pattern="[a-zA-Z0-9]+[a-zA-Z0-9 ]+" /></td>
          </tr>
          <?php /*?><tr class="text">

	  <td class="pad">Logo</td>

	  <td><input name="warranty_logo" type="file"  id="warranty_logo" value=""></td>

	</tr><?php */?>
          <tr class="text" style="display:none">
            <td width="18%" valign="top" class="pad"> Description</td>
            <td width="24%">
            <input name="warranty_description" type="text" class="inpuTxt" id="warranty_description" value="<?php echo $warranty_description;?>" />
           </td>
          </tr>
          <tr class="text" style="display:none">
            <td width="18%" class="pad">Abbreviation </td>
            <td width="24%"><input name="warranty_abbrv" type="text" class="inpuTxt" id="warranty_abbrv" value="<?php echo $warranty_abbrv;?>" maxlength="2" /></td>
          </tr>
          <tr class="text">
            <td width="18%" class="pad"> Status </td>
            <td width="24%"><select name="warranty_status" class="inpuTxt" id="warranty_status">
                <option value="active" <?php if( $warranty_status =="active"){ echo "selected='selected'";}?>>Active</option>
                <option value="inactive" <?php if($warranty_status =="inactive"){ echo "selected='selected'";}?>>Inactive</option>
              </select></td>
          </tr>
          <tr class="text">
            <td width="18%" class="pad"></td>
            <td width="24%"><input name="save"  type="submit" class="inputton" id="save" value="Save"></td>
          </tr>
          <tr class="text">
            <td class="redstar pad" colspan="2">* Required Fields</td>
          </tr>
        </table></td>
    </tr>
  </table>
</form>