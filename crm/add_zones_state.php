<?php 
	$data_action = $_REQUEST["action"];
	$pcode		 = $_REQUEST["pcode"];
	if($_REQUEST["action"] == "edit")
	{
		$rs 		 = $s->getData_with_condition('tbl_zones','zone_id',$pcode);
		$row		 = mysqli_fetch_object($rs);
		$country_id  = $row->zone_country_id;
		$zone_name	 = $row->zone_name;
		$zone_code	 = $row->zone_code;
		$data_action = "update";
	}
	if($_REQUEST["action"] == "add_new")
	{
			$data_action = "insert";
	}
?>

<form name="frx1" id="frx1" action="index.php?pagename=zones_manager&action=<?php echo $data_action;?>&pcode=<?php echo $pcode;?>" method="post">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0" >
          <tr>
            <td width="13%" class="pageheadTop">Zones </td>
            <td width="83%" class="headLink"><ul>
                <li><a href="index.php?pagename=zones_manager">Back</a></li>
              </ul></td>
            <td width="4%" class="headLink"><input  type="submit" name="edit" id="edit" class="inputton" value="Save" /></td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td class="pHeadLine"></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td valign="top" class="pagecontent"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="tblBorder">
          <tr>
            <td colspan="2" ><table width="100%" border="0" cellspacing="0" cellpadding="0" class="tblBorder">
                <tr>
                  <td class="pagehead"> Zone [State] Details </td>
                </tr>
              </table></td>
          </tr>
          <tr class="text">
            <td class="pad" align="left"> Country<span class="redstar"> *</span></td>
            <td align="left"><select name="country" id="country" class="inpuTxt" required >
                <option value="">Select Country</option>
                <?php
	//$sql_country = "select * from tbl_country order by country_name";
	$rs_co		 = $s->getData_without_condition('tbl_country','country_name');//mysqli_query($GLOBALS["___mysqli_ston"],$sql_country);
	if(mysqli_num_rows($rs_co)>0)
	{
		while($row_country=mysqli_fetch_object($rs_co))
		{	
?>
                <option value="<?php echo $row_country->country_id;?>" <?php if($row_country->country_id==$country_id ){ echo "selected='selected'"; }?>><?php echo ucfirst($row_country->country_name);?></option>
                <?php
		}
	}
?>
              </select></td>
          </tr>
          <tr class="text" align="left">
            <td width="13%" class="pad">Zone Name<span class="redstar"> *</span></td>
            <td ><input name="zone_name" id="zone_name" type="text" class="inpuTxt" value="<?php echo $zone_name;?>" required="required" /></td>
          </tr>
          <tr class="text" align="left">
            <td width="13%" class="pad" >Zone Code<span class="redstar"> *</span></td>
            <td width="87%"><input name="zone_code" id="zone_code" type="text" class="inpuTxt" value="<?php echo $zone_code;?>" required="required" /></td>
          </tr>
          <tr class="text" align="left" >
            <td ></td>
            <td><input  type="submit" name="edit" id="edit" class="inputton" value="Save" />
              &nbsp;</td>
          </tr>
          <tr class="text">
            <td class="redstar pad" colspan="2"> * Required Fields </td>
          </tr>
        </table></td>
    </tr>
  </table>
</form>
<!-- This function will validate the form --> 
<script language="JavaScript" type="text/javascript">
	var frmvalidator = new Validator("frx1");
	frmvalidator.addValidation("country","dontselect=0","Please select Country.");
	frmvalidator.addValidation("zone_code","req","Please enter Zone Code");
	frmvalidator.addValidation("zone_name","req","Please enter Zone Name");
</script>