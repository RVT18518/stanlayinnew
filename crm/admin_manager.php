<?php
	$data_action = $_REQUEST["action"];
	$pcode		 = $_REQUEST["pcode"];	
	$order_by    = $_REQUEST["orderby"]; 
	
	if($_REQUEST['action']=='ChangeStatus')
	{
		$rs_status = $s->getData_with_condition('tbl_admin','admin_id',$pcode);
		if(mysqli_num_rows($rs_status)>0)
		{
			$row_status = mysqli_fetch_object($rs_status);
			if($row_status->admin_status == 'active')
			{
				$fileArray["admin_status"] = 'inactive';
			}
			else if($row_status->admin_status == 'inactive')
			{
				$fileArray["admin_status"] = 'active';
			}
			$result      = $s->editRecord('tbl_admin',$fileArray,'admin_id',$pcode);
			$s->pageLocation("index.php?pagename=admin_manager&action=ChangeDone&result=$result"); 
		}
	}
?>

<form name="frx1" id="frx1" method="post" action="#">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="53%" class="pageheadTop">Employee Manager</td>
            <td width="47%" class="headLink"><ul>
                <li><a href="index.php?pagename=add_admin&action=add_new">Add New Employee </a></li>
              </ul></td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td class="pHeadLine"></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><?php 
		if($_REQUEST['action']=='ChangeDone')
		{
			$result = $_REQUEST['result'];
			if($result==0)
			{
				echo "<p class='success'>Status Change Successfully</p><br />";	
			}
			else if($result==1)
			{
				echo "<p class='error'>Status Changing Fails</p><br />";	
			}
		}
		if($data_action=='delete')
		{
			$result = $s->delete_table_withCondition('tbl_admin','admin_id',$pcode);	
			if($result)
			{
				echo "<p class='success'>".record_delete."</p><br />";	
			}
			else 
			{
				echo "<p class='error'>".record_not_delete."</p><br />";	
			}
		}
		if($_REQUEST['action']=='update')
		{
			 if($result==0)
			{
				echo "<p class='success'>".record_update."</p><br />";	
			}
			else if($result==1)
			{
				echo "<p class='error'>".record_not_update."</p><br />";	
			}
		}
	    if($_REQUEST['action']=='insert')
		{
			 if($result==0)
			{
				echo "<p class='success'>".record_added."</p><br />";	
			}
			else if($result==1)
			{
				echo "<p class='error'>".record_not_added."</p><br />";	
			}
			
		}
	?></td>
    </tr>
    <tr>
      <td valign="top" class="pagecontent"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="tblBorder">
          <tr>
            <td class="pagehead">Employee Details</td>
          </tr>
          <tr>
            <td><table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-striped table-hover">
                <?php 
				if($order_by == '')
	{
		$order_by = 'admin_status,admin_fname';	
	}
	
	
	if($_REQUEST["order"] == 'asc')
	{
		$order = 'desc';
	}
	else if($_REQUEST["order"] == 'desc')
	{
		$order = 'asc';
	}
	else
	{
		$order = 'asc';
	}

				
 	$query 	= " deleteflag = 'active' and admin_level <> '2' and admin_role_id <> -2  order by $order_by $order";
	$rs  	= $s->selectWhere('tbl_admin', $query);
	
	
	if(mysqli_num_rows($rs)>0)
	{
?>
                <tr class="head">
                  <th width="6%" align="center" valign="top"  >ID</th>
                  <th width="16%" valign="top" class="pad" ><a href="index.php?pagename=admin_manager&orderby=admin_fname&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=<?php echo $max_results;?>">Employee Name</a></th>
                  <td width="16%" valign="top" class="pad">Employee Role</td>
                  <td width="15%" valign="top" class="pad"><a href="index.php?pagename=admin_manager&orderby=admin_team&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=<?php echo $max_results;?>">Team</a></td>
                  <td width="15%" valign="top" class="pad">Employee Email (Username) </td>
                  <td width="11%" align="center" valign="top">Status</td>
                  <td width="21%" align="center" valign="top">Action</td>
                </tr>
                <?php 
		$i=1;
		$ij=1;
		while($row = mysqli_fetch_object($rs))
		{
?>
                <tr class="text" align="left" >
                  <td width="6%" align="center"><?php echo $ij++;  //$row->admin_id;?></td>
                  <td width="16%" class="pad"><?php echo ucfirst($row->admin_fname)." ".ucfirst($row->admin_lname);?></td>
                  <td width="16%" class="pad"><?php 
			$rs_role  = $s->getData_with_condition('tbl_admin_role_type','admin_role_id',$row->admin_role_id);
			if(mysqli_num_rows($rs_role)>0)
			{
				$row_role = mysqli_fetch_object($rs_role);
				echo  $row_role->admin_role_name;
			}
			else
			{
				echo "In Valid Role";
			}
			
$teamname=$s->teamname($row->admin_team);

if($teamname=='0' || $teamname=='')
{
	$teamname="N/A";
}
else
{
$teamname=$s->teamname($row->admin_team);	
}

/*
if($teamname=='1')
{
	$teamname="Anup Nayyar";
}

if($teamname=='2')
{
	$teamname="Dinesh Chopra";
}

if($teamname=='3')
{
	$teamname="Harvinder Singh";
}

if($teamname=='4')
{
	$teamname="Rajat Nagrath";
}
if($teamname=='5')
{
	$teamname="E";
}*/




?></td>
                  <td width="15%" class="pad"><?php echo  $teamname;?></td>
                  <td width="15%" class="pad"><?php echo  $row->admin_email;?></td>
                  <td width="11%" align="center"><?php 
	if($row->admin_status =="active")
	{
?>
                    <img src="images/green.gif" title="Active" border="0" alt=""  /> &nbsp; &nbsp; <a href="index.php?pagename=admin_manager&action=ChangeStatus&pcode=<?php echo $row->admin_id;?>"><img src="images/red_light.gif" title="Inactive" border="0"  /></a>
                    <?php
	}
	else if($row->admin_status =="inactive")
	{
?>
                    <a href="index.php?pagename=admin_manager&action=ChangeStatus&pcode=<?php echo $row->admin_id;?>"><img src="images/green_light.gif" title="Active" border="0"  /></a> &nbsp; &nbsp; <img src="images/red.gif" title="Inactive" border="0"  />
                    <?php		
	}
?></td>
                  <td width="21%" align="center"><a href="index.php?pagename=add_admin&action=edit&pcode=<?php echo $row->admin_id;?>"> <img src="images/e.gif" title="Edit" border="0"  /></a> &nbsp; &nbsp; <a href="index.php?pagename=admin_manager&action=delete&pcode=<?php echo $row->admin_id;?>" onclick='return del();'> <img src="images/x.gif" title="Delete" border="0"  /></a></td>
                </tr>
                <?php 
		}
	}
	else
	{
?>
                <tr class='text'>
                  <td colspan='7' class='redstar'>&nbsp; No record present in database.</td>
                </tr>
                <?php 
	}
?>
              </table></td>
          </tr>
        </table></td>
    </tr>
  </table>
</form>
