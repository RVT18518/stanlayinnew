<?php
include("my_conn.php"); 
//$connect = mysqli_connect('localhost', 'root', '', 'iclokcom_stanlayall_live');
$query = "SELECT * FROM tbl_admin where admin_status='active' and deleteflag='active' ORDER BY admin_fname asc";
$result = mysqli_query($connect, $query);
?>
<html>  
 <head>  
          <title>Live Table Data Edit Delete using Tabledit Plugin in PHP</title>  
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
          <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>            
    <script src="jquery.tabledit.min.js"></script>
    </head>  
    <body>  
  <div class="container">  
 
            <div class="table-responsive">  
    <h3 align="center">Employee Abbreviation</h3><br />  
    <table id="editable_table" class="table table-bordered table-striped">
     <thead>
      <tr>
       <th>ID</th>
       <th>First Name</th>
       <th>Last Name</th>
       <th>Name Abbreviation</th>
      </tr>
     </thead>
     <tbody>
     <?php
     while($row = mysqli_fetch_array($result))
     {
      echo '
      <tr>
       <td>'.$row["admin_id"].'</td>
       <td>'.$row["admin_fname"].'</td>
       <td>'.$row["admin_lname"].'</td>
	   <td>'.$row["admin_abrv"].'</td>
	   
      </tr>
      ';
     }
     ?>
     </tbody>
    </table>
   </div>  
  </div>  
 </body>  
</html>  
<script>  
$(document).ready(function(){  
//alert();
     $('#editable_table').Tabledit({
      url:'action.php',
      columns:{
       identifier:[0, "admin_id"],
       editable:[[1, 'admin_fname'], [2, 'admin_lname'], [3, 'admin_abrv']]
      },
      restoreButton:false,
      onSuccess:function(data, textStatus, jqXHR)
      {
       if(data.action == 'delete')
       {
        $('#'+data.admin_id).remove();
       }
      }
     });
 
});  
 </script>