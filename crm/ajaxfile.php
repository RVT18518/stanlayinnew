<?php
## Database configuration
include 'config.php';
include("../includes1/function_lib.php"); 
## Read value
$draw = $_POST['draw'];
$row = $_POST['start'];
$rowperpage = $_POST['length']; // Rows display per page
$columnIndex = $_POST['order'][0]['column']; // Column index
if($_POST['columns'][$columnIndex]['data']!='')
{
$columnName = "orders_id";//$_POST['columns'][$columnIndex]['data']; // Column name
}
else
{
$columnName = 	$_POST['columns'][$columnIndex]['data'];
}
$columnSortOrder = "desc";//$_POST['order'][0]['dir']; // asc or desc
$searchValue = $_POST['search']['value']; // Search value

## Search 
$searchQuery = " ";
if($searchValue != ''){
   $searchQuery = " and (customers_name like '%".$searchValue."%' or 
        customers_email like '%".$searchValue."%' or 
        shipping_city like'%".$searchValue."%' ) ";
}

## Total number of records without filtering
$sel = mysqli_query($con,"select count(*) as allcount from tbl_order");
$records = mysqli_fetch_assoc($sel);
$totalRecords = $records['allcount'];

## Total number of record with filtering
$sel = mysqli_query($con,"select count(*) as allcount from tbl_order WHERE 1 ".$searchQuery);
$records = mysqli_fetch_assoc($sel);
$totalRecordwithFilter = $records['allcount'];

## Fetch records
$empQuery = "select * from tbl_order WHERE 1 ".$searchQuery." order by ".$columnName." ".$columnSortOrder." limit ".$row.",".$rowperpage;
$empRecords = mysqli_query($con, $empQuery);
$data = array();

while ($row = mysqli_fetch_assoc($empRecords)) {
   $data[] = array( 
      "sno"=>$i,
	  "orders_id"=>$row['offercode'].'-'.$row['orders_id'],
      "offercode"=>$row['offercode'],
      "customers_id"=>$row['customers_id'],
      "customers_name"=>$row['customers_name'],
	  "account_manager"=>$s->account_manager_name_full($row['order_by']),
      "shipping_city"=>$row['shipping_city']
   );
}

## Response
$response = array(
  "draw" => intval($draw),
  "iTotalRecords" => $totalRecordwithFilter,
  "iTotalDisplayRecords" => $totalRecords,
  "aaData" => $data
);

echo json_encode($response);
?>