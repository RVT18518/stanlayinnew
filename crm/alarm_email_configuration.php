<?php
$maxdays="60";
if($_REQUEST["action"]=='')
{
	$_REQUEST["action"]="edit";
}
 	$data_action = $_REQUEST["action"];//exit;
	$rs 		 = $s->getData_without_condition('tbl_alarm_email_configuration');
	if(mysqli_num_rows($rs)!=0)
	{
		$row	 						= mysqli_fetch_object($rs);
		$pcode	 						= $row->alarm_config_id;
		$financial_year_start			= $row->financial_year_start;
		$alarm1							= $row->alarm_1;
		$alarm2							= $row->alarm_2;
		$alarm3							= $row->alarm_3;
		$alarm_1_additional_email		= $row->alarm_1_additional_email;
		$alarm_2_additional_email		= $row->alarm_2_additional_email;
		$alarm_3_additional_email		= $row->alarm_3_additional_email;
		$alarm_enq_not_assigned			= $row->alarm_enq_not_assigned;
 		$enq_not_assigned_alarm_email	= $row->enq_not_assigned_alarm_email;
		$enq_not_assigned_alarm_email2	= $row->enq_not_assigned_alarm_email2;
		$sales_head_email				= $row->sales_head_email;
	}
	if($_REQUEST['action'] == 'delete_image')
	{
		unlink("../".$row->store_logo);
		$DelLogo["store_logo"] = '';
		$DelLogo["display_title"] = "name";
		$result      = $s->editRecord('tbl_alarm_email_configuration',$DelLogo,'alarm_config_id',$pcode);
		$s->pageLocation("index.php?pagename=alarm_email_configuration&action=edit");
	}
	if($_REQUEST['action'] == 'update' && $_REQUEST["add"]=='Save')
	{
		$fileArray["admin_email"]  					= $_REQUEST["admin_email"];
		$fileArray["financial_year_start"]  		= $_REQUEST["financial_year_start"];
		$fileArray["alarm_1"]  						= $_REQUEST["alarm_1"];
		$fileArray["alarm_2"]  						= $_REQUEST["alarm_2"];
		$fileArray["alarm_3"]  						= $_REQUEST["alarm_3"];
		$fileArray["alarm_1_additional_email"]		= $_REQUEST["alarm_1_additional_email"];
		$fileArray["alarm_2_additional_email"]		= $_REQUEST["alarm_2_additional_email"];
		$fileArray["alarm_3_additional_email"]		= $_REQUEST["alarm_3_additional_email"];		
		$fileArray["alarm_enq_not_assigned"]		= $_REQUEST["alarm_enq_not_assigned"];
		$fileArray["enq_not_assigned_alarm_email"]	= $_REQUEST["enq_not_assigned_alarm_email"];
		$fileArray["enq_not_assigned_alarm_email2"]	= $_REQUEST["enq_not_assigned_alarm_email2"];
		$fileArray["sales_head_email"]				= $_REQUEST["sales_head_email"];
		$fileArray["inventory_report_email"]		= $_REQUEST["inventory_report_email"];
		$fileArray["inventory_report_email2"]		= $_REQUEST["inventory_report_email2"];
		$fileArray["incoming_stock_report_email"]	= $_REQUEST["incoming_stock_report_email"];
		$fileArray["incoming_stock_report_email2"]	= $_REQUEST["incoming_stock_report_email2"];
		$fileArray["outgoing_stock_report_email"]	= $_REQUEST["outgoing_stock_report_email"];
		$fileArray["outgoing_stock_report_email2"]	= $_REQUEST["outgoing_stock_report_email2"];
		//////////////
	}
	if($data_action =='update')
	{
		$result      = $s->editRecord('tbl_alarm_email_configuration',$fileArray,'alarm_config_id',$pcode);
		$s->pageLocation("index.php?pagename=alarm_email_configuration&action=edit&setX=1");
	}
	if($data_action =='edit')
	{
		$rs  = $s->getData_with_condition('tbl_alarm_email_configuration','alarm_config_id',$pcode);
		$row = mysqli_fetch_object($rs);
		$data_action = 'update';
	}
?>

<form name="frx1" id="frx1" action="index.php?pagename=alarm_email_configuration&action=<?php echo $data_action;?>&pcode=<?php echo $pcode;?>" method="post" enctype="multipart/form-data">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="90%" class="pageheadTop"> Enquiry Alarms Email Configuration </td>
            <td width="9%" class="headLink" align="right"><input name="add" type="submit" class="inputton" id="add" value="Save">
              &nbsp; </td>
            <td width="1%"  class="pad" align="right"></td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td class="pHeadLine"></td>
    </tr>
    
    <tr>
      <td valign="top"><?php 
	if($_REQUEST['setX']=='1')
	{
	 	if($result==0)
		{
			echo "<p class='success'>".record_update."</p><br />";	
		}
		else if($result==1)
		{
			echo "<p class='error'>".record_not_update."</p><br />";	
		}
	}
		
?></td>
    </tr>
    <tr>
      <td valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="tblBorder">
                <tr class="pagehead">
                  <td class="pad">Enquiry Alarms Email Configuration</td>
                </tr>
                <tr>
                  <td valign="top" ><table width="100%" border="0" cellpadding="3" cellspacing="0">
                      <tr class="head">
                        <td  class="pad" colspan="2">Email Configuration</td>
                      </tr>
                      <tr class="text">
                        <td align="right"  width="50%"  class="pad"><strong>Admin Email</strong> <span class="redstar">*</span></td>
                        <td width="50%" ><input type="email" name="admin_email" id="admin_email" value="<?php echo $row->admin_email;?>" class="inpuTxt" autocomplete="nopes" /></td>
                      </tr>
                      <tr class="text">
                        <td align="right"  class="pad"><strong>Inventory Report Email</strong> <span class="redstar">*</span></td>
                        <td ><input type="email" name="inventory_report_email" id="inventory_report_email" value="<?php echo $row->inventory_report_email;?>" class="inpuTxt" autocomplete="nopes" /></td>
                      </tr>
                      
                      <tr class="text">
                        <td align="right"  class="pad"><strong>Inventory Report Aditional Person Email</strong> <span class="redstar">*</span></td>
                        <td ><input type="email" name="inventory_report_email2" id="inventory_report_email2" value="<?php echo $row->inventory_report_email2;?>" class="inpuTxt" autocomplete="nopes" /></td>
                      </tr>
                      <tr class="text">
                        <td align="right"  class="pad"><strong>Incoming Stock Report Email</strong> <span class="redstar">*</span></td>
                        <td ><input type="email" name="incoming_stock_report_email" id="incoming_stock_report_email" value="<?php echo $row->incoming_stock_report_email;?>" class="inpuTxt" autocomplete="nopes" /></td>
                      </tr>

                      <tr class="text">
                        <td align="right"  class="pad"><strong>Incoming Stock Report Aditional Person Email</strong> <span class="redstar">*</span></td>
                        <td ><input type="email" name="incoming_stock_report_email2" id="incoming_stock_report_email2" value="<?php echo $row->incoming_stock_report_email2;?>" class="inpuTxt" autocomplete="nopes" /></td>
                      </tr>

                      <tr class="text">
                        <td align="right"  class="pad"><strong>Outgoing Stock Report Email</strong> <span class="redstar">*</span></td>
                        <td ><input type="email" name="outgoing_stock_report_email" id="outgoing_stock_report_email" value="<?php echo $row->outgoing_stock_report_email;?>" class="inpuTxt" autocomplete="nopes" /></td>
                        </tr>
                         <tr class="text">
                        <td align="right"  class="pad"><strong>Outgoing Stock Report Aditional Person Email</strong> <span class="redstar">*</span></td>
                        <td ><input type="email" name="outgoing_stock_report_email2" id="outgoing_stock_report_email2" value="<?php echo $row->outgoing_stock_report_email2;?>" class="inpuTxt" autocomplete="nopes" /></td>
                        </tr>
                      <tr class="head" style="display:non">
                        <td class="pad" colspan="2">Define Financial Quarters</td>
                      </tr>
                      <tr class="text" >
                        <td align="right"  class="pad"><strong>Financial Year Starts from</strong> <span class="redstar">*</span></td>
                        <td ><select name="financial_year_start" id="financial_year_start" class="inpuTxtSelect">
                            <option value="0">Select Year</option>
                            <option value="January" <?php if("January" == $financial_year_start ){ echo "selected='selected'"; }?>>January 1</option>
                            <option value="February"<?php if("February" == $financial_year_start ){ echo "selected='selected'"; }?>>February 1</option>
                            <option value="March"<?php if("March" == $financial_year_start ){ echo "selected='selected'"; }?>>March 1</option>
                            <option value="April"<?php if("April" == $financial_year_start ){ echo "selected='selected'"; }?>>April 1</option>
                            <option value="May"<?php if("May" == $financial_year_start ){ echo "selected='selected'"; }?>>May 1</option>
                            <option value="June"<?php if("June" == $financial_year_start ){ echo "selected='selected'"; }?>>June 1</option>
                            <option value="July"<?php if("July" == $financial_year_start ){ echo "selected='selected'"; }?>>July 1</option>
                            <option value="August"<?php if("August" == $financial_year_start ){ echo "selected='selected'"; }?>>August 1</option>
                            <option value="September"<?php if("September" == $financial_year_start ){ echo "selected='selected'"; }?>>September 1</option>
                            <option value="October"<?php if("October" == $financial_year_start ){ echo "selected='selected'"; }?>>October 1</option>
                            <option value="November"<?php if("November" == $financial_year_start ){ echo "selected='selected'"; }?>>November 1</option>
                            <option value="December"<?php if("December" == $financial_year_start ){ echo "selected='selected'"; }?>>December 1</option>
                          </select>
                          <!--ALTER TABLE `tbl_alarm_email_configuration` ADD `financial_year_start` VARCHAR(12) NOT NULL DEFAULT '0' AFTER `meta_desc`;--></td>
                      </tr>
                      <!--alarms starts-->
                      
                      <tr class="pagehead" >
                        <td  class="pad" colspan="2">Alarms Configuration Outside Sales Executives</td>
                      </tr>
                      <tr class="text" >
                        <td align="right"  class="pad"><strong>Sales Head Email </strong><span class="redstar">*</span></td>
                        <td><input type="email" name="sales_head_email" id="sales_head_email" value="<?php echo stripslashes($sales_head_email);?>" class="inpuTxt" required="required" /></td>
                      </tr>
                      <tr class="pageheadlevel1" >
                        <td colspan="2" align="center"  class="pad ">Ist Level Escalation for Alarm1to sales A/c executive </td>
                      </tr>
                      <tr class="text" >
                        <td align="right"  class="pad"><strong>Alarm 1</strong> <span class="redstar">*</span></td>
                        <td><select name="alarm_1" id="alarm_1" class="inpuTxtSelect">
                            <option value="">Select Alarm Days</option>
                            <?php
                      for ($d=1; $d<=$maxdays; $d++) {?>
                            <option value="<?php echo $d;?>" <?php if($d == $alarm1 ){ echo "selected='selected'"; }?>><?php echo $d;?> days</option>
                            <?php  }?>
                          </select></td>
                      </tr>
                      <tr class="text" >
                        <td align="right"  class="pad"><strong>Alarm 1 Additional Person Email</strong> <span class="redstar">*</span></td>
                        <td><input type="email" name="alarm_1_additional_email" id="alarm_1_additional_email" value="<?php echo stripslashes($alarm_1_additional_email);?>" class="inpuTxt" /></td>
                      </tr>
                      
                      <tr class="pageheadlevel2" >
                        <td colspan="2" align="center"  class="pad ">2nd Level Escalation to A/c executive with copy to Team Leader</td>
                      </tr>
                      <tr class="text" >
                        <td align="right"  class="pad"><strong> Alarm 2 </strong><span class="redstar">*</span></td>
                        <td><select name="alarm_2" id="alarm_2" class="inpuTxtSelect">
                            <option value="">Select Alarm Days</option>
                            <?php
                      for ($d2=1; $d2<=$maxdays; $d2++) {?>
                            <option value="<?php echo $d2;?>" <?php if($d2 == $alarm2 ){ echo "selected='selected'"; }?>><?php echo $d2;?> days</option>
                            <?php  }?>
                          </select></td>
                      </tr>
                      <tr class="text" >
                        <td align="right"  class="pad"><strong>Alarm 2 Additional Person Email</strong> <span class="redstar">*</span></td>
                        <td><input type="email" name="alarm_2_additional_email" id="alarm_2_additional_email" value="<?php echo stripslashes($alarm_2_additional_email);?>" class="inpuTxt" /></td>
                      </tr>
                      
                      <tr class="pageheadlevel3" >
                        <td colspan="2" align="center"  class="pad ">3rd Level Escalation to A/c executive with copy to Team Leader and Sales head</td>
                      </tr>
                      <tr class="text" >
                        <td align="right"  class="pad"><strong>Alarm 3 </strong><span class="redstar">*</span></td>
                        <td><select name="alarm_3" id="alarm_3" class="inpuTxtSelect" required>
                            <option value="">Select Alarm Days</option>
                            <?php
                      for ($d3=1; $d3<=$maxdays; $d3++) {?>
                            <option value="<?php echo $d3;?>" <?php if($d3 == $alarm3 ){ echo "selected='selected'"; }?>><?php echo $d3;?> days</option>
                            <?php  }?>
                          </select></td>
                      </tr>
                      <tr class="text" >
                        <td align="right"  class="pad"><strong>Alarm 3 Additional Person Email</strong> <span class="redstar">*</span></td>
                        <td><input type="email" name="alarm_3_additional_email" id="alarm_3_additional_email" value="<?php echo stripslashes($alarm_3_additional_email);?>" class="inpuTxt" /></td>
                      </tr>
                      
                      
                       <tr class="pagehead" >
                        <td colspan="2" align="center"  class="pad ">Inside Sales Alarm for enquiries not assigned </td>
                      </tr>
                      
                      <tr class="text" >
                        <td align="right"  class="pad"><strong>Enquries not assigned </strong><span class="redstar">*</span></td>
                        <td><select name="alarm_enq_not_assigned" id="alarm_enq_not_assigned" class="inpuTxtSelect" required>
                            <option value="">Select Alarm Days</option>
                            <?php
                      for ($d4=1; $d4<=$maxdays; $d4++) {?>
                            <option value="<?php echo $d4;?>" <?php if($d4 == $alarm_enq_not_assigned){ echo "selected='selected'"; }?>><?php echo $d4;?> days</option>
                            <?php  }?>
                          </select></td>
                      </tr>
                      <tr class="text" >
                        <td align="right"  class="pad"><strong> Default Email for this</strong> <span class="redstar">*</span></td>
                        <td><input type="email" name="enq_not_assigned_alarm_email" id="enq_not_assigned_alarm_email" value="<?php echo stripslashes($enq_not_assigned_alarm_email);?>" class="inpuTxt" autocomplete="nopes" /></td>
                      </tr>


                      <tr class="text" >
                        <td align="right"  class="pad"><strong> Alarm Aditional  Email for this alarm</strong> <span class="redstar">*</span></td>
                        <td><input type="email" name="enq_not_assigned_alarm_email2" id="enq_not_assigned_alarm_email2" value="<?php echo stripslashes($enq_not_assigned_alarm_email2);?>" class="inpuTxt" autocomplete="nopes" /></td>
                      </tr>
                      
                      <!--alarms ends-->
                      <tr class="text">
                        <td class="pad">&nbsp;</td>
                        <td align="left"><input name="add" type="submit" class="inputton" id="add" value="Save"></td>
                      </tr>
                      <tr class="text">
                        <td class="redstar pad" colspan="2"> * Required Fields </td>
                      </tr>
                  </table></td>
                </tr>
              </table></td>
    </tr>
  </table>
</form>