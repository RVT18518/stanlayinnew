<?php
include("../config.php");
include("../../includes1/function_lib.php");
$account_manager=$_SESSION["AdminLoginID_SET"];
/**
 * CSV export
 * @param  array  $records      array of records
 * @param  string $filename     name of the file including .csv
 * @param  array  $headerFields array of column names
 * @param  string $delimiter    set the delimiter
 * @return download             csv file
 */
function export(array $records, string $filename, array $headerFields = [], string $delimiter = ',')
{
    //set headers to download file rather than displayed
    header('Content-Type: text/csv');
    header('Content-Disposition: attachment; filename="'.$filename.'";');
       //open output
    $f = fopen('php://output', 'w');
    
    //check for header fields
    if (!empty($headerFields)) {
        //set column headers
        fputcsv($f, $headerFields, $delimiter);
    }

    foreach($records as $row) {
        //output each row of the data,
        fputcsv($f, array_values($row), $delimiter);
    }
}

//set filename
$filename = 'events.csv';
//set column names
$headerFields = ['ID', 'Task Type', 'Remarks', 'Start', 'End', 'Product Category', 'Account Manager', 'Customer', 'Opportunity Value'];

//create array
$records = [];

//loop through data and add to array
if($_SESSION['AdminLoginID_SET']=='4' || $_SESSION['AdminLoginID_SET']=='32' || $_SESSION['AdminLoginID_SET']=='46' || $_SESSION['AdminLoginID_SET']=='74'  )
{
$result = $db->rows("SELECT * FROM events ORDER BY id");
}
else
{
$result = $db->rows("SELECT * FROM events where account_manager='$account_manager' ORDER BY id");
}
foreach($result as $row) {
    $records[] = [
        $row->id,
        $row->evttxt,
		$row->title,
        date("d-M-Y - H:i:s", strtotime($row->start_event)),
        date("d-M-Y - H:i:s", strtotime($row->end_event)),
        $s->application_name($row->product_category),
        $s->admin_name($row->account_manager),
        $s->customer_name($row->customer),
        $row->opportunity_value		
    ];
}
//send params to csv
export($records, $filename, $headerFields);