<?php
include("../config.php");

if (isset($_POST['id'])) {
    $row = $db->row("SELECT * FROM events where id=?", [$_POST['id']]);
    $data = [
        'id'        => $row->id,
        'title'     => $row->title,
        'start'     => date('d-m-Y H:i:s', strtotime($row->start_event)),
        'end'       => date('d-m-Y H:i:s', strtotime($row->end_event)),
		'color'     => $row->color,
        'textColor' => $row->text_color,

        'product_category'     => $row->product_category,
		'account_manager'     => $row->account_manager,
		'customer'     => $row->customer,
		'evttxt'     => $row->evttxt,
		'lead_type'     => $row->lead_type,	
		'opportunity_value'     => $row->opportunity_value,	
		'status'     => $row->status	
					
    ];

    echo json_encode($data);
}
