<?php
include("../config.php");

if (isset($_POST['title'])) {

    //collect data
    $error      		= null;
    $title      		= $_POST['title'];
    $start      		= $_POST['startDate'];
    $end        		= $_POST['endDate'];
    $color      		= $_POST['color'];
    $text_color 		= $_POST['text_color'];
	$product_category 	= $_POST['product_category'];
	$account_manager 	= $_POST['account_manager'];	
	$customer 			= $_POST['customer'];	
	$evttxt 			= $_POST['evttxt'];	
	$lead_type 			= $_POST['lead_type'];	
	$opportunity_value 	= $_POST['opportunity_value'];	
	$status 			= $_POST['status'];	
	
	

    //validation
    if ($title == '') {
        $error['title'] = 'Title is required';
    }

    if ($start == '') {
        $error['start'] = 'Start date is required';
    }

    if ($end == '') {
        $error['end'] = 'End date is required';
    }

    //if there are no errors, carry on
    if (! isset($error)) {

        //format date
        $start = date('Y-m-d H:i:s', strtotime($start));
        $end = date('Y-m-d H:i:s', strtotime($end));
        
        $data['success'] = true;
        $data['message'] = 'Success!';

        //store
        $insert = [
            'title'       		=> $title,
            'start_event' 		=> $start,
            'end_event'   		=> $end,
            'product_category' 	=> $product_category,
            'account_manager'   => $account_manager,
			'customer'   		=> $customer,
			'evttxt'   			=> $evttxt,
			'lead_type'   		=> $lead_type,
			'opportunity_value' => $opportunity_value,
			'status'   			=> $status,
            'color'       		=> $color,
            'text_color'  		=> $text_color
        ];
        $db->insert('events', $insert);
      
    } else {

        $data['success'] = false;
        $data['errors'] = $error;
    }

    echo json_encode($data);
}
