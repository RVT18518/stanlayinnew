<?php
include("../config.php");

if (isset($_POST['id'])) {

    //collect data
    $error      = null;
    $id         = $_POST['id'];
    $start      = $_POST['start'];
    $end        = $_POST['end'];

    //optional fields
    $title      		= isset($_POST['title']) ? $_POST['title']: '';
    $color      		= isset($_POST['color']) ? $_POST['color']: '';
    $text_color 		= isset($_POST['text_color']) ? $_POST['text_color']: '';
	$product_category 	= isset($_POST['product_category']) ? $_POST['product_category']: '';
	$account_manager 	= isset($_POST['account_manager']) ? $_POST['account_manager']: '';
	$customer 			= isset($_POST['customer']) ? $_POST['customer']: '';
	$evttxt 			= isset($_POST['evttxt']) ? $_POST['evttxt']: '';
	$lead_type 			= isset($_POST['lead_type']) ? $_POST['lead_type']: '';
	$opportunity_value 	= isset($_POST['opportunity_value']) ? $_POST['opportunity_value']: '';
	$status 			= isset($_POST['status']) ? $_POST['status']: '';
	
	
/*if task completed color change to green else Blue  which is default color of tasks starts added by Rumit on dateed 9-dec-2021*/	
	if($status=="Pending")
	{
		$color 		= "#3788d8";
	}
else if($status=="Completed")
	{
		$color 		= "#1eb353";
	}
/*if task completed color change to green else Blue  which is default color of tasks ends*/		
	
    //validation
    if ($start == '') {
        $error['start'] = 'Start date is required';
    }

    if ($end == '') {
        $error['end'] = 'End date is required';
    }

    //if there are no errors, carry on
    if (! isset($error)) {

        //reformat date
        $start = date('Y-m-d H:i:s', strtotime($start));
        $end = date('Y-m-d H:i:s', strtotime($end));
        
        $data['success'] = true;
        $data['message'] = 'Success!';

        //set core update array
        $update = [
            'start_event' => date('Y-m-d H:i:s', strtotime($_POST['start'])),
            'end_event' => date('Y-m-d H:i:s', strtotime($_POST['end']))
        ];

        //check for additional fields, and add to $update array if they exist
        if ($title !='') {
            $update['title'] = $title;
        }

        if ($color !='') {
            $update['color'] = $color;
        }

        if ($text_color !='') {
            $update['text_color'] = $text_color;
        }
		
		
 if ($product_category !='') {
            $update['product_category'] = $product_category;
        }
 if ($account_manager !='') {
            $update['account_manager'] = $account_manager;
        }
 if ($customer !='') {
            $update['customer'] = $customer;
        }
 if ($evttxt !='') {
            $update['evttxt'] = $evttxt;
        }								
		
		
 if ($lead_type !='') {
            $update['lead_type'] = $lead_type;
        }								
 if ($opportunity_value !='') {
            $update['opportunity_value'] = $opportunity_value;
        }
		
 if ($status !='') {
            $update['status'] = $status;
        }								
		

        //set the where condition ie where id = 2
        $where = ['id' => $_POST['id']];

        //update database
        $db->update('events', $update, $where);
      
    } else {

        $data['success'] = false;
        $data['errors'] = $error;
    }

    echo json_encode($data);
}