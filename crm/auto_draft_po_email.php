
<?php
//session_start();
date_default_timezone_set("Asia/Kolkata");
include_once("../includes1/function_lib.php");
$today=date("Y-m-d");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Incoming Stock Report</title>
<style>
body {
    margin: 0;
    font-family: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,"Noto Sans",sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol","Noto Color Emoji";
    font-size: 1rem;
    font-weight: 400;
    line-height: 1.5;
    color: #212529;
    text-align: left;
    background-color: #fff;
}

.text {
	font:inherit;
	height: 28px;

}
.text a {

	text-decoration: none;
}
.text a:hover {
	text-decoration: underline;
}

td.hover_effect table tr td {
	border-bottom: 1px solid #e7e7e7
}
td.hover_effect1 table tr td {
	border-bottom: 1px solid #e7e7e7
}
.tblBorder tbody tr:nth-of-type(odd) {
/*    background-color: rgba(0,0,0,.05);*/
	background-color:#f2f6fe;
}

.head {
	font: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	font-size: 13px;
	font-weight: bold;
	background-color: #F2F2F2;
	/*background:#ffffff;*/
	height: 28px;
}


</style>
</head>
<body style="padding:0px; margin:0px">

<p>Dear Sir/Madam,</p>
<h2 align="center">Draft PO Generated <?php echo date("d F Y ", strtotime($today));?></h2>
<?php
$sql_product_vendor="select vendor_master.ID as VID,vendor_master.purchase_type, vendor_master.C_Name,vendor_master.Contact_1, vendor_master.Number, vendor_master.Email, vendor_po_final.PO_ID, vendor_po_final.Date,  vendor_po_final.Exporter,vendor_po_order.po_type, vendor_po_order.Confirm_Purchase from vendor_master,vendor_po_order,vendor_po_final where vendor_po_order.VPI=vendor_master.ID AND vendor_po_order.ID=vendor_po_final.PO_ID AND vendor_po_order.Confirm_Purchase='inactive' AND vendor_po_order.po_type='1' GROUP by vendor_po_final.PO_ID order by vendor_po_order.ID desc";

		
//	$rs_product_vendor_paging=mysqli_query($GLOBALS["___mysqli_ston"], $sql_product_vendor_paging);
//	$top_record_fpo=mysqli_num_rows($rs_product_vendor_paging);
//echo 	  $sql_product_vendor;
$rs_product_vendor=mysqli_query($GLOBALS["___mysqli_ston"], $sql_product_vendor);
	$i=1;
	if(mysqli_num_rows($rs_product_vendor)>0)
	{


		
		?>
        <table width="100%" border="1" cellpadding="5" cellspacing="0" class="tblBorder" align="center" bordercolor="#f6f6f6">
          <tr class="head">
          
            <td align="left" valign="top" width="10%"  >S. No.</td>
            <td align="left" valign="top" width="10%" >PO ID</td>
            <td align="left" valign="top" >Company Name</td>
            <td align="left" valign="top" width="10%" >Date</td>
            <td align="left" valign="top" width="10%" >PO Total</td>
            <td align="center" valign="top"   width="10%">Po Type</td>
          </tr>
          <?php
		
		while($row_product_vendor=mysqli_fetch_object($rs_product_vendor))
		{	
		  ?>
          <tr >
            
            <td align="left" valign="middle"  ><?php echo $i; ?></td>
            <td align="left" valign="middle"  ><?php echo $row_product_vendor->PO_ID; ?></td>
            <td align="left" valign="middle"  ><?php echo $row_product_vendor->C_Name; ?></td>
            <td align="left" valign="middle"  ><?php echo $s->date_format_india($row_product_vendor->Date); ?></td>
            <td align="left" valign="middle"  ><?php 
					//echo $row_product_vendor->Currency."-";
					echo $s->po_total($row_product_vendor->PO_ID);
				?></td>
            <td align="center" valign="middle"   style="text-transform:capitalize"><?php echo $row_product_vendor->purchase_type; ?></td>
          </tr>
          <?php
		$i++; }
		?>
        <tr><td colspan="5">&nbsp;</td></tr>
         </table>
        <?php
		}
		  ?>
<p>Regards <br />
Team ACL</p>
