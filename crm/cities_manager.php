<?php
	$data_action = $_REQUEST['action'];
	$pcode		 = $_REQUEST["pcode"];
	$StateCode = $_REQUEST["StateCode"];
	$order 		 = 'asc';
	$order_by    = 'city_name' ; 
	if(!isset($_GET['pageno']))
	{ 
    	$page = 1; 
	} 
	else 
	{ 
    	$page = $_GET['pageno']; 
	} 
	if(!isset($_GET['records']))
	{ 
    	$max_results = 100; 
	} 
	else 
	{ 
    	$max_results = $_GET['records']; 
	} 
	$from = (($page * $max_results) - $max_results);  
	
	if($_REQUEST['action']=='update' ||$_REQUEST['action']=='insert')
	{
		$fileArray["state_code"] = $_REQUEST["state"];
		$fileArray["city_name"]  	  = $_REQUEST["city_name"];
		$fileArray["city_code"] 	  = $_REQUEST["city_code"];
	}
	
	if($_REQUEST['action']=='update')
	{
		$result 	 = $s->editRecord('all_cities',$fileArray,'city_id',$pcode);
		$data_action = "updateDone";
		$s->pageLocation("index.php?pagename=cities_manager&action=$data_action&StateCode=$StateCode&pageno=$page&records=$max_results&result=$result"); 
	}
	if($_REQUEST['action']=='insert')
	{
		$result = $s->insertRecord('all_cities',$fileArray);
		$data_action = "insertDone";
		$s->pageLocation("index.php?pagename=cities_manager&action=$data_action&StateCode=$StateCode&pageno=$page&records=$max_results&result=$result"); 
	}
?>
<script type="text/javascript">
function OnSelect() 
{
	window.location = document.frx1.records.value;
}
function OnSelectPages()
{
	window.location = document.frx1.pages_select.value;
}
function StateSelect()
{
	window.location = document.frx1.state.value;
}
</script>

<form name="frx1" id="frx1" action="#" method="post">
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table-responsive">
    <tr>
      <td><table width="100%" border="0" cellspacing="3" cellpadding="0">
          <tr>
            <td width="40%" class="pageheadTop"> Cities Manager </td>
            <td width="60%" class="headLink"><ul>
                <li><a href="index.php?pagename=add_city&action=add_new">Add New City</a></li>
              </ul></td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td class="pHeadLine"></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><?php 
		if($data_action=='delete')
		{
			$result = $s->delete_table_withCondition('all_cities','city_id',$pcode);	
			if($result)
			{
				echo "<p class='success'>".record_delete."</p><br />";	
			}
			else 
			{
				echo "<p class='error'>".record_not_delete."</p><br />";	
			}
		}
		$result = $_REQUEST['result'];
		if($_REQUEST['action']=='updateDone')
		{
			 if($result==0)
			{
				echo "<p class='success'>".record_update."</p><br />";	
			}
			else if($result==1)
			{
				echo "<p class='error'>".record_not_update."</p><br />";	
			}
		}
	    if($_REQUEST['action']=='insertDone')
		{
			 if($result==0)
			{
				echo "<p class='success'>".record_added."</p><br />";	
			}
			else if($result==1)
			{
				echo "<p class='error'>".record_not_added."</p><br />";	
			}
			
		}
	?></td>
    </tr>
    <?php
	$searchRecord = " state_code='$StateCode'";
	$rs 	= $s->getData_withPages('all_cities',$order_by, $order,$from,$max_results,$searchRecord);
	if(mysqli_num_rows($rs)==0 && $page!=1)
	{
		$page=1;
		$s->pageLocation("index.php?pagename=cities_manager&StateCode=$StateCode&orderby=$order_by&order=$order&pageno=$page&records=$max_results"); 
	}
?>
    <tr>
      <td valign="top" class="pagecontent"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="tblBorder table table-striped">
          <tr class="pagehead">
            <td colspan="2" class="pad">Cities Details </td>
            <td width="25%" align="right" nowrap>State :
              <select name="state" id="state" class="inpuTxtSelect" onChange="StateSelect();">
                <option value="0">Please Select State </option>
                <?php
				
		$rs_country = $s->getData_without_condition('tbl_zones');
		if(mysqli_num_rows($rs_country)>0)
		{
			while($row_country = mysqli_fetch_object($rs_country))
			{
?>
                <option <?php if($row_country->zone_id == $StateCode){ echo "selected='selected'";} ?> 
value="index.php?pagename=cities_manager&StateCode=<?php echo $row_country->zone_id;?>&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=<?php echo $max_results;?>"> <?php echo ucfirst($row_country->zone_name);?></option>
                <?php
			}
		}
		
	?>
              </select></td>
            <td width="19%"><div align="right">Records View &nbsp;
                <select name="records" onchange="OnSelect();"  >
                  <option <?php if($max_results==10){ echo "selected='selected'";} ?> 
value="index.php?pagename=cities_manager&StateCode=<?php echo $StateCode;?>&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=10">10</option>
                  <option <?php if($max_results==20){ echo "selected='selected'";} ?>
value="index.php?pagename=cities_manager&StateCode=<?php echo $StateCode;?>&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=20">20</option>
                  <option <?php if($max_results==50){ echo "selected='selected'";} ?>
value="index.php?pagename=cities_manager&StateCode=<?php echo $StateCode;?>&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=50">50</option>
                  <option <?php if($max_results==100){ echo "selected='selected'";} ?>
value="index.php?pagename=cities_manager&StateCode=<?php echo $StateCode;?>&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=100">100</option>
                </select>
              </div></td>
          </tr>
          <?php
	$i=1;
	if(mysqli_num_rows($rs)!=0)
	{		

?>
          <tr class="head">
            <td width="18%"  align="center">ID</td>
            <td width="38%"  class="pad">City Name</td>
            <td  align="center">City ISO Code </td>
            <td  align="center">Action</td>
          </tr>
          <?php while($row = $s->getRecordObj($rs))//changed by rumit 5-july-2019
		//while($row = mysqli_fetch_object($rs))
		{
?>
          <tr class="text" align="left" onmouseover="bgr_color(this, '#EAB9BA')" onmouseout="bgr_color(this, '')">
            <td height="19" align='center' nowrap><?php echo $row->city_id;?></td>
            <td nowrap class="pad"><?php echo $row->city_name;?></td>
            <td align="center" nowrap><?php echo $row->city_code;?></td>
            <td align='center' nowrap><a href='index.php?pagename=add_city&action=edit&pcode=<?php echo $row->city_id;?>' style='cursor:hand'> <img src='images/e.gif' title='Edit' border='0'  /></a> &nbsp; <a href='index.php?pagename=cities_manager&action=delete&StateCode=&pcode=<?php echo $row->city_id;?>' style='cursor:hand' onclick="return del();"> <img src='images/x.gif' title='Delete'border='0'  /></a></td>
          </tr>
          <?php  
		}
?>
          <tr class='head headLink'>
            <td colspan="4" align="right" nowrap="nowrap"><table width="100%">
                <tr>
                  <td width="92%"  nowrap="nowrap"><div align="right">
                      <?php						
		 	$total_pages = $s->getTotal_pages('all_cities',$order_by, $order,$max_results,$searchRecord);
			if($page > 1)
			{ 
				$prev = ($page - 1); 
			echo "<ul><li><a href='index.php?pagename=cities_manager&StateCode=$StateCode&orderby=$order_by&order=$order&pageno=$prev&records=$max_results'>< Previous</a></li></ul>"; 
			} 
?>
                    </div></td>
                  <td width="5%" align="center"  nowrap="nowrap"><select name="pages_select" onchange="OnSelectPages();">
                      <?php
			for($i = 1; $i <= $total_pages; $i++)
			{ 
?>
                      <option <?php if($page==$i){ echo "selected='selected'";} ?> 
value="index.php?pagename=cities_manager&StateCode=<?php echo $StateCode;?>&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $i;?>&records=<?php echo $max_results;?>"> <?php echo $i;?></option>
                      <?php
			} 
?>
                    </select></td>
                  <td width="3%"  nowrap="nowrap" ><div align="left">
                      <?php		
			if($page < $total_pages)
			{ 
				$next = ($page + 1); 
				echo "<ul><li><a href='index.php?pagename=cities_manager&StateCode=$StateCode&orderby=$order_by&order=$order&pageno=$next&records=$max_results'>Next ></a></li></ul>";
			} 
			 
		?>
                    </div></td>
                </tr>
              </table></td>
          </tr>
          <?php 
}
else if(!isset($_REQUEST["StateCode"]))
{	
?>
          <tr class='text' height="20">
            <td colspan="4"  class='redstar'><b>Please Select State First.</b></td>
          </tr>
          <?php
}
else if(isset($_REQUEST["StateCode"]))
{	
?>
          <tr class='text'>
            <td colspan="4"  class='redstar'>&nbsp; No record present in database.</td>
          </tr>
          <?php
}
?>
        </table></td>
    </tr>
  </table>
</form>
