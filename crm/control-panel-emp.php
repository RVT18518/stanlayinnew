<table width="100%" border="0" cellspacing="7" cellpadding="0" class="pagecontent ">
  <tr>
    <td width="100%" colspan="4"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="40%" class="pageheadTop">Control Panel</td>
          <td width="60%" class="headLink">&nbsp;</td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td class="pHeadLine" colspan="4"></td>
  </tr>
  <tr>
    <td align="left" colspan="4" class="pagehead">Control Panel</td>
  </tr>
  <tr>
    <td colspan="4"><table width="100%" border="0" cellspacing="0" cellpadding="5" class="tblBorder">
        <tr class="head">
          <td colspan="2" align="left" valign="middle" class="pad" >General Configuration</td>
          <td colspan="2" class="pad">Company &amp; Employee Managment</td>
        </tr>
        <tr>
          <td align="center" valign="top" width="13%" class="bdr_bottom "><img width="50" alt=" " src="images/icons/home-icon.gif" /></td>
          <td align="left" valign="top" width="35%" class="bdr_bottom"><strong>
            <h2><a href="index.php?pagename=Dashboard">DashBoard</a></h2>
            </strong></td>
          <td align="center" valign="top" width="15%" class="bdr_bottom bdr_left"><img width="50" alt="" src="images/icons/management.png" /></td>
          <td align="left" valign="top" width="37%" class="bdr_bottom"><strong>
            <h2><a href="index.php?pagename=company_manager">Create New Company</a></h2>
            </strong></td>
        </tr>
        <tr>
          <td align="center" valign="top" class="bdr_bottom "><img width="50" alt=" " src="images/icons/offer.png" /></td>
          <td align="left" valign="top" class="bdr_bottom bdr_right"><strong>
            <h2><a href="index.php?pagename=general_configuration&action=edit">Create New Offer</a></h2>
            </strong></td>
          <td align="center" valign="top" class="bdr_bottom"><img width="50" alt=" " src="images/icons/leadgen-icon.png" /></td>
          <td align="left" valign="top" class="bdr_bottom"><strong>
            <h2><a href="index.php?pagename=lead_manager">Create New Lead</a></h2>
            </strong></td>
        </tr>
        <tr>
          <td align="center" valign="top" class="bdr_bottom"><img width="50" alt=" " src="images/icons/seo.jpg" /></td>
          <td align="left" valign="top" class="bdr_bottom bdr_right"><strong>
            <h2><a href="index.php?pagename=manager_product_meta">Product SEO Manager</a></h2>
            </strong></td>
         <td align="center" valign="top" class="bdr_bottom "><img width="50" alt=" " src="images/icons/display_order.jpg" /></td>
          <td align="left" valign="top" class="bdr_bottom " ><strong>
            <h2><a href="index.php?pagename=manager_product_sort">Product Display Manager</a></h2>
            </strong></td>
            
        </tr>
        <tr  class="display_none">
          <td align="center" valign="top" class="bdr_bottom"><img width="60" alt=" " src="images/icons/news-icon.jpg" /></td>
          <td align="left" valign="top" class="bdr_bottom"><strong>
            <h2><a href="index.php?pagename=manage_news_event">News and Events</a></h2>
            </strong></td>
           <td align="center" valign="top" class="bdr_bottom"><img width="50" alt=" " src="images/icons/designation_manager.jpg" /></td>
          <td align="left" valign="top" class="bdr_bottom"><strong>
            <h2><a href="index.php?pagename=manage_designation">New Designation</a></h2>
            </strong></td>
        </tr>
        <tr class="head">
          <td colspan="2" align="left" valign="middle" class="pad bdr_bottom" >Tools</td>
          <td colspan="2" align="left" valign="middle" class="pad bdr_bottom" >Catalog Manager</td>
        </tr>
        <tr>
          <td width="13%" align="center" valign="top" class="bdr_bottom "><img width="50" alt=" " src="images/home.png" /></td>
          <td width="35%" align="left" valign="top" class="bdr_bottom bdr_right"><strong>
            <h2><a href="index.php?pagename=manage_application">Application Manager</a></h2>
            </strong></td>
          <td width="15%" align="center" valign="top" class="bdr_bottom "><img width="50" alt=" " src="images/icons/pro_cat.jpg" /></td>
          <td width="37%" align="left" valign="top" class="bdr_bottom "><strong>
            <h2><a href="index.php?pagename=manage_product">Product Managment : Website</a></h2>
            </strong></td>
        </tr>
        <tr>
          <td width="13%" align="center" valign="top" class="bdr_bottom "><img width="50" alt=" " src="images/icons/icn_cms.gif" /></td>
          <td width="35%" align="left" valign="top" class="bdr_bottom bdr_right"><strong>
            <h2><a href="index.php?pagename=cms_manager">CMS</a></h2>
            </strong></td>
          <td width="15%" align="center" valign="top" class="bdr_bottom "><img width="50" alt=" " src="images/icons/pro_cat.jpg" /></td>
          <td width="37%" align="left" valign="top" class="bdr_bottom"><strong>
            <h2><a href="index.php?pagename=manage_category">Category Managment</a></h2>
            </strong></td>
        </tr>
        <tr>
          <td width="13%" align="center" valign="top" class="bdr_bottom "><img width="50" alt=" " src="images/icons/dbbkp.jpg" /></td>
          <td width="35%" align="left" valign="top" class="bdr_bottom bdr_right"><strong>
            <h2><a href="index.php?pagename=database_backup">Database Backup</a></h2>
            </strong></td>
          <td width="15%" align="center" valign="top" class="bdr_bottom "><img width="50" alt=" " src="images/icons/pro_cat.jpg" /></td>
          <td width="37%" align="left" valign="top" class="bdr_bottom "><strong>
            <h2><a href="index.php?pagename=manage_product_entry">Product Entry : CRM</a></h2>
            </strong></td>
        </tr>
        <tr>
          <td width="13%" align="center" valign="top"><img width="50" alt=" " src="images/icons/server.jpg" /></td>
          <td width="35%" align="left" valign="top" class="bdr_bottom bdr_right"><strong>
            <h2><a href="index.php?pagename=serverinfo">Server Info</a></h2>
            </strong></td>
          <td width="15%" align="center" valign="top" class="bdr_bottom "><img width="50" alt=" " src="images/icons/stock_mgmt.jpg" /></td>
          <td width="37%" align="left" valign="top" class="bdr_bottom "><strong>
            <h2><a href="index.php?pagename=manager_product_meta">Stock Managment</a></h2>
            </strong></td>
        </tr>
        <tr class="head display_none">
          <td colspan="2" align="left" valign="middle" class="pad" >Offer Management</td>
          <td colspan="2" class="pad">Lead Managment</td>
        </tr>
        <tr class="display_none">
          <td width="13%" align="center" valign="top" class="bdr_bottom "><img width="50" alt=" " src="images/icons/offer.png" /></td>
          <td width="35%" align="left" valign="top" class="bdr_bottom bdr_right " ><strong>
            <h2><a href="index.php?pagename=order_manager">View Offer</a></h2>
            </strong></td>
          <td width="15%" align="center" valign="top" class="bdr_bottom "><img width="50" alt=" " src="images/icons/leadgen-icon.png" /></td>
          <td width="37%" align="left" valign="top" class="bdr_bottom "><strong>
            <h2><a href="index.php?pagename=lead_manager">Lead / Offer Managment</a></h2>
            </strong></td>
        </tr>
        <tr class="head">
          <td colspan="2" align="left" valign="middle" class="pad" >My Account</td>
          <td colspan="2" class="pad">My Attendance</td>
        </tr>
        <tr>
          <td width="13%" align="center" valign="top" class="bdr_bottom  "><img width="50" alt=" " src="images/icons/passwe.jpg" /></td>
          <td width="35%" align="left" valign="top" class="bdr_bottom bdr_right"><strong>
            <h2><a href="index.php?pagename=my_details_change&action=edit">My Account Details</a></h2>
            </strong></td>
          <td width="15%" align="center" valign="top" class="bdr_bottom "><img width="50" alt=" " src="images/icons/attendance.jpg" /></td>
          <td width="37%" align="left" valign="top" class="bdr_bottom "><strong>
            <h2><a href="index.php?pagename=view_attendance">My Attendance</a></h2>
            </strong></td>
        </tr>
        <tr>
          <td width="13%" align="center" valign="top"><img width="50" alt=" " src="images/icons/pass.png" /></td>
          <td width="35%" align="left" valign="top"><strong>
            <h2><a href="index.php?pagename=change_password">Change Password</a></h2>
            </strong></td>
          <td width="15%" align="center" valign="top">&nbsp;</td>
          <td width="37%" align="left" valign="top">&nbsp;</td>
        </tr>
        <tr class="head">
          <td align="center" valign="top">&nbsp;</td>
          <td align="left" valign="top">&nbsp;</td>
          <td align="center" valign="top">&nbsp;</td>
          <td align="left" valign="top">&nbsp;</td>
        </tr>
      </table></td>
  </tr>
  <tr >
    <td colspan="4" align="right" valign="top">&nbsp;</td>
  </tr>
</table>
