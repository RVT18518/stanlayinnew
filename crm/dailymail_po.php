<?php include_once("email-all-main.php");
	
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>ACL Daily follow ups</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<style type="text/css">
td.hover_effect table tr td {
	border-bottom: 1px solid #e7e7e7
}
td.hover_effect1 table tr td {
	border-bottom: 1px solid #e7e7e7
}
td.hover_effect table tr:hover {
	background-color: #ddfdd1
}
td.hover_effect1 table tr:hover {
	background-color: #fdddd1
}
</style>
</head>

<body>
<h4>Good morning Finance Team,</h4>
<?php $target_date=date('m-d-Y'); ?>
<table width='100%' border='1' cellpadding='4' cellspacing='2' class='tblBorder' align="center">
  <tr class='pagehead'>
    <td colspan="7" valign="top" class='pad'><h3>Your vendors due for payment today!</h3></td>
  </tr>
    <?php
 	$sql_invoice_vendor="select * from vendor_po_invoice where status='1' and Due_on<='$target_date' order by po_id desc";
	$rs_invoice_vendor=mysqli_query($GLOBALS["___mysqli_ston"],$sql_invoice_vendor);
	$i=1;
	if(mysqli_num_rows($rs_invoice_vendor)>0)
	{
		?>
          <tr class="head">
            <th align="left" valign="top"  class="pad">S. No.</th>
            <th align="left" valign="top"  class="pad">PO ID</th>
            <th align="left" valign="top"  class="pad">Company Name</th>
            <th align="left" valign="top"  class="pad">Value Total</th>
            
            
            <th align="left" valign="top"  class="pad">Invoice No.</th>
            <th align="left" valign="top"  class="pad">Invoice Date</th>
            <th align="left" valign="top"  class="pad">Due On</th>
          </tr>
          
        <?php
		
		while($row_invoice_vendor=mysqli_fetch_object($rs_invoice_vendor))
		{	
		  ?>

          <tr class="text" bgcolor="#660099" onmouseout="bgr_color(this, '')" onmouseover="bgr_color(this, '#EAB9BA')">
            <td align="left" valign="top"  class="pad"><?php echo $i; ?></td>
            <td align="left" valign="top"  class="pad"><?php echo $row_invoice_vendor->po_id; ?></td>
            <td align="left" valign="top"  class="pad"><?php echo company_name($row_invoice_vendor->v_ID) ?></td>
            <td align="left" valign="top"  class="pad"><?php echo $row_invoice_vendor->value; ?></td>
            <td align="left" valign="top"  class="pad"><?php echo $row_invoice_vendor->I_No; ?></td>
            <td align="left" valign="top"  class="pad"><?php echo $row_invoice_vendor->I_Date; ?></td>	
            <td align="left" valign="top"  class="pad"><?php echo $row_invoice_vendor->Due_on; ?></td>
        
          </tr>
          <?php } ?>
          
             <?php
		$i++; }
	
		  ?>
</table>
<h4>Team ACL</h4>

</body>
</html>
