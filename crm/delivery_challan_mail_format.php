<?php //include_once("email-all-main.php");
	include_once("../includes1/function_lib.php");
	/*******courier co name************/
function couriercoName($STvalue)
{
if(is_numeric($STvalue))
{
$sqlcourier = "select courier_name from tbl_courier_master where courier_id = '$STvalue' and deleteflag = 'active'";
$rscourier  = mysqli_query($GLOBALS["___mysqli_ston"],  $sqlcourier);
$rowcourier = mysqli_fetch_object($rscourier);
$courier	  = $rowcourier->courier_name;
}
else
{
$courier = $STvalue;
}
return ucfirst($courier);
}



function couriercoNameTrackingLink($STvalue)
{
if(is_numeric($STvalue))
{
$sqlcourier = "select tracking_link from tbl_courier_master where courier_id = '$STvalue' and deleteflag = 'active'";
$rscourier  = mysqli_query($GLOBALS["___mysqli_ston"],  $sqlcourier);
$rowcourier = mysqli_fetch_object($rscourier);
$tracking_link	  = $rowcourier->tracking_link;
}
else
{
$tracking_link = $STvalue;
}
return $tracking_link;
}

// function for date format global added  by rumit 25-08-2019 
function date_format_india_with_time($date)
{
return  $date_formate_with_time=date("d/M/Y - H:i:s", strtotime($date));
}

function date_format_india($date)
{
return $date_formate=date("d/M/Y", strtotime($date));
}


$tracking_link=couriercoNameTrackingLink($_REQUEST["Carrier_Details"]);
	/*
	echo "<pre>";
	print_r($_REQUEST);
	
	exit;*/
?>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<title>Dispatch details</title>
<link href="css/style.css" rel="stylesheet" type="text/css">
<style type="text/css">
body {
	font-family:Arial, Helvetica, sans-serif;
	font-size:12px;
}
td {
	border-bottom: 0px solid #f6f6f6;
	border-left: 0px solid #f6f6f6;
}
table {
	border-right: 0px solid #f6f6f6;
	border-top: 0px solid #f6f6f6;
}
table.bor_opt tr td {
	color:#000;
	border:none;
	padding:3px;
	line-height:15px;
	margin:0px;
}
table tr td {
	vertical-align:middle!important;
}
</style>
</head>
<body>
<p>Dear <strong> Sir/Madam
  <?php //echo $_REQUEST["Buyer_Contact_Person"];?>
  </strong>,</p>
<p>Greetings of the day !</p>
<p>We are pleased to hereby confirm that material against your order has been shipped details as below:<br>
</p>
<table width="50%" border="0" cellpadding="4" cellspacing="0">
  <tr>
    <td width="45%" align="left"><strong>PO #: </strong></td>
    <td width="65%" align="left"><?php echo  $_REQUEST["PO_No"];?></td>
  </tr>
  <tr>
    <td align="left"><strong>Invoice #: </strong></td>
    <td align="left"><?php echo $_REQUEST["Invoice_No"];?></td>
  </tr>
  <tr>
    <td align="left"><strong>Vide Courier: </strong></td>
    <td align="left"><?php echo couriercoName($_REQUEST["Carrier_Details"]);?></td>
  </tr>
  <tr>
    <td align="left"><strong>Tracking #: </strong></td>
    <td align="left"><?php echo $_REQUEST["awb_no"];?></td>
  </tr>
</table>
<?php 
//echo "link: ".$tracking_link;
if($tracking_link!='' && $tracking_link!='0')
{?>
<p>You can track your consignment status at <a href="<?php echo $tracking_link;?>" target="_blank" style="color:#0000FF">Track your shipment</a> </p>
<?php }?>
<p>We request you to :- </p>
<p>Acknowledge upon receipt of consignment.<br>
  Original documents are usually enclosed with shipment inside box. In case, an additional original set of documents are required, please email <a href="mailto:cord@stanlay.com" style="color:#0000FF">cord@stanlay.com</a>.</p>
<p>Should you need any further assistance, please feel free to contact us. We look forward to being of assistance. </p>
<p>&nbsp;</p>
<p><a href=""><img src="https://www.stanlay.in/images/logo.png"  border="0" height="30" alt="Stanlay" title="Stanlay" /></a>
<p class="text"><strong>Thanking You</strong><br>
  <strong>ACL Stanlay Team</strong><br>
  <strong>Tel</strong>: 011-41860000<br>
  <strong>Helpline</strong>: 011-41406926<br>
  <strong>Web</strong> : <a href="https://www.stanlay.in" title="Stanlay" target="_blank" style="color:#0000FF">www.stanlay.in</a>, <a href="https://www.stanlay.com" title="Stanlay" target="_blank" style="color:#0000FF">www.stanlay.com</a><br>
  <strong>An ISO 9001 : 2015 Company</strong><br>
  ACL Stanlay is India's largest engineering test & measurement equipment supplier</p>
<p>&nbsp;</p>
</body>
</html>