<!-- Datatable CSS -->
<link href='//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css' rel='stylesheet' type='text/css'>

<!-- jQuery Library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!-- Datatable JS -->
<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>


<!-- Table -->
<table id='empTable' class='display dataTable'>

  <thead>
    <tr>
    <th>S no.</th>
      <th>Date</th>
      <th>UID</th>
      <th>Delivery Order #</th>
      <th>Account Manager</th>
      <th>View PO</th>
      <th>Days Since Do recd.</th>
      <th>Serial no. allocated</th>
      <th>Generate delivery challan</th>
      <th>Reason for delay</th>
    </tr>
  </thead>

</table>



<script>
$(document).ready(function(){
//	alert('ajax');
   $('#empTable').DataTable({
      'processing': true,
      'serverSide': true,
      'serverMethod': 'post',
      'ajax': {
          'url':'ajaxfile.php'
      },
      'columns': [
         { data: 'sno' },
         { data: 'offercode' },
         { data: 'customers_id' },
         { data: 'customers_name' },
         { data: 'account_manager' },
         { data: 'orders_id' },
         { data: 'offercode' },
         { data: 'customers_id' },
         { data: 'customers_name' },
         { data: 'shipping_city' },		 
      ]
   });
});
</script>