<?php include("../../includes1/function_lib.php"); ?>
<?php
/* Database connection start */
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "iclokcom_stanlayall_live";

$conn = mysqli_connect($servername, $username, $password, $dbname) or die("Connection failed: " . mysqli_connect_error());

/* Database connection end */


// storing  request (ie, get/post) global array to a variable  
$requestData= $_REQUEST;


$columns = array( 
// datatable column index  => database column name
	0 =>'orders_id', 
	1 =>'orders_status', 
	2 =>'date_ordered', 
	3 =>'total_order_cost', 
	4 =>'lead_id',
	5 =>'cust_segment',  
	6 =>'ref_source',
	7 =>'app_cat_id',  
	8 =>'admin_fname', 
	9 => 'admin_lname',
	10=> 'admin_email',
	11 => 'admin_abrv'
);




// getting total number records without any search
//$sql = "SELECT admin_fname, admin_lname, admin_email,admin_abrv ";
//$sql.=" FROM tbl_admin";
$sql="SELECT o.follow_up_date,o.orders_id,o.orders_status,o.date_ordered,o.total_order_cost, o.customers_id,o.order_by,o.lead_id,o.offer_probability,a.admin_id,a.admin_fname,a.admin_lname,a.admin_team,l.acc_manager, l.id, l.cust_segment,l.ref_source,l.app_cat_id from tbl_order o , tbl_admin a, tbl_lead l where o.order_by=a.admin_id and l.id=o.lead_id and (`orders_status` IN ('confirmed','Order Closed')) group by o.orders_id desc ";
$query=mysqli_query($conn, $sql) or die("employee-grid-data.php: tbl_admin");
$totalData = mysqli_num_rows($query);
$totalFiltered = $totalData;  // when there is no search parameter then total number rows = total number filtered rows.




/*$sql = "SELECT admin_fname, admin_lname, admin_email,admin_abrv  ";
$sql.=" FROM tbl_admin WHERE 1 = 1";*/
$sql="SELECT o.follow_up_date,o.orders_id,o.orders_status,o.date_ordered,o.total_order_cost, o.customers_id,o.order_by,o.lead_id,o.offer_probability,a.admin_id,a.admin_fname,a.admin_lname,a.admin_team,l.acc_manager, l.id, l.cust_segment,l.ref_source,l.app_cat_id from tbl_order o , tbl_admin a, tbl_lead l where o.order_by=a.admin_id and l.id=o.lead_id and (`orders_status` IN ('confirmed','Order Closed'))  ";
// getting records as per search parameters
if( !empty($requestData['columns'][0]['search']['value']) ){   //name
	$sql.=" AND orders_id LIKE '".$requestData['columns'][0]['search']['value']."%' ";
}
if( !empty($requestData['columns'][1]['search']['value']) ){  //salary
	$sql.=" AND o.orders_status LIKE '".$requestData['columns'][1]['search']['value']."%' ";
}
if( !empty($requestData['columns'][2]['search']['value']) ){ //age
	$sql.="AND  l.ref_source LIKE ".$requestData['columns'][2]['search']['value']."%' ";
//	$sql.=" AND ( employee_age >= '".$minRange."' AND  employee_age <= '".$maxRange."' ) ";
}
$query=mysqli_query($conn, $sql);// or die("employee-grid-data.php: get tbl_admin");
$totalFiltered = mysqli_num_rows($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.
	
$sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."   LIMIT ".$requestData['start']." ,".$requestData['length']."   ";  // adding length

$query=mysqli_query($conn, $sql) or die("employee-grid-data.php: get tbl_admin");

	


$data = array();
while( $row=mysqli_fetch_array($query) ) {  // preparing an array
	$nestedData=array(); 
	$nestedData[] = $row['orders_id']; 
	$nestedData[] = $row['orders_status'];  
	$nestedData[] = $row['date_ordered']; 
	$nestedData[] = $row['total_order_cost']; 
	$nestedData[] = $row['lead_id']; 
	$nestedData[] = $s->lead_cust_segment_name($row['cust_segment']); 
	$nestedData[] = $s->enq_source_name($row['ref_source']); 
	$nestedData[] = $s->application_name($row['app_cat_id']); 
	$nestedData[] = $row['admin_fname']; 
	$nestedData[] = $row['admin_lname']; 
	$nestedData[] = $row['admin_email']; 
	$nestedData[] = $row['admin_abrv']; 
	//$nestedData[] = $row["admin_email"];

	
	$data[] = $nestedData;
}


$json_data = array(
			"draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
			"recordsTotal"    => intval( $totalData ),  // total number of records
			"recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data"            => $data   // total data array
			);

echo json_encode($json_data);  // send data as json format

?>
