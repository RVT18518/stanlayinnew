<?php
	 $date 	  	= date('Y-m-d');
//	$order_date	= "2013-05-10";
//	$a			= (strtotime($date)-strtotime($order_date))/86400;

$financial_year_start = $s->fetchGeneral_config('financial_year_start')." 1";
$fys = array( 'CALENDAR' => $financial_year_start);


// COMPUTE THE QUARTERS
foreach ($fys as $key => $start)
{
    // THE END OF THE YEAR
    $nextyear           = date('c', strtotime($start . ' + 12 Months'));

    // THE FIRST DAY OF THE QUARTER
    $res["q1"]["alpha"] = date('c', strtotime($start));
    $res["q2"]["alpha"] = date('c', strtotime($start . ' + 3 Months'));
    $res["q3"]["alpha"] = date('c', strtotime($start . ' + 6 Months'));
    $res["q4"]["alpha"] = date('c', strtotime($start . ' + 9 Months'));

    // THE LAST DAY OF THE QUARTER
    $res["q1"]["omega"] = date('c', strtotime($res["q2"]["alpha"] . ' - 1 Day'));
    $res["q2"]["omega"] = date('c', strtotime($res["q3"]["alpha"] . ' - 1 Day'));
    $res["q3"]["omega"] = date('c', strtotime($res["q4"]["alpha"] . ' - 1 Day'));
    $res["q4"]["omega"] = date('c', strtotime($nextyear           . ' - 1 Day'));

    // SAVE THESE ELEMENTS
    $new[$key] = $res;
}

//print_r($new);

function getMonthsInRange($startDate, $endDate) {
$months = array();
while (strtotime($startDate) <= strtotime($endDate)) {
    $months[] = array('year' => date('Y', strtotime($startDate)), 'month' => date('m', strtotime($startDate)), );
    $startDate = date('d M Y', strtotime($startDate.
        '+ 1 month'));
}

return $months;
}



$q1_start_date=$new['CALENDAR']['q1']['alpha'];
$q1_start_date_show=date("Y-m-d", strtotime($q1_start_date)); 
$q1_start_date_show_month=date("m", strtotime($q1_start_date)); 
$q1_end_date=$new['CALENDAR']['q1']['omega'];
$q1_end_date_show=date("Y-m-d", strtotime($q1_end_date)); 



$q2_start_date=$new['CALENDAR']['q2']['alpha'];
$q2_start_date_show=date("Y-m-d", strtotime($q2_start_date)); 
$q2_end_date=$new['CALENDAR']['q2']['omega'];
$q2_end_date_show=date("Y-m-d", strtotime($q2_end_date)); 


$q3_start_date=$new['CALENDAR']['q3']['alpha'];
$q3_start_date_show=date("Y-m-d", strtotime($q3_start_date)); 
$q3_end_date=$new['CALENDAR']['q3']['omega'];
$q3_end_date_show=date("Y-m-d", strtotime($q3_end_date)); 

$q4_start_date=$new['CALENDAR']['q4']['alpha'];
$q4_start_date_show=date("Y-m-d", strtotime($q4_start_date)); 
$q4_end_date=$new['CALENDAR']['q4']['omega'];
$q4_end_date_show=date("Y-m-d", strtotime($q4_end_date)); 




	    $date_filter = "date_ordered>='$q1_start_date_show'"; 
		$sql = "SELECT  * from tbl_order where ".$date_filter." AND(orders_status='Order Closed' OR orders_status='Order Received') AND  order_by='99'"; 
		$rs = mysqli_query($GLOBALS["___mysqli_ston"],$sql);
	  $apr = $May = $Jun = $Jul = $Aug =  $Sep = $Oct = $Nov = $Dec = $Jan =  $Feb = $Mar = 0;
	  while( $row = mysqli_fetch_object($rs))
		{
			
			$date_v = explode('-',$row->date_ordered);
			
			if($date_v[1]=='04') { $apr+=$row->total_order_cost; }
			if($date_v[1]=='05') { $May+=$row->total_order_cost; }
			if($date_v[1]=='06') { $Jun+=$row->total_order_cost; }
			if($date_v[1]=='07') { $Jul+=$row->total_order_cost; }
			if($date_v[1]=='08') { $Aug+=$row->total_order_cost; }
			if($date_v[1]=='09') { $Sep+=$row->total_order_cost; }
			if($date_v[1]=='10') { $Oct+=$row->total_order_cost; }
			if($date_v[1]=='11') { $Nov+=$row->total_order_cost; }
			if($date_v[1]=='12') { $Dec+=$row->total_order_cost; }
			if($date_v[1]=='01') { $Jan+=$row->total_order_cost; }
			if($date_v[1]=='02') { $Feb+=$row->total_order_cost; }
			if($date_v[1]=='03') { $Mar+=$row->total_order_cost; }
			
			
		}
	/*
	
	$startDate=$q1_start_date_show;

$endDate=$q4_end_date_show;
echo $abc=getMonthsInRange($startDate, $endDate);*/
/*echo "<pre>";
print_r($abc);*/
/*$abc_ctr=count($abc);		
for($m=0; $m<$abc_ctr; $m++)
{
echo "<br>".$months_r[]=$abc[$m]["month"];
}
echo $mths=implode(",",$months_r);*/
?>

<script>
                            var barChartData = {
                            //labels : ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],
							labels : [<?php echo $mths;?>],
                            datasets : [
                                {
                                    fillColor : "#FC8213",
                                    data : [<?php echo $Jan;?>,<?php echo $Feb;?>,<?php echo $Mar;?>,<?php echo $apr;?>,<?php echo $May;?>,<?php echo $Jun;?>,<?php echo $Jul;?>,<?php echo $Aug;?>,<?php echo $Sep;?>,<?php echo $Oct;?>,<?php echo $Nov;?>,<?php echo $Dec;?>]
                                }
                            ]

                        };
                            new Chart(document.getElementById("bar").getContext("2d")).Bar(barChartData);

                        </script>

 
