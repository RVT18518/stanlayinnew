<style type="text/css">
table tr td {
	font-family:Tahoma, Geneva, sans-serif;
	font-size:12px
}
table tr th.aa {
	background-color:#0e0e0e;
	font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	font-size:12px;
	text-align:center;
	text-decoration:none;
	height:35px;
/*	background-image:url(http://www.kingschest.com/images/email_top.gif);*/
	background-repeat:repeat-x;
}
.aa a {
	font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	font-size:11px;
	text-align:center;
	text-decoration:none;
	color:#fff;
	font-weight:normal;
}
#bottom {
	text-align:center;
	float:left;
	height:180px;
	width:100%;
	color:#FF9900;
	padding-top:8px;
	padding-bottom:10px;
	background-color:#0E0E0E;
	background-repeat:repeat-x;
	border-top:1px solid #e5e5e5;
	border-bottom:1px solid #e5e5e5;
}
.bottom-center {
	height:155px;
	width:100%;
	margin:auto;
}
.bottom-center-small {
	height:155px;
	min-width:190px;
	width:auto;
	float:left;
	margin-left:15px;
	text-align:left
}

ul {
	padding:0px;
	margin:0px;
}
ul li {
	list-style:none;
	text-align:left;
	padding:0px;
	margin:0px;
	padding-bottom:10px;
	padding-left:0px;
}
ul li a {
	font-family:Arial, Helvetica, sans-serif;
	font-weight:bold;
	color:#fff;
	text-decoration:none;
	font-size:11px;
}
ul li a:hover {
	font-family:Arial, Helvetica, sans-serif;
	font-weight:bold;
	color:#ECC182;
	text-decoration:underline
}
.copyright {
	font-family:Arial, Helvetica, sans-serif;
	font-weight:bold;
	color:#666;
	font-weight:bold;
	margin-top:15px;
	padding-top:10px;
	padding-bottom:10px;
	float:left;
	font-size:10px;
	padding-left:15px;
}
a {
	font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	font-size:11px;
	text-align:center;
	text-decoration:none;
	color:#333;
	font-weight:normal;
}
</style>
<table border="0" cellpadding="2" cellspacing="0" align="center" width="100%">
  <tr>
  	<th class="aa"><a href=""><img src="/images/logo_mail.png" width="150" border="0" alt="Stanlay" title="Stanlay" /></a></th>
  </tr>
  <tr>
    <td height="15"></td>
  </tr>
  <tr>
    <td height="15"></td>
  </tr>
  <tr>
    <td align="left"><strong>Dear User ,</strong></td>
  </tr>
 
  <tr>
    <td height="0"></td>
  </tr>
  <tr>
    <td>
    Please visit CRM/ KNOWLEDGE CENTRE for viewing or downloading latest data, <strong>following folder name</strong> has been created:…………or <strong>following file has been added</strong> , <strong>file name……………</strong>.. 
    
    </td>
  </tr> 
     
<tr>
    <td height="20"></td>
  </tr>
<!--  <tr>
    <td height="20"> <strong> If you have any query or need any assistance, please call us Mon to Sat (10:00AM to 6:30PM) on our Customer Service Helpline or email us per contact detail below:</strong></td>
  </tr>-->
  <tr>
    <td height="20"></td>
  </tr>
  <!--<tr>
    <td>Phone : +91 11 43434444 (From All Networks)</td>
  </tr>
  <tr>
    <td>Email : support@stanlay.com </td>
  </tr>-->
  <tr>
    <td height="20"></td>
  </tr>
  <tr>
    <td height="20"><strong>Warm regards,</strong><br />
    <a href="https://www.stanlay.in"><img src="/images/logo_mail.png" width="150" border="0" alt="Stanlay" title="Stanlay" /></a><br /></td>
  </tr>
   <tr>
    <td height="15">Stanlay</td>
  </tr>
  
  <tr>
    <td height="15"></td>
  </tr>
</table>
