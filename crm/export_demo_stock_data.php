<?php
	include("../includes1/function_lib.php");
	if(isset($_SESSION["ELqueryX"]))
	{
$sql_query = $_SESSION["ELqueryX"]; //exit;
		//$s->ExcelExpo($sql_query);
$rs=mysqli_query($GLOBALS["___mysqli_ston"],$sql_query);
while ($row=mysqli_fetch_assoc($rs))
{
$stock_id			= $row["stock_id"];
$cat_id				= $s->product_category_name($row["cat_id"]);
$pro_id				= $s->product_name($row["pro_id"]);
$serial_no			= $row["serial_no"];
$date_added			= $s->date_format_india($row["date_added1"]);
$date_updated		= $s->date_format_india($row["date_updated"]);
$qty				= $row["qty"];
$available_qty		= $s->product_qty($row["pro_id"]);
$office_location_id	= $s->stock_office_location_name($row["office_location_id"]);
$remarks			= strip_tags($row["remarks"]);
$transf_stock_id	= $row["transf_stock_id"];
$added_by			= $s->account_manager_name_full($row["added_by"]);
$status				= $row["status"];
$deleteflag			= $row["deleteflag"];
	$data[] = array(
//		"Stock Id" => $stock_id,
		  "Office Location" => $office_location_id,
  		  "Product Name" => $pro_id,
		  "Product category" => $cat_id,
		// "Date Updated" => $date_updated,
		  "Demo Stock Qty " => $qty,
		  "Warehouse Qty" => $available_qty,
		  "Serial No." => $serial_no,
		  "Date Added" => $date_added,
		  //"Transfer stock ID" => $transf_stock_id,
		  "Remarks" => $remarks,
		  "Added by " => $added_by,
//	  	  "Status" => $status,
	//	  "deleteflag" => $deleteflag
		  		   );

}

function filterData(&$str)
	{
		$str = preg_replace("/\t/", "\\t", $str);
		$str = preg_replace("/\r?\n/", "\\n", $str);
		if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
	}

	// file name for download
	$fileName = "Demo Stock report - As on dated :" . date('d-M-Y ').".xls";
	// headers for download
	header("Content-Disposition: attachment; filename=\"$fileName\"");
	header("Content-Type: application/vnd.ms-excel");
	$flag = false;
	foreach($data as $row) {
		if(!$flag) {
			// display column names as first row
			echo implode("\t", array_keys($row)) . "\n";
			$flag = true;
		}
		// filter data
		array_walk($row, 'filterData');
		echo implode("\t", array_values($row)) . "\n";
	}

	exit;
	}
		else
	{
		$s->pageLocation($_SERVER['HTTP_REFERER']."&error=xlsError") ;
	}
?>