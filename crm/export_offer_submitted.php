<?php
error_reporting(E_ALL);
require_once '../vendor/autoload.php';
include("../includes1/function_lib.php");

use OneSheet\Style\BorderStyle;
use OneSheet\Style\Style;
use OneSheet\Style\Styler;	
if(isset($_SESSION["ELqueryX"]))
	{
$sql_query = $_SESSION["ELqueryX"]; //exit;
// create a header style
	$headerStyle = (new \OneSheet\Style\Style());
	$headerStyle->setFontName('Calibri');
	$headerStyle->setFontSize(12);
    $headerStyle->setFontBold();
    $headerStyle->setFontColor('FFFFFF');
    $headerStyle->setFillColor('333333');
	$headerStyle->setSurroundingBorder();
	//$headerStyle->setBorderLeft(BorderStyle::THIN, '333333');DOUBLE, '000000');	
	
	$headerStyle->setBorderRight(BorderStyle::THIN, '333333');
	$headerStyle->setBorderLeft(BorderStyle::THIN, '333333');
	$headerStyle->setBorderTop(BorderStyle::THIN, '333333');
	$headerStyle->setBorderBottom(BorderStyle::THIN, '333333');
	

// create a data style
	$greydataStyle = (new \OneSheet\Style\Style());
    $greydataStyle->setFontName('Calibri');
    $greydataStyle->setFontSize(12);
	$greydataStyle->setFillColor('f6f6f6'); //grey light
	$greydataStyle->setBorderRight(BorderStyle::THIN, '333333');
	$greydataStyle->setBorderLeft(BorderStyle::THIN, '333333');
	$greydataStyle->setBorderTop(BorderStyle::THIN, '333333');
	$greydataStyle->setBorderBottom(BorderStyle::THIN, '333333');
// create a second data style
	$orangedataStyle2 = (new \OneSheet\Style\Style());
    $orangedataStyle2->setFontName('Calibri');
    $orangedataStyle2->setFontSize(12);
    $orangedataStyle2->setFillColor('FFD966'); //orange light
	$orangedataStyle2->setBorderRight(BorderStyle::THIN, '333333');
	$orangedataStyle2->setBorderLeft(BorderStyle::THIN, '333333');
	$orangedataStyle2->setBorderTop(BorderStyle::THIN, '333333');
	$orangedataStyle2->setBorderBottom(BorderStyle::THIN, '333333');
	//$orangedataStyle2->setBorderLeft(BorderStyle::THIN, '333333');DOUBLE, '000000');	
	
// create a third data style
	$reddataStyle3 = (new \OneSheet\Style\Style());
    $reddataStyle3->setFontName('Calibri');
    $reddataStyle3->setFontSize(12);
    $reddataStyle3->setFillColor('C65911');	//dark red 
	$reddataStyle3->setBorderRight(BorderStyle::THIN, '333333');
	$reddataStyle3->setBorderLeft(BorderStyle::THIN, '333333');
	$reddataStyle3->setBorderTop(BorderStyle::THIN, '333333');
	$reddataStyle3->setBorderBottom(BorderStyle::THIN, '333333');	
 //   $reddataStyle3->setBorderLeft(BorderStyle::THIN, '333333');DOUBLE, '000000');

// create a fourth data style
   $greendataStyle4 = (new \OneSheet\Style\Style());
   $greendataStyle4->setFontName('Calibri');
   $greendataStyle4->setFontSize(12);
   $greendataStyle4->setFillColor('92D050');	//green light
	$greendataStyle4->setBorderRight(BorderStyle::THIN, '333333');
	$greendataStyle4->setBorderLeft(BorderStyle::THIN, '333333');
	$greendataStyle4->setBorderTop(BorderStyle::THIN, '333333');
	$greendataStyle4->setBorderBottom(BorderStyle::THIN, '333333');   
  // $greendataStyle4->setBorderLeft(BorderStyle::THIN, '333333');DOUBLE, '000000');

// create a fourth data style
   $bluedataStyle5 = (new \OneSheet\Style\Style());
   $bluedataStyle5->setFontName('Calibri');
   $bluedataStyle5->setFontSize(12);
   $bluedataStyle5->setFillColor('00B0F0');	//yellow/orange light
	$bluedataStyle5->setBorderRight(BorderStyle::THIN, '333333');
	$bluedataStyle5->setBorderLeft(BorderStyle::THIN, '333333');
	$bluedataStyle5->setBorderTop(BorderStyle::THIN, '333333');
	$bluedataStyle5->setBorderBottom(BorderStyle::THIN, '333333');   
  // $greendataStyle4->setBorderLeft(BorderStyle::THIN, '333333');DOUBLE, '000000');
  
   $lgreendataStyle6 = (new \OneSheet\Style\Style());
   $lgreendataStyle6->setFontName('Calibri');
   $lgreendataStyle6->setFontSize(12);
   $lgreendataStyle6->setFillColor('73FDD6');	//light green
  // $greendataStyle4->setBorderLeft(BorderStyle::THIN, '333333');DOUBLE, '000000');
	$lgreendataStyle6->setBorderRight(BorderStyle::THIN, '333333');
	$lgreendataStyle6->setBorderLeft(BorderStyle::THIN, '333333');
	$lgreendataStyle6->setBorderTop(BorderStyle::THIN, '333333');
	$lgreendataStyle6->setBorderBottom(BorderStyle::THIN, '333333');   

   $cherrydataStyle7 = (new \OneSheet\Style\Style());
   $cherrydataStyle7->setFontName('Calibri');
   $cherrydataStyle7->setFontSize(12);
   $cherrydataStyle7->setFillColor('F4B084');	//pink cherry
  // $greendataStyle4->setBorderLeft(BorderStyle::THIN, '333333');DOUBLE, '000000');
	$cherrydataStyle7->setBorderRight(BorderStyle::THIN, '333333');
	$cherrydataStyle7->setBorderLeft(BorderStyle::THIN, '333333');
	$cherrydataStyle7->setBorderTop(BorderStyle::THIN, '333333');
	$cherrydataStyle7->setBorderBottom(BorderStyle::THIN, '333333');     

   $whitedataStyle8 = (new \OneSheet\Style\Style());
   $whitedataStyle8->setFontName('Calibri');
   $whitedataStyle8->setFontSize(12);
   $whitedataStyle8->setFillColor('FFFFFF');	//pink cherry
   	$whitedataStyle8->setBorderRight(BorderStyle::THIN, '333333');
	$whitedataStyle8->setBorderLeft(BorderStyle::THIN, '333333');
	$whitedataStyle8->setBorderTop(BorderStyle::THIN, '333333');
	$whitedataStyle8->setBorderBottom(BorderStyle::THIN, '333333');
  // $greendataStyle4->setBorderLeft(BorderStyle::THIN, '333333');DOUBLE, '000000');


   $deaddataStyle8 = (new \OneSheet\Style\Style());
   $deaddataStyle8->setFontName('Calibri');
   $deaddataStyle8->setFontSize(12);
   $deaddataStyle8->setFillColor('F40000');	//pink cherry
   	$deaddataStyle8->setBorderRight(BorderStyle::THIN, '333333');
	$deaddataStyle8->setBorderLeft(BorderStyle::THIN, '333333');
	$deaddataStyle8->setBorderTop(BorderStyle::THIN, '333333');
	$deaddataStyle8->setBorderBottom(BorderStyle::THIN, '333333');   
  // $greendataStyle4->setBorderLeft(BorderStyle::THIN, '333333');DOUBLE, '000000');


// prepare some dummy header data
//$dummyHeader = array('Strings', 'Ints', 'Floats', 'Dates', 'Times', 'Uids');

$dummyHeader = array('Alphanumeric code', 'Enquiry Date', 'DO Date', 'Account Manager', 'Order Value', 'Company Name', 'Customer Name', 'Contact No', 'Product Category', 'Referral Source', 'Customer Segment', 'State', 'Order Status', 'Offer Probability', 'Enquiry Status', 'Follow up date',  'Product Name', 'Remarks');
// prepare some dummy data
$dummyData = array();

$rs=mysqli_query($GLOBALS["___mysqli_ston"],$sql_query);
while ($row=mysqli_fetch_assoc($rs))
{
$orders_id			=$row["order_id"];
$alphanumeric		=$s->get_alphanumeric_id_enq_order_id($orders_id);



if($orders_id!='0' )
{
$orders_status		= $s->get_order_status($orders_id); //$row["orders_status"];
$date_ordered		= date("d-M-Y", strtotime($s->get_delivery_order_date($orders_id)));
$enq_date			= date("d-M-Y", strtotime($row["Enq_Date"]));
// date("M-Y", strtotime($s->get_offer_date_order_id($orders_id)));//date("d-M-Y", strtotime($row["date_ordered"]));
$product_name		= $s->product_name_generated_for_excel($row["order_id"]);
if($date_ordered=='01-Jan-1970')
{
	$date_ordered="N/A";
}
else
{
	$date_ordered=$date_ordered;
}
if($orders_status=='Order Closed' || $orders_status=='Confirmed')
{
	$total_order_cost	=$s->Get_POTOTALVALUE($orders_id);
}
else
{
$total_order_cost	=$s->OrderTotalInfo_sales_home_letter($orders_id);//$row["total_order_cost"];
}

$customers_id		=$s->get_customers_id_by_order_id($orders_id);
$comp_name			=$s->company_names($customers_id);
}
else
{
$product_name		= "N/A";
$total_order_cost	="0";	
$orders_status		="Enquiry";
$comp_name			= "N/A"; //$row["Cus_name"];

$date_ordered		= "N/A"; //date("M-Y", strtotime($row["Enq_Date"]));
$enq_date			= date("d-M-Y", strtotime($row["Enq_Date"]));
//	$date_ordered="N/A";
}
$cust_name			= $row["Cus_name"];

$app_cat_id			= $s->product_category_name($row["product_category"]);
$dead_duck			= $row["dead_duck"];//$s->enq_stage_name($row["offer_probability"]);
if($dead_duck=='0')
{
$offer_probability	= $row["enq_stage"];//$s->enq_stage_name($row["offer_probability"]);
}
else
{
$offer_probability	= "Dead: 0%";//$s->enq_stage_name($row["offer_probability"]);
}

$cust_mob			= $row["Cus_mob"];
$ref_source			=$s->enq_source_name($row["ref_source"]);
$cust_segment		=$s->lead_cust_segment_name($row["cust_segment"]);
$acc_manager		=$s->account_manager_name_full($row["acc_manager"]);
$admin_team	 	    = $row["state"];//$s->teamname($row["admin_team"]);
$follow_up_date		= date("d-M-Y", strtotime($s->get_follow_up_date($orders_id)));

if($follow_up_date=='01-Jan-1970')
{
	$follow_up_date="N/A";
}
else
{
	$follow_up_date=$follow_up_date;
}
//date("d-M-Y", strtotime($row["follow_up_date"]));

//$create_date		=$row["create_date"];
$enq_remarks=$row["enq_remark_edited"];
$dummyData[] = array(
						 $alphanumeric,
						 $enq_date,
						 $date_ordered, 
						 $acc_manager,
						 $total_order_cost,
						 $comp_name,
						 $cust_name, 
						 $cust_mob,
						 $app_cat_id,
						 $ref_source,
						 $cust_segment,
						 $admin_team,
						 $orders_status,						 
						 $s->enq_stage_name($offer_probability),
						 $dead_duck,
						 $follow_up_date,
						 $product_name,
						 $enq_remarks,
						 
		  		   );	
				   
}
// create new OneSheet instance
$onesheet = new \OneSheet\Writer();

// add header with style
$onesheet->addRow($dummyHeader, $headerStyle);

// freeze everything above cell A2 (the first row will be frozen)
$onesheet->setFreezePaneCellId('A2');

// enable autosizing of column widths and row heights
$onesheet->enableCellAutosizing();

// add dummy data row by row and switch between styles

//echo "<pre>";
//print_r($dummyData);
foreach ($dummyData as $key=> $data) {
   /* if ($key % 2) {
        $onesheet->addRow($data, $greydataStyle);
    } else {
        $onesheet->addRow($data, $orangedataStyle2);
    }*/
 //echo $data[0];
	
	if ($data[13]=='Enquiry: 10%')
	{
		$onesheet->addRow($data, $greydataStyle);//grey 
	}
	
	else if ($data[13]=='RFQ/Tender Published: 30%' )
	{
		$onesheet->addRow($data, $orangedataStyle2);//orange
	}


	else if ($data[13]=='Offer/Tender Submitted: 50%' )
	{
		$onesheet->addRow($data, $reddataStyle3);//red
	}
	
		else if ($data[13]=='Negotiation Stage: 70%' )
	{
		$onesheet->addRow($data, $greendataStyle4);//green-
	}
	
		else if ($data[13]=='Order Expected: 90%' )
	{
		$onesheet->addRow($data, $bluedataStyle5);//turquoise blue- 
	}
	
	else if ($data[13]=='Po Received: 95%' )
	{
		$onesheet->addRow($data, $lgreendataStyle6);//light green- 
	}
	
	else if ($data[13]=='OIH/Delivery Order: 100%' )
	{
		$onesheet->addRow($data, $cherrydataStyle7);//pink- 
	}
	
	else if ($data[13]=='1' )
	{
		$onesheet->addRow($data, $deaddataStyle8);//pink- 
	}
	
	else {
        $onesheet->addRow($data, $whitedataStyle8);//red 
    }
}
// ignore the coming rows for autosizing
$onesheet->disableCellAutosizing();
// add an oversized dummy row
$onesheet->addRow(array('','','','','',''));
$onesheet->addRow(array('LEGENDS:','','','','',''));
$onesheet->addRow(array('Dead: 0%'),$deaddataStyle8);
$onesheet->addRow(array('Enquiry Stage: 10%'),$greydataStyle);
$onesheet->addRow(array('RFQ: 30%'),$orangedataStyle2);
$onesheet->addRow(array('Offer Submitted: 50%'),$reddataStyle3);
$onesheet->addRow(array('Negotiation Stage: 70%'),$greendataStyle4);
$onesheet->addRow(array('Order Expected: 90%'),$bluedataStyle5);
$onesheet->addRow(array('PO Received: 95%'),$lgreendataStyle6);
$onesheet->addRow(array('OIH/ Delivery Order: 100%'),$cherrydataStyle7);

// add the all the dummy rows once more, because we can =)
//$onesheet->addRows($dummyData);
// Override column widths for columns 6, 7, 8 (column 0 is the first column)
$onesheet->setFixedColumnWidths(array(5 => 30, 6 => 25, 7 => 20, 8 => 30, 9 => 15, 14 => 15, 16 => 55, 17 => 55)); //commented not to fix column width - auto work well 01-08-2020
/*if($acc_manager!=' ' && $acc_manager=='0')
{
	$file_name=$acc_manager;
}*/
$onesheet->writeToBrowser($acc_manager.'-MEL-'.date('d-M-Y').'_onesheet.xlsx');
	}
	
	else
	{
			$s->pageLocation($_SERVER['HTTP_REFERER']."&error=xlsError") ;
	}
?>