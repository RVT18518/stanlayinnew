<?php
error_reporting(E_ALL);
require_once '../vendor/autoload.php';
include("../includes1/function_lib.php");

use OneSheet\Style\BorderStyle;
use OneSheet\Style\Style;
use OneSheet\Style\Styler;	
if(isset($_SESSION["ELqueryX"]))
	{
$sql_query = $_SESSION["ELqueryX"]; //exit;
// create a header style
	$headerStyle = (new \OneSheet\Style\Style());
	$headerStyle->setFontName('Calibri');
	$headerStyle->setFontSize(12);
    $headerStyle->setFontBold();
    $headerStyle->setFontColor('FFFFFF');
    $headerStyle->setFillColor('333333');
	//$headerStyle->setBorderLeft(DOUBLE, '000000');	

// create a data style
	$greydataStyle = (new \OneSheet\Style\Style());
    $greydataStyle->setFontName('Calibri');
    $greydataStyle->setFontSize(12);
	$greydataStyle->setFillColor('f6f6f6'); //grey light
	//$greydataStyle->setBorderLeft(DOUBLE, '000000');
// create a second data style
	$orangedataStyle2 = (new \OneSheet\Style\Style());
    $orangedataStyle2->setFontName('Calibri');
    $orangedataStyle2->setFontSize(12);
    $orangedataStyle2->setFillColor('FFD966'); //orange light
	//$orangedataStyle2->setBorderLeft(DOUBLE, '000000');	
	
// create a third data style
	$reddataStyle3 = (new \OneSheet\Style\Style());
    $reddataStyle3->setFontName('Calibri');
    $reddataStyle3->setFontSize(12);
    $reddataStyle3->setFillColor('C65911');	//dark red 
 //   $reddataStyle3->setBorderLeft(DOUBLE, '000000');

// create a fourth data style
   $greendataStyle4 = (new \OneSheet\Style\Style());
   $greendataStyle4->setFontName('Calibri');
   $greendataStyle4->setFontSize(12);
   $greendataStyle4->setFillColor('92D050');	//green light
  // $greendataStyle4->setBorderLeft(DOUBLE, '000000');

// create a fourth data style
   $bluedataStyle5 = (new \OneSheet\Style\Style());
   $bluedataStyle5->setFontName('Calibri');
   $bluedataStyle5->setFontSize(12);
   $bluedataStyle5->setFillColor('00B0F0');	//yellow/orange light
  // $greendataStyle4->setBorderLeft(DOUBLE, '000000');
  
   $lgreendataStyle6 = (new \OneSheet\Style\Style());
   $lgreendataStyle6->setFontName('Calibri');
   $lgreendataStyle6->setFontSize(12);
   $lgreendataStyle6->setFillColor('73FDD6');	//light green
  // $greendataStyle4->setBorderLeft(DOUBLE, '000000');

   $cherrydataStyle7 = (new \OneSheet\Style\Style());
   $cherrydataStyle7->setFontName('Calibri');
   $cherrydataStyle7->setFontSize(12);
   $cherrydataStyle7->setFillColor('F4B084');	//pink cherry
  // $greendataStyle4->setBorderLeft(DOUBLE, '000000');

   $whitedataStyle8 = (new \OneSheet\Style\Style());
   $whitedataStyle8->setFontName('Calibri');
   $whitedataStyle8->setFontSize(12);
   $whitedataStyle8->setFillColor('FFFFFF');	//pink cherry
  // $greendataStyle4->setBorderLeft(DOUBLE, '000000');


   $deaddataStyle8 = (new \OneSheet\Style\Style());
   $deaddataStyle8->setFontName('Calibri');
   $deaddataStyle8->setFontSize(12);
   $deaddataStyle8->setFillColor('F40000');	//pink cherry
  // $greendataStyle4->setBorderLeft(DOUBLE, '000000');


// prepare some dummy header data
//$dummyHeader = array('Strings', 'Ints', 'Floats', 'Dates', 'Times', 'Uids');

$dummyHeader = array('Offer', 'Order Date', 'Account Manager', 'Order Value', 'Customer Name', 'Product Category', 'Referral Source', 'Customer Segment',  'Offer Probability');
// prepare some dummy data
$dummyData = array();
$rs=mysqli_query($GLOBALS["___mysqli_ston"],$sql_query);
while ($row=mysqli_fetch_assoc($rs))
{
 //$sql_query = $_SESSION["ELqueryX"]; //exit;
//$s->ExcelExpo($sql_query);
 
$orders_id			=$row["orders_id"];
$date_ordered		=$row["date_ordered"];
$comp_name			=$s->company_names($row["customers_id"]);
$total_order_cost	=$s->Get_POTOTALVALUE($row["orders_id"]);//$row["total_order_cost"];
$app_cat_id			=$s->product_category_name($row["app_cat_id"]);
$orders_status		=$row["orders_status"];
$offer_probability	=$row["offer_probability"];//$s->enq_stage_name($row["offer_probability"]);

$ref_source			= $s->enq_source_name($row["ref_source"]);
$cust_segment		=$s->lead_cust_segment_name($row["cust_segment"]);
$acc_manager		=$s->account_manager_name_full($row["acc_manager"]);
$admin_team	 	    =$s->teamname($row["admin_team"]);
//$create_date		=$row["create_date"];

$dummyData[] = array(
						 $orders_id,
						 $date_ordered, 
						 $acc_manager,
						 $total_order_cost,
						 $comp_name,
						 $app_cat_id,
						 $ref_source,
						 $cust_segment,
						 $s->enq_stage_name($offer_probability),
		  		   );	
				   
}
// create new OneSheet instance
$onesheet = new \OneSheet\Writer();

// add header with style
$onesheet->addRow($dummyHeader, $headerStyle);

// freeze everything above cell A2 (the first row will be frozen)
$onesheet->setFreezePaneCellId('A2');

// enable autosizing of column widths and row heights
$onesheet->enableCellAutosizing();

// add dummy data row by row and switch between styles

//echo "<pre>";
//print_r($dummyData);
foreach ($dummyData as $key=> $data) {
   /* if ($key % 2) {
        $onesheet->addRow($data, $greydataStyle);
    } else {
        $onesheet->addRow($data, $orangedataStyle2);
    }*/
 //echo $data[0];
	
	if ($data[8]=='Enquiry: 10%')
	{
		$onesheet->addRow($data, $greydataStyle);//grey 
	}
	
	else if ($data[8]=='RFQ/Tender Published: 30%' )
	{
		$onesheet->addRow($data, $orangedataStyle2);//orange
	}


	else if ($data[8]=='Offer/Tender Submitted: 50%' )
	{
		$onesheet->addRow($data, $reddataStyle3);//red
	}
	
		else if ($data[8]=='Negotiation Stage: 70%' )
	{
		$onesheet->addRow($data, $greendataStyle4);//green-
	}
	
		else if ($data[8]=='Order Expected: 90%' )
	{
		$onesheet->addRow($data, $bluedataStyle5);//turquoise blue- 
	}
	
	else if ($data[8]=='Po Received: 95%' )
	{
		$onesheet->addRow($data, $lgreendataStyle6);//light green- 
	}
	
	else if ($data[8]=='OIH/Delivery Order: 100%' )
	{
		$onesheet->addRow($data, $cherrydataStyle7);//pink- 
	}
	
	else if ($data[11]=='1' )
	{
		$onesheet->addRow($data, $deaddataStyle8);//pink- 
	}
	
	else {
        $onesheet->addRow($data, $whitedataStyle8);//red 
    }
}
// ignore the coming rows for autosizing
$onesheet->disableCellAutosizing();
// add an oversized dummy row
$onesheet->addRow(array('','','','','',''));
$onesheet->addRow(array('LEGENDS:','','','','',''));
$onesheet->addRow(array('Dead: 0%'),$deaddataStyle8);
$onesheet->addRow(array('Enquiry Stage: 10%'),$greydataStyle);
$onesheet->addRow(array('RFQ: 30%'),$orangedataStyle2);
$onesheet->addRow(array('Offer Submitted: 50%'),$reddataStyle3);
$onesheet->addRow(array('Negotiation Stage: 70%'),$greendataStyle4);
$onesheet->addRow(array('Order Expected: 90%'),$bluedataStyle5);
$onesheet->addRow(array('PO Received: 95%'),$lgreendataStyle6);
$onesheet->addRow(array('OIH/ Delivery Order: 100%'),$cherrydataStyle7);

// add the all the dummy rows once more, because we can =)
//$onesheet->addRows($dummyData);
// Override column widths for columns 6, 7, 8 (column 0 is the first)
//$onesheet->setFixedColumnWidths(array(5 => 10, 6 => 10, 7 => 10)); //commented not to fix column width - auto work well 01-08-2020
$onesheet->writeToBrowser('Order-Confirmed-'.date('d-M-Y').'_onesheet.xlsx');
	}
	
	else
	{
			$s->pageLocation($_SERVER['HTTP_REFERER']."&error=xlsError") ;
	}

?>