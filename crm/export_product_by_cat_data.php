<?php
	include("../includes1/function_lib.php");
	if(isset($_SESSION["ELqueryX_product"]))
	{
$sql_query = $_SESSION["ELqueryX_product"]; //exit;
//$s->ExcelExpo($sql_query);
$rs=mysqli_query($GLOBALS["___mysqli_ston"],$sql_query);
while ($row=mysqli_fetch_assoc($rs))
{
$pro_title			=htmlentities(stripslashes($row["pro_title"]));
$pro_id				=$row["pro_id"];
$pro_max_discount	=$row["pro_max_discount"];
$pro_price			=$s->product_price($row["pro_id"]);
$status				=$row["status"];
$pro_code_offer		=$row["pro_code_offer"];
$app_cat_id			=$s->product_category_name($row["app_cat_id"]);
$upc_code			=$s->pro_upc_code($row["pro_id"]);

	$data[] = array(
		"Product Name" => $pro_title,
		 "Product ID" => $pro_id,
		 "Max Discount" => $pro_max_discount,
		 "Product Price" => $pro_price,
		 "Status" => $status,
		 "Product Offer Code" => $pro_code_offer,
		 "UPC Code" => $upc_code,
		  "Category" => $app_cat_id	
		  	   );
}
function filterData(&$str)
	{
		$str = preg_replace("/\t/", "\\t", $str);
		$str = preg_replace("/\r?\n/", "\\n", $str);
		if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
	}
	// file name for download
	$fileName = "Product Manager database - As on dated :" . date('d-M-Y ').".xls";
// headers for download
	header("Content-Disposition: attachment; filename=\"$fileName\"");
	header("Content-Type: application/vnd.ms-excel");
	$flag = false;
	foreach($data as $row) {
		if(!$flag) {
			// display column names as first row
			echo implode("\t", array_keys($row)) . "\n";
			$flag = true;
		}
		// filter data
		array_walk($row, 'filterData');
		echo implode("\t", array_values($row)) . "\n";
	}
	exit;
}
		else
	{
		$s->pageLocation($_SERVER['HTTP_REFERER']."&error=xlsError") ;
	}
?>