<?php
$maxdays="60";
if($_REQUEST["action"]=='')
{
	$_REQUEST["action"]="edit";
}
 	$data_action = $_REQUEST["action"];//exit;
	$rs 		 = $s->getData_without_condition('tbl_general_configuraction');
	if(mysqli_num_rows($rs)!=0)
	{
		$row	 						= mysqli_fetch_object($rs);
		$pcode	 						= $row->gen_config_id;
		$financial_year_start			= $row->financial_year_start;
		$alarm1							= $row->alarm_1;
		$alarm2							= $row->alarm_2;
		$alarm3							= $row->alarm_3;
		$alarm_1_additional_email		= $row->alarm_1_additional_email;
		$alarm_2_additional_email		= $row->alarm_2_additional_email;
		$alarm_3_additional_email		= $row->alarm_3_additional_email;
		$alarm_enq_not_assigned			= $row->alarm_enq_not_assigned;
 		$enq_not_assigned_alarm_email	= $row->enq_not_assigned_alarm_email;
		$sales_head_email				= $row->sales_head_email;
		$pdf_key						= $row->pdf_key;		
	}
	if($_REQUEST['action'] == 'delete_image')
	{
		unlink("../".$row->store_logo);
		$DelLogo["store_logo"] = '';
		$DelLogo["display_title"] = "name";
		$result      = $s->editRecord('tbl_general_configuraction',$DelLogo,'gen_config_id',$pcode);
		$s->pageLocation("index.php?pagename=general_configuration&action=edit");
	}
	if($_REQUEST['action'] == 'update' && $_REQUEST["add"]=='Save')
	{
		$fileArray["website_title"] = addslashes($_REQUEST["title"]);
		$fileArray["store_name"]   	= addslashes($_REQUEST["store_name"]);
		$fileArray["admin_email"]  	= $_REQUEST["admin_email"];
		$fileArray["meta_content"] 	= $_REQUEST["meta_content"];
		$fileArray["meta_desc"]  	= $_REQUEST["meta_desc"];
		$fileArray["pdf_key"]  		= $_REQUEST["pdf_key"];
		$fileArray["financial_year_start"]  	= $_REQUEST["financial_year_start"];
		
		$fileArray["alarm_1"]  		= $_REQUEST["alarm_1"];
		$fileArray["alarm_2"]  		= $_REQUEST["alarm_2"];
		$fileArray["alarm_3"]  		= $_REQUEST["alarm_3"];
		$fileArray["alarm_1_additional_email"]	= $_REQUEST["alarm_1_additional_email"];
		$fileArray["alarm_2_additional_email"]	= $_REQUEST["alarm_2_additional_email"];
		$fileArray["alarm_3_additional_email"]	= $_REQUEST["alarm_3_additional_email"];		
		$fileArray["alarm_enq_not_assigned"]	= $_REQUEST["alarm_enq_not_assigned"];
		$fileArray["enq_not_assigned_alarm_email"]	= $_REQUEST["enq_not_assigned_alarm_email"];
		$fileArray["sales_head_email"]			= $_REQUEST["sales_head_email"];
				
		
		
		if($_FILES["logo"]["name"] != '')
		{
			unlink("../".$row->store_logo);
			$StoreLogoImage 		 = $s->ImageUpload('uploads/storelogo/','logo','STORELOGO','257');
			$fileArray["store_logo"] = $StoreLogoImage;
		}
		if($_REQUEST["display_title"] == "")
		{
			$fileArray["display_title"] = "name";//$_REQUEST["displayTitle"];
		}
		if($_REQUEST["cont_email"] == "")
		{
			$fileArray["contact_email"] = $_REQUEST["admin_email"];
		}
		else
		{
			$fileArray["contact_email"] = $_REQUEST["cont_email"];
		}
		if($_REQUEST["customer_support"] == "")
		{
			$fileArray["customer_support__email"] = $_REQUEST["admin_email"];
		}
		else
		{
			$fileArray["customer_support__email"] = $_REQUEST["customer_support"];
		}
		if($_REQUEST["general_contact"] == "")
		{
			$fileArray["gen_contact_email"]  = $_REQUEST["admin_email"];
		}
		else
		{
			$fileArray["gen_contact_email"] = $_REQUEST["general_contact"];
		}
		//////////////
	}
	if($data_action =='update')
	{
		$result      = $s->editRecord('tbl_general_configuraction',$fileArray,'gen_config_id',$pcode);
		$s->pageLocation("index.php?pagename=general_configuration&action=edit&setX=1");
	}
	if($data_action =='edit')
	{
		$rs  = $s->getData_with_condition('tbl_general_configuraction','gen_config_id',$pcode);
		$row = mysqli_fetch_object($rs);
		$data_action = 'update';
	}
?>

<form name="frx1" id="frx1" action="index.php?pagename=general_configuration&action=<?php echo $data_action;?>&pcode=<?php echo $pcode;?>" method="post" enctype="multipart/form-data">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="90%" class="pageheadTop">General Configuration </td>
            <td width="9%" class="headLink" align="right"><input name="add" type="submit" class="inputton" id="add" value="Save">
              &nbsp; </td>
            <td width="1%"  class="pad" align="right"></td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td class="pHeadLine"></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><?php 
	if($_REQUEST['setX']=='1')
	{
	 	if($result==0)
		{
			echo "<p class='success'>".record_update."</p><br />";	
		}
		else if($result==1)
		{
			echo "<p class='error'>".record_not_update."</p><br />";	
		}
	}
		
?></td>
    </tr>
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="3">
          <tr>
            <td valign="top" class="pagecontent"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="tblBorder">
                <tr class="pagehead">
                  <td   class="pad">Store Configuration</td>
                </tr>
                <tr>
                  <td ><table width="100%" border="0" cellpadding="3" cellspacing="0">
                      <tr class="head">
                        <td  class="pad" colspan="2">Website Configuration</td>
                      </tr>
                      <tr class="text">
                        <td align="right" class="pad"><strong>Website Title </strong><span class="redstar">*</span></td>
                        <td><input type="text" name="title" id="title" value="<?php echo stripslashes($row->website_title);?>" class="inpuTxt" /></td>
                      </tr>
                      <tr class="text">
                        <td align="right" class="pad"><strong>Company Name</strong> <span class="redstar">*</span></td>
                        <td><input type="text" name="store_name" id="store_name" value="<?php echo stripslashes($row->store_name);?>" class="inpuTxt" /></td>
                      </tr>
                 
                      <tr class="head">
                        <td  class="pad" colspan="2">Email Configuration</td>
                      </tr>
                      <tr class="text">
                        <td align="right"  class="pad"><strong>Admin Email</strong> <span class="redstar">*</span></td>
                        <td ><input type="email" name="admin_email" id="admin_email" value="<?php echo $row->admin_email;?>" class="inpuTxt" /></td>
                      </tr>
                      <?php /* <tr class="text">
<td  class="pad">Contact Us Email</td>
<td ><input type="text" name="cont_email" id="cont_email" value="<?php echo $row->contact_email;?>" class="inpuTxt" /></td>
</tr>
<tr class="text">
<td  class="pad">Customer Support Email</td>
<td ><input type="text" name="customer_support" id="customer_support" value="<?php echo $row->customer_support__email;?>" class="inpuTxt" /></td>
</tr>
<tr class="text">
<td  class="pad">General Contact Email</td>
<td ><input type="text" name="general_contact" id="general_contact" value="<?php echo $row->gen_contact_email;?>" class="inpuTxt" /></td>
</tr> */?>
                      <tr class="head">
                        <td  class="pad" colspan="2">SEO Configuration</td>
                      </tr>
                      <tr class="text">
                        <td align="right" valign="top" class="pad"><strong>Meta Content </strong></td>
                        <td align="left"><textarea class="inpuTxtSelect" cols="35" name="meta_content" id="meta_content" rows="6" ><?php echo stripslashes($row->meta_content);?></textarea></td>
                      </tr>
                      <tr class="text" valign="top">
                        <td align="right" class="pad"><strong>Meta Description </strong></td>
                        <td align="left"><textarea class="inpuTxtSelect" cols="35" name="meta_desc" id="meta_desc" rows="6" ><?php echo stripslashes($row->meta_desc);?></textarea></td>
                      </tr>
                      
                       <tr class="head" style="display:non">
                        <td  class="pad" colspan="2">PDF Keys</td>
                      </tr>
                      <tr class="text" >
                        <td align="right"  class="pad"><strong>PDF Key</strong> <span class="redstar">*</span></td>
                        <td ><select name="pdf_key" id="pdf_key" class="inpuTxtSelect">
                    <option value="0">Select PDF Key</option>
                    <option value="3cef76d3-dc7e-473f-8e0b-81c42dd932a0" <?php if("3cef76d3-dc7e-473f-8e0b-81c42dd932a0" == $pdf_key ){ echo "selected='selected'"; }?>>3cef76d3-dc7e-473f-8e0b-81c42dd932a0</option>
                    <option value="7577944f-6e07-4dec-8244-6a830f893e45" <?php if("7577944f-6e07-4dec-8244-6a830f893e45" == $pdf_key ){ echo "selected='selected'"; }?>>7577944f-6e07-4dec-8244-6a830f893e45</option>
                    <option value="83a1c6d5-d224-40f2-88d9-2cab648eca8e" <?php if("83a1c6d5-d224-40f2-88d9-2cab648eca8e" == $pdf_key ){ echo "selected='selected'"; }?>>83a1c6d5-d224-40f2-88d9-2cab648eca8e</option>
                    <option value="373e7cf3-0985-49aa-833c-2102c7b07f48" <?php if("373e7cf3-0985-49aa-833c-2102c7b07f48" == $pdf_key ){ echo "selected='selected'"; }?>>373e7cf3-0985-49aa-833c-2102c7b07f48</option>
                    <option value="b7f89c94-8a5e-4da1-a0b0-32c7b286ecf1" <?php if("b7f89c94-8a5e-4da1-a0b0-32c7b286ecf1" == $pdf_key ){ echo "selected='selected'"; }?>>b7f89c94-8a5e-4da1-a0b0-32c7b286ecf1</option>
                    <option value="a1b1a7d1-363b-44a8-a1e1-055bf47a0a3f" <?php if("a1b1a7d1-363b-44a8-a1e1-055bf47a0a3f" == $pdf_key ){ echo "selected='selected'"; }?>>a1b1a7d1-363b-44a8-a1e1-055bf47a0a3f</option>
                    <option value="0b1f25cf-cd31-466b-8062-0cd4d71eb7a8" <?php if("0b1f25cf-cd31-466b-8062-0cd4d71eb7a8" == $pdf_key ){ echo "selected='selected'"; }?>>0b1f25cf-cd31-466b-8062-0cd4d71eb7a8</option>
                   <option value="9c549974-4f12-4386-adf6-528e4e9fce5d" <?php if("9c549974-4f12-4386-adf6-528e4e9fce5d" == $pdf_key ){ echo "selected='selected'"; }?>>9c549974-4f12-4386-adf6-528e4e9fce5d</option>
                   <option value="03267e6b-fe7f-4541-97b6-c8060901a7df" <?php if("03267e6b-fe7f-4541-97b6-c8060901a7df" == $pdf_key ){ echo "selected='selected'"; }?>>03267e6b-fe7f-4541-97b6-c8060901a7df</option>
                     
                    
                    
                          </select>
                          <!--ALTER TABLE `tbl_general_configuraction` ADD `financial_year_start` VARCHAR(12) NOT NULL DEFAULT '0' AFTER `meta_desc`;--></td>
                      </tr>  
                      <tr class="head" style="display:none">
                        <td  class="pad" colspan="2">Define Quarters</td>
                      </tr>
                      <tr class="text" >
                        <td align="right"  class="pad"><strong>Finacial Year Starts from</strong> <span class="redstar">*</span></td>
                        <td ><select name="financial_year_start" id="financial_year_start" class="inpuTxtSelect">
                    <option value="0">Select Year</option>
                    <option value="January" <?php if("January" == $financial_year_start ){ echo "selected='selected'"; }?>>January 1</option>
                    <option value="February"<?php if("February" == $financial_year_start ){ echo "selected='selected'"; }?>>February 1</option>
                    <option value="March"<?php if("March" == $financial_year_start ){ echo "selected='selected'"; }?>>March 1</option>
                    <option value="April"<?php if("April" == $financial_year_start ){ echo "selected='selected'"; }?>>April 1</option>
                    <option value="May"<?php if("May" == $financial_year_start ){ echo "selected='selected'"; }?>>May 1</option>
                    <option value="June"<?php if("June" == $financial_year_start ){ echo "selected='selected'"; }?>>June 1</option>
                    <option value="July"<?php if("July" == $financial_year_start ){ echo "selected='selected'"; }?>>July 1</option>
                    <option value="August"<?php if("August" == $financial_year_start ){ echo "selected='selected'"; }?>>August 1</option>
                    <option value="September"<?php if("September" == $financial_year_start ){ echo "selected='selected'"; }?>>September 1</option>
                    <option value="October"<?php if("October" == $financial_year_start ){ echo "selected='selected'"; }?>>October 1</option>
                    <option value="November"<?php if("November" == $financial_year_start ){ echo "selected='selected'"; }?>>November 1</option>
                    <option value="December"<?php if("December" == $financial_year_start ){ echo "selected='selected'"; }?>>December 1</option>
                          </select><!--ALTER TABLE `tbl_general_configuraction` ADD `financial_year_start` VARCHAR(12) NOT NULL DEFAULT '0' AFTER `meta_desc`;--></td>
                      </tr>
       <!--alarms starts-->               
       
        <tr class="head" >
                        <td  class="pad" colspan="2">Alarms Configuration</td>
                      </tr>
                      <tr class="text" >
                        <td align="right"  class="pad"><strong>Sales Head Email <span class="redstar">*</span></strong></td>
                        <td><input type="email" name="sales_head_email" id="sales_head_email" value="<?php echo stripslashes($sales_head_email);?>" class="inpuTxt" required="required" /></td>
                      </tr>
                      <tr class="text" >
                        <td align="right"  class="pad"><strong>Alarm 1</strong> <span class="redstar">*</span></td>
                        <td><select name="alarm_1" id="alarm_1" class="inpuTxtSelect">
                            <option value="">Select Alarm Days</option>
                        <?php
                      for ($d=1; $d<=$maxdays; $d++) {?>
					<option value="<?php echo $d;?>" <?php if($d == $alarm1 ){ echo "selected='selected'"; }?>><?php echo $d;?> days</option>
						<?php  }?>
                            
                           
                           </select>
                        </td>
                      </tr>
                      
                      
                      <tr class="text" >
                        <td align="right"  class="pad"><strong>Alarm 1 Additional Person Email</strong> <span class="redstar">*</span></td>
                        <td><input type="email" name="alarm_1_additional_email" id="alarm_1_additional_email" value="<?php echo stripslashes($alarm_1_additional_email);?>" class="inpuTxt" />                        </td>
                      </tr>
                      
                      
                      
                      <tr class="text" >
                        <td align="right"  class="pad"><strong> Alarm 2 </strong><span class="redstar">*</span></td>
                        <td><select name="alarm_2" id="alarm_2" class="inpuTxtSelect">
                            <option value="">Select Alarm Days</option>
                        <?php
                      for ($d2=1; $d2<=$maxdays; $d2++) {?>
					<option value="<?php echo $d2;?>" <?php if($d2 == $alarm2 ){ echo "selected='selected'"; }?>><?php echo $d2;?> days</option>
						<?php  }?>
                            
                           
                           </select>
                        </td>
                      </tr>
                      
                      <tr class="text" >
                        <td align="right"  class="pad"><strong>Alarm 2 Additional Person Email</strong> <span class="redstar">*</span></td>
                        <td><input type="email" name="alarm_2_additional_email" id="alarm_2_additional_email" value="<?php echo stripslashes($alarm_2_additional_email);?>" class="inpuTxt" />                        </td>
                      </tr>
                      
                      
                      
                      
                      <tr class="text" >
                        <td align="right"  class="pad"><strong>Alarm 3 </strong><span class="redstar">*</span></td>
                        <td><select name="alarm_3" id="alarm_3" class="inpuTxtSelect" required>
                            <option value="">Select Alarm Days</option>
                        <?php
                      for ($d3=1; $d3<=$maxdays; $d3++) {?>
					<option value="<?php echo $d3;?>" <?php if($d3 == $alarm3 ){ echo "selected='selected'"; }?>><?php echo $d3;?> days</option>
						<?php  }?>
                           </select>
                        </td>
                      </tr>
                      
                      <tr class="text" >
                        <td align="right"  class="pad"><strong>Alarm 3 Additional Person Email</strong> <span class="redstar">*</span></td>
                        <td><input type="email" name="alarm_3_additional_email" id="alarm_3_additional_email" value="<?php echo stripslashes($alarm_3_additional_email);?>" class="inpuTxt" />                        </td>
                      </tr>
                      
                      
					 <tr class="text" >
                        <td align="right"  class="pad"><strong>Enquries not assigned</strong><span class="redstar">*</span></td>
                        <td><select name="alarm_enq_not_assigned" id="alarm_enq_not_assigned" class="inpuTxtSelect" required>
                            <option value="">Select Alarm Days</option>
                        <?php
                      for ($d4=1; $d4<=$maxdays; $d4++) {?>
					<option value="<?php echo $d4;?>" <?php if($d4 == $alarm_enq_not_assigned){ echo "selected='selected'"; }?>><?php echo $d4;?> days</option>
						<?php  }?>
                           </select>
                        </td>
                      </tr>
                      
                      <tr class="text" >
                        <td align="right"  class="pad"><strong>Enquiry not assigned alarm email</strong> <span class="redstar">*</span></td>
                        <td><input type="email" name="enq_not_assigned_alarm_email" id="enq_not_assigned_alarm_email" value="<?php echo stripslashes($enq_not_assigned_alarm_email);?>" class="inpuTxt" />                        </td>
                      </tr>                      
                      
                      
             
		<!--alarms ends-->                      
                      <tr class="text">
                        <td class="pad">&nbsp;</td>
                        <td align="left"><input name="add" type="submit" class="inputton" id="add" value="Save"></td>
                      </tr>
                      <tr class="text">
                        <td class="redstar pad" colspan="2"> * Required Fields </td>
                      </tr>
                  </table></td>
                </tr>
              </table></td>
          </tr>
        </table></td>
    </tr>
  </table>
</form>
<script language="JavaScript" type="text/javascript">
 var frmvalidator = new Validator("frx1");
 frmvalidator.addValidation("title","req","Please enter website title.");
 frmvalidator.addValidation("store_name","req","Please enter Website Name.");
 frmvalidator.addValidation("admin_email","req","Please enter Admin Email.");
 frmvalidator.addValidation("admin_email","email","Please enter valid Admin Email Address.");
/* frmvalidator.addValidation("cont_email","email","Please enter valid Contact Us Email Address.");
 frmvalidator.addValidation("customer_support","email","Please enter valid Customer Support Email Address.");
 frmvalidator.addValidation("general_contact","email","Please enter valid General Contact Email Address.");*/
</script> 
