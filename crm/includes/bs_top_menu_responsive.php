<div class="container">
  <div class="row">
    <div class="span12">
      <div class="head">
        <div class="row-fluid">
            <div class="span12">
           
                    <h1 class="muted"> <?php
		  if($_SESSION["AdminLoginID_SET"]=='4')
		  {
		  ?>
            <a href="index.php?pagename=report_summary" ><img src="../images/logo.png" width="180" height="51" alt="img" border="0" /></a>
            <?php } else {?>
            <a href="index.php?pagename=report_summary" ><img src="../images/logo.png" width="180" height="51" alt="img" border="0" /></a>
            <?php }?></h1>
                </div>

             
        </div>

      <section class="navigation">
  <div class="nav-container">
   
    <nav>
      <div class="nav-mobile"><a id="nav-toggle" href="#!"><span></span></a></div>
      <ul class="nav-list">
        <li>
          <a href="index.php?pagename=report_summary">My Dashboard</a>
        </li>
        <li>
          <a href="#!">Sales Manager</a>
		  <ul class="nav-dropdown">
            <li>
              <a href="index.php?pagename=company_manager">Company Manager</a>
            </li>
           <li><a href="index.php?pagename=web_enq_manager_edited" class="arrow">Add/View Enquiry</a></li>
            <!--      <li><a href="index.php?pagename=add_lead&action=add_new" class="arrow">Create New Lead/Offer</a></li>-->
            <li><a href="index.php?pagename=lead_manager" class="arrow">View Leads/Create Offer</a></li>
            <li><a href="index.php?pagename=order_manager">View Your Offer/Book Order</a></li>
            <li><a href="index.php?pagename=sales_manager">View Sales Performance</a></li>
            <li><a href="index.php?pagename=transfer_account_manager">Transfer Account Manager</a></li>
            <li><a href="index.php?pagename=web_enq_manager">Enquiry Manager</a></li>
          </ul>
        </li>
        <li>
          <a href="#!">Dispatch Cordination</a>
          <ul class="nav-dropdown">
            <li><a href="index.php?pagename=do_manager">DO Manager</a></li>
          </ul>
        </li>
       
	   
	    <li>
          <a href="#!">Warehousing</a>
		  <ul class="nav-dropdown">
            <li><a href="view_stock.php" target="_blank">Product Quantity View (PQV)</a></li>
            <li><a href="index.php?pagename=manager_product_stock">Stock Out</a></li>
            <li><a href="index.php?pagename=view_incoming_stock">Incoming Manager</a></li>
            <li><a href="index.php?pagename=generate_serial_no">Generate Serial No </a></li>
          </ul>
        </li>
	   
	   
        <li>
          <a href="#!">Purchasing</a>
          <ul class="nav-dropdown">
            <li><a href="index.php?pagename=purchase_manager">Purchase Manager</a></li>
            <li><a href="index.php?pagename=vendor_manager">Vendor Master</a></li>
            <li><a href="index.php?pagename=add_vendor_product_list">Vendor Products Manager</a></li>
            <!--<li><a href="index.php?pagename=do_manager">DO Manager</a></li>-->
          </ul>
        </li>
		
        <li>
          <a href="#!">Tendering</a>
          <ul class="nav-dropdown">
           <li><a href="index.php?pagename=add_tender&amp;action=add_new">Upload Tender</a></li>
            <li><a href="index.php?pagename=view_open_tender&amp;action=view_open_tender">View Open Tenders</a></li>
            <li><a href="index.php?pagename=view_close_tender&amp;action=view_close_tender">View Close Tenders</a></li>
            <li><a href="index.php?pagename=view_emd_stat&amp;action=view_emd_stat">View EMD Status</a></li>
            <li><a href="index.php?pagename=view_sd_stat&amp;action=view_sd_stat">View SD Status</a></li>
          </ul>
        </li>
		
		
		 <li>
          <a href="#!">Knowledge Center</a>
          <ul class="nav-dropdown">
         <li> <a href="index.php?pagename=add_fol_cat&amp;action=add_new">Add Folder/Document</a></li>
            <li> <a href="index.php?pagename=edit_fol_cat&amp;action=edit">Edit Folder/Document</a></li>
            <li> <a href="index.php?pagename=kc_home">View/Download Folder/Document</a></li>
          </ul>
        </li>
		
		
		
		 <li>
          <a href="#!">A/c Payable</a>
          <ul class="nav-dropdown">
        <li><a href="index.php?pagename=cus_payments&amp;action=view_payments">Pending Customers Payments</a></li>
            <li><a href="index.php?pagename=ven_payments&amp;action=view_payments">Pending Vendors Payments</a></li>
            <li><a href="index.php?pagename=c_form_payments&amp;action=view_payments">Pending C Form Receivable</a></li>
            <li><a href="index.php?pagename=c_form_ven&amp;action=view_payments">Vendor C Form Payable</a></li>
            <li><a href="index.php?pagename=view_emd_stat&amp;action=view_emd_stat">Pending EMD Status</a></li>
            <li><a href="index.php?pagename=view_sd_stat&amp;action=view_sd_stat">Pending SD Status</a></li>
			<li><a href="index.php?pagename=view_pi_stat">Proforma Invoices</a></li>
          </ul>
        </li>
		
		
		 <li>
          <a href="#!">HR Management</a>
          <ul class="nav-dropdown">
       <li><a href="index.php?pagename=manage_designation" class="arrow">Designation Manager</a></li>
            <li><a href="index.php?pagename=admin_manager" class="arrow">Employee Manager</a></li>
            <li><a href="index.php?pagename=view_attendance">Attendance Manager</a></li>
            <li><a href="index.php?pagename=manage_team">Team Manager</a></li>
          </ul>
        </li>
		
		
		 <li>
          <a href="#!">Admin Managment</a>
          <ul class="nav-dropdown">
       <li><a href="index.php?pagename=manage_product" class="arrow">Product Manager</a></li>
            <li><a href="index.php?pagename=manage_application" class="arrow">Product Category Manager</a></li>
            <li><a href="index.php?pagename=customer_manager" class="arrow">Customer Database Manager</a></li>
            <li><a href="index.php?pagename=manage_enq_source">Enquiry Source Manager</a></li>
            <li><a href="index.php?pagename=offer_id_generation">Offer ID Generation</a></li>
            <li><a href="index.php?pagename=manage_co_branch_address" class="arrow">Company Branch Manager-DC</a></li>
          </ul>
        </li>
		
		
		
		
		
		
      </ul>
    </nav>
  </div>
</section>
      

	  


<article>

<?php if($_SESSION["AdminLoginID_SET"]!=''){?>
Welcome <?php echo $_SESSION["AdminName_SET"];?><span class="saperator">|</span> <?php echo date("l F d, Y")?> <span class="saperator">|</span> 
 <?php }?>
</article> 
  
  <!-- Collapsible content -->

</nav>
<!--/.Navbar-->