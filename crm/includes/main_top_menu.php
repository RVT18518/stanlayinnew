<ul class="menuH decor1" >
  <li><a href="index.php?pagename=report_summary" class="arrow"><img src="images/newicon/dashboard.png" width="14" style="margin-top: -2px;" /> My Dashboard</a></li>
  <li><a  href="index.php?pagename=order_closed" class="arrow"><img src="images/newicon/sales1.png" width="14" style="margin-top: -2px;" /> Sales Manager</a>
    <ul>
      <li><a href="index.php?pagename=company_manager" class="arrow">Company Manager</a></li>
      <li><a href="index.php?pagename=web_enq_manager_edited" class="arrow">Add/View Enquiry</a></li>
      <!--      <li><a href="index.php?pagename=add_lead&action=add_new" class="arrow">Create New Lead/Offer</a></li>-->
      <li><a href="index.php?pagename=lead_manager" class="arrow">View Leads/Create Offer</a></li>
      <li><a href="index.php?pagename=order_manager">View Offer/Book Order/Create PI &nbsp;</a></li>
      <li><a href="index.php?pagename=customer_po_manager">View Customer PO </a></li>
      <li><a href="index.php?pagename=order_closed">View Sales Performance</a></li>
      <li><a href="index.php?pagename=transfer_account_manager">Transfer Account Manager</a></li>
      <li><a href="index.php?pagename=web_enq_manager">Enquiry Manager</a></li>
      <li><a href="index.php?pagename=calendar">Task Manager</a></li>      
      <li><a href="index.php?pagename=target_summary">Target Summary</a></li>      
    </ul>
  </li>
  <li><a class="arrow"><img src="images/newicon/dispatch.png" width="14" style="margin-top: -2px;" /> Dispatch Cordination</a>
    <ul>
      <!--            <li><a>Demos</a></li>-->
      <li><a href="index.php?pagename=do_manager">DO Manager</a></li>
    </ul>
  </li>
  <li><a class="arrow"><img src="images/newicon/warehousing-icon.png" width="14" style="margin-top: -2px;" /> Warehousing</a>
    <ul>
      <li><a href="view_stock.php" target="_blank">Product Quantity View (PQV)</a></li>
      <li><a href="index.php?pagename=manager_product_stock">Stock Out</a></li>
      <li><a href="index.php?pagename=view_incoming_stock">Incoming Manager</a></li>
      <li><a href="index.php?pagename=generate_serial_no">Generate Serial No </a></li>
      <li><a href="index.php?pagename=do_manager">Delivery Order Manager</a></li>
      <li><a href="index.php?pagename=delivery_challan_manager">Delivery Challan </a></li>
      <li><a href="index.php?pagename=demo_stock_manager">Demo Stock</a></li>
      <li><a href="index.php?pagename=inventory_manager">Inventory Manager</a></li>
      <li><a href="index.php?pagename=stock_report_outgoing">Stock Report Outgoing</a></li>
      <li><a href="index.php?pagename=stock_report_incoming">Stock Report Incoming</a></li>
    </ul>
  </li>
  <li><a class="arrow"><img src="images/newicon/nav-icon-history.png" width="14" style="margin-top: -2px;" /> Purchasing</a>
    <ul>
      <li><a href="index.php?pagename=edit_po">Purchase Manager</a></li>
      <li><a href="index.php?pagename=vendor_manager">Vendor Master</a></li>
      <li><a href="index.php?pagename=add_vendor_product_list">Vendor Products Manager</a></li>
      <li><a href="index.php?pagename=purchase_order">Create New PO</a></li>
      <!--<li><a href="index.php?pagename=do_manager">DO Manager</a></li>-->
    </ul>
  </li>
  <li><a class="arrow" href="index.php?pagename=tendering&action=ten"><img src="images/newicon/Info-Icon.png" width="14" style="margin-top: -2px;" /> Tendering</a>
    <ul>
      <li><a href="index.php?pagename=add_tender&action=add_new">Upload Tender</a></li>
      <li><a href="index.php?pagename=view_open_tender&action=view_open_tender">View Open Tenders</a></li>
      <li><a href="index.php?pagename=view_close_tender&action=view_close_tender">View Close Tenders</a></li>
      <li><a href="index.php?pagename=view_emd_stat&action=view_emd_stat">View EMD Status</a></li>
      <li><a href="index.php?pagename=view_sd_stat&action=view_sd_stat">View SD Status</a></li>
    </ul>
  </li>
  <li><a class="arrow" href="index.php?pagename=kc_home"><img src="images/newicon/tender.png" width="14" style="margin-top: -2px;" /> Knowledge Center</a>
    <ul>
      <li> <a href="index.php?pagename=add_fol_cat_new&action=add_new">Add Folder/Document</a></li>
      <li> <a href="index.php?pagename=edit_fol_cat&action=edit">Edit Folder/Document</a></li>
      <li> <a href="index.php?pagename=kc_home">View/Download Folder/Document</a></li>
    </ul>
  </li>
  <li><a class="arrow" href="index.php?pagename=ac_payble&action=view"><img src="images/newicon/automatic-payment-plan.png" width="14" style="margin-top: -2px;" /> A/c Payable</a>
    <ul>
      <li><a href="index.php?pagename=cus_payments&action=view_payments">Pending Customers Payments</a></li>
      <li><a href="index.php?pagename=ven_payments&action=view_payments">Pending Vendors Payments</a></li>
      <li><a href="index.php?pagename=c_form_payments&action=view_payments">Pending C Form Receivable</a></li>
      <li><a href="index.php?pagename=c_form_ven&action=view_payments">Vendor C Form Payable</a></li>
      <li><a href="index.php?pagename=view_emd_stat&action=view_emd_stat">Pending EMD Status</a></li>
      <li><a href="index.php?pagename=view_sd_stat&action=view_sd_stat">Pending SD Status</a></li>
      <li><a href="index.php?pagename=view_pi_stat">Proforma Invoices</a></li>
    </ul>
  </li>
  <li><a class="arrow"><img src="images/newicon/icon-crm.png" width="14" style="margin-top: -2px;" /> HR Management</a>
    <ul>
      <li><a href="index.php?pagename=manage_designation" class="arrow">Designation Manager</a></li>
      <li><a href="index.php?pagename=admin_manager" class="arrow">Employee Manager</a></li>
      <li><a href="index.php?pagename=view_attendance">Attendance Manager</a></li>
      <li><a href="index.php?pagename=manage_team">Team Manager</a></li>
    </ul>
  </li>
  <li><a class="arrow"><img src="images/newicon/Vista_icons_08.png" width="14" style="margin-top: -2px;" /> Admin Management</a>
    <ul>
      <li><a href="index.php?pagename=manage_product" class="arrow">Product Manager</a></li>
      <li><a href="index.php?pagename=manage_application" class="arrow">Product Category Manager</a></li>
      <li><a href="index.php?pagename=customer_manager" class="arrow">Customer Database Manager</a></li>
      <li><a href="index.php?pagename=manage_enq_source">Enquiry Source Manager</a></li>
      <li><a href="index.php?pagename=offer_id_generation">Offer ID Generation</a></li>
      <li><a href="index.php?pagename=manage_co_branch_address" class="arrow">Company Branch Manager</a></li>
      <li><a href="index.php?pagename=manage_co_bank_address" class="arrow">Company Bank Manager</a></li>
      <li><a href="index.php?pagename=manage_designation_comp" class="arrow">Company Designation Manager</a></li>
      <li><a href="index.php?pagename=manage_cust_segment" class="arrow">Customer Segment Manager</a></li>
      <li><a href="index.php?pagename=manage_country" class="arrow">Country Manager</a></li>
      <li><a href="index.php?pagename=zones_manager" class="arrow">Zone/State Manager</a></li>
      <li><a href="index.php?pagename=cities_manager" class="arrow">City Manager</a></li>
      <li><a href="index.php?pagename=manage_courier_name" class="arrow">Manage Courier Name</a></li>
      <li><a href="index.php?pagename=manage_mode_master" class="arrow">Mode Manager</a></li>
      <li><a href="index.php?pagename=location_manager" class="arrow">Stock Location Manager</a></li>
      <li><a href="index.php?pagename=alarm_email_configuration&action=edit&setX=1" class="arrow">Alarm Email Configuration</a></li>
      <li><a href="index.php?pagename=manage_warranty_master" class="arrow">Warranty Manager</a></li>
	  <li><a href="index.php?pagename=manage_calibration_master" class="arrow">Calibration Manager</a></li>                              
  	  <li><a href="index.php?pagename=manage_hvc_master" class="arrow">HVC Manager</a></li>                              
      <li><a href="index.php?pagename=manage_supply_orders_payment_terms" class="arrow">Supply Order Payment Terms</a></li>
      <li><a href="index.php?pagename=manage_supply_orders_delivery_terms" class="arrow">Supply Order Delivery Terms</a></li>      
	  <li><a href="index.php?pagename=manage_service_orders_payment_terms" class="arrow">Service Order Payment Terms</a></li>
      <li><a href="index.php?pagename=manage_product_upc" class="arrow">UPC Code Manager</a></li>      
      <li><a href="index.php?pagename=manage_product_type_master" class="arrow">Product Type Manager</a></li>
	  <li><a href="index.php?pagename=manage_product_type_class_master" class="arrow">Product Class Manager</a></li>  
  	  <li><a href="index.php?pagename=manage_vendor_confirmation_email" class="arrow">Vendor Confirmation Mail Manager</a></li>    
  	  <li><a href="index.php?pagename=po_approval_email_configuration" class="arrow">Po Approval Email Configuration</a></li> 
      <li><a href="index.php?pagename=manage_key_customer_master" class="arrow">Key Customer Master</a></li>          
      <li><a href="index.php?pagename=manage_tasktype_master" class="arrow">Task Type Manager</a></li>          
      <li><a href="index.php?pagename=manage_opportunity_value_master" class="arrow">Opportunity Value Manager</a></li>          
      
      </ul>
  </li>
  
  
  
  <li><a class="arrow"><img src="images/newicon/service_icon.png" width="14" style="margin-top: -2px;" /> Service Support</a>
    <ul>
      <li><a href="https://www.stanlay.in/warranty_support/admin/" class="arrow" target="_blank">Warranty Support</a></li>
      <li><a href="index.php?pagename=out_of_warranty_manager" class="arrow">Out of Warranty</a></li>
      <li><a href="index.php?pagename=out_of_calibration_manager" class="arrow">Out of Calibration</a></li>
    </ul>
  </li>
  
  
</ul>