<?php
error_reporting(E_ALL);
require_once '../vendor/autoload.php';
include("../includes1/function_lib.php");
/*$onesheet = new \OneSheet\Writer('/optional/fonts/directory');
$onesheet->addRow(array('hello', 'world'));
$onesheet->addRow(array('Rumit', 'Verma'));
$onesheet->writeToFile('hello_world.xlsx');*/
		//$s->ExcelExpo($sql_query);

//$sql_query = "SELECT * from tbl_inventory where 1=1 AND YEAR( date_ordered ) IN ( 2019 ) and TRIM(pro_model) = 'st-sc' order by date_ordered desc"; //exit;
		//$s->ExcelExpo($sql_query);


use OneSheet\Style\BorderStyle;
use OneSheet\Style\Style;
use OneSheet\Style\Styler;
if(isset($_SESSION["ELqueryX_inventory"]))
	{
$sql_query = $_SESSION["ELqueryX_inventory"]; //exit;

// create a header style
$headerStyle = (new \OneSheet\Style\Style());
	$headerStyle->setFontName('Arial');
	$headerStyle->setFontSize(13);
    $headerStyle->setFontBold();
    $headerStyle->setFontColor('FFFFFF');
    $headerStyle->setFillColor('777777');
	//$headerStyle->setBorderLeft(DOUBLE, '000000');	

// create a data style
$dataStyle1 = (new \OneSheet\Style\Style());
    $dataStyle1->setFontName('Arial');
    $dataStyle1->setFontSize(11);
	$dataStyle1->setFillColor('f6f6f6'); //grey light
	//$dataStyle1->setBorderLeft(DOUBLE, '000000');
// create a second data style
$dataStyle2 = (new \OneSheet\Style\Style());
    $dataStyle2->setFontName('Arial');
    $dataStyle2->setFontSize(11);
    $dataStyle2->setFillColor('f7efee'); //red light
	//$dataStyle2->setBorderLeft(DOUBLE, '000000');	
	
// create a third data style
$dataStyle3 = (new \OneSheet\Style\Style());
    $dataStyle3->setFontName('Arial');
    $dataStyle3->setFontSize(11);
    $dataStyle3->setFillColor('ddefff');	//blue light
 //   $dataStyle3->setBorderLeft(DOUBLE, '000000');

// create a fourth data style
$dataStyle4 = (new \OneSheet\Style\Style());
   $dataStyle4->setFontName('Arial');
   $dataStyle4->setFontSize(11);
   $dataStyle4->setFillColor('e9fbf2');	//green light
  // $dataStyle4->setBorderLeft(DOUBLE, '000000');

// prepare some dummy header data
//$dummyHeader = array('Strings', 'Ints', 'Floats', 'Dates', 'Times', 'Uids');
$dummyHeader = array('Stock Type', 'EID#', 'Month', 'Customer Name', 'Invoice/PO No', 'Item Code', 'Product Name', 'Quantity');
// prepare some dummy data
$dummyData = array();
$rs=mysqli_query($GLOBALS["___mysqli_ston"],$sql_query);
while($row=mysqli_fetch_object($rs))
{
	$lead_id 			= $s->get_lead_id_from_offer($row->orders_id);
	$enq_id	 			= $s->get_enq_id($lead_id);
	if($row->stock_type=='outgoing')
	{
	$invoice_or_po_no	= $s->Get_invoice_no($row->orders_id);
	}
	else
	{
	$invoice_or_po_no	= $row->orders_id;
	}
	if($row->stock_type=='outgoing')
	{
	$co_name			= $s->company_name($row->customers_id);
	}
	else
	{
	$co_name			= $s->vendor_name($row->customers_id);
	}	
	
//	print_r($row);exit;
	$dummyData[] = array(
						 $row->stock_type,
						 $enq_id, 
						 date("M Y", strtotime($row->date_ordered)),
						 $co_name,
						 $invoice_or_po_no,
						 $row->pro_model,
						 $row->pro_name,
						 $row->pro_quantity,
		  		   );		   
}
/*echo "<pre>";

print_r($dummyData);*/
/*for ($i = 1; $i <= 100; $i++) {
    $dummyData[] = array(
        substr(md5(microtime()), rand(11,22)),
        rand(333,333333),
        microtime(1),
        date(DATE_RSS, time() + $i*60*60*24),
        date('H:i:s', time() + $i),
        uniqid(null, 1)
    );
}*/

// create new OneSheet instance
$onesheet = new \OneSheet\Writer();

// add header with style
$onesheet->addRow($dummyHeader, $headerStyle);

// freeze everything above cell A2 (the first row will be frozen)
$onesheet->setFreezePaneCellId('A2');

// enable autosizing of column widths and row heights
$onesheet->enableCellAutosizing();

// add dummy data row by row and switch between styles

//echo "<pre>";
//print_r($dummyData);
foreach ($dummyData as $key=> $data) {
   /* if ($key % 2) {
        $onesheet->addRow($data, $dataStyle1);
    } else {
        $onesheet->addRow($data, $dataStyle2);
    }*/
	
 //echo $data[0];

	
	if ($data[0]=='Incoming')
	{
		$onesheet->addRow($data, $dataStyle4);//green- incoming
	}
	else {
        $onesheet->addRow($data, $dataStyle2);//red outgoing
    }
	
	/*
	if ($data[7]=='2')
	{
		$onesheet->addRow($data, $dataStyle1);
	}
	
	
	
	else if ($data[7]=='6')
	{
		$onesheet->addRow($data, $dataStyle3);
	}
	
	
	else if ($data[7]=='10')
	{
		$onesheet->addRow($data, $dataStyle2);
	}
	
	else {
        $onesheet->addRow($data, $dataStyle4);
    }*/
	
}
// ignore the coming rows for autosizing
$onesheet->disableCellAutosizing();
// add an oversized dummy row
$onesheet->addRow(array('','','','','',''));
$onesheet->addRow(array('LEGENDS',':','','','',''));
$onesheet->addRow(array('Outgoing Stock'),$dataStyle4);
$onesheet->addRow(array('Incoming Stock'),$dataStyle2);
// add the all the dummy rows once more, because we can =)
//$onesheet->addRows($dummyData);
// Override column widths for columns 6, 7, 8 (column 0 is the first)
$onesheet->setFixedColumnWidths(array(5 => 10, 6 => 10, 7 => 10));
// write everything to the specified file
//$fileName = "Inventory_report_As_on_dated_:" . date('d-M-Y ').".xlsx";
//$fileName =$onesheet->writeToFile(str_replace('.php', ' As_on_dated'.date('d-M-Y ').''.'_onesheet.xlsx', __FILE__));
//$onesheet->writeToFile(str_replace('.php', ' As_on_dated'.date('d-M-Y ').''.'_onesheet.xlsx', __FILE__));
//	header("Content-Disposition: attachment; filename=\"$fileName\"");
//header("Content-Type: application/vnd.ms-excel");
//$onesheet->writeToFile(str_replace('.php', '_As_on_dated'.date('d-M-Y').''.'_onesheet.xlsx', __FILE__));
$onesheet->writeToBrowser('inventory_report_As_on_dated'.date('d-M-Y').'_onesheet.xlsx');
	
	header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Content-Type: application/force-download");
    header("Content-Type: application/octet-stream");
    header("Content-Type: application/download");
	header("Content-Disposition: attachment; filename=inventory_report_As_on_dated".date('d-M-Y')."_onesheet.xlsx");
    header("Content-Transfer-Encoding: binary ");
	}
	
	else
	{
			$s->pageLocation($_SERVER['HTTP_REFERER']."&error=xlsError") ;
	}
	
?>