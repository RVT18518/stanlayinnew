// JavaScript Document
function mmLoadMenus() 
{
	if (window.mm_menu_1205180357_0)return;
//Configuration Menu     - ID = 0;
	window.mm_menu_1205180357_0 = new Menu("root",150,23,"",12,"#FFFFFF","#363636","#58582C","#D8D8B1","left","middle",2,0,1000,-5,7,true,false,true,5,true,true);
// submenu Option
	mm_menu_1205180357_0.addMenuItem("General&nbsp;Configuration","location='index.php?pagename=general_configuration&action=edit'");
  	//mm_menu_1205180357_0.addMenuItem("Admin&nbsp;Manager","location='index.php?pagename=admin_manager'");
// Submenue property
  	mm_menu_1205180357_0.hideOnMouseOut=true;
  	mm_menu_1205180357_0.bgColor='#000000';
  	mm_menu_1205180357_0.menuBorder=0;
  	mm_menu_1205180357_0.menuLiteBgColor='#0D409A';
  	mm_menu_1205180357_0.menuBorderBgColor='#0D409A';
//////////////////////////////////////////////////////////////////////////

//Product Menu     - ID = 03;
	window.mm_menu_1205180357_03 = new Menu("root",180,23,"",12,"#FFFFFF","#363636","#58582C","#D8D8B1","left","middle",2,0,1000,-5,7,true,false,true,5,true,true);
// submenu Option
	mm_menu_1205180357_03.addMenuItem("Product&nbsp;Manager","location='index.php?pagename=manage_product'");
	mm_menu_1205180357_03.addMenuItem("Category&nbsp;Manager","location='index.php?pagename=manage_category'");
	//mm_menu_1205180357_03.addMenuItem("Application&nbsp;Category&nbsp;Manager","location='index.php?pagename=manage_app_category'");
	//mm_menu_1205180357_03.addMenuItem("Product&nbsp;Application&nbsp;Manager","location='index.php?pagename=manage_product_app'");
	//mm_menu_1205180357_03.addMenuItem("Customer&nbsp;Reference&nbsp;Manager","location='index.php?pagename=manage_product_customer'");
	//mm_menu_1205180357_03.addMenuItem("Installation&nbsp;Manager","location='index.php?pagename=manager_installation'");	

//////////////////////////////////////////////////////////////////////////
//Newsletter Menu - ID = 04;
	window.mm_menu_1205180357_04 = new Menu("root",180,23,"",12,"#FFFFFF","#363636","#58582C","#D8D8B1","left","middle",2,0,1000,-5,7,true,false,true,5,true,true);
// submenu Option
	
	mm_menu_1205180357_04.addMenuItem("Newsletter&nbsp;Category&nbsp;Manager","location='index.php?pagename=manage_newsletter_category'");
	mm_menu_1205180357_04.addMenuItem("Newsletter&nbsp;Manager","location='index.php?pagename=manage_newsletter'");
	mm_menu_1205180357_04.addMenuItem("Newsletter&nbsp;Users","location='index.php?pagename=NewsLetter_Subscriber_Manager'");


//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//Downloads Menu - ID = 04;
	window.mm_menu_1205180357_05 = new Menu("root",130,23,"",12,"#FFFFFF","#363636","#58582C","#D8D8B1","left","middle",2,0,1000,-5,7,true,false,true,5,true,true);
// submenu Option
	mm_menu_1205180357_05.addMenuItem("Downloads&nbsp;Manager","location='index.php?pagename=manage_downlods'");

/////////////////////////////////////////////////////////////////////////////
	window.mm_menu_1205180357_06 = new Menu("root",130,23,"",12,"#FFFFFF","#363636","#58582C","#D8D8B1","left","middle",2,0,1000,-5,7,true,false,true,5,true,true);
// submenu Option
	mm_menu_1205180357_06.addMenuItem("Jobs&nbsp;Manager","location='index.php?pagename=manager_job'");
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
	window.mm_menu_1205180357_07 = new Menu("root",230,23,"",12,"#FFFFFF","#363636","#58582C","#D8D8B1","left","middle",2,0,1000,-5,7,true,false,true,5,true,true);
// submenu Option
//	mm_menu_1205180357_07.addMenuItem("Press&nbsp;Release&nbsp;Manager","location='index.php?pagename=manager_press_release'");
//	mm_menu_1205180357_07.addMenuItem("Media&nbsp;Coverage&nbsp;Manager","location='index.php?pagename=manager_media_coverage'");
	mm_menu_1205180357_07.addMenuItem("Corporate&nbsp;Broucher","location='index.php?pagename=manage_corporate_videos'");
	//mm_menu_1205180357_07.addMenuItem("Technical&nbsp;Articales&nbsp;Manager","location='index.php?pagename=manage_technical_artical'");

	mm_menu_1205180357_07.addMenuItem("Corporate&nbsp;Presentation","location='index.php?pagename=manager_product_seminar_presentation'");
/////////////////////////////////////////////////////////////////////////////

//Tools Menu     - ID = 01;
	window.mm_menu_1205180357_01 = new Menu("root",140,23,"",12,"#FFFFFF","#363636","#58582C","#D8D8B1","left","middle",2,0,1000,-5,7,true,false,true,5,true,true);
// submenu Option
  	mm_menu_1205180357_01.addMenuItem("CMS","location='index.php?pagename=cms_manager'");
	//mm_menu_1205180357_01.addMenuItem("Newsletter&nbsp;Users","location='index.php?pagename=NewsLetter_Subscriber_Manager'");
	mm_menu_1205180357_01.addMenuItem("News&nbsp;Manager","location='index.php?pagename=manage_news'");
  	mm_menu_1205180357_01.addMenuItem("Event&nbsp;Manager","location='index.php?pagename=manager_event'");
//	mm_menu_1205180357_01.addMenuItem("Create&nbsp;Album","location='index.php?pagename=manage_gallery'");
	mm_menu_1205180357_01.addMenuItem("Gallery&nbsp;Manager","location='index.php?pagename=manage_gallery'");
	mm_menu_1205180357_01.addMenuItem("Testimonials&nbsp;Manager","location='index.php?pagename=manage_testimonials'");
//	mm_menu_1205180357_01.addMenuItem("News&nbsp;&amp;&nbsp;Event&nbsp;Manager","location='index.php?pagename=manage_news_event'");
//	mm_menu_1205180357_01.addMenuItem("Popup&nbsp;Manager","location='index.php?pagename=manage_popup'");
//	mm_menu_1205180357_01.addMenuItem("FAQ","location='index.php?pagename=manager_faq'");
//	mm_menu_1205180357_01.addMenuItem("Server&nbsp;Info","location='index.php?pagename=serverinfo'");

// Submenue property
  	mm_menu_1205180357_01.hideOnMouseOut=true;
  	mm_menu_1205180357_01.bgColor='#A1ABC7';
  	mm_menu_1205180357_01.menuBorder=0;
  	mm_menu_1205180357_01.menuLiteBgColor='#0D409A';
  	mm_menu_1205180357_01.menuBorderBgColor='#0D409A';

//My Account    - ID = 08;
	window.mm_menu_1205180357_02 = new Menu("root",120,23,"",12,"#FFFFFF","#363636","#58582C","#D8D8B1","left","middle",2,0,1000,-5,7,true,false,true,5,true,true);
// submenu Option
  	mm_menu_1205180357_02.addMenuItem("My&nbsp;Account&nbsp;Details","location='index.php?pagename=my_details_change&action=edit'");
  	mm_menu_1205180357_02.addMenuItem("Change&nbsp;Password","location='index.php?pagename=change_password'");

// Submenue property
  	mm_menu_1205180357_02.hideOnMouseOut=true;
  	mm_menu_1205180357_02.bgColor='#ffffff';
  	mm_menu_1205180357_02.menuBorder=0;
  	mm_menu_1205180357_02.menuLiteBgColor='#ffffff';
  	mm_menu_1205180357_02.menuBorderBgColor='#ffffff';	

mm_menu_1205180357_0.writeMenus();
}