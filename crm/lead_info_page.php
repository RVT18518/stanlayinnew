<?php
	include_once("../includes1/modulefunction.php");

		$admin_id	 = $_SESSION["AdminLoginID_SET"];
		$data_action = $_REQUEST["action"];
		$pcode 		 = $_REQUEST["pcode"];
		$pcode_emp	 = $_REQUEST["pcode_emp"];
		$comp_name	 = $_REQUEST['comp_id'];
		
	
		$rs		  				= $front->getData_with_condition('tbl_lead','id',$pcode);
		$row	  				= mysqli_fetch_object($rs);
			
		$lead_desc          	= $row->lead_desc; 
		$ref_source          	= $row->ref_source;
		$date_opened          	= $row->date_opened; 
		$cust_segment          	= $row->cust_segment;
		$desc_details          	= $row->desc_details; 
		$dec_time_frame        	= $row->dec_time_frame;
		$acc_manager          	= $row->acc_manager;
		$status          		= $row->status;
		$priority          		= $row->priority;
		$app_cat_id  			= $row->app_cat_id; 
		$estimated_value  		= $row->estimated_value; 

		
		$lead_fname          	= $row->lead_fname; 
		$lead_lname          	= $row->lead_lname; 
		
		$lead_contact_address1 	= $row->lead_contact_address1;
		$lead_contact_address2 	= $row->lead_contact_address2; 
		$salutation          	= $row->salutation;
		$lead_title          	= $row->lead_title;
		$lead_contact_address3 	= $row->lead_contact_address3; 
		$lead_contact_address4 	= $row->lead_contact_address4; 
		$lead_email          	= $row->lead_email; 
		$lead_phone          	= $row->lead_phone; 
		
		$lead_contact_city     	= $row->lead_contact_city; 
		$lead_contact_zip_code 	= $row->lead_contact_zip_code; 
		$lead_area_code       	= $row->lead_area_code; 
		$lead_fax          		= $row->lead_fax; 
		$state    				= $row->lead_contact_state; 
		$lead_contact_country  	= $row->lead_contact_country; 
		
		$no_of_emp				= $row->no_of_emp;
		$comp_name				= $row->comp_name;
		
		$comp_website			= $row->comp_website;
		$comp_revenue			= $row->comp_revenue;
		$cust_segment			= $row->cust_segment;
			
		$data_action 			= "update";
		
		

	
	

?>
<style>
body {
	padding: 1px;
	margin: 0px;
	font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	font-size: 12px;
}
#pageheader {
	background: #FFFFFF;
}
#pageheader h1 {
	font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	font-size: 22px;
	color: #FFF;
	font-weight: bold;
	margin: 20px;
}
#topmenu {
	background-color: #FFFFFF;
	/*background-color: #CCCCCC;*/
	margin-top: 1px;
	height: 25px;
}
#topmenu a {
	color: #FFF;
	text-decoration: none;
	font-size: 12px;
	font-weight: bold;
	font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	display: block;
	float: left;
	margin-left: 20px;
	line-height: 25px;
	padding: 0px 8px 0px 8px;
}
#topmenu a:hover {
	color: #FFF;
	background-color: #CCCCCC;
	text-decoration: none;
}
#pagefooter {
	background-color: #58595B;/*background-color: #DDDDDD;*/
}
#pagefooter h3 {
	font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	font-size: 12px;
	color: #FFF;
	font-weight: normal;
	margin: 5px 20px 5px 20px;
}
.categoryhead {
	background-color: #58595B;
	color: #FFFFFF;
}
.categorylist {
	background-color: #FFEAEA;
	font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	font-size: 12px;
	line-height: 20px;
}
.categorylist .level1 {
	padding-left: 10px;
}
.categorylist a {
	color: #4E4E4E;
	text-decoration: none;
 margin::0px 2px 0px 2px;
}
.categorylist .level2 {
	padding-left: 20px;
}
.categorylist .level3 {
	padding-left: 30px;
}
.categorylist a:hover {
	color: #58595B;
	text-decoration: underline;
}
select {
	font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	font-size: 12px;
}
.pagehead {
	background: #58595B;
	color: #FFFFFF;
	font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	font-size: 12px;
	font-weight: bold;
	border-bottom: 1px solid #A1ABC7;
	margin-bottom: 10px;
	height: 25px;
	padding-left: 10px;
	background-image: url(../images/report_back.png);
	background-repeat: repeat-x;
}
.pageheadTop {
	color: #58595B;
	font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	font-size: 18px;
	font-weight: bold;
	padding: 20px 0px 10px 15px;
}
.pagecontent a {
	color: #333;
	text-decoration: none;
}
#pagebody ul li {
	float: left;
	list-style-type: none;
}
#pagebody ul {
	margin: 0px;
	padding: 20px;
}
#pagebody .pagecontent {
	padding-left: 0px;
}
.pagecontent a:hover {
	color: #333;
	text-decoration: underline;
}
.head {
	font: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #333;
	background-color: #F2F2F2;
	/*background:#ffffff;*/
	height: 28px;
}
.head_offer {
	font: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	font-size: 12px;
	font-weight: bold;
	color: #DC241A;
	background-color: #f6f6f6;
	/*	background-color:#FFD7B9;*/
	/*background:#ffffff;*/
	height: 28px;
}
.head_crm {
	font: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	font-size: 12px;
	font-weight: bold;
	color: #333;
	background-color: #F2F2F2;/*background:#ffffff;*/

}
.text {
	font: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	font-size: 12px;
	color: #535353;
	background: #FFF;
	height: 28px;
	border-top-style: none;
	border-right-style: none;
	border-bottom: solid 1px #ffffff;
	border-left-style: none;
}
.text a {
	color: #58595B;
	text-decoration: none;
}
.text a:hover {
	color: #58595B;
	text-decoration: underline;
}
form .inpuTxt {
	font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	font-size: 12px;
	color: #666;
	border: 1px solid #CCC;
	width: 250px;
	padding: 2px;
}
form .inpuTxt1 {
	font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	font-size: 12px;
	color: #666;
	border: 1px solid #CCC;
	width: 256px;
	padding: 2px;
}
form .inpuTxtText {
	font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	font-size: 12px;
	color: #666;
	border: 1px solid #CCC;
	width: 300px;
	padding: 2px;
}
form .inpuTxtSelect {
	font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	font-size: 12px;
	color: #666;
	border: 1px solid #CCC;
}
form input {
	font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	font-size: 10px;
	color: #666;
}
form label {
	font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	font-size: 12px;
	color: #666;
}
a.nlinks {
	background-image: url(../images/but-bg.jpg);
	background-repeat: repeat-x;
	background-color: #d8dfeb;
	background-position: top;
	font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	font-size: 12px;
	font-weight: bold;
	color: #333;
	border: 1px solid #CCC;
	height: 22px;
}
.btn {
	font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	font-size: 12px;
	border: double 2px #FFFFFF;
	color: #FFFFFF;
	background-color: #CCCCCC;
	cursor: pointer
}
.inputton_green {
	font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	font-size: 13px;
	font-weight: bold;
	border: double 5px #efefef;
	padding: 3px 20px 3px 20px;
	color: #FFFFFF;
	background-color: #338533;
	cursor: pointer;
}
.inputton_green:hover {
	font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	font-size: 13px;
	font-weight: bold;
	border: double 5px #efefef;
	padding: 3px 20px 3px 20px;
	color: #000;
	background-color: #ca4c00;
	cursor: pointer
}
.inputton {
	font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	font-size: 13px;
	font-weight: bold;
	border: double 5px #efefef;
	padding: 3px 20px 3px 20px;
	color: #FFFFFF;
	background-color: #c60006;
	cursor: pointer;
}
.inputton:hover {
	font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	font-size: 13px;
	font-weight: bold;
	border: double 5px #efefef;
	padding: 3px 20px 3px 20px;
	color: #fff;
	background-color: #ff0000;
	cursor: pointer
}
/*form .inputton {
	background-image: url(../images/but-bg.jpg);
	background-repeat: repeat-x;
	background-color: #d8dfeb;
	background-position: top;
	font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	font-size: 12px;
	font-weight: bold;
	color: #333;
	border: 1px solid #ccc;
	/*height: 20px;*/
	/*padding:1px;
	cursor:pointer;
}*/

.login_head {
	background: #58595B;
	color: #FFFFFF;
	font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
}
.redstar {
	font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	color: #F00;
}
.pHeadLine {
	background: #CCCCCC;
	height: 3px;
}
.tblBorder {
	border: 1px solid #CCCCCC;
}
.topRight {
	font-size: 12px;
	font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	color: #FFFFFF;
	text-align: right;
	padding-right: 20px;
}
.topRight a {
	color: #CCCCCC;
	text-decoration: underline;
}
.topRight a:hover {
	color: #FFFFFF;
	text-decoration: none;
}
.saperator {
	padding: 5px;
}
.headLink {
	font: Calibri;
	font-size: 11px;
	color: #ffffff;
	font-weight: bold;
}
.headLink ul {
	margin: 0px;
	padding: 0px;
	list-style: none;
}
.headLink ul li {
	display: block;
	float: right;
	color: #ffffff;
	/*background:url(../images/linkbg.gif) repeat-x;*/
	background-color: #666600;
	margin-right: 10px;
	padding: 5px 15px 5px 15px;
	border-radius: 2px;
	border: 3px solid #fff;
}
.headLink ul li:hover {
	display: block;
	float: right;
	color: #ffffff;
	/*background:url(../images/linkbg.gif) repeat-x;*/
	background-color: #ED3237;
	margin-right: 10px;
	padding: 5px 15px 5px 15px;
	border-radius: 2px;
	border: 3px solid #fff;
}
.headLink ul li a {
	color: #ffffff;
	text-decoration: none;
}
.headLink ul li a:hover {
	color: #ffffff;
	text-decoration: none;
}
.pad {
	padding-left: 15px;
}
.error {
	font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	font-size: 12px;
	color: #F00;
	font-weight: normal;
	display: block;
	border: 1px solid #FF9191;
	background-image: url(../images/error.gif);
	background-repeat: no-repeat;
	background-color: #FFC6C6;
	vertical-align: middle;
	height: 18px;
	margin: 0px;
	padding-left: 30px;
	padding-top: 7px;
}
.success {
	font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	font-size: 12px;
	color: #060;
	font-weight: normal;
	display: block;
	border: 1px solid #B1CFB1;
	background-image: url(../images/success.gif);
	background-repeat: no-repeat;
	background-color: #D1E3D1;
	vertical-align: middle;
	height: 18px;
	margin: 0px;
	padding-left: 30px;
	padding-top: 7px;
}
.oldpr {
	text-decoration: line-through;
}
.newpr {
	color: #58595B;
	font-weight: bold;
}
#dhtmltooltip {
	position: absolute;
	width: 250px;
	border: 2px solid black;
	padding: 2px;
	background-color: lightyellow;
	visibility: hidden;
	z-index: 100;
	/*Remove below line to remove shadow. Below line should always appear last within this CSS*/
	filter: progid:DXImageTransform.Microsoft.Shadow(color=gray, direction=135);
}
.text_front {
	font: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	font-size: 12px;
	color: #535353;
	background: #ffffff;
	height: 28px;
	border-top-style: none;
	border-right-style: none;
	border-bottom: solid 1px #ffffff;
	border-left-style: none;
}
.bdr_top {
	border-top: 1px solid #e6e6e6;
}
.bdr_bottom {
	border-bottom: 1px solid #e6e6e6;
}
.bdr_right {
	border-right: 1px solid #e6e6e6;
}
.bdr_left {
	border-left: 1px solid #e6e6e6;
}
a {
	margin: 0px;
	padding: 0px;
	color: #CCCCCC;
	text-decoration: none;
	outline: none;
}
a:hover {
	text-decoration: none;
	outline: none;
	border: none;
	margin: 0px;
	padding: 0px;
	color: #58595B000;
}
#next_link {
	background-color: #58595B;
	/*background-color: #CCCCCC;*/
	height: 25px;
}
#next_link a {
	color: #FFF;
	text-decoration: none;
	font-size: 12px;
	font-weight: bold;
	font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	display: block;
	padding: 0px 8px 0px 8px;
}
#next_link a:hover {
	color: #FFF;
	background-color: #58595B;
	text-decoration: none;
}
/*********************LEFT MENU CSS STARTS *******************************/


ul#navigation {
	position: fixed;
	margin: 0px;
	padding: 0px;
	top: 97px;
	left: 0px;
	list-style: none;
	z-index: 9999;/*	border:solid 1px #FF0000;*/
}
ul#navigation li {
	width: 60px; /* 100px*/
}
ul#navigation li a {
	display: block;
	margin-left: -2px;
	width: 100px;
	height: 70px;
	background-color: #fff;
	background-repeat: no-repeat;
	background-position: center center;
	border: 1px solid #AFAFAF;
	-moz-border-radius: 0px 10px 10px 0px;
	-webkit-border-bottom-right-radius: 10px;
	-webkit-border-top-right-radius: 10px;
	-khtml-border-bottom-right-radius: 10px;
	-khtml-border-top-right-radius: 10px;
    /*-moz-box-shadow: 0px 4px 3px #000;
    -webkit-box-shadow: 0px 4px 3px #000;
    */
    /*opacity: 0.6;*/
    filter:progid:DXImageTransform.Microsoft.Alpha(opacity=100)!important;
}
ul#navigation .home a {
	background-image: url(../images/home.png);
}
ul#navigation .new_company a {
	background-image: url(../images/new-user2.png);
}
ul#navigation .lead_mgmt a {
	background-image: url(../images/lead.png);
}
ul#navigation .search a {
	background-image: url(../images/search.png);
}
ul#navigation .podcasts a {
	background-image: url(../images/sales-report.png);
}
ul#navigation .stock_mgmt a {
	background-image: url(../images/stock1.png);
}
ul#navigation .dely_challan a {
	background-image: url(../images/challen2.png);
}
ul#navigation .create_offer a {
	background-image: url(../images/stock2.png);
}
/***************Some missing CSS*****************************/
.inpuTxtsmall {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
	color: #666;
	border: 1px solid #CCC;
	width: 150px;
	padding: 2px;
}
.inpuTxtsmallfree {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
	color: #666;
	border: 1px solid #CCC;
	padding: 2px;
}
.inpuTxtprice {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
	color: #666;
	border: 1px solid #CCC;
	width: 45px;
	padding: 2px;
}
.display_none {
	display: none
}
/*********************LEFT MENU CSS ENDS *******************************/

.bgred {
	background-color: #FF8080
}
.bgblue {
	background-color: #0000CC
}
.bgorange {
	background-color: #FC6
}
.bggreen {
	background-color: #060;
}
.bggreen a {
	color: #FFF;
}
.bggreen:hover a {
	color: #FFF;
}
.bggreen:hover {
	background-color: #690;
}
.inpuTxtSelect {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #666;
	border: 1px solid #CCC;
}
.search_top_text {
	padding: 0px;
	margin: 0px;
	border: none;
	border: 1px solid #aaa;
	height: 30px;
	text-align: left;
	width: 45%;
	padding-left: 3px
}
.search_top_select {
	padding: 3px;
	margin: 0px;
	border: none;
	border: 1px solid #aaa;
	height: 32px;
	text-align: left;
	width: 190px;
	vertical-align: top
}
.search_top_go {
	padding: 3px;
	margin: 0px;
	border: none;
	border: 1px solid #cbcbcb;
	height: 32px;
	text-align: center;
	width: 42px;
	vertical-align: top;
	font-weight: bold;
	background-color: #c60006;
	color: #fff;
	cursor: pointer
}
.search_top_go:hover {
	padding: 3px;
	margin: 0px;
	border: none;
	border: 1px solid #cbcbcb;
	height: 32px;
	text-align: center;
	width: 42px;
	vertical-align: top;
	font-weight: bold;
	background-color: #F00;
	color: #fff;
	cursor: pointer
}
.search_top_go1 {
	padding: 2px;
	margin: 0px;
	border: none;
	border: 1px solid #cbcbcb;
	height: 32px;
	text-align: center;
	vertical-align: top;
	font-weight: bold;
	background-color: #c60006;
	color: #fff;
	cursor: pointer;
	width: 100px;
	margin: auto;
	z-index: 1;
	position: absolute;
	margin-left: 5.5%;
	font-weight: bold;
	font-size: 14px;
	border-radius: 8px;
	border: 2px solid #933
}
.search_top_go1:hover {
	padding: 2px;
	margin: 0px;
	border: none;
	border: 1px solid #cbcbcb;
	height: 32px;
	text-align: center;
	vertical-align: top;
	font-weight: bold;
	background-color: #FF0000;
	color: #fff;
	cursor: pointer;
	width: 100px;
	margin: auto;
	z-index: 1;
	position: absolute;
	margin-left: 5.5%;
	font-weight: bold;
	font-size: 14px;
	border-radius: 8px;
	border: 2px solid #900
}
p.crm_home_tab {
	display: block;
	background-color: #ac0005;
	color: #fff;
	border: 2px solid #ccc;
	border-radius: 6px;
	font-family: Calibri;
	font-weight: bold;
	font-size: 14px;
	padding: 7px;
	margin-bottom: 15px;
	width: 55%;
	text-align: center;
	margin: auto;
	margin-bottom: 5px;
}
p.crm_home_tab:hover {
	display: block;
	background-color: #0099FF;
	color: #fff;
	border: 2px solid #ccc;
	border-radius: 6px;
	font-family: Calibri;
	font-weight: bold;
	font-size: 14px;
	padding: 7px;
	margin-bottom: 5px;
	cursor: pointer
}
#show_pqv {
	height: 900px;
	width: 100%;
	background-color: rgba(0,0,0,0.8);
	position: fixed;
	padding-top: 50px;
	margin-left: 0px;
	z-index: 2;
}
.tblBorder1 {
	border: 1px solid #CCCCCC;
}
table.tblBorder1 tr td {
	border-top: 1px solid #CCCCCC;
	border-right: 1px solid #CCCCCC;
}
table.tblBorder1 tr.text1 td {
	background-color: #fff;
	font-size: 13px;
}
table.tblBorder1 tr.text2 td {
	background-color: #f2f6fe;
	font-size: 13px;
}
table.tblBorder1 tr.text1:hover td {
	background-color: #fddcd8
}
table.tblBorder1 tr.text2:hover td {
	background-color: #fddcd8
}
.add_vendor {
	font-size: 15px;
	background-color: #349734;
	color: #fff;
	margin: auto;
	width: 225px;
	border: 2px solid #FFF
}
.add_vendor:hover {
	font-size: 15px;
	background-color: #38c638;
	color: #fff;
	margin: auto;
	width: 225px;
	border: 2px solid #FFF;
	text-decoration: none
}
#livesearch {
	z-index: 9999999999999;
	position: absolute;
	margin-top: 3px;
	margin-left: -5px;
	background-color: #fff;
}
#livesearch a.search_indi {
	font-family: Verdana, Geneva, sans-serif;
	font-weight: normal;
	color: #333;
	font-size: 15px;
	margin-left: 0px;
	padding-left: 5px;
	text-decoration: none;
}
</style>

<table width="50%" align="center" border="0" cellpadding="5" cellspacing="5" class="tblBorder">
  <tr>
    <td colspan="100%" class="pagehead pad">Customer Lead Details</td>
  </tr>
  <tr class="text">
    <td colspan="100%" align="left" valign="top"  class="pad head_crm">Lead Company Details </td>
  </tr>
  <tr >
    <td colspan="100%" align="left" valign="top" id="txtresultcomp"><table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr class="text">
           <td align="left" valign="top"  class="pad" colspan="2">
		  		<?php echo $front->company_name_return($comp_name); ?>
           </td>
        </tr>
        <tr>
          <td colspan="5"><table width="100%" border="0" cellpadding="5" cellspacing="0">
              <tr>
                <td colspan="4" align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="5">
                    <tr class="text">
                      <td colspan="2" align="left" valign="top"  class="pad head_crm">Lead Contact Details <br />
                        <?php //echo "<pre>"; print_r($_SESSION); ?></td>
                      <td colspan="2" align="left" valign="top"  class="pad head_crm"></td>
                    </tr>
                    <tr class="text">
                      <td align="left" valign="top"  class="pad"><strong>First Name</strong></td>
                      <td align="left" valign="top"  ><?php echo $lead_fname; ?>"</td>
                      <td align="left" valign="top" class="pad"  ><strong>Last Name</strong></td>
                      <td align="left" valign="top" class="pad"><?php echo $lead_lname; ?></td>
                    </tr>
                    <tr class="text">
                      <td align="left" valign="top" class="pad"><strong>Address</strong></td>
                      <td valign="top" colspan="3"><?php echo $lead_contact_address1;?></td>
                    </tr>
                    <tr class="text">
                      <td align="left" valign="top"  class="pad"><strong>Email</strong></td>
                      <td align="left" valign="top"  class="" colspan="3"><?php echo $lead_email;?></td>
                    </tr>
                    <tr class="text">
                      <td align="left" valign="top" class="pad"  ><strong>Contact Number</strong></td>
                      <td align="left" valign="top" class="" colspan="3"><?php echo $lead_phone; ?></td>
                    </tr>
                    <tr class="text">
                      <td align="left" valign="top"  class="pad"><strong>City</strong></td>
                      <td align="left" valign="top"><?php echo $lead_contact_city; ?></td>
                      <td align="left" valign="top" class="pad"><strong>Zip Code</strong></td>
                      <td valign="top" ><?php echo $lead_contact_zip_code; ?></td>
                    </tr>
                    <tr class="text">
                      <td align="left" valign="top"  class="pad" ><strong>State</strong></td>
                      <td align="left" valign="top" id="txtResult"  colspan="3"><?php echo $row_state->zone_name; 
	
?></td>
                    </tr>
                    
                      <tr class="text">
                      <td align="left" valign="top"  class="pad" ><strong>Customer Segment</strong></td>
                      <td align="left" valign="top" id="txtResult2"  colspan="3"><?php echo ucfirst(str_replace("_"," ",$cust_segment)); ?></td>
                    </tr>
                    <tr class="text">
                      <td align="left" valign="top"  class="pad" ><strong>Message Received from Customer</strong></td>
                      <td align="left" valign="top" id="txtResult2"  colspan="3"><?php echo $lead_desc; ?></td>
                    </tr>
                    
                     <tr class="text">
                      <td align="left" valign="top"  class="pad" ><strong>Product Interest</strong></td>
                      <td align="left" valign="top" id="txtResult2"  colspan="3"><?php echo $front->application_name_return($app_cat_id); ?></td>
                    </tr>
                    
                     <tr class="text">
                      <td align="left" valign="top"  class="pad" ><strong>Estimated Value</strong></td>
                      <td align="left" valign="top" id="txtResult2"  colspan="3"><?php echo $estimated_value; ?></td>
                    </tr>
                    
                  </table></td>
              </tr>
            </table></td>
        </tr>
        <tr class="text">
          <td colspan="100%" align="left" valign="middle"  class="" bgcolor="#666600" ><strong style="padding:10px; font-size:15px; color:#fff">Product Interested</strong></td>
        </tr>
        <tr class="text">
          <td align="left" valign="top" colspan="100%" width="100%"><?php $front->ShowShopCartAdmin1();?></td>
        </tr>
        <tr class="text">
          <td align="left" valign="top" class="pad"><strong>Decision Timeframe</strong></td>
          <td align="left" valign="top"><?php 
		  if($dec_time_frame=='15')
		  {
			  echo "Within 15 Days Urgent";
		  }
		  if($dec_time_frame=='60')
		  {
			  echo "Within 60 Days Priority";
		  }
		  if($dec_time_frame=='180')
		  {
			  echo "In 61 to 360 Days";
		  }
		  
		 // echo $dec_time_frame; ?></td>
        </tr>
        <tr class="text">
          <td align="left" valign="top" class="pad"><strong>Notes</strong></td>
          <td align="left" valign="top"><?php echo $desc_details; ?></td>
        </tr>
        <tr class="text">
          <td colspan="100%" align="left" valign="top"  class="pad head_crm"></td>
        </tr>
        <tr class="text">
          <td align="right" valign="top" colspan="100%"></td>
        </tr>
        <tr class="text">
          <td align="right" valign="top" colspan="100%"><a href="http://www.stanlay.in/crm/index.php?pagename=product_catalog&lead_id=<?php echo $pcode; ?>&acc_manager=<?php echo $acc_manager; ?>" style="text-decoration:none">
            <div style="background-color:#F00; padding:5px; width:150px; color:#fff; font-weight:bold; cursor:pointer; text-align:center; border:2px solid #030; font-size:15px;border-radius:5px; display:none">Create Offer</div>
            </a></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td></td>
  </tr>
</table>
