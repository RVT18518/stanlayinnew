<?php 
	$data_action = $_REQUEST['action'];
	$pcode		 = $_REQUEST["pcode"];
	$order 		 = $_REQUEST["order"];
	$order_by    = $_REQUEST["orderby"];

	if(!isset($_GET['pageno']))
	{ 
    	$page = 1; 
	} 
	else 
	{ 
    $page = $_GET['pageno']; 
	} 
	if(!isset($_GET['records']))
	{ 
    	$max_results = 100; 
	} 
	else 
	{ 
    $max_results = $_GET['records']; 
	} 
	$from = (($page * $max_results) - $max_results); 

	if($_REQUEST['action']=='update' ||$_REQUEST['action']=='insert')
	{
		$fileArray["application_name"]  		= addslashes($_REQUEST["application_name"]);
		$fileArray["cat_abrv"]      			= addslashes($_REQUEST["cat_abrv"]);
		$fileArray["sort_order"]  				= addslashes($_REQUEST["sort_order"]);
		$fileArray['tax_class_id'] 	 			= $_REQUEST['tax_class_id'];// now class id is rates id
		$fileArray['hsn_code'] 	 				= $_REQUEST['hsn_code'];// now class id is rates id
		$fileArray['cat_warranty'] 	 			= $_REQUEST['cat_warranty'];// added on 29-jan-2020
		$fileArray['parent_id1'] 				= $_REQUEST["parent_id1"];
		$fileArray["login_id"]      			= $_SESSION["AdminLoginID_SET"];
		$fileArray["application_status"]		= $_REQUEST["application_status"];
		$fileArray["application_description"] 	= addslashes($_REQUEST["application_description"]);
		$fileArray["application_added_by"] 		= $_SESSION["AdminLoginID_SET"];// use session variable for login id 
		$fileArray["update_product_warranty"]	= $_REQUEST["update_product_warranty"];
		
			
	}
	if($_REQUEST['action'] == 'ChangeStatus')
	{
		$rs_status = $s->getData_with_condition('tbl_application','application_id',$pcode);
		if(mysqli_num_rows($rs_status)>0)
		{
			$row_status = mysqli_fetch_object($rs_status);
			if($row_status->application_status == 'active')
			{
				$fileArray["application_status"] = 'inactive';
			}
			else if($row_status->application_status == 'inactive')
			{
				$fileArray["application_status"] = 'active';
			}
			$result = $s->editRecord('tbl_application',$fileArray,'application_id',$pcode);
		}
	}
	if( $_REQUEST['action'] == 'update' )
	{
			
		if($_FILES["application_logo"]["name"] !="")
		{
			$rs_del    			= $s->getData_with_condition('tbl_application','application_id',$pcode);
			$row_del   			= mysqli_fetch_object($rs_del);
		}		
		$result = $s->editRecord('tbl_application',$fileArray,'application_id',$pcode);
		
		
/***************************update this category products warranty starts*************************************/
if($_REQUEST["update_product_warranty"]=="Yes")
{
$sqlTotalProX = "select * from tbl_index_g2 where deleteflag = 'active' and pro_id = $pcode";		
			$rsTotalProX  = mysqli_query($GLOBALS["___mysqli_ston"],$sqlTotalProX);
			if(mysqli_num_rows($rsTotalProX)>0)
			{
				$j=0;
				while($rowTotalProX = mysqli_fetch_object($rsTotalProX))
				{
					$j++;
$fileArray_pro_warranty['pro_warranty'] = $_REQUEST['cat_warranty'];// added on 10-feb-2020
$result_update_pro_warranty 			= $s->editRecord('tbl_products',$fileArray_pro_warranty,'pro_id',$rowTotalProX->match_pro_id_g2);					
				}
			}
}
/***************************update this category products warranty ends***************************************/

/***************************update this category products calibration starts*************************************/
if($_REQUEST["update_product_calibration"]=="Yes")
{
$sqlTotalProX = "select * from tbl_index_g2 where deleteflag = 'active' and pro_id = $pcode";		
			$rsTotalProX  = mysqli_query($GLOBALS["___mysqli_ston"],$sqlTotalProX);
			if(mysqli_num_rows($rsTotalProX)>0)
			{
				
				while($rowTotalProX = mysqli_fetch_object($rsTotalProX))
				{
				$fileArray_pro_calibration['pro_calibration'] = $_REQUEST['cat_calibration'];// added on 10-feb-2020
	$result_update_pro_calibration 			= $s->editRecord('tbl_products',$fileArray_pro_calibration,'pro_id',$rowTotalProX->match_pro_id_g2);					
				}
			}
}
/***************************update this category products calibration ends***************************************/
	}
	if($_REQUEST['action'] == 'insert')
	{
			if($img_result != -1 )
			{
				$result = $s->insertRecord('tbl_application' ,$fileArray);
			}
	}
	
	function showSubApplicationCategories($cat_id , $dashes)
	{
		$dashes.= '&nbsp;&nbsp;&nbsp;';
		if(strlen(trim($order))<=0)
		{
			$order		= "asc";
		}
		if(strlen(trim($order_by))<=0)
		{
			$order_by	= 'application_name';
		}	
		
		"sub::".$sql_sub_cat = "SELECT * from tbl_application where parent_id1=$cat_id and deleteflag='active' order by ".$order_by." ".$order;
		$rsSub = mysqli_query($GLOBALS["___mysqli_ston"],$sql_sub_cat);
		if(mysqli_num_rows($rsSub) >= 1)
		{
			while($rowSub = mysqli_fetch_array($rsSub))
			{
?>

<tr class="text" onmouseover="bgr_color(this, '#EAB9BA')" onMouseOut="bgr_color(this, '')" >
  <td width="5%" align="center"><?php //echo stripslashes($rowSub['application_id'])."<br>";?></td>
  <td width="15%"><?php echo $dashes."&raquo; ". stripslashes($rowSub['application_name'])."<br>";?></td>
  <td width="15%" align="center"><?php echo $rowSub['sort_order'];?></td>
  <td width="15%" align="center"><?php 
				if($rowSub['application_status'] =="active")
				{
?>
    <img src="images/green.gif" title="Active" border="0"  /> &nbsp; &nbsp; <a href="index.php?pagename=manage_application&action=ChangeStatus&pcode=<?php echo $rowSub['application_id'];?>"><img src="images/red_light.gif" title="Inactive" border="0"  /></a>
    <?php
				}
				else if($rowSub['application_status'] =="inactive")
				{
?>
    <a href="index.php?pagename=manage_application&action=ChangeStatus&pcode=<?php echo $rowSub['application_id']?>"><img src="images/green_light.gif" title="Active" border="0" /></a> &nbsp; &nbsp; <img src="images/red.gif" title="Inactive" border="0"  />
    <?php
				}
?></td>
  <td width="15%" align="center"><input type="button" value="Add Products" onclick="window.location.href='index.php?pagename=g2_index_matching_add_pro&activeOP=group&action=edit&pcode=<?php echo $rowSub['application_id'];?>'" class="inputton" /></td>
  <td  align="center">&nbsp;&nbsp;<a href="index.php?pagename=add_application&action=edit&pcode=<?php echo $rowSub['application_id'] ; ?>"> <img src="images/e.gif" border="0" title="Edit"/></a> &nbsp; &nbsp; <a href="index.php?pagename=manage_application&action=delete&pcode=<?php echo $rowSub['application_id'] ; ?>"  onclick="return del();"><img src="images/x.gif" border="0" title="Delete" /></a></td>
</tr>
<?php 
				showSubApplicationCategories($rowSub['application_id'] , $dashes); 
			}
 		}
	}	
?>
<script type="text/javascript">
function OnSelect() 
{
	window.location = document.frx1.records.value;
}
function OnSelectPages()
{
	window.location = document.frx1.pages_select.value;
}
</script>
<form name="frx1" id="frx1" action="#" method="post">
  <table width="100%" align="center" cellpadding="0" cellspacing="0" class="pagecontent">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="46%" class="pageheadTop">Product Category Manager</td>
            <td width="54%" class="headLink"><ul>
                <li><a href="index.php?pagename=add_application&action=add_new">Add New Category</a></li>
              </ul></td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td class="pHeadLine"></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><?php
		if($_REQUEST['action']=='ChangeStatus')
		{
			if($result==0)
			{
				echo "<p class='success'>Status Change Successfully</p><br/>";	
			}
			else if($result==1)
			{
				echo "<p class='error'>Status Changing Fails</p><br/>";	
			}
			$data_action = "Changed";
		}
		if($data_action=='delete')
		{
			$rs_del    			= $s->getData_with_condition('tbl_application','application_id',$pcode);
			$row_del   			= mysqli_fetch_object($rs_del);
			
			$result = $s->delete_table_withCondition('tbl_application','application_id',$pcode);	
			if($result)
			{
				
				echo "<p class='success'>".record_delete."</p><br/>";	
			}
			else 
			{
				echo "<p class='error'>".record_not_delete."</p><br/>";	
			}
		}
		if($_REQUEST['action']=='update')
		{
			 if($result==0)
			{
				echo "<p class='success'>".record_update."</p><br/>";	
			}
			else if($result==1)
			{
				echo "<p class='error'>".record_not_update."</p><br/>";	
			}
		}
		else if($_REQUEST['action']=='insert')
		{
			 if($result==0)
			{
				echo "<p class='success'>".record_added."</p><br/>";	
			}
			else if($result==1)
			{
				echo "<p class='error'>".record_not_added."</p><br/>";	
			}
		}
?></td>
    </tr>
    <tr>
      <td valign="top"><table width="100%" cellpadding="5" cellspacing="0" class="tblBorder" border="0">
          <tr class="pagehead">
            <td colspan="2" class="pad"> Category Details</td>
            <td align="right" colspan="4">Records View &nbsp;
              <select name="records" onchange="OnSelect();"  >
                <option <?php if($max_results==25){ echo "selected='selected'";} ?>
value="index.php?pagename=manage_application&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=25"> 25</option>
                <option <?php if($max_results==50){ echo "selected='selected'";} ?>
value="index.php?pagename=manage_application&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=50"> 50</option>
                <option <?php if($max_results==100){ echo "selected='selected'";} ?>
value="index.php?pagename=manage_application&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=100"> 100</option>
              </select></td>
          </tr>
          <?php
	$order_by 	= 'application_name';	
	$sql_sub	= "select * from tbl_application where parent_id1 = 0 and deleteflag='active' order by ".$order_by." ".$order;
	$rs			= mysqli_query($GLOBALS["___mysqli_ston"],$sql_sub);
	
	if($order_by == '')
	{
		$order_by = 'application_name';	
	}
	//$rs = $s->getData_withPages('tbl_application',$order_by, $order,$from,$max_results);
	if($_REQUEST["order"] == 'asc')
	{
		$order = 'desc';
	}
	else if($_REQUEST["order"] == 'desc')
	{
		$order = 'asc';
	}
	else
	{
		$order = 'asc';
	}
	if(mysqli_num_rows($rs)== 0 && $page != 1)
	{
		$page=1;
		$s->pageLocation("index.php?pagename=manage_application&orderby=application_name&order=$order&pageno=$page&records=$max_results"); 
	}
	$i=1;
	$ij=1;
	if(mysqli_num_rows($rs)!=0)
	{
?>
          <tr class="head">
            <td width="5%" align="center">ID</td>
            <td width="15%"><a href="index.php?pagename=manage_application&orderby=application_name&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=<?php echo $max_results;?>"> Name</a></td>
            <td width="10%" align="center" ><a href="index.php?pagename=manage_application&orderby=sort_order&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=<?php echo $max_results;?>">Display Order</a></td>
            <td width="15%" align="center">Status</td>
            <td width="15%" align="center">Add Products</td>
            <td width="15%" align="center">Action</td>
          </tr>
          <?php
	while($row = mysqli_fetch_object($rs))
	{
?>
          <tr class="text" onmouseover="bgr_color(this, '#FFFF99')" onmouseout="bgr_color(this, '')" bgcolor="#660099">
            <td align="center"><?php echo $ij++;  //$row->application_id;?></td>
            <td><span style="color:#FF0000"><?php echo stripslashes($row->application_name);?></span></td>
            <td align="center" nowrap><?php echo stripslashes($row->sort_order);?></td>
            <td align="center" nowrap><?php 
	if($row->application_status =="active")
	{
?>
              <img src="images/green.gif" title="Active" border="0" alt=""  /> &nbsp; &nbsp; <a href="index.php?pagename=manage_application&action=ChangeStatus&pcode=<?php echo $row->application_id;?>&pageno=<?php echo $page;?>&records=<?php echo $max_results;?>"><img src="images/red_light.gif" title="Inactive" border="0"  /></a>
              <?php
	}
	else if($row->application_status =="inactive")
	{
?>
              <a href="index.php?pagename=manage_application&action=ChangeStatus&pcode=<?php echo $row->application_id;?>&pageno=<?php echo $page;?>&records=<?php echo $max_results;?>"><img src="images/green_light.gif" title="Active" border="0"  /></a> &nbsp; &nbsp; <img src="images/red.gif" title="Inactive" border="0"  />
              <?php		
	}
?></td>
            <td align="center"><input type="button" value="Add Products" onclick="window.location.href='index.php?pagename=g2_index_matching_add_pro&activeOP=group&action=edit&pcode=<?php echo $row->application_id;?>'" class="inputton" /></td>
            <td align="center">&nbsp; &nbsp;<a href="index.php?pagename=add_application&action=edit&pcode=<?php echo $row->application_id;?>"><img src="images/e.gif"  border="0"  alt="Edit"/></a> &nbsp; &nbsp;
              <?php
	  if($_SESSION["AdminLoginID_SET"]=='4' || $_SESSION["AdminLoginID_SET"]=='32')
				  {
				  ?>
              <a href="index.php?pagename=manage_application&action=delete&pcode=<?php echo $row->application_id;?>" onclick='return del();'><img src="images/x.gif"  border="0"  alt="Edit"/></a>
              <?php }?></td>
          </tr>
          <?php 
		  showSubApplicationCategories($row->application_id , "&nbsp;&nbsp;&nbsp;");	
		$i++;
	}
?>
          <tr class='head headLink'>
            <td colspan="6" align="right" ><table width="15%" cellpadding="0" cellspacing="0" border="0">
                <tr>
                  <td align="right" ><?php						
		 	$total_pages = $s->getTotal_pages('tbl_application',$order_by, $order,$max_results );
			if($page > 1)
			{ 
			$prev = ($page - 1); 
			echo "<ul><li><a href='index.php?pagename=manage_application&orderby=$order_by&order=$order&pageno=$prev&records=$max_results'>< Previous</a></li></ul>"; 
			} ?>
            </td>
           <td align="right"><select name="pages_select" onchange="OnSelectPages();"  >
            <?php
			for($i = 1; $i <= $total_pages; $i++)
			{ 
?>
             <option <?php if($page==$i){ echo "selected='selected'";} ?> 
             value="index.php?pagename=manage_application&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $i;?>&records=<?php echo $max_results;?>"><?php echo $i;?></option>
                      <?php
			} 
?>
                    </select></td>
                  <td  align="left"><?php		
			if($page < $total_pages)
			{ 
				$next = ($page + 1); 
				echo "<ul><li><a href='index.php?pagename=manage_application&orderby=$order_by&order=$order&pageno=$next&records=$max_results'>Next ></a></li></ul>";
			} ?>
            </td>
                </tr>
              </table></td>
          </tr>
          <?php } else { ?>
          <tr class='text'>
            <td colspan='6' class='redstar'>&nbsp; No record present in database</td>
          </tr>
          <?php }?>
        </table></td>
    </tr>
  </table>
</form>