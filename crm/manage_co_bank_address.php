<?php 
//session_start();
	$data_action = $_REQUEST['action'];
	$pcode		 = $_REQUEST["pcode"];
	$order 		 = $_REQUEST["order"];
	$order_by    = $_REQUEST["orderby"];

	if(!isset($_GET['pageno']))
	{ 
    	$page = 1; 
	} 
	else 
	{ 
    $page = $_GET['pageno']; 
	} 
	if(!isset($_GET['records']))
	{ 
    	$max_results = 50; 
	} 
	else 
	{ 
    $max_results = $_GET['records']; 
	} 
	$from = (($page * $max_results) - $max_results); 
	
	
	if($_REQUEST['action']=='update' ||$_REQUEST['action']=='insert')
	{
		$fileArray["bank_name"]     = addslashes($_REQUEST["bank_name"]);
		$fileArray["bank_acc_no"]   = addslashes($_REQUEST["bank_acc_no"]);
		$fileArray["bank_address"]  = addslashes($_REQUEST["bank_address"]);
		$fileArray["ifsc_code"]  	= addslashes($_REQUEST["ifsc_code"]);		
		$fileArray["sort_order"]  	= addslashes($_REQUEST["sort_order"]);
		$fileArray["status"]    = $_REQUEST["status"];
	}
	if($_REQUEST['action'] == 'ChangeStatus')
	{
		$rs_status = $s->getData_with_condition('tbl_company_bank_address','bank_id',$pcode);
		if(mysqli_num_rows($rs_status)>0)
		{
			$row_status = mysqli_fetch_object($rs_status);
			if($row_status->status == 'active')
			{
				$fileArray["status"] = 'inactive';
			}
			else if($row_status->status == 'inactive')
			{
				$fileArray["status"] = 'active';
			}
			$result = $s->editRecord('tbl_company_bank_address',$fileArray,'bank_id',$pcode);
		}
	}
	if( $_REQUEST['action'] == 'update' )
	{
		$result = $s->editRecord('tbl_company_bank_address',$fileArray,'bank_id',$pcode);
	}
	if($_REQUEST['action'] == 'insert')
	{
			if($img_result != -1 )
			{
				$result = $s->insertRecord('tbl_company_bank_address' ,$fileArray);
			}
	}
?>
<script type="text/javascript">
function OnSelect() 
{
	window.location = document.frx1.records.value;
}
function OnSelectPages()
{
	window.location = document.frx1.pages_select.value;
}
</script>

<form name="frx1" id="frx1" action="#" method="post">
  <table width="100%" align="center" cellpadding="0" cellspacing="0" class="pagecontent">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="46%" class="pageheadTop">Company Bank Manager</td>
            <td width="54%" class="headLink"><ul>
                <li><a href="index.php?pagename=add_co_bank_address&action=add_new">Add New Bank </a></li>
              </ul></td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td class="pHeadLine"></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><?php
		if($_REQUEST['action']=='ChangeStatus')
		{
			if($result==0)
			{
				echo "<p class='success'>Status Change Successfully</p><br/>";	
			}
			else if($result==1)
			{
				echo "<p class='error'>Status Changing Fails</p><br/>";	
			}
			$data_action = "Changed";
		}
		
		if($_REQUEST['action']=='update')
		{
			 if($result==0)
			{
				echo "<p class='success'>".record_update."</p><br/>";	
			}
			else if($result==1)
			{
				echo "<p class='error'>".record_not_update."</p><br/>";	
			}
		}
		else if($_REQUEST['action']=='insert')
		{
			 if($result==0)
			{
				echo "<p class='success'>".record_added."</p><br/>";	
			}
			else if($result==1)
			{
				echo "<p class='error'>".record_not_added."</p><br/>";	
			}
		}
?></td>
    </tr>
    <tr>
      <td valign="top"><table width="100%" cellpadding="5" cellspacing="0" class="tblBorder" border="0">
          <tr class="pagehead">
            <td colspan="6" class="pad"> Bank Details</td>
            <td align="right" colspan="3">Records View &nbsp;
              <select name="records" onchange="OnSelect();"  >
                <option <?php if($max_results==25){ echo "selected='selected'";} ?>
value="index.php?pagename=manage_co_bank_address&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=25"> 25</option>
                <option <?php if($max_results==50){ echo "selected='selected'";} ?>
value="index.php?pagename=manage_co_bank_address&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=50"> 50</option>
                <option <?php if($max_results==100){ echo "selected='selected'";} ?>
value="index.php?pagename=manage_co_bank_address&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=100"> 100</option>
              </select></td>
          </tr>
          <?php
	$rs = $s->getData_without_condition('tbl_company_bank_address');
	if($order_by == '')
	{
		$order_by = 'status';	
	}
	$rs = $s->getData_withPages('tbl_company_bank_address',$order_by, $order,$from,$max_results);
	if($_REQUEST["order"] == 'asc')
	{
		$order = 'desc';
	}
	else if($_REQUEST["order"] == 'desc')
	{
		$order = 'asc';
	}
	else
	{
		$order = 'asc';
	}
	if(mysqli_num_rows($rs)== 0 && $page != 1)
	{
		$page=1;
		$s->pageLocation("index.php?pagename=manage_co_bank_address&orderby=location&order=$order&pageno=$page&records=$max_results"); 
	}
	$i=1;
	$ij=1;
	if(mysqli_num_rows($rs)!=0)
	{
?>
          <tr class="head">
            <td width="5%" align="center">ID</td>
            <td width="15%"><a href="index.php?pagename=manage_co_bank_address&orderby=location&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=<?php echo $max_results;?>"> Name</a></td>
	       <td width="15%" align="center">Bank A/c number</td>
	       <td width="15%" align="center">IFSC Code</td>
            <td width="15%" align="center">Address</td>

            <td width="10%" align="center" ><a href="index.php?pagename=manage_co_bank_address&orderby=sort_order&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=<?php echo $max_results;?>">Display Order</a></td>
            <td width="15%" align="center">Status</td>
            <td width="15%" align="center">Action</td>
          </tr>
          <?php
	while($row = mysqli_fetch_object($rs))
	{
?>
          <tr  class="text" onmouseover="bgr_color(this, '#FFFF99')" onmouseout="bgr_color(this, '')" bgcolor="#660099">
            <td align="center"><?php echo $ij++;  //$row->bank_id;?></td>
            <td><span style="color:#FF0000"><?php echo stripslashes($row->bank_name);?></span> </td>
            <td  align="center" ><?php echo stripslashes($row->bank_acc_no);?></td>
            <td  align="center" ><?php echo stripslashes($row->ifsc_code);?></td>
            <td  align="center" ><?php echo stripslashes($row->bank_address);?></td>
            <td  align="center" ><?php echo stripslashes($row->sort_order);?></td>
            <td  align="center" ><?php 
	if($row->status =="active")
	{
?>
              <img src="images/green.gif" title="Active" border="0" alt=""  /> &nbsp; &nbsp; <a href="index.php?pagename=manage_co_bank_address&action=ChangeStatus&pcode=<?php echo $row->bank_id;?>&pageno=<?php echo $page;?>&records=<?php echo $max_results;?>"><img src="images/red_light.gif" title="Inactive" border="0"  /></a>
              <?php
	}
	else if($row->status =="inactive")
	{
?>
              <a href="index.php?pagename=manage_co_bank_address&action=ChangeStatus&pcode=<?php echo $row->bank_id;?>&pageno=<?php echo $page;?>&records=<?php echo $max_results;?>"><img src="images/green_light.gif" title="Active" border="0"  /></a> &nbsp; &nbsp; <img src="images/red.gif" title="Inactive" border="0"  />
              <?php		
	}
?></td>
            <td align="center">&nbsp; &nbsp;<a href="index.php?pagename=add_co_bank_address&action=edit&pcode=<?php echo $row->bank_id;?>"><img src="images/e.gif"  border="0" alt="Edit"/></a> &nbsp; &nbsp;
              <?php
	  if($_SESSION["AdminLoginID_SET"]=='4' || $_SESSION["AdminLoginID_SET"]=='32')
				  {
				  ?>
              <a href="index.php?pagename=manage_co_bank_address&action=delete&pcode=<?php echo $row->bank_id;?>" onclick='return del();'><img src="images/x.gif"  border="0"  alt="Edit"/></a>
              <?php }?></td>
          </tr>
          <?php 
		$i++;
	}
?>
          <tr class='head headLink'>
            <td colspan="8" align="right" nowrap="nowrap"><table width="100%">
                <tr>
                  <td  nowrap="nowrap" align="right"><?php						
		 	$total_pages = $s->getTotal_pages('tbl_company_bank_address',$order_by, $order,$max_results );
			if($page > 1)
			{ 
				$prev = ($page - 1); 
			echo "<ul><li><a href='index.php?pagename=manage_co_bank_address&orderby=$order_by&order=$order&pageno=$prev&records=$max_results'>< Previous</a></li></ul>"; 
			} 
?></td>
                  <td width="43" nowrap="nowrap" align="center"><select name="pages_select" onchange="OnSelectPages();"  >
                      <?php
			for($i = 1; $i <= $total_pages; $i++)
			{ 
?>
                      <option <?php if($page==$i){ echo "selected='selected'";} ?> 
value="index.php?pagename=manage_co_bank_address&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $i;?>&records=<?php echo $max_results;?>"><?php echo $i;?></option>
                      <?php
			} 
?>
                    </select></td>
                  <td width="59" nowrap="nowrap" align="left"><?php		
			if($page < $total_pages)
			{ 
				$next = ($page + 1); 
				echo "<ul><li><a href='index.php?pagename=manage_co_bank_address&orderby=$order_by&order=$order&pageno=$next&records=$max_results'>Next ></a></li></ul>";
			} 
			 
?></td>
                </tr>
              </table></td>
          </tr>
          <?php
}
else
{
?>
          <tr class='text'>
            <td colspan='8' class='redstar'>&nbsp; No record present in database</td>
          </tr>
          <?php 
}
?>
        </table></td>
    </tr>
  </table>
</form>