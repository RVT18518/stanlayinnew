<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"/>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<?php 
//this new dynamic module for team added by rumit on dated 6Jan 2017
	$data_action = $_REQUEST['action'];
	$pcode		 = $_REQUEST["pcode"];
	$order 		 = $_REQUEST["order"];
	$order_by    = $_REQUEST["orderby"]; 
if($order =='')
{
	$order ="asc";
}

if($order_by =='')
{
	$order_by ="cust_segment_id";
}
	if(!isset($_GET['pageno']))
	{ 
    	$page = 1; 
	} else { 
    $page = $_GET['pageno']; 
	} 
	if(!isset($_GET['records']))
	{ 
    	$max_results = 50; 
	} else { 

    $max_results = $_GET['records']; 

	} 

	$from = (($page * $max_results) - $max_results); 

	

	

	if($_REQUEST['action']=='update' ||$_REQUEST['action']=='insert')

	{

		$fileArray["cust_segment_name"]        = addslashes($_REQUEST["cust_segment_name"]);
		$fileArray["cust_segment_abbrv"]       = addslashes($_REQUEST["cust_segment_abbrv"]);
		$fileArray["cust_segment_status"]      = $_REQUEST["cust_segment_status"];

		$fileArray["cust_segment_description"] = addslashes($_REQUEST["cust_segment_description"]);

	//commented below 3 lines on 15july2017  to add 2 more sub team lead	
//$fileArray_tl["admin_cust_segment_lead"]        = addslashes($_REQUEST["cust_segment_manager"]);
		//$fileArray_tl["sub_cust_segment_lead"]        = "0";//addslashes($_REQUEST["sub_cust_segment_lead"]);		
			
			//$result_empl_cust_segment_updated = $s->editRecord('tbl_admin',$fileArray_tl,'admin_team',$pcode);//exit;

		

	}

	if($_REQUEST['action'] == 'ChangeStatus')

	{

		$rs_status = $s->getData_with_condition('tbl_cust_segment','cust_segment_id',$pcode);

		if(mysqli_num_rows($rs_status)>0)

		{

			$row_status = mysqli_fetch_object($rs_status);

			if($row_status->cust_segment_status == 'active')

			{

				$fileArray["cust_segment_status"] = 'inactive';

			}

			else if($row_status->cust_segment_status == 'inactive')

			{

				$fileArray["cust_segment_status"] = 'active';

			}


			$result = $s->editRecord('tbl_cust_segment',$fileArray,'cust_segment_id',$pcode);
			
		
		}

	}

	if( $_REQUEST['action'] == 'update' )

	{

				$result = $s->editRecord('tbl_cust_segment',$fileArray,'cust_segment_id',$pcode);

	}

	if($_REQUEST['action'] == 'insert')

	{

			if($img_result != -1 )

			{

				$result = $s->insertRecord('tbl_cust_segment' ,$fileArray);

			}

	}

?>
<script type="text/javascript">

function OnSelect() 

{

	window.location = document.frx1.records.value;

}

function OnSelectPages()

{

	window.location = document.frx1.pages_select.value;

}

</script>

<form name="frx1" id="frx1" action="#" method="post">
  <table width="100%" align="center" cellpadding="0" cellspacing="0" class="pagecontent">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="53%" class="pageheadTop">Customer Segment Manager</td>
            <td width="47%" class="headLink"><ul>
                <li><a href="index.php?pagename=add_cust_segment&action=add_new">Add New </a></li>
              </ul></td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td class="pHeadLine"></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><?php

		if($_REQUEST['action']=='ChangeStatus')

		{

			if($result==0)

			{

				echo "<p class='success'>Status Change Successfully</p><br/>";	

			}

			else if($result==1)

			{

				echo "<p class='error'>Status Changing Fails</p><br/>";	

			}

			$data_action = "Changed";

		}

		if($data_action=='delete')

		{

			$rs_del    = $s->getData_with_condition('tbl_cust_segment','cust_segment_id',$pcode);

			$row_del   = mysqli_fetch_object($rs_del);

			$del_image_large  = $row_del->cust_segment_logo_large;

			$del_image_medium = $row_del->cust_segment_logo_medium;

			$del_image_small  = $row_del->cust_segment_logo_small;

			$result = $s->delete_table_withCondition('tbl_cust_segment','cust_segment_id',$pcode);	

			if($result)

			{

				if(file_exists($del_image_large)>0)

				{

					unlink($del_image_large);

				}

				if(file_exists($del_image_medium)>0)

				{

					unlink($del_image_medium);

				}

				if(file_exists($del_image_small)>0)

				{

					unlink($del_image_small);

				}

				echo "<p class='success'>".record_delete."</p><br/>";	

			}

			else 

			{

				echo "<p class='error'>".record_not_delete."</p><br/>";	

			}

		}

		if($_REQUEST['action']=='update')

		{

			 if($result==0)

			{

				echo "<p class='success'>".record_update."</p><br/>";	

			}

			else if($result==1)

			{

				echo "<p class='error'>".record_not_update."</p><br/>";	

			}

		}

		else if($_REQUEST['action']=='insert')

		{

			 if($result==0)

			{

				echo "<p class='success'>".record_added."</p><br/>";	

			}

			else if($result==1)

			{

				echo "<p class='error'>".record_not_added."</p><br/>";	

			}

		}

?></td>
    </tr>
    <tr>
      <td width="100%"><table id="cust_segment_table" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Status</th>
              <th>Action</th>
            </tr>
            <?php 
			
			$query="1=1";
$star="cust_segment_id, cust_segment_name, cust_segment_status";	
$rs= $s->selectWhere('tbl_cust_segment',$query,$star);

//function selectWhere($table, $query, $star = '*')


//$sql = "select $star from $table where  $query";

while($row[] = mysqli_fetch_object($rs))
{
}
/*echo "<pre>";
print_r($row);*/

/*$data=array('data'=>$row);
$myJSON = json_encode($data);

echo $myJSON;*/

$data=$row;
/*echo "<pre>";
print_r($data);*/
	//		print_r($data['rec']);
			if(!empty($data)){ foreach($data as $key => $val){ 
			
			//echo $val;
			
			?>
            <tr>
              <td><?php echo $val->cust_segment_id;?></td>
              <td><?php echo $val->cust_segment_name;?></td>
              <td><?php echo $val->cust_segment_status;?></td>
              <td><a href="<?php //echo base_url('backoffice/bestsuite/edit/'.base64_encode($val->bestsuite_id)); ?>" class="btn btn-default btn-xs"><i class="fa fa-edit"></i> Edit</a> &nbsp;<a href="<?php //echo base_url('backoffice/bestsuite/delete/'.base64_encode($val->bestsuite_id)); ?>" class="btn btn-danger btn-xs" onclick="return confirm('<?php //echo lang('record_confirm_lang_delete');?>')"><i class="fa fa-trash"></i> Delete</a></td>
            </tr>
            <?php }}?>
          </thead>
        </table></td>
    </tr>
  </table>
</form>
<script>
	//var base_url='<?php //base_url()?>';	
	$(document).ready(function() {	
	var table;
	table = $('#cust_segment_table').DataTable({
			"processing": true,
        "serverSide": true,
		 "ajax": "getcustsegments.php"
		});
	});
</script> 
<!-- <script>
   $(document).ready(function() {
    $('#cust_segment_table').DataTable( {
        "ajax": "getcustsegments.php"
    } );
} );
   </script>-->