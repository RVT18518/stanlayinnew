<?php 
	$data_action = $_REQUEST['action'];
	$pcode		 = $_REQUEST["pcode"];
	$order 		 = $_REQUEST["order"];
	$order_by    = $_REQUEST["orderby"]; 
	if(!isset($_GET['pageno']))
	{ 
    	$page = 1; 
	} else { 
    $page = $_GET['pageno']; 
	} 
	if(!isset($_GET['records']))
	{ 
    	$max_results = 50; 
	} else { 
    $max_results = $_GET['records']; 
	} 
	$from = (($page * $max_results) - $max_results); 
	
	
	if($_REQUEST['action']=='update' ||$_REQUEST['action']=='insert')
	{
		$fileArray["designation_name"]        = addslashes($_REQUEST["designation_name"]);
		//$fileArray["designation_status"]      = $_REQUEST["designation_status"];
		$fileArray["designation_description"] = addslashes($_REQUEST["designation_description"]);
		$designation_added_by = "1";// use session variable for login id 
		$img_result = "";
		if($_FILES["designation_logo"]["name"] != "")
		{
			$large				= 456;//$s->fetchGeneral_config('largeimg');
			$medium				= 600;//$s->fetchGeneral_config('mediumimg');
			$small				= 206;//$s->fetchGeneral_config('smallimg');	
			$img_result_large   = $s->ImageUpload('uploads/manufacturer_logos/large/','designation_logo','MAFLOGO',$large,340);	
			$img_result_medium  = $s->ImageUpload('uploads/manufacturer_logos/medium/','designation_logo','MAFLOGO',$medium,600);	
			$img_result_small   = $s->ImageUpload('uploads/manufacturer_logos/small/','designation_logo','MAFLOGO',$small,154);	
			if($img_result_large != -1)
			{
				$fileArray["designation_logo_large"]  = $img_result_large;
			}
			if($img_result_medium != -1)
			{
				$fileArray["designation_logo_medium"] = $img_result_medium;
			}
			if($img_result_small  != -1)
			{
				$fileArray["designation_logo_small"] = $img_result_small;
			}	
		}
	}
	if($_REQUEST['action'] == 'ChangeStatus')
	{
		$rs_status = $s->getData_with_condition('tbl_designation','designation_id',$pcode);
		if(mysqli_num_rows($rs_status)>0)
		{
			$row_status = mysqli_fetch_object($rs_status);
			if($row_status->designation_status == 'active')
			{
				$fileArray["designation_status"] = 'inactive';
			}
			else if($row_status->designation_status == 'inactive')
			{
				$fileArray["designation_status"] = 'active';
			}
			$result = $s->editRecord('tbl_designation',$fileArray,'designation_id',$pcode);
		}
	}
	if( $_REQUEST['action'] == 'update' )
	{
			
		if($_FILES["designation_logo"]["name"] !="")
		{
			$rs_del    = $s->getData_with_condition('tbl_designation','designation_id',$pcode);
			$row_del   = mysqli_fetch_object($rs_del);
			$del_image_large  = $row_del->designation_logo_large;
			$del_image_medium = $row_del->designation_logo_medium;
			$del_image_small  = $row_del->designation_logo_small;
			if(file_exists($del_image_large)>0)
			{
				unlink($del_image_large);
			}
			if(file_exists($del_image_medium)>0)
			{
				unlink($del_image_medium);
			}
			if(file_exists($del_image_small)>0)
			{
				unlink($del_image_small);
			}
		}
		
	
	$fileArray['tax_class_id'] 	 	= $_REQUEST['tax_class_id'];// now class id is rates id
	$fileArray['est_arrival_min'] 	= $_REQUEST['est_arrival_min'];// now class id is rates id
	$fileArray['est_arrival_max'] 	= $_REQUEST['est_arrival_max'];// now class id is rates id
	$fileArray['pro_rap_amount'] 	= $_REQUEST['pro_rap_amount'];
	$result = $s->editRecord('tbl_designation',$fileArray,'designation_id',$pcode);
	}
	if($_REQUEST['action'] == 'insert')
	{
			if($img_result != -1 )
			{
				$result = $s->insertRecord('tbl_designation' ,$fileArray);
			}
	}
?>
<script type="text/javascript">
function OnSelect() 
{
	window.location = document.frx1.records.value;
}
function OnSelectPages()
{
	window.location = document.frx1.pages_select.value;
}
</script>
<form name="frx1" id="frx1" action="#" method="post">
  <table width="100%" align="center" cellpadding="0" cellspacing="0" class="pagecontent">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="53%" class="pageheadTop">Designation Manager</td>
            <td width="47%" class="headLink"><ul>
                <li><a href="index.php?pagename=add_designation&action=add_new">Add New </a></li>
            </ul></td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td class="pHeadLine"></td>
    </tr>
    
    <tr>
      <td><?php
		if($_REQUEST['action']=='ChangeStatus')
		{
			if($result==0)
			{
				echo "<p class='success'>Status Change Successfully</p><br/>";	
			}
			else if($result==1)
			{
				echo "<p class='error'>Status Changing Fails</p><br/>";	
			}
			$data_action = "Changed";
		}
		if($data_action=='delete')
		{
			$rs_del    = $s->getData_with_condition('tbl_designation','designation_id',$pcode);
			$row_del   = mysqli_fetch_object($rs_del);
			$del_image_large  = $row_del->designation_logo_large;
			$del_image_medium = $row_del->designation_logo_medium;
			$del_image_small  = $row_del->designation_logo_small;
			$result = $s->delete_table_withCondition('tbl_designation','designation_id',$pcode);	
			if($result)
			{
				if(file_exists($del_image_large)>0)
				{
					unlink($del_image_large);
				}
				if(file_exists($del_image_medium)>0)
				{
					unlink($del_image_medium);
				}
				if(file_exists($del_image_small)>0)
				{
					unlink($del_image_small);
				}
				echo "<p class='success'>".record_delete."</p><br/>";	
			}
			else 
			{
				echo "<p class='error'>".record_not_delete."</p><br/>";	
			}
		}
		if($_REQUEST['action']=='update')
		{
			 if($result==0)
			{
				echo "<p class='success'>".record_update."</p><br/>";	
			}
			else if($result==1)
			{
				echo "<p class='error'>".record_not_update."</p><br/>";	
			}
		}
		else if($_REQUEST['action']=='insert')
		{
			 if($result==0)
			{
				echo "<p class='success'>".record_added."</p><br/>";	
			}
			else if($result==1)
			{
				echo "<p class='error'>".record_not_added."</p><br/>";	
			}
		}
?></td>
    </tr>
    <tr>
      <td><table width="100%" cellpadding="0" cellspacing="0" class="tblBorder table table-striped table-hover">
          <tr class="pagehead">
            <td colspan="2" class="pad"> Designation Details</td>
            <td width="15%" align="center">Records View &nbsp;
              <select name="records" onchange="OnSelect();"  >
                <option <?php if($max_results==10){ echo "selected='selected'";} ?> 
value="index.php?pagename=manage_designation&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=10"> 10</option>
                <option <?php if($max_results==20){ echo "selected='selected'";} ?>
value="index.php?pagename=manage_designation&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=20"> 20</option>
                <option <?php if($max_results==50){ echo "selected='selected'";} ?>
value="index.php?pagename=manage_designation&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=50"> 50</option>
                <option <?php if($max_results==100){ echo "selected='selected'";} ?>
value="index.php?pagename=manage_designation&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=100"> 100</option>
              </select></td>
          </tr>
          <?php
	$rs= $s->getData_without_condition('tbl_designation');
	if($order_by == '')
	{
		$order_by = 'designation_name';	
	}
	$rs = $s->getData_withPages('tbl_designation',$order_by, $order,$from,$max_results);
	if($_REQUEST["order"] == 'asc')
	{
		$order = 'desc';
	}
	else if($_REQUEST["order"] == 'desc')
	{
		$order = 'asc';
	}
	else
	{
		$order = 'asc';
	}
	if(mysqli_num_rows($rs)== 0 && $page != 1)
	{
		$page=1;
		$s->pageLocation("index.php?pagename=manage_designation&orderby=designation_name&order=$order&pageno=$page&records=$max_results"); 
	}
	$i=1;
	if(mysqli_num_rows($rs)!=0)
	{
?>
          <tr class="head">
            <Td width="9%" align="center">ID</Td>
            <td width="57%"><a href="index.php?pagename=manage_designation&orderby=designation_name&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=<?php echo $max_results;?>"> Designation Name</a></td>
            <!-- <td width="30%" align="center">
		  <a href="index.php?pagename=manage_designation&orderby=designation_status&order=<?php //echo $order;?>&pageno=<?php //echo $page;?>&records=<?php //echo $max_results;?>">
		  Status</a></td>-->
            <td align="center">Action</td>
          </tr>
          <?php
		  $ij=1;
	while($row = mysqli_fetch_object($rs))
	{
?>
          <tr class="text" >
            <td align="center"><?php echo $ij++; //$row->designation_id; ?></td>
            <td><?php echo stripslashes($row->designation_name);?></td>
            <td align="center"><a href="index.php?pagename=add_designation&action=edit&pcode=<?php echo $row->designation_id;?>"><img src="images/e.gif"  border="0"  alt="Edit"/></a> &nbsp; &nbsp;
             
              <a href="index.php?pagename=manage_designation&action=delete&pcode=<?php echo $row->designation_id;?>" onclick='return del();'><img src="images/x.gif"  border="0"  alt="Edit"/></a>
             
</td>
          </tr>
          <?php 
		$i++;
	}
?>
          <tr class='head headLink'>
            <td colspan="3" align="right" nowrap="nowrap"><table width="100%">
                <tr>
                  <td  nowrap="nowrap" align="right"><?php						
		 	$total_pages = $s->getTotal_pages('tbl_designation',$order_by, $order,$max_results );
			if($page > 1)
			{ 
				$prev = ($page - 1); 
			echo "<ul><li><a href='index.php?pagename=manage_designation&orderby=$order_by&order=$order&pageno=$prev&records=$max_results'>< Previous</a></li></ul>"; 
			} 
?></td>
                  <td width="43" nowrap="nowrap" align="center"><select name="pages_select" onchange="OnSelectPages();"  >
                      <?php
			for($i = 1; $i <= $total_pages; $i++)
			{ 
?>
                      <option <?php if($page==$i){ echo "selected='selected'";} ?> 
value="index.php?pagename=manage_designation&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $i;?>&records=<?php echo $max_results;?>"><?php echo $i;?></option>
                      <?php
			} 
?>
                    </select></td>
                  <td width="100" nowrap="nowrap" align="left"><?php		
			if($page < $total_pages)
			{ 
				$next = ($page + 1); 
				echo "<ul><li><a href='index.php?pagename=manage_designation&orderby=$order_by&order=$order&pageno=$next&records=$max_results'>Next ></a></li></ul>";
			} 
			 
?></td>
                </tr>
              </table></td>
          </tr>
          <?php
}
else
{
?>
          <tr class='text'>
            <td colspan='3' class='redstar'>&nbsp; No record present in database</td>
          </tr>
          <?php 
}
?>
        </table></td>
    </tr>
  </table>
</form>