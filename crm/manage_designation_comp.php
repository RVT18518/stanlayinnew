<?php 
	$data_action = $_REQUEST['action'];
	$pcode		 = $_REQUEST["pcode"];
	$order 		 = $_REQUEST["order"];
	$order_by    = $_REQUEST["orderby"]; 
	if(!isset($_GET['pageno']))
	{ 
    	$page = 1; 
	} else { 
    $page = $_GET['pageno']; 
	} 
	if(!isset($_GET['records']))
	{ 
    	$max_results = 50; 
	} else { 
    $max_results = $_GET['records']; 
	} 
	$from = (($page * $max_results) - $max_results); 
	
	
	if($_REQUEST['action']=='update' ||$_REQUEST['action']=='insert')
	{
		$fileArray["designation_name"]        = addslashes($_REQUEST["designation_name"]);
		//$fileArray["designation_status"]      = $_REQUEST["designation_status"];
		$fileArray["designation_description"] = addslashes($_REQUEST["designation_description"]);
		//$designation_added_by
		$fileArray["designation_added_by"] = $_SESSION["AdminLoginID_SET"];// use session variable for login id 
		$img_result = "";
		
	}
	if($_REQUEST['action'] == 'ChangeStatus')
	{
		$rs_status = $s->getData_with_condition('tbl_designation_comp','designation_id',$pcode);
		if(mysqli_num_rows($rs_status)>0)
		{
			$row_status = mysqli_fetch_object($rs_status);
			if($row_status->designation_status == 'active')
			{
				$fileArray["designation_status"] = 'inactive';
			}
			else if($row_status->designation_status == 'inactive')
			{
				$fileArray["designation_status"] = 'active';
			}
			$result = $s->editRecord('tbl_designation_comp',$fileArray,'designation_id',$pcode);//exit;
		}
	}
	if( $_REQUEST['action'] == 'update' )
	{
	
	$result = $s->editRecord('tbl_designation_comp',$fileArray,'designation_id',$pcode);
	}
	if($_REQUEST['action'] == 'insert')
	{
			if($img_result != -1 )
			{
				$result = $s->insertRecord('tbl_designation_comp' ,$fileArray);
			}
	}
?>
<script type="text/javascript">

function OnSelect() 

{

	window.location = document.frx1.records.value;

}

function OnSelectPages()

{

	window.location = document.frx1.pages_select.value;

}

</script>

<form name="frx1" id="frx1" action="#" method="post">
  <table width="100%" align="center" cellpadding="0" cellspacing="0" class="pagecontent">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="53%" class="pageheadTop">Company Designation Manager</td>
            <td width="47%" class="headLink"><ul>
                <li><a href="index.php?pagename=add_designation_comp&action=add_new">Add New </a></li>
              </ul></td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td class="pHeadLine"></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><?php
		if($_REQUEST['action']=='ChangeStatus')
		{
			if($result==0)
			{
				echo "<p class='success'>Status Change Successfully</p><br/>";	
			}
			else if($result==1)
			{
				echo "<p class='error'>Status Changing Fails</p><br/>";	
			}
			$data_action = "Changed";
		}
		if($data_action=='delete')
		{
			$rs_del    = $s->getData_with_condition('tbl_designation_comp','designation_id',$pcode);
			$row_del   = mysqli_fetch_object($rs_del);
			$del_image_large  = $row_del->designation_logo_large;
			$del_image_medium = $row_del->designation_logo_medium;
			$del_image_small  = $row_del->designation_logo_small;
			$result = $s->delete_table_withCondition('tbl_designation_comp','designation_id',$pcode);	
			if($result)
			{
				if(file_exists($del_image_large)>0)
				{
					unlink($del_image_large);
				}
				if(file_exists($del_image_medium)>0)
				{
					unlink($del_image_medium);
				}
				if(file_exists($del_image_small)>0)
				{
					unlink($del_image_small);
				}
				echo "<p class='success'>".record_delete."</p><br/>";	
			}
			else 
			{
				echo "<p class='error'>".record_not_delete."</p><br/>";	
			}
		}
		if($_REQUEST['action']=='update')
		{
			 if($result==0)
			{
				echo "<p class='success'>".record_update."</p><br/>";	
			}
			else if($result==1)
			{
				echo "<p class='error'>".record_not_update."</p><br/>";	
			}
		}
		else if($_REQUEST['action']=='insert')
		{
			 if($result==0)
			{
				echo "<p class='success'>".record_added."</p><br/>";	
			}
			else if($result==1)
			{
				echo "<p class='error'>".record_not_added."</p><br/>";	
			}
		}
?></td>
    </tr>
    <tr>
      <td width="100%"><table width="100%" align="center" cellpadding="0" cellspacing="0" class="pagecontent">
          <tr>
            <td width="100%"><article>
                <div class="height10"></div>
                <div id="results"></div>
                <div class="loader"></div>
              </article></td>
          </tr>
        </table></td>
    </tr>
  </table>
</form>
<script src="js/jquery-1.9.0.min.js"></script> 
<script type="text/javascript">
        // fetching records
                            function displayRecords(numRecords, pageNum, orderBy, orderSe) {
                                $.ajax({
                                    type: "GET",
                                    url: "getrecords_comp_designation.php",
                                    data: "show=" + numRecords + "&pagenum=" + pageNum + "&orderby=" + orderBy + "&orderse=" + orderSe,
                                    cache: true,
                                    beforeSend: function() {
                                        $('.loader').html('<img src="loader.gif" alt="" width="24" height="24" style="padding-left: 400px; margin-top:10px;" >');
                                    },
                                    success: function(html) {
                                        $("#results").html(html);
                                        $('.loader').html('');
                                    }
                                });
                            }

        // used when user change row limit
                             function changeDisplayRowCount(numRecords,orderBy,orderSe) {
								/* alert(numRecords);
								 alert(orderBy);
								 alert(orderSe);*/
                                displayRecords(numRecords, 1, orderBy, orderSe);
                            }
							
							 function changeOrderBy(numRecords,orderBy, orderSe) {
                                displayRecords(numRecords, 1, 'asc', orderSe);
                            }

                            $(document).ready(function() {
                                 displayRecords(50, 1,'desc', 'designation_id');
                            });
        </script>