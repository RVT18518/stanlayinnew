<?php 
	$data_action = $_REQUEST['action'];
	$pcode		 = $_REQUEST["pcode"];
	$order 		 = $_REQUEST["order"];
	$order_by    = $_REQUEST["orderby"]; 
	if(!isset($_GET['pageno']))
	{ 
    	$page = 1; 
	} else { 
    $page = $_GET['pageno']; 
	} 
	if(!isset($_GET['records']))
	{ 
    	$max_results = 50; 
	} else { 
    $max_results = $_GET['records']; 
	} 
	$from = (($page * $max_results) - $max_results); 
	
	
	if($_REQUEST['action']=='update' ||$_REQUEST['action']=='insert')
	{
		$fileArray["designation_name"]        = addslashes($_REQUEST["designation_name"]);
		//$fileArray["designation_status"]      = $_REQUEST["designation_status"];
		$fileArray["designation_description"] = addslashes($_REQUEST["designation_description"]);
		//$designation_added_by
		$fileArray["designation_added_by"] = $_SESSION["AdminLoginID_SET"];// use session variable for login id 
		$img_result = "";
		
	}
	if($_REQUEST['action'] == 'ChangeStatus')
	{
		$rs_status = $s->getData_with_condition('tbl_designation_comp','designation_id',$pcode);
		if(mysqli_num_rows($rs_status)>0)
		{
			$row_status = mysqli_fetch_object($rs_status);
			if($row_status->designation_status == 'active')
			{
				$fileArray["designation_status"] = 'inactive';
			}
			else if($row_status->designation_status == 'inactive')
			{
				$fileArray["designation_status"] = 'active';
			}
			$result = $s->editRecord('tbl_designation_comp',$fileArray,'designation_id',$pcode);//exit;
		}
	}
	if( $_REQUEST['action'] == 'update' )
	{
	
	$result = $s->editRecord('tbl_designation_comp',$fileArray,'designation_id',$pcode);
	}
	if($_REQUEST['action'] == 'insert')
	{
			if($img_result != -1 )
			{
				$result = $s->insertRecord('tbl_designation_comp' ,$fileArray);
			}
	}
?>
<script type="text/javascript">
function OnSelect() 
{
	window.location = document.frx1.records.value;
}
function OnSelectPages()
{
	window.location = document.frx1.pages_select.value;
}
</script>
<form name="frx1" id="frx1" action="#" method="post">
  <table width="100%" align="center" cellpadding="0" cellspacing="0" class="pagecontent">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="53%" class="pageheadTop">Company Designation Manager</td>
            <td width="47%" class="headLink"><ul>
                <li><a href="index.php?pagename=add_designation_comp&action=add_new">Add New </a></li>
            </ul></td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td class="pHeadLine"></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><?php
		if($_REQUEST['action']=='ChangeStatus')
		{
			if($result==0)
			{
				echo "<p class='success'>Status Change Successfully</p><br/>";	
			}
			else if($result==1)
			{
				echo "<p class='error'>Status Changing Fails</p><br/>";	
			}
			$data_action = "Changed";
		}
		if($data_action=='delete')
		{
			$rs_del    = $s->getData_with_condition('tbl_designation_comp','designation_id',$pcode);
			$row_del   = mysqli_fetch_object($rs_del);
			$del_image_large  = $row_del->designation_logo_large;
			$del_image_medium = $row_del->designation_logo_medium;
			$del_image_small  = $row_del->designation_logo_small;
			$result = $s->delete_table_withCondition('tbl_designation_comp','designation_id',$pcode);	
			if($result)
			{
				if(file_exists($del_image_large)>0)
				{
					unlink($del_image_large);
				}
				if(file_exists($del_image_medium)>0)
				{
					unlink($del_image_medium);
				}
				if(file_exists($del_image_small)>0)
				{
					unlink($del_image_small);
				}
				echo "<p class='success'>".record_delete."</p><br/>";	
			}
			else 
			{
				echo "<p class='error'>".record_not_delete."</p><br/>";	
			}
		}
		if($_REQUEST['action']=='update')
		{
			 if($result==0)
			{
				echo "<p class='success'>".record_update."</p><br/>";	
			}
			else if($result==1)
			{
				echo "<p class='error'>".record_not_update."</p><br/>";	
			}
		}
		else if($_REQUEST['action']=='insert')
		{
			 if($result==0)
			{
				echo "<p class='success'>".record_added."</p><br/>";	
			}
			else if($result==1)
			{
				echo "<p class='error'>".record_not_added."</p><br/>";	
			}
		}
?></td>
    </tr>
    <tr>
      <td width="100%"><table width="100%" cellpadding="5" cellspacing="0" class="tblBorder">
          <tr class="pagehead">
            <td colspan="2" class="pad"> Company Designation Details</td>
            <td width="24%" align="center">  Status</td>
            <td width="26%" align="right">Records View &nbsp;
              <select name="records" onchange="OnSelect();"  >
                <option <?php if($max_results==10){ echo "selected='selected'";} ?> 
value="index.php?pagename=manage_designation_comp&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=10"> 10</option>
                <option <?php if($max_results==20){ echo "selected='selected'";} ?>
value="index.php?pagename=manage_designation_comp&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=20"> 20</option>
                <option <?php if($max_results==50){ echo "selected='selected'";} ?>
value="index.php?pagename=manage_designation_comp&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=50"> 50</option>
                <option <?php if($max_results==100){ echo "selected='selected'";} ?>
value="index.php?pagename=manage_designation_comp&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=100"> 100</option>
              </select></td>
          </tr>
          <?php
	$rs= $s->getData_without_condition('tbl_designation_comp');
	if($order_by == '')
	{
		$order_by = 'designation_name';	
	}
	$rs = $s->getData_withPages('tbl_designation_comp',$order_by, $order,$from,$max_results);
	if($_REQUEST["order"] == 'asc')
	{
		$order = 'desc';
	}
	else if($_REQUEST["order"] == 'desc')
	{
		$order = 'asc';
	}
	else
	{
		$order = 'asc';
	}
	if(mysqli_num_rows($rs)== 0 && $page != 1)
	{
		$page=1;
		$s->pageLocation("index.php?pagename=manage_designation_comp&orderby=designation_name&order=$order&pageno=$page&records=$max_results"); 
	}
	$i=1;
	if(mysqli_num_rows($rs)!=0)
	{
?>
          <tr class="head">
            <Td width="10%" align="center">ID</Td>
            <td width="40%"><a href="index.php?pagename=manage_designation_comp&orderby=designation_name&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=<?php echo $max_results;?>"> Designation Name</a></td>
            <td align="center"></td>
            <!-- <td width="30%" align="center">
		  <a href="index.php?pagename=manage_designation_comp&orderby=designation_status&order=<?php //echo $order;?>&pageno=<?php //echo $page;?>&records=<?php //echo $max_results;?>">
		  Status</a></td>-->
            <td align="center">Action</td>
          </tr>
          <?php
		  $ij=1;
	while($row = mysqli_fetch_object($rs))
	{
?>
          <tr class="text" onmouseover="bgr_color(this, '#FFFF99')" onmouseout="bgr_color(this, '')">
            <td align="center"><?php echo $ij++; //$row->designation_id; ?></td>
            <td><?php echo stripslashes($row->designation_name);?></td>
            <td align="center"><?php 
	if($row->designation_status =="active")
	{
?>
              <img src="images/green.gif" title="Active" border="0"  /> &nbsp; &nbsp; <a href="index.php?pagename=manage_designation_comp&action=ChangeStatus&pcode=<?php echo $row->designation_id?>"><img src="images/red_light.gif" title="Inactive" border="0"  /></a>
              <?php
	}
	else if($row->designation_status =="inactive")
	{
?>
              <a href="index.php?pagename=manage_designation_comp&action=ChangeStatus&pcode=<?php echo $row->designation_id?>"><img src="images/green_light.gif" title="Active" border="0"  /></a> &nbsp; &nbsp; <img src="images/red.gif" title="Inactive" border="0"  />
              <?php		
	}
?></td>
            <td align="center"><a href="index.php?pagename=add_designation&action=edit&pcode=<?php echo $row->designation_id;?>"><img src="images/e.gif"  border="0"  alt="Edit"/></a> &nbsp; &nbsp;
             
              <a href="index.php?pagename=manage_designation_comp&action=delete&pcode=<?php echo $row->designation_id;?>" onclick='return del();'><img src="images/x.gif"  border="0"  alt="Edit"/></a>
             
</td>
          </tr>
          <?php 
		$i++;
	}
?>
          <tr class='head headLink'>
            <td colspan="4" align="right" nowrap="nowrap"><table width="100%">
                <tr>
                  <td  nowrap="nowrap" align="right"><?php						
		 	$total_pages = $s->getTotal_pages('tbl_designation_comp',$order_by, $order,$max_results );
			if($page > 1)
			{ 
				$prev = ($page - 1); 
			echo "<ul><li><a href='index.php?pagename=manage_designation_comp&orderby=$order_by&order=$order&pageno=$prev&records=$max_results'>< Previous</a></li></ul>"; 
			} 
?></td>
                  <td width="43" nowrap="nowrap" align="center"><select name="pages_select" onchange="OnSelectPages();"  >
                      <?php
			for($i = 1; $i <= $total_pages; $i++)
			{ 
?>
                      <option <?php if($page==$i){ echo "selected='selected'";} ?> 
value="index.php?pagename=manage_designation_comp&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $i;?>&records=<?php echo $max_results;?>"><?php echo $i;?></option>
                      <?php
			} 
?>
                    </select></td>
                  <td width="99" nowrap="nowrap" align="left"><?php		
			if($page < $total_pages)
			{ 
				$next = ($page + 1); 
				echo "<ul><li><a href='index.php?pagename=manage_designation_comp&orderby=$order_by&order=$order&pageno=$next&records=$max_results'>Next ></a></li></ul>";
			} 
			 
?></td>
                </tr>
              </table></td>
          </tr>
          <?php
}
else
{
?>
          <tr class='text'>
            <td colspan='4' class='redstar'>&nbsp; No record present in database</td>
          </tr>
          <?php 
}
?>
        </table></td>
    </tr>
  </table>
</form>