 
<link href="css/style.css" rel="stylesheet" type="text/css" />
<?php
	$data_action = $_REQUEST['action'];
	$pcode		 = $_REQUEST["pcode"];
	$order 		 = 'asc';
	$order_by    = 'fees_Id';
	$result	     = $_REQUEST["result"]; 
	if(!isset($_GET['pageno']))
	{ 
    	$page = 1; 
	} 
	else 
	{ 
    	$page = $_GET['pageno']; 
	} 
	if(!isset($_GET['records']))
	{ 
    	$max_results = 10; 
	} 
	else 
	{ 
    	$max_results = $_GET['records']; 
	}
	$from = (($page * $max_results) - $max_results);  
	
	if($_REQUEST['action']=='update' ||$_REQUEST['action']=='insert')
	{
		$file_array["fees"]         = $_REQUEST["fees"];	
		$file_array["status"]       = $_REQUEST["status"];
	}
	if($_REQUEST['action']=='ChangeStatus')
	{
		$rs_status = $s->getData_with_condition('tbl_fees ','fees_Id',$pcode);
		if(mysqli_num_rows($rs_status)>0)
		{
			$row_status = mysqli_fetch_object($rs_status);
			if($row_status->status == 'active')
			{
				$fileArray["status"] = 'inactive';
				$dateArray["status"] = 'inactive';
			}
			else if($row_status->status == 'inactive')
			{
				$fileArray["status"] = 'active';
				$dateArray["status"] = 'active';
			}
			
			$result      = $s->editRecord('tbl_fees ',$fileArray,'fees_Id',$pcode);
			$result      = $s->editRecord('tbl_fees',$dateArray,'fees_Id',$pcode);
		$s->pageLocation("index.php?pagename=manage_fees&action=ChangeStatusDone&result=$result&orderby=contatct_address_id&order=$order&pageno=$page&records=$max_results"); 
		}
	}
	if($_REQUEST['action']=='update')
	{
		$result = $s->editRecord('tbl_fees ',$file_array,'fees_Id',$pcode);
		$data_action = "updateDone";
		$s->pageLocation("index.php?pagename=manage_fees&action=$data_action&pcode=$pcode&result=$result"); 
	}
	if($_REQUEST['action']=='insert')
	{
		$result = $s->insertRecord('tbl_fees ',$file_array);
		$data_action = "insertDone";
		$s->pageLocation("index.php?pagename=manage_fees&action=$data_action&pcode=$pcode&result=$result"); 
	}
?>
<script type="text/javascript">
function OnSelect() 
{
	window.location = document.frx1.records.value;
}
function OnSelectPages()
{
	window.location = document.frx1.pages_select.value;
}
</script>
<form name="frx1" id="frx1" action="#" method="post">
<table width="100%" align="center" cellpadding="0" cellspacing="0" class="pagecontent">
	
	 <tr>
       <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
           <tr>
             <td width="32%" class="pageheadTop">Fees Manager</td>
             <td width="68%" class="headLink"><ul>
                 <li><a href="index.php?pagename=add_fees&amp;action=add_new">Add New Fees </a></li>
             </ul></td>
           </tr>
       </table></td>
    </tr>
	 <tr>
       <td class="pHeadLine"></td>
    </tr>
	 <tr>
       <td>&nbsp;</td>
    </tr>
	<tr><td>
<?php 
		if($_REQUEST['action']=='ChangeStatusDone')
		{
			if($result==0)
			{
				echo "<p class='success'>Status Change Successfully</p><br />";	
			}
			else if($result==1)
			{
				echo "<p class='error'>Status Changing Fails</p><br />";	
			}
		}	
		if($data_action=='delete')
		{
		    $result = $s->delete_table_withCondition('tbl_fees ','fees_Id',$pcode);
				
			if($result)
			{
				echo "<p class='success'>".record_delete."</p><br />";	
			}
			else 
			{
				echo "<p class='error'>".record_not_delete."</p><br />";	
			}
		}
		if($_REQUEST['action']=='updateDone')
		{
			 if($result==0)
			{
				echo "<p class='success'>".record_update."</p><br />";	
			}
			else if($result==1)
			{
				echo "<p class='error'>".record_not_update."</p><br />";	
			}
		}
		else if($_REQUEST['action']=='insertDone')
		{
			 if($result==0)
			{
				echo "<p class='success'>".record_added."</p><br />";	
			}
			else if($result==1)
			{
				echo "<p class='error'>".record_not_added."</p><br />";	
			}
			
		}
	?>	</td></tr>	<tr><td width="100%">
	<table width="100%" cellpadding="0" cellspacing="0" class="tblBorder">
		<tr class="pagehead">
		  <td colspan="3" class="pad" > Fees Details </td>
		  <td align="right" nowrap="nowrap" colspan="2">Records View &nbsp; 
            <select name="records" onchange="OnSelect();"  >
          <option <?php if($max_results==10){ echo "selected='selected'";} ?> 
value="index.php?pagename=manage_fees&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=10">10</option>
          <option <?php if($max_results==20){ echo "selected='selected'";} ?>
value="index.php?pagename=manage_fees&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=20">20</option>
          <option <?php if($max_results==50){ echo "selected='selected'";} ?>
value="index.php?pagename=manage_fees&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=50">50</option>
          <option <?php if($max_results==100){ echo "selected='selected'";} ?>
value="index.php?pagename=manage_fees&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=100">100</option>
        </select> </td>
		  </tr>
<?php
	
	$rs = $s->getData_withPages('tbl_fees', $order_by, $order, $from, $max_results);
	
	if(mysqli_num_rows($rs)==0 && $page!=1)
	{
		$page=1;
		$s->pageLocation("index.php?pagename=manage_fees&orderby=fees_Id&order=$order&pageno=$page&records=$max_results"); 
	}
	$i=1;
	if(mysqli_num_rows($rs)!=0)
	{		
?>		
		<tr class="head">
		  <Td width="25%" align="center" >ID</Td>
		  <td width="40%" class="pad"> Fees</td>
		    <td width="12%" align="center" > Status		  </td>
		  <td width="23%"  ><div align="center">Action</div></td></tr>
<?php
			while($row = mysqli_fetch_object($rs))
			{
?>
		<tr class="text" onmouseover="bgr_color(this, '#EAB9BA')" onMouseOut="bgr_color(this, '')">
		<td align="center" valign="middle"><?php echo $row->fees_Id; ?></Td>
		<td class="pad" valign="middle" ><?php echo ucfirst($row->fees) ; ?></td>
		<td align="center" valign="middle">
<?php 
	if($row->status =="active")
	{
?>
<img src="images/green.gif" title="Active" border="0"  /> &nbsp; &nbsp;
<a href="index.php?pagename=manage_fees&amp;action=ChangeStatus&amp;pcode=<?php echo $row->fees_Id;?>&amp;pageno=<?php echo $page;?>&amp;records=<?php echo $max_results;?>"><img src="images/red_light.gif" title="Inactive" border="0"  /></a>
<?php
	}
	else if($row->status =="inactive")
	{
?>
<a href="index.php?pagename=manage_fees&amp;action=ChangeStatus&amp;pcode=<?php echo $row->fees_Id;?>&amp;pageno=<?php echo $page;?>&amp;records=<?php echo $max_results;?>"><img src="images/green_light.gif" title="Active" border="0"  /></a> &nbsp; &nbsp;
<img src="images/red.gif" title="Inactive" border="0"  />
<?php		
	}
?>		</td>
		<td align="center" valign="middle">
		  <div align="center"><a href="index.php?pagename=add_fees&amp;action=edit&amp;pcode=<?php echo $row->fees_Id;?>">
	      <img src="images/e.gif" border="0" alt="Edit"/></a> &nbsp; &nbsp; 
	
		  <a href="index.php?pagename=manage_fees&amp;action=delete&amp;pcode=<?php echo $row->fees_Id;?>" onclick='return del();'>
          <img src="images/x.gif" border="0" alt="Delete" /></a> </div></td></tr>
<?php 

   }
?>
<tr class='head headLink'><td colspan="8" align="right" nowrap="nowrap">
<table width="100%">
<tr><td  nowrap="nowrap">
  <div align="right">
    <?php						
		 	 $total_pages = $s->getTotal_pages('tbl_fees ',$order_by, $order,$max_results );
			if($page > 1)
			{ 
				$prev = ($page - 1); 
			echo "<ul><li><a href='index.php?pagename=manage_fees&orderby=$order_by&order=$order&pageno=$prev&records=$max_results'> Previous</a></li></ul>"; 
			} 
?>
  </div></td>
<td width="43" nowrap="nowrap" align="center">
			  <select name="pages_select" onchange="OnSelectPages();">
                <?php
			for($i = 1; $i <= $total_pages; $i++)
			{ 
?>
<option <?php if($page==$i){ echo "selected='selected'";} ?> 
value="index.php?pagename=manage_fees&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $i;?>&records=<?php echo $max_results;?>">
<?php echo $i;?></option>
<?php
			} 
?>
              </select>			</td><td width="59" nowrap="nowrap" >
              <div align="left">
                <?php		
			if($page < $total_pages)
			{ 
				$next = ($page + 1); 
				echo "<ul><li><a href='index.php?pagename=manage_fees&orderby=$order_by&order=$order&pageno=$next&records=$max_results'>Next </a></li></ul>";
			} 
			 
		?>
              </div></td>
</tr></table>
</td></tr>


<?php 
		}
		else
		{	
?>	
<tr class='text'><td colspan='8' class='redstar'> &nbsp; No record present in database.</td></tr> 
<?php
		}
?>	
	</table>
		</td></tr>
</table>
</form>
