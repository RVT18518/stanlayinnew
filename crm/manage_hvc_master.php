<?php 
//this new dynamic module for Mode master manager added by rumit on dated 27Jul 2019
	$data_action = $_REQUEST['action'];
	$pcode		 = $_REQUEST["pcode"];
	$order 		 = $_REQUEST["order"];
	$order_by    = $_REQUEST["orderby"]; 

if($order =='')
{
	$order ="asc";
}

if($order_by =='')
{
	$order_by ="hvc_id";
}
	if(!isset($_GET['pageno']))
	{ 
    	$page = 1; 
	} else { 
    $page = $_GET['pageno']; 
	} 
	if(!isset($_GET['records']))
	{ 
    	$max_results = 50; 
	} else { 
    $max_results = $_GET['records']; 
	} 
	$from = (($page * $max_results) - $max_results); 
	if($_REQUEST['action']=='update' || $_REQUEST['action']=='insert')
	{
		$fileArray["hvc_name"]        = addslashes($_REQUEST["hvc_name"]);
		$fileArray["hvc_abbrv"]       = addslashes($_REQUEST["hvc_abbrv"]);
		$fileArray["hvc_status"]      = $_REQUEST["hvc_status"];
		$fileArray["hvc_description"] = addslashes($_REQUEST["hvc_description"]);
	}

	if($_REQUEST['action'] == 'ChangeStatus')
	{
	$rs_status = $s->getData_with_condition('tbl_hvc_master','hvc_id',$pcode);
		if(mysqli_num_rows($rs_status)>0)
		{
			$row_status = mysqli_fetch_object($rs_status);
			if($row_status->hvc_status == 'active')
			{
				$fileArray["hvc_status"] = 'inactive';
			}
			else if($row_status->hvc_status == 'inactive')
			{
				$fileArray["hvc_status"] = 'active';
			}

		$result = $s->editRecord('tbl_hvc_master',$fileArray,'hvc_id',$pcode);
		
	}
	}
	if( $_REQUEST['action'] == 'update' )
	{
				$result = $s->editRecord('tbl_hvc_master',$fileArray,'hvc_id',$pcode);
	}
	if($_REQUEST['action'] == 'insert')
	{
			if($img_result != -1 )
			{
				$result = $s->insertRecord('tbl_hvc_master' ,$fileArray);
			}
}
?>
<script type="text/javascript">
function OnSelect() 
{
	window.location = document.frx1.records.value;
}
function OnSelectPages()
{
	window.location = document.frx1.pages_select.value;
}
</script>

<form name="frx1" id="frx1" action="#" method="post">
  <table width="100%" align="center" cellpadding="0" cellspacing="0" class="pagecontent">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="53%" class="pageheadTop">HVC  Manager</td>
            <td width="47%" class="headLink"><ul>
                <li><a href="index.php?pagename=add_hvc&action=add_new">Add New </a></li>
              </ul></td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td class="pHeadLine"></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><?php
		if($_REQUEST['action']=='ChangeStatus')
		{
			if($result==0)
			{
				echo "<p class='success'>Status Change Successfully</p><br/>";	
			}
			else if($result==1)
			{
				echo "<p class='error'>Status Changing Fails</p><br/>";	
			}

			$data_action = "Changed";

		}

		if($data_action=='delete')
		{
			$rs_del    = $s->getData_with_condition('tbl_hvc_master','hvc_id',$pcode);
			$row_del   = mysqli_fetch_object($rs_del);
	
			$result = $s->delete_table_withCondition('tbl_hvc_master','hvc_id',$pcode);	
			if($result)
			{
				echo "<p class='success'>".record_delete."</p><br/>";	
			}
			else 
			{
				echo "<p class='error'>".record_not_delete."</p><br/>";	
			}
		}

		if($_REQUEST['action']=='update')
		{
			 if($result==0)
			{
				echo "<p class='success'>".record_update."</p><br/>";	
			}
			else if($result==1)
			{
				echo "<p class='error'>".record_not_update."</p><br/>";	
			}
		}
		else if($_REQUEST['action']=='insert')
		{
			 if($result==0)
			{
				echo "<p class='success'>".record_added."</p><br/>";	
			}
			else if($result==1)
			{
				echo "<p class='error'>".record_not_added."</p><br/>";	
			}
		}
?>
</td>
    </tr>
    <tr>
      <td width="100%"><table width="100%" align="center" cellpadding="0" cellspacing="0" class="pagecontent">
          <tr>
            <td width="100%"><article>
                <div class="height10"></div>
                <div id="results"></div>
                <div class="loader"></div>
              </article></td>
          </tr>
        </table></td>
    </tr>
  </table>
</form>
<script src="js/jquery-1.9.0.min.js"></script> 
<script type="text/javascript">
        // fetching records
                            function displayRecords(numRecords, pageNum, orderBy, orderSe) {
                                $.ajax({
                                    type: "GET",
                                    url: "getrecords_hvc_master.php",
                                    data: "show=" + numRecords + "&pagenum=" + pageNum + "&orderby=" + orderBy + "&orderse=" + orderSe,
                                    cache: false,
                                    beforeSend: function() {
                                    $('.loader').html('<img src="loader.gif" alt="" width="24" height="24" style="padding-left: 400px; margin-top:10px;" >');
                                    },
                                    success: function(html) {
                                        $("#results").html(html);
                                        $('.loader').html('');
                                    }
                                });
                            }

        // used when user change row limit
                            function changeDisplayRowCount(numRecords,orderBy,orderSe) {
                                displayRecords(numRecords, 1, orderBy, orderSe);
                            }
							
							 function changeOrderBy(numRecords,orderBy, orderSe) {
                                displayRecords(numRecords, 1, 'asc', orderSe);
                            }

                            $(document).ready(function() {
                                displayRecords(50, 1,'desc', 'hvc_id');
                            });
</script>