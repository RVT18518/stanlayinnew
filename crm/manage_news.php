<link href="css/style.css" rel="stylesheet" type="text/css" />
<?php
	$data_action = $_REQUEST['action'];
	$pcode		 = $_REQUEST["pcode"];
	$order 		 = $_REQUEST["order"];
	$order_by    = $_REQUEST["orderby"]; 
	$img_result_large  	= -2;
	$img_result_medium 	= -2;
	$img_result_small  	= -2;	
	if(strlen(trim($order_by))<=0)
	{
		$order_by = "news_date";
	}
	if(strlen(trim($order))<=0)
	{
		$order = "asc";
	}
	
	if(!isset($_GET['pageno']))
	{ 
    	$page = 1; 
	} 
	else 
	{ 
    	$page = $_GET['pageno']; 
	} 
	if(!isset($_GET['records']))
	{ 
    	$max_results = 10; 
	} 
	else 
	{ 
    	$max_results = $_GET['records']; 
	} 
	$from = (($page * $max_results) - $max_results);  
	
	if($_REQUEST['action']=='update' ||$_REQUEST['action']=='insert')
	{
		$datexc			   	= explode("-",$_REQUEST["news_date"]); 
		$h = mktime(0, 0, 0, $datexc[1], $datexc[0], $datexc[2]);
		//echo $h;
		
		$d = date("l, F d, Y", $h) ;
		//echo "$d";
		//exit();
		$fileArray["newstitle"]          = $_REQUEST["newstitle"];	
		$fileArray["news_date"]          = $s->getDateformate($_REQUEST["news_date"],"ymd",'ydm',"-","-");
		$fileArray["date_text"]          = $d;	
		$fileArray["news_short"]         = $_REQUEST["news_desc"];	
		$fileArray["newsletter_content"] = $_REQUEST["newsletter_content"];
		$fileArray["news_by"] 			 = $_REQUEST["news_by"];
		$fileArray["status"]         	 = $_REQUEST["status"];
	}
	if($_REQUEST['action']=='ChangeStatus')
	{
		$rs_status = $s->getData_with_condition('tbl_news','newsletter_id',$pcode);
		if(mysqli_num_rows($rs_status)>0)
		{
			$row_status = mysqli_fetch_object($rs_status);
			if($row_status->status == 'active')
			{
				$fileArray["status"] = 'inactive';
			}
			else if($row_status->status == 'inactive')
			{
				$fileArray["status"] = 'active';
			}
			$result      = $s->editRecord('tbl_news',$fileArray,'newsletter_id',$pcode);
			$s->pageLocation("index.php?pagename=manage_news&action=ChangeDone&pageno=$page&records=$max_results&result=$result"); 
		}
	}
	if($_REQUEST['action']=='update')
	{
		$result = $s->editRecord('tbl_news',$fileArray,'newsletter_id',$pcode);
			
	}
	if($_REQUEST['action']=='insert')
	{
		$result = $s->insertRecord('tbl_news' ,$fileArray);
	}
?>
<script type="text/javascript">
function OnSelect() 
{
	window.location = document.frx1.records.value;
}
function OnSelectPages()
{
	window.location = document.frx1.pages_select.value;
}
</script>
<form name="frx1" id="frx1" action="#" method="post">
<table width="100%" align="center"  cellpadding="0" cellspacing="0" class="pagecontent">
	<tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="24%" class="pageheadTop">News Manager</td>
            <td width="76%" class="headLink"><ul>
                <li><a href="index.php?pagename=add_news&action=add_new">Add News</a></li>
                </ul></td>
          </tr>
      </table></td>
    </tr>
	<tr>
      <td class="pHeadLine"></td>
    </tr>
	<tr>
      <td>&nbsp;</td>
    </tr>
<tr><td>
<?php 
	if($_REQUEST['action']=='ChangeDone')
	{
		$result = $_REQUEST['result'];
		if($result==0)
		{
			echo "<p class='success'>Status Change Successfully</p><br />";	
		}
		else if($result==1)
		{
			echo "<p class='error'>Status Changing Fails</p><br />";	
		}
	}		
	if($_REQUEST['action']=='mailSend')
	{
		$i = $_REQUEST["i"];
		if($i >0)
		{
			echo "<p class='success'> Newsletter Send to $i Custmors or Subscribers by E-mail. </p><br />";
		}
		else
		{
			echo "<p class='error'> No newsletter Send to the Custmors or Subscribers by E-mail. </p><br />";
		}
	}
	if($data_action=='delete')
	{
		$result = $s->delete_table_withCondition('tbl_news','newsletter_id',$pcode);	
		if($result)
		{
			echo "<p class='success'>".record_delete."</p><br />";	
		}
		else 
		{
			echo "<p class='error'>".record_not_delete."</p><br />";	
		}
	}
	if( $_REQUEST['action'] == 'update' )
	{
		 if($result==0)
		{
			echo "<p class='success'>".record_update."</p><br />";	
		}
		else if($result==1)
		{
			echo "<p class='error'>".record_not_update."</p><br />";	
		}
	}
	else if($_REQUEST['action']=='insert')
	{
		 if($result==0)
		{
			echo "<p class='success'>".record_added."</p><br />";	
		}
		else if($result==1)
		{
			echo "<p class='error'>".record_not_added."</p><br />";	
		}
	}
?>	
		</td>
	</tr>
	<tr><td width="100%"><table width="100%" cellpadding="0" cellspacing="0" class="tblBorder">
      <tr class="pagehead">
        <td colspan="4" class="pad" >Manage News</td>
		<td nowrap="nowrap"> <div align="right">Records View &nbsp; 
            <select name="records" onchange="OnSelect();"  >
          <option <? if($max_results==10){ echo "selected='selected'";} ?> 
value="index.php?pagename=manage_news&orderby=<? echo $order_by;?>&order=<? echo $order;?>&pageno=<? echo $page;?>&records=10">10</option>
          <option <? if($max_results==20){ echo "selected='selected'";} ?>
value="index.php?pagename=manage_news&orderby=<? echo $order_by;?>&order=<? echo $order;?>&pageno=<? echo $page;?>&records=20"> 
            20</option>
          <option <? if($max_results==50){ echo "selected='selected'";} ?>
value="index.php?pagename=manage_news&orderby=<? echo $order_by;?>&order=<? echo $order;?>&pageno=<? echo $page;?>&records=50"> 
            50</option>
          <option <? if($max_results==100){ echo "selected='selected'";} ?>
value="index.php?pagename=manage_news&orderby=<? echo $order_by;?>&order=<? echo $order;?>&pageno=<? echo $page;?>&records=100"> 
            100</option>
        </select>
  </div> </td>
      </tr>
<?php
	if($order_by == '')
	{
		$order_by = 'newsletter_id';	
	}
		
	$rs = $s->getData_withPages('tbl_news',$order_by, $order, $from,$max_results);
	
	if(mysqli_num_rows($rs)==0 && $page!=1)
	{
		$page=1;
		$s->pageLocation("index.php?pagename=manage_news&pageno=$page&records=$max_results"); 
	}
	if($order == "asc")
	{
		$order_new = "desc";
	}
	else if($order == "desc")
	{
		$order_new = "asc";
	}
	
	$i=1;
	if(mysqli_num_rows($rs)!=0)
	{
?>	
	<tr class="head">
    <td width="10%" align="center" class="pad"><a href="index.php?pagename=manage_news&orderby=newsletter_id&order=<?php echo $order_new; ?>&pageno=<?php echo $page;?>&records=<?php echo $max_results;?>">ID</a></td>
	<td width="10%" class="pad"><a href="index.php?pagename=manage_news&orderby=news_date&order=<?php echo $order_new; ?>&pageno=<?php echo $page;?>&records=<?php echo $max_results;?>">Date</a> </td>
    <td width="46%" class="pad"><a href="index.php?pagename=manage_news&orderby=newstitle&order=<?php echo $order_new; ?>&pageno=<?php echo $page;?>&records=<?php echo $max_results;?>">News Title</a></td>
	
	<td width="19%"  align="center" nowrap="nowrap">
	Status</td>
	<td width="15%"  align="center" nowrap="nowrap">Action</td>
	</tr>
<?
	while($row = mysqli_fetch_object($rs))
	{
?>	  
      <tr class="text" onmouseover="bgr_color(this, '#EAB9BA')" onMouseOut="bgr_color(this, '')">
        <td align="center" class="pad"><? echo $row->newsletter_id;?></td>
		<td class="pad">
		<? 
		echo $s->getDateformate($row->news_date,"mdy",'dmy',"-","-");
		
		?> </td>
        <td class="pad"><? echo $row->newstitle;?> </td>
        
       
        <td  align="center" nowrap="nowrap">
		<?php 
	if($row->status =="active")
	{
?>
<img src="images/green.gif" title="Active" border="0"  /> &nbsp; &nbsp;
<a href="index.php?pagename=manage_news&action=ChangeStatus&pcode=<?php echo $row->newsletter_id;?>&pageno=<?php echo $page;?>&records=<?php echo $max_results;?>"><img src="images/red_light.gif" title="Inactive" border="0"  /></a>
<?php
	}
	else if($row->status =="inactive")
	{
?>
<a href="index.php?pagename=manage_news&action=ChangeStatus&pcode=<?php echo $row->newsletter_id;?>&pageno=<?php echo $page;?>&records=<?php echo $max_results;?>"><img src="images/green_light.gif" title="Active" border="0"  /></a> &nbsp; &nbsp;
<img src="images/red.gif" title="Inactive" border="0"  />
<?php		
	}
?>	
		</td>
        <td  align="center" nowrap="nowrap">
		<!--<a href="index.php?pagename=news_view&action=view&pcode=<? echo $row->newsletter_id;?>">
		<img src="images/view.gif" border="0" title="View"/></a>--> 
		<a href="index.php?pagename=add_news&action=edit&pcode=<? echo $row->newsletter_id;?>">
		<img src="images/e.gif" border="0" title="Edit"/></a> &nbsp;  &nbsp; 
		<a href="index.php?pagename=manage_news&action=delete&pcode=<? echo $row->newsletter_id;?>&pageno=<? echo $page;?>&records=<? echo $max_results;?>" onclick='return del();'>
		<img src="images/x.gif" border="0" title="Delete" /></a> </td>
      </tr>
<? 
		$i++;
	}
?>
<tr class='head headLink'><td colspan="5" align="right" nowrap="nowrap">
<table width="100%">
<tr><td  nowrap="nowrap">
  <div align="right">
<?php
		 	$total_pages = $s->getTotal_pages('tbl_news','newsletter_id', 'asc',$max_results );
			if($page > 1)
			{
				$prev = ($page - 1);
				echo "<ul><li><a href='index.php?pagename=manage_news&orderby=$order_by&order=$order&pageno=$prev&records=$max_results'>Previous</a></li></ul>"; 
			}
?>
  </div></td>
<td width="43" nowrap="nowrap" align="center">
			  <select name="pages_select" onchange="OnSelectPages();"  >
<?php
			for($i = 1; $i <= $total_pages; $i++)
			{ 
?>
<option <? if($page==$i){ echo "selected='selected'";} ?> value="index.php?pagename=manage_news&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<? echo $i;?>&records=<? echo $max_results;?>"><? echo $i;?></option>
	       <?
			} 
?>
              </select>
			</td><td width="59" nowrap="nowrap" >
              <div align="left">
                <?		
			if($page < $total_pages)
			{ 
				$next = ($page + 1); 
				echo "<ul><li><a href='index.php?pagename=manage_news&orderby=$order_by&order=$order&pageno=$next&records=$max_results'>Next </a></li></ul>";
			} 
		?>
              </div></td>
</tr></table>
</td></tr>

<?
}
else
{
?>
<tr class='text'><td colspan='5' class='redstar'> &nbsp; No record present in database.</td></tr> 
<? 
 }
?>
    </table></td>
	</tr>
</table>
</form>
