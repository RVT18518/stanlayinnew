<?php
	$data_action = $_REQUEST['action'];
	$pcode		 = $_REQUEST["pcode"];
	$order 		 = 'asc';
	$order_by    = 'news_event_id';
	$result	     = $_REQUEST["result"]; 
	if(!isset($_GET['pageno']))
	{
    	$page = 1;
	}
	else 
	{ 
    	$page = $_GET['pageno']; 
	} 
	if(!isset($_GET['records']))
	{ 
    	$max_results = 10; 
	} 
	else 
	{
    	$max_results = $_GET['records']; 
	} 
	$from = (($page * $max_results) - $max_results);  
	
	if($_REQUEST['action']=='update' ||$_REQUEST['action']=='insert')
	{
		$file_array["event_title"] 	= $_REQUEST["event_title"];
		$file_array["date1"] 		= $_REQUEST["date1"];
		$file_array["sort_date"] 	= $s->getDateformate($_REQUEST["sort_date"],'dmy',"ymd","-","-");//exit();
		if($_FILES["event_logo"]["name"] != "")
		{
			$rs_del    			= $s->getData_with_condition('tbl_news_event','news_event_id',$pcode);
			$row_del   			= mysqli_fetch_object($rs_del);
			$del_image_small  	= "../".$row_del->event_logo_small;
			$del_image_large  	= "../".$row_del->event_logo;
			if(file_exists($del_image_large)>0)
			{
				unlink($del_image_large);
			}

			if(file_exists($del_image_small)>0)
			{
				unlink($del_image_small);
			}

			
			
			$filePath_small 		= $s->ImageUpload("uploads/news_event/small/", "event_logo", "MEDIA_", "94", "62");
			$filePath = $s->fileUpload("uploads/news_event/", "event_logo", "MEDIA_");
			if($filePath != -1)
			{
				$file_array["event_logo"] = $filePath;
			}
			if($filePath_small != -1)
			{
				$file_array["event_logo_small"] = $filePath_small;
			}
			
		}
		$file_array["description"] 	= $_REQUEST["description"];
		$file_array["venue"] 		= addslashes($_REQUEST["venue"]);
		$file_array["status"] 		= $_REQUEST["status"];		
	}
	if($_REQUEST['action']=='ChangeStatus')
	{
		$rs_status = $s->getData_with_condition('tbl_news_event ','news_event_id',$pcode);
		if(mysqli_num_rows($rs_status)>0)
		{
			$row_status = mysqli_fetch_object($rs_status);
			if($row_status->status == 'active')
			{
				$fileArray["status"] = 'inactive';
			}
			else if($row_status->status == 'inactive')
			{
				$fileArray["status"] = 'active';
			}
			$result = $s->editRecord('tbl_news_event', $fileArray, 'news_event_id', $pcode);
			$s->pageLocation("index.php?pagename=manage_news_event&action=ChangeStatusDone&result=$result"); 
		}
	}
	
	if($_REQUEST['action']=='update')
	{
		$result 	 = $s->editRecord('tbl_news_event ',$file_array,'news_event_id',$pcode);
		$data_action = "updateDone";
		$s->pageLocation("index.php?pagename=manage_news_event&action=$data_action&pcode=$pcode&result=$result"); 
	}
	
	if($_REQUEST['action']=='insert')
	{
		$result 	 = $s->insertRecord('tbl_news_event ', $file_array);
		$data_action = "insertDone";
		$s->pageLocation("index.php?pagename=manage_news_event&action=$data_action&pcode=$pcode&result=$result"); 
	}
?>
<script type="text/javascript">
function OnSelect() 
{
	window.location = document.frx1.records.value;
}
function OnSelectPages()
{
	window.location = document.frx1.pages_select.value;
}
</script>

<form name="frx1" id="frx1" action="#" method="post">
  <table width="100%" align="center" cellpadding="0" cellspacing="0" class="pagecontent">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="55%" class="pageheadTop">News &amp;  Event Manager</td>
            <td width="45%" class="headLink"><ul>
                <li><a href="index.php?pagename=add_news_event&action=add_new">Add News Event </a></li>
              </ul></td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td class="pHeadLine"></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><?php 
		if($_REQUEST['action']=='ChangeStatusDone')
		{
			if($result==0)
			{
				echo "<p class='success'>Status Change Successfully</p><br />";	
			}
			else if($result==1)
			{
				echo "<p class='error'>Status Changing Fails</p><br />";	
			}
		}
		if($data_action=='delete')
		{
			$result = $s->delete_table_withCondition('tbl_news_event ','news_event_id',$pcode);	
			if($result)
			{
				echo "<p class='success'>".record_delete."</p><br />";	
			}
			else 
			{
				echo "<p class='error'>".record_not_delete."</p><br />";	
			}
		}
		if($_REQUEST['action']=='updateDone')
		{
			 if($result==0)
			{
				echo "<p class='success'>".record_update."</p><br />";	
			}
			else if($result==1)
			{
				echo "<p class='error'>".record_not_update."</p><br />";	
			}
		}
		else if($_REQUEST['action']=='insertDone')
		{
			 if($result==0)
			{
				echo "<p class='success'>".record_added."</p><br />";	
			}
			else if($result==1)
			{
				echo "<p class='error'>".record_not_added."</p><br />";	
			}
		}
	?></td>
    </tr>
    <tr>
      <td width="100%"><table width="100%" cellpadding="0" cellspacing="0" class="tblBorder">
          <tr class="pagehead">
            <td colspan="5" class="pad" nowrap="nowrap" > News &amp; Event Details </td>
            <td align="center" nowrap="nowrap">Records View&nbsp;
              <select name="records" onchange="OnSelect();"  >
                <option <?php if($max_results==10){ echo "selected='selected'";} ?> 
value="index.php?pagename=manage_news_event&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=10">10</option>
                <option <?php if($max_results==20){ echo "selected='selected'";} ?>
value="index.php?pagename=manage_news_event&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=20">20</option>
                <option <?php if($max_results==50){ echo "selected='selected'";} ?>
value="index.php?pagename=manage_news_event&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=50">50</option>
                <option <?php if($max_results==100){ echo "selected='selected'";} ?>
value="index.php?pagename=manage_news_event&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=100">100</option>
              </select></td>
          </tr>
          <?php
	$searchRecord = " 1 = 1 ";
	$rs = $s->getData_withPages('tbl_news_event ',$order_by, $order,$from,$max_results, $searchRecord);
	
	if(mysqli_num_rows($rs)==0 && $page!=1)
	{
		$page=1;
		$s->pageLocation("index.php?pagename=manage_news_event&orderby=news_event_id&order=$order&pageno=$page&records=$max_results"); 
	}
	$i=1;
	if(mysqli_num_rows($rs)>0)
	{		
?>
          <tr class="head">
            <Td width="12%" align="center" >ID</Td>
            <td width="12%" class="pad">Date</td>
            <td width="27%" class="pad" >News &amp; Event Title</td>
            <td width="20%" class="pad" >Venue</td>
            <td width="14%" align="center">Status</td>
            <td width="15%" align="center">Action</td>
          </tr>
          <?php
			while($row = mysqli_fetch_object($rs))
			{
?>
          <tr class="text" onmouseover="bgr_color(this, '#FFFF99')" onMouseOut="bgr_color(this, '')">
            <td align="center" valign="middle"><?php echo $row->news_event_id; ?></td>
            <td class="pad" valign="middle"><?php echo $row->date1; ?></td>
            <td class="pad" valign="middle"><?php echo ucfirst(stripslashes($row->event_title)); ?></td>
            <td class="pad" valign="middle" ><?php echo ucfirst(stripslashes($row->venue)); ?></td>
            <td align="center" valign="middle"><?php 
	if($row->status =="active")
	{
?>
              <img src="images/green.gif" title="Active" border="0"  /> &nbsp; &nbsp; <a href="index.php?pagename=manage_news_event&action=ChangeStatus&pcode=<?php echo $row->news_event_id;?>&pageno=<?php echo $page;?>&records=<?php echo $max_results;?>"><img src="images/red_light.gif" title="Inactive" border="0"  /></a>
              <?php
	}
	else if($row->status =="inactive")
	{
?>
              <a href="index.php?pagename=manage_news_event&action=ChangeStatus&pcode=<?php echo $row->news_event_id;?>&pageno=<?php echo $page;?>&records=<?php echo $max_results;?>"><img src="images/green_light.gif" title="Active" border="0"  /></a> &nbsp; &nbsp; <img src="images/red.gif" title="Inactive" border="0"  />
              <?php		
	}
?></td>
            <td align="center" valign="middle"><a href="index.php?pagename=add_news_event&action=edit&pcode=<?php echo $row->news_event_id;?>"> <img src="images/e.gif" border="0"  alt="Edit"/></a> &nbsp; &nbsp; <a href="index.php?pagename=manage_news_event&action=delete&pcode=<?php echo $row->news_event_id;?>" onclick='return del();'> <img src="images/x.gif" border="0" alt="Delete" /></a></td>
          </tr>
          <?php 
		
			}
	
?>
          <tr class='head headLink'>
            <td colspan="6" align="right" nowrap="nowrap"><table width="100%">
                <tr>
                  <td  nowrap="nowrap" align="right"><?php						
		 	$total_pages = $s->getTotal_pages('tbl_news_event', $order_by, $order, $max_results, $searchRecord);
			if($page > 1)
			{ 
			$prev = ($page - 1); 
			echo "<ul><li><a href='index.php?pagename=manage_news_event&orderby=$order_by&order=$order&pageno=$prev&records=$max_results'> Previous</a></li></ul>"; 
			} 
?></td>
                  <td width="43" nowrap="nowrap" align="center"><select name="pages_select" onchange="OnSelectPages();">
                      <?php
			for($i = 1; $i <= $total_pages; $i++)
			{ 
?>
                      <option <?php if($page==$i){ echo "selected='selected'";} ?> 
value="index.php?pagename=manage_news_event&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $i;?>&records=<?php echo $max_results;?>"> <?php echo $i;?></option>
                      <?php
			} 
?>
                    </select></td>
                  <td width="59" nowrap="nowrap" align="left"><?php		
			if($page < $total_pages)
			{ 
				$next = ($page + 1); 
				echo "<ul><li><a href='index.php?pagename=manage_news_event&orderby=$order_by&order=$order&pageno=$next&records=$max_results'>Next </a></li></ul>";
			} 
?></td>
                </tr>
              </table></td>
          </tr>
          <?php 
		}
		else
		{	
?>
          <tr class='text'>
            <td colspan='7' class='redstar'>&nbsp; No record present in database.</td>
          </tr>
          <?php
		}
?>
        </table></td>
    </tr>
  </table>
</form>
