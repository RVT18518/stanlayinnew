<?php

	$data_action = $_REQUEST['action'];

	$pcode		 = $_REQUEST["pcode"];

	$order 		 = $_REQUEST["order"];

	$order_by    = $_REQUEST["orderby"]; 

	$result		 = $_REQUEST["result"];

	if(!isset($_GET['pageno']))

	{ 

    	$page = 1; 

	} 

	else 

	{ 

    	$page = $_GET['pageno']; 

	} 

	if(!isset($_GET['records']))

	{ 

    	$max_results = 100; 

	} 

	else 

	{ 

    	$max_results = $_GET['records']; 

	} 

	$from = (($page * $max_results) - $max_results);  

	

	if($_REQUEST['action']=='update' ||$_REQUEST['action']=='insert')

	{

		$dataArray["module_id"]    	  = $_REQUEST["module_id"];

		$dataArray["page_title"] 	  = $_REQUEST["page_title"];

		$dataArray["page_name"]		  = $_REQUEST["page_name"];

		$dataArray["display_order"]   = $_REQUEST["display_order"];

		$dataArray["page_type"]		  = $_REQUEST["page_type"];

		$dataArray["manager_page_id"] = $_REQUEST["manager_page"];

		$dataArray["help_text"]		  = $_REQUEST["helpText"];

	}

	if($_REQUEST['action']=='update')

	{

		$result = $s->editRecord('tbl_website_page',$dataArray,'page_id',$pcode);

		if($result == 0)

		{

			$s->pageLocation("index.php?pagename=manage_website_page&action=updateDone&result=0");

		}

		else if($result == 1)

		{

			$s->pageLocation("index.php?pagename=manage_website_page&action=updateDone&result=1");

		}

	}

	if($_REQUEST['action']=='insert')

	{

		$result = $s->insertRecord('tbl_website_page',$dataArray);

		if($result == 0)

		{

			$s->pageLocation("index.php?pagename=manage_website_page&action=insertDone&result=0");

		}

		else if($result == 1)

		{

			$s->pageLocation("index.php?pagename=manage_website_page&action=insertDone&result=1");

		}

	}

?>

<script type="text/javascript">

function OnSelect() 

{

	window.location = document.frx1.records.value;

}

function OnSelectPages()

{

	window.location = document.frx1.pages_select.value;

}

</script>

<form name="frx1" id="frx1" action="#" method="post">

<table width="100%" align="center" cellpadding="0" cellspacing="0" class="pagecontent">

	

	 <tr>

       <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

           <tr>

              <td width="24%" class="pageheadTop" nowrap>Website PHP Page Manager</td>

             <td width="76%" class="headLink"><ul>

                 <li><a href="index.php?pagename=add_page_php&action=add_new">Add PHP Page </a></li>

             </ul></td>

           </tr>

       </table></td>

    </tr>

	 <tr>

       <td class="pHeadLine"></td>

    </tr>

	 <tr>

       <td>&nbsp;</td>

    </tr>

	<tr><td>

<?php 

		if($data_action=='delete')

		{

			$result = $s->delete_table_withCondition('tbl_website_page','page_id',$pcode);	

			if($result)

			{

				echo "<p class='success'>".record_delete."</p><br />";	

			}

			else 

			{

				echo "<p class='error'>".record_not_delete."</p><br />";	

			}

		}

		if($_REQUEST['action']=='updateDone')

		{

			 if($result==0)

			{

				echo "<p class='success'>".record_update."</p><br />";	

			}

			else if($result==1)

			{

				echo "<p class='error'>".record_not_update."</p><br />";	

			}

		}

		else if($_REQUEST['action']=='insertDone')

		{

			 if($result==0)

			{

				echo "<p class='success'>".record_added."</p><br />";	

			}

			else if($result==1)

			{

				echo "<p class='error'>".record_not_added."</p><br />";	

			}

			

		}

	?>	</td></tr>	

	<tr><td width="100%">

	<table width="100%" cellpadding="0" cellspacing="0" class="tblBorder">

		<tr class="pagehead">

		  <td colspan="6" > Pages Details </td>

		  <td align="right" nowrap>Records View &nbsp; 

        <select name="records" onchange="OnSelect();"  >

          <option <?php if($max_results==10){ echo "selected='selected'";} ?> 

value="index.php?pagename=manage_website_page&pageno=<?php echo $page;?>&records=10">10</option>

          <option <?php if($max_results==20){ echo "selected='selected'";} ?>

value="index.php?pagename=manage_website_page&pageno=<?php echo $page;?>&records=20">20</option>

          <option <?php if($max_results==50){ echo "selected='selected'";} ?>

value="index.php?pagename=manage_website_page&pageno=<?php echo $page;?>&records=50">50</option>

          <option <?php if($max_results==100){ echo "selected='selected'";} ?>

value="index.php?pagename=manage_website_page&pageno=<?php echo $page;?>&records=100">100</option>

        </select> </td>

		  </tr>

<?php

	

	$rs = $s->getData_withPages('tbl_website_page','page_id','asc',$from,$max_results);

	

	if(mysqli_num_rows($rs)==0 && $page!=1)

	{

		$page=1;

		$s->pageLocation("index.php?pagename=manage_website_page&pageno=$page&records=$max_results"); 

	}

	$i=1;

	if(mysqli_num_rows($rs)!=0)

	{		

?>		

		<tr class="head">

		  <Td width="11%" align="center" >ID</Td>

		  <td width="17%" class="pad">Module Name </td>

		  <td width="16%" class="pad">Page Type </td>

		  <td width="16%" class="pad">

		  Page Title</td>

		  <td width="17%" class="pad"> Page Name </td>

		  <td width="8%" nowrap align="center" >Display Order </td>

		  

		  <td width="15%"  ><div align="center">Action</div></td></tr>

<?php

			while($row = mysqli_fetch_object($rs))

			{

?>

		<tr class="text" onmouseover="bgr_color(this, '#FFFF99')" onMouseOut="bgr_color(this, '')">

		<td align="center" valign="top"><?php echo $row->page_id; ?></Td>

		<td valign="top" nowrap class="pad" >

		<?php 

			/*$rs_module	 = $s->getData_with_condition('tbl_module','module_id',$row->module_id);

			$row_module  = mysqli_fetch_object($rs_module);

			echo $row_module->module_name;
*/
		?></td>

		<td class="pad" valign="top" >

		<?php  

			if($row->page_type == 0)

			{

				echo "Manager Page";

			}

			else if($row->page_type == 1)

			{

				echo "Sub Page";

			}

		 ?></td>

		<td valign="top" nowrap class="pad" ><?php echo $row->page_title; ?></td>

		<td valign="top" nowrap class="pad"><?php echo $row->page_name; ?>	</td>

		<td nowrap  valign="top"><div align="center">		  <?php echo $row->display_order; ?> </div></td>

		

		<td align="center" valign="top">

		  <div align="center"><a href="index.php?pagename=add_page_php&action=edit&pcode=<?php echo $row->page_id;?>">

	      <img src="images/e.gif" border="0"  alt="Edit"/></a> &nbsp; &nbsp; 

		  <a href="index.php?pagename=manage_website_page&action=delete&pcode=<?php echo $row->page_id;?>" onclick='return del();'>

          <img src="images/x.gif" border="0" alt="Delete" /></a> </div></td></tr>

<?php 

				$i++;

			}

?>

<tr class='head headLink'><td colspan="7" align="right" nowrap="nowrap">

<table width="100%">

<tr><td  nowrap="nowrap">

  <div align="right">

    <?php						

		 	$total_pages = $s->getTotal_pages('tbl_website_page','page_id','asc',$max_results );

			if($page > 1)

			{ 

				$prev = ($page - 1); 

			echo "<ul><li><a href='index.php?pagename=manage_website_page&pageno=$prev&records=$max_results'>< Previous</a></li></ul>"; 

			} 

?>

  </div></td>

<td width="43" nowrap="nowrap" align="center">

			  <select name="pages_select" onchange="OnSelectPages();">

                <?php

			for($i = 1; $i <= $total_pages; $i++)

			{ 

?>

<option <?php if($page==$i){ echo "selected='selected'";} ?> 

value="index.php?pagename=manage_website_page&pageno=<?php echo $i;?>&records=<?php echo $max_results;?>">

<?php echo $i;?></option>

<?php

			} 

?>

              </select>

			</td><td width="59" nowrap="nowrap" >

              <div align="left">

                <?php		

			if($page < $total_pages)

			{ 

				$next = ($page + 1); 

				echo "<ul><li><a href='index.php?pagename=manage_website_page&pageno=$next&records=$max_results'>Next ></a></li></ul>";

			} 

			 

		?>

              </div></td>

</tr></table>

</td></tr>

<?php 

		}

		else

		{	

?>	

<tr class='text'><td colspan='7' class='redstar'> &nbsp; No record present in database.</td></tr> 

<?php

		}

?>	

	</table>

		</td></tr>

</table>

</form>

