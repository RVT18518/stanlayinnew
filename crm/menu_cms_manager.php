
<link href="css/style.css" rel="stylesheet" type="text/css" />
<?php
	$data_action = $_REQUEST['action'];
	$pcode		 = $_REQUEST["pcode"];
	$order 		 = 'asc';
	$order_by    = 'menu_id';
	$result	     = $_REQUEST["result"]; 
	if(!isset($_GET['pageno']))
	{ 
    	$page = 1; 
	} 
	else 
	{ 
    	$page = $_GET['pageno']; 
	} 
	if(!isset($_GET['records']))
	{ 
    	$max_results = 10; 
	} 
	else 
	{ 
    	$max_results = $_GET['records']; 
	} 
	$from = (($page * $max_results) - $max_results);  
	
	if($_REQUEST['action']=='update' ||$_REQUEST['action']=='insert')
	{
		$file_array["menu_title"]    = $_REQUEST["menu_title"];	
		$file_array["menu_position"] = $_REQUEST["menu_position"];
		$file_array["menu_status"]   = $_REQUEST["menu_status"];
	}
	if($_REQUEST['action']=='ChangeStatus')
	{
		$rs_status = $s->getData_with_condition('tbl_menu_cms','menu_id',$pcode);
		if(mysqli_num_rows($rs_status)>0)
		{
			$row_status = mysqli_fetch_object($rs_status);
			if($row_status->menu_status == 'active')
			{
				$fileArray["menu_status"] = 'inactive';
			}
			else if($row_status->menu_status == 'inactive')
			{
				$fileArray["menu_status"] = 'active';
			}
			$result      = $s->editRecord('tbl_menu_cms',$fileArray,'menu_id',$pcode);
		$s->pageLocation("index.php?pagename=menu_cms_manager&action=ChangeStatusDone&result=$result&orderby=menu_id&order=$order&pageno=$page&records=$max_results"); 
		}
	}
	if($_REQUEST['action']=='update')
	{
		$result = $s->editRecord('tbl_menu_cms',$file_array,'menu_id',$pcode);
		$data_action = "updateDone";
		$s->pageLocation("index.php?pagename=menu_cms_manager&action=$data_action&pcode=$pcode&result=$result"); 
	}
	if($_REQUEST['action']=='insert')
	{
		print_r($file_array);
		$result = $s->insertRecord('tbl_menu_cms',$file_array);
		$data_action = "insertDone";
		$s->pageLocation("index.php?pagename=menu_cms_manager&action=$data_action&pcode=$pcode&result=$result"); 
	}
?>
<script type="text/javascript">
function OnSelect() 
{
	window.location = document.frx1.records.value;
}
function OnSelectPages()
{
	window.location = document.frx1.pages_select.value;
}
</script>
<form name="frx1" id="frx1" action="#" method="post">
<table width="100%" align="center" cellpadding="0" cellspacing="0" class="pagecontent">
	
	 <tr>
       <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
           <tr>
             <td width="24%" class="pageheadTop">Menu CMS Manager</td>
             <td width="76%" class="headLink"><ul>
                 <li><a href="index.php?pagename=add_menu_cms&action=add_new">Add New Menu </a></li>
             </ul></td>
           </tr>
       </table></td>
    </tr>
	 <tr>
       <td class="pHeadLine"></td>
    </tr>
	 <tr>
       <td>&nbsp;</td>
    </tr>
	<tr><td>
<?php 
		if($_REQUEST['action']=='ChangeStatusDone')
		{
			if($result==0)
			{
				echo "<p class='success'>Status Change Successfully</p><br />";	
			}
			else if($result==1)
			{
				echo "<p class='error'>Status Changing Fails</p><br />";	
			}
		}	
		if($data_action=='delete')
		{
			$result = $s->delete_table_withCondition('tbl_menu_cms','menu_id',$pcode);	
			$result = $s->delete_table_withCondition('tbl_cms_pages ','menu_title',$pcode);	
			if($result)
			{
				echo "<p class='success'>".record_delete."</p><br />";	
			}
			else 
			{
				echo "<p class='error'>".record_not_delete."</p><br />";	
			}
		}
		if($_REQUEST['action']=='updateDone')
		{
			 if($result==0)
			{
				echo "<p class='success'>".record_update."</p><br />";	
			}
			else if($result==1)
			{
				echo "<p class='error'>".record_not_update."</p><br />";	
			}
		}
		else if($_REQUEST['action']=='insertDone')
		{
			 if($result==0)
			{
				echo "<p class='success'>".record_added."</p><br />";	
			}
			else if($result==1)
			{
				echo "<p class='error'>".record_not_added."</p><br />";	
			}
			
		}
	?>	</td></tr>	<tr><td width="100%">
	<table width="100%" cellpadding="0" cellspacing="0" class="tblBorder">
		<tr class="pagehead">
		  <td colspan="6" class="pad" > CMS Menu Details </td>
		  <td align="center">Records View &nbsp; 
        <select name="records" onchange="OnSelect();"  >
          <option <?php if($max_results==10){ echo "selected='selected'";} ?> 
value="index.php?pagename=menu_cms_manager&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=10">10</option>
          <option <?php if($max_results==20){ echo "selected='selected'";} ?>
value="index.php?pagename=menu_cms_manager&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=20">20</option>
          <option <?php if($max_results==50){ echo "selected='selected'";} ?>
value="index.php?pagename=menu_cms_manager&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=50">50</option>
          <option <?php if($max_results==100){ echo "selected='selected'";} ?>
value="index.php?pagename=menu_cms_manager&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=100">100</option>
        </select> </td>
		  </tr>
<?php
	
	$rs = $s->getData_withPages('tbl_menu_cms',$order_by, $order,$from,$max_results);
	
	if(mysqli_num_rows($rs)==0 && $page!=1)
	{
		$page=1;
		$s->pageLocation("index.php?pagename=menu_cms_manager&orderby=menu_id&order=$order&pageno=$page&records=$max_results"); 
	}
	$i=1;
	if(mysqli_num_rows($rs)!=0)
	{		
?>		
		<tr class="head">
		  <Td width="11%" align="center" >ID</Td>
		  <td width="55%" class="pad"> Menu Title		 </td>
		  <td width="13%"align="center">Menu Position </td>
		  <td width="1%" nowrap class="pad" >&nbsp;</td>
		  <td width="1%" nowrap class="pad">&nbsp;</td>
		  <td width="6%" align="center" > Status
		  </td>
		  <td width="13%"  ><div align="center">Action</div></td></tr>
<?php
			while($row = mysqli_fetch_object($rs))
			{
?>
		<tr class="text" onmouseover="bgr_color(this, '#EAB9BA')" onMouseOut="bgr_color(this, '')">
		<td align="center" valign="middle"><?php echo $row->menu_id; ?></Td>
		<td class="pad" valign="middle" ><?php echo ucfirst($row->menu_title) ; ?></td>
		<td align="center" valign="middle"><?php echo ucfirst($row->menu_position) ; ?></td>
		<td nowrap class="pad" valign="middle">&nbsp;</td>
		<td nowrap class="pad" valign="middle">&nbsp;</td>
		<td align="center" valign="middle">
<?php 
	if($row->menu_status =="active")
	{
?>
<img src="images/green.gif" title="Active" border="0"  /> &nbsp; &nbsp;
<a href="index.php?pagename=menu_cms_manager&action=ChangeStatus&pcode=<?php echo $row->menu_id;?>&pageno=<?php echo $page;?>&records=<?php echo $max_results;?>"><img src="images/red_light.gif" title="Inactive" border="0"  /></a>
<?php
	}
	else if($row->menu_status =="inactive")
	{
?>
<a href="index.php?pagename=menu_cms_manager&action=ChangeStatus&pcode=<?php echo $row->menu_id;?>&pageno=<?php echo $page;?>&records=<?php echo $max_results;?>"><img src="images/green_light.gif" title="Active" border="0"  /></a> &nbsp; &nbsp;
<img src="images/red.gif" title="Inactive" border="0"  />
<?php		
	}
?>	
		</td>
		<td align="center" valign="middle">
		  <div align="center"><a href="index.php?pagename=add_menu_cms&action=edit&pcode=<?php echo $row->menu_id;?>">
	      <img src="images/e.gif" border="0"  alt="Edit"/></a> &nbsp; &nbsp; 
<?php
$mID	= $row->menu_id;
if($mID == 5 || $mID == 6 || $mID == 7)
{
?>		
 <img src="images/protect.gif" border="0" alt="Protected" />		
<?php
}
else
{
?>
  <a href="index.php?pagename=menu_cms_manager&action=delete&pcode=<?php echo $row->menu_id;?>" onclick='return del();'>
          <img src="images/x.gif" border="0" alt="Delete" /></a> 
<?php
}
?>
</div></td></tr>
<?php
	}
?>
<tr class='head headLink'><td colspan="10" align="right" nowrap="nowrap">
<table width="100%">
<tr><td  nowrap="nowrap">
  <div align="right">
    <?php						
		 	$total_pages = $s->getTotal_pages('tbl_menu_cms',$order_by, $order,$max_results );
			if($page > 1)
			{ 
				$prev = ($page - 1); 
			echo "<ul><li><a href='index.php?pagename=menu_cms_manager&orderby=$order_by&order=$order&pageno=$prev&records=$max_results'> Previous</a></li></ul>"; 
			} 
?>
  </div></td>
<td width="43" nowrap="nowrap" align="center">
			  <select name="pages_select" onchange="OnSelectPages();">
                <?php
			for($i = 1; $i <= $total_pages; $i++)
			{ 
?>
<option <?php if($page==$i){ echo "selected='selected'";} ?> 
value="index.php?pagename=menu_cms_manager&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $i;?>&records=<?php echo $max_results;?>">
<?php echo $i;?></option>
<?php
			} 
?>
              </select>			</td><td width="59" nowrap="nowrap" >
              <div align="left">
                <?php		
			if($page < $total_pages)
			{ 
				$next = ($page + 1); 
				echo "<ul><li><a href='index.php?pagename=menu_cms_manager&orderby=$order_by&order=$order&pageno=$next&records=$max_results'>Next </a></li></ul>";
			} 
			 
		?>
              </div></td>
</tr></table>
</td></tr>


<?php 
		}
		else
		{	
?>	
<tr class='text'><td colspan='10' class='redstar'> &nbsp; No record present in database.</td></tr> 
<?php
		}
?>	
	</table>
		</td></tr>
</table>
</form>
