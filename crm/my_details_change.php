<?php
	//include('query_checking.php');	
	$data_action = $_REQUEST["action"];
	$pcode 		 = $_SESSION["AdminLoginID_SET"];
	if($data_action == 'update')
	{
		 $file_array["admin_fname"] 	= $_REQUEST["admin_fname"];
		 $file_array["admin_lname"] 	= $_REQUEST["admin_lname"];
		 $file_array["admin_telephone"] = $_REQUEST["admin_telephone"];
		 $file_array["admin_address"] 	= addslashes($_REQUEST["admin_address"]);
		 $file_array["admin_city"] 		= $_REQUEST["admin_city"];
		 $file_array["admin_country"]	= $_REQUEST["admin_country"];
		 $file_array["admin_state"] 	= addslashes($_REQUEST["state"]);
		 $file_array["admin_zip"] 		= $_REQUEST["admin_zip"];
		 if($_REQUEST["state"]!= "" && $_REQUEST["state"] != '0')
		 {
			$result=$s->editRecord('tbl_admin',$file_array,'admin_id',$pcode);
		 }
		 else
		 {
			$error = 1;
		 }
		 $data_action="edit";
	}
	if($data_action=="edit")
	{
		$rs = $s->getData_with_condition("tbl_admin","admin_id",$pcode);
		$row=mysqli_fetch_object($rs);
		$admin_fname 	 = $row->admin_fname;
		$admin_lname 	 = $row->admin_lname;
		$admin_telephone = $row->admin_telephone;
		$admin_address 	 = stripslashes($row->admin_address);
		$admin_city		 = $row->admin_city;
		$admin_country	 = $row->admin_country;
		$admin_state	 = stripslashes($row->admin_state);
		$admin_zip		 = $row->admin_zip;
		$admin_age		 = $row->admin_age;  
		$data_action	 = "update";
	}
?>
<script language="javascript" src="../js/zone_state.js" type="text/javascript"></script>

<form name="frx1" id="frx1" action="index.php?pagename=my_details_change&action=<?php echo $data_action;?>&pcode=<?php echo $pcode;?>" method="post">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="24%" class="pageheadTop">My Account </td>
            <td width="76%" class="headLink" align="right"><input type="submit" name="add " id="add" class="inputton" value="Save" /></td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td class="pHeadLine"></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><?php 
		if($error != 1)
		{
			if($_REQUEST['action']=='update')
			{
				if($result==0)
				{
					echo "<p class='success'>".record_update."</p><br />";	
				}
				else if($result==1)
				{
					echo "<p class='error'>".record_not_update."</p><br />";	
				}
			}
		}
?></td>
    </tr>
    <tr>
      <td><table width="100%" border="0" cellpadding="0" cellspacing="0" class="tblBorder">
          <tr>
            <td colspan="2" class="pagehead" >My Details</td>
          </tr>
          <tr class="text">
            <td width="11%" class="pad" >First Name<span class="redstar"> *</span></td>
            <td width="89%" ><input type="text" name="admin_fname" id="admin_fname" class="inpuTxt"  value="<?php echo $admin_fname;?>"></td>
          </tr>
          <tr class="text">
            <td class="pad">Last Name<span class="redstar"> *</span></td>
            <td><input type="text" name="admin_lname" id="admin_lname" class="inpuTxt"  value="<?php echo $admin_lname;?>"></td>
          </tr>
          <tr class="text">
            <td class="pad">Telephone No. </td>
            <td><input type="text" name="admin_telephone" id="admin_telephone" class="inpuTxt"  value="<?php echo $admin_telephone;?>"></td>
          </tr>
          <tr class="text">
            <td valign="top" class="pad">Addres <span class="redstar">*</span></td>
            <td><textarea name="admin_address" id="admin_address" class="inpuTxt"  rows="6"><?php echo $admin_address;?></textarea></td>
          </tr>
          <tr class="text">
            <td class="pad" align="left">City<span class="redstar"> *</span></td>
            <td align="left" ><input name="admin_city" type="text" class="inpuTxt" id="admin_city" value="<?php echo $admin_city;?>"></td>
          </tr>
          <tr class="text">
            <td class="pad" align="left"> Country<span class="redstar"> *</span></td>
            <td align="left"><select name="admin_country" id="admin_country" class="inpuTxt" onChange="htmlData('zone_state.php', 'ch='+this.value);">
                <option value="">Select Country</option>
                <?php
	$sql_country="select * from tbl_country order by country_name";
	$rs_co=mysqli_query($GLOBALS["___mysqli_ston"],$sql_country);
	if(mysqli_num_rows($rs_co)>0)
	{
		while($row_country=mysqli_fetch_object($rs_co))
		{	
?>
                <option value="<?php echo $row_country->country_id;?>" 
<?php
		if($row_country->country_id==$admin_country)
		{
			echo "selected='selected'";
		}
?>><?php echo $row_country->country_name;?></option>
                <?php
		}
	}
?>
              </select></td>
          </tr>
          <tr class="text">
            <td class="pad" align="left">State/Province <span class="redstar"> *</span></td>
            <td align="left" id="txtResult"><?php	
	if(is_numeric($admin_state))
	{
 		$rs_module = $s->getData_with_condition('tbl_zones','zone_id',$admin_state);
		if(mysqli_num_rows($rs_module)>0)
		{
			$row_state = mysqli_fetch_object($rs_module);
?>
              <input type="text" name="state" id="state" value="<?php echo $row_state->zone_name;?>" class="inpuTxt">
              <?php
		}
	}
	else
	{
?>
              <input type="text" name="state" id="state" value="<?php echo $admin_state;?>" class="inpuTxt">
              <?php	
	}
	if($error == 1)
	{
		echo "<span class='redstar'>Please fill the Country then State.</span>";
		$error =0;
	}

?></td>
          </tr>
          <tr class="text">
            <td class="pad" align="left">Zip/Postal Code <span class="redstar"> *</span></td>
            <td align="left"><input name="admin_zip" type="text" class="inpuTxt" id="admin_zip" value="<?php echo $admin_zip;?>"></td>
          </tr>
          <?php //php role table mapp here ?>
          <tr class="text">
            <td ></td>
            <td><input type="submit" name="add " id="add" class="inputton" value="Save" /></td>
          </tr>
          <tr class="text">
            <td class="redstar pad" colspan="2"> * Required Fields </td>
          </tr>
        </table></td>
    </tr>
  </table>
</form>
<script language="JavaScript" type="text/javascript">
	var frmvalidator = new Validator("frx1");
	frmvalidator.addValidation("admin_fname","req","Please enter your first name");
	frmvalidator.addValidation("admin_lname","req","Please enter your last name");
	frmvalidator.addValidation("admin_address","req","Please enter address");
	frmvalidator.addValidation("admin_city","req","Please enter city");
	frmvalidator.addValidation("admin_country","dontselect=0","Please select country");
	frmvalidator.addValidation("state","req","Please enter state.");
	frmvalidator.addValidation("admin_zip","req","Please enter  zip/postal code");
</script>