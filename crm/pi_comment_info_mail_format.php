<style type="text/css">
body {
	padding: 10px;
	margin:0px;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}

</style>
<?php
$date = date_default_timezone_set('Asia/Kolkata');
?>
<table width="100%"  border="0" >
  <tr class="text">
    <td>Dear <strong><?php echo $Prepared_by_name;?></strong>,</td>
  </tr>
  <tr class="text">
    <td height='3' ></td>
  </tr>
 
  <tr class="text">
    <td height='3' ></td>
  </tr>
  <tr class="text">
    <td height='25' >Your Proforma Invoice <strong>#<?php echo $pi_id ;?></strong> of offer # <?php echo $pcode;?> has been : 
    <?php if($_REQUEST["pi_status"]=='reject')
	{?>
    <strong style="color:#F00"><?php echo ucwords($_REQUEST["pi_status"]);?></strong>
    <?php 
	}
	else if($_REQUEST["pi_status"]=='approved')
	{?>
    <strong  style="color:#009900"><?php echo ucwords($_REQUEST["pi_status"]);?></strong>
    
    <?php } else {?>

    <strong style="color:#960"><?php echo ucwords($_REQUEST["pi_status"]);?></strong>
    <?php }?>
    </td>
  </tr>
  <tr class="text">
    <td height='3' ></td>
  </tr>
  <tr class="text">
    <td height='3' >Update Date : <?php echo gmdate("D, d M Y H:i:s") ;?></td>
  </tr>
  

  
  
  
  <tr class="text">
    <td height='3' ></td>
  </tr>
  <tr class="text">
    <td height='25' ><strong>Our suggestions on this Proforma Invoice:</strong></td>
  </tr>
  
  <tr class="text">
    <td   align="justify"><?php if($_REQUEST["comment"]!='') {echo nl2br(stripslashes($_REQUEST["comment"]));} else {echo "N/A";}?></td>
  </tr>
</table>
