<?php
//include("session_check.php");
	$today=date('d-m-Y');
	$pcode 		= $_REQUEST["order_id"];
 	$sql_Price_type_sys 	= "select pi_id,Prepared_by from tbl_performa_invoice where O_Id = '$pcode'";
$rs_Price_type_sys	 	= mysqli_query($GLOBALS["___mysqli_ston"],$sql_Price_type_sys); 
$row_Price_type_sys  	= mysqli_fetch_object($rs_Price_type_sys);
$pi_id =	$row_Price_type_sys->pi_id;	
$Prepared_by =	$row_Price_type_sys->Prepared_by;	
$Ref_No="PI-".$pi_id;

$sql = "select admin_fname,admin_lname,admin_email,admin_telephone from tbl_admin where admin_id = '$Prepared_by' and deleteflag = 'active' and admin_status='active'";
	$rs  = mysqli_query($GLOBALS["___mysqli_ston"],$sql);
	$row = mysqli_fetch_object($rs);
	$admin_fname	  = $row->admin_fname.' '.$row->admin_lname;
	$admin_email	  = $row->admin_email;
	$admin_telephone  = $row->admin_telephone;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Proforma Invoice Approval</title>
<link href='css/style.css' rel='stylesheet' type='text/css' />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script src="js/web_rupee_symbol.js" type="text/javascript"></script>
<style type="text/css">
table tr td {
	font-family:Arial, Helvetica, sans-serif;
	font-size:13px
}

a {
	font-family:Arial, Helvetica, sans-serif;
	font-size:11px;
	text-align:left;
	text-decoration:none;
	color:#333;
	font-weight:normal;
}
</style>
</head>

<body>
<table  cellpadding="5" cellspacing="1" align="left" width="100%">
 <tr>
    <td colspan="7" align="left"><strong>Dear Finance Team,</strong></td>
  </tr>
  
  <tr>
    <td colspan="7" height="15"></td>
  </tr>
  <tr>
    <td colspan="7"><p>PI of offer No. <?php echo $pcode; ?>, received in your CRM dashboard for approval. <br />
      </p></td>
  </tr>
  <tr>
    <td colspan="7" height="15"></td>
  </tr>
  
  <tr>
    <td colspan="7" height="20"><strong>Prepared By : </strong>
     <?php if($row->admin_fname!=''){?>
     <?php echo $admin_fname;?>
      <?php }?>
     </td>
  </tr>
  <?php if($admin_telephone!='' && $admin_telephone!='0'){?>
  <tr>
    <td colspan="7"><strong>Phone : </strong><?php echo $admin_telephone;?></td>
  </tr>
  <?php }?>
  <?php if($admin_email!=''){?>
  <tr>
    <td colspan="7"><strong>Email : </strong><?php echo $admin_email;?></td>
  </tr>
  <?php }?>
  <tr>
    <td colspan="7" height="15">&nbsp;</td>
  </tr>
  <tr>
    <th  align="left" ><a href="https://www.stanlay.in"><img alt="ACS" align='text-top' src="http://www.stanlay.in/images/stanlay-logo-black.png" border="0"  /></a></th>
  </tr>
  
</table>
</body>
</html>