<?php 
$pcode = $_REQUEST["pcode"];
// This will update meta tags and meta descriptions
if(isset($_REQUEST['save_meta']))  
{
	$dataArr['meta_content']  = addslashes($_REQUEST['meta_content']);
	$dataArr['meta_desc']     = addslashes($_REQUEST['meta_desc']);
	$dataArr['seo_title']     = addslashes($_REQUEST['seo_title']);
	$edit_button = $_REQUEST['edit_button'];
	$result = $s->editRecord('tbl_products', $dataArr, 'pro_id',  $_REQUEST['edit_button']);
	// This will redirect the page to add_general_products page. 	
	if($result == 0)
	{
		$s->pageLocation("index.php?pagename=add_general_prodcuts&subpagename=pro_meta&action=update_done&pcode=".$_REQUEST['edit_button'] . "&msg=record_update");
	}
	else
	{
		$s->pageLocation("index.php?pagename=add_general_prodcuts&subpagename=pro_meta&action=update_failed&pcode=".$_REQUEST['edit_button'] . "&msg=record_not_update");
	}
} 
$sqlProSel  = "select * from tbl_products where deleteflag = 'active' and pro_id = $pcode";
$rsProSel	= mysqli_query($GLOBALS["___mysqli_ston"],$sqlProSel);
if(mysqli_num_rows($rsProSel) > 0)
{
	$rowProSel = mysqli_fetch_object($rsProSel);
}
?>

<form id="frm_meta" name="frm_meta" action="" method="post">
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tblBorder">
    <?php
	if($_REQUEST["action"] == "update_done")
	{
		echo "<tr><td colspan='2'><p class='success'>Record updated successfully.</p> <br/></td></tr>";
	}
	else if($_REQUEST["action"] == "update_failed")
	{
		echo "<tr><td colspan='2'><p class='error'>Record couldn't update.</p> <br/></td></tr>";
	}
?>
    <tr>
      <td colspan="2" class="pagehead">Product Meta Tags
        <input type="hidden" id="edit_button" name="edit_button" value="<?php echo $_REQUEST['pcode']; ?>" /></td>
    </tr>
    <tr class="text">
      <td valign="top" class="pad" width="16%">Page  Title</td>
      <td width="84%" align="left"><input type="text" name="seo_title" class="inpuTxt" id="seo_title" value="<?php echo stripslashes($rowProSel->seo_title); ?>"></td>
    </tr>
    <tr class="text">
      <td valign="top" class="pad" width="16%">Meta Keywords</td>
      <td width="84%" align="left"><textarea name="meta_content" rows="4" class="inpuTxt" id="meta_content"><?php echo stripslashes($rowProSel->meta_content); ?></textarea></td>
    </tr>
    <tr class="text">
      <td valign="top" class="pad">Meta Description</td>
      <td><textarea name="meta_desc"  rows="4" class="inpuTxt" id="meta_desc"><?php echo  stripslashes($rowProSel->meta_desc); ?></textarea></td>
    </tr>
    <tr class="text">
      <td>&nbsp;</td>
      <td><input name='save_meta' id='save_meta' type='submit' class='inputton' value='Save' /></td>
    </tr>
  </table>
</form>
