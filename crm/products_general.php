<?php
	$subpagename = $_REQUEST['subpagename'];
	$data_action = $_REQUEST['action'];
	$pcode		 = $_REQUEST["pcode"];
    
	$rs_gen_pro  = $s->getdata_with_condition('tbl_products' , 'pro_id', $pcode);
    $row_gen_pro = mysqli_fetch_object($rs_gen_pro);
	$GroupAttID	 = $row_gen_pro->group_attr_id;	
	/*if($GroupAttID == -1)
	{
		$rs_proXSD  	= $s->getdata_with_condition('tbl_products', 'pro_group_id', $pcode);
		if(mysqli_num_rows($rs_proXSD)>0)
		{
    		$row_proXSD 	= mysqli_fetch_object($rs_proXSD);
			$proWeight		= $row_proXSD->pro_weight;
			$sqlProAttQty 	= "select * from tbl_products_attributes_values where deleteflag = 'active' and pro_group_id = $pcode and pro_id = $row_proXSD->pro_id and group_id = -1";
			$rsProAttQty 	= mysqli_query($GLOBALS["___mysqli_ston"],$sqlProAttQty);
			if(mysqli_num_rows($rsProAttQty)>0)
			{
				$rowProAttQty = mysqli_fetch_object($rsProAttQty);
				$proQty		  = $rowProAttQty->pro_qty;
			}
		}
	}*/
if($_REQUEST['subpagename']=='products_general')
{
	if($_REQUEST['action']=='update')
	{
			$dataArr['pro_SKU']	 	  	  = addslashes($_REQUEST['SKU']);
			$dataArr['pro_name']	 	  = addslashes($_REQUEST['pro_name']);
			$dataArr['permalink']	 	  = addslashes(str_replace(" ","_",$_REQUEST['permalink']));
			$dataArr['pro_last_modified'] = date('Y-m-d H:i:s') ;
			
			if(isset($_REQUEST["pro_expiration_date"]) && $_REQUEST["pro_expiration_date"] != '')
			{
				$pro_expiration_date								 = $_REQUEST["pro_expiration_date"];
				$dataArr["pro_expiration_date"]    	 = $s->getDateformate($pro_expiration_date,'mdy','ymd','-');
			}
			
			if($_REQUEST["start_date"] != '' && $_REQUEST["expires_date"] != '')
			{
				$start								 = $_REQUEST["start_date"];
				$dataArr["new_arrival_start"]    	 = $s->getDateformate($start,'mdy','ymd','-');
				$expires					 		 = $_REQUEST["expires_date"];
				$dataArr["new_arrival_end "] 		 = $s->getDateformate($expires,'mdy','ymd','-');
			}
			
			
			$dataArr['manufacturers_id'] 			 = $_REQUEST['manufacturers_id'];
			$dataArr['pro_short_description'] 		 = addslashes($_REQUEST['pro_short_description']);
			$dataArr['pro_description'] 			 = addslashes($_REQUEST['pro_description']);
			
			
//	This will retrieve attribute values and save in corresponding fields
			
					$dataArr["dress_asking_price"] 			=  $_REQUEST["dress_asking_price"];
					$dataArr["dress_style"] 				=  addslashes($_REQUEST["dress_style"]);
					$dataArr["dress_color"] 				=  addslashes($_REQUEST["dress_color"]);
					$dataArr["dress_condition"] 			=  $_REQUEST["dress_condition"];
					$dataArr["dress_purchased_in"] 			=  $_REQUEST["dress_purchased_in"];
					$dataArr["dress_neckline"] 				=  $_REQUEST["dress_neckline"];
					$dataArr["dress_waistline"] 			=  $_REQUEST["dress_waistline"];
					$dataArr["dress_silhouette"] 			=  $_REQUEST["dress_silhouette"];
					$dataArr["dress_original_owner"]		=  addslashes($_REQUEST["dress_original_owner"]);
					$dataArr["dress_price_paid"]			=  addslashes($_REQUEST["dress_price_paid"]);
					$dataArr["dress_seller"] 				=  addslashes($_REQUEST["dress_seller"]);
					$dataArr["dress_detail"] 				=  addslashes($_REQUEST["dress_detail"]);
					$dataArr["dress_sales_detail"] 			=  $_REQUEST["dress_sales_detail"];
					$dataArr["is_featured"] 				=  $_REQUEST["is_featured"];
					$dataArr["dress_listingno"] 			=  addslashes($_REQUEST["dress_listingno"]);
					$dataArr["dress_labelsize"] 			=  $_REQUEST["dress_labelsize"];
					$dataArr["dress_streetsize"] 			=  $_REQUEST["dress_streetsize"];
					$dataArr["dress_bust"] 					=  $_REQUEST["dress_bust"];
					$dataArr["dress_waist"] 				=  $_REQUEST["dress_waist"];
					$dataArr["dress_hips"] 					=  $_REQUEST["dress_hips"];
					$dataArr["dress_height_with_shoes"] 	=  $_REQUEST["dress_height_with_shoes"];
					$dataArr["dress_receipt"] 				=  $_REQUEST["dress_receipt"];
					$dataArr["dress_original_receipt"]  	=  $_REQUEST["dress_original_receipt"];
					
					$result 								= $s->editRecord('tbl_products' , $dataArr , 'pro_id', $pcode);
			
					
					//$result 								 = $s->editRecord('tbl_products_' , $dataArr , 'pro_id', $pcode);
			/*if($GroupAttID == -1)
			{			
				$dataArrX['pro_SKU']			= $_REQUEST['SKU']."001";
				$dataArrX['pro_name']	 	  	= $_REQUEST['pro_name'];
				$dataArrX['pro_model'] 		  	= $_REQUEST['pro_model'];
				$dataArrX['pro_last_modified'] 	= date('Y-m-d H:i:s') ;
				if($_REQUEST["start_date"] != '' && $_REQUEST["expires_date"] != '')
				{
					$start								 = $_REQUEST["start_date"];
					$dataArrX["new_arrival_start"]    	 = $s->getDateformate($start,'mdy','ymd','-');
					$expires					 		 = $_REQUEST["expires_date"];
					$dataArrX["new_arrival_end "] 		 = $s->getDateformate($expires,'mdy','ymd','-');
				}
				
				$dataArrX['manufacturers_id'] 		= $_REQUEST['manufacturers_id'];
				$dataArrX['pro_short_description'] 	= addslashes($_REQUEST['pro_short_description']);
				$dataArrX['pro_description'] 		= addslashes($_REQUEST['pro_description']);
				$dataArrX['pro_weight'] 			= $_REQUEST['proWeight'];	
				
				$dataArrX["pro_metal"]   			= $_REQUEST["metal"];
				$dataArrX["pro_color"]   			= $_REQUEST["color"];
				$dataArrX["pro_clasp_backing"]  	= $_REQUEST["clasp_backing"];
				$dataArrX["pro_length"]   			= $_REQUEST["length"];
				$dataArrX["pro_width"]   			= $_REQUEST["width"];
				
				$dataArrX["pro_group_id"] 			= $pcode;
				//print_r($dataArrX);
				//exit();
				$sqlProPro 							= "select * from tbl_products where deleteflag = 'active' and pro_group_id = $pcode";
				$rsProPro							= mysqli_query($GLOBALS["___mysqli_ston"],$sqlProPro);
				if(mysqli_num_rows($rsProPro) > 0)
				{
					$rowProPro	  	= mysqli_fetch_object($rsProPro);
					$result1 	  	= $s->editRecord( 'tbl_products', $dataArrX, 'pro_id', $rowProPro->pro_id);
					$sqlProAttQty 	= "select * from tbl_products_attributes_values where deleteflag = 'active' and pro_group_id = $pcode and pro_id = $rowProPro->pro_id and group_id = -1";
					$rsProAttQty 	= mysqli_query($GLOBALS["___mysqli_ston"],$sqlProAttQty);
					if(mysqli_num_rows($rsProAttQty)>0)
					{
						$rowProAttQty  = mysqli_fetch_object($rsProAttQty);
						$AttQtyValueID = $rowProAttQty->pro_att_val_id;
					}					
					$prefix					= "+";
					$price					= 0.00;
					$pro_qty				= $_REQUEST["qty"];
					$dataXqty["prefix"]		= $prefix;
					$dataXqty["price"]		= $price;
					$dataXqty["pro_qty"]	= $pro_qty;
					$result1 				= $s->editRecord( 'tbl_products_attributes_values', $dataXqty, 'pro_att_val_id',$AttQtyValueID);
				}
				else
				{
					$result1 	= $s->insertRecord( 'tbl_products', $dataArrX); ////
					$sqlProPro 	= "select * from tbl_products where deleteflag = 'active' order by pro_id desc";
					$rsProPro	= mysqli_query($GLOBALS["___mysqli_ston"],$sqlProPro);
					if(mysqli_num_rows($rsProPro)>0)
					{
						$rowProPro					= mysqli_fetch_object($rsProPro);
						$dataXqty["pro_id"]			= $rowProPro->pro_id;
						$dataXqty["group_id"]		= -1;
						$dataXqty["pro_group_id"] 	= $rowProPro->pro_group_id;
						$dataXqty["prefix"]			= "+";
						$dataXqty["price"]			= 0.00;
						$dataXqty["pro_qty"]		= $_REQUEST["qty"];
						$result2 				    = $s->insertRecord( 'tbl_products_attributes_values', $dataXqty);
					}
				}
			}*/
			$s->pageLocation("index.php?pagename=add_general_prodcuts&subpagename=products_general&action=edit&result=$result&pcode=$pcode"); 
			if($result==0)
			{
				$_SESSION['ActionMSZ']=" ";
				$_SESSION['ActionMSZ']=" Product General Details Updated Successfully. ";	
			}
			else if($result==1)
			{
				$_SESSION['ActionMSZ']=" ";
				$_SESSION['ActionMSZ']=" Product General Details Not Updated Successfully !";		
			}
	}
}
	
?>
<SCRIPT language="JavaScript">
<!--
// Create CalendarPopup object
var cal1 = new CalendarPopup("testdiv1");
cal1.setReturnFunction("showDate1");
cal1.showYearNavigation();
//-->
</SCRIPT>


<SCRIPT language="JavaScript">document.write(cal2.getStyles());</SCRIPT>
<SCRIPT language="JAVASCRIPT">
// Function to get input back from calendar popup
function showDate1(y,m,d) {
document.frx2.pro_expiration_date.value   = m + "-" + d + "-" + y;
}		
	
//--></script>
<form name="frx2" id="frx2" action="index.php?pagename=add_general_prodcuts&subpagename=products_general&action=update&pcode=<?php echo $pcode;?>" method="post">
<table width="100%" cellpadding="0" cellspacing="0"   border="0"  class="tblBorder">
 <tr>         <td colspan="2" class="pagehead">Products General Details </td>    </tr>
	<tr class="text" >
			<td class="pad"> Name <span class="redstar">*</span></td>
			<td><input name="pro_name" type="text" class="inpuTxt" id="pro_name"  value="<?php echo  $row_gen_pro->pro_name; ?>" /></td>
	</tr>
	<tr class="text" >
			<td class="pad"> Permalink <span class="redstar"></span></td>
			<td><input name="permalink" type="text" class="inpuTxt" id="permalink"  value="<?php echo  $row_gen_pro->permalink; ?>" /></td>
	</tr>
	
	<tr class="text" >
			<td class="pad"> Is Featured Dress <span class="redstar"></span></td>
			<td><select name="is_featured" class="selectlist" id="is_featured">
      <option value="yes" <?php if($row_gen_pro->is_featured == 'yes'){echo "selected";}?>>Yes</option>
      <option value="no" <?php if($row_gen_pro->is_featured == 'no'){echo "selected";}?>>No</option>
    </select></td>
	</tr>
	
    <tr class="text">
      <td class="pad">SKU <span class="redstar">*</span></td>
      <td><input name="SKU" type="text" class="inpuTxt" id="SKU"  value="<?php echo  $row_gen_pro->pro_SKU; ?>" /></td>
    </tr>
	 <?php if(trim($row_gen_pro->pro_type_value) == "Gift Certificate" ){?>
	<tr class="text">
	  <td class="pad">Certificate Expiration Date</td>
	<td width="84%" align="left" nowrap="nowrap" >
	<input name="pro_expiration_date" type="text" class="inpuTxt" id="pro_expiration_date" value="<?php echo $s->getDateformate($row_gen_pro->pro_expiration_date ,'ymd','mdy','-');?>" readonly="true">&nbsp;
<a id="anchor1" onClick="cal1.showCalendar('anchor1'); return false;" href="#" name="anchor1">
<img alt="" src="images/cal.gif" border="0"></a>
<div id="testdiv1" style="Z-INDEX: 102; VISIBILITY: hidden; POSITION: absolute"></div></td>	</tr>
<?php }?>



	<!--<tr class="text">
	  <td class="pad">New Arrival End Date </td>
	<td width="84%" align="left" nowrap="nowrap" >
	<input name="expires_date" type="text" class="inpuTxt" id="expires_date" value="<?php //echo $s->getDateformate($row_gen_pro->new_arrival_end ,'ymd','mdy','-');?>" readonly="true">&nbsp;
<a id="anchor2" onClick="cal2.showCalendar('anchor2'); return false;" href="#" name="anchor2">
<img alt="" src="images/cal.gif" border="0"></a>
<div id="testdiv1" style="Z-INDEX: 102; VISIBILITY: hidden; POSITION: absolute"></div></td>
</tr>-->
     <tr class="text">
       <td class="pad">Manufacturer Name <span class="redstar">*</span></td>
       <td><select name="manufacturers_id" class="inpuTxt" id="manufacturers_id">
         <option value="0">Select Manufacturer</option>
         <?php
		 $query   ="deleteflag='active' and manufacturers_status='active'";
		$rs_manu  = $s->selectwhere('tbl_manufacturers',$query);
		while($row_manu = mysqli_fetch_object($rs_manu))
		{
		?>
         <option  value="<?php echo $row_manu->manufacturers_id; ?>"
	   <?php
		   if($row_manu->manufacturers_id == $row_gen_pro->manufacturers_id)
		   {
				echo "selected='selected'";
		   }
	   ?> ><?php echo stripslashes($row_manu->manufacturers_name); ?></option>
         <?php } ?>
       </select></td>
     </tr>
  <tr class="text">
  <td valign="top" class="pad">Short Description <span class="redstar">*</span></td>
  <td><textarea name="pro_short_description" rows="4" class="inpuTxt" id="pro_short_description">
<?php echo stripslashes($row_gen_pro->pro_short_description); ?>
</textarea></td></tr>

  <tr class="text">
    <td valign="top" class="pad"><strong>More Info </strong></td>
    <td > 
	<table width="100%" border="0" cellpadding="0" cellspacing="1">
  
  <tr>
    <td width="113">Style </td>
    <td width="11">:</td>
    <td width="239"><input name="dress_style" type="text" class="textfield"  id="dress_style" value="<?php echo  $row_gen_pro->dress_style; ?>" size="25" /></td>
    <td width="105">Listing Number<br />    </td>
    <td width="6">: </td>
    <td width="197"><input name="dress_listingno" type="text" class="textfield"  id="dress_listingno" value="<?php echo  $row_gen_pro->dress_listingno; ?>" size="25" /></td>
  </tr>
  <tr>
    <td>Color</td>
    <td>:</td>
    <td><input name="dress_color" type="text" class="textfield"  id="dress_color" value="<?php echo  $row_gen_pro->dress_color; ?>" size="25" /></td>
    <td>Label Size <span class="mandatory">*</span></td>
    <td>:</td>
    <td><select name="dress_labelsize" class="selectlist" id="dress_labelsize">
      <option value="-1">All</option>
      <?php	for($ids=0;$ids < 28 ; $ids++){ ?>
      <option value="<?php echo $ids;?>" <?php if($row_gen_pro->dress_labelsize == $ids){echo "selected";}?>><?php echo "Size ".$ids;?></option>
      <?php 	$ids++;}?>
    </select></td>
  </tr>
  <tr>
    <td>Condition <span class="mandatory">*</span></td>
    <td>:</td>
    <td><select name="dress_condition" class="selectlist" id="dress_condition">
      <option value="-1">All</option>
      <?php
								$table1 = "tbl_dress_condition";
								$query1  = " status = 'active' AND deleteflag = 'active'";
								$drs_cond = $front->selectWhere($table1, $query1, $star = '*');
								while($condition_data = mysqli_fetch_object($drs_cond)){
								 ?>
      <option value="<?php echo $condition_data->condition_id;?>" <?php if($row_gen_pro->dress_condition == $condition_data->condition_id){echo "selected";}?>><?php echo $condition_data->condition_name;?></option>
      <?php }?>
    </select>    </td>
    <td>Street Size <span class="mandatory">*</span></td>
    <td>:</td>
    <td><select name="dress_streetsize" class="selectlist" id="dress_streetsize">
      <option value="-1">All</option>
      <?php	for($ids=0;$ids < 28 ; $ids++){ ?>
      <option value="<?php echo $ids;?>" <?php if($row_gen_pro->dress_streetsize == $ids){echo "selected";}?>><?php echo "Size ".$ids;?></option>
      <?php 	$ids++;}?>
    </select></td>
  </tr>
  <tr>
    <td>Purchased in <span class="mandatory">*</span></td>
    <td>:</td>
    <td><select name="dress_purchased_in" class="selectlist" id="dress_purchased_in">
      <?php
											$Y = date("Y");
											for($idy=0;$idy < 15 ; $idy++)
											{
									?>
      <option value="<?php echo ($Y-$idy);?>" <?php if($row_gen_pro->dress_purchased_in == ($Y-$idy)){echo "selected";}?>><?php echo ($Y-$idy);?></option>
      <?php 	}?>
    </select>    </td>
    <td>Bust</td>
    <td>:</td>
    <td><select name="dress_bust" class="selectlist" id="dress_bust">
      <option value="-1">All</option>
      <?php
									$table1 = "tbl_dress_bust";
									$query1  = " status = 'active' AND deleteflag = 'active'";
									$drs_bust = $front->selectWhere($table1, $query1, $star = '*');
									while($bust_data = mysqli_fetch_object($drs_bust)){
								 	?>
      <option value="<?php echo $bust_data->bust_id;?>" <?php if($row_gen_pro->dress_bust == $bust_data->bust_id){echo "selected";}?>><?php echo $bust_data->bust_name;?></option>
      <?php }?>
    </select></td>
  </tr>
  <tr>
    <td>Neckline</td>
    <td>:</td>
    <td><select name="dress_neckline" class="selectlist" id="dress_neckline">
      <option value="-1">All</option>
      <?php
									$table1 = "tbl_dress_neckline";
									$query1  = " status = 'active' AND deleteflag = 'active'";
									$drs_neckline = $front->selectWhere($table1, $query1, $star = '*');
									while($neckline_data = mysqli_fetch_object($drs_neckline)){
								 	?>
      <option value="<?php echo $neckline_data->neckline_id;?>" <?php if($row_gen_pro->dress_neckline == $neckline_data->neckline_id){echo "selected";}?>><?php echo $neckline_data->neckline_name;?></option>
      <?php }?>
    </select></td>
    <td>Waist</td>
    <td>: </td>
    <td><select name="dress_waist" class="selectlist" id="dress_waist">
      <option value="-1">All</option>
      <?php
									$table1 = "tbl_dress_waistsize";
									$query1  = " status = 'active' AND deleteflag = 'active'";
									$drs_waistsize = $front->selectWhere($table1, $query1, $star = '*');
									while($waistsize_data = mysqli_fetch_object($drs_waistsize)){
								 	?>
      <option value="<?php echo $waistsize_data->waistsize_id;?>" <?php if($row_gen_pro->dress_waist == $waistsize_data->waistsize_id){echo "selected";}?>><?php echo $waistsize_data->waistsize_name;?></option>
      <?php }?>
    </select></td>
  </tr>
  <tr>
    <td>Waistline</td>
    <td>:</td>
    <td><select name="dress_waistline" class="selectlist" id="dress_waistline">
      <option value="-1">All</option>
      <?php
									$table1 = "tbl_dress_waistline";
									$query1  = " status = 'active' AND deleteflag = 'active'";
									$drs_waistline = $front->selectWhere($table1, $query1, $star = '*');
									while($waistline_data = mysqli_fetch_object($drs_waistline)){
								 	?>
      <option value="<?php echo $waistline_data->waistline_id;?>" <?php if($row_gen_pro->dress_waistline == $waistline_data->waistline_id){echo "selected";}?>><?php echo $waistline_data->waistline_name;?></option>
      <?php }?>
    </select></td>
    <td>Hips</td>
    <td>: </td>
    <td><select name="dress_hips" class="selectlist" id="dress_hips">
      <option value="-1">All</option>
      <?php
									$table1 = "tbl_dress_bust";
									$query1  = " status = 'active' AND deleteflag = 'active'";
									$drs_bust = $front->selectWhere($table1, $query1, $star = '*');
									while($bust_data = mysqli_fetch_object($drs_bust)){
								 	?>
      <option value="<?php echo $bust_data->bust_id;?>" <?php if($row_gen_pro->dress_waistline == $bust_data->bust_id){echo "selected";}?>><?php echo $bust_data->bust_name;?></option>
      <?php }?>
    </select></td>
  </tr>
  <tr>
    <td>Silhouette</td>
    <td>:</td>
    <td><select name="dress_silhouette" class="selectlist" id="dress_silhouette">
      <option value="-1">All</option>
      <?php
									$table1 = "tbl_dress_silhouette";
									$query1  = " status = 'active' AND deleteflag = 'active'";
									$drs_silhouette = $front->selectWhere($table1, $query1, $star = '*');
									while($silhouette_data = mysqli_fetch_object($drs_silhouette)){
								 	?>
      <option value="<?php echo $silhouette_data->silhouette_id;?>" <?php if($row_gen_pro->dress_silhouette == $silhouette_data->silhouette_id){echo "selected";}?>><?php echo $silhouette_data->silhouette_name;?></option>
      <?php }?>
    </select></td>
    <td>Height with shoes</td>
    <td>:</td>
    <td><select name="dress_height_with_shoes" class="selectlist" id="dress_height_with_shoes">
      <option value="-1">All</option>
<?php
									$table1 = "tbl_dress_height_w_shoes";
									$query1  = " status = 'active' AND deleteflag = 'active'";
									$drs_height_w_shoes = $front->selectWhere($table1, $query1, $star = '*');
									while($height_w_shoes_data = mysqli_fetch_object($drs_height_w_shoes)){
?>
      <option value="<?php echo $height_w_shoes_data->height_w_shoes_id;?>" <?php if($row_gen_pro->dress_height_with_shoes == $height_w_shoes_data->height_w_shoes_id){echo "selected";}?>><?php echo $height_w_shoes_data->height_w_shoes_name;?></option>
      <?php }?>
    </select></td>
  </tr>
  <tr>
    <td>Original Owner</td>
    <td>:</td>
    <td><input name="dress_original_owner" type="text" class="textfield"  id="dress_original_owner" value="<?php echo  $row_gen_pro->dress_original_owner; ?>" size="25" /></td>
    <td>Has Receipt</td>
    <td>:</td>
    <td><select name="dress_receipt" class="selectlist" id="dress_receipt">
      <option value="yes" <?php if($row_gen_pro->dress_receipt == 'yes'){echo "selected";}?>>Yes</option>
      <option value="no" <?php if($row_gen_pro->dress_receipt == 'no'){echo "selected";}?>>No</option>
    </select></td>
  </tr>
  <tr>
    <td>Price Paid <span class="mandatory">*</span></td>
    <td>:</td>
    <td><input name="dress_price_paid" type="text" class="textfield"  id="dress_price_paid" value="<?php echo  $row_gen_pro->dress_price_paid;?>" size="25" /></td>
    <td>Original Receipt <span class="mandatory">*</span></td>
    <td>:</td>
    <td><select name="dress_original_receipt" class="selectlist" id="dress_original_receipt">
     <option value="yes" <?php if($row_gen_pro->dress_original_receipt == 'yes'){echo "selected";}?>>Yes</option>
      <option value="no" <?php if($row_gen_pro->dress_original_receipt == 'no'){echo "selected";}?>>No</option>
    </select></td>
  </tr>
  <tr>
    <td valign="top">Seller Is</td>
    <td valign="top">:</td>
    <td colspan="4"><textarea name="dress_seller" cols="80" rows="5" class="textfield" id="dress_seller"><?php echo  $row_gen_pro->dress_seller;?></textarea></td>
  </tr>
</table>	</td>
  </tr>
  <tr class="text">
    <td valign="top" class="pad">Sell Detail <span class="redstar"></span></td>
    <td><?php
					$sBasePath  			 = "../fckeditor/";
					$oFCKeditor 			 = new FCKeditor('pro_description') ;
					$oFCKeditor->BasePath 	 = $sBasePath;
					$oFCKeditor->Value		 =  stripslashes($row_gen_pro->pro_description); 
					$oFCKeditor->Width 		 = '600' ;
					$oFCKeditor->Height 	 = '300' ;
					$oFCKeditor->Create() ;
?></td>
  </tr>
  <tr class="text">
<td valign="top" class="pad">Dress Detail <span class="redstar"> </span></td>
 <td><?php
					$sBasePath  			 = "../fckeditor/";
					$oFCKeditor 			 = new FCKeditor('dress_detail') ;
					$oFCKeditor->BasePath 	 = $sBasePath;
					$oFCKeditor->Value		 =  stripslashes($row_gen_pro->dress_detail); 
					$oFCKeditor->Width 		 = '600' ;
					$oFCKeditor->Height 	 = '300' ;
					$oFCKeditor->Create() ;
?></td></tr>
	<tr class="text">
	<td>&nbsp;</td>
		<td>
		  <input name="save"  type="submit" class="inputton" id="save" value="Save" /></td>	
	</tr>
<tr class="text"><td>&nbsp;</td><td class="redstar pad"> * Required Fields </td></tr>
</table>
</form>
<script language="JavaScript" type="text/javascript">
 var frmvalidator = new Validator("frx2");
  frmvalidator.addValidation("pro_name","req","Please enter the Product Name.");
  /*frmvalidator.addValidation("permalink","req","Please enter the Permalink.");*/
  frmvalidator.addValidation("SKU","req","Please enter the SKU.");
   //frmvalidator.addValidation("pro_zip","maxsize","Maximum 10 character");
  frmvalidator.addValidation("manufacturers_id","dontselect=0","Please select Manufacturers.");
  frmvalidator.setAddnlValidationFunction("CheckLenth");
 function CheckLenth()
 {
	  var frm = document.forms["frx2"];
	  if(frm.pro_zip.value.length > 9)
	  {
		alert("Maximum 10 character\n[Current length = "+frm.pro_zip.value.length+" ]");
		return false;
	  }
	  else
	  {
		return true;
	  }
 }
 
<?php if($OpOn == 'product'){ ?>
  frmvalidator.addValidation("proWeight","req","Please Enter the Product Weight.");
<?php 	}  ?>
  frmvalidator.addValidation("pro_short_description","req","Please Enter the Product Short Description.");
<?php /*if($GroupAttID == -1){ ?>
 frmvalidator.addValidation("qty","req","Please Enter the quantity.");
 frmvalidator.addValidation("proWeight","req","Please Enter the weight.");
<?php }*/?>
</script>