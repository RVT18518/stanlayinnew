<?php
//session_start();
date_default_timezone_set("Asia/Kolkata");
include_once("../../includes1/function_lib.php"); 
$today=date("Y-m-d");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Incoming Stock Report</title>
<style>
body {
    margin: 0;
    font-family: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,"Noto Sans",sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol","Noto Color Emoji";
    font-size: 1rem;
    font-weight: 400;
    line-height: 1.5;
    color: #212529;
    text-align: left;
    background-color: #fff;
}
/*.table {
    width: 100%;
    margin-bottom: 1rem;
    color: #212529;
}

.table td, .table th {
    padding: .75rem;
    vertical-align: top;
    border-top: 1px solid #dee2e6;
}

th {
    text-align: inherit;
}

.table-striped tbody tr:nth-of-type(odd) {
    background-color: rgba(0,0,0,.05);
}*/


.text {
	font:inherit;
	height: 28px;

}
.text a {

	text-decoration: none;
}
.text a:hover {
	text-decoration: underline;
}

td.hover_effect table tr td {
	border-bottom: 1px solid #e7e7e7
}
td.hover_effect1 table tr td {
	border-bottom: 1px solid #e7e7e7
}
.tblBorder tbody tr:nth-of-type(odd) {
    background-color: rgba(0,0,0,.05);
}

.head {
	font: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	font-size: 13px;
	font-weight: bold;
	background-color: #F2F2F2;
	/*background:#ffffff;*/
	height: 28px;
}


</style>
</head>
<body style="padding:0px; margin:0px">

<p>Dear Sir/Madam,</p>
<p>Incoming stock report for the month of <?php echo date("F Y", strtotime($today));?></p>
<?php
$year_search=date('Y');	
	if($year_search!='' && $year_search!='0')

	{

	//$orders_status='Pending';

	$year_search_search=" AND YEAR( vendor_po_order.received_date ) IN ( $year_search )";

	}
$month_search=date('m');
if($month_search!='' && $month_search!='0')

	{

	//$orders_status='Pending';

		$month_search_search=" AND MONTH( vendor_po_order.received_date ) IN ( $month_search )";
}

		$searhRecord = $year_search_search.$month_search_search;
/*		if($ManuFact != '0')
		{
			$searhRecord = $searhRecord ."and manufacturers_id = $ManuFact";
		}
*/		//echo $searhRecord ;
	//}
?>
  <?php 
// $sql="SELECT t1.orders_id, t1.customers_id, t1.received_date, t1.time_ordered, t1.orders_status, t1.order_by, t1.lead_id, t2.pro_id, t2.proidentry, t2.pro_model,count(t2.pro_id) as pcount, sum(t2.pro_quantity) as pqty, t2.pro_quantity, t2.pro_name from tbl_order as t1 INNER JOIN tbl_order_product as t2 on t1.orders_id=t2.order_id where t1.deleteflag = 'active' and t2.deleteflag = 'active' and t1.orders_status!='Failed' and t1.orders_status!='Offer Cancelled' and t1.orders_status!='On Hold' and t1.orders_status!='Pending' $searhRecord group by t2.pro_id   order by count(t2.pro_id) desc ";
$limit="limit 0,200";
$sql="select vendor_master.C_Name,vendor_master.Contact_1, vendor_po_final.Prodcut_Qty as par_qty, vendor_po_final.Model_No,vendor_po_final.Product_Desc, vendor_po_final.ID, vendor_po_final.PO_ID, vendor_po_final.Date, vendor_po_final.Sup_Ref, vendor_po_final.Exporter, vendor_po_order.VPI, vendor_po_order.Confirm_Purchase,vendor_po_order.received_date,vendor_po_final.E_date  from vendor_master,vendor_po_order,vendor_po_final where vendor_po_order.VPI=vendor_master.ID AND vendor_po_order.ID=vendor_po_final.PO_ID AND vendor_po_order.Confirm_Purchase='active' $searhRecord order by vendor_po_order.received_date desc $limit";
		$rs = mysqli_query($GLOBALS["___mysqli_ston"],$sql);			
				
	?>
 <?php /*?><div class="pagehead" align="center"><h3>Outgoing Stock Report: <?php echo date("F Y", strtotime($today));?></h3></div><?php */?>


   <table width="87%" border="1" cellpadding="4" cellspacing="0" class="tblBorder" align="center" bordercolor="#f6f6f6">
                <tr class="head">
                  <td align="center">S. No.</td>
                  <td >Product Name </td>
                  <td align="left">PO ID</td>
                  <td align="left">Item Code</td>
                  <td align="left">Date</td>
                  <td align="left"> Qty</td>
                </tr>
                <?php 
				if(mysqli_num_rows($rs)!=0)
		{
				$i=0;
			while($row = mysqli_fetch_object($rs))
			{
			
				$qty		 		= $row->ware_house_stock;
				//$incoming_stock		= $s->incoming_qty($row->pro_model);
				//$outgoing_stock		= $s->outgoing_selling_qty($row->pro_id);
				//$demo_stock			= $s->total_demo_stock($row->pro_id);
				//$bal_stock			= $incoming_stock-$outgoing_stock-$demo_stock;
				$i++;
	?>
                <tr class="text" >
                  <td align="center"><?php echo $i;//$row->pro_id ;?></td>
                  <td width="35%"><?php echo $row->Product_Desc;?></td>
                  <td align="left"><?php echo $row->PO_ID;?></td>
                  <td align="left"><?php echo $row->Model_No;?></td>
                  <td align="left"><?php echo date("d/M/Y", strtotime($row->received_date));
				  //if($month_search=='0'){echo "All";} else {echo date('F Y', mktime(0, 0, 0, $month_search, 10));}//$row->Received_date;?></td>
                  <td align="left"><?php echo $row->par_qty;?></td>
                </tr>
                <?php		}	?>
                <tr class="head">
                  <td nowrap="nowrap" align="center">&nbsp;</td>
                  <td nowrap="nowrap" align="center">&nbsp;</td>
                  <td nowrap="nowrap" align="center">&nbsp;</td>
                  <td nowrap="nowrap" align="center">&nbsp;</td>
                  <td nowrap="nowrap" align="right">&nbsp;</td>
                  <td nowrap="nowrap" align="right">&nbsp;</td>
                </tr>
                <?php	
		}
		else
		{
	?>
                <tr class='text'>
                  <td colspan='7' class='redstar' align="center">&nbsp; No record present in database.</td>
                </tr>
                <?php
		}
		//}
	?>
              </table>
          
          
<p>Regards <br />
Team ACL</p>
<!--<img src="https://www.stanlay.in/images/logo.png" width="180" height="51" />-->
</body>
</html>
