<?php
//session_start();
date_default_timezone_set("Asia/Kolkata");
include_once("../../includes1/function_lib.php"); 
$today=date("Y-m-d");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Outgoing Stock Report</title>
<style>
body {
    margin: 0;
    font-family: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,"Noto Sans",sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol","Noto Color Emoji";
    font-size: 1rem;
    font-weight: 400;
    line-height: 1.5;
    color: #212529;
    text-align: left;
    background-color: #fff;
}
/*.table {
    width: 100%;
    margin-bottom: 1rem;
    color: #212529;
}

.table td, .table th {
    padding: .75rem;
    vertical-align: top;
    border-top: 1px solid #dee2e6;
}

th {
    text-align: inherit;
}

.table-striped tbody tr:nth-of-type(odd) {
    background-color: rgba(0,0,0,.05);
}*/


.text {
	font:inherit;
	height: 28px;

}
.text a {

	text-decoration: none;
}
.text a:hover {
	text-decoration: underline;
}

td.hover_effect table tr td {
	border-bottom: 1px solid #e7e7e7
}
td.hover_effect1 table tr td {
	border-bottom: 1px solid #e7e7e7
}
.tblBorder tbody tr:nth-of-type(odd) {
    background-color: rgba(0,0,0,.05);
}

.head {
	font: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	font-size: 13px;
	font-weight: bold;
	background-color: #F2F2F2;
	/*background:#ffffff;*/
	height: 28px;
}


</style>
</head>
<body style="padding:0px; margin:0px">

<p>Dear Sir/Madam,</p>
<h2 align="center">Outgoing stock report for the month of <?php echo date("F Y", strtotime($today));?></h2>
<?php
	$data_action = $_REQUEST['action'];
	$pcode		 = $_REQUEST["pcode"];
	$result		 = $_REQUEST["result"];
	/*if($_REQUEST['action']=='SearchRecord')
	{*/
		$ProID		= $_REQUEST["pro_id"];
		$ManuFact	= $_REQUEST["manufact"];
		$item_code	= $_REQUEST["item_code"];  
		$qtyTo		= $_REQUEST["qtyTo"];
		$datevalid_from 		= $_REQUEST["datevalid_from"];
		$datevalid_to			= $_REQUEST["datevalid_to"];

		$searhRecord = "  ";
		if($ProID!= '')		
		
		
		if($datevalid_from!='' && $datevalid_to!='')
	{
		
$date_range_search="AND (date( t1.date_ordered ) BETWEEN '$datevalid_from' AND '$datevalid_to')";
	}
$year_search=date('Y');	
	if($year_search!='' && $year_search!='0')
	{
	//$orders_status='Pending';
	$year_search_search=" AND YEAR( t1.date_ordered ) IN ( $year_search )";
	}
$month_search=date('m');
if($month_search!='' && $month_search!='0')
	{
	//$orders_status='Pending';
		$month_search_search=" AND MONTH( t1.date_ordered ) IN ( $month_search )";
}
	
		$searhRecord = $searchproid.$searchitemcode.$year_search_search.$month_search_search.$date_range_search;

?>

 <?php /*?><div class="pagehead" align="center"><h3>Outgoing Stock Report: <?php echo date("F Y", strtotime($today));?></h3></div><?php */?>


   <table width="87%" border="1" cellpadding="4" cellspacing="0" class="tblBorder" align="center" bordercolor="#f6f6f6">
    
      <?php 
$sql="SELECT t1.orders_id, t1.customers_id, t1.date_ordered, t1.time_ordered, t1.orders_status, t1.order_by, t1.lead_id, tbl_order_product.pro_id, tbl_order_product.proidentry, tbl_order_product.pro_model,count(tbl_order_product.pro_id) as pcount, sum(tbl_order_product.pro_quantity) as pqty, tbl_order_product.pro_quantity, tbl_order_product.pro_name from tbl_order as t1 INNER JOIN tbl_order_product as tbl_order_product on t1.orders_id=tbl_order_product.order_id where t1.deleteflag = 'active' and tbl_order_product.deleteflag = 'active' and t1.orders_status!='Failed' and t1.orders_status!='Offer Cancelled' and t1.orders_status!='On Hold' and t1.orders_status!='Pending' $searhRecord group by tbl_order_product.pro_id order by pqty desc ";
//$_SESSION["ELqueryX"] = $sql;	
$rs = mysqli_query($GLOBALS["___mysqli_ston"],$sql);			
	?>

            <tr class="head">
              <th align="center">S. No.</th>
              <th >Product Name </th>
              <th align="left">Item Code</th>
              <th align="left">Month</th>
              <th align="left">Order Count</th>
              <th align="left">Selling Qty</th>
            </tr>
            <?php if(mysqli_num_rows($rs)!=0)
		{	
				$i=0;
			while($row = mysqli_fetch_object($rs))
			{
			
				$qty		 		= $row->ware_house_stock;
				//$incoming_stock		= $s->incoming_qty($row->pro_model);
				//$outgoing_stock		= $s->outgoing_selling_qty($row->pro_id);
				//$demo_stock			= $s->total_demo_stock($row->pro_id);
				//$bal_stock			= $incoming_stock-$outgoing_stock-$demo_stock;
$i++;
	?>
            <tr class="text" >
              <td align="center"><?php echo $i;//$row->pro_id ;?></td>
              <td><?php echo $row->pro_name;?></td>
              <td align="left"><?php echo $row->pro_model;?></td>
              <td align="left"><?php if($month_search=='0'){echo "All";} else {echo date('F Y', mktime(0, 0, 0, $month_search, 10));}//$row->date_ordered;?></td>
              <td align="left"><?php echo $row->pcount;?>
                <?php // echo $row->pro_quantity;//$outgoing_stock;//$s->outgoing_selling_qty($row->pro_id);?></td>
              <td align="left"><?php echo $row->pqty;?></td>
            </tr>
            <?php		
		}
			
	?>
            
      <?php	
		}
		else
		{
	?>
            <tr class='text'>
              <td colspan='6' class='redstar' align="center">&nbsp; No record present in database.</td>
            </tr>
            <?php
		}
	
	?>
    <tr class="head">
              <td nowrap="nowrap" align="center">&nbsp;</td>
              <td nowrap="nowrap" align="center">&nbsp;</td>
              <td nowrap="nowrap" align="center">&nbsp;</td>
              <td nowrap="nowrap" align="right">&nbsp;</td>
              <td nowrap="nowrap" align="right">&nbsp;</td>
              <td nowrap="nowrap" align="right">&nbsp;</td>
      </tr>
          </table>
          
          
<p>Regards <br />
Team ACL</p>
<!--<img src="https://www.stanlay.in/images/logo.png" width="180" height="51" />-->
</body>
</html>
