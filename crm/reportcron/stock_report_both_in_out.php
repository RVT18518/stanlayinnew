<?php
//session_start();
date_default_timezone_set("Asia/Kolkata");
include_once("../../includes1/function_lib.php"); 
$today=date("Y-m-d");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Incoming Stock Report</title>
<style>
body {
	margin: 0;
	font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
	font-size: 1rem;
	font-weight: 400;
	line-height: 1.5;
	color: #212529;
	text-align: left;
	background-color: #fff;
}
/*.table {
    width: 100%;
    margin-bottom: 1rem;
    color: #212529;
}

.table td, .table th {
    padding: .75rem;
    vertical-align: top;
    border-top: 1px solid #dee2e6;
}

th {
    text-align: inherit;
}

.table-striped tbody tr:nth-of-type(odd) {
    background-color: rgba(0,0,0,.05);
}*/


.text {
	font:inherit;
	height: 28px;
}
.text a {
	text-decoration: none;
}
.text a:hover {
	text-decoration: underline;
}
td.hover_effect table tr td {
	border-bottom: 1px solid #e7e7e7
}
td.hover_effect1 table tr td {
	border-bottom: 1px solid #e7e7e7
}
.tblBorder tbody tr:nth-of-type(odd) {
	background-color: rgba(0,0,0,.05);
}
.head {
	font: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	font-size: 13px;
	font-weight: bold;
	background-color: #F2F2F2;
	/*background:#ffffff;*/
	height: 28px;
}
</style>
</head>
<?php

if (date('m')>3) {
    $year = date('Y').'-'.(date('Y')+1);
} else {
    $year = (date('Y')-1).'-'.date('Y');
}
$financial_year=$year;
//?>
<body style="padding:0px; margin:0px">
<p>Dear Sir/Madam,</p>
<h2 align="center">Incoming Outgoing & Balance sale Stock report for the  FY <?php echo $financial_year;//date("Y", strtotime($today));?></h2>
<?php
$site_url="https://www.stanlay.in/";
//$site_url="http://localhost/stanlay.in/";
	$data_action = $_REQUEST['action'];
	$pcode		 = $_REQUEST["pcode"];
	$result		 = $_REQUEST["result"];
	/*if($_REQUEST['action']=='SearchRecord')
	{*/
		$ProID		= $_REQUEST["pro_id"];
		$ManuFact	= $_REQUEST["manufact"];
		$item_code	= $_REQUEST["item_code"];  
		$qtyTo		= $_REQUEST["qtyTo"];
		$datevalid_from 	= $_REQUEST["datevalid_from"];
		$datevalid_to		= $_REQUEST["datevalid_to"];
		
		$searhRecord = "  ";
		if($ProID!= '')		
		{
			$searchproid = " and tbl_products.pro_id = '$ProID' ";
		}
		else
		{
				$searchproid = "";
		}
		
		/*if($datevalid_from!='' && $datevalid_to!='')
	{
		
$date_range_search="AND (date( Date ) BETWEEN '$datevalid_from' AND '$datevalid_to')";
	}
	*/	
		
		if($item_code!= '')		
		{
			$searchitemcode = " and tbl_products_entry.model_no = '$item_code' ";
		}
		else
		{
				$searchitemcode = "";
		}
		


		
			$searhRecord = $searchproid.$searchitemcode.$date_range_search;


function incoming_qty_stock_report($modelno,$month_search=0,$year_search=0,$datevalid_from=0,$datevalid_to=0)
{

if($datevalid_from!='0' && $datevalid_to!='0' && $datevalid_from!='' && $datevalid_to!='')
	{
	
$date_range_search=" AND (date( vendor_po_order.received_date ) BETWEEN '$datevalid_from' AND '$datevalid_to')";
	}
	
	if($year_search!='' && $year_search!='0')
	{
	//$orders_status='Pending';
	$year_search_search=" AND YEAR( vendor_po_order.received_date ) IN ( $year_search )";
	}

if($month_search!='' && $month_search!='0')
	{
	//$orders_status='Pending';
		$month_search_search=" AND MONTH( vendor_po_order.received_date ) IN ( $month_search )";
}
$searhRecord =  $year_search_search.$month_search_search.$date_range_search.$searchproid;	

$sql ="	select vendor_po_final.Prodcut_Qty as par_qty  from vendor_po_order,vendor_po_final where vendor_po_order.ID=vendor_po_final.PO_ID AND vendor_po_order.Confirm_Purchase='active' and vendor_po_final.Model_No = '$modelno' $searhRecord order by vendor_po_order.received_date desc";
$rs	 = mysqli_query($GLOBALS["___mysqli_ston"],$sql);
while($row=mysqli_fetch_object($rs))
{

	$sum += $row->par_qty;
}
if($sum=='' || $sum=='0')
{
	$incoming_qty="0";
}
else
{
$incoming_qty=$sum;
}
return $incoming_qty;
}



function outgoing_selling_qty($ProID,$item_code,$month_search=0,$year_search=0,$datevalid_from=0,$datevalid_to=0)
{
	
//echo "QQQQQQ".$item_code;
	if($ProID!= '')		
		{
			$searchproid = "  and tbl_order_product.pro_id ='$ProID' ";
		}
		
		else
		{
				$searchproid = "";
		}
		
		
		
		if($item_code!='')		
		{
			$searchitemcode = " and TRIM(tbl_order_product.pro_model) = '$item_code' ";
		}
		else
		{
			$searchitemcode = "";
		}
	
	
	if($datevalid_from!='0' && $datevalid_to!='0' && $datevalid_from!='' && $datevalid_to!='')
	{
		
$date_range_search=" AND (date( tbl_order.date_ordered ) BETWEEN '$datevalid_from' AND '$datevalid_to')";
	}
	
	
	if($year_search!='' && $year_search!='0')

	{

	//$orders_status='Pending';

	$year_search_search=" AND YEAR( tbl_order.date_ordered ) IN ( $year_search )";

	}

if($month_search!='' && $month_search!='0')

	{

	//$orders_status='Pending';

	$month_search_search=" AND MONTH( tbl_order.date_ordered ) IN ( $month_search )";
}
$searhRecord =  $year_search_search.$month_search_search.$date_range_search.$searchproid.$searchitemcode;
				
	
$sql = "select tbl_order_product.pro_id as 'Product ID', tbl_order_product.pro_name as 'Product Name', count(tbl_order_product.pro_id) as 'Selling Count', sum(tbl_order_product.pro_quantity) as 'selling_qty' from tbl_order_product INNER JOIN tbl_order on tbl_order.orders_id = tbl_order_product.order_id where tbl_order.deleteflag = 'active'  and tbl_order.orders_status IN ('confirmed','Order Closed') and tbl_order_product.deleteflag = 'active' $searhRecord  group by tbl_order_product.pro_id order by count(tbl_order_product.pro_id) desc ";
$rs	 = mysqli_query($GLOBALS["___mysqli_ston"],$sql);
$row=mysqli_fetch_object($rs);
if($row->selling_qty=='' || $row->selling_qty=='0')
{
	$selling_qty="0";
}
else
{

$selling_qty=$row->selling_qty;
}
return $selling_qty;

}


function total_demo_stock($ID)
{   
$sqlState = "SELECT *, sum(qty) as product_stock_sum  from tbl_demo_stock where pro_id = '$ID' and status='active' and deleteflag='active' ";
$rsState  = mysqli_query($GLOBALS["___mysqli_ston"],  $sqlState);
$rowState = mysqli_fetch_object($rsState);

if($rowState->product_stock_sum=='')
{
$product_stock_sum=	'0';
}
else
{
	$product_stock_sum	  = $rowState->product_stock_sum;
}
return $product_stock_sum;
}

?>
<?php
	$datevalid_from="2019-04-01";
		$datevalid_to="2020-03-31";
		
$sql="SELECT 
		tbl_products.pro_id,
			tbl_products.pro_title,
			tbl_products_entry.model_no
			FROM tbl_products_entry
			INNER join tbl_products on tbl_products.pro_id=tbl_products_entry.pro_id
			WHERE tbl_products.deleteflag = 'active' AND tbl_products.STATUS = 'active' AND tbl_products_entry.deleteflag = 'active' AND tbl_products_entry.STATUS = 'active' $searhRecord GROUP by tbl_products.pro_id  ORDER BY tbl_products.pro_title asc ";
		$rs = mysqli_query($GLOBALS["___mysqli_ston"],$sql);			
			
	?>
<?php /*?><div class="pagehead" align="center"><h3>Outgoing Stock Report: <?php echo date("F Y", strtotime($today));?></h3></div><?php */?>
<table width="87%" border="1" cellpadding="4" cellspacing="0" class="tblBorder" align="center" bordercolor="#f6f6f6">
  <?php
            	if(mysqli_num_rows($rs)!=0)
		{?>
  <tr class="head">
    <td align="center" width="5%">S. No.</td>
    <td width="25%" >Item  Name </td>
    <td align="left" width="10%">Item Code</td>
    <td align="left" width="10%">Qty Incoming</td>
    <td align="left" width="10%">Qty Outgoing</td>
    <td align="left" width="10%" >Demo Stock</td>
    <td align="left" width="10%">Balance Sale Stock</td>
  </tr>
  <?php
				$i=0; 
			while($row = mysqli_fetch_object($rs))
			{
				$i++;
				//$rsProName  = $s->getData_with_condition('tbl_products','pro_id', $row->pro_id);
				//$rowProName = $row->pro_title;//mysqli_fetch_object($rsProName); 
				
				$qty		 		= $row->ware_house_stock;

				$ProID				= $row->pro_id;
				$item_code			= $row->model_no;
				$incoming_stock		= incoming_qty_stock_report($item_code,$month_search,$year_search,$datevalid_from,$datevalid_to);
		//$ProID,$item_code,$month_search=0,$year_search=0,$datevalid_from=0,$datevalid_to=0
				$outgoing_stock		= outgoing_selling_qty($ProID,$item_code,$month_search,$year_search,$datevalid_from,$datevalid_to);
				$demo_stock			= total_demo_stock($row->pro_id);
				$bal_stock			= $incoming_stock-$outgoing_stock-$demo_stock;
				
	?>
  <tr class="text" >
    <td align="center"><?php echo $i;//$row->pro_id ;?></td>
    <td><?php echo $row->pro_title;?></td>
    <td align="left"><?php echo $item_code;?></td>
    <td align="left"><?php echo $incoming_stock;//$s->incoming_qty($row->model_no);?></td>
    <td align="left"><?php echo $outgoing_stock;//$s->outgoing_selling_qty($row->pro_id);?></td>
    <td align="left"><?php echo $demo_stock;//$s->total_demo_stock($row->pro_id);?></td>
    <td align="left"><?php echo $bal_stock;?></td>
  </tr>
  <?php	}?>
  <tr class="head">
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="right">&nbsp;</td>
    <td align="right">&nbsp;</td>
    <td align="right">&nbsp;</td>
    <td align="right">&nbsp;</td>
    <td align="right">&nbsp;</td>
  </tr>
  <?php }?>
</table>
<p>Regards <br />
  Team ACL</p>
<!--<img src="https://www.stanlay.in/images/logo.png" width="180" height="51" />-->
</body>
</html>
