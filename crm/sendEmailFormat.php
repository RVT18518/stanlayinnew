<?php 
	$msz 	  = $_REQUEST["msz"];
?>
<style type="text/css">
body {
	padding: 10px;
	margin:0px;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
.pagehead {
	background:#666666;
	color: #FFFFFF;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight: bold;
	border-bottom: 1px solid #A1ABC7;
	margin-bottom:10px;
	height: 20px;
	padding-left:10px;
}

.pageheadTop {
	color: #6b7184;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 18px;
	font-weight: bold;
	padding:20px 0px 10px 15px;
}

.pagecontent a {
	color: #213e8e;
	text-decoration: none;
}

#pagebody ul li {
	float: left;
	list-style-type: none;
}
#pagebody ul {
	margin: 0px;
	padding: 20px;
}
#pagebody .pagecontent {
	padding-left: 0px;
}
.pagecontent a:hover {
	color: #5B7DD9;
	text-decoration: underline;
}

.head{ 
	font:Arial, Helvetica, sans-serif;
	font-size:12px;
	font-weight:bold;
	color:#6B7184;
	/*background-color:#FFD7B9;*/
	background:#DEDEDE;
	height:28px;
	}
	
.text {
	font:Arial, Helvetica, sans-serif;
	font-size:12px;
	color:#535353;
	/*background:#f6f4f2;*/
	height:28px;
	border-top-style: none;
	border-right-style: none;
	border-bottom: solid 1px #ffffff;
	border-left-style: none;
}
.text a{
	color:#213e8e;
	text-decoration:none;
}

.text a:hover{
	color:#213e8e;
	text-decoration:underline;
}

a.nlinks {
	background-image: url(../images/but-bg.jpg);
	background-repeat: repeat-x;
	background-color: #d8dfeb;
	background-position: top;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight: bold;
	color: #333;
	border: 1px solid #CCC;
	height: 22px;
}

.pHeadLine{
	background:#CCCCCC;
	height:3px;
}

.tblBorder{
	border:1px solid #CCCCCC;
}
.topRight{
	font:12px Arial, Helvetica, sans-serif;
	color:#FFFFFF;
	text-align:right;
	padding-right:20px;
}

.topRight a{
	color:#FFFFFF;
	text-decoration:underline;
}
.topRight a:hover{
	color:#FFFFFF;
	text-decoration:none;
}
.saperator{
	padding:5px;
}

.headLink{
	font:Arial, Helvetica, sans-serif;
	font-size:12px;
	color:#333333;
	font-weight:bold;
}
.headLink ul{
	margin:0px;
	padding:0px;
	list-style:none;
}
.headLink ul li{
	display:block;
	float:right;
	background:url(../images/linkbg.gif) repeat-x;
	margin-right:10px;
	padding:2px 5px 2px 5px;
	border:1px solid #69748C;
}


.headLink ul li a{
	color:#333333;
	text-decoration:none;
}
.headLink ul li a:hover{
	color:#333333;
	text-decoration:none;
}
.pad{
	padding-left:15px;
}

</style>

<table width="800">
<tr class="text"><td nowrap="nowrap"><?php echo nl2br(stripslashes($msz));?></td></tr>
</table>