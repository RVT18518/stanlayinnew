<?php
include("../includes1/modulefunction.php");
include("../includes1/function_lib.php"); 
include("session_check.php");

	$pcode 					= $_REQUEST["pcode"];//orderid
	$service_order_id 		= $_REQUEST["serviceorderID"];//serviceorderid
	
	$today_date	= date("Y-m-d");


   	$sql 	= "select * from tbl_order_service where deleteflag='active' and service_orders_id = '$service_order_id'";
	$rs	 	= mysqli_query($GLOBALS["___mysqli_ston"], $sql); 
	if(mysqli_num_rows($rs)>0)
	{
		$row  						= mysqli_fetch_object($rs);
	 	$name 						= ucfirst(stripslashes($row->customers_name));
		$order_date 				= $row->date_ordered;
		$customers_contact_no		= $row->customers_contact_no;
		$customers_name				= $row->customers_name;
		$customers_email			= $row->customers_email;
		$customers_id				= $row->customers_id;		
		$shipping_street_address 	= $row->shipping_street_address;
		$shipping_city 				= $row->shipping_city;
		$shipping_zip_code 			= $row->shipping_zip_code;
		$shipping_state 			= $row->shipping_state;
		$shipping_fax_no 			= $row->shipping_fax_no;
		$offer_subject 				= $row->offer_subject;
		$order_in_favor_of 			= $row->order_in_favor_of;
		$offer_warranty 			= $row->offer_warranty;
		$offer_calibration 			= $row->offer_calibration;
		$offer_validity 			= $row->offer_validity;
		$shipping_country_name 		= $row->shipping_country_name;
		$shipping_telephone_no 		= $s->tel_number($row->customers_id);
		
		$billing_street_address 	= $row->billing_street_address;
		$billing_city 				= $row->billing_city;
		$billing_zip_code 			= $row->billing_zip_code;
		$billing_state 				= $row->billing_state;
		$billing_fax_no 			= $row->billing_fax_no;
		
		$order_by 					= $row->order_by;
 		$payment_terms				= $row->payment_terms;
		$tax_included				= $row->tax_included;
		$tax_perc					= $row->taxes_perc;
		$discount_perc				= $row->discount_perc;
		$show_discount				= $row->show_discount;
		$lead_id					= $row->lead_id;
		$delivery_day				= $row->delivery_day;
		$follow_up_date				= $row->follow_up_date;
		$order_in_favor_of			= $row->order_in_favor_of;		
		
		$offercode					= $row->offercode;//orderid

	}
	
//added on 11-mar-2020 tbl order service table insert data from tbl_order
/***************************************************************************************/	

	$sql_serv_ord 	= "select * from tbl_order_service where deleteflag='active' and service_orders_id  = '$service_order_id'";
	$rs_serv_ord	= mysqli_query($GLOBALS["___mysqli_ston"],$sql_serv_ord); 
	mysqli_num_rows($rs_serv_ord); 
	if(mysqli_num_rows($rs_serv_ord)<=0)//lessthanzero
	{
		$Buyer_address							= $s->buyer_address($pcode);
//	$Consignee=$s->consignee_address($pcode);	
 		$dataArray["offercode"] 				= $offercode;
		$today									= date("Y-m-d");
		
		$dataArray["customers_id"] 				= htmlspecialchars($customers_id,ENT_QUOTES);
		$dataArray["order_type"] 				= htmlspecialchars("Service",ENT_QUOTES);
		$dataArray["customers_name"] 			= htmlspecialchars($name,ENT_QUOTES);
		$dataArray["customers_email"] 			= htmlspecialchars($customers_email,ENT_QUOTES);
		$dataArray["customers_contact_no"] 		= htmlspecialchars($shipping_telephone_no,ENT_QUOTES);
		$dataArray["shipping_street_address"]	= htmlspecialchars($shipping_street_address,ENT_QUOTES);
		$dataArray["shipping_city"] 			= htmlspecialchars($shipping_city,ENT_QUOTES);		
		$dataArray["shipping_zip_code"] 		= htmlspecialchars($shipping_zip_code,ENT_QUOTES);		
		$dataArray["shipping_state"] 			= htmlspecialchars($shipping_state,ENT_QUOTES);
		$dataArray["shipping_country_name"] 	= htmlspecialchars($shipping_country_name,ENT_QUOTES);
		$dataArray["shipping_telephone_no"] 	= htmlspecialchars($shipping_telephone_no,ENT_QUOTES);
		$dataArray["show_discount"] 			= htmlspecialchars($show_discount,ENT_QUOTES);

		$dataArray["billing_street_address"]	= htmlspecialchars($billing_street_address,ENT_QUOTES);
		$dataArray["billing_city"] 				= htmlspecialchars($billing_city,ENT_QUOTES);		
		$dataArray["billing_zip_code"] 			= htmlspecialchars($billing_zip_code,ENT_QUOTES);		
		$dataArray["billing_state"] 			= htmlspecialchars($billing_state,ENT_QUOTES);
		$dataArray["billing_country_name"] 		= htmlspecialchars($billing_country_name,ENT_QUOTES);
		$dataArray["billing_telephone_no"] 		= htmlspecialchars($billing_telephone_no,ENT_QUOTES);
		$dataArray["order_in_favor_of"]			= htmlspecialchars($order_in_favor_of,ENT_QUOTES);
		$dataArray["offer_warranty"] 			= htmlspecialchars($offer_warranty,ENT_QUOTES);
		$dataArray["order_by"] 					= $_SESSION["AdminLoginID_SET"];//htmlspecialchars($order_by,ENT_QUOTES);
		$dataArray["order_id"] 					= htmlspecialchars($pcode,ENT_QUOTES);
		$dataArray["follow_up_date"] 			= htmlspecialchars($follow_up_date,ENT_QUOTES);
		
		$result    								= $s->insertRecord('tbl_order_service', $dataArray); //exit;
/***************************************************ends************************************/
	}
	else
	{
		$row_serv_ord  						= mysqli_fetch_object($rs_serv_ord);
		$order_date_service 			= $row_serv_ord->time_order_serviceed;
	 	$order_id 							= $row_serv_ord->order_id;
		$service_orders_id 					= $row_serv_ord->service_orders_id;
		$offer_subject_service				= $row_serv_ord->offer_subject;
		$get_service_follow_up_date			= $row_serv_ord->follow_up_date;
		$get_show_discount					= $row_serv_ord->show_discount;
		$get_offer_warranty					= $row_serv_ord->offer_warranty;

		$service_payment_terms				= $row_serv_ord->payment_terms;
		$service_delivery_day				= $row_serv_ord->delivery_day;
		$service_offer_validity				= $row_serv_ord->offer_validity;
		
		$service_payment_terms_show=$s->service_payment_terms_name($service_payment_terms);
		
		if($get_service_follow_up_date=="0000-00-00")
		{
			$get_service_follow_up_date									= date("Y-m-d");
		}
		
//		
//	if($service_payment_terms=='1')
//	{
//		$service_payment_terms_show="100% Advance Against P.I.";
//	}
//
//	if($service_payment_terms=='7')
//	{
//	$service_payment_terms_show="With in 7 days in form of demand draft or At Par cheque payable at New
//        Delhi.";
//
//	}
//	if($service_payment_terms=='30')
//	{
//		$service_payment_terms_show="With in 30 days in form of demand draft or At Par cheque payable at New
//
//        Delhi.";
//
//	}
//
//	if($service_payment_terms=='60')
//	{
//		$service_payment_terms_show="With in 60 days in form of demand draft or At Par cheque payable at New
//
//        Delhi.";
//	}
//
//	if($service_payment_terms=='0')
//	{
//		$service_payment_terms_show="Against Delivery in form of demand draft or At Par cheque payable at New
//
//        Delhi.";
//	}
//
//	if($service_payment_terms=='10')
//	{
//		$service_payment_terms_show="100% through letter of credit at site.";
//	}
//	if($service_payment_terms=='80')
//	{
//		$service_payment_terms_show="20% Advance along with work order and rest 80% before dispatch.";
//	}
//	if($service_payment_terms=='50')
//	{
//		$service_payment_terms_show="50% Advance along with work order and rest 50% before dispatch.";
//	}
		//$get_offer_warranty					= $row_serv_ord->offer_warranty;
//		$dataArray["follow_up_date"] 			= htmlspecialchars($follow_up_date,ENT_QUOTES);
	}
	
$sql_freight			= "select * from tbl_order_service_product where deleteflag='active' and service_order_id = '$service_orders_id'";
$result_freight			= mysqli_query($GLOBALS["___mysqli_ston"], $sql_freight);
$row_freight			= mysqli_fetch_object($result_freight);
$freight_amount			= $row_freight->freight_amount;
$freight_amount_after_gst=($freight_amount*(100/118)); //gst percentage amount removed from actual
	
$enq_id					= $s->get_enq_id($lead_id);
//$offercode			= $_REQUEST["offercode"];//orderid
/*$offer_revised_count	= $s->offer_revised_count($pcode);
$sql_offer_coding		= "UPDATE `tbl_order` SET `offercode` = '$offercode$offer_revised_count' WHERE orders_id= $pcode"; //exit;
$rs_offer_coding		= mysqli_query($GLOBALS["___mysqli_ston"], $sql_offer_coding);*/
$action					= $_REQUEST["action"];
if($action=='edit' && $_REQUEST["submit"]=='Generate Service Offer')
	{
		
/*echo "<pre>";
print_r($_REQUEST); */
//exit;
$service_follow_up_date		 			= $_REQUEST["service_follow_up_date"];
$ArrayData["show_discount"]  			= $_REQUEST["service_show_discount"];
$ArrayData["follow_up_date"] 			= $service_follow_up_date;
$ArrayData['offer_subject']  			= $_REQUEST["offer_subject"];//$offer_subject_edit;
//added on 10-april-2020 for save data in service order table 
$ArrayData['designation_id']  			= $_REQUEST["designation"];
$ArrayData['shipping_street_address']  	= $_REQUEST["shipping_street_address"];
$ArrayData['shipping_city']  			= $_REQUEST["shipping_city"];
$ArrayData['shipping_state']  			= $_REQUEST["shipping_state"];
$ArrayData['shipping_zip_code']  		= $_REQUEST["shipping_zip_code"];
$ArrayData['customers_contact_no']  	= $_REQUEST["customers_contact_no"];
$ArrayData['customers_name']  			= $_REQUEST["customers_name"];
$ArrayData['customers_email']  			= $_REQUEST["customers_email"];
	



	
$ArrayData['offer_warranty'] 	= $_REQUEST["offer_warranty"];//$offer_subject_edit;
$ArrayData['payment_terms']  	= $_REQUEST["payment_terms"];//$offer_subject_edit;
$ArrayData['delivery_day']   	= $_REQUEST["delivery_day"];//$offer_subject_edit;
$ArrayData['offer_validity']  	= $_REQUEST["offer_validity"];//$offer_subject_edit;
//$ArrayData['freight_included']  = $_REQUEST["freight_included"];//$offer_subject_edit;

$s->editRecord('tbl_order_service',$ArrayData,'service_orders_id',$service_orders_id);	//exit;
		
$pro_id					= $_REQUEST["pro_id"];
$pro_price				= $_REQUEST["pro_price"];
$pro_qty				= $_REQUEST["pro_qty"];
$today_date				= date("Y-m-d");
$discount_percent		= $_REQUEST["discount_percent"];
//code changed by rumit on 13-12-2017
//$sql_sel_dis			= "select * from `prowise_discount_service`  WHERE orderid= $pcode";
$sql_sel_dis 			= "select * from tbl_order_service where deleteflag='active' and service_orders_id = '$service_orders_id'";
$result_sel_dis			= mysqli_query($GLOBALS["___mysqli_ston"], $sql_sel_dis);
$row_sel_dis			= mysqli_fetch_object($result_sel_dis);
$show_discount			= $row_sel_dis->show_discount;
//print_r($row_sel_dis);
/*	echo "<pre>";
print_r($_REQUEST); */// exit;

$sql					= "Delete from `prowise_discount_service` WHERE orderid= $pcode ";
$result					= mysqli_query($GLOBALS["___mysqli_ston"], $sql);
for($i=0; $i<count($_REQUEST["pro_qty"]); $i++)
{
/*echo "<pre>";
print_r($_REQUEST); */

$pro_quantity			= $pro_qty[$i];//exit;
$pro_sort				= $_REQUEST["pro_sort"][$i];//exit;
/*$pro_price[$i];
$pro_id[$i];
$discount_percent[$i]; */
$freight_gst_per			= 0.18; //18%gst on freight added on 17feb2019
$freight_included_with_gst	=	$_REQUEST['freight_included']+($_REQUEST['freight_included']*$freight_gst_per); //exit;
$freight_included		= $freight_included_with_gst;//$_REQUEST["freight_included"];
$discount_amount		= $pro_price[$i]*$discount_percent[$i]/100; //exit;//code changed by rumit on 13-12-2017
//echo "dis amt=<br>".$discount_amount_revised = implode(",", $discount_amount);
$sql_tbl_pro_dis		= "UPDATE `tbl_order_service_product` SET `pro_quantity` = '$pro_quantity', pro_sort='$pro_sort', freight_amount='$freight_included' WHERE order_id= $pcode and pro_id= $pro_id[$i]";//exit;
$result_tbl_pro_dis		= mysqli_query($GLOBALS["___mysqli_ston"], $sql_tbl_pro_dis); //exit;
if($discount_amount!='' || $discount_amount!='0')
{
//code changed by rumit on 13-12-2017
			 $dataArray_pro_dis['discount_percent']	= $discount_percent[$i];
			 $dataArray_pro_dis['show_discount']	= $show_discount;
			 $dataArray_pro_dis['serviceorderid']	= $service_orders_id;
			 $dataArray_pro_dis['orderid']			= $pcode;
			 $dataArray_pro_dis['proid']			= $pro_id[$i];
			 $dataArray_pro_dis['discount_amount']	= $discount_amount;//($mysql_fetch_dis_price[$i-1]*$_SESSION[$admin_discount1])/100;
			 $s->insertRecord("prowise_discount_service",$dataArray_pro_dis);

			$pro_id_revised 						= implode(",", $pro_id);
			$pro_price_revised 						= implode(",", $pro_price);
			$discount_percent_revised 				= implode(",", $discount_percent);
//exit;
/*********offer revision module starts***********/
			/*	$dataArray["order_id"]				= 		$pcode;
				$dataArray["offer_discount_per"]	= 	    $discount_percent_revised;
				$dataArray["offer_pro_id"]			= 	    $pro_id_revised;
				$dataArray["offer_pro_amt_per_pro"]	= 	    $pro_price_revised;
				$result_mmeber = $s->insertRecord('tbl_offer_revised',$dataArray);*/

//$sql_order_date 		= "UPDATE `tbl_order` SET `date_ordered` = '$today_date' WHERE orders_id= $pcode"; //exit;
//$result_order_date		= mysqli_query($GLOBALS["___mysqli_ston"], $sql_order_date);
}

}
//exit; for loop ends for qty & sort order update
for($i=0; $i<count($_REQUEST["pro_qty"]); $i++)
{
/*echo "<pre>";
print_r($_REQUEST); 
*/
$pro_quantity			= $pro_qty[$i];//exit;
$pro_sort				= $_REQUEST["pro_sort"][$i];//exit;
$pro_price[$i];
$pro_id[$i];
$discount_percent[$i]; 
$discount_amount		= $pro_price[$i]*$discount_percent[$i]/100; //exit;//code changed by rumit on 13-12-2017
//echo "dis amt=<br>".$discount_amount_revised = implode(",", $discount_amount);
$sql_tbl_pro_dis		= "UPDATE `tbl_order_service_product` SET `pro_quantity` = '$pro_quantity', pro_sort='$pro_sort' WHERE order_id= $pcode and pro_id= $pro_id[$i]"; //exit;
$result_tbl_pro_dis		= mysqli_query($GLOBALS["___mysqli_ston"], $sql_tbl_pro_dis); //exit;
}
//exit;
$s->pageLocation("service_offer_delivery_challan.php?pcode=$pcode&serviceorderID=$service_order_id&result=$result_order_date&offercode=$new_offer_code_after_edit");
	}

	$today					= date('d-m-Y');
	$pcode 					= $_REQUEST["pcode"];
	$action					= $_REQUEST["action"];
	$offer_acc_manager		= $row->order_by;
$offer_acc_manager_team_id	= $s->admin_team($offer_acc_manager);
//Team Abrv(A)
$team_abbrv					= $s->team_abbrv($offer_acc_manager_team_id);
//Person Abrv(A)
$admin_abrv					= $s->admin_abrv($offer_acc_manager);
//Enquiry source Abrv(C)
$lead_ref_source			= $s->lead_ref_source($row->lead_id);
$enq_source_abbrv			= $s->enq_source_abbrv($lead_ref_source);
//Category/Application source Abrv(D)
//Product code
$pro_code_offer				= $s->pro_code_offer($row->pro_id);
if($pro_code_offer=='' || $pro_code_offer=='0')
{
//Product code if ist pro hasn't any procodeoffer
$pro_code_offer				= $s->pro_code_offer_all($row->orders_id);
}
/*else
{
echo "PRO CODE=".$pro_code_offer				=$s->pro_code_offer($row->pro_id);
}*/
$prod_ids_concat						= $s->prod_ids_concat($service_orders_id);// for pro id by tbl_order_product
//$lead_app_cat_id_pro_ids_concat			=$s->pro_app_cat_id($prod_ids_concat);
//$cat_abrv					=$s->cat_abrv($lead_app_cat_id);
$cat_abrv					= $s->cat_abrv_app_cat_ids($lead_app_cat_id_pro_ids_concat);
//offer revised how many  times
$offer_revised_count		= $s->offer_revised_count($row->orders_id);
//Offer Coding (A+B+C+D)=Ofer coding

// get previous offer code from tbl_order  table- not generating currently
$offer_ID_coding=$offercode;
//echo "NEw ".$team_abbrv.$admin_abrv.$pro_code_offer.$cat_abrv.$enq_source_abbrv.$offer_revised_count;
//$sql_order_date				= "UPDATE `tbl_order` SET `date_ordered` = '$today_date' WHERE orders_id= $pcode"; //exit;
//$result_order_date			= mysqli_query($GLOBALS["___mysqli_ston"], $sql_order_date);
/*
/**********************************************************************************************************/
	$GST_tax_amt			= $s->GST_tax_amount_on_service_offer($service_orders_id);//$rowOrderPro->GST_percentage;				
	$sql_lead_id			= "SELECT salutation,comp_person_id from tbl_lead where id= '$lead_id'";
	$rs_lead_id				= mysqli_query($GLOBALS["___mysqli_ston"], $sql_lead_id);
	$row_lead_id  			= mysqli_fetch_object($rs_lead_id);
	$salutation				= ucwords($row_lead_id->salutation);
	$comp_person_id			= $row_lead_id->comp_person_id;//added by rumit on 5th April 2018
/*
if($comp_person_id=='0' || $comp_person_id=='')
{
 	*/
	
   	$sql_comp 	= "select * from tbl_order_service where deleteflag='active' and service_orders_id = '$service_order_id'";
	$rs_comp	 	= mysqli_query($GLOBALS["___mysqli_ston"], $sql_comp); 
	if(mysqli_num_rows($rs_comp)>0)
	{
		$row_comp  						= mysqli_fetch_object($rs_comp);
	 	$comp_person_name			= ucfirst(stripslashes($row_comp->customers_name));
		$comp_name					= $s->company_name($row_comp->customers_id);
	 	$comp_person_designation	= str_replace("Others,","",$row_comp->designation_id);
		$comp_shipping_street_address =$row_comp->shipping_street_address.", ".$row_comp->shipping_city.", ".$row_comp->shipping_state.", ".$row_comp->shipping_zip_code;
		$order_date 				= $row_comp->date_ordered;
		$customers_contact_no		= $row_comp->customers_contact_no;
		$customers_name				= $row_comp->customers_name;
		$customers_email			= $row_comp->customers_email;
		$customers_id				= $row_comp->customers_id;		
//		$shipping_street_address 	= $row_comp->shipping_street_address;
		$row_comp->designation_id	= "Others";
		//$comp_person_designation	= str_replace("Others,","",$row_comp->designation_id);
	/*$comp_person_name			= $row_comp->fname."&nbsp;".$row_comp->lname;
	$comp_name					= $s->company_name($row_comp->id);
 	$comp_person_designation	= str_replace("Others,","",$row_comp->designation_id);
	$comp_shipping_street_address=$row_comp->address.", ".$row_comp->city.", ".$row_comp->state.", ".$row_comp->zip;


*/

	}
	
	
	
	
	/*$sql_comp					= "SELECT id,fname,lname,comp_name,designation_id,address,city,country,state,zip,india_mart_co from tbl_comp where id= '$customers_id'";
	$rs_comp					= mysqli_query($GLOBALS["___mysqli_ston"], $sql_comp);
	$row_comp  					= mysqli_fetch_object($rs_comp);
	$comp_person_name			= $row_comp->fname."&nbsp;".$row_comp->lname;
	$comp_name					= $s->company_name($row_comp->id);
 	$comp_person_designation	= str_replace("Others,","",$row_comp->designation_id);
	$comp_shipping_street_address=$row_comp->address.", ".$row_comp->city.", ".$row_comp->state.", ".$row_comp->zip;
	$india_mart_co				= $row_comp->india_mart_co;
}
else
{
	$sql_comp					= "SELECT fname,lname,company_id,designation_id,address,city,country,state,zip from tbl_comp_person where id= '$comp_person_id'";
	$rs_comp					= mysqli_query($GLOBALS["___mysqli_ston"], $sql_comp);
	$row_comp  					= mysqli_fetch_object($rs_comp);
	$comp_person_name			= $row_comp->fname."&nbsp;".$row_comp->lname;
	$comp_name					= $s->company_name($row_comp->company_id);
 	$comp_person_designation	= $s->designation_name($row_comp->designation_id);
}*/

	$sql_admin					= "SELECT * from tbl_admin where admin_id= '$order_by'";
	$rs_admin					= mysqli_query($GLOBALS["___mysqli_ston"], $sql_admin);
	$row_admin  				= mysqli_fetch_object($rs_admin);
	$emp_name					= $row_admin->admin_fname."&nbsp;".$row_admin->admin_lname;
	$emp_designation			= $row_admin->admin_designation;
	$emp_telephone				= $row_admin->admin_telephone;
	$emp_email					= $row_admin->admin_email;
	if($payment_terms=='1')
	{
		$payment_terms_show="100% Advance Against P.I.";
	}

	if($payment_terms=='7')
	{
	$payment_terms_show="With in 7 days in form of demand draft or At Par cheque payable at New
        Delhi.";

	}
		if($payment_terms=='30')
	{
		$payment_terms_show="With in 30 days in form of demand draft or At Par cheque payable at New

        Delhi.";

	}

	if($payment_terms=='60')
	{
		$payment_terms_show="With in 60 days in form of demand draft or At Par cheque payable at New

        Delhi.";
	}

	if($payment_terms=='0')
	{
		$payment_terms_show="Against Delivery in form of demand draft or At Par cheque payable at New

        Delhi.";
	}

	if($payment_terms=='10')
	{
		$payment_terms_show="100% through letter of credit at site.";
	}
	if($payment_terms=='80')
	{
		$payment_terms_show="20% Advance along with work order and rest 80% before dispatch.";
	}
	if($payment_terms=='50')
	{
		$payment_terms_show="50% Advance along with work order and rest 50% before dispatch.";
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $_SERVER['HTTP_HOST'];?>:Your Order</title>
<link href='css/style.css' rel='stylesheet' type='text/css' />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script src="js/web_rupee_symbol.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript">
function printerX()
{
	window.print();
}

function ViewPrintX( orderID)
{
  var path;
  path = "packing_slip.php?pcode="+orderID;
  newwindow=window.open(path,'name',"menubar=1,resizable=1,status=1,toolbar=1,scrollbars=1");
	if (window.focus) {newwindow.focus();}
}
</script>
<style type="text/css">
.pagehead {
	color: #333333;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight: bold;
	background-color: #FFFFFF;
}
.redstar {
	color:#FF0000;
	border: solid 1px #ffffff;
}
.headlist {
	font-size: 13px;
	font-weight: bold;
	color: #333;
	background-color: #F2F2F2;
	/* background: #ffffff; */
    height: 28px;
}
.head {
	color: #333333;
	font-size: 12px;
	font-weight: bold;
	background-color: #FFFFFF;
}
.tblBorder {
	border: 0px solid #fff;
}
.tblBorder_invoice_pro_items {
	border: 1px solid #ededed;
	border-collapse:collapse;
}
.tblBorder_invoice {
	border: 1px solid #000;
	border-collapse:collapse;
}
.tblBorder_invoice_bottom {
	border-bottom: 2px solid #000;
}
.tblBorder_invoice_right {
	border-right: 2px solid #000;
}
.tblBorder_invoice_left {
	border-left: 2px solid #000;
}
.tblBorder_invoice_top {
	border-top: 2px solid #000;
}
.classname {
	border: none;
	width: auto;
	padding-top: 1px;
	height: 290px;
	font-family: '3 of 9 Barcode';
	font-size: 35px;
}
.classname1 {
	border: none;
	width: auto
}
.classname_blue {
	border: none;
	width: auto;
	padding-top: 1px;
	height: 290px;
	font-family: "basawa 3 of 9 MHR";
	font-size: 50px;
}
.classname_blue1 {
	border: none;
	width: auto
}
table.bor_opt tr td {
	color:#000;
	font-family:Arial, Helvetica, sans-serif;
	font-size:13px;
	border:none;
	border:1px solid #ccc;
	padding:3px;
	line-height:15px;
	margin:0px;
}
table.signhed tr td {
	border:1px solid 5px;
	padding:30px 2px 5px 2px;
	vertical-align:bottom;
	text-align:center;
	font-weight:bold;
	font-size:15px;
	line-height:16px;
	vertical-align:top
}
table.tax_all tr td {
	border:1px solid 5px;
	padding:5px;
	vertical-align:bottom;
	line-height:25px;
	line-height:20px;
	vertical-align:top
}
table tr td input, textarea {
	vertical-align:top;
	margin-left:10px;
	border:none;
	border:1px solid #999;
	padding:3px;
	margin-bottom:15px;
}
table tr td input:focus {
	background:#FC9;
}
table tr td textarea:focus {
	background:#990;
}
table tr td strong.left_strong {
	width:170px;
	display:block;
	float:left;
}
table tr td strong.left_normal {
	display:block;
	float:left;
	font-weight:normal;
	margin-right:30px
}
table tr td {
	font-size:11px!important
}
.inputton {
	font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	font-size: 13px;
	font-weight: bold;
	border: double 5px #efefef;
	padding: 3px 20px 3px 20px;
	color: #FFFFFF;
	background-color: #c60006;
	cursor: pointer;
}
.success {
	font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	font-size: 12px;
	color: #060;
	font-weight: normal;
	display: block;
	border: 1px solid #B1CFB1;
	background-image: url(images/success.gif);
	background-repeat: no-repeat;
	background-color: #D1E3D1;
	vertical-align: middle;
	height: 30px;
	margin: 0px;
	padding-left: 30px;
	padding-top: 7px;
}
</style>
<script language="javascript" src="../js/pro_name.js" type="text/javascript"></script>
</head>
<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0" height="950">
  <tr class="text">
    <td valign="top"><form name="offer_del_chlan" id="offer_del_chlan" action="service_offer_delivery_challan.php?action=edit&pcode=<?php echo $pcode;?>&serviceorderID=<?php echo $service_order_id;?>&offercode=<?php echo $offer_ID_coding;?>" method="post">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <?php if($_REQUEST["result"]==1){echo "<p class='success'>".record_update."</p><br />";}?>
      <tr class="text">
        <td><table width="100%" border="0" cellspacing="2" cellpadding="7"  class="tblBorder">
            <tr>
              <td width="56%" valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:11px; line-height:13px" ><img alt="ACS" src="http://www.stanlay.in/images/stanlay-logo-black.png"/><br />
                Asian Contec Limited<br>
                Asian Centre,<br>
                B-28, Okhla Industrial Area, Phase - 1,<br>
                New Delhi - 110020.<br>
                Tel : +91-11-41860000 (100 lines)<br>
                Fax : +91-11-41860066<br>
                Sales helpline : +91-11-41406926<br>
                Email : sales@stanlay.com<br>
                Web : www.stanlay.in | www.stanlay.com</td>
              <td width="44%" align="right" valign="top" style="padding-top:10px;"><img alt="" src="https://www.stanlay.in/images/iso.jpg" height="150"  /></td>
            </tr>
            <tr> </tr>
          </table></td>
      </tr>
      <tr>
        <td align="right" class="pad"  valign="top"><?php if($action!='edit'){?>
          <a href="service_offer_delivery_challan.php?pcode=<?php echo $pcode;?>&serviceorderID=<?php echo $service_order_id;?>&offercode=<?php echo $offer_ID_coding;?>&action=edit" class="inputton pad">Click to edit offer</a>
          <p>&nbsp;</p>
          <?php } else { ?>
          <input type="submit" name="submit" value="Generate Service Offer" class="inputton" />
          <?php }?></td>
      </tr>
      <tr>
        <td colspan="4"><table width="100%" border="0" cellspacing="0" cellpadding="2">
            <tr>
              <td valign="top" class="tblBorder_invoice_bottom tblBorder_invoice_top tblBorder_invoice_left" style="font-family:Arial, Helvetica, sans-serif; font-size:11px; line-height:13px" width="50%"><strong>To,</strong><br />
                <?php if($action!='edit'){?>
				
				<?php echo $salutation;?> <?php echo $comp_person_name;?> <strong><br />
                <?php echo str_replace("Others,","",$comp_person_designation);?></strong><br />
                M/s. <?php echo $comp_name;?><br />
                <?php if($shipping_street_address==''){
				 echo $comp_shipping_street_address; } else{echo $shipping_street_address;?>
                ,<br />
                <?php echo $s->StateName($shipping_state);?>, <?php echo $s->CityName($shipping_city);?>, <?php echo $shipping_zip_code; }?>
                
                <?php } else {
					?>
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>Name</td>
    <td><input type="text" value="<?php echo $comp_person_name;?>" name="customers_name" /></td>
  </tr>
  <tr>
    <td>Designation</td>
    <td><input type="text" value="<?php echo $comp_person_designation;?>" name="designation" /></td>
  </tr>
  <tr>
    <td>Address</td>
    <td><input type="text" value="<?php echo $shipping_street_address;?>" name="shipping_street_address" /></td>
  </tr>
  <tr>
    <td>City</td>
    <td><input type="text" value="<?php echo $shipping_city;?>" name="shipping_city" /></td>
  </tr>
  <tr>
    <td>State</td>
    <td><input type="text" value="<?php echo $shipping_state;?>" name="shipping_state" /></td>
  </tr>
  
  <tr>
    <td>Pincode</td>
    <td><input type="text" value="<?php echo $shipping_zip_code;?>" name="shipping_zip_code" /></td>
  </tr>
</table>

               
					<?php }?>
                </td>
              <td valign="top" class="tblBorder_invoice_right tblBorder_invoice_bottom tblBorder_invoice_top tblBorder_invoice_left pad" align="left"><table width="50%" border="0" cellspacing="0" cellpadding="5">
                  <tr>
                    <td nowrap="nowrap"><strong>Service Reference</strong></td>
                    <td align="left" nowrap="nowrap"><strong>:</strong></td>
                    <td align="left" nowrap="nowrap"><!--ST---> 
                      <?php echo $service_orders_id;//$offer_ID_coding."-".$pcode;
//				echo $offer_ID_coding.$offer_revised_count."-".$pcode;
/*$words = explode(" ",$comp_name);
$acronym = "";
foreach ($words as $w) {
$acronym .= $w[0];
}
echo $acronym;?>/<?php 
$words = explode("-",$order_date); echo $words[2].$words[1].substr($words[0],-2);?>/<?php echo $pcode; */?></td>
                  </tr>
                  <?php /*if($enq_id!='0'){?>
                  <tr>
                    <td><strong>EID</strong></td>
                    <td align="left"><strong>:</strong></td>
                    <td align="left"><?php echo $enq_id;?></td>
                  </tr>
                  <?php } */?>
                  <tr>
                    <td nowrap="nowrap"><strong>Offer Reference</strong></td>
                    <td align="left" nowrap="nowrap"><strong>:</strong></td>
                    <td align="left" nowrap="nowrap"><!--ST---> 
                      <?php echo $offer_ID_coding."-".$pcode;
//				echo $offer_ID_coding.$offer_revised_count."-".$pcode;
/*$words = explode(" ",$comp_name);
$acronym = "";
foreach ($words as $w) {
$acronym .= $w[0];
}
echo $acronym;?>/<?php 
$words = explode("-",$order_date); echo $words[2].$words[1].substr($words[0],-2);?>/<?php echo $pcode; */?></td>
                  </tr>
                  <tr>
                    <td><strong>Date</strong></td>
                    <td align="left"><strong>:</strong></td>
                    <td align="left"><?php echo date("d-M-Y", strtotime($order_date_service));?></td>
                  </tr>
                  <?php if($customers_contact_no!=''){?>
                  <tr>
                    <td><strong>Mob</strong></td>
                    <td align="left"><strong>:</strong></td>
                    <td align="left"><?php echo $customers_contact_no ;?></td>
                  </tr>
                  <?php } ?>
                  <?php if($shipping_telephone_no!=''){?>
                  <tr>
                    <td><strong>Tel</strong></td>
                    <td align="left"><strong>:</strong></td>
                    <td align="left"><?php echo $shipping_telephone_no;?></td>
                  </tr>
                  <?php }
				   if($shipping_fax_no!='')	{?>
                  <tr>
                    <td><strong>Fax</strong></td>
                    <td align="left"><strong>:</strong></td>
                    <td align="left"><?php echo $shipping_fax_no;?></td>
                  </tr>
                  <?php }?>
                  <tr>
                    <td><strong>E-Mail</strong></td>
                    <td align="left"><strong>:</strong></td>
                    <td align="left">
                     <?php
   if($action=='edit' || $action=='delete'){?>
                    <input type="text" value="<?php echo $customers_email;?>" name="customers_email" />
                    <?php } else {?>
                    <?php echo $customers_email;?>
                    <?php }?></td>
                  </tr>
                </table></td>
            </tr>
            <tr>
              <?php
   if($action=='edit' || $action=='delete'){?>
              <td  class="tblBorder_invoice_bottom  tblBorder_invoice_left" colspan="2"  ><table width="100%" border="0" cellpadding="5" cellspacing="0">
                  <tr>
                    <td width="10%"><strong>Subject</strong></td>
                    <td width="50%"><input type="text" value="<?php echo $offer_subject_service;?>" name="offer_subject" class="form-control"  style="width:90%"  /></td>
                    <td width="10%"><strong>Follow up date:</strong></td>
                    <td width="30%"><input type='date'  name='service_follow_up_date' id='service_follow_up_date' required autocomplete='off' value="<?php echo $get_service_follow_up_date;?>"></td>
                  </tr>
                  <tr>
                    <td  ><strong>Show Discount:</strong></td>
                    <td class="pad">&nbsp;&nbsp;&nbsp;
                      <select name="service_show_discount" id="service_show_discount">
                        <option value='Yes' <?php if($get_show_discount == "Yes") {echo "selected='selected'";}?>>Yes</option>
                        <option value='No' <?php if($get_show_discount == "No") {echo "selected='selected'";}?>>No</option>
                      </select></td>
                    <td><strong>Warranty</strong></td>
                    <td class="pad">&nbsp;&nbsp;&nbsp;
                      <?php
    $query_warranty	= " deleteflag = 'active' and warranty_status='active' order by warranty_name";	
	$rs_warranty = $s->selectWhere('tbl_warranty_master', $query_warranty);
?>
                      <select name='offer_warranty'  id='offer_warranty'>
                        <option value='0' <?php if($get_offer_warranty=='0') {echo "selected='selected'";}?>>Nil</option>
                        <?php
if(mysqli_num_rows($rs_warranty)>0)
	{    
	while($row_warranty = mysqli_fetch_object($rs_warranty))
	{        
?>
                        <option value='<?php echo $row_warranty->warranty_id;?>'  <?php if($row_warranty->warranty_id ==  $get_offer_warranty) {echo "selected='selected'";}?>><?php echo stripslashes($row_warranty->warranty_name);?></option>
                        <?php
		}
	}
?>
                      </select></td>
                  </tr>
                  <tr>
                    <td ><strong>Payment Terms:</strong></td>
                    <td class="pad">&nbsp;&nbsp;&nbsp;
                      
                      
                      <?php
    $query_service_payment_terms	= " deleteflag = 'active' and service_order_payment_terms_status='active' order by service_order_payment_terms_name";	
	$rs_service_payment_terms = $s->selectWhere('tbl_service_order_payment_terms_master', $query_service_payment_terms);
?>
                      <select name='payment_terms'  id='payment_terms'  class='inpuTxt'>
                        <option value='0' <?php if($get_offer_service_payment_terms=='0') {echo "selected='selected'";}?>>Nil</option>
                        <?php
if(mysqli_num_rows($rs_service_payment_terms)>0)
	{    
	while($row_service_payment_terms = mysqli_fetch_object($rs_service_payment_terms))
	{        
?>
                        <option value='<?php echo $row_service_payment_terms->service_order_payment_terms_id;?>'  <?php if($row_service_payment_terms->service_order_payment_terms_id ==  $service_payment_terms) {echo "selected='selected'";}?>><?php echo stripslashes($row_service_payment_terms->service_order_payment_terms_name);?></option>
                        <?php
		}
	}
?>
                      </select>
                      
                      
                      
                      
                      <?php /*?><select name='payment_terms' id='payment_terms' class='inpuTxt'>
                        <option value='1' <?php if($service_payment_terms==1) { echo "selected='selected'";}?>>100% Advance Against P.I.</option>
                        <option value='7' <?php if($service_payment_terms==7) { echo "selected='selected'";}?>>Within 7 Days</option>
                        <option value='30' <?php if($service_payment_terms==30) { echo "selected='selected'";}?>>Within 30 Days</option>
                        <option value='60' <?php if($service_payment_terms==60) { echo "selected='selected'";}?>>Within 60 Days</option>
                        <option value='Against_Delivery'  <?php if($service_payment_terms=='Against_Delivery') { echo "selected='selected'";}?>>Against Delivery</option>
                        <option value='10' <?php if($service_payment_terms==10) { echo "selected='selected'";}?>>100% through letters of credit</option>
                        <option value='80' <?php if($service_payment_terms==80) { echo "selected='selected'";}?>>20% Advance along with work order and rest 80% before dispatch</option>
                        <option value='50' <?php if($service_payment_terms==50) { echo "selected='selected'";}?>>50% Advance along with work order and rest 50% before dispatch</option>
                      </select><?php */?></td>
                    <td><strong>Delivery Terms</strong></td>
                    <td class="pad">&nbsp;&nbsp;&nbsp;
                      <select name='delivery_day' id='delivery_day' class='inpuTxt'>
                        <option value='0' <?php if($service_delivery_day==0) { echo "selected='selected'";}?>>NA</option>
                        <option value='1' <?php if($service_delivery_day==1) { echo "selected='selected'";}?>>Immediate Upon Receipt of Payment</option>
                        <option value='2' <?php if($service_delivery_day==2) { echo "selected='selected'";}?>>Immediate Upon Receipt of Order</option>
                        <option value='7' <?php if($service_delivery_day==7) { echo "selected='selected'";}?>>Within 7 Days</option>
                        <option value='15' <?php if($service_delivery_day==15) { echo "selected='selected'";}?>>Within 15 Days</option>
                        <option value='30' <?php if($service_delivery_day==30) { echo "selected='selected'";}?>>Within 30 Days</option>
                        <option value='60' <?php if($service_delivery_day==30) { echo "selected='selected'";}?>>Within 60 Days</option>
                        <option value='90' <?php if($service_delivery_day==90) { echo "selected='selected'";}?>>Within 90 Days</option>
                        <option value='3' <?php if($service_delivery_day==3) { echo "selected='selected'";}?>>Within 4 Months of Receipt of L/C</option>
                      </select></td>
                  </tr>
                  <tr>
                    <td  ><strong>Offer Validity:</strong></td>
                    <td class="pad">&nbsp;&nbsp;&nbsp;
                      <select name='offer_validity' id='offer_validity' class='inpuTxt'>
                        <option value='7' <?php if($service_offer_validity==7) { echo "selected='selected'";}?>>7 Days</option>
                        <option value='15' <?php if($service_offer_validity==15) { echo "selected='selected'";}?>>Within 15 Days</option>
                        <option value='30' <?php if($service_offer_validity==30) { echo "selected='selected'";}?>>Within 30 Days</option>
                        <option value='60' <?php if($service_offer_validity==60) { echo "selected='selected'";}?>>Within 60 Days</option>
                        <option value='90' <?php if($service_offer_validity==90) { echo "selected='selected'";}?>>Within 90 Days</option>
                        <option value='120' <?php if($service_offer_validity==120) { echo "selected='selected'";}?>>Within 120 Days</option>
                        <option value='150' <?php if($service_offer_validity==150) { echo "selected='selected'";}?>>Within 150 Days</option>
                        <option value='180' <?php if($service_offer_validity==180) { echo "selected='selected'";}?>>Within 180 Days</option>
                      </select></td>
                    <td><strong>Freight</strong></td>
                    <td class="pad">
              <input type="text" name='freight_included' id='freight_included' class='inpuTxt' value="<?php echo number_format($freight_amount_after_gst,2);?>" />
                      
                      <?php /*?><select name='freight_included' id='freight_included' class='inpuTxt' required>
                        <option value=''>Select Freight</option>
                        <option value='0' <?php if($freight_amount==0) { echo "selected='selected'";}?>>0</option>
                        <option value='1000' <?php if($freight_amount_after_gst==1000) { echo "selected='selected'";}?>>1000</option>
                        <option value='1500' <?php if($freight_amount_after_gst==1500) { echo "selected='selected'";}?>>1500</option>
                        <option value='2000' <?php if($freight_amount_after_gst==2000) { echo "selected='selected'";}?>>2000</option>
                        <option value='2500' <?php if($freight_amount_after_gst==2500) { echo "selected='selected'";}?>>2500</option>
                        <option value='3000' <?php if($freight_amount_after_gst==3000) { echo "selected='selected'";}?>>3000</option>
                        <option value='4000' <?php if($freight_amount_after_gst==4000) { echo "selected='selected'";}?>>4000</option>
                        <option value='5000' <?php if($freight_amount_after_gst==5000) { echo "selected='selected'";}?>>5000</option>
                        <option value='6000' <?php if($freight_amount_after_gst==6000) { echo "selected='selected'";}?>>6000</option>
                        <option value='7000' <?php if($freight_amount_after_gst==7000) { echo "selected='selected'";}?>>7000</option>
                        <option value='8000' <?php if($freight_amount_after_gst==8000) { echo "selected='selected'";}?>>8000</option>
                        <option value='9000' <?php if($freight_amount_after_gst==9000) { echo "selected='selected'";}?>>9000</option>
                        <option value='10000' <?php if($freight_amount_after_gst==10000) { echo "selected='selected'";}?>>10000</option>
                        <option value='12000' <?php if($freight_amount_after_gst==12000) { echo "selected='selected'";}?>>12000</option>
                        <option value='15000' <?php if($freight_amount_after_gst==15000) { echo "selected='selected'";}?>>15000</option>
                        <option value='20000' <?php if($freight_amount_after_gst==20000) { echo "selected='selected'";}?>>20000</option>
                        <option value='25000' <?php if($freight_amount_after_gst==25000) { echo "selected='selected'";}?>>25000</option>
                        <option value='50000' <?php if($freight_amount_after_gst==50000) { echo "selected='selected'";}?>>50000</option>
                        <option value='100000' <?php if($freight_amount_after_gst==100000) { echo "selected='selected'";}?>>100000</option>
                      </select><?php */?></td>
                  </tr>
                </table></td>
              <?php } else {?>
              <td colspan="2" class="tblBorder_invoice_bottom  tblBorder_invoice_left" >&nbsp;&nbsp;<strong>Subject:</strong> &nbsp; <strong><?php echo $offer_subject_service;?></strong></td>
              <?php } ?>
            </tr>
          </table></td>
      </tr>
      <tr>
        <td class='tblBorder_invoice_right tblBorder_invoice_bottom  tblBorder_invoice_left'><p style="padding-left:10px;"><strong>Dear Sir,</strong><br />
            This Refers to the discussions in regard to the above subject. We are pleased to provide our price offer as below for your consideration.</p></td>
      </tr>
      <?php
   if($action=='edit' || $action=='delete'){
	?>
      
      <!--edit and delete products rumit 11dec2017 added new file for edit offer_products.php-->
    <?php
	if($_REQUEST["action"]=='delete')
	  {
		 //echo "in remove";//exit;
//		$dataArray["deleteflag"] 			= "inactive";//$_REQUEST["inactive"];
		$delete_pro_id	= $_REQUEST["ID"];
		$sql_del		= "DELETE from tbl_order_service_product where pro_id='$delete_pro_id' and order_id='$pcode'";//exit;
		$result    		= mysqli_query($GLOBALS["___mysqli_ston"], $sql_del);
	  $s->pageLocation("service_offer_delivery_challan.php?pcode=$pcode&serviceorderID=$service_order_id&offercode=$offer_ID_coding&result=$result");
	  }	


	if($_REQUEST["delete"]=='Delete')
	 	{
	$pro_del_count=count($_REQUEST["pro_del"]);
	for($d=0; $d<$pro_del_count; $d++)
		{
		$delete_pro_id	= $_REQUEST["pro_del"][$d];
		$sql_del		= "DELETE from tbl_order_service_product where pro_id='$delete_pro_id' and order_id='$pcode'";//exit;
		$result    		= mysqli_query($GLOBALS["___mysqli_ston"], $sql_del);
			}
		$s->pageLocation("service_offer_delivery_challan.php?pcode=$pcode&serviceorderID=$service_order_id&offercode=$offer_ID_coding&result=$result");
//	exit;		
		}
	
	if($_REQUEST["ADD_PRODUCT"]=='Add Product')
	 	{
			
		$service_follow_up_date		 			= $_REQUEST["service_follow_up_date"];
$ArrayData["show_discount"]  			= $_REQUEST["service_show_discount"];
$ArrayData["follow_up_date"] 			= $service_follow_up_date;
$ArrayData['offer_subject']  			= $_REQUEST["offer_subject"];//$offer_subject_edit;
//added on 10-april-2020 for save data in service order table 

$ArrayData['designation_id']  			= $_REQUEST["designation"];
$ArrayData['shipping_street_address']  	= $_REQUEST["shipping_street_address"];
$ArrayData['shipping_city']  			= $_REQUEST["shipping_city"];
$ArrayData['shipping_state']  			= $_REQUEST["shipping_state"];
$ArrayData['shipping_zip_code']  		= $_REQUEST["shipping_zip_code"];
$ArrayData['customers_contact_no']  	= $_REQUEST["customers_contact_no"];
$ArrayData['customers_name']  			= $_REQUEST["customers_name"];
$ArrayData['customers_email']  			= $_REQUEST["customers_email"];
	



	
$ArrayData['offer_warranty'] 	= $_REQUEST["offer_warranty"];//$offer_subject_edit;
$ArrayData['payment_terms']  	= $_REQUEST["payment_terms"];//$offer_subject_edit;
$ArrayData['delivery_day']   	= $_REQUEST["delivery_day"];//$offer_subject_edit;
$ArrayData['offer_validity']  	= $_REQUEST["offer_validity"];//$offer_subject_edit;
//$ArrayData['freight_included']  = $_REQUEST["freight_included"];//$offer_subject_edit;

echo "aaa".$s->editRecord('tbl_order_service',$ArrayData,'service_orders_id',$service_orders_id);	//exit;
		
			
	/////////////////////////////////////////////////////////////////////////////////////////		
	$qtycount=count($_REQUEST["qtyIN"]);
	$procount=count($_REQUEST["proid"]);
	for($q=0; $q<$qtycount; $q++)
			{
			if($_REQUEST["qtyIN"][$q] >=1){
//updated this below query comment and add query on 23-10-2019 to get desc by pro_id in pro_entry table, before this was getring  blank description if description added in product.
//		echo $SQLDOProduct2 		= "select * from tbl_products_entry where model_no='".$_REQUEST["proid"][$q]."'"; exit;
		$SQLDOProduct2 		= "select * from tbl_products_entry where pro_id='".$_REQUEST["proid"][$q]."' and deleteflag='active'"; 
		$SQLDOProductRS2	= mysqli_query($GLOBALS["___mysqli_ston"], $SQLDOProduct2);  
		$SQLDOProductROW2	= mysqli_fetch_object($SQLDOProductRS2);
		
		$dataArray["service_order_id"] 	= $service_orders_id;
		$dataArray["order_id"] 			= $pcode;
		$dataArray["pro_id"] 			= $SQLDOProductROW2->pro_id;
		$dataArray["proidentry"] 		= $SQLDOProductROW2->pro_id_entry;
		$dataArray["customers_id"] 		= $pcode;
		$dataArray["pro_model"] 		= $SQLDOProductROW2->model_no;
		$dataArray["pro_name"] 			= $s->product_name($SQLDOProductROW2->pro_id);
		$pro_price						= $s->product_price($SQLDOProductROW2->pro_id);
		$dataArray["pro_price"] 		= $pro_price;
		$dataArray["pro_final_price"] 	= $pro_price;
		$dataArray['group_id'] 	  		= "0";//$rowProName->group_attr_id;
		$dataArray['qty_attDset_id'] 	= "0";//$rowdrop->pro_att_val_id;
		$dataArray['pro_text'] 			= "0";//$rowdrop->pro_att_val_id;
		$dataArray['pro_ret_amt'] 		= "0";//$rowdrop->pro_att_val_id;
		$dataArray['pro_ret_remarks'] 	= "0";//$rowdrop->pro_att_val_id;
		$dataArray['pro_ret_qty'] 		= "0";//$rowdrop->pro_att_val_id;
		$dataArray['barcode'] 			= "0";//$rowdrop->pro_att_val_id;
		$application_id					= $front->pro_application_id($SQLDOProductROW2->pro_id);
		$GST_percentage					= $front->ApplicationTax($application_id);
		$dataArray["GST_percentage"] 	= $GST_percentage;
		$discounted_price				= 0;
		$discounted_price_tax_amt		= ($pro_price-$discounted_price)*$GST_percentage/100;
		$dataArray["Pro_tax"] 			= $discounted_price_tax_amt;//$s->product_price($SQLDOProductROW2->pro_id);
		$dataArray["hsn_code"] 			= $s->ProHsn_code($SQLDOProductROW2->pro_id);
		$dataArray["pro_quantity"] 		= $_REQUEST["qtyIN"][$q];//1;//changed 1 to dynamic qty on dated 13-mar-2020 to get add pro in one go
		$result    =  $s->insertRecord('tbl_order_service_product', $dataArray);

/*

echo "<br>".$_REQUEST["qtyIN"][$q];
echo "<br>".$_REQUEST["proid"][$q];
echo "<br>".$_REQUEST["proidentry"][$q];*/
			}
			}
//exit;
$s->pageLocation("service_offer_delivery_challan.php?pcode=$pcode&serviceorderID=$service_order_id&offercode=$offer_ID_coding&result=$result_order_date");
		}
			?>  
      <tr>
        <td  valign="top">
          <table width="100%" border="0" cellspacing="1" cellpadding="0" class="bor_opt1">
          <tr>
            <td>
          
          <tr>
            <td  ><table border="0" width="100%" cellspacing="1" cellpadding="0" class="tblBorder_invoice_bottom tblBorder_invoice_top tblBorder_invoice_left">
              <tr>
                <td><h3>ADD/EDIT PRODUCTS:</h3></td>
              </tr>
              <tr>
                <td  valign="top" >
      
      <form name="update_do" method="post" enctype="multipart/form-data">
        <table width="100%" border="0" cellpadding="5" cellspacing="0" >
          <tr>
            <td width="25%" class="pad">&nbsp;&nbsp;&nbsp;<strong>Select Product Group</strong></td>
            <td align="left"><strong>Price List </strong></td>
          </tr>
          <tr>
            <td width="25%"><?php 
	$dashVal = "&nbsp;&nbsp;&nbsp;";
	$sqlCate = "select application_id,application_name from tbl_application where deleteflag = 'active' order by application_name asc";
	$rsCate  = mysqli_query($GLOBALS["___mysqli_ston"], $sqlCate);
	//echo "<br>".	$selected_c->pro_id;
if(mysqli_num_rows($rsCate)>0)
	{
	  echo $dashVal;
	  //echo "india_mart_co".	  $india_mart_co;
	  
	  ?>
              <select name="category_id" id="category_id" onChange="htmlData_service_pro('service_offer_delivery_challan_product_list_ajax.php', 'tm='+this.value + '&'+'tm2='+document.getElementById('price_list').value);">
                <option value="" <?php if($rowCate->application_id == $category_id) {echo "selected='selected'";}?>>All Category</option>
                <?php	while($rowCate = mysqli_fetch_object($rsCate))
					{?>
                <option value="<?php echo $rowCate->application_id;?>" <?php if($rowCate->application_id == $category_id){echo "selected='selected'";}?>><?php echo $rowCate->application_name;?></option>
                <?php }?>
              </select>
              <?php
	} ?></td>
            <td width="25%" align="left"><select name="price_list" id="price_list" onChange="htmlData_service_pro('service_offer_delivery_challan_product_list_ajax.php', 'tm='+document.getElementById('category_id').value + '&'+'tm2='+this.value);">
                <option value="pvt" <?php if($price_list=="pvt") {echo "selected='selected'";}?>>Pvt.</option>
                <option value="govt" <?php if($price_list=="govt") {echo "selected='selected'";}?>>Govt.</option>
                <option value="Export_USD" <?php if($price_list=="Export_USD") {echo "selected='selected'";}?>>Export USD</option>
              </select></td>
          </tr>
          <tr>
            <td colspan="2" id="txtresultcomp1"></td>
          </tr>
        </table>
      </form></td>
  </tr>
</table>
</tr>
</table>
</td>
</tr>
<tr>
  <td  valign="top" class="offers_head">&nbsp;</td>
</tr>
<tr>
  <td  valign="top" class="offers_head"><h3>Previous Item(s) Supplied</h3></td>
</tr>
<tr>
  <td  valign="top" class="offers_head">&nbsp;</td>
</tr>
<tr>
  <td valign="top"><!--delivery challan table shows - serial no , pro name etc as disussed with Amit>-->
    
    <?php $s->OrderItemsInfo_Delivery_Challan($pcode);?>
    
    <!-- </div>--></td>
</tr>
<tr>
  <td  valign="top" class="offers_head">&nbsp;</td>
</tr>
<tr>
  <td  valign="top" class="tblBorder_invoice_bottom" height="30"><h3>Service Offer</h3></td>
</tr>
<tr>
  <td  valign="top" class="offers_head"><?php $s->ServiceOrderItemsInfo_invoice1_edit($service_orders_id);// service orders function created 12-march 2020?></td>
</tr>
<?php } else {?>
<?php /*?><tr>
  <td  valign="top" class="offers_head">Previous Offer</td>
</tr>

<tr>
      <td valign="top"><!--delivery challan table shows - serial no , pro name etc as disussed with Amit>-->
        
        <?php $s->OrderItemsInfo_Delivery_Challan($pcode);?>
        
        <!-- </div>--></td>
    </tr><?php */?>
<tr>
  <td  valign="top" class="offers_head" style="padding-top:50px;"><?php 
  
 // echo "AAA:".$service_orders_id;
  if($service_orders_id=='' || $service_orders_id=='0')
  {
  $s->OrderItemsInfo_invoice1($pcode);	  
  }
  else
  {
  $s->ServiceOrderItemsInfo_invoice1($service_orders_id);
  }
  ?></td>
</tr>
<?php } ?>
<tr>
  <td align="right" class='tblBorder_invoice_right tblBorder_invoice_bottom  tblBorder_invoice_left'><?php $s->ServiceOrderTotalInfo_offer_letter($service_orders_id);///////// order total?></td>
</tr>
</table>
</form>
</tr>
</table>
<table cellpadding="0" cellspacing="0" width="100%">
  <tr>
    <td height="50">&nbsp;</td>
  </tr>
  <tr>
    <td align="right" height="20"><strong style="font-size:12px"><br />
      <br />
      Continues........(Page No.1)</strong></td>
  </tr>
  <tr>
    <td height="50">&nbsp;</td>
  </tr>
  <tr>
    <td class="pad offer_bot"><p>We are an <strong>ISO 9001:2015</strong> certified quality organization with sales and servicing from 6 locations- <strong>New Delhi, Lucknow, Mumbai, Bangalore, Hyderabad & Ahmedabad to ensure that our customers receive our best attention on supply, training and service.</strong></p>
      <p><strong> <u>Terms And Conditions:</u></strong></p>
      <?php if($tax_included=='Excluded' || $GST_tax_amt>0)
{
?>
      <p><strong>Taxes:</strong> IGST extra as shown above<br />
        Our GST No is 07AAACA0859J1ZQ </p>
      <p>In Case of Intra State Sale, IGST should be read as IGST = 50% SGST + 50% CGST for internal references and will be invoice as such </p>
      <?php }?>
      <p><strong>Order in favour of:</strong> Asian Contec Limited, B-28, Okhla Industrial Area, Phase - I, New Delhi -
        110020
        <?php //echo $order_in_favor_of;?>
        <!--Asian Contec Limited, B-28, Okhla Industrial Area, Phase-1, New Delhi-110020--></p>
      <?php if($service_delivery_day!=1 && $service_delivery_day!=2 && $service_delivery_day!=3) {?>
      <p><strong>Delivery:</strong> With in <?php echo $service_delivery_day;?> days upon receipt of order/payment.</p>
      <?php } ?>
      <?php if($service_delivery_day==1) {?>
      <p><strong>Delivery:</strong> Immediate Upon Receipt of Payment.</p>
      <?php } ?>
      <?php if($service_delivery_day==2) {?>
      <p><strong>Delivery:</strong> Immediate Upon Receipt of Order.</p>
      <?php } ?>
      <?php if($service_delivery_day==3) {?>
      <p><strong>Delivery:</strong> Within 4 Months of Receipt of L/C</p>
      <?php } ?>
      <p><strong>Payment: </strong><?php echo $service_payment_terms_show; ?><!--Advance Advance against PI --> 
        
      </p>
      <p><strong>Delivery by: </strong> Spot on/ GATI/ DTDC as mutually agreed.</p>
      <p><strong>Validity: </strong>Offer is valid for <?php echo $service_offer_validity;?> Days from offer date.</p>
      <p> <strong>Warranty:</strong>
        <?php 
//		echo $get_offer_warranty;
		
		if($get_offer_warranty=="6 Months" || $get_offer_warranty=="7") {?>
        6 Months
        <?php } else { ?>
        <?php echo $s->warranty_name($get_offer_warranty); ?>
        <?php  } 
//calibration added in offer 02-march-2020		
		//$offer_calibration="1";
		?>
      </p>
      <?php
	  if($offer_calibration!='1')
	  {
	  ?>
      <p> <strong>Calibration:</strong> Due every <?php echo $s->calibration_name($offer_calibration); ?> </p>
      <?php }?>
      <p> <strong>Note:</strong><!-- Discount % if mentioned in offer should be utilised only for your internal reference & should not/will not be further mentioned in commercial documents including purchase order, invoice etc.--></p>
      <p> 1. Discount % if mentioned in offer should be utilised only for your internal reference & should not/will not be further mentioned in commercial documents including purchase order, invoice etc.</p>
      <p>2. Unless otherwise agreed to, Standard Payments are 100% in Advance,  unless any other payment term has been agreed to. Delay of the payment caused by Buyer, may entitle M/s Asian Contec limited to Charge interest @15 % per annum basis.</p>
      3. Offer is subject to GENERAL STANDARD TERMS AND CONDITIONS FOR THE SALE OF PRODUCTS, enclosed.
      </p>
      <br>
      <p>We look forward to receiving your valued order for which we thank you in advance </p>
      <p><strong>Thanking you,</strong><br />
        Your sincerely,<br />
        For <strong>ASIAN CONTEC LIMITED</strong></p>
      <p>&nbsp;</p>
      <p><strong><?php echo $emp_name;//$s->admin_name($order_by);?></strong><br />
        <strong><?php echo $s->designation_name($emp_designation);?></strong><br />
        Mobile: <?php echo $emp_telephone;?><br />
        Tel No: +91-11-41860000 (100 Lines)<br />
        
        <!--Fax No: +91-11-41860666 <br />--> 
        
        E-Mail: sales@stanlay.com, <?php echo $emp_email;?><br />
        Web: www.stanlay.in<br />
        Web: www.stanlay.com</p></td>
  </tr>
</table>
<style type="text/css" media="print">
.dontprint{display:none!important; border:none!important}
</style>
<p class="dontprint"><a class="pdf" href="service_offer_delivery_challan_pdf.php?pcode=<?php echo $_REQUEST["pcode"]; ?>&offercode=<?php echo $offercode;?>&serviceorderID=<?php echo $service_order_id;?>" target="_blank" ><img src="images/pdf_download.png" alt="Download PDF" title="Download PDF" /></a>
<?php /*?><p class="dontprint"><a class="pdf" href="html-pdf/example03n.php?url=stanlay.in/crm/offer_delivery_challan_pdf.php?pcode=<?php echo $_REQUEST["pcode"]; ?>&offercode=<?php echo $offercode;?>&url_code=<?php echo $_REQUEST["pcode"]; ?>" target="_blank"><img src="images/pdf_download.png" alt="Download PDF" title="Download PDF" /></a> <a class="doc" href="html-doc/example.php?url=stanlay.in/crm/offer_delivery_challan_pdf.php?pcode=<?php echo $_REQUEST["pcode"]; ?>&offercode=<?php echo $offer_ID_coding;?>&url_code=<?php echo $_REQUEST["pcode"]; ?>" target="_blank"><img src="images/doc_download.png" alt="Download Word" title="Download Word" /></a> </p>
<?php */?></body>
</html>