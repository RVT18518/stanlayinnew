<?php
//session_start();
date_default_timezone_set("Asia/Kolkata");
//	unset($_SESSION["message"]);
//include("../includes1/modulefunction.php");
include("../includes1/function_lib.php"); 
$result = mysqli_query($GLOBALS["___mysqli_ston"],'TRUNCATE TABLE tbl_inventory');
//include("session_check.php");
/*******************Outcoming inventory table query insert with outgoing name*******************/
$sql_outgoing="INSERT INTO tbl_inventory (invoice_id, orders_id,customers_id, pro_id, proidentry, pro_model, pro_name, pro_quantity,date_ordered,stock_type) SELECT t1.offercode, t1.orders_id, t1.customers_id, t2.pro_id, t2.proidentry, t2.pro_model, t2.pro_name, t2.pro_quantity, t1.date_ordered, 'outgoing' from tbl_order t1 LEFT OUTER JOIN tbl_order_product t2 on t1.orders_id=t2.order_id where 1=1 and orders_status IN ('Confirmed','Order Closed') ORDER BY `t1`.`orders_id` DESC";
$rs_outgoing=mysqli_query($GLOBALS["___mysqli_ston"],$sql_outgoing);


/*******************Incoming inventory table query insert with Incoming name*******************/
$sql_incoming="INSERT INTO tbl_inventory (invoice_id, orders_id,customers_id, pro_id, proidentry, pro_model, pro_name, pro_quantity,date_ordered,stock_type) 
select  
vendor_po_order.ID, 
vendor_po_item.O_ID,
vendor_po_item.Vendor_List,
vendor_po_item.pro_id,
vendor_po_item.pro_id_entry,
vendor_po_final.Model_No, 
vendor_po_final.Product_Desc,
vendor_po_final.Prodcut_Qty,
vendor_po_order.received_date,
'Incoming' 
from vendor_po_item,vendor_po_order,vendor_po_final 
where vendor_po_item.O_ID=vendor_po_order.ID 
AND vendor_po_order.ID=vendor_po_final.PO_ID 
AND vendor_po_order.Confirm_Purchase='active' 
GROUP by vendor_po_final.ID  
ORDER BY `vendor_po_item`.`O_ID`  DESC";
$rs_incoming=mysqli_query($GLOBALS["___mysqli_ston"],$sql_incoming);
if($rs_incoming==1)
{
	echo "Records inserted successfully!";
}
else
{
	echo "Records insertion failed!";
}
?>