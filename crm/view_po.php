<?php
include("../includes1/modulefunction.php");
include("../includes1/function_lib.php"); 
include("./includes/number_format.php"); 

?>
<style type="text/css">
table {
	border:1px solid #000;
}
table tr th {
	border:1px solid #000;
	font-family:Arial, Helvetica, sans-serif;
	font-size:12px;
	padding:5px;
	font-weight:normal;
	vertical-align:top
}
table tr.nobor th {
	border:1px solid #000;
	border-bottom:0px;
	border-top:0px
}
table tr.nobor_top th {
	border:1px solid #000;
	border-bottom:0px;
}
table tr.nobor_bot th {
	border:1px solid #000;
	border-top:0px
}
pre {
	font-family:Arial, Helvetica, sans-serif;
	font-size:12px;
	margin:0px;
	padding:0px;
}
span.break {
	display:block;
}
</style>
<?php 
		  /*added by rumit 27-09-2019* data was missed befoew because of wrong PO ID it was given ID instead of PO_ID noe we can se view of purchase receipt/order */ 
 	
if($_REQUEST['action']=="Save_Po")
  {
		$sql_product_vendor_price="select * from vendor_po_final where PO_ID='".$_REQUEST['po_id']."'";
		$rs_product_vendor_price=mysqli_query($GLOBALS["___mysqli_ston"],$sql_product_vendor_price);
		$row_product_vendor_price=mysqli_fetch_object($rs_product_vendor_price);
   ?>

<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <th scope="col" colspan="3"><strong style="font-size:16px">PURCHASE ORDER CONFIRM RECEIPT</strong></th>
  </tr>
  <tr>
    <th scope="col" rowspan="3" width="52%" align="left"> <span class="break"><strong>Buyer :</strong></span>
      <pre>
<?php echo $row_product_vendor_price->buyer;  ?>
    </pre>
    </th>
    <th scope="col"  align="left" width="25%"> <span class="break"><strong>Purchase Order No.</strong></span> <span class="break"><strong>ACL - <?php echo $row_product_vendor_price->PO_ID;  ?></strong></span></th>
    <th scope="col" align="left"><span class="break"><strong>Date </strong></span> <span class="break"><strong><?php echo $row_product_vendor_price->Date;  ?></strong></th>
  </tr>
  <tr>
    <th scope="col" align="left"><strong>Suppler's Ref.</strong><br />
      <?php echo $row_product_vendor_price->Sup_Ref;  ?> </th>
    <th scope="col" align="left"> <span class="break"><strong>Destination</strong></span> <span class="break"><?php echo $row_product_vendor_price->Destination;  ?></span></th>
  </tr>
  <tr>
    <th scope="col" colspan="2" align="left"><pre><strong>Despatch Through</strong>
<?php echo $row_product_vendor_price->Dispatch;  ?></pre></th>
  </tr>
  <tr>
    <th scope="col" width="52%" align="left"><pre><strong>Consignee:</strong>
<?php echo $row_product_vendor_price->Consignee;  ?></pre></th>
    <th scope="col"  align="left" width="25%" colspan="2"><strong>Notify :</strong><br />
      <?php echo $row_product_vendor_price->Notify;  ?></th>
  </tr>
  <tr>
    <th scope="col" width="52%" align="left"> <span class="break"><strong>Vendor/Exporter :</strong></span>
      <pre><?php echo $row_product_vendor_price->Exporter;  ?></pre></th>
    <th scope="col"  align="left" width="25%" colspan="2"> <span class="break"><strong>Term Of Delivery</strong></span> <span class="break"> <?php echo $row_product_vendor_price->Term_Delivery;  ?> </span></th>
  </tr>
  <tr>
    <td colspan="3"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="border:none">
        <tr>
          <th scope="col" colspan="3" width="5%"><strong>S. No.</strong></th>
          <th scope="col" colspan="3" width="65%"> <strong>DESCRIPTION OF GOODS</strong> </th>
          <th scope="col" colspan="3"><strong>QTY.</strong></th>
          <th scope="col" colspan="3"><strong>Product Price <br />
            (Each)</strong></th>
          <th scope="col" colspan="3"><strong>Total Price</strong></th>
        </tr>
        <?php     
        $sql_product_vendor1="select * from vendor_po_final where PO_ID='".$_REQUEST['po_id']."'";
		$rs_product_vendor1=mysqli_query($GLOBALS["___mysqli_ston"],$sql_product_vendor1);
		if(mysqli_num_rows($rs_product_vendor1)>0)
		{
			$i=1;
			while($row_product_vendor1=mysqli_fetch_object($rs_product_vendor1))
				{	

  ?>
        <tr class="nobor_top">
          <th scope="col" colspan="3" width="5%" height="50">0<?php echo $i; $i++; ?></th>
          <th scope="col" colspan="3" width="55%" align="left"><?php echo $row_product_vendor1->Product_Desc;  ?></th>
          <th scope="col" colspan="3"><?php echo $row_product_vendor1->Prodcut_Qty;  ?></th>
          <th scope="col" colspan="3"><strong><?php echo $Flag=$row_product_vendor1->Flag;  ?></strong> <?php echo $row_product_vendor1->Prodcut_Price;  ?></th>
          <th scope="col" colspan="3"><strong><?php echo $row_product_vendor1->Flag;  ?></strong>
            <?php 
		  echo round(($row_product_vendor1->Prodcut_Qty*$row_product_vendor1->Prodcut_Price),2); 
		  $total+=$row_product_vendor1->Prodcut_Qty*$row_product_vendor1->Prodcut_Price;  ?></th>
        </tr>
        <?php
			}
	}
	   
	   if($row_product_vendor_price->Tax_Value=="0" && $row_product_vendor_price->Freight_Value=="0") 
	   {
	   
	   ?>
        <tr>
          <th scope="col" colspan="12" align="right"><strong>PO TOTAL</strong></th>
          <th scope="col" colspan="3"><strong><?php echo $Flag." ".round($total,0);  ?></strong></th>
        </tr>
        <?php
	   }
	   else
	     {
			 $Tax_Value			= "0";
			 $Freight_Value 	= "0";
			 $Handling_Value	= "0";
			 $Bank_Value 		= "0";
			 
	   ?>
        <tr>
          <th scope="col" colspan="12" align="right">SUB TOTAL</th>
          <th scope="col" colspan="3"><?php echo $Flag." ".$total;  ?></th>
        </tr>
        <?php 
		if($row_product_vendor_price->Discount!="0") 
	   {
		   ?>
        <tr>
          <th scope="col" colspan="12" align="right">Discount @ <?php echo $row_product_vendor_price->Discount; ?>(%)</th>
          <th scope="col" colspan="3" align="right"> <?php echo $Flag; ?> <?php echo round(($total*$row_product_vendor_price->Discount)/100,2); $total=$total-(($total*$row_product_vendor_price->Discount)/100) ?></th>
        </tr>
        <?php
	   }
	   if($row_product_vendor_price->Packing_Charge!="0") 
	   {
		?>
        <tr>
          <th scope="col" colspan="12" align="right">Packing Charge @</th>
          <th scope="col" colspan="3" align="right"> <?php echo $Flag; ?> <?php echo round(($total*$row_product_vendor_price->Packing_Charge)/100,2); $total=$total+(($total*$row_product_vendor_price->Packing_Charge)/100) ?></th>
        </tr>
        <?php
	   }
	 if($row_product_vendor_price->Discount!="0" || $row_product_vendor_price->Packing_Charge!="0") 
	   {
		?>
        <tr>
          <th scope="col" colspan="12" align="right"><strong>SUB TOTAL</strong></th>
          <th scope="col" colspan="3" align="right"><strong><?php echo $Flag." ".$total;  ?></strong></th>
        </tr>
        <?php
	   }
	   if($row_product_vendor_price->Excise_Duty!="0") 
	   {
		?>
        <tr>
          <th scope="col" colspan="12" align="right">Excise Duty @</th>
          <th scope="col" colspan="3" align="right"> <?php echo $Flag; ?> <?php echo round(($total*$row_product_vendor_price->Excise_Duty)/100,2); $total=$total+(($total*$row_product_vendor_price->Excise_Duty)/100) ?> </th>
        </tr>
        <?php
	   }
	   if($row_product_vendor_price->Education_Cess!="0") 
	   {
		?>
        <tr>
          <th scope="col" colspan="12" align="right">Education Cess on ED @</th>
          <th scope="col" colspan="3" align="right"> <?php echo $Flag; ?> <?php echo round(($total*$row_product_vendor_price->Education_Cess)/100,2); $total=$total+(($total*$row_product_vendor_price->Education_Cess)/100) ?></th>
        </tr>
        <?php
	   }
	   if($row_product_vendor_price->HSEC!="0") 
	   {
		?>
        <tr>
          <th scope="col" colspan="12" align="right">HSEC on ED @</th>
          <th scope="col" colspan="3" align="right"> <?php echo $Flag; ?> <?php echo round(($total*$row_product_vendor_price->HSEC)/100,2); $total=$total+(($total*$row_product_vendor_price->HSEC)/100) ?></th>
        </tr>
        <?php 
	   }
		if($row_product_vendor_price->Tax_Value!="0") 
	   {
		   ?>
        <tr>
          <th scope="col" colspan="12" align="right"><?php echo $row_product_vendor_price->tax_type; echo " (".$row_product_vendor_price->Tax_Value;  ?> %) </th>
          <th scope="col" colspan="3" align="right"> <?php echo $Flag." ";  echo $Tax_Value=round((($total*$row_product_vendor_price->Tax_Value)/100),2);  ?> </th>
        </tr>
        <?php }
		if($row_product_vendor_price->Freight_Value!="0") 
	   {
		 ?>
        <tr>
          <th scope="col" colspan="12" align="right">Freight</th>
          <th scope="col" align="right" > <?php echo $Flag." ";  echo $Freight_Value=$row_product_vendor_price->Freight_Value;  ?></th>
        </tr>
        <?php } 
        
        if($row_product_vendor_price->Handling_Value!="0") 
	   { ?>
        <tr>
          <th scope="col" colspan="12" align="right">Handling Fees</th>
          <th scope="col" align="right" > <?php echo $Flag." ";  echo $Handling_Value=$row_product_vendor_price->Handling_Value;  ?> </th>
        </tr>
        <?php } 
        if($row_product_vendor_price->Bank_Value!="0") 
	   { ?>
        <tr>
          <th scope="col" colspan="12" align="right">Bank Fees</th>
          <th scope="col" align="right"> <?php echo $Flag." ";  echo $Bank_Value=$row_product_vendor_price->Bank_Value;  ?> </th>
        </tr>
        <?php } ?>
        <tr>
          <th scope="col" colspan="12" align="right"><strong>PO TOTAL</strong></th>
          <th scope="col" colspan="3" align="right"><strong><?php echo $Flag." ".round($total=($total+$Tax_Value+$Freight_Value+$Handling_Value+$Bank_Value),0);  ?></strong></th>
        </tr>
        <?php 
		 }
		?>
      </table></td>
  </tr>
</table>
<table cellpadding="0" cellspacing="0" align="center" width="100%" style="border-top:0px">
  <tr>
    <th width="0px" height="150" style="border-right:0px; border-top:0px; text-align:left"> <h3 style="padding:0px; margin:0px; font-size:11px">Amount Chargable (
        <?php  echo $Flag; ?>
        ) :
        <?php 
	$total=round($total,0);
	$obj    = new toWords($total);
    echo ucwords(substr($obj->words,0,-2));  // gives twelve thousand three hundred and forty five  pounds  sixty seven  p

 ?>
        Only/-</h3>
      <br />
      <strong>Buyer Tin No-07760235667</strong>
      <p style="padding:0px; margin:0px; padding-top:10px; font-weight:bold; font-size:11px"><strong>Terms & Conditions:</strong></p>
      <p style="padding:0px; margin:0px">
      
      <ol type="1" style="padding:0px; padding-left:25px; margin:0px; font-weight:bold; font-size:11px; line-height:25px">
        <li>Payment Terms- <?php echo $row_product_vendor_price->Payment_Terms;  ?> </li>
        <li>Delivery:- <?php echo $row_product_vendor_price->Delivery;  ?></li>
        <li>Freight: <?php echo $row_product_vendor_price->Freight;  ?></li>
        <li>Warranty:- &nbsp; <?php echo $row_product_vendor_price->Warranty;  ?>
        <li>Order Acknowledgement:- &nbsp; <?php echo $row_product_vendor_price->ORDER_Acknowledgement;  ?></li>
         <li>Price Basis:- &nbsp; <?php echo $s->vendor_price_basis($row_product_vendor_price->Sup_Ref); ?>
        </li> 
      </ol>
      </p>
    </th>
    <th valign="middle" style="vertical-align:middle; padding:0px; border-left:0px; border-top:0px; width:300px"> <table border="0" cellpadding="0" cellspacing="0" align="right" style="border-right:0px">
        <tr>
          <th style="border-right:0px"> <p style="text-transform:uppercase; font-weight:bold; font-size:12px; letter-spacing:0px; padding:0px; margin:0px">For Asian Contec Ltd. <br />
              <br />
              <br />
              <br />
              <br />
              Authorised Signatory</p>
          </th>
        </tr>
      </table>
    </th>
  </tr>
</table>
<?php
  }
?>
<style type="text/css" media="print">
.dontprint
{ display: none; }
</style>
<p class="dontprint">
  <input type="button" name="submit_po" value="Print"  onclick="window.print()" />
  <input type="button" value="Close" onclick="window.close();" />
</p>
