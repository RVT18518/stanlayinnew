<?php 
include_once("../includes1/function_lib.php");
include_once("session_check.php");
$admin_id=$_SESSION["AdminLoginID_SET"]; 
?>
<!DOCTYPE HTML>
<html>
  <head>
  <meta charset="utf-8">
  <title>Backoffice</title>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="js/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#datepicker" ).datepicker();
  } );

  $( function() {
    $( "#datepicker_to" ).datepicker();
  } );

  </script>
  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <link href="css/bootstrap.css?ver=1.2.2" rel="stylesheet" type="text/css" media="all">
  <link href="css1/style.css?ver=1.2.2" rel="stylesheet" type="text/css" />
  <link href="themes/1/tooltip.css" rel="stylesheet" type="text/css" />
  <script src="themes/1/tooltip.js" type="text/javascript"></script>
    <style type="text/css">
.text_red {
	background-color:#eee;
	color:#333;
	border-bottom:1px solid #fff!important;
	border-top:1px solid #666!important;
}

ul li { display: table; }
ul li ul { display: table-row;line-height:15px; }
ul li ul li { display: table-cell; border: 1px solid #333; width:80px;  font-family:Arial, Helvetica, sans-serif; font-weight:700; font-size:12px;}
</style>
  </head>
  <?php
	$data_action 			= $_REQUEST["action"];
	$CateID		 			= $_REQUEST["CateID"];
	$pcode		 			= $_REQUEST["pcode"];
	$manufacturers_id 		= $_REQUEST["manufacturers_id"];
	$process_status_search 	= $_REQUEST["process_status"];
	$hot_product_search		= $_REQUEST["hot_product_search"];
	$product_type_class_id	= $_REQUEST["product_type_class_id"];	

	if(!isset($_GET['pageno']))
	{ 
    	$page = 1; 
	} 
	else 
	{ 
    	$page = $_GET['pageno']; 
	} 
	if(!isset($_GET['records']))
	{ 
    	$max_results = 10000; 
	} 
	else 
	{ 
    	$max_results = $_GET['records']; 
	} 
	$from = (($page * $max_results) - $max_results);  
	
	if($_REQUEST["status_search"]=="")
	{
		$status_search 			= "pvt";
	}
	else
	{
	$status_search 			= $_REQUEST["status_search"];
	}
	$temp					=1;
	$app_cat_id				= $_REQUEST["app_cat_id"];
	/*echo "<pre>";
	print_r($_REQUEST);*/
	
/**************************************************************************/
//////////////////////////////////////////////// 
function showSubApplicationCategories($cat_id, $dashes = '',$check_pid)
{    
	$dashes .= '&nbsp;&nbsp;&nbsp;&nbsp;&raquo;';    
	$rsSub = mysqli_query($GLOBALS["___mysqli_ston"],"SELECT application_id ,  application_name FROM tbl_application WHERE parent_id1=" . $cat_id ." and deleteflag='active' and application_status='active' order by application_name") or die(mysqli_error());    
	
	if(mysqli_num_rows($rsSub) >= 1)
	{        
		while($rows_sub = mysqli_fetch_array($rsSub))
		{            
			if($rows_sub['application_id'] == $check_pid)
			{
			echo "<option value='$check_pid' selected='selected'>". $dashes . stripslashes($rows_sub['application_name']) . " </option>";
			}
			else
			{
			echo "<option value='".$rows_sub['application_id']."'>".$dashes . stripslashes($rows_sub['application_name']) . "</option><br />";          }
				showSubApplicationCategories($rows_sub['application_id'], $dashes,$check_pid);        
		}    
	}
}
		
?>


  <table  width="90%" align="center"  border="0" cellpadding="0"  cellspacing="0" class="pagecontent" bgcolor="#FFFFFF">
    <tr>
      <td  class="pageheadTop">Product Stock Manager:</td>
    </tr>
    <tr>
      <td class="pHeadLine" colspan="2"></td>
    </tr>
    <tr>
      <td valign="top"><!-- td valign="top" width="20%" class="pad"><?php //include("products_left_category.php"); ?></td-->
        
        <table width="100%" border="0" cellpadding="3" cellspacing="0" class="tblBorder1" >
          <tr>
            <td valign="top" colspan="12"><table width="100%" border="0" cellpadding="5" cellspacing="0" class="table-striped table-hover">
                <tr class='pagehead'>
                <td  valign="middle" class="pad" colspan='7'>Products Warehouse Stock Report:</td>
                <td align="right"  class="headLink" colspan="5"><?php 
	if(isset($_SESSION["ELqueryX"]))
	{
?>
                    <ul>
                    <li><a href="export_product_data.php" >Export to Excel</a></li>
                  </ul>
                    <?php		
	}
	else
	{
?>
                    <ul>
                    <li><a href="index.php?pagename=bestseller_report&error=xlsError" >Export to Excel</a></li>
                  </ul>
                    <?php 
	}
?></td>
              </tr>
                <tr>
                <td colspan="12"><form name="search_filter_stock" id="search_filter_stock" action="view_stock.php?action=search_type" method="post">
                    <table width="100%" cellpadding="0" cellspacing="0" border="0" >
                    <tr class="1text">
                      <td  align="right"> Date from</td>
                      <td><input type='text' name='datepicker' id='datepicker' value="<?php echo $_REQUEST["datepicker"];?>" autocomplete="nopes"  /></td>
                      <td nowrap="nowrap">Date to </td>
                      <td><input type='text'   name='datepicker_to' id='datepicker_to' value="<?php echo $_REQUEST["datepicker_to"];?>" autocomplete="nopes"  /></td>
                      <td><?php 
	$query1	= " deleteflag = 'active' and parent_id1 = 0 and application_status='active' order by application_name ";	
	$rsCate1 = $s->selectWhere('tbl_application', $query1);
?>
                        <select name="app_cat_id" class="inpuTxt" id="app_cat_id" onchange="this.form.submit()">
                          <option value="">Select Product Group</option>
                          <?php 
 //$CateParent
	if(mysqli_num_rows($rsCate1)>0)
	{    
		while($rowCate1 = mysqli_fetch_object($rsCate1))
		{        
?>
                          <option value='<?php echo $rowCate1->application_id;?>' <?php if($rowCate1->application_id == $app_cat_id){echo "selected='selected'";}?>><?php echo stripslashes($rowCate1->application_name);?></option>
                          <?php
				showSubApplicationCategories($rowCate1->application_id,'&nbsp;',$app_cat_id);    
		}
	}
?>
                        </select></td>
                      <td><select name="status_search" class="inpuTxt" id="status_search" style="width:150px" onchange="this.form.submit()">
                          <option value="All">All</option>
                          <option value="Export_USD" <?php if($status_search =="Export_USD"){ echo "selected='selected'";}?>>Export_USD</option>
                          <option value="govt" <?php if( $status_search =="govt"){ echo "selected='selected'";}?>>Govt</option>
                          <option value="pvt" <?php if( $status_search =="pvt"){ echo "selected='selected'";}?>>Pvt</option>
                        </select></td>
                      <td><select name="hot_product_search"  id="hot_product_search" class="inpuTxt" onchange="this.form.submit()">
                          <option value="">Select Hot Product</option>
                          <option value="1" <?php if($hot_product_search=='1'){ echo "selected='selected'";}?> >Yes</option>
                          <option value="0" <?php if($hot_product_search=='0'){ echo "selected='selected'";}?>>No</option>
                        </select></td>
                        
                        <td >
                        
                        <select name="product_type_class_id" class="npuTxt" id="product_type_class_id" onchange="this.form.submit()">
                <option value="" selected="selected">Select Product Type Class</option>
                <?php
	$rs_role_pcl = $s->getData_without_condition('tbl_product_type_class_master','product_type_class_name');
	if(mysqli_num_rows($rs_role_pcl)>0)
	{
		while($row_role_pcl = mysqli_fetch_object($rs_role_pcl))
		{	
?>
                <option value="<?php echo $row_role_pcl->product_type_class_id;?>" <?php if($row_role_pcl->product_type_class_id == $product_type_class_id ){ echo "selected='selected'"; }?>><?php echo $row_role_pcl->product_type_class_name;?></option>
                <?php
		}
	}
?>
              </select></td>
                        
                      <td><input type="submit" value="Search" name="search" id="search" class="inputton" /></td>
                  </form>
                    <!--<form name="frx1" id="frx1" action="view_stock.php?action=search_type" method="post" style="display:inline">
                    <input type="submit" class="inputton" value="View All" name="view_all" />
                  </form>--></td>
              </tr>
              </table></td>
          </tr>
          <tr class="head">
            <td class="pad" align="center">S. No.</td>
            <td width="10%" class="pad">Product Name</td>
            <td width="5%" class="pad">Product Item Code</td>
            <td width="10%" class="pad">Product Category</td>
            <td width="5%" class="pad">List Type</td>
            <td width="10%" class="pad" align="left">Warehouse Stock</td>
            <td width="10%" class="pad" align="left">Product Price</td>
            <td width="5%" class="pad" align="left">Max Discount(%)</td>
            <td width="10%" class="pad" align="left">QBD</td>
            <td width="10%" class="pad" align="left">Last Modified</td>
            <td width="10%" class="pad" align="left">View Old Stock</td>
            <td width="15%" class="pad" align="left">Qty of I/S <br>& Date</td>
          </tr>
          <?php
//$search = "pvt";
/*****************************************************************************************************************/	
//	if($_REQUEST["status_search"]!='' || $_REQUEST["app_cat_id"]!='')
	//{
	
	 $search = $_REQUEST["status_search"];
	 if($search=="")
	 {
		 $search="pvt";
	 }
	 
	 $search_c = $_REQUEST["app_cat_id"];
	 $search_from = $_REQUEST["datepicker"];
	 $datepicker_to = $_REQUEST["datepicker_to"];
	 
	 if($search!='' && $search!='All')

	{
	
	$search_search=" and tbl_products_entry.price_list='$search' ";
	}
	else
	{
	$search_search=" ";
	}
	
	
	 if($search_c!='')

	{
	$search_c_search=" 	 and tbl_products_entry.app_cat_id='$search_c' ";
//	$search_c_search=" and tbl_index_g2.pro_id='$search_c'";
	}
	
	
	if($search_from!='' && $datepicker_to!='')
	{
		
$date_range=" AND (date( tbl_products_entry.last_modified ) BETWEEN '$search_from' AND '$datepicker_to')";
	}
	
if($hot_product_search!='') {
//rumit
$search_hot_product		= " and tbl_products.hot_product='$hot_product_search' ";
	}


if($product_type_class_id!='') {
//rumit
$search_product_type_class_id		= " and tbl_products.product_type_class_id='$product_type_class_id' ";
	}




//new query for pqv changed on 28-12-2019 by rumit
/*echo $sql="Select tbl_products.pro_title,
tbl_products.hot_product,
tbl_products.ware_house_stock,
tbl_products.pro_id,
tbl_products.pro_max_discount, 
tbl_products_entry.pro_price_entry,
tbl_products_entry.app_cat_id, 
tbl_products_entry.last_modified, 
tbl_products_entry.price_list, 
tbl_index_g2.pro_id as app_cat_id_multi, 
tbl_products_entry.model_no 
FROM tbl_products 
INNER JOIN tbl_index_g2 ON tbl_index_g2.match_pro_id_g2=tbl_products.pro_id 
INNER JOIN tbl_products_entry ON tbl_index_g2.match_pro_id_g2=tbl_products_entry.pro_id 
WHERE tbl_products.deleteflag='active' 
and tbl_products.deleteflag = 'active' 
AND tbl_products.STATUS = 'active' 
AND tbl_products_entry.deleteflag = 'active' 
AND tbl_products_entry.STATUS = 'active' 
AND tbl_products_entry.price_list!=''
$search_search 
$search_c_search $date_range  $search_hot_product	 
order by tbl_products.pro_title asc ";
*/

//old query for pqv changed on 28-12-2019 by rumit
 if($search_c!='')
	{
	"Filterrrrr CAT SELE::". $sql="Select tbl_products.pro_title,
tbl_products.hot_product,
tbl_products.qty_slab,
tbl_products.ware_house_stock,
tbl_products.pro_id,
tbl_products.upc_code,
tbl_products.pro_max_discount, 
tbl_products_entry.pro_desc_entry,
tbl_products_entry.pro_price_entry,
tbl_products_entry.app_cat_id, 
tbl_products_entry.last_modified, 
tbl_products_entry.price_list, 
tbl_index_g2.pro_id as app_cat_id_multi, 
tbl_products_entry.model_no 
FROM tbl_products 
INNER JOIN tbl_index_g2 ON tbl_index_g2.match_pro_id_g2=tbl_products.pro_id 
INNER JOIN tbl_products_entry ON tbl_index_g2.match_pro_id_g2=tbl_products_entry.pro_id 
WHERE tbl_products.deleteflag='active' 
and tbl_products.deleteflag = 'active' 
AND tbl_products.STATUS = 'active' 
AND tbl_products_entry.deleteflag = 'active' 
AND tbl_products_entry.STATUS = 'active' 
AND tbl_products_entry.price_list!=''
$search_search 
$search_c_search $date_range  $search_hot_product $search_product_type_class_id	
GROUP by tbl_products.pro_id 
order by tbl_products.pro_title asc ";
	}
	
	else{
			
	"NO CAT SELE::".		$sql = "SELECT 
			tbl_products.pro_id,
			tbl_products.pro_title,
			tbl_products.hot_product,
			tbl_products.qty_slab,	
			tbl_products.upc_code,		
			tbl_products.ware_house_stock,
			tbl_products.pro_max_discount,
			tbl_products_entry.pro_desc_entry,
			tbl_products_entry.pro_price_entry,
			tbl_products_entry.app_cat_id as app_cat_id_multi,
			tbl_products_entry.last_modified,
			tbl_products_entry.price_list,
			tbl_products_entry.model_no
			FROM tbl_products_entry
			left join tbl_products on tbl_products.pro_id=tbl_products_entry.pro_id
			WHERE tbl_products.deleteflag = 'active' AND tbl_products.STATUS = 'active' AND tbl_products_entry.deleteflag = 'active' AND tbl_products_entry.STATUS = 'active' AND tbl_products_entry.price_list!='' 
			$search_search $search_c_search $date_range $search_hot_product $search_product_type_class_id  ORDER BY tbl_products.pro_title ASC ";
	
	}
		/*LIMIT $from, $max_results*/	
			$rsPro  = mysqli_query($GLOBALS["___mysqli_ston"],$sql);
//echo $sql;			
		//$searchRecord="status='active'";
		//$rsPro = $s->getData_withPages('tbl_products',$order_by, $order,$from,$max_results,$searchRecord);
	//}
	
//	    $sql_export = "select * from tbl_comp $searchRecord order by $order_by $order";
//export to excel query
	  $_SESSION["ELqueryX"] = $sql;
	
	if($_REQUEST['view_all']=="View All") {
		
		$sql = "SELECT 
		tbl_products.pro_id,
			tbl_products.pro_title,
			tbl_products.qty_slab,
			tbl_products.upc_code,
			tbl_products.ware_house_stock,
			tbl_products.pro_max_discount,
			tbl_products_entry.pro_desc_entry,
			tbl_products_entry.pro_price_entry,
			tbl_products_entry.last_modified,
			tbl_products_entry.price_list,
			tbl_products_entry.model_no
			FROM tbl_products_entry
			left join tbl_products 
			on tbl_products.pro_id=tbl_products_entry.pro_id
			WHERE tbl_products.deleteflag = 'active' 
			AND tbl_products.STATUS = 'active' 
			AND tbl_products_entry.deleteflag = 'active' 
			AND tbl_products_entry.STATUS = 'active' 
			$date_range 
			ORDER BY tbl_products.pro_title,tbl_products_entry.price_list='pvt' ASC";
			
			$rsPro  = mysqli_query($GLOBALS["___mysqli_ston"],$sql);		
	}
/*****************************************************************************************************************/

//echo $sql;
/*if(mysqli_num_rows($rsPro)<1 && $page > 1)
{
		$s->pageLocation("view_stock.php?pageno=1&orderby=$order_by&order=$order"); 
}*/
//echo mysqli_num_rows($rsPro);
if(mysqli_num_rows($rsPro)>0)
{
	while($rowPro = mysqli_fetch_object($rsPro))
	{
			if($rowPro->ware_house_stock<=0)
			  {
				$text_class="text_red";			
			  }
			  else
			   {
				   $text_class="";
			   }
			   
			
?>
<?php if($rowPro->qty_slab=='Yes')
{?>
<div style="display:none;">
  <div id="qbdtip<?php echo $rowPro->pro_id;?>"> 
    <!-- <img src="http://localhost/stanlay.in/uploads/pro_image/PROIMG_small_6640261.png" style="float:right;margin-left:12px;width:75px;height:75px;" alt="" />-->
    <p>
      <ul>
        <li>
          <ul>
            <li><strong> Quantity</strong></li>
            <li> <strong>Discount Allowed</strong></li>
          </ul>
        </li>
        <?php $sql_qty_dis="select * from tbl_pro_qty_max_discount_percentage where proid=".$rowPro->pro_id." order by min_qty";
				$rs_qty_disc=mysqli_query($GLOBALS["___mysqli_ston"],$sql_qty_dis);
				  $rsqty_num_rows	 = mysqli_num_rows(  $rs_qty_disc);
				if($rsqty_num_rows>0)
				{
				while($row_qty_disc=mysqli_fetch_object($rs_qty_disc)){?>
        <li>
          <ul>
            <li >&nbsp; Qty <?php echo $row_qty_disc->min_qty;?> to <?php echo $row_qty_disc->max_qty;?></li>
            <li style="color:#333">&nbsp; <?php echo $row_qty_disc->max_discount_percent;?>%</li>
          </ul>
        </li>
        <?php } } else {?>
        <li>
          <ul>
            <li >For all Qty </li>
            <li style="color:#FF0000"><?php //echo $s->product_discount($rowPro->pro_id);?>%</li>
          </ul>
        </li>
        <?php }?>
      </ul>
    </p>
  </div>
</div>
<?php }?>

          <tr class="text">
            <div style="display:none;">
              <div id="tip<?php echo $rowPro->pro_id;?>"> 
                <!-- <img src="http://localhost/stanlay.in/uploads/pro_image/PROIMG_small_6640261.png" style="float:right;margin-left:12px;width:75px;height:75px;" alt="" />-->
                <h4><?php echo $rowPro->pro_title;?></h4>
                <p><?php echo $rowPro->pro_desc_entry;?></p>
              </div>
            </div>
            <td align="center" valign="top" class="pad " style="border-bottom:1px solid #eee"><?php echo $temp++; ?></td>
            <td valign="top" class="pad " style="border-bottom:1px solid #eee"><a href="#;" onmouseover="tooltip.pop(this, '#tip<?php echo $rowPro->pro_id;?>');">
              <?php if($rowPro->hot_product=='1'){ ?>
              <span id="glyphicon"  class="glyphicon glyphicon-fire" style="color: orangered;" title="Hot product" ></span>
              <?php } echo $rowPro->pro_title;?>
              </a></td>
            <td valign="top" class="pad " style="border-bottom:1px solid #eee" >
            <strong>Model No.: </strong><?php echo $rowPro->model_no;?><br>
            <strong>UPC Code: </strong><?php echo $rowPro->upc_code;?></td>
            <td align="left" valign="top" class="pad " style="border-bottom:1px solid #eee"><?php 
			echo $s->application_name($s->Product_app_id($rowPro->pro_id));
			//echo $s->application_name($rowPro->app_cat_id_multi);?></td>
            <td align="left" valign="top" class="pad " style="border-bottom:1px solid #eee"><?php echo stripslashes($rowPro->price_list); ?></td>
            <td align="left" valign="top" class="pad " style="border-bottom:1px solid #eee"><?php echo stripslashes($rowPro->ware_house_stock); ?></td>
            <td align="left" valign="top" class="pad " style="border-bottom:1px solid #eee"><?php echo stripslashes($rowPro->pro_price_entry); ?></td>
            <td align="left" valign="top" class="pad " style="border-bottom:1px solid #eee"><?php if($rowPro->price_list=='govt') { echo "0"; } else echo stripslashes($rowPro->pro_max_discount);  ?></td>
            <td align="left" valign="top"  class="pad " style="border-bottom:1px solid #eee">
            <?php if($rowPro->price_list!='govt') { ?>
            <a href="#;" onClick="tooltip.pop(this, '#qbdtip<?php echo $rowPro->pro_id;?>');" <?php if($rowPro->qty_slab=='Yes'){ echo 'style="color:red"';}?> ><strong><?php echo $rowPro->qty_slab;?></strong></a><?php } else { echo "No";}?></td>
            <td align="left" valign="top" class="pad" style="border-bottom:1px solid #eee" nowrap="nowrap"  ><?php $last_modified_date=substr($rowPro->last_modified,0,10);
if($last_modified_date!='')	{  echo $nice_date = date('d-M-Y', strtotime( $last_modified_date));} else { echo "N/A";	}?></td>
            <td align="left" valign="top" class="pad " style="border-bottom:1px solid #eee"><a href="view_stock_previous.php?pcode=<?php echo $rowPro->pro_id;?>" target="_blank">View </a></td>
              <td align="left" valign="top" class="pad " style="border-bottom:1px solid #eee"><?php echo $s->pqv_incoming_stock_qty_with_date($rowPro->upc_code);?></td>
          </tr>
          <?php
				
		}
?>
          <tr class='head headLink'>
            <td colspan="12" align="right" ><table width="100%" cellpadding="1" cellspacing="0" border="0" style="display:none">
                <tr>
                <td align="left"><!--<input type="submit" value="Send to Manager"  name="EmailUpload" class="inputton" onclick="return check_del();" />--></td>
                <td width="43"  align="right" style="display:none"><select name="pages_select" onchange="OnSelectPages();">
                    <?php
			for($i = 1; $i <= $total_pages; $i++)
			{ 
?>
                    <option <?php if($page==$i){ echo "selected='selected'";} ?> 
value="view_stock.php?action=search_type&status_search=<?php echo $status_search;?>&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $i;?>&records=<?php echo $max_results;?>"> <?php echo $i;?></option>
                    <?php
			} 
?>
                  </select></td>
                <td width="59"  align="left" style="display:none"><?php		
			if($page < $total_pages)
			{ 
				$next = ($page + 1); 
				echo "<ul><li><a href='view_stock.php?action=search_type&status_search=$status_search&orderby=$order_by&order=$order&pageno=$next&records=$max_results'>Next </a></li></ul>";
			} 
?></td>
              </tr>
              </table></td>
          </tr>
          <tr class='pagehead'>
            <td  valign="middle" class="pad" colspan='9'></td>
            <td align="right">&nbsp;</td>
            <td align="right">&nbsp;</td>
            <td align="right"><?php /*?>Records View &nbsp;
                    <select name="records" onchange="OnSelect();"  >
                      <option <?php if($max_results==25){ echo "selected='selected'";} ?> 
value="view_stock.php?action=search&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=25">25</option>
                      <option <?php if($max_results==50){ echo "selected='selected'";} ?>
value="view_stock.php?action=search&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=50">50</option>
                      <option <?php if($max_results==100){ echo "selected='selected'";} ?>
value="view_stock.php?action=search&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=100">100</option>
                      <option <?php if($max_results==500){ echo "selected='selected'";} ?>
value="view_stock.php?action=search&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=500">500</option>
                    </select><?php */?></td>
          </tr>
          <tr>
            <td colspan="12" bgcolor="#333333" align="right" style="display:none"><?php  $temp_p=round($top_record_fpo/50)+1;  ?>
              <form name="pading_fpo_frm" method="post">
                <input type="hidden" name="status_search" value="<?php echo $_REQUEST['status_search']; ?>" />
                <input type="hidden" name="app_cat_id" value="<?php echo $_REQUEST['app_cat_id']; ?>" />
                <select name="pading_fpo" onchange="this.form.submit()">
                  <?php for($pageno=1;$pageno<=$temp_p;$pageno++) { ?>
                  <option value="<?php echo $pageno-1; ?>" <?php if($pageno==$_REQUEST['pading_fpo']+1) { echo "selected"; } ?>><?php echo $pageno; ?></option>
                  <?php } ?>
                </select>
              </form></td>
          </tr>
          <?php
	}
	else
	{
?>
          <tr class='text'>
            <td colspan='12' align="center" class="redstar"><?php echo no_record?></td>
          </tr>
          <?php	
	}
?>
        </table></td>
    </tr>
  </table>
  <script type="text/javascript">
	window.onload = function(){
		g_globalObject = new JsDatePick({
			useMode:2,
			target:"follow_up_date",
			dateFormat:"%Y-%m-%d"
			/*selectedDate:{				This is an example of what the full configuration offers.
				day:5,						For full documentation about these settings please see the full version of the code.
				month:9,
				year:2006
			},
			yearsRange:[1978,2020],
			limitToToday:false,
			cellColorScheme:"beige",
			dateFormat:"%m-%d-%Y",
			imgPath:"img/",
			weekStartDay:1*/
		});
		g_globalObject1 = new JsDatePick({
			useMode:2,
			target:"follow_up_date1",
			dateFormat:"%m-%d-%Y"
			/*selectedDate:{				This is an example of what the full configuration offers.
				day:5,						For full documentation about these settings please see the full version of the code.
				month:9,
				year:2006
			},
			yearsRange:[1978,2020],
			limitToToday:false,
			cellColorScheme:"beige",
			dateFormat:"%m-%d-%Y",
			imgPath:"img/",
			weekStartDay:1*/
		});
	};
</script>
</html>