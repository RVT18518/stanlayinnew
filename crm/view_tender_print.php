<?php include("../includes1/function_lib.php"); include("session_check.php"); $admin_id=$_SESSION["AdminLoginID_SET"]; ?>
<?php
	$data_action = $_REQUEST["action"];
	$pcode 		 = $_REQUEST["pcode"];
	$pcode_emp	 = $_REQUEST["pcode_emp"];





	if($data_action=="view")
	{
		
		$rs		  			= $s->getData_with_condition_ten('tbl_tender','ID',$pcode);
		$row	  			= mysqli_fetch_object($rs);
		
		$tnd_submit_on 		= $row->tnd_submit_on;
		$tnd_ID 	  		= $row->tnd_ID;
		$Customer_Name  	= $row->Customer_Name;
		$tnd_Validity  		= $row->tnd_Validity;
		$Due_On				= $row->Due_On;
		$tnd_For			= $row->tnd_For;
		$tnd_Value			= $row->tnd_Value;
		
		$tnd_Document  		= $row->tnd_Document;
		$tnd_EMD_Applicable	= $row->tnd_EMD_Applicable;
		$tnd_Commercial_Bid	= $row->tnd_Commercial_Bid;
		$tnd_EMD			= $row->tnd_EMD;
		$tnd_Technical_Bid  = $row->tnd_Technical_Bid;
		$tnd_EMD_Date		= $row->tnd_EMD_Date;
		$tnd_Emd_Value		= $row->tnd_Emd_Value;
		$tnd_Emd_Draw		= $row->tnd_Emd_Draw;
		$tnd_Scanned_Copy	= $row->tnd_Scanned_Copy;
		$tnd_EMD_Details	= $row->tnd_EMD_Details;
		
		$tnd_winning_status	= $row->tnd_winning_status;
		$tnd_SD_APP  		= $row->tnd_SD_APP;
		$tnd_SD_TYPE		= $row->tnd_SD_TYPE;
		$tnd_DS_Date		= $row->tnd_DS_Date;
		$tnd_SD_Value		= $row->tnd_SD_Value;
		$tnd_SD_Bank		= $row->tnd_SD_Bank;
		$tnd_SD_Return		= $row->tnd_SD_Return;
		$tnd_EMD_SD			= $row->tnd_EMD_SD;
		$tnd_SD_Scanned_Copy= $row->tnd_SD_Scanned_Copy;
		$tnd_SD_Details		= $row->tnd_SD_Details;
		
		
		
		
		$data_action 	= "update";
	}
	
?>
<!DOCTYPE HTML>
<html>
<head>
<title>Stanlay Admin Panel</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all">
<!-- Custom Theme files -->
<link href="css/style.css" rel="stylesheet" type="text/css" media="all"/>
<!--js-->
<!--icons-css-->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!--Google Fonts-->
<link href='//fonts.googleapis.com/css?family=Carrois+Gothic' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Work+Sans:400,500,600' rel='stylesheet' type='text/css'>


<link href="css1/style.css" rel="stylesheet" type="text/css" />
</head>
<body onload="window.print()">
<table width="100%" border="0" cellspacing="0" cellpadding="10">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="5">
        <tr>
          <td width="46%" class="pageheadTop">Tendering &raquo; View Tender</td>
          <td width="47%" class="headLink" align="right"></td>
          <td width="7%" align="right"></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td class="pHeadLine"></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td style="border:1px solid #CCC"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="">
        <tr>
          <td colspan="5" class="pagehead">View Tender Details:</td>
        </tr>
       
        <tr class="text">
          <td align="left" valign="top"  class="pad">Tender submitted on</td>
          <td align="left" valign="top"  ><?php  echo $tnd_submit_on; ?></td>
          <td width="5%" valign="top" >&nbsp;</td>
          <td align="left" valign="top" class="pad">Tender</td>
          <td align="left" valign="top" ><?php echo $tnd_ID;?></td>
        </tr>
       
        <tr class="text">
          <td width="20%" valign="top" class="pad" >Customer Name</td>
          <td width="25%" valign="top" ><?php echo $Customer_Name;?></td>
          <td width="5%" valign="top" >&nbsp;</td>
          <td align="left" valign="top" class="pad">Tender Validity</td>
          <td align="left" valign="top" ><?php echo $tnd_Validity;  ?></td>
        </tr>
       
        <tr class="text">
          <td align="left" valign="top" class="pad">Tender Value</td>
          <td align="left" valign="top" ><?php echo $tnd_Value; ?></td>
          <td align="left" valign="top" >&nbsp;</td>
          <td align="left" valign="top" class="pad">Due On</td>
          <td align="left" valign="top" ><?php echo $Due_On; ?></td>
        </tr>
       
        <tr class="text">
          <td align="left" valign="top" class="pad">Tender For</td>
          <td align="left" valign="top" ><?php echo $tnd_For; ?></td>
          <td align="left" valign="top" ><strong></strong></td>
        </tr>
        <tr>
          <td colspan="5" class="pagehead">Tender Documents & EMD Details:</td>
        </tr>
       <tr class="text">
          <td align="left" valign="top"  class="pad">Tender Documents </td>
          <td align="left" valign="top"  ><a href="../<?php echo $tnd_Document; ?>" target="_blank">Click to View Tender Documents</a> </td>
          <td align="left" valign="top" >&nbsp;</td>
          <td align="left" valign="top" class="pad">EMD Applicable</td>
          <td align="left" valign="top" ><?php echo $tnd_EMD_Applicable; ?> </td>
        </tr>
       
        <tr class="text">
          <td align="left" valign="top"  class="pad">Commercial Bid </td>
          <td align="left" valign="top"  ><a href="../<?php echo $tnd_Commercial_Bid; ?>" target="_blank">Click to View Commercial Bid</a></td>
          <td align="left" valign="top" >&nbsp;</td>
          <td align="left" valign="top"  class="pad">EMD (If EMD Applicable Yes)</td>
          <td align="left" valign="top"  ><?php echo $tnd_EMD; ?></td>
        </tr>
       
        <tr class="text">
          <td align="left" valign="top" class="pad">Technical Bid </td>
          <td align="left" valign="top"><a href="../<?php echo $tnd_Technical_Bid; ?>" target="_blank">Click to View Technical Bid</a></td>
          <td align="left" valign="top">&nbsp;</td>
          <td align="left" valign="top" class="pad">EMD Dated</td>
          <td align="left" valign="top"><?php echo $tnd_EMD_Date; ?>
        </td>
        </tr>
        
           <tr class="text">
          <td align="left" valign="top" class="pad">EMD Scanned Copy </td>
          <td align="left" valign="top"><a href="../<?php echo $tnd_Scanned_Copy; ?>" target="_blank">Click to View EMD Scanned</a></td>
          <td align="left" valign="top">&nbsp;</td>
          <td align="left" valign="top" class="pad">EMD Details </td>
          <td align="left" valign="top"><?php echo $tnd_EMD_Details; ?>
        </td>
        </tr>
        
        
       
        <tr>
          <td align="left" valign="top" class="pad">EMD Value</td>
          <td align="left" valign="top"><?php echo $tnd_Emd_Value; ?></td>
          <td align="left" valign="top">&nbsp;</td>
          <td align="left" valign="top" class="pad">Drawn On (Bank)</td>
          <td align="left" valign="top" >
          
          <?php echo $tnd_Emd_Draw; ?>
          
         </td>
        </tr>
        <tr>
       
        
          <td align="left" valign="top" class="pad">Return By</td>
          <td align="left" valign="top" ><?php echo $tnd_return_by; ?></td>
        </tr>
        
        
          <tr>
            <td colspan="5" class="pagehead"><h5>Security Deposit Details</h5></td>
          </tr>
          
           
           <tr class="text">
            <td align="left" valign="top" class="pad">Tender Status</td>
            <td align="left" valign="top" ><?php if($tnd_winning_status=="") { echo "Tender in Consideration"; } else {  echo $tnd_winning_status;  } ?></td>
          </tr>
         
       
          <tr class="text">
            <td align="left" valign="top" class="pad">SD Applicable</td>
            <td align="left" valign="top" >
            
            <?php echo $tnd_SD_APP; ?>
            
          </td>
            <td align="left" valign="top" >&nbsp;</td>
            <td align="left" valign="top"  class="pad">SD (If SD Applicable Yes)</td>
            <td align="left" valign="top"  >
            
            <?php echo $tnd_SD_TYPE; ?>
            
           </td>
          </tr>
          
            <tr class="text">
          <td align="left" valign="top"  class="pad">SD Scanned Copy </td>
          <td align="left" valign="top"  ><a href="../<?php echo $tnd_SD_Scanned_Copy; ?>" target="_blank">Click to View SD Scanned Copy</a></td>
          <td align="left" valign="top" >&nbsp;</td>
          <td align="left" valign="top" class="pad">SD Details </td>
          <td align="left" valign="top" ><?php echo $tnd_SD_Details; ?>
        </td>
        </tr>
         
          <tr class="text">
            <td align="left" valign="top" class="pad">SD Dated</td>
            <td align="left" valign="top" ><?php echo $tnd_DS_Date; ?></td>
            <td align="left" valign="top" >&nbsp;</td>
            <td align="left" valign="top" class="pad">SD Value</td>
            <td align="left" valign="top" ><?php echo $tnd_SD_Value; ?></td>
          </tr>
         
          <tr>
            
            <td align="left" valign="top" class="pad">Drawn On (Bank)</td>
            <td align="left" valign="top" >
            <?php echo $tnd_SD_Bank; ?>
            
            </td>
           <td>&nbsp;</td>
            <td align="left" valign="top" class="pad">Return By</td>
            <td align="left" valign="top"> <?php echo $tnd_SD_Return; ?></td>
          </tr>
         
        
          <tr class="text">
            <td align="left" valign="top" class="pad">EMD Received</td>
            <td align="left" valign="top" >
            <?php echo $tnd_EMD_SD; ?>
            
           </td>
          </tr>
          <tr>
    <td colspan="5">&nbsp;</td>
  </tr>
        
            
       
      </table></td>
  </tr>
  
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
