<?php
$url=$s->fetchGeneral_config('siteurl')."crm/"; 
?>
<link href="../css1/style.css" rel="stylesheet" type="text/css" />
<style>
.dropbtn {
	background-color: #333;
	color: white;
	padding: 5px;
	font-size: 12px;
	margin-right:50px;
	border: none;
	cursor: pointer;
}
.dropdown {
	position: relative;
	display: inline-block;
	padding: 10px;
}
.dropdown-content {
	display: none;
	position: absolute;
	background-color: #f9f9f9;
	min-width: 100px;
	box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
	z-index:99999999999
}
.dropdown-content a {
	color: black;
	padding: 12px 16px;
	text-decoration: none!important;
	display: block;
	color:#333!important;
	text-align:left;
}
.dropdown-content a:hover {
	color: black;
	padding: 12px 16px;
	text-decoration: none;
	display: block;
	background-color: #CCCCCC;
}
.dropdown:hover .dropdown-content {
	display: block;
}
.dropdown:hover .dropbtn {
	background-color: #3e8e41;
}
.menuH {
	margin: 0;
	width: 100%;
	float:none;
	height: 36px;
	background: #333333 url(bg.gif) repeat-x 0 0;
	border: 1px solid #BBB;
	position:relative;/*! for IE htc*/
	z-index:4;/*H2*/
	font-family: Arial, Helvetica, sans-serif;
	list-style: none;
	padding: 0;
	margin-top:10px
}
.menuH li {
	padding: 0;
	/*margin: 0;



    display: block; TODO: I am testing them if they can be removed. If can, remove them.*/



    float: left;
	height: 36px;
	position: relative;/*move it into .menuH if you want submenu to be positioned relative to the whole menu instead of this li element*/
}
.menuH li:hover, .menuH li.onhover {
	background: #ccc url(bg.gif) repeat-x 0 -48px;
}
ul.menuH a {
	padding: 0 10px;
	line-height: 36px; /*Note: keep this value the same as the height of .menuH and .menuH li */
	font-size: 12px;
	font-weight: bold;
	color: #FFFFFF;
	display: block;
	outline: 0;
	text-decoration: none;
}
.menuH a:hover {
	color: red;
}
.menuH a.arrow {
	background: url(arrow.gif) no-repeat right center;
}
.menuH ul a.arrow {
	background: url(right.gif) no-repeat 97% center;
}
/*Used to align a top-level item to the right*/        



.menuH .menuRight {
	float: right;
	margin-right: 0px;
}
/*for the top-level separators*/



.menuH li.separator {
	font-size:0;
	overflow:hidden;
	border-left:1px solid #F7F7F7;
	border-right:1px solid #BBBBBB;
}
/*######sub level###########*/



        



ul.menuH ul {
	width:260px;
	position: absolute;
	left: -9999px;
	border: 1px solid #DDD;
	border-top: 0;
	background: #FFF;
	text-align: left;
	list-style: none;
	margin: 0;
	/*Following 1px(padding-right) will determine how much it is overlapped by the sub-sub-menu */



    padding: 10px 0;
	box-sizing:content-box;
}
.menuH li li {
	float: none;
	white-space:nowrap;
	height: 26px;
	border-bottom:1px solid rgba(204,204,204,1);
	padding-bottom:30px;
	font-size: 12px!important;
}
.menuH li li:hover, .menuH li li.onhover {
	background:#f7f7f7;
}
.menuH ul a {
	padding: 0 20px;
	line-height: 30px;
	font-size: 1.2em!important;
	;
	font-weight: normal;
	color: #0066CC;
	text-align: left;
}
.menuH ul a:hover {
	color: red;
}
.menuH li:hover ul, .menuH li.onhover ul {
	left: -1px;/*Use this property to change offset of the dropdown*/
	top: auto;
}
.menuH li:hover .dropToLeft, .menuH li.onhover .dropToLeft {
	left: auto;
	right: -1px;
	top: auto;
}
.menuH ul ul {
	border-top: 1px solid #DDD;
}
.menuH li:hover ul ul, .menuH li:hover ul ul ul, .menuH li:hover ul ul ul ul,  .menuH li.onhover ul ul, .menuH li.onhover ul ul ul, .menuH li.onhover ul ul ul ul {
	left: -9999px;
	top:0;
}
.menuH li li:hover ul, .menuH li li li:hover ul, .menuH li li li li:hover ul,  .menuH li li.onhover ul, .menuH li li li.onhover ul, .menuH li li li li.onhover ul {
	left: 260px;
}
/*####### special effects ##########*/



        



.decor1 {
	-moz-border-radius: 3px; /* Firefox */
	-webkit-border-radius: 3px; /* Safari and Chrome */
	border-radius: 3px; /* Opera 10.5+, future browsers, and now also our behavior htc file */
	-moz-box-shadow: 0px 1px 4px #eee; /* Firefox */
	-webkit-box-shadow: 0px 1px 4px #eee; /* Safari and Chrome */
	box-shadow: 0px 1px 4px #eee; /* Opera 10.5+, future browsers and IE6+ using our behavior htc file */
}
.menuH ul {
	-moz-border-radius: 0px 0px 4px 4px;
	-webkit-border-radius: 0px 0px 4px 4px;
	border-radius: 0px 0px 4px 4px;
	-moz-box-shadow: 0px 6px 6px #CCC;
	-webkit-box-shadow: 0px 6px 6px #CCC;
	box-shadow: 0px 6px 6px #CCC;
	/* Gradient background */



    background: -moz-linear-gradient(top, #FFFFFF, #EEEEEE);
	background: -webkit-gradient(linear, 0% 0%, 0% 100%, from(#FFFFFF), to(#EEEEEE));
}
</style>
<!--skycons-icons-->

<script src="../../js/skycons.js"></script>

<!--//skycons-icons-->
<style>
	table tr td {
			border-right:0px solid #CCC!important;
			border-bottom:0px solid #CCC!important;
	}
		table tr th {
			border-right:0px solid #CCC!important;
			border-bottom:0px solid #CCC!important;
			padding:10px;
	}

	table tr td {
			font-size:12px!important;
	}
	.bdr_bottom{border-bottom:1px solid #CCC!important;}
	.bdr_top{border-top:1px solid #CCC!important;}
	.bdr_right{border-right:1px solid #CCC!important;}
	.bdr_left{border-left:1px solid #CCC!important;}
	.tbl_border{border:1px solid #CCC!important;}
	
	.back_button {
  background: #3498db;
  background-image: -webkit-linear-gradient(top, #3498db, #2980b9);
  background-image: -moz-linear-gradient(top, #3498db, #2980b9);
  background-image: -ms-linear-gradient(top, #3498db, #2980b9);
  background-image: -o-linear-gradient(top, #3498db, #2980b9);
  background-image: linear-gradient(to bottom, #3498db, #2980b9);
  -webkit-border-radius: 28;
  -moz-border-radius: 28;
  border-radius: 28px;
  font-family: Arial;
  color: #ffffff;
  font-size: 15px;
  padding: 8px 15px 8px 15px;
  text-decoration: none;
}
.back_button a{
  color: #ffffff;
  border:none!important;
}
.back_button:hover a{
  color: #fff000;
  border:none!important;
}
.back_button:hover {
  background: #3cb0fd;
  background-image: -webkit-linear-gradient(top, #3cb0fd, #3498db);
  background-image: -moz-linear-gradient(top, #3cb0fd, #3498db);
  background-image: -ms-linear-gradient(top, #3cb0fd, #3498db);
  background-image: -o-linear-gradient(top, #3cb0fd, #3498db);
  background-image: linear-gradient(to bottom, #3cb0fd, #3498db);
  text-decoration: none;
}
.back_button_ac {
  background: #339966;
  text-decoration: none;
}


.submit_but {
  background: #3498db;
  background-image: -webkit-linear-gradient(top, #3498db, #2980b9);
  background-image: -moz-linear-gradient(top, #3498db, #2980b9);
  background-image: -ms-linear-gradient(top, #3498db, #2980b9);
  background-image: -o-linear-gradient(top, #3498db, #2980b9);
  background-image: linear-gradient(to bottom, #3498db, #2980b9);
  -webkit-border-radius: 28;
  -moz-border-radius: 28;
  border-radius: 28px;
  font-family: Arial;
  color: #ffffff;
  font-size: 20px;
  padding: 10px 20px 10px 20px;
  text-decoration: none;
  cursor:pointer
}

.submit_but:hover {
  background: #3cb0fd;
  background-image: -webkit-linear-gradient(top, #3cb0fd, #3498db);
  background-image: -moz-linear-gradient(top, #3cb0fd, #3498db);
  background-image: -ms-linear-gradient(top, #3cb0fd, #3498db);
  background-image: -o-linear-gradient(top, #3cb0fd, #3498db);
  background-image: linear-gradient(to bottom, #3cb0fd, #3498db);
  text-decoration: none;
  outline:none
}

table{border: 0px solid #ddd}
</style>

<section>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="100%"><table width="100%" border="0" cellspacing="0" cellpadding="10" id="pageheader">
        <tr>
          <td bgcolor="#fff" style="padding:15px 15px 15px 50px "><!--<h1>Stanlay</h1>-->
            
            <?php



		  if($_SESSION["AdminLoginID_SET"]=='4')



		  {



		  ?>
            <a href="<?php echo $url;?>index.php?pagename=report_summary" ><img src="../images/logo.png" width="180" height="51" alt="img" border="0" /></a>
            <?php } else {?>
            <a href="<?php echo $url;?>index.php?pagename=report_summary" ><img src="../images/logo.png" width="180" height="51" alt="img" border="0" /></a>
            <?php }?></td>
          <td  align="center"  bgcolor="#fff" style="padding:15px"></td>
          <?php if($_SESSION["AdminLoginID_SET"]!=''){?>
          <td width="49%" class="topRight" bgcolor="#fff">Welcome <?php echo $_SESSION["AdminName_SET"];?><span class="saperator">|</span> <?php echo date("l F d, Y")?> <span class="saperator">|</span> <a href="<?php echo $url;?>index.php?pagename=report_summary" ><strong>Home</strong></a> <span class="saperator">|</span>
            <div class="dropdown">
              <button class="dropbtn">My Account</button>
              <div class="dropdown-content"> <a href="<?php echo $url;?>index.php?pagename=change_password" style="border-bottom:none">Change Password</a> <a href="<?php echo $url;?>index.php?pagename=view_attendance" style="border-bottom:none">My Attendance</a> <a href="<?php echo $url;?>logout.php">Logout</a> </div>
            </div></td>
          <?php }?>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td width="100%" height="30" style="text-align:left; background-color:#DDEFFF; font-weight:bold;; color:#666; border-bottom:2px solid #060; font-family:Arial, Helvetica, sans-serif; padding-left:10px; padding-top:5px; font-size:12px"><marquee behavior="alternate" scrollamount="2" scrolldelay="2" onmouseover="this.stop();" onmouseout="this.start();" direction="right">
      Hi, <strong style="text-decoration:underline"><?php echo trim($_SESSION["AdminName_SET"]); ?></strong>, Welcome to your control Room for planning, organizing &amp; controlling your activities &amp; sales.
      </marquee></td>
  </tr>
  <tr>
    <td height="15" ><ul class="menuH decor1" style="width:94.5%; margin:15px auto">
        <li><a href="<?php echo $url;?>index.php?pagename=report_summary" class="arrow"><img src="images/newicon/dashboard.png" width="14" style="margin-top: -2px;" /> My Dashboard</a></li>
        <li><a  href="index.php?pagename=sales_manager" class="arrow"><img src="images/newicon/sales1.png" width="14" style="margin-top: -2px;" /> Sales Manager</a>
          <ul>
            <li><a href="<?php echo $url;?>index.php?pagename=company_manager" class="arrow">Company Manager</a></li>
            <li><a href="<?php echo $url;?>index.php?pagename=add_lead&action=add_new" class="arrow">Create New Lead/Offer</a></li>
            <li><a href="<?php echo $url;?>index.php?pagename=lead_manager" class="arrow">View Leads/Create Offer</a></li>
            <li><a href="<?php echo $url;?>index.php?pagename=order_manager">View Your Offers</a></li>
            <li><a href="<?php echo $url;?>index.php?pagename=sales_manager">View Sales Performance</a></li>
            <li><a href="<?php echo $url;?>index.php?pagename=transfer_account_manager">Transfer Account Manager</a></li>
            <li><a href="<?php echo $url;?>index.php?pagename=web_enq_manager">Enquiry Manager</a></li>
          </ul>
        </li>
        <li><a class="arrow"><img src="images/newicon/dispatch.png" width="14" style="margin-top: -2px;" /> Dispatch Cordination</a>
          <ul>
            <li><a>Demos</a></li>
          </ul>
        </li>
        <li><a class="arrow"><img src="images/newicon/warehousing-icon.png" width="14" style="margin-top: -2px;" /> Warehousing</a>
          <ul>
            <li><a href="<?php echo $url;?>view_stock.php" target="_blank">Product Quantity View (PQV)</a></li>
            <li><a href="<?php echo $url;?>index.php?pagename=manager_product_stock">Stock Out</a></li>
            <li><a href="<?php echo $url;?>index.php?pagename=view_incoming_stock">Incoming Manager</a></li>
            <li><a href="<?php echo $url;?>index.php?pagename=generate_serial_no">Generate Serial No </a></li>
          </ul>
        </li>
        <li><a class="arrow"><img src="images/newicon/nav-icon-history.png" width="14" style="margin-top: -2px;" /> Purchasing</a>
          <ul>
            <li><a href="<?php echo $url;?>index.php?pagename=purchase_manager">Purchase Manager</a></li>
            <li><a href="<?php echo $url;?>index.php?pagename=vendor_manager">Vendor Master</a></li>
            <li><a href="<?php echo $url;?>index.php?pagename=add_vendor_product_list">Vendor Products Manager</a></li>
            <li><a href="<?php echo $url;?>index.php?pagename=do_manager">DO Manager</a></li>
          </ul>
        </li>
        <li><a class="arrow" href="<?php echo $url;?>index.php?pagename=tendering&action=ten"><img src="images/newicon/Info-Icon.png" width="14" style="margin-top: -2px;" /> Tendering</a>
          <ul>
            <li><a href="<?php echo $url;?>index.php?pagename=add_tender&action=add_new">Upload Tender</a></li>
            <li><a href="<?php echo $url;?>index.php?pagename=view_open_tender&action=view_open_tender">View Open Tenders</a></li>
            <li><a href="<?php echo $url;?>index.php?pagename=view_close_tender&action=view_close_tender">View Close Tenders</a></li>
            <li><a href="<?php echo $url;?>index.php?pagename=view_emd_stat&action=view_emd_stat">View EMD Status</a></li>
            <li><a href="<?php echo $url;?>index.php?pagename=view_sd_stat&action=view_sd_stat">View SD Status</a></li>
          </ul>
        </li>
        <li><a class="arrow" href="<?php echo $url;?>index.php?pagename=kc_home"><img src="images/newicon/tender.png" width="14" style="margin-top: -2px;" /> Knowledge Center</a>
          <ul>
            <li> <a href="<?php echo $url;?>index.php?pagename=add_fol_cat&action=add_new">Add Folder/Document</a></li>
            <li> <a href="<?php echo $url;?>index.php?pagename=edit_fol_cat&action=edit">Edit Folder/Document</a></li>
            <li> <a href="<?php echo $url;?>index.php?pagename=kc_home">View/Download Folder/Document</a></li>
          </ul>
        </li>
        <li><a class="arrow" href="<?php echo $url;?>index.php?pagename=ac_payble&action=view"><img src="images/newicon/automatic-payment-plan.png" width="14" style="margin-top: -2px;" /> A/c Payable</a>
          <ul>
            <li><a href="<?php echo $url;?>index.php?pagename=cus_payments&action=view_payments">Pending Customers Payments</a></li>
            <li><a href="<?php echo $url;?>index.php?pagename=ven_payments&action=view_payments">Pending Vendors Payments</a></li>
            <li><a href="<?php echo $url;?>index.php?pagename=c_form_payments&action=view_payments">Pending C Form Receivable</a></li>
            <li><a href="<?php echo $url;?>index.php?pagename=c_form_ven&action=view_payments">Vendor C Form Payable</a></li>
            <li><a href="<?php echo $url;?>index.php?pagename=view_emd_stat&action=view_emd_stat">Pending EMD Status</a></li>
            <li><a href="<?php echo $url;?>index.php?pagename=view_sd_stat&action=view_sd_stat">Pending SD Status</a></li>
          </ul>
        </li>
        <li><a class="arrow"><img src="images/newicon/icon-crm.png" width="14" style="margin-top: -2px;" /> HR Management</a>
          <ul>
            <li><a href="<?php echo $url;?>index.php?pagename=manage_designation" class="arrow">Designation Manager</a></li>
            <li><a href="<?php echo $url;?>index.php?pagename=admin_manager" class="arrow">Employee Manager</a></li>
            <li><a href="<?php echo $url;?>index.php?pagename=view_attendance">Attendance Manager</a></li>
            <li><a href="<?php echo $url;?>index.php?pagename=manage_team">Team Manager</a></li>
          </ul>
        </li>
        <li><a class="arrow"><img src="images/newicon/Vista_icons_08.png" width="14" style="margin-top: -2px;" /> Admin Managment</a>
          <ul>
            <li><a href="<?php echo $url;?>index.php?pagename=manage_product" class="arrow">Product Manager</a></li>
            <li><a href="<?php echo $url;?>index.php?pagename=manage_application" class="arrow">Product Category Manager</a></li>
            <li><a href="<?php echo $url;?>index.php?pagename=customer_manager" class="arrow">Customer Database Manager</a></li>
            <li><a href="<?php echo $url;?>index.php?pagename=manage_enq_source">Enquiry Source Manager</a></li>
            <li><a href="<?php echo $url;?>index.php?pagename=offer_id_generation">Offer ID Generation</a></li>
          </ul>
        </li>
      </ul>
      <?php //include("includes/main_top_menu.php");?></td>
  </tr>
  <tr>
 
  
</table>
</section>
<header style="background-color:rgba(204,204,204,0.3); display:none">
  <section class="upper-nav">
    <div class="row">
     
    </div>
  </section>
  <nav id="main-nav" class="row">
    <div id="logo" class="three columns"> <a aria-label="Home" role="button" href="#" class="main-logo"> <img src="../../images/logo.png" /> </a> </div>
    <div id="site-links" class="columns"> <a id="optimizely-demo-button" href="#" class="button demo lightgray" style="margin-top:9px; font-size:25px">RMA REPAIR</a> <?php time(); ?> </div>
  </nav>
</header>