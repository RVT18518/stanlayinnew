<?php 
	include("../../../includes1/function_lib.php");
	include("session_check.php");
	$admin_id=$_SESSION["AdminLoginID_SET"];
	
	if(!isset($_SESSION["AdminLoginID_SET"]))
		{
		//	$s->pageLocation("admin_login.php");
							$s->pageLocation("http://localhost/stanlay.in/crm/admin_login.php?action=error");
		}
		
		if($_REQUEST['save_repair']) {
			$dataArray["Dignosis"] 				= htmlspecialchars($_REQUEST['Dignosis'],ENT_QUOTES);
			$dataArray["pfaced"] 				= htmlspecialchars($_REQUEST['pfaced'],ENT_QUOTES);
			$dataArray["parts_replaced"] 		= htmlspecialchars($_REQUEST['parts_replaced'],ENT_QUOTES);
			$dataArray["edd"] 					= htmlspecialchars($_REQUEST['edd'],ENT_QUOTES);
			$dataArray["r_repair_date"] 		= htmlspecialchars($_REQUEST['r_repair_date'],ENT_QUOTES);
			
			$result    	=  $s->editRecord('tbl_srf', $dataArray,"S_ID",$_REQUEST['S_ID']); 
		}
		
		if($_REQUEST['save_di']) {
			$dataArray["dis_date"] 			= htmlspecialchars($_REQUEST['dis_date'],ENT_QUOTES);
			$dataArray["courier_name"] 		= htmlspecialchars($_REQUEST['courier_name'],ENT_QUOTES);
			$dataArray["tracking_id"] 		= htmlspecialchars($_REQUEST['tracking_id'],ENT_QUOTES);
			$dataArray["Special_note"] 		= htmlspecialchars($_REQUEST['Special_note'],ENT_QUOTES);
			
			$result    	=  $s->editRecord('tbl_srf', $dataArray,"S_ID",$_REQUEST['S_ID']);
			 
		}

	?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Warranty Process Repair</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="styles.css">

</head>

<body>
<?php 
	
include('header.php'); ?>


<main class="templates">
  <section class="section--white">
    <?php include('left.php'); ?>
    		
            
            <form name="save_dio" action="process-repair.php?id=<?php echo $_REQUEST["id"]; ?>" method="post">
           
    
         <table width="97.3%" cellpadding="0" cellspacing="0" style="margin:auto" class="tbl_border">
       <tr>
        	<td colspan="2" style="padding:10px; font-size:18px; font-weight:bold; background-color:rgba(0,204,102,0.1); text-transform:uppercase">Repair Process:</td>
        </tr>
        	
             <?php 
			
			$i=1;
			$sql_rma = "select * from tbl_srf where S_ID='".$_REQUEST["id"]."'";
			$row_rma = mysqli_query($con,  $sql_rma);
			$rs_rma  = mysqli_fetch_object($row_rma);
			
		?>&nbsp;
         <input type="hidden" name="S_ID" value="<?php echo $rs_rma->S_ID; ?>" />
        <tr>
        	<td width="15%" nowrap="nowrap">RMA#</td>
            <td><?php echo $rs_rma->RMA_NO; ?></td>
        </tr>
        
        <tr>
        	<td>Product Name & Serial</td>
            <td><?php echo ucwords(strtolower($s->tbl_order_product_all($rs_rma->S_No)));  ?> S. No. <?php echo $rs_rma->S_No; ?></td>

        </tr>
        
         <tr>
        	<td>SRF Generate Date</td>
            <td><?php echo $rs_rma->SRF_Place_Date; ?></td>

        </tr>
        
         <tr>
        	<td>Problem Faced</td>
            <td><?php echo $rs_rma->problem_facing; ?></td>

        </tr>
         <tr>
        	<td>Enter Start Repair Date</td>
            <td><input type="date" name="r_repair_date" value="<?php echo $rs_rma->r_repair_date; ?>" placeholder="yyyy-mm-dd" style="width:25%" /></td>

        </tr>
        
       
         <tr>
        	<td>Diagnosis</td>
            <td><textarea maxlength='350' placeholder="Maximum 350 Characters" rows="3" name="Dignosis"><?php echo $rs_rma->Dignosis; ?></textarea>
            
            <br />
            <strong style="color:rgba(255,0,0,1)">Important:</strong> After Diagnosis > Save Repairs!</td>

        </tr>
        
        
        
        <tr>
        	<td>Parts to be replaced/repaired</td>
            <td>
            
           
            Add Parts:  <select name="Product_category" id="Product_category" onchange="this.form.submit()" style="width:20%">
               <option value="">Select Category</option>
               
               <?php
			   $sql_cat_vendor="select * from tbl_application where deleteflag='active' order by application_name";
	$rs_cat_vendor=mysqli_query($con,  $sql_cat_vendor);
	while($row_cat_vendor=mysqli_fetch_object($rs_cat_vendor))
		{	
			   ?>
               <option value="<?php echo $row_cat_vendor->application_id;  ?>" <?php if($row_cat_vendor->application_id==$_REQUEST['Product_category']) { echo "selected"; } ?>><?php echo $row_cat_vendor->application_name;  ?></option>
               
               <?php 
		}
			   
			   ?>
               
            </select>
            
            
             <?php if($_REQUEST['Product_category']) { ?>
            <select name="Product_List" id="Product_List" style="width:40%">
               <option value="">Select Product</option>
               
               <?php
			  echo  $sql_product_vendor="select tbl_products_entry.app_cat_id,tbl_products_entry.pro_id_entry,tbl_products.pro_title from tbl_products_entry,tbl_products where tbl_products_entry.pro_id=tbl_products.pro_id and price_list='pvt' AND tbl_products_entry.app_cat_id='".$_REQUEST['Product_category']."' order by tbl_products.pro_title";
	$rs_product_vendor=mysqli_query($con,  $sql_product_vendor);
	if(mysqli_num_rows($rs_product_vendor)>0)
	{
		while($row_product_vendor=mysqli_fetch_object($rs_product_vendor))
		{	
			   ?>
               <option value="<?php echo $row_product_vendor->pro_id_entry;  ?>" <?php if($row_product_vendor->pro_id_entry==$_REQUEST['Product_List']) { echo "selected"; } ?>><?php echo $row_product_vendor->pro_title;  ?></option>
               
               <?php 
		}
	}
			   
			   ?>
               
            </select>
                <?php 
	}
			   
			   ?>  <input type="submit" value="Add" name="add_but" />
             
            
            
            <?php
				if($_REQUEST['add_but']) {
			
				$sql_rma_check = "select * from tbl_srf_parts where S_ID='".$_REQUEST["id"]."' AND Product_List='".$_REQUEST["Product_List"]."'";
				$row_rma_check = mysqli_query($con,  $sql_rma_check);
				if(mysqli_num_rows($row_rma_check)>0) {
						echo "<strong style='color:#ff0000'>Product Already Added!</strong><strong style='color:#009933'>";
				}
				else {
				$dataArray["Product_List"] 				= htmlspecialchars($_REQUEST['Product_List'],ENT_QUOTES);
				$dataArray["S_ID"] 						= htmlspecialchars($_REQUEST['S_ID'],ENT_QUOTES);
				
					$result    	=  $s->insertRecord('tbl_srf_parts', $dataArray); 
					
					if($result==0) echo "<strong style='color:#009933'>Parts Added!";
				 }
				}
				
			
			?>
           <a href="view_stock.php" title="Click to View Stock!" target="_blank" style="display:block; float:right; text-align:center"><input type="button" class="submit_but" value="View Stock" /> <br />This will help you decide expected date of dispatch!</a>
            <hr style="padding:0px; margin:10px;" />
            <h2>Parts to be Replaced/Repaired!</h2> 
            
            <?php 
				if($_REQUEST['remove_pro']=="yes") {
			
			 $sql_rma_delete = "delete from tbl_srf_parts where ID='".$_REQUEST["pro_id"]."'";
			if(mysqli_query($con,  $sql_rma_delete)) echo "<span style='color:#ff0000'>Record Deleted!</span>";
			
		}
			?>
            
            <ul>
            <?php
			$sql_rma_show = "select * from tbl_srf_parts where S_ID='".$_REQUEST["id"]."' group by Product_List";
			$row_rma_show = mysqli_query($con,  $sql_rma_show);
			while($rs_rma_show  = mysqli_fetch_object($row_rma_show)) {
			?>
            	<li><?php echo $s->product_name($s->product_entry_id($rs_rma_show->Product_List)); ?> 
                <a href="process-repair.php?id=<?php echo $_REQUEST["id"]; ?>&remove_pro=yes&pro_id=<?php echo $rs_rma_show->ID; ?>" style="text-decoration:none!imporant; border:none" title="Click to Remove Parts!" onclick="return confirm('Are you sure!');"><img src="../../images/fancy_closebox.png" width="20"  /></a></li>
                
                <?php } ?>
            </ul>
            
           </strong>
            </td>

        </tr>
        
         <tr>
        	<td>Expected Date of dispatch</td>
            <td><input type="date" name="edd" value="<?php echo $rs_rma->edd; ?>" style="width:25%" placeholder="yyyy-mm-dd" /></td>

        </tr>
        
          <tr>
            <td colspan="2" style="text-align:center"><input type="submit" name="save_repair" value="Save Repair" class="submit_but" />  &nbsp;
            	<a href="offer_repairs.php?pcode=<?php echo $_REQUEST['id']; ?>&edit=yes" target="_blank">
                	<input type="button" name="save_repair" value="Generate Offer" class="submit_but" />
                </a>
            </td>
        </tr>
       
         <tr>
        	<td colspan="2" style="padding:10px; font-size:18px; font-weight:bold; background-color:rgba(0,204,102,0.1); text-transform:uppercase">Dispatch Confirmation</td>
            </tr>
            <tr>
            
          <td>            
Dispatch Date:</td>
            <td>
 <input type="date" name="dis_date" value="<?php echo $rs_rma->dis_date; ?>" style="width:35%" placeholder="yyyy-mm-dd" />

            
            </td>

        </tr>
        
         <tr>
            
          <td>Courier Name:</td>
            <td> <input type="text" name="courier_name" value="<?php echo $rs_rma->courier_name; ?>" style="width:35%" />
</td>

        </tr> 
        
         <tr>
            
          <td>Tracking ID:</td>
            <td> <input type="text" name="tracking_id" value="<?php echo $rs_rma->tracking_id; ?>" style="width:35%" /> 
</td>

        </tr> 
        
         
        
         <tr>
            
          <td>Special Note:</td>
            <td> <textarea name="Special_note" rows="5" style="width:35%"><?php echo $rs_rma->Special_note; ?></textarea></td>

        </tr> 
            
          <tr>
            <td colspan="2" style="text-align:center">
            
            <input type="submit" name="save_di" value="Update Dispatch Information" class="submit_but" />  &nbsp; 
            <input type="submit" name="close_case" value="Close Case & Notify to Customer" class="submit_but" />
            
            </td>

        </tr>
       
        </table>
        
        </form>
      
    </div>
  </section>
   <?php  include('footer.php'); 
   
   if($_REQUEST['close_case']) {
	   
	   
	  
	$dataArray['Confirm_Resolved_Repair'] = 'active';
	$s->editRecord('tbl_srf', $dataArray,"S_ID",$_REQUEST['id']); 
		
		
    $msg = "
   	<p>Dear Customer,</p>
	<p>Your product for repair under RMA #  ".$rs_rma->RMA_NO."<br />
	has been dispatched.<br />
	&nbsp;<br />
	<h1>Dispatch Details:</h1>
	Dipatch Date:  ".$rs_rma->dis_date."<br />
	Courier Name:".$rs_rma->courier_name."<br />
	Tracking ID: ".$rs_rma->tracking_id." <br /><br /></p>
	
	
	<p>Thanks & Regards,<br />
	Team Stanlay<br />
	+91-11-41860000</p>
   ";


	    $to 		= "dsr@stanlay.com";
		$subject 	= "Repair Dispatch Information";
		$headers  	= 'MIME-Version: 1.0' . "\r\n";
		$headers    .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		
		
		// Additional headers
		$headers 	.= 'To: Stanlay SRF <sales@stanlay.com>' . "\r\n";
		$headers 	.= 'From: '.$rs_rma->cname.' <'.$rs_rma->email.'>' . "\r\n";

		
		// Mail it
		mail($to, $subject, $msg, $headers);
		mail($_REQUEST['email'], $subject, $msg, $headers);
   
   }
   
   
   ?>
</main>
    
</body>
</html>