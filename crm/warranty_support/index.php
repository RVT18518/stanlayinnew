<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Warranty Support: Dashboard</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="styles.css">
</head>

<body>
<?php include('header.php'); ?>


<main class="templates">
  <section class="section--white">
    <div class="row">
      <div class="twelve columns">
        <p class="imitation"></p>    
      </div>  
    </div>
    
    <div class="row">
    
    <?php include('left.php'); ?>
    
    
      
      <div class="nine columns">
        <div class="twelve columns">
         
  <h1 class="headline">Warranty Support</h1>
        <p class="sub-title" style="font-weight:bold; font-size:18px">Service Request Form, support requests, You can request service warranty for your product, track service requst status and updated time to time service progress as well.</p>
</div>

 <div class="twelve columns">
        <p class="imitation"></p>    
      </div>  
      
    
        <a href="srf.php" class="button orange getFormButton middle" style="width:30%">Service Request Form</a>  <span style="margin-top:9px; font-size:15px">Click button to generate service request form & RMA Number <span class="icon" style="color:red; font-weight:bold; font-size:18px">*</span></span> 
        <br /><br />
      
      
        
         <a href="track_repairs.php" class="button orange getFormButton middle" style="width:30%">Track Repair Status</a>  <span style="margin-top:9px; font-size:15px">Users RMA Number <span class="icon">#</span> Require.</span> 
      
      
<br /><br />
      <br /><br />
      <br /><br />
      
      
       
       
        </div> 
          <p class="sub-title" style="font-weight:bold; font-size:15px"><span style="color:#ff0000;font-size:19px">*</span> Any Product being shipped back to our works should have RMA # displayed on or included in the returned products packaging.</p>
      </div>   
      
     
    </div>
  </section>
   <?php include('footer.php'); ?>
</main>
    
</body>
</html>