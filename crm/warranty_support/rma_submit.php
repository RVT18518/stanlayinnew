<?php include("../includes1/modulefunction.php"); include("../includes1/function_lib.php");

 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Warranty Support: Thankyou for Service Request</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="styles.css">
</head>

<body>
<?php include('header.php'); ?>
<main class="templates">
  <section class="section--white">
    <div class="row">
      <div class="twelve columns">
        <p class="imitation"></p>
      </div>
    </div>
    <div class="row">
      <?php include('left.php'); ?>
      <div class="nine columns">
        <div class="twelve columns">
          <h1 class="headline">Thank you <?php echo $_REQUEST['name']; ?>, </h1>
          <p class="sub-title" style="font-weight:bold; font-size:18px"> Your Warranty Support form has been Submitted Successfully. <br />
            Our Customer support devision will cantact you soon.
          <p class="sub-title" style="font-weight:bold; font-size:15px"><span style="color:#ff0000;font-size:19px">*</span> Any Product being shipped back to our works should have RMA # displayed on or <br />
            &nbsp;&nbsp;&nbsp;included in the returned products packaging.</p>
          <p class="sub-title" style="font-weight:bold; font-size:15px"><span style="color:#ff0000;font-size:19px">**</span> Please note that products shipped to us on freight to pay basis will not be accepted > <br />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;freight/courier cost from your desk to our desk/works is to your account.
            .</p>
          <?php 
       
       echo $msg1 = '
	   <table width="100%" border="1">
       <tr>
        	<th colspan="2" style="padding:10px; font-size:18px; background-color:rgba(0,204,102,0.1); text-transform:uppercase">Tracking Details</th>
        </tr>
       	<tr>
        	<td>Name: </td>
            <td>'.$_REQUEST['name'].'</td>
        </tr>
        <tr>
        	<td>Your Company Name: </td>
            <td>'.$_REQUEST['cname'].'</td>
        </tr>
        <tr>
        	<td>Your Return Address: </td>
            <td>'.$_REQUEST['address'].'</td>
        </tr><tr>
        	<td>Email ID: </td>
            <td>'.$_REQUEST['email'].'</td>
        </tr>
		<tr>
        	<td>Mobile No.: </td>
            <td>'.$_REQUEST['mobile'].'</td>
        </tr>
		
		<tr>
        	<td>Instrument details with serial no.: </td>
            <td>'.$_REQUEST['instrument_details'].'</td>
        </tr>
        <tr>
        	<td>Problem you are facing: </td>
            <td>'.$_REQUEST['problem_facing'].'</td>
        </tr>
        
         <tr>
        	<td>RMA Number: </td>
            <td>'.$_REQUEST['RMA_NO'].'</td>
        </tr>
         <tr>
        	<td>Stanlay Order ID: </td>
            <td>'.$_REQUEST['order_id'].'</td>
        </tr>';
		?>
          <?php 
       
        $msg = '
	   	<table>
   
	<p align="center"><a aria-label="Home" role="button" href="http://www.stanlay.in/" class="main-logo"> <img src="http://www.stanlay.in/images/logo.png" /> </a> </p>
    <p align="center"> <a id="optimizely-demo-button" href="#" class="button demo lightgray" style="margin-top:9px; font-size:25px">Online Service Request Form</a> </p>
	   
	   
	   
	   <p class="sub-title" style="font-weight:bold; font-size:15px"><span style="color:#ff0000;font-size:19px">*</span> Any Product being shipped back to our works should have RMA # displayed on or <br />  &nbsp;&nbsp;&nbsp;included in the returned products packaging.</p>
	   
	   <p class="sub-title" style="font-weight:bold; font-size:15px"><span style="color:#ff0000;font-size:19px">**</span> Please note that products shipped to us on freight to pay basis will not be accepted > <br />  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;freight/courier cost from your desk to our desk/works is to your account.
         .</p>
	   
	   <table width="100%" border="1">
       <tr>
        	<th colspan="2" style="padding:10px; font-size:18px; background-color:rgba(0,204,102,0.1); text-transform:uppercase">Tracking Details</th>
        </tr>
       	<tr>
        	<td>Name: </td>
            <td>'.$_REQUEST['name'].'</td>
        </tr>
        <tr>
        	<td>Your Company Name: </td>
            <td>'.$_REQUEST['cname'].'</td>
        </tr>
        <tr>
        	<td>Your Return Address: </td>
            <td>'.$_REQUEST['address'].'</td>
        </tr>
		<tr>
        	<td>Email ID: </td>
            <td>'.$_REQUEST['email'].'</td>
        </tr>
		
		<tr>
        	<td>Mobile No.: </td>
            <td>'.$_REQUEST['mobile'].'</td>
        </tr>
		
		<tr>
        	<td>Instrument details with serial no.: </td>
            <td>'.$_REQUEST['instrument_details'].'</td>
        </tr>
		
		
		
        <tr>
        	<td>Problem you are facing: </td>
            <td>'.$_REQUEST['problem_facing'].'</td>
        </tr>
        
         <tr>
        	<td>RMA Number: </td>
            <td>'.$_REQUEST['RMA_NO'].'</td>
        </tr>
         <tr>
        	<td>Stanlay Order ID: </td>
            <td>'.$_REQUEST['order_id'].'</td>
        </tr>
		 <tr><td colspan="2">
		<p><br />
Regards,<br />
'.$_REQUEST['name'].'</p></td>
        </tr>';
		?>
          <tr>
            <td colspan="2" style="text-align:center!important"><input type="button" value="Print" onclick="window.print();" /></td>
          </tr>
          </table>
          </p>
        </div>
        <div class="twelve columns">
          <p class="imitation"></p>
        </div>
      </div>
    </div>
    </div>
  </section>
  <?php include('footer.php'); ?>
</main>

<?php

		/*$dataArray["name"] 				= htmlspecialchars($_REQUEST['name'],ENT_QUOTES);
		$dataArray["cname"] 			= htmlspecialchars($_REQUEST['cname'],ENT_QUOTES);
		$dataArray["address"] 			= htmlspecialchars($_REQUEST['address'],ENT_QUOTES);
		$dataArray["email"] 			= htmlspecialchars($_REQUEST['email'],ENT_QUOTES);
		$dataArray["problem_facing"] 	= htmlspecialchars($_REQUEST['problem_facing'],ENT_QUOTES);
		$dataArray["RMA_NO"] 			= htmlspecialchars($_REQUEST['RMA_NO'],ENT_QUOTES);
		$dataArray["order_id"] 			= htmlspecialchars($_REQUEST['order_id'],ENT_QUOTES);
		$dataArray["Wa_Status"] 		= htmlspecialchars($_REQUEST['Wa_Status'],ENT_QUOTES);

		$result    	=  $s->editRecord('tbl_srf', $dataArray,"RMA_NO",$_REQUEST['RMA_NO']); */
		
		$dataArray["RMA_NO"] 			= $RMA_NO;
 		$dataArray["S_No"] 				= $_REQUEST['s_no'];
		$today=date("Y-m-d");
		$dataArray["name"] 				= htmlspecialchars($_REQUEST['name'],ENT_QUOTES);
		$dataArray["cname"] 			= htmlspecialchars($_REQUEST['cname'],ENT_QUOTES);
		$dataArray["address"] 			= htmlspecialchars($_REQUEST['address'],ENT_QUOTES);
		$dataArray["email"] 			= htmlspecialchars($_REQUEST['email'],ENT_QUOTES);
		
		$dataArray["mobile"] 			= htmlspecialchars($_REQUEST['mobile'],ENT_QUOTES);
		$dataArray["instrument_details"]= htmlspecialchars($_REQUEST['instrument_details'],ENT_QUOTES);
		
		$dataArray["problem_facing"] 	= htmlspecialchars($_REQUEST['problem_facing'],ENT_QUOTES);
		$dataArray["RMA_NO"] 			= htmlspecialchars($_REQUEST['RMA_NO'],ENT_QUOTES);
		$dataArray["order_id"] 			= htmlspecialchars($_REQUEST['order_id'],ENT_QUOTES);
		$dataArray["p_rec_date"] 		= htmlspecialchars($today,ENT_QUOTES);
		$dataArray["Wa_Status"] 		= htmlspecialchars($_REQUEST['Wa_Status'],ENT_QUOTES);
		
		
		
		$result    =  $s->insertRecord('tbl_srf', $dataArray); //exit;
		
		// To send HTML mail, the Content-type header must be set
		$to 		= "Sandeep@stanlay.com";
		$subject 	= "New Warranty Support Request!";
		$headers  	= 'MIME-Version: 1.0' . "\r\n";
		$headers 	.= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		
		
		// Additional headers sales@stanlay.com
		$headers 	.= 'To: Stanlay SRF <sales@stanlay.com>' . "\r\n";
		$headers 	.= 'From: '.$dataArray["cname"].' <'.$dataArray["email"].'>' . "\r\n";
		$msg;
		
		// Mail it
		mail($to, $subject, $msg, $headers);
		mail('Sandeep@stanlay.com', $subject, $msg, $headers);
		mail($_REQUEST['email'], $subject, $msg, $headers);






?>
</body>
</html>