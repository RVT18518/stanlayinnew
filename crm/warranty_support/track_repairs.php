<?php include("../includes1/modulefunction.php"); include("../includes1/function_lib.php");  ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Warranty Support: Track Repairs</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="styles.css">
</head>

<body>
<?php include('header.php'); ?>


<main class="templates">
  <section class="section--white">
    <div class="row">
      <div class="twelve columns">
        <p class="imitation"></p>    
      </div>  
    </div>
    
    <div class="row">
    
    <?php include('left.php'); ?>
    
    
      
      <div class="nine columns">
        <div class="twelve columns">
         
  <h1 class="headline">Track Your Repair Status </h1>
  <center>
       <form name="track_repairs">
       	<input type="text" name="rma_submit" placeholder="Enter Your RMA No" style="width:50%" required="required" /> 
        
        <input class="fsSubmitButton" style="color: rgb(0, 0, 0); background: rgb(180, 86, 146);" type="submit" name="Track_Repairs" value="Track" />
        
       
       </form>
       
        </center>
        
    
        

       
       
       
       </p>
</div>

 <div class="twelve columns">
        <p class="imitation"></p>    
      </div>  
      
    
      
      
      
       
       
        </div> 
         
      </div>   
       <?php if($_REQUEST['Track_Repairs']) { ?>
     <table width="97.3%" cellpadding="0" cellspacing="0" style="margin:auto">
       <tr>
        	<td colspan="8" style="padding:10px; font-size:18px; font-weight:bold; background-color:rgba(0,204,102,0.1); text-transform:uppercase">View Repair Details:</td>
        </tr>
        	
             <?php 
			
			$i=1;
			$sql_rma = "select * from tbl_srf where RMA_NO='".$_REQUEST["rma_submit"]."'";
			$row_rma = mysqli_query($con,  $sql_rma);
			$rs_rma  = mysqli_fetch_object($row_rma);
			
			
			
		?>
        
        <tr>
        	<td width="15%" nowrap="nowrap">RMA#</td>
            <td><?php echo $rs_rma->RMA_NO; ?></td>

        </tr>
        
          <tr>
        	<td>Product Name & Serial</td>
            <td><?php echo ucwords(strtolower($s->tbl_order_product_all($rs_rma->S_No)));  ?> S. No. <?php echo $rs_rma->S_No; ?></td>

        </tr>
        
         <tr>
        	<td>SRF Generate Date</td>
            <td><?php echo $rs_rma->SRF_Place_Date; ?></td>

        </tr>
        
         <tr>
        	<td width="15%" nowrap="nowrap">Product Received</td>
            <td>
            	<strong>At:</strong> <?php echo $rs_rma->p_rec_date; ?> <br />
                <strong>Works Start On:</strong> <?php echo $rs_rma->r_repair_date; ?>
            
            </td>

        </tr>
        
        <tr>
        	<td>Problem Faced</td>
            <td><?php echo $rs_rma->problem_facing; ?></td>
        </tr>
       
         <tr>
        	<td>Diagnosis</td>
            <td><?php echo $rs_rma->Dignosis; ?></td>

        </tr>
        
          <tr>
        	<td>Exptected Date of dispatch</td>
            <td><?php echo $rs_rma->edd; ?></td>

        </tr>
        
         
        
        <tr>
        	<td>Parts replaced/repaired</td>
            <td>
            
           
           
            <ul>
            <?php
			$sql_rma_show = "select * from tbl_srf_parts where S_ID='".$rs_rma->S_ID."' group by Product_List";
			$row_rma_show = mysqli_query($con,  $sql_rma_show);
			while($rs_rma_show  = mysqli_fetch_object($row_rma_show)) {
			?>
            	<li><?php echo $s->product_name($s->product_entry_id($rs_rma_show->Product_List)); ?> </li>
                
                <?php } ?>
            </ul>
            
           </strong>
            </td>

        </tr>
        
       
        
        
        
        
       
         <tr>
        	<td colspan="2" style="padding:10px; font-size:18px; font-weight:bold; background-color:rgba(0,204,102,0.1); text-transform:uppercase">Dispatch Confirmation</td>
            </tr>
            <tr>
            
          <td>            
Dispatch Date:</td>
            <td>
 <?php echo $rs_rma->dis_date; ?>

            
            </td>

        </tr>
        
         <tr>
            
          <td>Courier Name:</td>
            <td> <?php echo $rs_rma->courier_name; ?>
</td>

        </tr> 
        
         <tr>
            
          <td>Tracking ID:</td>
            <td> <?php echo $rs_rma->tracking_id; ?>
</td>

        </tr> 
        
         
        
         <tr>
            
          <td>Special Note:</td>
            <td><?php echo $rs_rma->Special_note; ?></td>

        </tr>  
        
        
          <tr>
            <td colspan="2" style="text-align:center">
            </td>

        </tr>
       <tr>
        	<td colspan="2" style="padding:10px; font-size:18px; font-weight:bold; background-color:rgba(0,204,102,0.1); text-transform:uppercase">No of Days Taken: 
            
     <?php 
	 $your_date = strtotime($rs_rma->r_repair_date);
	 $now 		= strtotime($rs_rma->dis_date);
     $datediff 	= $now - $your_date;
     echo $t_days	= floor($datediff/(60*60*24));
	 
	 ?>
     
     </td>
            </tr>
        </table>
        
         <?php } ?>
    </div>
  </section>
   <?php include('footer.php'); ?>
</main>




    
</body>
</html>