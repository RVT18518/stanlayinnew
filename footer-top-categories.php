<section class="top-cat-hp">
  <div class="container">
    <div class="row">
      <div class="blk-heading">Top Categories</div>
      <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
          <div class="item active">
            <div class="container clearfix">
              <?php 
$sql = "select cate_id, cat_permalink_n, cate_name, cate_image from tbl_category where parent_id = '0' and deleteflag = 'active' and cate_status='active' order by sort_order asc ";
		$rs  = mysqli_query($GLOBALS["___mysqli_ston"],$sql);
$num_count=mysqli_num_rows($rs);
		$i=0;
		while($row = mysqli_fetch_object($rs)) {
			$i++;
?>
              <div class="col-md-4 col-sm-4">
                <div class="cat-img">
                  <?php /*?><img src="<?php echo $css_path_images.$row->cate_image; ?>" width="110" height="141" alt=""><?php */?>
                </div>
                <div class="cat-list">
                  <h6 class="head"><a href="<?php echo $css_path.$row->cat_permalink_n;  ?>/" title="<?php echo $row->cate_name;  ?>" ><?php echo $row->cate_name;  ?></a></h6>
                  <?php
$sql_sub = "select cate_id, cat_permalink_n, cate_name from tbl_category where parent_id = '".$row->cate_id."' and deleteflag = 'active' and cate_status='active' order by sort_order asc";
		$rs_sub  = mysqli_query($GLOBALS["___mysqli_ston"],$sql_sub);
		if(mysqli_num_rows($rs_sub)>0) {
			?>
              <ul>
                   <?php
		while($row_sub = mysqli_fetch_object($rs_sub)) {
?>
                    <li>
                      <h6><a href="<?php echo $css_path.$row->cat_permalink_n;  ?>/<?php echo $row_sub->cat_permalink_n;  ?>/" title="<?php echo $row_sub->cate_name;  ?>"><?php echo $row_sub->cate_name;  ?></a></h6>
                    </li>
                    <?php  }  ?>
                  </ul>
                  <?php
            	  }
				  ?>
                </div>
              </div>
              <?php 
               if($i%3==0)
	  {
if($i!=$num_count)
		  {
	  echo ' </div ></div >';
	  echo ' <div class="item "><div class="container clearfix">';
		  }
	  }
		}
		?>
            </div>
          </div>
        </div>
        <div class="control-panel"> <a class="left carousel-control" href="#myCarousel" data-slide="prev"> &nbsp;</a> <a class="right carousel-control" href="#myCarousel" data-slide="next"> &nbsp;</a> </div>
      </div>
    </div>
  </div>
</section>