<?php $css_path="http://localhost/stanlay.in/";//"https://www.stanlay.in/"; ?>
<span aria-hidden="true" id="myBtn" onclick="topFunction()" class="gray7 dn db-ns o-60 tc f2" style="opacity: 0.8; display: inline;width:45px;height: 45px;"><i class="fa fa-4x fa-angle-up"></i></span>
<footer class="animate-box" data-animate-effect="fadeInUp animated-fast">
  <div class="container">
    <div class="row clearfix">
      <div class="col-md-6 col-sm-12">
        <div class="col-md-4 col-sm-4">
          <div class="blk-heading">Quick Links</div>
          <ul>
            <li><a href="<?php echo $css_path; ?>about-us.php">About Us</a></li>
            <li><a href="<?php echo $css_path; ?>career.php">Career</a></li>
            <li><a href="<?php echo $css_path; ?>blog/">Blog</a></li>
            <li><a href="<?php echo $css_path; ?>news.php">News and Events</a></li>
            <li><a href="https://www.stanlay.com" target="_blank">Stanlay.com</a></li>
          </ul>
        </div>
        <div class="col-md-4 col-sm-4">
          <div class="blk-heading">Video &amp; Training</div>
          <ul>
            <li><a href="<?php echo $css_path; ?>video.php">Videos</a></li>
            <li><a href="<?php echo $css_path; ?>catalogues.php">Catalogues</a></li>
            <li><a href="<?php echo $css_path; ?>support-&-traning.php">Support &amp; Training</a></li>
            <li><a href="<?php echo $css_path; ?>warranty_support/" target="_blank">Warranty Support</a></li>
          </ul>
        </div>
        <div class="col-md-4 col-sm-4">
          <div class="blk-heading">Achievement</div>
          <ul>
            <li><a href="<?php echo $css_path; ?>awards-&-prizes.php">Award &amp; Prizes</a></li>
            <li><a href="<?php echo $css_path; ?>trade-fairs-&-events.php">Trade Fairs &amp; Events</a></li>
          </ul>
        </div>
      </div>
      <div class="col-md-6 col-sm-12">
        <div class="col-md-5 col-sm-4 sm-icons">
          <div class="blk-heading">Follow Us</div>
          <a href="https://www.facebook.com/Stanlay-164626704400/" target="_blank" title="See our social activities" class="sm-fb">Facebook</a> <a href="#;" class="sm-tw">Twitter</a> <a href="#;" class="sm-linkedin">Linked In</a> <a href="https://www.youtube.com/user/Stanlayvideo/" target="_blank" title="See our videos and tutorials" class="sm-ytube">Youtube</a>
          <div class="sales-helpline">
            <div>Sales Helpline</div>
            <strong>Email:</strong> <a href="mailto:sales@stanlay.com">sales@stanlay.com</a><br>
            <strong>Tel:</strong> 011-41860000 </div>
        </div>
        <div class="col-md-7 col-sm-8 signup">
          <div class="blk-heading">Sign up for Email Notification</div>
          <form>
            <input type="text" name="newsletter_email"  placeholder="Enter your email address and press enter">
          </form>
        </div>
      </div>
    </div>
  </div>
  <div class="container-fluid copyright">
    <div class="container">
      <div class="copyright-lt">&copy; Copyright 2019 Stanlay . All rights reserved.</div>
      <div class="copyright-rt"></div>
    </div>
  </div>
</footer>
<?php //mysql_close();?>

<link href="<?php echo BASE_URL;?>css/lightslider.css" rel="stylesheet"  >
<link rel="stylesheet" href="<?php echo BASE_URL;?>css/animate.css"  >
<link href="<?php echo BASE_URL;?>css/prettyPhoto.css" rel="stylesheet"  >
<link href="<?php echo BASE_URL;?>css/responsive.css" rel="stylesheet"  >
<link href="<?php echo BASE_URL;?>css/pgwslider.css" rel="stylesheet"  >
<link rel="stylesheet"  href="<?php echo BASE_URL;?>css/lightslider.css"/>
<link rel="stylesheet"  media="all" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"  >
<!--/header menu--> 
<script src="<?php echo $css_path; ?>js/jquery.js"></script> 
<script async="async" src="<?php echo $css_path; ?>js/bootstrap.min.js"></script> 
<script  defer="defer" src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script> 
<?php /*?><script async="async" src="<?php echo $css_path; ?>js/megamenu.js"></script> <?php */?>
<script><!--megamenu js inline as recommneded by gogole page speed 20-jan2020-->
$(document).ready(function(){"use strict";$('.menu > ul > li:has( > ul)').addClass('menu-dropdown-icon');$('.menu > ul > li > ul:not(:has(ul))').addClass('normal-sub');$(".menu > ul > li").hover(function(e){if($(window).width()>943){$(this).children("ul").fadeIn(150);e.preventDefault();}},function(e){if($(window).width()>943){$(this).children("ul").fadeOut(150);e.preventDefault();}});$(".menu > ul > li").click(function(){if($(window).width()<943){$(this).children("ul").fadeToggle(150);}});$(".menu-mobile").click(function(e){$(".menu > ul").toggleClass('show-on-mobile');e.preventDefault();});});</script> 
<script src="<?php echo $css_path; ?>js/responsive-tabs.js"></script> 
<script src="<?php echo $css_path; ?>js/pgwslider.js"></script> 
<script async="async" src="<?php echo $css_path; ?>js/jquery.expander.js"></script> 
<script src="<?php echo $css_path; ?>js/jquery.prettyPhoto.js"></script> 
<script async="async" src="<?php echo $css_path; ?>js/jquery.isotope.min.js"></script> 
<script src="<?php echo $css_path; ?>js/wow.min.js"></script> 
<script src="<?php echo $css_path; ?>tabs/easy-responsive-tabs.js"></script> 
<script src="<?php echo $css_path; ?>js/slick.js"></script> 
<script src="<?php echo $css_path; ?>js/lightslider.js"></script> 
<script src="<?php echo $css_path; ?>js/jquery.simplyscroll.min.js"></script>
<script src="<?php echo $css_path; ?>js/stellarnav.min.js"></script> 
<script >
	jQuery(document).ready(function($) {
		jQuery('.stellarnav').stellarNav({
			theme: 'light'
		});
		$(".subcat1").hover(
			function(){ // Mouse Over
			$(this).parent().addClass("active-parent");
			},
			function(){ // Mouse Out
			$(this).parent().removeClass("active-parent");
			}
		);

	$(".subcat2").hover(
			function(){ // Mouse Over
			$(this).parent().addClass("active-parent");
			},

		function(){ // Mouse Out
			$(this).parent().removeClass("active-parent");
			}
		);
	$(".dropdown").hover(            
        function() {
            $('.dropdown-menu', this).not('.in .dropdown-menu').stop(true,true).slideDown("400");
            $(this).toggleClass('open');        
        },
        function() {
            $('.dropdown-menu', this).not('.in .dropdown-menu').stop(true,true).slideUp("400");
            $(this).toggleClass('open');       
        }
    );
});
</script> 
<!-- 09June2018 ends--> 

<?php /*?><script async="async" src="<?php echo $css_path; ?>js/main.js"></script>
<script src="https://www.google.com/recaptcha/api.js?onload=myCallBack&render=explicit" async defer></script><?php */?>
    <script>
     
    


document.addEventListener("DOMContentLoaded", function() {
//	alert('test');
  var lazyloadImages;    

  if ("IntersectionObserver" in window) {
    lazyloadImages = document.querySelectorAll(".lazy");
    var imageObserver = new IntersectionObserver(function(entries, observer) {
      entries.forEach(function(entry) {
        if (entry.isIntersecting) {
          var image = entry.target;
          image.src = image.dataset.src;
          image.classList.remove("lazy");
          imageObserver.unobserve(image);
        }
      });
    });

    lazyloadImages.forEach(function(image) {
      imageObserver.observe(image);
    });
  } else {  
    var lazyloadThrottleTimeout;
    lazyloadImages = document.querySelectorAll(".lazy");
    
    function lazyload () {
      if(lazyloadThrottleTimeout) {
        clearTimeout(lazyloadThrottleTimeout);
      }    

      lazyloadThrottleTimeout = setTimeout(function() {
        var scrollTop = window.pageYOffset;
        lazyloadImages.forEach(function(img) {
            if(img.offsetTop < (window.innerHeight + scrollTop)) {
              img.src = img.dataset.src;
              img.classList.remove('lazy');
            }
        });
        if(lazyloadImages.length == 0) { 
          document.removeEventListener("scroll", lazyload);
          window.removeEventListener("resize", lazyload);
          window.removeEventListener("orientationChange", lazyload);
        }
      }, 20);
    }

    document.addEventListener("scroll", lazyload);
    window.addEventListener("resize", lazyload);
    window.addEventListener("orientationChange", lazyload);
  }
})
</script>
<script>
//Get the button
var mybutton = document.getElementById("myBtn");

// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 100 || document.documentElement.scrollTop > 100) {
    mybutton.style.display = "block";
  } else {
    mybutton.style.display = "none";
  }
}

// When the user clicks on the button, scroll to the top of the document
/*function topFunction() {
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
}*/

function topFunction() {
   $("html, body").animate({scrollTop: "0"});
}
</script>
<?php //include_once("analyticstracking.php") ?>
