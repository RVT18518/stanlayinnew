<?php
/*date_default_timezone_set('Asia/Kolkata');
//settings
$cache_ext  = '.html'; //file extension
$cache_time     = 3600;  //Cache file expires afere these seconds (1 hour = 3600 sec)
$cache_folder   = 'cache/category/'; //folder to store Cache files
$ignore_pages   = array('', '');

$dynamic_url    = 'https://'.$_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] . $_SERVER['QUERY_STRING']; // requested dynamic page (full url)
$cache_file     = $cache_folder.md5($dynamic_url).$cache_ext; // construct a cache file
$ignore = (in_array($dynamic_url,$ignore_pages))?true:false; //check if url is in ignore list

if (!$ignore && file_exists($cache_file) && time() - $cache_time < filemtime($cache_file)) { //check Cache exist and it's not expired.
    ob_start('ob_gzhandler'); //Turn on output buffering, "ob_gzhandler" for the compressed page with gzip.
    readfile($cache_file); //read Cache file
    echo '<!-- cached page - '.date('l jS \of F Y h:i:s A', filemtime($cache_file)).', Page : '.$dynamic_url.' -->';
    ob_end_flush(); //Flush and turn off output buffering
    exit(); //no need to proceed further, exit the flow.
}
//Turn on output buffering with gzip compression.
ob_start('ob_gzhandler'); 
######## Your Website Content Starts Below #########

*/
?>
<!DOCTYPE html>
<html lang="en">
  <head>
  <?php 
	include_once("includes/modulefunction.php"); 
//	 include('cat_url_rewrite.php');
?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo @$seo_title;?></title>
<meta name="description" content="<?php echo @$meta_desc;?>">
<meta name="keywords" content="<?php echo @$meta_content;?>">
<meta name="Robots" content="index, follow" />
<meta name="googlebot" content="noodp" />
<meta name="Robots" content="all" />
<meta name="revisit-after" content="1 days" />
<meta name="Author" content="www.stanlay.in, YBP" />
<link rel="canonical" href="https://www.stanlay.in<?php echo $_SERVER["REQUEST_URI"]; ?>" />
<?php include('header-scripts.php'); ?>
 </head>
 <body class="homepage">
<div id="loader"></div>