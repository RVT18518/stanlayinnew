<?php
date_default_timezone_set('Asia/Kolkata');
//settings
$cache_ext  = '.html'; //file extension
$cache_time     = 3600;  //Cache file expires afere these seconds (1 hour = 3600 sec)
$cache_folder   = 'cache/cms/'; //folder to store Cache files
$ignore_pages   = array('', '');

$dynamic_url    = 'https://'.$_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] . $_SERVER['QUERY_STRING']; // requested dynamic page (full url)
$cache_file     = $cache_folder.md5($dynamic_url).$cache_ext; // construct a cache file
$ignore = (in_array($dynamic_url,$ignore_pages))?true:false; //check if url is in ignore list

if (!$ignore && file_exists($cache_file) && time() - $cache_time < filemtime($cache_file)) { //check Cache exist and it's not expired.
    ob_start('ob_gzhandler'); //Turn on output buffering, "ob_gzhandler" for the compressed page with gzip.
    readfile($cache_file); //read Cache file
    echo '<!-- cached page - '.date('l jS \of F Y h:i:s A', filemtime($cache_file)).', Page : '.$dynamic_url.' -->';
    ob_end_flush(); //Flush and turn off output buffering
    exit(); //no need to proceed further, exit the flow.
}
//Turn on output buffering with gzip compression.
ob_start('ob_gzhandler'); 
######## Your Website Content Starts Below #########

?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php 
	include_once("includes/modulefunction.php"); 
$uri=explode("/",$_SERVER['REQUEST_URI']);
$size=sizeof($uri)-1;
$pid=$uri[$size];
$uri=explode(".php",$pid);
$size=sizeof($uri)-2;
$pid=@$uri[$size];
/*if($pid=='contact-us')
{
	echo "aaaaa";
}*/
$page	= $front->get_url_id_cms('tbl_cms_pages',$pid);
$PAGE_PERMALINK	= stripslashes($front->getContentpage($page ,'permalink'));
	//WEBSITE TITLE, META CONTENT & DESC
if(stripslashes($front->getContentpage($page ,'meta_content'))!='')
	{
	$META_CONTENT	= stripslashes($front->getContentpage($page ,'meta_content'));
	}
	else
	{
	$META_CONTENT	= stripslashes($meta_content = $front->fetchGeneral_config('meta_content'));		
	}
	if(stripslashes($front->getContentpage($page ,'meta_desc'))!='')
	{
	$META_DESC		= stripslashes($front->getContentpage($page ,'meta_desc'));
	}
	else
	{
	$META_DESC 		= stripslashes($meta_desc	 = $front->fetchGeneral_config('meta_desc'));	
	}

$TITLE			= stripslashes($front->getContentpage($page ,'page_title'));
	if(strlen(trim($TITLE)) <= 0)
	{
		$TITLE 	= stripslashes($front->fetchGeneral_config("store"));
	}
$pages_name			= stripslashes($front->getContentpage($page ,'pages_name'));	
?>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo $TITLE;?></title>
<meta name="keywords" content="<?php echo $META_CONTENT;?>">
<meta name="description" content="<?php echo $META_DESC;?>">
<meta name="Robots" content="index, follow" />
<meta name="googlebot" content="noodp" />
<meta name="Robots" content="all" />
<meta name="revisit-after" content="7 days" />
<meta name="Author" content="www.stanlay.in, YBP" />
<?php include('header-scripts.php'); ?>
</head>
