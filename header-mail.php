<!DOCTYPE html>
<html lang="en">
<head>
<?php 
include_once("includes/modulefunction.php"); 

		$PAGE_TITLE 		= stripslashes($front->fetchGeneral_config("webtitle"));

		$PAGE_DESC		 	= stripslashes($front->fetchGeneral_config("webtitlecont"));

		$PAGE_META_DESC 	= stripslashes($front->fetchGeneral_config("meta_desc"));

		$PAGE_META_CONTENT 	= stripslashes($front->fetchGeneral_config("meta_content"));

?>
<?php //$css_path="<?php echo $css_path;
$css_path_images="https://www.stanlay.in/";//"http://localhost/stanlay.in/";//"https://www.stanlay.in/";
$css_path="https://www.stanlay.in/";//http://localhost/web/";//<?php echo $css_path; ?>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo $PAGE_TITLE;?></title>
<meta name="description" content="<?php echo $PAGE_META_DESC;?>" />
<meta http-equiv="Expires" content="30" />
<meta name="keywords" content="<?php echo $PAGE_META_CONTENT;?>" />
<meta name="Robots" content="index, follow" />
<meta name="author" content="Asian Contec Ltd">
<meta name="googlebot" content="noodp" />
<meta name="Robots" content="all" />
<meta name="revisit-after" content="1 days" />

<?php include('header-scripts.php'); ?></head>
