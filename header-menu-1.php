<div class="container-fluid" id="topband">
  <div class="cta"><span>Sales Helpline: +91-11-41860000</span> | <span>Email: <a href="mailto:sales@stanlay.in">sales@stanlay.in</a></span></div>
  <div class="container">
    <!-- Navbar -->
    <div class="navbar navbar-default" role="navigation">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
        <a class="" href="<?php echo $css_path; ?>"><img src="<?php echo $css_path; ?>images/logo.png" alt=""></a> </div>
      <div class="navbar-collapse collapse">
        <!-- Left nav -->
        <ul class="nav navbar-nav">
          <li class="visible-xs">
            <form name="frm" method="get" action="<?php echo $css_path; ?>search-result.php">
              <div class="input-group">
                <input type="search" class="form-control" name="search_value" value="" onKeyUp="showResult(this.value)"  placeholder="Search">
                <span class="input-group-btn">
                <button class="btn btn-primary" type="submit"><span class="glyphicon glyphicon-search"></span></button>
                </span> </div>
            </form>
            <div id="livesearch"></div>
          </li>
        </ul>
        </li>
        </ul>
        </li>
        </ul>
        <!-- Right nav -->
        <ul class="nav navbar-nav navbar-right">
          <li><a href="<?php echo $css_path; ?>">Home</a></li>
          <li><a href="<?php echo $css_path; ?>video.php">Video &amp; Training</a></li>
          <li><a href="<?php echo $css_path; ?>">Blog</a></li>
          <li><a href="<?php echo $css_path; ?>about-us.php">About Us</a></li>
          <li><a href="<?php echo $css_path; ?>contact-us.php">Contact</a></li>
          <li class="hidden-xs"><a href="#toggle-search" class="animate"><span class="glyphicon glyphicon-search"></span></a>
            <div class="website-search animate">
              <form name="frm" method="get" action="<?php echo $css_path; ?>search-result.php" role="search">
                <div class="input-group">
                  <input type="search" class="form-control" name="search_value" value="" onKeyUp="showResult(this.value)"  placeholder="Search">
                  <span class="input-group-btn">
                  <button class="btn btn-danger" type="submit"><span class="glyphicon glyphicon-search"></span></button>
                  </span> </div>
              </form>
            </div>
          </li>
        </ul>
      </div>
      <!--/.nav-collapse -->
    </div>
  </div>
  <div id="main-nav" class="stellarnav">
    <ul>       <!--      MAIN CAT starts -->
              <?php 
$sql = "select cate_id, cat_permalink_n, cate_name from tbl_category where parent_id = '0' and deleteflag = 'active' and cate_status='active' order by sort_order asc ";
		$rs  = mysqli_query($GLOBALS["___mysqli_ston"],$sql);
		while($row = mysqli_fetch_object($rs)) {
			?>
      <li><a href="<?php echo $css_path.$row->cat_permalink_n;?>/" data-target="<?php echo $css_path.$row->cat_permalink_n;?>/" data-toggle="dropdown" title="<?php echo $row->cate_name;?>"><?php echo $row->cate_name;?></a>
        <!--  SUB CATegory statrs-->
                    <?php
$sql_sub = "select cate_id, cat_permalink_n, cate_name from tbl_category where parent_id = '".$row->cate_id."' and deleteflag = 'active' and cate_status='active' order by sort_order asc";
		$rs_sub  = mysqli_query($GLOBALS["___mysqli_ston"],$sql_sub);
		if(mysqli_num_rows($rs_sub)>0) {	?>
        <ul class="subcat1"><?php
		while($row_sub = mysqli_fetch_object($rs_sub)) {
			?>
          <li><a href="<?php echo $css_path.$row->cat_permalink_n;?>/<?php echo $row_sub->cat_permalink_n;  ?>/" title="<?php echo $row_sub->cate_name;?>"><?php echo $row_sub->cate_name;?></a>
            <!--      Products-->
            <ul class="subcat2">
         <!--      Products li starts -->
                      <?php
            $sql_sub_pro = "select tbl_products_1.pro_title, tbl_products_1.pro_permalink_n from tbl_products_1 
			left join tbl_products_categories on tbl_products_categories.pro_id = tbl_products_1.pro_id
			where tbl_products_categories.cate_id ='".$row_sub->cate_id."' AND 
			tbl_products_1.deleteflag = 'active' and tbl_products_1.status='active' order by tbl_products_1.sort_order asc";
		$rs_sub_pro  = mysqli_query($GLOBALS["___mysqli_ston"],$sql_sub_pro);
		while($row_sub_pro = mysqli_fetch_object($rs_sub_pro)) {
			?>
              <li><a href="<?php echo $css_path.$row->cat_permalink_n;?>/<?php echo $row_sub->cat_permalink_n;  ?>/<?php echo $row_sub_pro->pro_permalink_n;?>/" title="<?php echo ucwords($row_sub_pro->pro_title);?>"><?php echo ucwords($row_sub_pro->pro_title);?></a></li>
     <?php  }  ?>              
              <!--      Products li ends -->
            </ul>
          </li>
<?php }?>          
          <!--  SUB CATegory ends-->
        </ul>
         <?php }?>
        <!--sub cat UL if close-->
      </li>
  <?php  }?>     
    </ul>
  </div>
</div>
<!--/header menu-->