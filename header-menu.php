<div class="container-fluid" id="topband">
  <div class="cta"><span>Sales Helpline: +91-11-41860000</span> | <span>Email: <a href="mailto:sales@stanlay.com">sales@stanlay.com</a></span></div>
  <div class="container"> 
    <!-- Navbar -->
    <div class="navbar navbar-default" role="navigation">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
        <a class="" href="<?php echo BASE_URL;?>" onclick="ga('send', 'event', 'Logo', 'Click', 'Header Logo');"><img src="<?php echo BASE_URL;?>images/logow.png" alt=""></a> </div>
      <div class="navbar-collapse collapse"> 
        <!-- Left nav -->
        <ul class="nav navbar-nav">
          <li class="visible-xs">
            <form name="frm" method="post" action="<?php echo BASE_URL;?>search-result.php">
              <div class="input-group">
                <input type="search" class="form-control" name="search_value" value="" onKeyUp="showResult(this.value)"  placeholder="Search">
                <span class="input-group-btn">
                <button class="btn btn-primary" type="submit"><span class="glyphicon glyphicon-search"></span></button>
                </span> </div>
            </form>
            <div id="livesearch"></div>
          </li>
        </ul>
        <!-- Right nav -->
        <ul class="nav navbar-nav navbar-right">
          <?php /*?><li class="dropdown mega-dropdown products-link"> <a href="#" class="dropdown-toggle hidden-xs hidden-sm" data-toggle="dropdown" >Products <span class="caret"></span></a>
            <ul class="dropdown-menu mega-dropdown-menu">
              <?php 
$sql_web_cat = "select web_cate_id, web_cat_permalink_n, web_cate_name from tbl_web_category where parent_id = '0' and deleteflag = 'active' and web_cate_status='active' order by sort_order asc ";
		$rs_web_cat  = mysqli_query($GLOBALS["___mysqli_ston"],$sql_web_cat);
		while($row_web_cat = mysqli_fetch_object($rs_web_cat)) {
		?>
              <li class="col-sm-3">
                <ul>
                  <li class="dropdown-header"><?php echo $row_web_cat->web_cate_name;?></li>
                  <!--  SUB CATegory statrs-->
                  <?php
			$sql_sub = "select cate_id, match_cate_id_g2 from tbl_map_sub_cat_new_cate where cate_id = '$row_web_cat->web_cate_id' and deleteflag = 'active'  order by match_id_g2 asc";
		$rs_sub  = mysqli_query($GLOBALS["___mysqli_ston"],$sql_sub);
		if(mysqli_num_rows($rs_sub)>0) {
		while($row_sub = mysqli_fetch_object($rs_sub)) {
			?>
                  <li><a href='<?php echo $css_path.$front->Cat_URL1($row_sub->match_cate_id_g2);?>' title='<?php echo $front->category_name($row_sub->match_cate_id_g2);?>' ><?php echo $front->category_name($row_sub->match_cate_id_g2);?></a></li>
                  <?php }} ?>
                </ul>
              </li>
              <?php }?>
            </ul>
          </li><?php */?>
          <li><a href="<?php echo BASE_URL;?>video.php">Video &amp; Training</a></li>
          <li><a href="<?php echo BASE_URL;?>blog/">Experience</a></li>
          <li><a href="<?php echo BASE_URL;?>about-us.php">About Us</a></li>
          <li><a href="<?php echo BASE_URL;?>contact-us.php">Contact</a></li>
          <li class="hidden-xs"><a href="#toggle-search" class="animate"><span class="glyphicon glyphicon-search"></span></a>
            <div class="website-search animate">
              <form name="frm" method="post" action="<?php echo BASE_URL;?>search-result.php" role="search">
                <div class="input-group">
                  <input type="search" class="form-control" name="search_value" value="" onKeyUp="showResult(this.value)"  placeholder="Search">
                  <span class="input-group-btn">
                  <button class="btn btn-danger" type="submit"><span class="glyphicon glyphicon-search"></span></button>
                  </span> </div>
              </form>
            </div>
          </li>
        </ul>
      </div>
      <!--/.nav-collapse --> 
    </div>
  </div>
  <div id="main-nav" class="stellarnav">
    <ul>       <!--      MAIN CAT starts -->
              <?php 
$sql = "select cate_id, cat_permalink_n, cate_name from tbl_category where parent_id = '0' and deleteflag = 'active' and cate_status='active' order by sort_order asc limit 0,12";
		$rs  = mysqli_query($GLOBALS["___mysqli_ston"],$sql);
$i=0;
		while($row = mysqli_fetch_object($rs)) {
	 $i++;
	
	if($i>8)
	{
		$drop_class=' class="drop-left"';
	}
			?>
      <li <?php echo @$drop_class;?>><a href="<?php echo $css_path.$row->cat_permalink_n;?>/" data-target="<?php echo $css_path.$row->cat_permalink_n;?>/" data-toggle="dropdown" title="<?php echo $row->cate_name;?>"><?php echo $row->cate_name;?></a>
        <!--  SUB CATegory statrs-->
                    <?php
$sql_sub = "select cate_id, cat_permalink_n, cate_name from tbl_category where parent_id = '".$row->cate_id."' and deleteflag = 'active' and cate_status='active' order by sort_order asc";
		$rs_sub  = mysqli_query($GLOBALS["___mysqli_ston"],$sql_sub);
		if(mysqli_num_rows($rs_sub)>0) {	?>
        <ul class="subcat1"><?php
		while($row_sub = mysqli_fetch_object($rs_sub)) {
			?>
          <li><a href="<?php echo $css_path.$row->cat_permalink_n;?>/<?php echo $row_sub->cat_permalink_n;  ?>/" title="<?php echo $row_sub->cate_name;?>"><?php echo $row_sub->cate_name;?></a>
            <!--      Products-->
            <ul class="subcat2">
         <!--      Products li starts -->
                      <?php
            $sql_sub_pro = "select tbl_products_1.pro_title, tbl_products_1.pro_permalink_n from tbl_products_1 
			left join tbl_products_categories on tbl_products_categories.pro_id = tbl_products_1.pro_id
			where tbl_products_categories.cate_id ='".$row_sub->cate_id."' AND 
			tbl_products_1.deleteflag = 'active' and tbl_products_1.status='active' order by tbl_products_1.sort_order asc";
		$rs_sub_pro  = mysqli_query($GLOBALS["___mysqli_ston"],$sql_sub_pro);
		while($row_sub_pro = mysqli_fetch_object($rs_sub_pro)) {
			?>
              <li><a href="<?php echo $css_path.$row->cat_permalink_n;?>/<?php echo $row_sub->cat_permalink_n;  ?>/<?php echo $row_sub_pro->pro_permalink_n;?>/" title="<?php echo ucwords($row_sub_pro->pro_title);?>"><?php echo ucwords($row_sub_pro->pro_title);?></a></li>
     <?php  }  ?>              
              <!--      Products li ends -->
            </ul>
          </li>
<?php }?>          
          <!--  SUB CATegory ends-->
        </ul>
         <?php }?>

        <!--sub cat UL if close-->
      </li>
  <?php  }?>     
    <li class="more-menu" style="width:2.05%; border:none"><a href="#;" data-toggle="dropdown" id="show" title="More" >More</a></li>          
                      <?php 
$sql = "select cate_id, cat_permalink_n, cate_name from tbl_category where parent_id = '0' and deleteflag = 'active' and cate_status='active' order by sort_order asc limit 12,12";
		$rs  = mysqli_query($GLOBALS["___mysqli_ston"],$sql);
$i=0;
		while($row = mysqli_fetch_object($rs)) {
	 $i++;
	
	if($i>8)
	{
		$drop_class1=' class="show-menu"';
	}
			?>
      <li <?php //echo $drop_class1;?> class="show-menu"><a href="<?php echo $css_path.$row->cat_permalink_n;?>/" data-target="<?php echo $css_path.$row->cat_permalink_n;?>/" data-toggle="dropdown" title="<?php echo $row->cate_name;?>"><?php echo $row->cate_name;?></a>
        <!--  SUB CATegory statrs-->
                    <?php
$sql_sub = "select cate_id, cat_permalink_n, cate_name from tbl_category where parent_id = '".$row->cate_id."' and deleteflag = 'active' and cate_status='active' order by sort_order asc ";
		$rs_sub  = mysqli_query($GLOBALS["___mysqli_ston"],$sql_sub);
		if(mysqli_num_rows($rs_sub)>0) {	?>
        <ul class="subcat1"><?php
		while($row_sub = mysqli_fetch_object($rs_sub)) {
			?>
          <li><a href="<?php echo $css_path.$row->cat_permalink_n;?>/<?php echo $row_sub->cat_permalink_n;  ?>/" title="<?php echo $row_sub->cate_name;?>"><?php echo $row_sub->cate_name;?></a>
            <!--      Products-->
            <ul class="subcat2">
         <!--      Products li starts -->
                      <?php
            $sql_sub_pro = "select tbl_products_1.pro_title, tbl_products_1.pro_permalink_n from tbl_products_1 
			left join tbl_products_categories on tbl_products_categories.pro_id = tbl_products_1.pro_id
			where tbl_products_categories.cate_id ='".$row_sub->cate_id."' AND 
			tbl_products_1.deleteflag = 'active' and tbl_products_1.status='active' order by tbl_products_1.sort_order asc";
		$rs_sub_pro  = mysqli_query($GLOBALS["___mysqli_ston"],$sql_sub_pro);
		while($row_sub_pro = mysqli_fetch_object($rs_sub_pro)) {
			?>
              <li><a href="<?php echo $css_path.$row->cat_permalink_n;?>/<?php echo $row_sub->cat_permalink_n;  ?>/<?php echo $row_sub_pro->pro_permalink_n;?>/" title="<?php echo ucwords($row_sub_pro->pro_title);?>"><?php echo ucwords($row_sub_pro->pro_title);?></a></li>
     <?php  }  ?>              
              <!--      Products li ends -->
            </ul>
          </li>
<?php }?>          
          <!--  SUB CATegory ends-->
        </ul>
         <?php }?>

        <!--sub cat UL if close-->
      </li>
  <?php  }?>
<!--            <li class="more-menu" style="width:2.55%; border:none"><a href="#;" data-toggle="dropdown" id="show" title="More" >&nbsp;</a></li>-->
    </ul>
  </div>
</div>
<!--/header menu-->