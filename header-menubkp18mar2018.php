<body class="homepage">
<div class="loader"></div>
<div class="container-fluid" id="topband">
<div class="cta"><span>Sales Helpline: +91-9999911111</span> | <span>Email: <a href="mailto:sales@stanlay.in">sales@stanlay.in</a></span></div>
<div class="clearfix container">
<div class="row">
<div class="stanlay-logo"> <a class="navbar-brand" href="<?php echo $css_path; ?>"><img src="<?php echo $css_path; ?>images/logo.png" alt=""></a> </div>
<div class="navigation">
<a href="#" class="nav-button">Menu</a>
<nav class="nav">
<ul>
<li class="visible-xs">
  <form action="" method="GET" role="search">
    <div class="input-group">
      <input type="text" class="form-control" name="q" placeholder="Search">
      <span class="input-group-btn">
      <button class="btn btn-primary" type="submit"><span class="glyphicon glyphicon-search"></span></button>
      </span> </div>
  </form>
</li>
<li><a href="#;">Home</a></li>
<li class="nav-submenu level01"><a href="#">Categories</a>
  <ul class="level02">
    <!--      MAIN CAT starts -->
    <?php 
			$sql = "select cate_id, cat_permalink_n, cate_name from tbl_category where parent_id = '0' and deleteflag = 'active' and cate_status='active' order by sort_order asc limit 0,11";
		$rs  = mysqli_query($GLOBALS["___mysqli_ston"],$sql);
		while($row = mysqli_fetch_object($rs)) {
			?>
    <li class="nav-submenu"><a href="<?php echo $css_path.$row->cat_permalink_n;  ?>/" data-toggle="dropdown" title="<?php echo $row->cate_name;  ?>"><?php echo $row->cate_name;  ?></a> 
      <!--  SUB CATegory statrs-->
      <?php
			
			$sql_sub = "select cate_id, cat_permalink_n, cate_name from tbl_category where parent_id = '".$row->cate_id."' and deleteflag = 'active' and cate_status='active' order by sort_order asc";
			
		$rs_sub  = mysqli_query($GLOBALS["___mysqli_ston"],$sql_sub);
		if(mysqli_num_rows($rs_sub)>0) {
			?>
      <ul style="display: none;" class="level03">
        <?php
		while($row_sub = mysqli_fetch_object($rs_sub)) {
			?>
        <li class="nav-submenu nav-left"><a href="<?php echo $css_path.$row->cat_permalink_n;  ?>/<?php echo $row_sub->cat_permalink_n;  ?>/" title="<?php echo $row_sub->cate_name;  ?>" ><?php echo $row_sub->cate_name;  ?></a> 
          <!--      Products-->
          <ul style="display: none;" class="level04" >
            <!--      Products li starts -->
            <?php
            $sql_sub_pro = "select tbl_products_1.pro_title, tbl_products_1.pro_permalink_n, tbl_products_1.pro_id from tbl_products_1 
			left join tbl_products_categories on tbl_products_categories.pro_id = tbl_products_1.pro_id
			where tbl_products_categories.cate_id ='".$row_sub->cate_id."' AND 
			tbl_products_1.deleteflag = 'active' and tbl_products_1.status='active' order by tbl_products_1.sort_order asc";
		$rs_sub_pro  = mysqli_query($GLOBALS["___mysqli_ston"],$sql_sub_pro);
        
       
		while($row_sub_pro = mysqli_fetch_object($rs_sub_pro)) {
			?>
            <li><a href="<?php echo $css_path.$row->cat_permalink_n;  ?>/<?php echo $row_sub->cat_permalink_n;  ?>/<?php echo $row_sub_pro->pro_permalink_n; ?>/" title="<?php echo ucwords($row_sub_pro->pro_title);  ?>"> <?php echo ucwords($row_sub_pro->pro_title);  ?> </a> </li>
            <?php  }  ?>
            <!--      Products li ends -->
          </ul>
        </li>
        <?php
            	  }
				  ?>
        <!--  SUB CATegory ends-->
        
      </ul>
      <?php
            	  }
				  ?>
      <!--sub cat UL if close--> 
    </li>
    <?php
            	  }
				  ?>
    <!--      MAIN CAT ends -->
  </ul>
</li>
<li><a href="<?php echo $css_path;?>">Video &amp; Training</a></li>
<li><a href="<?php echo $css_path;?>">Blog</a></li>
<li><a href="<?php echo $css_path;?>about-us.php">About Us</a></li>
<li><a href="<?php echo $css_path;?>contact-us.php">Contact</a></li>
<li class="hidden-xs"><a href="#toggle-search" class="animate"><span class="glyphicon glyphicon-search"></span></a>
  <div class="website-search animate">
    <form action="" method="GET" role="search">
      <div class="input-group">
        <input type="text" class="form-control" name="q" placeholder="Search for keyword and hit enter">
        <span class="input-group-btn">
        <button class="btn btn-danger" type="reset"><span class="glyphicon glyphicon-remove"></span></button>
        </span> </div>
    </form>
    </nav>
    <a href="#" class="nav-close">Close Menu</a> </div>
</div>
</div>
</div>
