<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo $PAGE_TITLE;?></title>
<meta name="description" content="<?php echo $PAGE_META_DESC;?>" />
<meta name="keywords" content="<?php echo $PAGE_META_CONTENT;?>" />
<meta name="Robots" content="index, follow" />
<meta name="author" content="Asian Contec Ltd">
<meta name="googlebot" content="noodp" />
<meta name="Robots" content="all" />
<meta name="revisit-after" content="1 days" />
<meta name="theme-color" content="#535353" />