<meta name="theme-color" content="#535353" />
<!-- Bootstrap -->
<link href="<?php echo BASE_URL;?>css/bootstrap.min.css" rel="stylesheet"  >
<?php /*?><link rel="stylesheet" href="<?php echo BASE_URL;?>css/font-awesome.min.css"><?php */?>
<?php /*?><link href="<?php echo BASE_URL;?>css/animate.min.css" rel="stylesheet"><?php */?>
<!-- Animate.css -->
<link rel="stylesheet" href="<?php echo BASE_URL;?>css/main.css"    >
<!--<link rel="stylesheet" href="<?php //echo BASE_URL;?>css/menu.css"  >-->
<!--Simply Scroll-->
<link rel="stylesheet" href="<?php echo BASE_URL;?>css/jquery.simplyscroll.css" media="all" type="text/css"  >
<!--9June2018 Starts-->
<link rel="stylesheet" type="text/css" media="all" href="<?php echo BASE_URL;?>css/stellarnav.min.css"  >
<!--9June2018 Ends-->
<link rel="stylesheet" href="<?php echo BASE_URL;?>ddmenu3/menu-support.css"  >
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-83887917-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'UA-83887917-1');
</script>
<style>
#myBtn {
 	display: none; /* Hidden by default */
	position: fixed;
    bottom: 20px;
    right: 35px;
    z-index: 99;
    border: none;
    outline: none;
    background-color: #ed3338;
    color: white;
    cursor: pointer;
    padding: 0px ​10px 0px 10px!important;
    border-radius: 10px;
    font-size: 1.75rem!important;
    padding: 10px;
    font-weight: bolder;
    vertical-align: middle;
    text-align: center;
}
.fa-4x {font-size: 1em!important;}
#myBtn:hover { background-color: #000;}
</style>