<?php

	$sql_sub_cat_pro	 = "select tbl_products_1.pro_title, tbl_products_1.pro_short_desc, tbl_products_1.pro_id 

	from tbl_products_1

	INNER JOIN tbl_mapnew_pro

	ON tbl_products_1.pro_id=tbl_mapnew_pro.match_pro_id_g2

	where tbl_products_1.cate_id!='0'

	AND tbl_products_1.deleteflag = 'active'

	AND tbl_products_1.status='active' order by tbl_mapnew_pro.match_id_g2  desc limit 0,3";			

	$rs_sub_cat_pro  	 = mysqli_query($GLOBALS["___mysqli_ston"],$sql_sub_cat_pro);

	$rs_sub_cat_pro_num  	 = mysqli_num_rows($rs_sub_cat_pro);

	if($rs_sub_cat_pro_num>0)

	{?>



<section class="newly-launch  animate-box" data-animate-effect="fadeInDown animated-fast" style="background-color:#fff">

  <div class="container">

    <h3><?php echo $heading_section4 		= stripslashes($front->section_heading("heading_section4"));?><!--Our New Product Range--></h3>

    <!-- tabs -->

    <div class="tabbable tabs-bottom">

      <div class="tab-content responsive">

        <?php

			$i=0;

			while($rs_cat_pro_num_sub  = mysqli_fetch_object($rs_sub_cat_pro)) {

			$i++;

		  ?>

        <div class="clearfix tab-pane <?php if($i==1) { echo "active";}?>" id="prd<?php echo $i;?>">

          <div class="col-md-4"><img src="<?php echo $css_path_images; ?>uploads/pro_image/large/<?php echo $front->get_PROImages($rs_cat_pro_num_sub->pro_id); ?>" width="317" height="406" alt=""></div>

          <div class="col-md-8">

            <div class="head"><?php echo $rs_cat_pro_num_sub->pro_title; ?></div>

            <p><?php echo substr($rs_cat_pro_num_sub->pro_short_desc,0,100); ?> <!--The Portable Skid Resistance and Friction Tester is a direct reading instrument that provides the measure of friction between a skidding tyre and and wet road/ runway surface. It also determines the friction of asphalt and flooring materials.--></p>

            <p><a href="<?php echo $front->Pro_URL($rs_cat_pro_num_sub->pro_id); ?>" class="go-arrow-btn">Go Arrow</a></p>

            <p>&nbsp;</p>

          </div>

        </div>

        <?php }?>

      </div>

            <!-- tab content -->

      <ul class="nav nav-tabs responsive-tabs">

        <?php

			$sql_sub_cat_pro	 = "

			select tbl_products_1.pro_title, tbl_products_1.pro_short_desc, tbl_products_1.pro_id

			from tbl_products_1 

			INNER JOIN tbl_mapnew_pro

			ON tbl_products_1.pro_id=tbl_mapnew_pro.match_pro_id_g2

			where tbl_products_1.cate_id!='0' AND tbl_products_1.deleteflag = 'active' AND tbl_products_1.status='active' order by tbl_mapnew_pro.match_id_g2  desc limit 0,3";

			$rs_sub_cat_pro  	 = mysqli_query($GLOBALS["___mysqli_ston"],$sql_sub_cat_pro);

			$j=0;

			while($rs_cat_pro_num_sub  = mysqli_fetch_object($rs_sub_cat_pro)) {

			$j++;

			?>

        <li <?php if($j==1) { echo 'class="active"';}?>><a href="#prd<?php echo $j;?>" data-toggle="tab"><?php echo $rs_cat_pro_num_sub->pro_title; ?></a></li>

        <?php }?>

      </ul>

    </div>

    

    <!-- /tabs --> 

    

  </div>

</section>

<?php }?>

