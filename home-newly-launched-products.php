<?php
include_once("includes/modulefunction.php"); 
/*echo	$sql_sub_cat_pro	 = "select tbl_products_1.pro_title, tbl_products_1.pro_short_desc, tbl_products_1.pro_id 
	from tbl_products_1
	INNER JOIN tbl_mapnew_pro
	ON tbl_products_1.pro_id=tbl_mapnew_pro.match_pro_id_g2
	where tbl_products_1.cate_id!='0'
	AND tbl_products_1.deleteflag = 'active'
	AND tbl_products_1.status='active' order by tbl_mapnew_pro.match_id_g2  desc limit 0,3";			*/
	
	$sql_sub_cat_pro	 = "select pro_title, pro_short_desc, pro_id 
	from tbl_products_1
	
	where cate_id!='0'
	AND deleteflag = 'active'
	AND status='active' order by pro_id  desc limit 0,3";				
	$rs_sub_cat_pro  	 = mysqli_query($GLOBALS["___mysqli_ston"],$sql_sub_cat_pro);
	$rs_sub_cat_pro_num  	 = mysqli_num_rows($rs_sub_cat_pro);
	if($rs_sub_cat_pro_num>0)
	{?>
<section class="newly-launch  animate-box" data-animate-effect="fadeInDown animated-fast" style="background-color:#fff">
  <div class="container">
    <h3><?php echo $heading_section4 		= stripslashes($front->section_heading("heading_section4"));?><!--Our New Product Range--></h3>
    <!-- tabs -->
    <div class="tabbable tabs-bottom">
      <div class="tab-content responsive">
        <?php
		$i=0;
			while($rs_cat_pro_num_sub  = mysqli_fetch_object($rs_sub_cat_pro)) {
			$i++;
		  ?>
        <div class="clearfix tab-pane <?php if($i==1) { echo "active";}?>" id="prd<?php echo $i;?>">
          <div class="col-md-4">
         <a href="<?php echo $front->Pro_URL($rs_cat_pro_num_sub->pro_id); ?>" > <img class="lazy" src="<?php echo $css_path_images;?>/images/blank.png" data-src="<?php echo $css_path_images; ?>uploads/pro_image/large/<?php echo $front->get_PROImages($rs_cat_pro_num_sub->pro_id); ?>" width="317" height="406" alt="image" /></a></div>
          <div class="col-md-8">
            <div class="head"><a href="<?php echo $front->Pro_URL($rs_cat_pro_num_sub->pro_id); ?>" ><?php echo $rs_cat_pro_num_sub->pro_title; ?></a></div>
            <p class="twolinetext"><?php echo strip_tags($rs_cat_pro_num_sub->pro_short_desc); ?></p>
            <p><a href="<?php echo $front->Pro_URL($rs_cat_pro_num_sub->pro_id); ?>" class="go-arrow-btn">Go Arrow</a></p>
            <p>&nbsp;</p>
          </div>
        </div>
        <?php }?>
      </div>
      <!-- tab content -->
      <ul class="nav nav-tabs responsive-tabs">
        <?php
			$sql_sub_cat_pro	 = "
select pro_title, pro_short_desc, pro_id 
	from tbl_products_1
	
	where cate_id!='0'
	AND deleteflag = 'active'
	AND status='active' order by pro_id  desc limit 0,3";
			$rs_sub_cat_pro  	 = mysqli_query($GLOBALS["___mysqli_ston"],$sql_sub_cat_pro);
			$j=0;
			while($rs_cat_pro_num_sub  = mysqli_fetch_object($rs_sub_cat_pro)) {
			$j++;
			?>
        <li <?php if($j==1) { echo 'class="active"';}?>><a href="#prd<?php echo $j;?>" data-toggle="tab"><?php echo $rs_cat_pro_num_sub->pro_title; ?></a></li>
        <?php }?>
      </ul>
    </div>
    <!-- /tabs --> 
  </div>
</section>
<?php }?>