<section class="about-hp animate-box slideInRight" >
  <div class="container">
   <div class="heading"><?php echo $heading_section2 = stripslashes($front->section_heading("heading_section2"));?><!--Asian Contec Ltd--></div>
    <?php  //$PAGE_DESC		 	= stripslashes($front->fetchGeneral_config("webtitlecont"));?>
    <p style=" font-weight:500" > Asian Contec Ltd under Its brand name Stanlay is an engineering distribution company for testing and project execution catering to the infrastructure segment including telecom, water, sewer & power utility segments, road construction, railways, building construction, heavy engineering and engineering institutional segments with offices in New Delhi, Lucknow, Mumbai, Vadodara, Bangalore, Hyderabad, Bhubaneshwar and Guwahati.</p>
  </div>
</section>
<section class="products-hp"> <!-- Modal -->
  <?php include('sidebar-contact.php');  ?>
  <div class="container">
    <div class="head"><!--Wide Variety of Product Category--> <?php echo $heading_section1 		= stripslashes($front->section_heading("heading_section1"));?></div>
    <div class="product-listhp" >
      <?php
	 $sql_wvpc = "select featured_image,short_desc,banner_image,cate_id, cat_permalink, cate_name from tbl_category where parent_id = '0' and deleteflag = 'active' and cate_status='active' order by sort_order asc ";
		$rs_wvpc  = mysqli_query($GLOBALS["___mysqli_ston"],$sql_wvpc);
		$g=0;
		while($row_wvpc = mysqli_fetch_object($rs_wvpc)) {
		$g++;
		if($row_wvpc->featured_image==NULL)
			{
			$cat_featured_image=$row_wvpc->banner_image;
			}
		else
		{
			$cat_featured_image=$row_wvpc->featured_image;
}
?>
      <div class="col-md-6 col-sm-6 col-xs-12 animate-box">
        <div class="moreBox"><a href="<?php echo $css_path.$row_wvpc->cat_permalink;  ?>/" ><img class="lazy" src="<?php echo $css_path_images;?>/images/blank.png" data-src="<?php echo $css_path_images.$cat_featured_image;?>" alt="<?php echo $row_wvpc->cate_name;  ?>"  style=" width:540px"   /></a>
          <div class="prd-details-hp" style="min-height:160px!important;">
            <h2><a href="<?php echo $css_path.$row_wvpc->cat_permalink;  ?>/" ><?php echo $row_wvpc->cate_name;  ?></a></h2>
            <p class="twolinetext"><?php echo strip_tags($row_wvpc->short_desc);  ?> </p>
            <a href="<?php echo $css_path.$row_wvpc->cat_permalink;  ?>/" class="read_more_link" >Read more</a> </div>
        </div>
      </div>
      <?php 
     if($g%2==0) {
  echo'<div class="clear"></div>';
 }
}?>
    </div>
  </div>
</section>
