<!DOCTYPE html>
<html lang="en">
  <head>
  <?php 
	include_once("includes/modulefunction.php"); 
	 include('cat_url_rewrite.php');
?>
  <?php $css_path="https://stanlay.in/web/"; ?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo $seo_title;?></title>
<meta name="description" content="<?php echo $meta_desc;?>">
<meta name="keywords" content="<?php echo $meta_content;?>">
<meta name="Robots" content="index, follow" />
<meta name="googlebot" content="noodp" />
<meta name="Robots" content="all" />
<meta name="revisit-after" content="1 days" />
<meta name="Author" content="www.stanlay.in, YBP" />
<link rel="canonical" href="https://www.stanlay.in<?php echo $_SERVER["REQUEST_URI"]; ?>" />

    <!-- Bootstrap -->
    <link href="<?php echo $css_path; ?>css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo $css_path; ?>css/font-awesome.min.css">
	<link href="<?php echo $css_path; ?>css/animate.min.css" rel="stylesheet">
    <link href="<?php echo $css_path; ?>css/prettyPhoto.css" rel="stylesheet">      
	<link rel="stylesheet" href="<?php echo $css_path; ?>css/reset.css">
	<link href="<?php echo $css_path; ?>css/main.css" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo $css_path; ?>css/menu.css">
	<link href="<?php echo $css_path; ?>css/responsive.css" rel="stylesheet">
    <link href="<?php echo $css_path; ?>css/pgwslider.css" rel="stylesheet">
	 <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    
  </head>