<!DOCTYPE html>
<html lang="en">
  <head>
  <?php 
	include_once("includes/modulefunction.php"); 

$uri=explode("/",$_SERVER['REQUEST_URI']);
$size=sizeof($uri)-1;
$pid=$uri[$size];

$uri=explode(".php",$pid);
$size=sizeof($uri)-2;
$pid=$uri[$size];
	
	$page	= $front->get_url_id_cms('tbl_cms_pages',$pid);
	
 	$PAGE_PERMALINK	= stripslashes($front->getContentpage($page ,'permalink'));
	//WEBSITE TITLE, META CONTENT & DESC
	
	if(stripslashes($front->getContentpage($page ,'meta_content'))!='')
	{
	$META_CONTENT	= stripslashes($front->getContentpage($page ,'meta_content'));
	}
	else
	{
	$META_CONTENT	= stripslashes($meta_content = $front->fetchGeneral_config('meta_content'));		
	}
	if(stripslashes($front->getContentpage($page ,'meta_desc'))!='')
	{
	$META_DESC		= stripslashes($front->getContentpage($page ,'meta_desc'));
	}
	else
	{
	$META_DESC 		= stripslashes($meta_desc	 = $front->fetchGeneral_config('meta_desc'));	
	}

	$TITLE			= stripslashes($front->getContentpage($page ,'page_title'));
	if(strlen(trim($TITLE)) <= 0)
	{
		$TITLE 	= stripslashes($front->fetchGeneral_config("store"));
	}
		
?>

  <?php $css_path="https://stanlay.in/web/"; ?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo $TITLE;?></title>
<meta name="keywords" content="<?php echo $META_CONTENT;?>">
<meta name="description" content="<?php echo $META_DESC;?>">

<meta name="Robots" content="index, follow" />
<meta name="googlebot" content="noodp" /> 
<meta name="Robots" content="all" />
<meta name="revisit-after" content="7 days" />
<meta name="Author" content="www.stanlay.in, YBP" />

    <!-- Bootstrap -->
    <link href="<?php echo $css_path; ?>css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo $css_path; ?>css/font-awesome.min.css">
	<link href="<?php echo $css_path; ?>css/animate.min.css" rel="stylesheet">
    <link href="<?php echo $css_path; ?>css/prettyPhoto.css" rel="stylesheet">      
	<link rel="stylesheet" href="<?php echo $css_path; ?>css/reset.css">
	<link href="<?php echo $css_path; ?>css/main.css" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo $css_path; ?>css/menu.css">
	<link href="<?php echo $css_path; ?>css/responsive.css" rel="stylesheet">
    <link href="<?php echo $css_path; ?>css/pgwslider.css" rel="stylesheet">
	 <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    
  </head>