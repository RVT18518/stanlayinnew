<?php 
	if($_REQUEST["action"]=="MailSend")
	{
		$to  	 =	$front->fetchGeneral_config("admin");
		$from    =	$_REQUEST["email"];// false mail id 
		$subject = 	"Stanlay: Feedback from website";
		$headers = 	"MIME-Version: 1.0\r\n".
					"Content-type: text/html; charset=iso-8859-1\r\n".
					"From: <".$from.">\r\n".
					"Date: ".date("r")."\r\n".
					"Subject: ".$subject."\r\n";
		ob_start();                       // start output buffer 2
		include("includes/inquery_form_format.php");        // fill ob2
		$message  = ob_get_contents();    // read ob2 ("b")
		ob_end_clean();
		
		$result1 = mail($to, $subject, $message, $headers);
		if($result1 == 1)
		{
			$front->pageLocation("enquiry_thanks.php");
		}
		else
		{
			$PAGE_PERMALINK	= stripslashes($front->getContentpage('86' ,'permalink'));
			$url	= str_replace(" ","_",$PAGE_PERMALINK)."_"."86.html";
			$front->pageLocation($url);
			//$front->pageLocation("contactus.php?action=error");
		}
	}
?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr><td><h1><?php echo  $front->CmsContent("7", "pages_name"); ?></h1></td></tr>
	<tr><td>&nbsp;</td></tr>
	<tr><td><?php echo stripslashes($front->CmsContent("7", "pages_html_text")); ?></td></tr>
<?php
	if($_REQUEST["action"] == "error")
	{		
?>
	<tr><td class="required">Your message could not send. Please try after some time.</td></tr>
<?php
	}
?>

	<tr>
		<td>
			<form id="frx1" name="frx1" enctype="multipart/form-data" method="post" action="contactus.php?action=MailSend">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td width="35%" align="right" class="highlight_brdrbtm"><strong><span class="required">*</span> Name:</strong></td>
					<td width="65%" class="highlight_brdrbtm"><input name="name" id="name" size="40" maxlength="50" class="inputbox" /></td>
				</tr>
				<tr>
					<td width="35%" align="right" ><strong><span class="required">*</span> Company:</strong></td>
					<td width="65%" ><input name="company" id="company" size="40" maxlength="50" class="inputbox" /></td>
				</tr>
				<tr>
					<td width="35%" align="right" class="highlight_brdrbtm"><strong><span class="required">*</span> Address:</strong></td>
					<td width="65%" class="highlight_brdrbtm"><textarea name="address" id="address" rows="3" cols="50" value="" class="inputbox"></textarea></td>
				</tr>
				<tr>
					<td width="35%" align="right" ><strong><span class="required">*</span> City:</strong></td>
					<td width="65%" ><input name="city" id="city" size="40" maxlength="50" class="inputbox" /></td>
				</tr>
				<tr>
					<td width="35%" align="right" class="highlight_brdrbtm"><strong><span class="required">*</span> State:</strong></td>
					<td width="65%" class="highlight_brdrbtm"><input name="state" id="state" size="40" maxlength="10" class="inputbox" /></td>
				</tr>
				<tr>
					<td width="35%" align="right" ><strong><span class="required">*</span> Zip Code:</strong></td>
					<td width="65%" ><input name="zipcode" id="zipcode" size="40" maxlength="20" class="inputbox" /></td>
				</tr>
				<tr>
					<td width="35%" align="right" class="highlight_brdrbtm"><strong><span class="required">*</span> Phone:</strong></td>
					<td width="65%" class="highlight_brdrbtm"><input name="phone" id="phone" size="40" maxlength="20" class="inputbox" /></td>
				</tr>
				<tr>
					<td width="35%" align="right" ><strong>Fax:</strong></td>
					<td width="65%" ><input name="fax" id="fax" size="40" maxlength="20" class="inputbox" /></td>
				</tr>
				<tr>
					<td width="35%" align="right" class="highlight_brdrbtm"><strong><span class="required">*</span> E-Mail:</strong></td>
					<td width="65%" class="highlight_brdrbtm"><input name="email" id="email" size="40" maxlength="50" class="inputbox" /></td>
				</tr>
				<tr>
					<td align="right" valign="top"><strong><span class="required">*</span> How can we help you?</strong></td>
					<td valign="top"><textarea name="comments" id="comments" rows="6" cols="50" value="" class="inputbox"></textarea></td>
				</tr>
				<tr>
				  <td align="right" class="highlight_brdrbtm"><strong>How would you like to be contacted?</strong></td>
				  <td class="highlight_brdrbtm"><input type="checkbox" name="checkbox" value="checkbox" />Email
			      <input type="checkbox" name="checkbox2" value="checkbox" /> Phone
			      <input type="checkbox" name="checkbox3" value="checkbox" /> No Preference</td>
			  </tr>
				<tr>
					<td align="right">&nbsp;</td>
					<td>
						<table border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td align="left" width="110"><input type="image" id="submit" name="submit" src="images/button_sendto.gif" alt="" border="0" /></td>
							<td align="left"><input type="reset" value="" class="enquiry_btn" /></td>
						</tr>
						</table>					</td>
				</tr>
			</table>
			</form>
		</td>
	</tr>
</table>
<script language="JavaScript" type="text/javascript">
 var frmvalidator = new Validator("frx1");
 frmvalidator.addValidation("name","req","Please enter Name.");
 frmvalidator.addValidation("company","req","Please enter Company.");
 frmvalidator.addValidation("address","req","Please enter Address.");
 frmvalidator.addValidation("city","req","Please enter City.");
 frmvalidator.addValidation("state","req","Please enter State.");
 frmvalidator.addValidation("zipcode","req","Please enter Zip Code.");
 frmvalidator.addValidation("phone","req","Please enter Phone.");
 frmvalidator.addValidation("email","req","Please enter E-Mail.");
 frmvalidator.addValidation("email","email","Please enter valid E-Mail.");
 frmvalidator.addValidation("comments","req","Please enter your area of inquiry.");
</script>