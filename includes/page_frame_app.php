<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title><?php echo $PAGE_TITLE;?></title>
		<meta name="keywords" content="<?php echo $PAGE_META_CONTENT;?>">
		<meta name="description" content="<?php echo $PAGE_META_DESC;?>">
		<link href="<?php echo $css_path; ?>style/style_app.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="<?php echo $css_path; ?>fade-show/jd.gallery.css" type="text/css" media="screen" charset="utf-8" />
		<script src="<?php echo $css_path; ?>fade-show/mootools-1.2.1-core-yc.js" type="text/javascript"></script>
		<script src="<?php echo $css_path; ?>fade-show/mootools-1.2-more.js" type="text/javascript"></script>
		<script src="<?php echo $css_path; ?>fade-show/jd.gallery.js" type="text/javascript"></script>
		<script type="text/javascript">
			function startGallery() {
				var myGallery = new gallery($('myGallery'), {timed: true});}
			window.addEvent('domready',startGallery);
		</script>
		<!--<link href="<?php echo $css_path; ?>/stanlay-style/style_category.css" rel="stylesheet" type="text/css" />-->
     <style type="text/css">
		.main-link {
	height:38px;
	width:980px;
	float:left;
	background-image:url(../../images/link-area-1.png);
	background-repeat:repeat;
	margin-top:15px;

}

@charset "utf-8";
/* CSS Document */

body {
	margin:0px;
	padding:0px;
	font-size:12px;
	font-family:Arial, Helvetica, sans-serif;
	font-weight:normal;
	background-color:#58595b;
	background-image:url(../images/background-images.png);
	background-repeat:repeat;
	background-attachment:fixed
}
#main {
	width:980px;
	min-height:900px;
	height:auto;
	margin:auto;
	overflow:hidden;
	border-top:none;
	border-bottom:none;
	background-color:#ebebed;
	padding-left:5px;
	padding-right:5px;
}
#top {
	width:980px;
	height:82px;
	float:left;
	background:#58595b;
}
#top-logo {
	width:220px;
	height:75px;
	float:left;
	background:#58595b;
	padding-top:7px;
	padding-left:10px
}
#top-logo-text {
	width:410px;
	height:52px;
	float:left;
	background:#58595b;
	padding-top:30px;
	text-align:left;
}
#top-search {
	width:220px;
	height:33px;
	float:left;
	padding-top:49px;
	padding-left:120px;
	background:#ED3237;
}
.text-control {
	font-size:1em;
	color:#CCC;
	font-family:Verdana, Geneva, sans-serif
}
.search {
	width:182px;
	height:25px;
	padding:0px;
	float:left;
	margin:0px;
	background:none;
	background-image:url(../images/search-back.png);
	background-repeat:no-repeat;
	border:none;
	color:#9d9d9d;
	padding-left:5px
}
.s-text {
	width:182px;
	height:25px;
	padding:0px;
	margin:0px;
	float:left;
	background:none;
	border:none;
}
.s-go {
	width:30px;
	height:25px;
	padding:0px;
	float:left;
	margin:0px;
	background:none;
	border:none;
}
.main-link {
	height:38px;
	width:980px;
	float:left;
	background-image:url(../images/link-area.png);
	background-repeat:no-repeat;
	margin-top:15px;
.
}
.main-link-small-home {
	height:30px;
	width:99px;
	float:left;
	padding-top:8px;
	margin-right:2px;
	text-align:center;
	font-family:Lucida Sans Unicode;
	font-weight:300;
	font-size:12px;
	color:#fff;
	text-decoration:none;
}
.main-link-small-home a {
	font-family:Lucida Sans Unicode;
	font-weight:300;
	font-size:12px;
	color:#fff;
	text-decoration:none;
}
.main-link-small-home:hover {
	background-image:url(../images/link-area-back-home.png);
	background-repeat:no-repeat;
}
.main-link-small {
	height:30px;
	width:98px;
	float:left;
	padding-top:8px;
	margin-right:2px;
	text-align:center;
}
.main-link-small:hover {
	background-image:url(../images/link-area-back.png);
	background-repeat:no-repeat;
}
.main-link-small-product {
	height:38px;
	width:100px;
	float:left;
	padding-top:0px;
	margin-right:0px;
	text-align:center;
}
.main-link-small-product:hover {
	background-image:url(../images/link-area-back.png);
	background-repeat:no-repeat;
}
.pad-mar-0 {
	padding:0px;
	margin:0px
}
.main-link-small a {
	font-family:Lucida Sans Unicode;
	font-weight:300;
	font-size:12px;
	color:#fff;
	text-decoration:none;
}
.link-location {
	height:35px;
	width:980px;
	float:left;
	background-image:url(../images/current-link-locations-background.png);
	background-repeat:no-repeat;
	margin-top:15px;
.
}
.link-location-box {
	height:24px;
	min-width:44px;
	width:auto;
	float:left;
	background-image:url(../images/link-location-arrow.png);
	background-repeat:no-repeat;
	background-position:right;
	margin-top:1px;
	padding-top:9px;
	text-align:center;
	padding-right:17px;
	padding-left:10px
}
.link-location-box-no-arrow {
	height:24px;
	min-width:44px;
	width:auto;
	float:left;
	margin-top:1px;
	padding-top:9px;
	text-align:center;
	padding-right:17px;
	padding-left:10px
}
.link-location-box-no-arrow a {
	font-family:Calibri;
	font-weight:bold;
	font-size:13px;
	color:#989797;
	text-decoration:none;
}
.link-location-box-no-arrow a:hover {
	font-family:Calibri;
	font-weight:bold;
	font-size:13px;
	color:#F00;
	text-decoration:none;
}
.link-location-box a {
	font-family:Calibri;
	font-weight:bold;
	font-size:13px;
	color:#8d8d8d;
	text-decoration:none;
}
.link-location-box a:hover {
	font-family:Calibri;
	font-weight:bold;
	font-size:13px;
	color:#F00;
	text-decoration:none;
}
.slide-show {
	height:380px;
	width:980px;
	float:left;
	margin-top:15px;
	background-image:url(../images/picture-back.png);
	background-repeat:no-repeat
}
.small-product-show {
	height:194px;
	width:980px;
	float:left;
	margin-top:15px;
	background-image:url(../images/product-background.png);
	background-repeat:no-repeat
}
.small-product-show-box {
	height:194px;
	width:245px;
	float:left;
	color:#777777
}
.small-product-show-box-mid {
	height:194px;
	width:245px;
	float:left;
	color:#777777
}
.small-product-show-box-right {
	height:194px;
	width:245px;
	float:left;
	color:#777777
}
.small-product-show-box-head {
	height:17px;
	padding-top:12px;
	width:235px;
	padding-left:10px;
	float:left;
	text-align:left;
	margin-bottom:5px;
	font-size:16px;
	color:#333;
	font-weight:bold;
}
.small-product-show-box-img {
	height:86px;
	width:245px;
	float:left;
	text-align:center
}
.small-product-show-box-text {
	height:55px;
	width:226px;
	float:left;
	text-align:justify;
	font-family:"Lucida Sans Unicode", "Lucida Grande", sans-serif;
	padding:10px;
	padding-bottom:0px;
	padding-top:0px;
	font-size:11px;
	line-height:15px;
}
.small-product-show-box:hover {
	cursor:pointer;
	background-image:url(../images/product-background-left.png);
	background-repeat:no-repeat;
	color:#017fbe
}
.small-product-show-box-mid:hover {
	cursor:pointer;
	background-image:url(../images/product-background-middle.png);
	background-repeat:no-repeat;
	color:#017fbe
}
.small-product-show-box-right:hover {
	cursor:pointer;
	background-image:url(../images/product-background-right.png);
	background-repeat:no-repeat;
	color:#017fbe
}
.bottom-bot {
	width:960px;
	min-height:60px;
	height:auto;
	overflow:hidden;
	float:left;
	background-color:#FFF;
	padding-top:10px;
	margin-top:15px;
	padding-left:20px;
}
.bottom-bot-left {
	width:240px;
	min-height:60px;
	height:auto;
	overflow:hidden;
	float:left;
}
.bottom-bot-right {
	width:690px;
	min-height:60px;
	height:auto;
	overflow:hidden;
	float:left;
	line-height:20px;
	color:#999
}
.bottom-bot-right a {
	font-size:11px;
	font-family:Arial;
	color:#333;
	text-decoration:none;
	font-weight:bold;
}
.bottom-bot-right a:hover {
	color:#F00;
}
.bottom-link {
	float:left;
	height:320px;
	width:980px;
	background-image:url(../images/bottom-link-back.png);
	background-repeat:no-repeat;
	margin-top:15px;
}
.link-location-bot {
	height:35px;
	width:900px;
	float:left;
}
.link-location-bot-1 {
	height:24px;
	width:80px;
	float:left;
	margin-top:1px;
	padding-top:9px;
	text-align:center;
}
.link-location-bot-1 a {
	font-family:Calibri;
	font-weight:bold;
	font-size:13px;
	color:#F00;
	text-decoration:none;
}
.link-location-box-bot {
	height:24px;
	min-width:44px;
	width:auto;
	float:left;
	background-image:url(../images/link-location-arrow-bot.png);
	background-repeat:no-repeat;
	background-position:right;
	margin-top:1px;
	padding-top:9px;
	text-align:center;
	padding-right:17px;
	padding-left:10px
}
.link-location-box-bot a {
	font-family:Calibri;
	font-weight:bold;
	font-size:13px;
	color:#8d8d8d;
	text-decoration:none;
}
.link-location-box-bot a:hover {
	font-family:Calibri;
	font-weight:bold;
	font-size:13px;
	color:#F00;
	text-decoration:none;
}
.link-location-bot-bot {
	height:273px;
	margin-top:10px;
	width:970px;
	margin-left:10px;
	float:left;
}
.link-location-bot-bot-box {
	height:273px;
	width:250px;
	margin-right:10px;
	float:left;
}
.link-location-bot-bot-box a {
	font-family:"Lucida Sans Unicode", "Lucida Grande", sans-serif;
	font-weight:normal;
	font-size:11px;
	color:#888888;
	text-decoration:none;
	line-height:19px
}
.link-location-bot-bot-box a:hover {
	font-family:"Lucida Sans Unicode", "Lucida Grande", sans-serif;
	font-weight:normal;
	font-size:11px;
	color:#F00;
	text-decoration:none;
}
.h4cont {
	padding:0px;
	margin:0px;
	padding-bottom:4px;
	color:#666666;
	font-family:'Lucida Sans Unicode', 'Lucida Grande', sans-serif;
	font-size:11px
}
.arrow {
	font-size:11px;
	font-weight:bold;
	font-family:Calibri;
	color:#333;
	margin-left:10px;
}
a {
text-decoration:none
}
		</style>
         <!-- EXPANDABLE SLIDING & FLOATING LINKS MENU -->
<!-- This goes into the HEAD of the html file -->
<script language="JavaScript" type="text/javascript">
<!-- Copyright 2003, Sandeep Gangadharan -->
<!-- For more free scripts go to http://www.sivamdesign.com/scripts/ -->
<!--
var place = 80;   // change the # 80 to adjust the placement of the menu from the top of the page. The greater
                  // number the lower will the menu be placed.
(document.getElementById && !document.all) ? dom = true : dom = false;
function moveIt() {
  if (dom) {document.getElementById('linksBox').style.left = parseInt(document.getElementById('linksBox').style.left) + 333 + "px";}
  if (document.all) {document.all["linksBox"].style.pixelLeft = parseInt(document.all["linksBox"].style.pixelLeft) + 333;}
}

function backIn() {
  if (dom) {document.getElementById('linksBox').style.left = parseInt(document.getElementById('linksBox').style.left) - 333 + "px";}
  if (document.all) {document.all["linksBox"].style.pixelLeft = parseInt(document.all["linksBox"].style.pixelLeft) - 333;}
}

function typeStart() {
//	alert('start');
  if (dom || document.all) {
    document.write('<div id="linksBox" style="position:absolute; left:-333px; top:205px; visibility:visible; cursor:hand; cursor:pointer" onmouseover="moveIt()" onmouseout="backIn()">') }
}

function typeEnd() { if (dom || document.all) { document.write('</div>') } }

function placeIt() {
  if (dom) {document.getElementById("linksBox").style.top = window.pageYOffset + place + "px";}
  if (document.all) {document.all["linksBox"].style.top = document.documentElement.scrollTop + place + "px"}
  window.setTimeout("placeIt()", 10); }
// -->

function smallwin() {
SW=window.open('<?php echo $css_path;?>contact.php','NewWin','toolbar=no,menubar=no,location=no,resizable=no,status=no,width=450,height=350,scrollbars=no')
                   // change the name, features and the figures of width and height above to customize the popup window.
SW.moveTo(190,240);  // change the #s at the left to adjust the left and top margins respectively
}

</script>
<style type="text/css">
.outerTable {
	border: #000000 0px solid;
	background: #ffffff;
	color: #000000;
}
.innerTable {
	background: #ffffff;
	border-top: #000000 1px solid;
	border-left: #000000 1px solid;
	color: #000000;
}
.linkText {
	border-left: #000000 1px solid;
	font-family: arial, helvetica, sans-serif;
	font-size: 20px;
	line-height: 25px;
	font-weight: bold;
	color: #FFF;
	font-family: Calibri;
}
.theDiv a {
	text-decoration: none;
	color: blue;
	background-color: #ffffff;
	font-family: arial, helvetica, sans-serif;
	font-size: 12px;
	padding-top: 0;
	padding-bottom: 0;
	padding-left: 4px;
	padding-right: 4px;
}
.theDiv a:hover {
	text-decoration: none;
	background-color: #ED3237;

}

table {
	text-decoration: none;
	background-color: #ffffff;
	color:#666;
	padding:0px;
		line-height:1.6em;
}

</style>
</head>
		<body>

        <script>typeStart()</script>
<table border="1" width="250" bgcolor="#ED3237" cellpadding="4" cellspacing="0" class="outerTable" style="display:non">
  <tr>
    <td width="10"><!-- edit the links below to suit your needs. Users can add as many links as they want. -->
      
      <div class="theDiv">
        <table border="0" width="210" bgcolor="#ffffff" cellpadding="0" cellspacing="0" class="innerable">
          <tr>
            <td valign="top"><table width="250" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td rowspan="5" style="border-right:#F00 solid 0px" valign="top"><img src="<?php echo $css_path;?>images/stanlay-logo-black.png" alt=""  style="display:none"  /></td>
                  <td><h2>Sachin Arora</h2></td>
                </tr>
                <tr>
                  <td><strong>Sales Coordinator</strong></td>
                </tr>
                <tr>
                  <td>Asian Contec Limited</td>
                </tr>
                <tr>
                  <td>Asian Center, b-28, okhla industrial area, </td>
                </tr>
                <tr>
                  <td>phase 1, New Delhi-110020, India</td>
                </tr>
                <tr>
                  <td rowspan="4" style="border-right:#F00 solid 0px" align="left" valign="top"><img src="<?php echo $css_path;?>images/email_us.png" alt=""  height="150" border="0" onclick="javascript:smallwin();"  style="display:none" /></td>
                  <td>Tel: +91-11-41860000, Fax:  +91-11-41860066</td>
                </tr>
                <tr>
                  <td>Sales Helpline:  +91-11-41406926</td>
                </tr>
                <tr>
                  <td>Mob: +91-9910659093</td>
                </tr>
                <tr>
                  <td nowrap="nowrap"><a href="mailto:sales@stanlay.com">sales@stanlay.com</a> / <a href="http://www.stanlay.com" title="www.stanlay.com" target="_blank">www.stanlay.com</a> / <a href="http://www.stanlay.in" title="www.stanlay.in" target="_blank">www.stanlay.in</a></td>
                </tr>
              </table></td>
          </tr>
        </table>
      </div>
      
      <!-- do not edit below. --></td>
    <td bgcolor="#ED3237" width="25" align="center" valign="middle" rowspan="5" class="linkText" ><!--<b>C<br />O<br />N<br />T<br />A<br />C<br />T<br /><br />U<br />S<br /></b>--> 
      <b>C<br />
      o<br />
      n<br />
      t<br />
      a<br />
      c<br />
      t<br />
      <br />
      u<br />
      s<br />
      </b> 
      
      <!--    <b>M<br />
      a<br />
      y<br />
      <br />
      I<br />
      h<br />
      e<br />
      l<br />
      
      p<br />
      u<br />
      </b>--></td>
  </tr>
  <!--<tr>
    <td width="210" bgcolor="#c0c0ff">&nbsp;</td>
  </tr>-->
</table>
<script>typeEnd()</script> 
		
<?php
/*echo "<pre>";
print_r($_SERVER);*/
$Cidp		= $_REQUEST["cidp"];
$path		= $_SERVER["REQUEST_URI"];
$path_exp	= explode("/",$path);
/*echo "<pre>";
print_r($path_exp);*/
//$perma_link	= $path_exp[3];



if($_SERVER["HTTP_HOST"]=='asian-pc' || $_SERVER["HTTP_HOST"]=='localhost')
{
$perma_link	= $path_exp[3];
}
else
{
$perma_link	= $path_exp[2];
}


$cat_query_perma_link					= "deleteflag = 'active' and cate_status = 'active' and cat_permalink = '$perma_link'";	
	if($perma_link!= "")
	{
//		echo "sdfdsfAAAAA";
		$rs_cat_query_perma_link		= $front->selectWhere("tbl_category", $cat_query_perma_link);
		if(mysqli_num_rows($rs_cat_query_perma_link)>0)
		{
			$row_cat_query_perma_link	= mysqli_fetch_object($rs_cat_query_perma_link);
			
//			print_r($row_cat_query_perma_link);
		
			$cate_id					= $row_cat_query_perma_link->cate_id;
			$cat_permalink1				= $row_cat_query_perma_link->cat_permalink;
		}
	}
if($cate_id==$Cidp)
{

//include("../modulefunction.php");
//	$cate_id		= $_REQUEST["cate_id"];
	$cat_query						= "deleteflag = 'active' and cate_status = 'active' and cate_id = $cate_id";	
	if($cate_id != "")
	{
		$rs_cat_query				= $front->selectWhere("tbl_category", $cat_query);
		if(mysqli_num_rows($rs_cat_query)>0)
		{
			$row_cat_query			= mysqli_fetch_object($rs_cat_query);
			$cat_name				= stripslashes($row_cat_query->cate_name);
			$description			= stripslashes($row_cat_query->cate_description);
			$cate_image				= $row_cat_query->cate_image;
			$cate_banner_image		= $row_cat_query->banner_image;
			$cate_description		= $row_cat_query->cate_description;
		}
	}
?>
<!-- The main Id is start from here -->
<div id="main"> 
          <!-- Top area  is starting from here --> 
          <!-- Top area  is Ended here --> 
          <!-- Link  is starting here -->
          <?php include("includes/header.php");?>
          <div class="link-location">
    <div class="link-location-box" style="padding-top:5px; height:28px"><a href="<?php echo $css_path;?>" title="Home"><img src="<?php echo $css_path;?>images/home-for-link-location.png" alt="Home" border="0" /></a></div>
    <div class="link-location-box">
     <?php
    if($_SESSION["new_new_path"]=='product-by-type' || $_SESSION["new_new_path"]=='')
{
?>
    <?php /*?>          <a href="<?php echo $css_path;?>product-by-type/" title="Product by Type">Product by Type</a></div><?php */?>
    <a href="<?php echo $css_path;?>product-by-application/" title="Product by Type">Product by Application</a> </div>
    <?php
} else {
	?>
    <a href="<?php echo $css_path;?>product-by-application/" title="Product by Type">Product by Application</a> </div>
          <div class="link-location-box"><a href="<?php echo $css_path;?>product-by-application/<?php echo $_SESSION["new_new_path2"];?>/" title="<?php echo ucwords(str_replace('-',' ',($_SESSION["new_new_path2"])))?>"><?php echo ucwords(str_replace('-',' ',($_SESSION["new_new_path2"])))?></a></div>
          <?php }?>
          <div class="link-location-box-no-arrow"><a href="" title="Basic Distance Measurement"><?php echo $cat_name;?></a></div>
        </div>
<!-- Link  is Endeding here -->
<?php
$sql_cat_pro	= "SELECT * from tbl_app_categories where app_cat_id=$cate_id";
//$sql_module = "select * from tbl_website_page where module_id = '$module_id' and page_type = '0'";
			$rs_cat_pro  = mysqli_query($GLOBALS["___mysqli_ston"],$sql_cat_pro);
			$rs_cat_pro_num  = mysqli_num_rows($rs_cat_pro);
			if($rs_cat_pro_num>0)
			{
				$num=1;
			while($row_cat_pro = mysqli_fetch_object($rs_cat_pro))
			{
 $sql_cat_product= "SELECT * from tbl_category where cate_id=$row_cat_pro->cate_id";
//$sql_module = "select * from tbl_website_page where module_id = '$module_id' and page_type = '0'";
			$rs_cat_product  = mysqli_query($GLOBALS["___mysqli_ston"],$sql_cat_product);
			$row_cat_product = mysqli_fetch_object($rs_cat_product);
			$pro_name		 = $row_cat_product->cate_name;
			$pro_id			 = $row_cat_product->cate_id;
			$pro_short_desc	 = $row_cat_product->short_desc;
			$pro_image		 = $row_cat_product->cate_image;
			$pro_permalink	 = $row_cat_product->cat_permalink;


	
 if($num==1) { echo "<div class='small-product-show'>"; } ?>
<a href="<?php echo $css_path;?><?php echo $pro_permalink;?>/" title="<?php echo $pro_name;?>">
        <div class="<?php if($num==1) { echo "small-product-show-box"; } else if($num==4) {echo "small-product-show-box-right"; } else { echo "small-product-show-box-mid"; } ?>">
  <div class="small-product-show-box-head"><?php echo stripslashes($row_cat_product->cate_name); ?></div>
  <div class="small-product-show-box-text"><?php echo $pro_short_desc;?></div>
  <div class="small-product-show-box-img"><img src="<?php echo $css_path;?><?php echo $pro_image;?>" width="175" height="85" border="0" /></div>
</div>
</a>
<?php  if($num==4) { echo "</div>"; }  $num++; if($num==5) { $num=1;  echo "<div>"; } ?>
    
<?php } }?>
<?php  if($num%4!=0) { echo "</div>"; } ?>
</div>

<?php include("includes/footer.php");?>
</div>
</div>
<?php 
}
?>
</body>
</html>