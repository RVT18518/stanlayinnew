<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $PAGE_TITLE;?></title>
<meta name="keywords" content="<?php echo $PAGE_META_CONTENT;?>">
<meta name="description" content="<?php echo $PAGE_META_DESC;?>">
<link href="css/style.css" rel="stylesheet" type="text/css" />
<!-- Accordion script -->
<script src="SpryAssets/SpryAccordion.js" type="text/javascript"></script>
<script language="JavaScript1.2" src="./js/scripts.js" type="text/javascript"></script>
<link href="SpryAssets/SpryAccordion.css" rel="stylesheet" type="text/css" />

<?php /*?><script type="text/javascript">
var GB_ROOT_DIR = "./greybox/";
</script>
<script type="text/javascript" src="greybox/AJS.js"></script>
<script type="text/javascript" src="greybox/AJS_fx.js"></script>
<script type="text/javascript" src="greybox/gb_scripts.js"></script>
<link href="greybox/gb_styles.css" rel="stylesheet" type="text/css" media="all" />

<?php */?>

<!-- Tabbing script -->
<link rel="stylesheet" href="tab/ui.tabs.css" type="text/css" media="print, projection, screen"/>
  <script type="text/javascript" src="tab/jquery-1.js"></script>
  <script src="tab/ui.core.js" type="text/javascript"></script>
  <script src="tab/ui.tabs.js" type="text/javascript"></script>
 <script type="text/javascript">
            $(function() {
                $('#container-1 > ul').tabs();
               
            });
    </script>	
	
</head>
<body>
<table width="1000" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="28" align="right" class="toplinks"><a href="#;">contact us</a> l <a href="#;">site map</a> </td>
  </tr>
  <tr>
    <td><?php include("includes/header.php");?>  </td>
  </tr>
  <tr>
    <td height="11"></td>
  </tr>
  <tr>
    <td class="white_space"><table width="965" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td class="banner_space"><img src="<?php echo $BANNERIMAGE;?>" alt="" width="965" height="200" /></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td class="greyBg"></td>
  </tr>
  <tr>
    <td height="15"></td>
  </tr>
  <tr>
    <td valign="top">
	<table width="965" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
	  <?php if($SHOWLEFTMENU == "yes"){?>
	  <td width="255" align="left" valign="top"><?php include("includes/leftmenu.php");?></td>
		<?php }?>
        <td valign="top" class="txt">
		<table width="100%" border="0" align="right" cellpadding="0" cellspacing="0">
          <tr><td width="708"><h1><?php echo $PAGE_NAME; ?></h1></td></tr>
		  <tr><td><?php echo $PAGE_CONTENT;?></td></tr>
		</table>
		</td>
		<?php /* if($SHOWRIGHTMENU == "yes"){?>
        <td width="250" valign="top"><?php include("includes/rightmenu.php");?></td>
		<?php } */?>
      </tr>
    </table>
	</td>
  </tr>
   <tr>
    <td height="15"></td>
  </tr>
   <tr>
    <td class="greyBg"></td>
  </tr>
  <tr>
    <td><?php include("includes/footer.php");?></td>
  </tr>
      
      <tr>
        <td height="20" colspan="2" class="copyright">&nbsp;</td>
      </tr>
    </table>	</td>
  </tr>
</table>
</body>
</html>

  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  <?php /*?><tr>
    <td valign="top">
	<table width="965" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
	  <?php if($SHOWLEFTMENU == "yes"){?>
        <td width="228" align="left" valign="top"><?php include("includes/leftmenu.php");?></td>
		<?php }?>
        <td width="452" valign="top" class="txt"><?php echo $PAGE_CONTENT;?></td>
		<?php if($SHOWRIGHTMENU == "yes"){?>
        <td width="273" valign="top"><?php include("includes/rightmenu.php");?></td>
		<?php }?>
      </tr>
    </table></td>
  </tr>
   <tr>
    <td height="15"></td>
  </tr>
   <tr>
    <td class="greyBg"></td>
  </tr>
  <tr>
    <td><?php include("includes/footer.php");?></td>
  </tr>
</table>
</body>
</html>
<?php */?>