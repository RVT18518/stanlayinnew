<?php 
	$data_action=$_REQUEST["action"];
	$pcode=$_REQUEST["pcode"];
	if($_REQUEST['action']=='update')
		{
			 $result=$s->edit_row_company($pcode);
		}
	$rs= $s->getData_without_condition('tbl_company');
	if(mysqli_num_rows($rs)!=0)
	{
		$row=mysqli_fetch_object($rs);
		$comp_id	  = $row->company_id;	
		$comp_name    = $row->company_name;
		$comp_fname   = $row->company_owner_first_name;
		$comp_lname   = $row->company_owner_last_name;
		$comp_email   = $row->company_email ;
		$comp_address = $row->company_address;
		$comp_city	  = $row->company_city;
		$comp_country = $row->company_country;
		$comp_state	  = $row->company_state;
		$comp_zip	  =	$row->company_zip;
		$comp_shopcart= $row->shopping_cart_show;
		$sql_co="select country_name from tbl_country where country_id=$comp_country";
		$rs_co=mysqli_query($GLOBALS["___mysqli_ston"],$sql_co);
		$row_co=mysqli_fetch_object($rs_co);
		$comp_country_name = $row_co->country_name;
	}
?>

<form name="frx1" id="frx1" method="post" action="index.php?pagename=company_details&action=edit&pcode=<?php echo $comp_id;?>">
  <table width="100%"  border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="24%" class="pageheadTop">Company Details</td>
            <td width="76%" class="headLink">&nbsp;</td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td class="pHeadLine"></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><?php 
			if($_REQUEST['action']=='update')
			{
				if($result==1)
				{
					echo "<p class='success'>".record_update."</p><br />";	
				}
				else if($result==0)
				{
					echo "<p class='error'>".record_not_update."</p><br />";	
				}
			}
?></td>
    </tr>
    <tr>
      <td valign="top" class="pagecontent"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="tblBorder">
          <tr >
            <td class="pagehead">Company Details</td>
          </tr>
          <tr>
            <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr class="text">
                  <td width="16%" class="pad" >Company Name</td>
                  <td width="84%"  align="left"><?php echo $comp_name;?></td>
                </tr>
                <tr class="text">
                  <td class="pad">Owner First Name </td>
                  <td><?php echo $comp_fname;?></td>
                </tr>
                <tr class="text">
                  <td class="pad">Owner Last Name </td>
                  <td><?php echo $comp_lname;?></td>
                </tr>
                <tr class="text">
                  <td class="pad">Email </td>
                  <td><?php echo $comp_email;?></td>
                </tr>
                <tr class="text">
                  <td valign="top" class="pad"> Address</td>
                  <td  ><?php echo $comp_address;?></td>
                </tr>
                <tr class="text">
                  <td class="pad">City</td>
                  <td><?php echo $comp_city;?></td>
                </tr>
                <tr class="text">
                  <td class="pad"> Country </td>
                  <td><?php echo $comp_country_name;?></td>
                </tr>
                <tr class="text">
                  <td class="pad"> State/Province </td>
                  <td><?php echo $comp_state;?></td>
                </tr>
                <tr class="text">
                  <td class="pad">Zip/Postal Code </td>
                  <td><?php echo $comp_zip;?></td>
                </tr>
                <tr class="text">
                  <td class="pad"> Enable Shopping Cart</td>
                  <td><?php echo  ucfirst($comp_shopcart);?></td>
                </tr>
                <tr class="text">
                  <td class="pad">&nbsp;</td>
                  <td><input name="add " type="submit" class="inputton" id="add" value="Change">
                    &nbsp;</td>
                </tr>
              </table></td>
          </tr>
        </table></td>
    </tr>
  </table>
</form>
