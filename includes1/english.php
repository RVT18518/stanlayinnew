<?php
define ("record_added", "Record added successfully" );
define ("record_not_added", "Record not added successfully" );

define ("record_delete", "Record deleted successfully" );
define ("record_not_delete", "Record not deleted successfully" );

define ("record_update", "Record updated successfully" );
define ("record_not_update", "Record not updated successfully" );

define("image_added" , "Image uploaded successfully");
define("image_not_added" , "Image not uploaded successfully");

define("validate_form_error", "Kindly fill all the fields.");

define("no_record", "No Record Found in database.");

define("record_exists" , "Record exists in database.");

define("INVALID_LOGIN" , "Incorrect login details. Please try again.");
define("OUT_STOCK" , "Out of stock.");
define("mail_sent_success" , "Mail sent successfully");
define("mail_sent_error" , "Error while sending e-mail, please try again ");
?>
