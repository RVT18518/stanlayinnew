<?php 
// display all error except deprecated and notice  
error_reporting( E_ALL & ~E_DEPRECATED & ~E_NOTICE );
session_start(); 
date_default_timezone_set("Asia/Kolkata");
include("english.php");
include("conn_db_connection.php");
//ob_start();
define('DB_DRIVER', 'mysql');
define("DB_HOST", $dataServer);
define("DB_USER", $dataUser);
define("DB_PASSWORD", $dataPassword);
define("DB_DATABASE", $dataDBName);
// basic options for PDO 
$dboptions = array(
    PDO::ATTR_PERSISTENT => FALSE,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
);
//connect with the server
try {
    $DB = new PDO(DB_DRIVER . ':host=' . DB_HOST . ';dbname=' . DB_DATABASE, DB_USER, DB_PASSWORD, $dboptions);
} catch (Exception $ex) {
    echo($ex->getMessage());
    die;
}
//mysqli
$con = mysqli_connect($dataServer, $dataUser,$dataPassword,$dataDBName);
// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  
  }
 $currency_symbol="&#x20b9;";
 $currency_symbol_dollar="&#36;";
/*$mysqli_conn = mysqli_connect($dataServer, $dataUser, $dataPassword) or die("<center>An Internal Error has Occured. Please report following error to the webmaster.<br><br>".mysqli_error()."'</center>");

mysqli_select_db($mysqli_conn,$dataDBName);
*/
class myclass
{
function conn()
{
include("conn_db_connection.php");
/*$s = @mysqli_connect($dataServer,$dataUser,$dataPassword) or die(mysqli_error());
$d = mysqli_select_db($dataDBName)or die("Error ! Database connection Failure"); */
$con = mysqli_connect($dataServer, $dataUser,$dataPassword,$dataDBName);
}

function delete_table_withCondition($table_name, $field_name, $condition)
{	
//$sql="DELETE  FROM $table_name WHERE $field_name='".$condition."' ";
$sql="update  ".$table_name ." set deleteflag='inactive' WHERE $field_name='".$condition."' ";
return   $x = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
}

function kc_delete_table_withCondition($table_name, $field_name, $condition)
{	   
//$sql="DELETE  FROM $table_name WHERE $field_name='".$condition."' ";
$sql="update  ".$table_name ." set deleteflag='active' WHERE $field_name='".$condition."' ";//exit;
return   $x = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
}

function delete_table_withCondition_ten($table_name, $field_name, $condition)
{	   
//$sql="DELETE  FROM $table_name WHERE $field_name='".$condition."' ";
$sql="update  ".$table_name ." set tnd_del_status='inactive' WHERE $field_name='".$condition."' ";
return   $x = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
}

function getMonthsInRange($startDate, $endDate) {
$months = array();
while (strtotime($startDate) <= strtotime($endDate)) {
    $months[] = array('year' => date('Y', strtotime($startDate)), 'month' => date('m', strtotime($startDate)), );
    $startDate = date('d M Y', strtotime($startDate.'+ 1 month'));
}
return $months;

}

function ApplicationHsn_code($IdValue)
{
if(is_numeric($IdValue))
{
$sqlApplication = "select hsn_code from tbl_application where application_id = '$IdValue' and deleteflag = 'active'";
$rsApplication  = mysqli_query($GLOBALS["___mysqli_ston"],  $sqlApplication);
$rowApplication = mysqli_fetch_object($rsApplication);
$hsn_code	  = $rowApplication->hsn_code;
}
else
{
$hsn_code 	= $IdValue;
}
return $hsn_code;
}

function ProHsn_code($IdValue)
{   
$sqlApplication = "select hsn_code from tbl_products_entry where pro_id = '$IdValue' and deleteflag = 'active'";
$rsApplication  = mysqli_query($GLOBALS["___mysqli_ston"],  $sqlApplication);
$rowApplication = mysqli_fetch_object($rsApplication);
$hsn_code	  = $rowApplication->hsn_code;
return $hsn_code;
}
//aded on 26-feb-2021
function get_pro_by_upc_code($IdValue)
{   
$sqlApplication = "select pro_id from tbl_products where upc_code = '$IdValue' ";
$rsApplication  = mysqli_query($GLOBALS["___mysqli_ston"],  $sqlApplication);
$rowApplication = mysqli_fetch_object($rsApplication);
$pro_id	  = $rowApplication->pro_id;
return $pro_id;
}

function Product_app_id($IdValue)
{   
if(is_numeric($IdValue))
{
$sqlApplication = "select pro_id from tbl_index_g2 where match_pro_id_g2 = '$IdValue' and deleteflag = 'active'";
$rsApplication  = mysqli_query($GLOBALS["___mysqli_ston"],  $sqlApplication);
$rowApplication = mysqli_fetch_object($rsApplication);
$pro_app_id	  = $rowApplication->pro_id;
}
else
{
$pro_app_id 	= $IdValue;
}
return $pro_app_id;
}//get selected category warranty & calibration function added on 23-june-2020
function app_category_details($application_id)
{
$sqlState = "select tax_class_id,hsn_code,cat_warranty,cat_calibration  from tbl_application where application_id = '$application_id' and deleteflag = 'active'";
$rsState  = mysqli_query($GLOBALS["___mysqli_ston"],  $sqlState);
$rowState = mysqli_fetch_object($rsState);
$details	  = $rowState;
return $details;
}// Function to add or Insert record in Database 
function insertRecord($table, $dataArray)
{   
$sql="Insert into ".$table ." set ";
$i=0;
foreach($dataArray as $key => $val)
{
if($i==0)
$sql.=" ".$key." = '".$val."'";
else
$sql.=", ".$key." = '".$val."'";			
$i++;
}
//echo $sql."<br> <br>";
//exit();
$result=mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
if (!$result)
{
return (1);//error
}
else 
{
return (0);//not error
}
}



function insertRecord_auto($table, $dataArray)
{   
$sql="Insert into ".$table ." set ";
$i=0;
foreach($dataArray as $key => $val)
{
if($i==0)
$sql.=" ".$key." = '".$val."'";
else
$sql.=", ".$key." = '".$val."'";			
$i++;
}
echo $sql."<br> <br>";
//exit();
$result=mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
if (!$result)
{
return (1);//error
}
else 
{
return (0);//not error
}
} 


 
// Function to add or Insert record in Database 
function insertRecord1($table, $dataArray)
{   
$sql="Insert into ".$table ." set ";
$i=0;
foreach($dataArray as $key => $val)
{
if($i==0)
$sql.=" ".$key." = '".$val."'";
else
$sql.=", ".$key." = '".$val."'";			
$i++;
}
//echo $sql."<br>";
//exit();
$result=mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
if (!$result)
{
return (1);//error
}
else 
{
return (0);//not error

}

} 

function delte_record($table, $id)
{   
$sql="DELETE FROM $table WHERE ID='$id'";
//echo $sql."<br>";
//exit();
$result=mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
if (!$result)
{
return (1);//error
}
else 
{
return (0);//not error
}

} 

function delete_record1($table, $id)
{   
$sql="DELETE FROM $table WHERE id='$id'";
//echo $sql."<br>";
//exit();
$result=mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
if (!$result)
{
return (1);//error
}
else 
{
return (0);//not error
}
} 

// Function to add or Insert record in Database 
function insertRecord2($table, $dataArray)
{   
$sql="Insert into ".$table ." set ";
$i=0;
foreach($dataArray as $key => $val)
{
if($i==0)
$sql.=" ".$key." = '".$val."'";
else
$sql.=", ".$key." = '".$val."'";			
$i++;
}
//echo $sql;
//exit();
$result=mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
if (!$result)
{
return (1);//error
}
else 
{
return (0);//not error
}

} 

// Function to edit record in Database 
function editRecord($table, $dataArray, $field , $v)
{   
$sql="update  ".$table ." set ";
$i=0;
foreach($dataArray as $key => $val)
{
if($i==0)
$sql.=" ".$key." = '".$val."'";
else
$sql.=", ".$key." = '".$val."'";			
$i++;
}
$sql .=" where  ". $field ." = '". $v ."'" ;
//getData_without_conditionecho $sql;
//echo $sql;
//exit; 
$result=mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
if (!$result)

{

return (1);//error

}

else 

{

return (0);//not error

}

} 

// Function to edit record in Database 

function editRecord_2($table, $dataArray, $field , $v, $field1 , $v1)
{   
$sql="update  ".$table ." set ";
$i=0;
foreach($dataArray as $key => $val)
{
if($i==0)
$sql.=" ".$key." = '".$val."'";
else
$sql.=", ".$key." = '".$val."'";			
$i++;
}
$sql .=" where  ". $field ." = '". $v ."' AND ". $field1 ." = '". $v1 ."'" ;
//echo $sql;
//exit();
$result=mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
if (!$result)
{
return (1);//error
}
else 
{
return (0);//not error
}
} 

// Function for getting sales target Sorted
function getTarget($field_name,$condition) {
$sql	= "SELECT sales_target FROM tbl_admin where deleteflag='active' AND $field_name='$condition'";
$result = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$result1 = mysqli_fetch_object($result);
return $result1->sales_target;
}

// Function for getting data Sorted
function getData_with_sorting($table_name, $field_name, $condition)
{   
$sql	= "SELECT * FROM $table_name where deleteflag='active' order by $field_name $condition";
$result = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
return $result;
}

// Function for getting data with out condition.
function getData_cust_segment($table_name, $orderby='1=1', $ase='asc')
{   
$sql 	= "SELECT * FROM $table_name where deleteflag='active' and cust_segment_status='active'  order by $orderby $ase";
$result = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
return $result;
}// Function for getting data with out condition.

function getData_without_condition($table_name, $orderby='1=1', $ase='asc')
{
$sql 	= "SELECT * FROM $table_name where deleteflag='active'  order by $orderby $ase";
$result = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
return $result;
}

// Function for getting data with out condition. added on 16-jul-2019 by rumit for city on enquiry manager inside sales
function getData_without_condition_status($table_name, $orderby='1=1', $ase='asc')
{
$sql 	= "SELECT * FROM $table_name where deleteflag='active' and status='active'  order by $orderby $ase";
$result = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
return $result;
}

function getData_with_condition_cust_segment($table_name, $orderby='1=1', $ase='asc')
{
$sql 	= "SELECT * FROM $table_name where deleteflag='active' and cust_segment_status='active'  order by $orderby $ase";
$result = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
return $result;

}
//new by rumit for order by condition 15july2019
function getData_with_condition_order_by($table_name, $field_name, $condition, $orderby='1=1', $ase='asc')
{
$sql	= "SELECT * FROM $table_name WHERE  deleteflag='active' and $field_name='".$condition."'  order by $orderby $ase ";
//echo $sql; 
//exit();
$result = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
return $result;
}

// Function for getting data with condition.	
function getData_with_condition($table_name, $field_name, $condition)
{
$sql	= "SELECT * FROM $table_name WHERE  deleteflag='active' and $field_name='".$condition."' ";
//echo $sql; 
//exit();
$result = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
return $result;
}

function getData_with_condition_kc($table_name, $field_name, $condition)
{
$sql	= "SELECT * FROM $table_name WHERE  kc_type='Folder' and deleteflag='active' and kc_status='active' and $field_name='".$condition."' order by sort_order DESC";
//echo $sql; 
//exit();
$result = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
return $result;
}	

function getData_with_condition_kc_view($table_name, $field_name, $condition)
{
$sql = "SELECT * FROM $table_name WHERE  deleteflag='active' and kc_status='active' and $field_name='".$condition."' order by sort_order DESC";
//echo $sql; 
//exit();
$result = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
return $result;
}	

function getData_with_condition_ten($table_name, $field_name, $condition)
{
$sql	= "SELECT * FROM $table_name WHERE tnd_del_status='active' and $field_name='".$condition."' ";
$sql; 
//exit();
$result = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
return $result;
}

function getData_with_new_condition($table_name, $field_name, $condition)
{
$sql	= "SELECT * FROM $table_name WHERE $field_name='".$condition."' ";
//echo $sql; 
//exit();
$result	= mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
return $result;
}

function set_singal_value($table_name, $field_name, $value , $condition)
{   
$sql	= "update $table_name set  $field_name='".$value."' ". $condition;
//echo $sql; 
//exit();
$result	= mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
return $result;
}

// Image upload function 
// destination name eg: ../foldername/filename
// File Name means file control name 
// Code name for image name 
function ImageUpload($destnation,$filename,$codeupname,$sizeWidth='0', $Givenheight='0')
{   
if ($_FILES[$filename]['name']!="")
{
$unique_id_query = strtoupper(substr(md5(uniqid(rand(), true)), 0 ,16));
$unique_add = $unique_id_query;
$unique_name = $codeupname.$unique_add ;
if($_FILES[$filename]["error"] > 0)
{
return -1;		// file error 
}
else
{
$img_type = explode(".",$_FILES[$filename]["name"]);
$imgtype  = strtoupper($img_type[1]);
$uploadedfile = $_FILES[$filename]['tmp_name'];
if($imgtype == 'JPG' || $imgtype == 'JPEG')
{
$src = imagecreatefromjpeg($uploadedfile);
}
else if($imgtype == 'PNG')
{
$src = imagecreatefrompng($uploadedfile);
}
else if($imgtype == 'GIF')
{
$src = imagecreatefromgif($uploadedfile);
}
else if($imgtype == 'BMP')
{
$src = imagecreatefromwbmp($uploadedfile);
}
list($width,$height)=getimagesize($uploadedfile);
$newwidth  = $sizeWidth;
if($Givenheight != '0')
{
$newwidth = $sizeWidth;
}
else
{
$newwidth = $width;
}
if($Givenheight != '0')
{
$newheight = $Givenheight;
}
else

{

$newheight = ($height/$width)*$newwidth;

}

$tmp=imagecreatetruecolor($newwidth,$newheight);

imagecopyresampled($tmp,$src,0,0,0,0,$newwidth,$newheight,$width,$height); 

$img_name   = $_FILES[$filename]["name"] ;

$img_name   = $img_name;

//			$img_name   = $unique_name.$img_name;

$image_path = $destnation. $img_name;

$IMGPATH 	= "../".$image_path ;

imagejpeg($tmp,$IMGPATH ,100);

imagedestroy($src);

imagedestroy($tmp); 

return $image_path; // returning image name and path 

}

}

else

{

return 0; // no file persent 

}

}

//**********************************************************************************/

function ImageUploadMulti($destnation,$filename,$codeupname, $i,$sizeWidth='0', $Givenheight='0')
{
if ($_FILES[$filename]['name'][$i] !="")
{
$unique_id_query = strtoupper(substr(md5(uniqid(rand(), true)), 0 ,16));
$unique_add = $unique_id_query;
$unique_name = $codeupname.$unique_add ;
if($_FILES[$filename]["error"][$i] > 0)
{
return -1;		// file error 
}
else
{
$img_type = explode(".",$_FILES[$filename]["name"][$i]);
$imgtype  = strtoupper($img_type[1]);
$uploadedfile = $_FILES[$filename]['tmp_name'][$i];
if($imgtype == 'JPG' || $imgtype == 'JPEG')
{
$src = imagecreatefromjpeg($uploadedfile);
}
else if($imgtype == 'PNG')
{
$src = imagecreatefrompng($uploadedfile);
}
else if($imgtype == 'GIF')
{
$src = imagecreatefromgif($uploadedfile);
}
else if($imgtype == 'BMP')
{
$src = imagecreatefromwbmp($uploadedfile);
}
list($width,$height)=getimagesize($uploadedfile);
$newwidth  = $sizeWidth;
if($Givenheight != '0')
{
$newwidth = $sizeWidth;
}
else
{
$newwidth = $width;
}
if($Givenheight != '0')
{
$newheight = $Givenheight;
}
else
{
$newheight = ($height/$width)*$newwidth;
}
$tmp=imagecreatetruecolor($newwidth,$newheight);
imagecopyresampled($tmp,$src,0,0,0,0,$newwidth,$newheight,$width,$height); 
$img_name   = $_FILES[$filename]["name"][$i];
$img_name   = $img_name;
//			$img_name   = $unique_name.$img_name;
$image_path = $destnation. $img_name;
$IMGPATH 	= "../".$image_path ;
imagejpeg($tmp,$IMGPATH ,100);
imagedestroy($src);
imagedestroy($tmp); 
return $image_path; // returning image name and path 
}
}
else
{
return 0; // no file persent 
}
}
//*********************************************************************************//
// This function use to change the date formate
function getDateformate($date,$informate='dmy',$formate='ymd',$spliter='-',$symbolChange='-')
{   
//And changing the value like y-m-d and m-d-y
$dateArray  = explode($spliter,$date);
//print_r($dateArray);
//echo sizeof($dateArray);
if(sizeof($dateArray)==1) {
$dateArray  = explode("/",$date);
$changeDate = $dateArray[0].$symbolChange.$dateArray[1].$symbolChange.$dateArray[2];
return $changeDate;
}
if($informate == 'dmy')
{
if($formate == 'ymd')
{
$changeDate = $dateArray[2].$symbolChange.$dateArray[1].$symbolChange.$dateArray[0];
return $changeDate;
}
if($formate == 'mdy')
{
$changeDate = $dateArray[1].$symbolChange.$dateArray[0].$symbolChange.$dateArray[2];
return $changeDate;
}
if($formate == 'dmy')
{
$changeDate = $dateArray[0].$symbolChange.$dateArray[1].$symbolChange.$dateArray[2];
return $changeDate;
}
if($formate == 'ydm')
{
$changeDate = $dateArray[2].$symbolChange.$dateArray[0].$symbolChange.$dateArray[1];
return $changeDate;
}
}
if($informate == 'mdy')
{
if($formate == 'ymd')
{
$changeDate = $dateArray[2].$symbolChange.$dateArray[0].$symbolChange.$dateArray[1];
return $changeDate;
}
if($formate == 'mdy')
{
$changeDate = $dateArray[0].$symbolChange.$dateArray[1].$symbolChange.$dateArray[2];
return $changeDate;
}
if($formate == 'dmy')
{
$changeDate = $dateArray[1].$symbolChange.$dateArray[2].$symbolChange.$dateArray[0];
return $changeDate;
}
if($formate == 'mdy')
{
$changeDate = $dateArray[0].$symbolChange.$dateArray[2].$symbolChange.$dateArray[1];
return $changeDate;
}
}
if($informate == 'ymd')
{
if($formate == 'ymd')
{
$changeDate = $dateArray[0].$symbolChange.$dateArray[1].$symbolChange.$dateArray[2];
return $changeDate;
}
if($formate == 'mdy')
{
$changeDate = $dateArray[1].$symbolChange.$dateArray[2].$symbolChange.$dateArray[0];
return $changeDate;
}

if($formate == 'dmy')
{
$changeDate = $dateArray[2].$symbolChange.$dateArray[1].$symbolChange.$dateArray[0];
return $changeDate;
}
if($formate == 'ydm')
{
$changeDate = $dateArray[2].$symbolChange.$dateArray[0].$symbolChange.$dateArray[1];
return $changeDate;
}
}
}
////////////////////////////////////////////////////////////////////////////////////////////////////
function dateSub($days)
{
$year  = date('Y');  
$month = date('m');
$date  = date('d');
$time = date('Y-m-d', @mktime(0, 0, 0, @$month, $date-$days, $year));
return $time;
}

function dateSub1($days)
{
$year  = date('Y');  
$month = date('m');
$date  = date('d');
$time = date('m/d/Y', mktime(0, 0, 0, $month, $date-$days, $year));
return $time;
}

function dateSub2($days)
{
$year  = date('Y');  
$month = date('m');
$date  = date('d');
$time = date('Y-m-d', mktime(0, 0, 0, $month, $date-$days, $year));
return $time;
}

function dateSub_exact_date($date,$days)
{
$date=explode('-',$date);
$year  = $date[0];  
$month = $date[1];
$date  = $date[2];
$time = date('m/d/Y', @mktime(0, 0, 0, $date+$days, $month, $year));
//return $date_formate=date("d/M/Y", strtotime($date));
echo date('d/M/Y', strtotime($Date. ' + '. $days.' day'));
//return $time;
}
//************************************************************************************//	
// code for takeing the database backup 

function backup_tables($tables = '*')
{
//get all of the tables list
if($tables == '*')
{
$tables = array();
$result = mysqli_query($GLOBALS["___mysqli_ston"],  'SHOW TABLES');
while($row = mysqli_fetch_row($result))
{
$tables[] = $row[0];
}
}
else
{
$tables = is_array($tables) ? $tables : explode(',',$tables);
}
//cycle through
foreach($tables as $table)
{
$result = mysqli_query($GLOBALS["___mysqli_ston"],  'SELECT * FROM '.$table);
$num_fields = mysqli_num_fields($result);
$row2 = mysqli_fetch_row(mysqli_query($GLOBALS["___mysqli_ston"],  'SHOW CREATE TABLE '.$table));
$return.= "\n\n".$row2[1].";\n\n";
for ($i = 0; $i < $num_fields; $i++) 
{
while($row = mysqli_fetch_row($result))
{
$return.= 'INSERT INTO '.$table.' VALUES(';
for($j=0; $j<$num_fields; $j++) 
{
$row[$j] = addslashes($row[$j]);
$row[$j] = ereg_replace("\n","\\n",$row[$j]);
if (isset($row[$j])) { $return.= '"'.$row[$j].'"' ; } else { $return.= '""'; }
if ($j<($num_fields-1)) { $return.= ','; }
}
$return.= ");\n";
}
}
$return.="\n\n\n";
}
//save file
$filePath = "../uploads/File/database_backup/";
$fileName = ('db-backup-'.time().'-'.(md5(implode(',',$tables))).'.sql');
$file	  = $filePath.$fileName;
$handle   = fopen($file,'w+');
$sucess	  = fwrite($handle,$return);
$fileSize = filesize($filePath.$fileName)/1024;
$fileSize = $fileSize;
if ($sucess)
{ 
// entering the basic details of a file in the table
$sql 	= "insert into tbl_datadase_backup (db_filename,db_file_path,db_file_size) values ('$fileName','$filePath','$fileSize')";
$result = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
}
else
{ 
return 0;
}	
fclose($handle);
}
//*******************************************************************************//
// Download Code
function output_file($file, $name, $mime_type='')
{
/*This function takes a path to a file to output ($file), 
the filename that the browser will see ($name) and 
the MIME type of the file ($mime_type, optional).
If you want to do something on download abort/finish,
register_shutdown_function('function_name');
*/
if(!is_readable($file)) die('File not found or inaccessible!');
$size = filesize($file);
$name = rawurldecode($name);
/* Figure out the MIME type (if not specified) */
$known_mime_types=array(
"pdf" => "application/pdf",
"sql" => "text/plain",
"txt" => "text/plain",
"html" => "text/html",
"htm" => "text/html",
"exe" => "application/octet-stream",
"zip" => "application/zip",
"doc" => "application/msword",
"xls" => "application/vnd.ms-excel",
"ppt" => "application/vnd.ms-powerpoint",
"gif" => "image/gif",
"png" => "image/png",
"jpeg"=> "image/jpg",
"jpg" =>  "image/jpg",
"php" => "text/plain"
);
if($mime_type=='')
{
$file_extension = strtolower(substr(strrchr($file,"."),1));
if(array_key_exists($file_extension, $known_mime_types))
{
$mime_type=$known_mime_types[$file_extension];
} 
else 
{
$mime_type="application/force-download";
}
}
@ob_end_clean(); //turn off output buffering to decrease cpu usage
// required for IE, otherwise Content-Disposition may be ignored
if(ini_get('zlib.output_compression'))
ini_set('zlib.output_compression', 'Off');
header('Content-Type: ' . $mime_type);
header('Content-Disposition: attachment; filename="'.$name.'"');
header("Content-Transfer-Encoding: binary");
header('Accept-Ranges: bytes');
/* The three lines below basically make the 
download non-cacheable */
header("Cache-control: private");
header('Pragma: private');
header("Expires: Mon, 5 Jun 2019 05:00:00 GMT");
// multipart-download and download resuming support
if(isset($_SERVER['HTTP_RANGE']))
{
list($a, $range) = explode("=",$_SERVER['HTTP_RANGE'],2);
list($range) = explode(",",$range,2);
list($range, $range_end) = explode("-", $range);
$range=intval($range);
if(!$range_end) {
$range_end=$size-1;
} else {
$range_end=intval($range_end);
}
$new_length = $range_end-$range+1;
header("HTTP/1.1 206 Partial Content");
header("Content-Length: $new_length");
header("Content-Range: bytes $range-$range_end/$size");
} else {
$new_length=$size;
header("Content-Length: ".$size);
}

/* output the file itself */
$chunksize = 3*(1024*1024); //you may want to change this
$bytes_send = 0;
if ($file = fopen($file, 'r'))
{
if(isset($_SERVER['HTTP_RANGE']))
fseek($file, $range);
while(!feof($file) && 
(!connection_aborted()) && 
($bytes_send<$new_length)
)
{
$buffer = fread($file, $chunksize);
print($buffer); //echo($buffer); // is also possible
flush();
$bytes_send += strlen($buffer);
}
fclose($file);
} else die('Error - can not open file.');
die();
}	

//*******************************************************************************//

// pagenation code function
function getData_withPages($table_name, $orderby='1=1', $ase='asc',$from='1=1', $max_results='5',$searchRecord='1=1' )
{
$sql	= "SELECT * FROM $table_name where deleteflag='active' and $searchRecord order by $orderby $ase LIMIT $from, $max_results"; 
//echo $sql;
//exit;
$result = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
return $result;
}//added this for DO manager

function getData_withPageswithoutdeleteflag($table_name, $orderby='1=1', $ase='asc',$from='1=1', $max_results='5',$searchRecord='1=1' ){	$sql	= "SELECT * FROM $table_name where 1 $searchRecord order by $orderby $ase LIMIT $from, $max_results"; 	//echo $sql;	//exit;
	$result = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
	return $result;
}

function getData_withPages_WL($table_name, $orderby='1=1', $ase='asc', $from='1=1', $max_results='10', $searchRecord='1=1',$star='*' )
{
$sql	= "SELECT $star FROM $table_name $searchRecord order by $orderby $ase";
//echo $sql;
//exit();
$result = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
return $result;
}

function getData_withPagesJoin($table_name, $orderby='1=1', $ase='asc', $from='1=1', $max_results='10', $searchRecord='1=1',$star='*' )
{
$sql	= "SELECT $star FROM $table_name $searchRecord order by $orderby $ase LIMIT $from, $max_results";
echo $sql;
//	exit();
$result = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
return $result;
}

function getData_withPagesJoinG($table_name, $orderby='1=1', $ase='asc', $group='1=1',  $from='1=1', $max_results='10', $searchRecord='1=1',$star='*' )
{
$sql	= "SELECT $star FROM $table_name $searchRecord group by $group  order by $orderby $ase LIMIT $from, $max_results";
$sql;
//exit();
$result = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
return $result;
}

// code for getting the no of pages  
function getTotal_pages($table_name, $orderby='1=1',$ase='asc',$max_results='1=1', $searchRecord='1=1')
{
// $max_results;
//	echo "SELECT COUNT(*) as Num FROM $table_name where $table_name.deleteflag='active'  $searchRecord  order by $orderby $ase";
//$total_results	= mysqli_result(mysqli_query($GLOBALS["___mysqli_ston"],  "SELECT COUNT(*) as Num FROM $table_name where $table_name.deleteflag='active' and $searchRecord  order by $orderby $ase"),0); 
$total_results_query	= "SELECT COUNT(*) as Num FROM $table_name where $table_name.deleteflag='active' and $searchRecord  order by $orderby $ase"; 
$res=mysqli_query($GLOBALS["___mysqli_ston"],$total_results_query);
$row=mysqli_fetch_object($res);
$total_results=$row->Num;
//exit;
$total_pages	= ceil($total_results / $max_results); 
return $total_pages;
}

function getTotal_pages_web_enq_manager($table_name, $orderby='1=1',$ase='asc',$max_results='1=1', $searchRecord='1=1')
{
// $max_results;
//echo "SELECT COUNT(*) as Num FROM $table_name where $table_name.deleteflag='active'  $searchRecord  order by $orderby $ase";
$total_results_query= "SELECT COUNT(*) as Num FROM $table_name where $table_name.deleteflag='active' $searchRecord  order by $orderby $ase"; 
//exit;
$res=mysqli_query($GLOBALS["___mysqli_ston"],$total_results_query);
$row=mysqli_fetch_object($res);
$total_results=$row->Num;
//exit;
$total_pages	= ceil($total_results / $max_results); 
return $total_pages;
/*$total_pages	= ceil($total_results / $max_results); 
return $total_pages;*/
}

//added by rumit on 04-09-2017 for DO manager
function getTotal_pageswithoutdeleteflag($table_name, $orderby='1=1',$ase='asc',$max_results='1=1', $searchRecord='1=1')
{
// $max_results;
//	echo "SELECT COUNT(*) as Num FROM $table_name where $table_name.deleteflag='active'  $searchRecord  order by $orderby $ase";
	$total_results_query	= "SELECT COUNT(*) as Num FROM $table_name where 1  $searchRecord  order by $orderby $ase"; 
	//exit;
	$res=mysqli_query($GLOBALS["___mysqli_ston"],$total_results_query);
$row=mysqli_fetch_object($res);
$total_results=$row->Num;
//exit;
$total_pages	= ceil($total_results / $max_results); 
return $total_pages;
}function getTotal_pages_mstock($table_name,$max_results)
{
$max_results;
//echo "SELECT COUNT(*) as Num FROM $table_name where deleteflag='active' order by pro_title asc";
$total_results_query	= "SELECT COUNT(*) as Num FROM $table_name where deleteflag='active' order by pro_title asc"; 
//exit;
$res=mysqli_query($GLOBALS["___mysqli_ston"],$total_results_query);
$row=mysqli_fetch_object($res);
$total_results=$row->Num;
//exit;
$total_pages	= ceil($total_results / $max_results); 
return $total_pages;
}

// code for getting the no of pages  
function getTotal_pagesJoin($table_name, $orderby='1=1',$ase='asc',$max_results='1=1', $searchRecord='1=1',$star='*')
{
$sql_total	   = "SELECT COUNT('$star') as Num FROM $table_name  $searchRecord  order by $orderby $ase";
//exit();
$res=mysqli_query($GLOBALS["___mysqli_ston"],$sql_total);
$row=mysqli_fetch_object($res);
$total_results=$row->Num;
$total_pages = ceil($total_results / $max_results); 
return $total_pages;
}

// account manager name // Admin-Emp name 

function account_manager_name($ID)
{
$sql = "select admin_fname from tbl_admin where admin_id = '$ID' and deleteflag = 'active' and admin_status='active'";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
$admin_fname	  = $row->admin_fname;
return $admin_fname;
}

function Get_CUSDUEDATE($ID) {
$sql = "select PO_Due_Date from tbl_delivery_order where O_Id = '$ID'";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
$PO_Due_Date	  = $row->PO_Due_Date;
return $PO_Due_Date;
}

function Get_POTOTALVALUE($ID) {
$sql = "select Price,Quantity from tbl_do_products where OID = '$ID'";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
while($row = mysqli_fetch_object($rs)) {
$totalAmt	  += ($row->Quantity*$row->Price);	
}
return $totalAmt;
}

function Get_VPOTOTALVALUE($ID) {
$sql = "select Prodcut_Qty,Prodcut_Price from vendor_po_final where PO_ID = '$ID'";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
while($row = mysqli_fetch_object($rs)) {
$totalAmt	  += ($row->Prodcut_Qty*$row->Prodcut_Price);	
}
$totalAmt = $totalAmt + ($totalAmt*2/100);
return $totalAmt;
}

function account_manager_name_full($ID)
{
$sql = "select admin_fname,admin_lname from tbl_admin where admin_id = '$ID' and deleteflag = 'active' and admin_status='active'";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
$admin_fname	  = $row->admin_fname.' '.$row->admin_lname;
return $admin_fname;
}
// account manager email// Admin-Emp email 

function account_manager_email($ID)
{
$sql = "select admin_email from tbl_admin where admin_id = '$ID' and deleteflag = 'active' and admin_status='active'";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
$admin_email	  = $row->admin_email;
return $admin_email;
}

function account_manager_phone($ID)
{
$sql = "select admin_telephone from tbl_admin where admin_id = '$ID' and deleteflag = 'active' and admin_status='active'";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
$admin_telephone	  = $row->admin_telephone;
return $admin_telephone;
}

function account_manager_TL_email($ID)
{
$sql = "select admin_email from tbl_admin where admin_team = '$ID' and admin_role_id='17'  and deleteflag = 'active' and admin_status='active'";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
$admin_email	  = $row->admin_email;
return $admin_email;
}function account_manager_teamlead($ID)
{
$sql = "select admin_team from tbl_admin where admin_id = '$ID' and deleteflag = 'active' and admin_status='active'";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
$admin_team	  = $row->admin_team;
return $admin_team;
}
//team_name

function teamname($ID)
{
$sql = "select team_name from tbl_team where team_id = '$ID' and deleteflag = 'active' and team_status='active'";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
$team_name	  = $row->team_name;
return $team_name;
}

function team_abbrv($ID)
{
$sql = "select team_abbrv from tbl_team where team_id = '$ID' and deleteflag = 'active' and team_status='active'";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
$team_abbrv	  = $row->team_abbrv;
return $team_abbrv;
}

function cat_abrv($ID)
{
$sql = "select cat_abrv from tbl_application where application_id = '$ID' and deleteflag = 'active' and application_status='active'";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
if($row->cat_abrv!='0')
{
$cat_abrv	  = $row->cat_abrv;
}
else
{
$cat_abrv	  = "";
}
return $cat_abrv;
}

function cat_abrv_app_cat_ids($lead_app_cat_id_pro_ids_concat)
{
$sql = "select GROUP_CONCAT( cat_abrv ) AS cat_abrvs  from tbl_application where application_id IN($lead_app_cat_id_pro_ids_concat) ";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = @mysqli_fetch_object($rs);
$cat_abrv	  = str_replace( ',', '', $row->cat_abrvs );
return $cat_abrv;
}

function pro_code_offer($ID)
{
$sql = "select pro_code_offer from tbl_products where pro_id = '$ID' and deleteflag = 'active' and status='active'";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
if($row->pro_code_offer!='0')
{
$pro_code_offer	  = $row->pro_code_offer;
}
else
{
$pro_code_offer	  = "";
}
return $pro_code_offer;
}

function pro_code_offer_all($ID)
{
$sql = "select pro_id from tbl_order_product where order_id = '$ID' and deleteflag = 'active'  order by order_pros_id desc";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
while($row = mysqli_fetch_object($rs))
{
$pro_code_offer=$this->pro_code_offer($row->pro_id);
if($pro_code_offer!='0' || $pro_code_offer!='')
{
$pro_code_offer_all=$pro_code_offer;
}
}
@$pro_code_offer_all	  = pro_code_offer_all;
return $pro_code_offer;
}
/**************************Offer revision********************/
function offer_revised_count($ID)
{
$sql = "SELECT count( order_id ) as offer_revised_count FROM `tbl_offer_revised` WHERE order_id = '$ID' and deleteflag = 'active' and offer_revised_status='active'";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row_num = mysqli_num_rows($rs);
$row = mysqli_fetch_object($rs);
if($row->offer_revised_count=='0')
{
$offer_revised_count	  = "";
}
else
{
$offer_revised_count	  = "R".$row->offer_revised_count;
}
return $offer_revised_count;
}
// account manager name // Admin-Emp name 

function app_cat_name($ID)
{
$sql = "select cate_name from tbl_category where cate_id = '$ID' and deleteflag = 'active' and cate_status='active'";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
$cate_name	  = $row->cate_name;
return $cate_name;
}

function application_name($ID)
{
$sql = "select application_name from tbl_application where application_id = '$ID' and deleteflag = 'active' and application_status='active'";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
$application_name	  = $row->application_name;
return $application_name;
}

function lead_app_cat_id($leadid)
{
$sql = "select app_cat_id from tbl_lead where id = '$leadid' and deleteflag = 'active' and status='active'";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
$app_cat_id	  = $row->app_cat_id;
return $app_cat_id;
}

function pro_app_cat_id($prod_ids_concat)
{
$sql = "SELECT GROUP_CONCAT( app_cat_id ) AS app_cat_ids FROM `tbl_products_entry` WHERE `pro_id` IN ($prod_ids_concat)";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = @mysqli_fetch_object($rs);
$app_cat_id	  = implode(',', array_unique(explode(',', $row->app_cat_ids)));
return $app_cat_id;

}

function lead_ref_source($leadid)
{
$sql = "select ref_source from tbl_lead where id = '$leadid' and deleteflag = 'active' and status='active'";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
$ref_source	  = $row->ref_source;
return $ref_source;
}

function prod_ids_concat($orderid)
{
//$sql = "select ref_source from tbl_lead where id = '$leadid' and deleteflag = 'active' and status='active'";
$sql="SELECT GROUP_CONCAT( pro_id ) AS pro_ids FROM `tbl_order_product` WHERE order_id =$orderid";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
$prod_ids_concat	  = $row->pro_ids;
return $prod_ids_concat;
}

function lead_cust_segment($leadid)
{
$sql = "select cust_segment from tbl_lead where id = '$leadid' ";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
$cust_segment	  = $row->cust_segment;
return $cust_segment;
}

function lead_cust_segment_name($cust_segment_id)
{
if(!is_numeric($cust_segment_id))
{
$sql = "select cust_segment_name from tbl_cust_segment where cust_segment_description = '$cust_segment_id' and deleteflag = 'active' and cust_segment_status='active'";
} 
else
{
$sql = "select cust_segment_name from tbl_cust_segment where cust_segment_id = '$cust_segment_id' and deleteflag = 'active' and cust_segment_status='active'";
}
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],$sql);
$row = mysqli_fetch_object($rs);
$cust_segment_name	  = $row->cust_segment_name;
/*	if($cust_segment_name=='')

{

echo $cust_segment_name="N/A";

}*/

return $cust_segment_name;
}

function enq_source_abbrv($ref_source)
{
if(!is_numeric($ref_source))
{
$sql = "select enq_source_abbrv from tbl_enq_source where enq_source_description = '$ref_source' and deleteflag = 'active' and enq_source_status='active'";
}
else
{
$sql= "select enq_source_abbrv from tbl_enq_source where enq_source_id = '$ref_source' and deleteflag = 'active' and enq_source_status='active'";
}
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
if($row->enq_source_abbrv!='0')
{
$enq_source_abbrv	  = $row->enq_source_abbrv;
}
else
{
$enq_source_abbrv	  = "";
}
return $enq_source_abbrv;
}

function enq_source_name($ref_source)
{
if(!is_numeric($ref_source))
{
$sql = "select enq_source_name from tbl_enq_source where enq_source_description = '$ref_source' and deleteflag = 'active' and enq_source_status='active'";
}
else
{
$sql = "select enq_source_name from tbl_enq_source where enq_source_id = '$ref_source' and deleteflag = 'active' and enq_source_status='active'";
}
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
$enq_source_name	  = $row->enq_source_name;
/*	if($enq_source_name=='')
{
$enq_source_name="N/A";
}*/
return $enq_source_name;
}

function product_entry_desc($ID)
{
$sql = "select pro_desc_entry from tbl_application where application_id = '$ID' and deleteflag = 'active' and application_status='active'";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
$pro_desc_entry	  = $row->pro_desc_entry;
return $pro_desc_entry;
}

//added on 19thjuly 2019 to get product name/dec by model id
function product_entry_desc_bymodel($model_no)
{
$sql = "select pro_id from tbl_products_entry where model_no = '$model_no' and deleteflag = 'active' and status='active'";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
$pro_id	  = $row->pro_id;
return $pro_id;
}
//added on 22july 2019 to get product name/dec by model id custom name for customer used in delivery challan

function customer_pro_desc_title($model_no,$order_id)
{
$sql = "select Description from tbl_do_products where ItemCode = '$model_no' and OID ='$order_id' ";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
$customer_pro_desc_title	  = $row->Description;
return $customer_pro_desc_title;
}
// Company name // COMP & LEAD MANAGER name 
function company_name($ID)
{
if(is_numeric($ID))
	{
		$sqlCountry = "select comp_name,co_extn_id, co_extn, office_type,co_division,co_city from tbl_comp where id = '$ID' and deleteflag = 'active'";
		$rsCountry  	= mysqli_query($GLOBALS["___mysqli_ston"], $sqlCountry);
		$rowCountry 	= mysqli_fetch_object($rsCountry);
		$co_extn_id		= $rowCountry->co_extn_id;
		$office_type	= $rowCountry->office_type;
		$co_division	= $rowCountry->co_division;
		$co_city		= $rowCountry->co_city;
if($co_extn_id!='0' && $co_extn_id!='' && $co_extn_id!='6')		
{
	$co_extn_id_name=$this->company_extn_name($co_extn_id);
}
/*if($co_division!='0' && $co_division!='')		
{
	$co_division_name=" / ".$rowCountry->co_division;
}
else
{
	$co_division_name="";
}
if($co_city!='0' && $co_city!='')		
{
	$co_city_name=" / ".$rowCountry->co_city;
}
else
{
	$co_city_name="";
}*/
$co_division_name="";
$co_city_name="";
				$comp_name1=str_replace('Pvt Ltd', "", $rowCountry->comp_name);
    			$comp_name2=str_replace('PVT. LTD.', "", $comp_name1);
             	$comp_name3=str_replace('Limited', "", $comp_name2);
             	$comp_name4=str_replace('Ltd', "", $comp_name3);
				$comp_name5=str_replace('Pvt ltd', "", $comp_name4);
				$comp_name6=str_replace('LLP', "", $comp_name5);
				$comp_name7=str_replace('Pvt. .', "", $comp_name6);
				$comp_name8=str_replace('Pvt.', "", $comp_name7);		
				$comp_name9=str_replace('PVT LIMITED', "", $comp_name8);		
				$comp_name10=str_replace('pvt ltd', "", $comp_name9);		
				$comp_name11=str_replace('LTD', "", $comp_name10);		
				$comp_name12=str_replace('PVT.', "", $comp_name11);		
				$comp_name13=str_replace('limited', "", $comp_name12);		
				$comp_name14=str_replace('Private', "", $comp_name13);		
				$comp_name15=str_replace('PVT ', "", $comp_name14);		
				$comp_name16=str_replace('(P)', "", $comp_name15);		
				$comp_name17=str_replace('PRIVATE LIMITED', "", $comp_name16);
				$comp_name18=str_replace('LIMITED...', "", $comp_name17);		
				$comp_name19=str_replace('LIMITED', "", $comp_name18);		

$comp_name	= $comp_name19." ".$co_extn_id_name."".$co_division_name."".$co_city_name;
	}
	return ucfirst($comp_name);

}

function company_extn_name($ID)
{
$sql = "select company_extn_name from tbl_company_extn where company_extn_id = '$ID' and deleteflag = 'active' and company_extn_status='active'";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
$company_extn_name	  = $row->company_extn_name;
return $company_extn_name;

}// this function is return the company name

function company_name_return($IdValue)
{
	if(is_numeric($IdValue))
	{
		$sqlCountry = "select comp_name,co_extn_id, co_extn, office_type,co_division,co_city from tbl_comp where id = '$IdValue' and deleteflag = 'active'";
		$rsCountry  	= mysqli_query($GLOBALS["___mysqli_ston"],  $sqlCountry);
		$rowCountry 	= mysqli_fetch_object($rsCountry);
		$co_extn_id		= $rowCountry->co_extn_id;
		$office_type	= $rowCountry->office_type;
		$co_division	= $rowCountry->co_division;
		$co_city		= $rowCountry->co_city;
if($co_extn_id!='0' && $co_extn_id!='6' && $co_extn_id!='')		
{
	$co_extn_id_name=$this->company_extn_name($co_extn_id);
}
/*if($co_division!='0' && $co_division!='')		
{
	$co_division_name=" / ".$rowCountry->co_division;
}
else
{
	$co_division_name="";
}
if($co_city!='0' && $co_city!='')		
{
	$co_city_name=" / ".$rowCountry->co_city;
}
else
{
	$co_city_name="";
}*/
$co_division_name="";
$co_city_name="";
				$comp_name1=str_replace('Pvt Ltd', "", $rowCountry->comp_name);
    			$comp_name2=str_replace('PVT. LTD.', "", $comp_name1);
             	$comp_name3=str_replace('Limited', "", $comp_name2);
             	$comp_name4=str_replace('Ltd', "", $comp_name3);
				$comp_name5=str_replace('Pvt ltd', "", $comp_name4);
				$comp_name6=str_replace('LLP', "", $comp_name5);
				$comp_name7=str_replace('Pvt. .', "", $comp_name6);
				$comp_name8=str_replace('Pvt.', "", $comp_name7);		
				$comp_name9=str_replace('PVT LIMITED', "", $comp_name8);		
				$comp_name10=str_replace('pvt ltd', "", $comp_name9);		
				$comp_name11=str_replace('LTD', "", $comp_name10);		
				$comp_name12=str_replace('PVT.', "", $comp_name11);		
				$comp_name13=str_replace('limited', "", $comp_name12);		
				$comp_name14=str_replace('Private', "", $comp_name13);		
				$comp_name15=str_replace('PVT ', "", $comp_name14);		
				$comp_name16=str_replace('(P)', "", $comp_name15);		
				$comp_name17=str_replace('PRIVATE LIMITED', "", $comp_name16);
				$comp_name18=str_replace('LIMITED...', "", $comp_name17);		
				$comp_name19=str_replace('LIMITED', "", $comp_name18);		
				$comp_name	= $comp_name19." ".$co_extn_id_name."".$co_division_name."".$co_city_name;
	}
	return ucfirst($comp_name);
}

function company_div_name($ID)
{
$sql = "select co_division from tbl_comp where id = '$ID' and deleteflag = 'active' and status='active'";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
$co_division	  = $row->co_division;
return $co_division;
}
// Company lead count // used in COMP MANAGER  21-5-2019

function lead_count_comp($ID)
{   
$sql = "SELECT count(id) as lead_generated_count FROM `tbl_lead` where comp_name=$ID ";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
$lead_generated_count	  = $row->lead_generated_count;
return $lead_generated_count;
}

function sub_co_count($ID)
{   
$sql = "SELECT count(id) as sub_co_count FROM `tbl_comp` WHERE `parent_id` =$ID ";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
$sub_co_count	  = $row->sub_co_count;
return $sub_co_count;
}
//this function created by rumit om dated 7jan2017

function product_category_name($ID)
{
$sql = "select application_name from tbl_application where application_id = '$ID' and deleteflag = 'active' and application_status='active'";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
$application_name	  = $row->application_name;
return $application_name;
}

function vendor_name($ID)
{
$sql = "select C_Name from vendor_master where ID = '$ID' and deleteflag = 'active' and status='active'";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
$comp_name	  = $row->C_Name;
return $comp_name;
}//added on 25-mar-2021
function vendor_email($ID)
{
$sql = "select Email from vendor_master where ID = '$ID' and deleteflag = 'active' and status='active'";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
$Email	  = $row->Email;
return $Email;
}function tel_number($ID)
{
$sql = "select telephone from tbl_comp where id = '$ID'";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
$comp_name	  = $row->telephone;
return $comp_name;

}

function tbl_order_all($ID)
{
$sql = "select * from tbl_order where orders_id='".$ID."'";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
//$comp_name	  = $row->comp_name;
return $row;

}

function tbl_order_product_all($ID)
{   
$sql = "select pro_name from tbl_order_product where barcode LIKE '%".$ID."%' limit 0,4";

$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);

//$comp_name	  = $row->comp_name;

while($row = mysqli_fetch_object($rs)) {

$pname .= $row->pro_name.", <br>";

}

return $pname;

}
//added on 18th july 2019 rumit to get serial no/barcode used in tbl_order_product

function serial_no_generated($ID)

{   

$sql = "select barcode from tbl_order_product where order_id = $ID";

$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);

$rsnum  = mysqli_num_rows($rs);

//$comp_name	  = $row->comp_name;

$b=0;

while($row = mysqli_fetch_object($rs)) {

	$b++;

	if($row->barcode!='' && $row->barcode!='0' )

	{

//echo "<br>".$barcode = $row->barcode;

echo "<b>(".$b.")</b> ".$barcode = $row->barcode."<br/>";

	}	

}

//return $barcode;

}
//added on 18th july 2019 rumit to get serial no/barcode used in tbl_order_product

function product_name_generated($ID)

{   

$sql = "select pro_name from tbl_order_product where order_id = $ID";

$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);

$rsnum  = mysqli_num_rows($rs);

//$comp_name	  = $row->comp_name;

	$i=0;

while($row = mysqli_fetch_object($rs)) {

	$i++;

	if($row->pro_name!='' && $row->pro_name!='0' )

	{

echo "<b>(".$i.")</b> ".$pro_name = $row->pro_name."<br/>";

	}

}

//return $barcode;

}
function product_qty_generated_for_excel($ID)

{   

$sql = "select pro_quantity from tbl_order_product where order_id = $ID";

$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);

$rsnum  = mysqli_num_rows($rs);

//$comp_name	  = $row->comp_name;

while($row = mysqli_fetch_object($rs)) {

	if($row->pro_quantity!='' && $row->pro_quantity!='0' )

	{

 $pro_quantity[] = $row->pro_quantity;

	}	

	//$barcode1 = implode(":",$barcode);

}

//echo "<pre>";

//	print_r($barcode);

//echo "count array:".count($barcode);

if(count($pro_quantity) != 0) 

{

	 $pro_quantity1=implode("\n",$pro_quantity);// to remove new line in excel press ctrl H then replace \n with ctrl+J added on 31-jan-2020

}

return $pro_quantity1;

}
function product_name_generated_for_excel($ID)

{   

$sql = "select pro_name from tbl_order_product where order_id = $ID";

$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);

$rsnum  = mysqli_num_rows($rs);

//$comp_name	  = $row->comp_name;

while($row = mysqli_fetch_object($rs)) {

	if($row->pro_name!='' && $row->pro_name!='0' )

	{

 $pro_name[] = $row->pro_name;

	}	

	//$barcode1 = implode(":",$barcode);

}
if(count($pro_name) != 0) 

{

 $pro_name1=implode("\n",$pro_name);// to remove new line in excel press ctrl H then replace \n with ctrl+J added on 31-jan-2020

}

return $pro_name1;

}
function serial_no_generated_for_excel($ID)

{   

$sql = "select barcode from tbl_order_product where order_id = $ID";

$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);

$rsnum  = mysqli_num_rows($rs);

//$comp_name	  = $row->comp_name;

while($row = mysqli_fetch_object($rs)) {

	if($row->barcode!='' && $row->barcode!='0' )

	{

 $barcode[] = $row->barcode;

	}	

	//$barcode1 = implode(":",$barcode);

}

//echo "<pre>";

//	print_r($barcode);

//echo "count array:".count($barcode);

if(count($barcode) != 0) 

{

 $barcode1=implode(", ",$barcode);

}

return $barcode1;

}
//added on 18th july 2019 rumit to get serial no/barcode used in tbl_order_product

function do_invoice_generated($ID)

{   

$sql = "select id from tble_invoice where o_id ='$ID'";

$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);

//$comp_name	  = $row->comp_name;

while($row = mysqli_fetch_object($rs)) {

$invoice_id = $row->id;

}

return $invoice_id;

}

//this function add n no. of days in current date 
function datePlus($days)

{

$year  = date('Y');  

$month = date('m');

$date  = date('d');

$dateCode[1] = 31 ;

$dateCode[3] = 31;

$dateCode[4] = 30;

$dateCode[5] = 31;

$dateCode[6] = 30;

$dateCode[7] = 31;

$dateCode[8] = 31;

$dateCode[9] = 30;

$dateCode[10] = 31;

$dateCode[11] = 30;

$dateCode[12] = 31;

$datetotal = $date + $days;

while($datetotal > $dateCode[$month])

{

$month++;

if($year % 4 == 0)

{

$dateCode[2] =29;

}

else

{

$dateCode[2] =28;

}

if($month > 12)

{

$month = 1;

$year  = $year + 1;

}

$datetotal = $datetotal - $dateCode[$month];

}

$completeDate = $year."-".$month."-".$datetotal;

return $completeDate;

}
/****************************************************************************************/

///////// This Function use to display Company Details

function CompanyName()

{

$display = $this->fetchGeneral_config('displayTitle');

if($display == 'name')

{

echo "<table border='0' cellpadding='0' cellspacing='0'>";

echo "<tr><td width='20'>&nbsp;</td><td>"; 

echo "<a href='index.php'>";

$store   = $this->fetchGeneral_config('store');

echo $store;

echo "</a>";

echo "</td></tr>";

echo "</table>";

}

else if($display == 'logo')

{

echo "<table border='0' cellpadding='0' cellspacing='0'>";

echo "<tr><td width='20'>&nbsp;</td><td>"; 

echo "<a href='index.php'>";

$logo	 = $this->fetchGeneral_config('StoreLogo');

echo "<img src='../$logo' onerror=\"style.display='none';\" border='0'/>";

echo "</a>";

echo "</td></tr>";

echo "</table>";
}

else if($display == 'both')
{
echo "<table border='0' cellpadding='0' cellspacing='0'>";
echo "<tr><td width='20'>&nbsp;</td>"; 
echo "<td><a href='index.php'>";
$logo	 = $this->fetchGeneral_config('StoreLogo');
echo "<img src='../$logo' onerror=\"style.display='none';\" border='0'/>";
echo "</a>";
echo "</td>";
echo "<td width='20'>";
echo "</td>";
echo "<td>";
echo "&nbsp; ";
echo "<a href='index.php'>";
$store   = $this->fetchGeneral_config('store');
echo $store;
echo "</a>";
echo "</td>";
echo "</tr>";
echo "</table>";
}
}
//**************************************************************************************/
// fetch data from general config table
function fetchGeneral_config($valueType)
{
// to get meta content ------------- meta_content
// to get meta description --------- meta_desc
// to get contact email ------------ contact
// to get website title ------------ webtitle
// to get the logo ----------------- StoreLogo
// to get the display title -------- displayTitle
// To Get Tax Location ------------- TaxLocation
//store name ----------------------- store
//admin email ---------------------- admin
//order email ---------------------- order
//customer support eamil ----------- support
//general contact ------------------ info
$sql_mail = "select * from tbl_general_configuraction where deleteflag='active'";
$rs_mail  =  mysqli_query($GLOBALS["___mysqli_ston"],  $sql_mail);
if(mysqli_num_rows($rs_mail)>0)
{
$row_mail = mysqli_fetch_object($rs_mail);
if($valueType == "webtitle")
{
return $row_mail->website_title;
}
else if($valueType == "siteurl")
{
return $row_mail->siteurl;
}
else if($valueType == "StoreLogo")
{
return $row_mail->store_logo;
}
else if($valueType == "displayTitle")
{
return $row_mail->display_title;
}
else if($valueType == "store")
{
return $row_mail->store_name;
}
else if($valueType == "admin")
{
return $row_mail->admin_email;
}
else if($valueType == "contact")
{
return $row_mail->contact_email;
}
else if($valueType == "support")
{
return $row_mail->customer_support__email;
}
else if($valueType == "info")
{
return $row_mail->gen_contact_email;
}
else if($valueType == "meta_content")
{
return $row_mail->meta_content;
}
else if($valueType == "meta_desc")
{
return $row_mail->meta_desc;
}
else if($valueType == "financial_year_start")
{
return $row_mail->financial_year_start;
}
else if($valueType == "alarm_1")
{
return $row_mail->alarm_1;
}
else if($valueType == "alarm_1_additional_email")
{
return $row_mail->alarm_1_additional_email;
}
else if($valueType == "alarm_2")
{
return $row_mail->alarm_2;

}
else if($valueType == "alarm_2_additional_email")
{
return $row_mail->alarm_2_additional_email;
}

else if($valueType == "alarm_3")
{
return $row_mail->alarm_3;
}
else if($valueType == "alarm_3_additional_email")
{
return $row_mail->alarm_3_additional_email;
}
else if($valueType == "pdf_key")
{
return $row_mail->pdf_key;

}

else
{
return "error";
}
}
else 
{return "error";
}	
}
/////////////////////////////////////////////////////////////////////////////////////////////
function fetchAlarm_config($valueType)
{
$sql_mail = "select * from tbl_alarm_email_configuration where deleteflag='active'";
$rs_mail  =  mysqli_query($GLOBALS["___mysqli_ston"],$sql_mail);
if(mysqli_num_rows($rs_mail)>0)
{
$row_mail = mysqli_fetch_object($rs_mail);
if($valueType == "admin")
{
return $row_mail->admin_email;
}

else if($valueType == "financial_year_start")
{
return $row_mail->financial_year_start;
}
else if($valueType == "alarm_1")
{
return $row_mail->alarm_1;
}
else if($valueType == "alarm_1_additional_email")
{
return $row_mail->alarm_1_additional_email;
}
else if($valueType == "alarm_2")
{
return $row_mail->alarm_2;
}
else if($valueType == "alarm_2_additional_email")
{
return $row_mail->alarm_2_additional_email;
}
else if($valueType == "alarm_3")
{
return $row_mail->alarm_3;
}
else if($valueType == "alarm_3_additional_email")
{
return $row_mail->alarm_3_additional_email;
}
else if($valueType == "alarm_enq_not_assigned")
{
return $row_mail->alarm_enq_not_assigned;
}
else if($valueType == "enq_not_assigned_alarm_email")
{
return $row_mail->enq_not_assigned_alarm_email;
}

else if($valueType == "enq_not_assigned_alarm_email2")
{
return $row_mail->enq_not_assigned_alarm_email2;
}else if($valueType == "sales_head_email")
{
return $row_mail->sales_head_email;
}
else if($valueType == "inventory_report_email")
{
return $row_mail->inventory_report_email;
}
else if($valueType == "inventory_report_email2")
{
return $row_mail->inventory_report_email2;
}
else if($valueType == "incoming_stock_report_email")
{
return $row_mail->incoming_stock_report_email;
}
else if($valueType == "incoming_stock_report_email2")
{
return $row_mail->incoming_stock_report_email2;
}
else if($valueType == "outgoing_stock_report_email")
{
return $row_mail->outgoing_stock_report_email;
}
else if($valueType == "outgoing_stock_report_email2")
{
return $row_mail->outgoing_stock_report_email2;
}
else

{

return "error";

}

}

else 

{return "error";

}	

}// This function return State Name

function StateName($STvalue)
{
if(is_numeric($STvalue))
{
$sqlState = "select * from tbl_zones where zone_id = '$STvalue' and deleteflag = 'active'";
$rsState  = mysqli_query($GLOBALS["___mysqli_ston"],  $sqlState);
$rowState = mysqli_fetch_object($rsState);
$state	  = $rowState->zone_name;
}
else
{
$state = $STvalue;
}
return ucfirst($state);
}

/**************************/
function ModeName($STvalue)
{
if(is_numeric($STvalue))
{
$sqlMode = "select * from tbl_mode_master where mode_id = '$STvalue' and deleteflag = 'active'";
$rsMode  = mysqli_query($GLOBALS["___mysqli_ston"],  $sqlMode);
$rowMode = mysqli_fetch_object($rsMode);
$Mode	  = $rowMode->mode_name;
}
else
{
$Mode = $STvalue;
}
return ucfirst($Mode);

}

/*******courier co name************/
function couriercoName($STvalue)
{
if(is_numeric($STvalue))
{
$sqlcourier = "select * from tbl_courier_master where courier_id = '$STvalue' and deleteflag = 'active'";
$rscourier  = mysqli_query($GLOBALS["___mysqli_ston"],  $sqlcourier);
$rowcourier = mysqli_fetch_object($rscourier);
$courier	  = $rowcourier->courier_name;
}
else
{
$courier = $STvalue;
}
return ucfirst($courier);
}
// This function return City Name added on 15th july 2019 
function CityName($STvalue)
{
if(is_numeric($STvalue))
{
$sqlcity = "select city_name from all_cities where city_id = '$STvalue' and deleteflag = 'active'";
$rscity  = mysqli_query($GLOBALS["___mysqli_ston"],  $sqlcity);
$rowcity = mysqli_fetch_object($rscity);
$city	  = $rowcity->city_name;
}
else
{
$city = $STvalue;
}
return ucfirst($city);
}

/////////////////////////////////////////////////////////////////////////////////////////////

// this function is return the country name
function CountryName($IdValue)
{
if(is_numeric($IdValue))
{
$sqlCountry = "select * from tbl_country where country_id = '$IdValue' and deleteflag = 'active'";
$rsCountry  = mysqli_query($GLOBALS["___mysqli_ston"],  $sqlCountry);
$rowCountry = mysqli_fetch_object($rsCountry);
$country	= $rowCountry->country_name;
}
else
{
$country 	= $IdValue;
}
return ucfirst($country);
}
/////////////////////////////////////////////////////////////////////////////////////////////
/*function get_po_tot($O_Id,$Tax_Per,$Tax_Stat)
{
}*/

function get_pro_id_entry_by_pro_id($id)
{
$sqlproidentry = "SELECT pro_id_entry FROM `tbl_products_entry` WHERE `pro_id` = '$id' ";
$rsproidentry  = mysqli_query($GLOBALS["___mysqli_ston"],$sqlproidentry);
$rowproidentry = mysqli_fetch_object($rsproidentry);
$pro_id_entry	= $rowproidentry->pro_id_entry;
return ucfirst($pro_id_entry);
}

function proidentrydesc($id)
{
$sqlproidentry = "select pro_desc_entry from tbl_products_entry where pro_id_entry = '$id' and deleteflag = 'active' and status='active'";
//exit;
$rsproidentry  = mysqli_query($GLOBALS["___mysqli_ston"],  $sqlproidentry);
$rowproidentry = mysqli_fetch_object($rsproidentry);
$pro_desc_entry	= $rowproidentry->pro_desc_entry;
$pro_desc_entry 	= $pro_desc_entry;
return ucfirst($pro_desc_entry);
}
////////////////// This function use to move location
function pageLocation($filename)
{
echo "<SCRIPT LANGUAGE='JavaScript'>window.location='$filename'</SCRIPT>"; 
}
/////////////////////////// This function use to export data in to .xls file 
function ExcelExpo($query)
{
include("conn_db_connection.php");
$excel=new Sql2Excel($dataServer,$dataUser,$dataPassword,$dataDBName);
$excel->ExcelOutput($query);
}

// Function for getting data with condition.	
function selectWhere($table, $query, $star = '*')
{
$sql = "select $star from $table where  $query";
//echo "=============== <Br>";
//echo $sql;
//exit(); 
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
return $rs;
}

function selectWhereJOIN($join)
{
$sql = "select $join";
echo "=============== <Br>";
echo $sql;
//exit(); 
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
return $rs;
}

/////////// This is the Function For CMS Menu //////////////////////////////////////////////
/*
$masterID = 0, CMS page ID
$style =  H, V
This is div ID based in split level
*/

function CmsMenu($masterID = 0, $style = 'H', $divID = '')
{
$sql = "select * from tbl_cms_pages where deleteflag = 'active' and page_status = 'active' and menu_id = $masterID";
$rs	 = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
if(mysqli_num_rows($rs) > 0)
{
while($row = mysqli_fetch_object($rs))
{
if($masterID == 0)
{
echo "<a href='cmspage.php?masterId=$row->pages_id&pageID=$row->pages_id'>";
echo strtolower($row->pages_name);
echo "</a>";
}
else
{
echo "<a href='cmspage.php?masterId=$masterID&pageID=$row->pages_id'>";
echo strtolower($row->pages_name);
echo "</a>";
}
}
}

}

/////////// This is the Function For CMS Content //////////////////////////////////////////////

/*

$pageID = CMS page ID

$field  = value from field.

values are :->

1 - page_name for page name

2 - page_title for page title

3 - meta_content for meta content

4 - meta_desc for meta description

5 - sort_desc for short description

6 - pages_html_text for html full page content

7

*/
function CmsContent($pageID, $field)
{
$sql = "select * from tbl_cms_pages where deleteflag = 'active' and page_status = 'active' and pages_id = $pageID";
$rs	 = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
if(mysqli_num_rows($rs) > 0)
{
$row = mysqli_fetch_object($rs);
return $row->$field;
}
}
////////////////////////////////////////////////////////////////////////////////////////////
function getResult($sql) {
global $conDataLinkString;
$result=mysqli_query($GLOBALS["___mysqli_ston"],  $sql) or die(mysqli_error()); 
return $result;
}
function getCountRow($result) {
$numRow=mysqli_num_rows($result);
return $numRow;
}
function getNextRow($result) {
$row=mysqli_fetch_assoc($result);
return $row;
}

function getRecord($result) {
$row=mysqli_fetch_array($result);
return $row;
}

function getRecordObj($result) {
$row=mysqli_fetch_object($result);
return $row;
}

////////////////////////////////////////////////////////////////////////////////////////////
function User_Information($pcode)
{
$sql = "select * from tbl_order where deleteflag='active' and order_id = '$pcode'";
$rs	 = mysqli_query($GLOBALS["___mysqli_ston"],  $sql); 
if(mysqli_num_rows($rs)>0)
{
$row  = mysqli_fetch_object($rs);
$name = ucfirst($row->cust_fname).' '.ucfirst($row->cust_lname);
echo "<table width='100%' border='0' cellpadding='0' cellspacing='0' class='tblBorder'>";
echo "<tr class='pagehead'> <td colspan='3' class='pad'>";
//echo $row->order_type;
echo "User Information</td> </tr>";
echo "<tr class='text'> <td width='15%' class='pad'>Client Name </td><td width='2%'>: &nbsp;</td><td> $name </td></tr>";
echo "<tr class='text'><td class='pad'>Client Email</td><td>:&nbsp;</td><td width='82%'>".$this->getDateformate($row->order_date,'ymd','mdy','-')."</td></tr>";
echo "<tr class='text'> <td class='pad'>Address </td><td>: &nbsp;</td> <td >$row->cust_address </td></tr>";
echo "<tr class='text'> <td class='pad'>City </td><td>: &nbsp;</td> <td>$row->cust_city </td></tr>";
echo "<tr class='text'> <td class='pad'>State </td><td>: &nbsp;</td> <td >".$this->StateName($row->cust_state)."</td></tr>";
echo "<tr class='text'> <td class='pad'>Zip </td><td>: &nbsp;</td> <td >".$row->cust_zip."</td></tr>";
echo "<tr class='text'> <td class='pad'>Phone </td><td>: &nbsp;</td> <td >".$row->cust_phone."</td></tr>";
echo "<tr class='text'> <td class='pad'>Fax </td><td>: &nbsp;</td> <td >".$row->cust_fax."</td></tr>";		
echo "</table>";
}
}
////////////////////////////////////////////////////////////////////////////////////////////
// this function gives currency symbol
function currencySymbol($where)
{
if($where == 0)
{
$currency_sql = "select currency_symbol from tbl_currencies where  deleteflag='active' and currency_super_default='yes' ";
$currency_rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $currency_sql); 
if(mysqli_num_rows($currency_rs)>0)
{
$currency_row = mysqli_fetch_object($currency_rs);
$symbol =  $currency_row->currency_symbol;
return $symbol;
}
}
else if($where == 1)
{
$currency_sql = "select currency_symbol,currency_value from tbl_currencies where deleteflag='active' and currency_status='yes'";
$currency_rs = mysqli_query($GLOBALS["___mysqli_ston"],  $currency_sql);
if(mysqli_num_rows($currency_rs)>0)
{
$currency_row = mysqli_fetch_object($currency_rs);
$symbol[0] = $currency_row->currency_symbol;
$symbol[1] = $currency_row->currency_value;
return $symbol;
}
}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

function sendmail($to, $from, $subject, $file_name)
{
$from_to 	  = str_replace(".com",".in",$from); //exit;//$s->fetchGeneral_config("info");	
$headers	= "MIME-Version: 1.0\r\n".
"Content-type: text/html; charset=iso-8859-1\r\n".
"From: ".$from_to."\r\n".
"Reply-To: ".$from."\r\n" .
"Date: ".date("r")."\r\n";
//"Subject: ".$subject."\r\n";
ob_start();                      // start output buffer 2
include($file_name);        // fill ob2
$message  = ob_get_contents();    // read ob2 ("b")
ob_end_clean();
//$message;
//echo $message;
//echo $headers;
//exit;
$result1 	= @mail($to, $subject, $message, $headers);
//print_r(error_get_last());
if($result1)
{
return $msg1 = "success"; 
}
else
{
return $msg1 = "fail";
}
//$front->pageLocation('cmspage.php?masterId=22&pageID=22&action='.$msg1);
return $msg1;

}
//added on 16-dec for delivery challan
function sendmaildispatch($to, $from, $subject, $file_name)
{
$from_to 	= "info@stanlay.in";//str_replace(".com",".in",$from); //exit;//$s->fetchGeneral_config("info");	
$headers	= 'MIME-Version: 1.0' . "\r\n".
"Content-type: text/html; charset=iso-8859-1\r\n".
"From: ".$from_to."\r\n".
"Reply-To: ".$from."\r\n" .
"Return-Path: ".$from."\r\n" .
"X-Priority: 3\r\n".
"X-Mailer: PHP". phpversion() ."\r\n".
"Date: ".date("r")."\r\n";

//"Subject: ".$subject."\r\n";
ob_start();                      // start output buffer 2
include($file_name);      // fill ob2
$message  = ob_get_contents();    // read ob2 ("b")
ob_end_clean();
//echo $message;
//echo $to;
echo $headers;
//exit;
$result1 	= mail($to, $subject, $message, $headers);
print_r(error_get_last());
if($result1)
{
return $msg1 = "success"; 

}
else
{
return $msg1 = "fail";
}
//$front->pageLocation('cmspage.php?masterId=22&pageID=22&action='.$msg1);
return $msg1;

}//////////////////////////////////////////////////////////////////////////////////////////////////////////
function sendmail_with_cc($to, $cc, $from, $subject, $file_name)
{
$reply_to=$from;
"REPLY TO".$from_to 	  = str_replace(".com",".in",$from); //exit;//$s->fetchGeneral_config("info");	
echo $headers	= "MIME-Version: 1.0\r\n".
"Content-type: text/html; charset=iso-8859-1\r\n".
"From: ".$from_to."\r\n".
"CC: ".$cc."\r\n".
"Reply-To: ".$from."\r\n" .
"Date: ".date("r")."\r\n";
//"Subject: ".$subject."\r\n";
ob_start();                      // start output buffer 2
include($file_name);        // fill ob2
$message  = ob_get_contents();    // read ob2 ("b")
ob_end_clean();
//$message;
//echo $message;
echo "<br> MAil to :".$to;
echo "<br> MAil CC :".$cc;
//exit;
$result1 	= mail($to, $subject, $message, $headers);

if($result1)

{

return $msg1 = "success"; 

}

else

{

return $msg1 = "fail";

}

//$front->pageLocation('cmspage.php?masterId=22&pageID=22&action='.$msg1);

return $msg1;

}

function sendmail_do($to,$to1, $from, $subject, $file_name)
{
//$to			= $this->fetchGeneral_config("contact");
//$from			= $_REQUEST['email'];// false mail id 
//$from_user  	= $_REQUEST['email'];
//$subject		= "Contact Us Form $from_user";
//$subject		= "Contact Us Form $from";
$headers	= "MIME-Version: 1.0\r\n".
"Content-type: text/html; charset=iso-8859-1\r\n".
"From: <".$from.">\r\n".
"Date: ".date("r")."\r\n".
"Subject: ".$subject."\r\n";
ob_start();                      // start output buffer 2
include($file_name);        // fill ob2
$message  = ob_get_contents();    // read ob2 ("b")
ob_end_clean();
//$message;
//	echo $message;
$result1 	= mail($to, $subject, $message, $headers);
$result2 	= mail($to1, $subject, $message, $headers);
//exit;
if($result1)
{
return $msg1 = "success"; 
}
else
{
return $msg1 = "fail";
}
//$front->pageLocation('cmspage.php?masterId=22&pageID=22&action='.$msg1);
return $msg1;
}

/////////////SMS GATEWAY STARTS ////////////////
function sendSMS($to_sms)
{
//live
//new SMS api by sandeep nary
//http://bhashsms.com/api/sendmsg.php?user=success&pass=654321&sender=BSHSMS&phone=9811169723&text=Test%20SMS&priority=ndnd&stype=normal
$url = 'http://myvaluefirst.com/smpp/sendsms?';
$username = "demostanlayhtp";
$password = "dmstnlya";
$type     = "1-way";
$senderid = "Testng";
$to       = $to_sms;//"$to_sms";
ob_start();                      // start output buffer 2
include('dailysms.php');        // fill ob2
$message  = ob_get_contents();    // read ob2 ("b")
ob_end_clean();
//echo  $message ;
$string="username=$username&password=$password&to=$to&from=$senderid&UDH=0&text=$message";
//exit;
$objURL = curl_init($url);
curl_setopt($objURL, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($objURL,CURLOPT_POST,1);
curl_setopt($objURL, CURLOPT_POSTFIELDS,$string);
$retval = trim(curl_exec($objURL));
curl_close($objURL);
}
/////////////SMS GATEWAY Ends ////////////////
/////////////NEW SMS GATEWAY STARTS ////////////////
function sendSMS_new($to_sms,$message)
{
//live
//new SMS api by sandeep nary
//http://bhashsms.com/api/sendmsg.php?user=success&pass=654321&sender=BSHSMS&phone=9811169723&text=Test%20SMS&priority=ndnd&stype=normal
$url = 'http://www.myvaluefirst.com/smpp/sendsms?';//http://bhashsms.com/api/sendmsg.php?';
$username = "asianchtptrans";
$password = "asncntl8";
$type     = "1-way";
$from = "ACLCRM";
$to       = $to_sms;//"$to_sms";
//  ob_start();                      // start output buffer 2
// include('dailysms.php');        // fill ob2
$message  = $message;//"Offer generated No. $off_id ";//ob_get_contents();    // read ob2 ("b")
//ob_end_clean();
//echo  $message ;
$string="username=$username&password=$password&to=$to&from=$from&text=$message&dlr-mask=19&dlr-url";
//$string="user=success&pass=654321&sender=BSHSMS&phone=$to&text=$message&priority=ndnd&stype=normal";
//exit;
$objURL = curl_init($url);
curl_setopt($objURL, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($objURL,CURLOPT_POST,1);
curl_setopt($objURL, CURLOPT_POSTFIELDS,$string);
echo  $retval = trim(curl_exec($objURL));
curl_close($objURL);
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////
function sendmail_addPro($to, $from, $subject, $message)
{
//$to			= $this->fetchGeneral_config("contact");
//$from			= $_REQUEST['email'];// false mail id 
//$from_user  	= $_REQUEST['email'];
//$subject		= "Contact Us Form $from_user";
//$subject		= "Contact Us Form $from";
$headers	= "MIME-Version: 1.0\r\n".
"Content-type: text/html; charset=iso-8859-1\r\n".
"From: <".$from.">\r\n".
"Date: ".date("r")."\r\n".
"Subject: ".$subject."\r\n";
//$message;
//echo $message;
$result1 	= mail($to, $subject, $message, $headers);
if($result1)
{
return $msg1 = "success"; 
}
else
{
return $msg1 = "fail";
}
//$front->pageLocation('cmspage.php?masterId=22&pageID=22&action='.$msg1);
return $msg1;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////

function sendmail_po($to, $from, $subject, $file_name)
{
//$to			= $this->fetchGeneral_config("contact");
//$from			= $_REQUEST['email'];// false mail id 
//$from_user  	= $_REQUEST['email'];
//$subject		= "Contact Us Form $from_user";
//$subject		= "Contact Us Form $from";
$headers	= "MIME-Version: 1.0\r\n".
"Content-type: text/html; charset=iso-8859-1\r\n".
"From: <".$from.">\r\n".
"Date: ".date("r")."\r\n".
"Subject: ".$subject."\r\n";
ob_start();                      // start output buffer 2
include($file_name);        // fill ob2
$message  = ob_get_contents();    // read ob2 ("b")
ob_end_clean();
//$message;
//echo $message;
$result1 	= mail($to, $subject, $message, $headers);
if($result1)
{
return $msg1 = "success"; 
}
else
{
return $msg1 = "fail";
}
//$front->pageLocation('cmspage.php?masterId=22&pageID=22&action='.$msg1);
return $msg1;
}

function sendSMS_pi($to_sms)
{
//live
$url = 'http://myvaluefirst.com/smpp/sendsms?';
$username = "asianchtptrans";
$password = "asncntl8";
$type     = "1-way";
$senderid = "ACLCRM";
$to       = $to_sms;//"$to_sms";
ob_start();                      // start output buffer 2
include('pi_pending_sms.php');        // fill ob2
$message  = ob_get_contents();    // read ob2 ("b")
ob_end_clean();
//echo  $message ;
//exit;
$string="username=$username&password=$password&to=$to&from=$senderid&text=$message";
//exit;
$objURL = curl_init($url);
curl_setopt($objURL, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($objURL,CURLOPT_POST,1);
curl_setopt($objURL, CURLOPT_POSTFIELDS,$string);
$retval = trim(curl_exec($objURL));
curl_close($objURL);
}

function sendSMS_po($to_sms)
{
//live
$url = 'http://myvaluefirst.com/smpp/sendsms?';
$username = "asiancontec";
$password = "asian2ct";
$type     = "1-way";
$senderid = "STNLAY";
$to       = $to_sms;//"$to_sms";
ob_start();                      // start output buffer 2
include('dailysms_po.php');        // fill ob2
$message  = ob_get_contents();    // read ob2 ("b")
ob_end_clean();
//echo  $message ;
//exit;
$string="username=$username&password=$password&to=$to&from=$senderid&UDH=0&text=$message";
//exit;
$objURL = curl_init($url);
curl_setopt($objURL, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($objURL,CURLOPT_POST,1);
curl_setopt($objURL, CURLOPT_POSTFIELDS,$string);
$retval = trim(curl_exec($objURL));
curl_close($objURL);
}
/////////////SMS GATEWAY Ends ////////////////
/*function sendSMS_pi_approval_pending()

{

//live

$url = 'http://www.myvaluefirst.com/smpp/sendsms';
$username = "asianchtptrans";
$password = "asncntl8";
$type     = "1-way";
$senderid = "ACLCRM";
$to       = $to_sms;//"$to_sms";
ob_start();                      // start output buffer 2
include('pi_pending_sms.php');        // fill ob2
$message  = ob_get_contents();    // read ob2 ("b")
ob_end_clean();
//echo  $message ;
//exit;
$string="username=$username&password=password&to=7701821170&from=ACLCRMtext=$message";
//exit;
$objURL = curl_init($url);
curl_setopt($objURL, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($objURL,CURLOPT_POST,1);
curl_setopt($objURL, CURLOPT_POSTFIELDS,$string);
$retval = trim(curl_exec($objURL));
curl_close($objURL);
}
*/
/////////////SMS GATEWAY Ends ////////////////
function po_date($po_id)
{
$price=0;	
$sql_product_vendor_price="select Date from vendor_po_final where PO_ID='".$po_id."'";
$rs_product_vendor_price=mysqli_query($GLOBALS["___mysqli_ston"],  $sql_product_vendor_price);
$row_product_vendor_price=mysqli_fetch_object($rs_product_vendor_price);		
return $this->getDateformate($row_product_vendor_price->Date,'ymd','mdy','-');
}

function po_total($po_id)
{
$price=0;	
$sql_product_vendor_price="select * from vendor_po_final where PO_ID='".$po_id."'";
$rs_product_vendor_price=mysqli_query($GLOBALS["___mysqli_ston"],  $sql_product_vendor_price);
$row_product_vendor_price=mysqli_fetch_object($rs_product_vendor_price);		
$sql_product_vendor1="select * from vendor_po_final where PO_ID='".$po_id."'";
$rs_product_vendor1=mysqli_query($GLOBALS["___mysqli_ston"],  $sql_product_vendor1);
if(mysqli_num_rows($rs_product_vendor1)>0)
{
$i=1;
while($row_product_vendor1=mysqli_fetch_object($rs_product_vendor1))
{	
$total+=$row_product_vendor1->Prodcut_Qty*$row_product_vendor1->Prodcut_Price;
$Tax_Value		=	(($total*$row_product_vendor_price->Tax_Value)/100);
$Freight_Value	=	$row_product_vendor_price->Freight_Value;
$Handling_Value	=	$row_product_vendor_price->Handling_Value;
$Bank_Value		=	$row_product_vendor_price->Bank_Value; 
}
}
return $total=$row_product_vendor_price->Flag."-".round(($total+$Tax_Value+$Freight_Value+$Handling_Value+$Bank_Value),0);
}

function order_gen_info($pcode)
{
$sql = "select orders_id,offercode, follow_up_date, order_type, date_ordered, orders_status, orders_date_finished from tbl_order where deleteflag='active' and orders_id='$pcode'";
$rs	 = mysqli_query($GLOBALS["___mysqli_ston"],  $sql); 
echo "<table width='100%' border='0' cellpadding='0' cellspacing='0' class='table table-striped table-bordered'>";
if(mysqli_num_rows($rs)>0)
{
$row = mysqli_fetch_object($rs);
echo "<tr class='pagehead'> <td colspan='2' class='pad'>";
//echo $row->order_type;
echo "Delivery Order No.: $pcode  </td> </tr>";
//echo "<tr> <td colspan='2' height='2'></td></tr>";
//echo "<tr class='text'> <td width='30%' class='pad' valign='top'>Ordered Date</td><td width='70%' valign='top'>: &nbsp;". $this->getDateformate($row->date_ordered,'ymd','mdy','-') ."</td> </tr>";
//echo "<tr class='text'> <td width='30%' class='pad' valign='top'>ACL Offer Reference No. </td><td valign='top'>: &nbsp; $row->offercode-$row->orders_id </td></tr>";
echo "<tr class='text'> <th width='30%' class='pad' valign='top'>Order Status </th><td valign='top'>$row->orders_status</td></tr>";
echo "<tr class='text'> <th width='30%' class='pad' valign='top'>Ordered Date</th><td width='68%' valign='top'>". $this->date_format_india($row->date_ordered) ."</td> </tr>";
echo "<tr class='text'> <th width='30%' class='pad' valign='top'>Follow up's Date</th><td width='68%' valign='top'>". $this->date_format_india($row->follow_up_date) ." </td> </tr>";
echo "<tr class='text'> <th width='30%' class='pad' valign='top'><form name='' method='post'>Change Follow up date:</th><td width='68%' valign='top'>
<input type='text' value='".$row->follow_up_date."' name='new_follow_date' /> <br>(YYYY-MM-DD)<br> <br> <input type='submit' name='update_followups' value='Update' class='search_top_go' style='width:100px; margin-left:11px' /> </form></td> </tr>";
//echo "<tr class='text'> <td width='30%' class='pad' valign='top'></td> <td></td></tr>";
//echo "<tr class='text'> <td width='30%' class='pad' valign='top'>Order Finished Date </td><td width='68%' valign='top'>: &nbsp; $row->orders_date_finished </td></tr>";
}
else
{
echo "<tr class='text'><td colspan='2' class='redstar'> &nbsp; No record present in database</td></tr>";
}
echo "</table>";
}
// This Display Customer Information
function order_account_info($pcode)
{
echo "<table width='100%' border='0' cellpadding='0' cellspacing='0' class='tblBorder'>";
echo "<tr class='pagehead'> <td colspan='2' class='pad'>Account Information </td></tr>";
$sql = "select customers_id,customers_name, customers_email, customers_contact_no from tbl_order where deleteflag='active' and orders_id='$pcode'";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql); 
if(mysqli_num_rows($rs)>0)
{
$row = mysqli_fetch_object($rs);
$customers_id=$row->customers_id;
$company_name=$this->company_name($row->customers_id);
$customer_GST_no=$this->cutomer_GSTno($pcode);
echo "<tr> <td colspan='2' height='2'></td></tr>";
echo "<tr class='text'><td width='30%' class='pad' valign='top'>Customer Name </td><td width='70%' valign='top'>: &nbsp; $row->customers_name </td></tr>";
echo "<tr class='text'><td width='30%' class='pad' valign='top'>Company Name </td><td width='70%' valign='top'>: &nbsp; $company_name </td></tr>";
echo "<tr class='text'> <td class='pad' valign='top'>Email</td><td valign='top'>: &nbsp; $row->customers_email</td></tr>";
echo "<tr class='text'> <td class='pad' valign='top'>Contact No.</td><td valign='top'>: &nbsp; $row->customers_contact_no</td></tr>";
echo "<tr class='text'> <td class='pad' valign='top'>GST no.</td><td valign='top'>: &nbsp; $customer_GST_no </td></tr>";
}
else
{
echo "<tr class='text'><td colspan='2' class='redstar'> &nbsp; No record present in database</td></tr>";
}
echo "</table>";
}
// This function shows billing address

function order_billing_address($pcode)
{
echo "<table width='100%' border='0' cellpadding='0' cellspacing='0' class='table table-striped table-bordered'>";
echo "<tr class='pagehead'><td colspan='2' class='pad'>Billing Address</td></tr>";
//echo "<tr> <td colspan='2' height='2'></td></tr>";
$sql = "select customers_contact_no,customers_id,billing_name, billing_company, billing_street_address, billing_city, billing_zip_code, billing_state, billing_country_name, billing_telephone_no, billing_fax_no from tbl_order where deleteflag='active' and orders_id='$pcode'";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql); 
if(mysqli_num_rows($rs)>0)
{
$row = mysqli_fetch_object($rs);
$customers_id=$row->customers_id;
$company_name=$this->company_name($row->customers_id);
$customer_GST_no=$this->cutomer_GSTno($pcode);
/*if($row->billing_company!='')
{
echo "<tr class='text'><td width='30%' class='pad' valign='top'>Company Name</td><td width='70%' valign='top'>: &nbsp;$row->billing_company</td></tr>";

}*/
echo "<tr class='text'><th width='30%' class='pad' valign='top'>Company Name</th><td width='70%' valign='top'>$company_name </td></tr>";
echo "<tr class='text'><th width='30%' class='pad' valign='top'>Company GST</th><td width='70%' valign='top'>$customer_GST_no </td></tr>";
echo "<tr class='text'><th width='30%' class='pad' valign='top'>Buyer/ Customer Name</th><td width='70%' valign='top'>$row->billing_name </td></tr>";
echo "<tr class='text'><th class='pad' valign='top' valign='top'>Address</th><td valign='top'>".nl2br($row->billing_street_address)."</td></tr>";
echo "<tr class='text'><th class='pad' valign='top'>City</th><td valign='top'>$row->billing_city</td></tr>";
echo "<tr class='text'><th class='pad' valign='top'>State/Province</th><td valign='top'>".$this->StateName($row->billing_state)."</td></tr>";
echo "<tr class='text'><th class='pad' valign='top'>Country</th><td valign='top'>".$this->CountryName($row->billing_country_name)."</td>";
echo "<tr class='text'><th class='pad' valign='top'>Zip/Postal Code</th><td valign='top'>$row->billing_zip_code</td></tr>";
if($row->billing_telephone_no!='')
{
echo "<tr class='text'><th class='pad' valign='top'>Telephone No.</th><td valign='top'>$row->billing_telephone_no</td></tr>";
}
echo "<tr class='text'> <th class='pad' valign='top'>Contact No.</th><td valign='top'>$row->customers_contact_no</td></tr>";
/*if($row->billing_fax_no!='')
{
echo "<tr class='text'><td class='pad' valign='top'>Fax No.</td><td valign='top'>: &nbsp;$row->billing_fax_no</td></tr>";		
}*/
}
else
{
echo "<tr class='text'><td colspan='2' class='redstar'> &nbsp; No record present in database</td></tr>";
}
echo "</table>";
}
// This function shows offer PO info

function order_po_info($pcode)
{
echo "<table width='100%' border='0' cellpadding='0' cellspacing='0' class='table table-striped table-bordered'>";
echo "<tr class='pagehead'><td colspan='2' class='pad'>PO details</td></tr>";
//echo "<tr> <td colspan='2' height='2'></td></tr>";
$sql = "select offercode,date_ordered,customers_contact_no,customers_id,billing_name, billing_company, billing_street_address, billing_city, billing_zip_code, billing_state, billing_country_name, billing_telephone_no, billing_fax_no from tbl_order where deleteflag='active' and orders_id='$pcode'";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql); 
if(mysqli_num_rows($rs)>0)
{
$row 				= mysqli_fetch_object($rs);
$customers_id		= $row->customers_id;
$offercode			= $row->offercode;
$company_name		= $this->company_name($row->customers_id);
$customer_GST_no	= $this->cutomer_GSTno($pcode);
$tax_percentage		= $this->tax_per($pcode);
$po_number			= $this->PO_no($pcode);
$po_date			= $this->date_format_india($this->PO_date_delivery_order($pcode));

/*if($row->billing_company!='')
{
echo "<tr class='text'><td width='30%' class='pad' valign='top'>Company Name</td><td width='70%' valign='top'>: &nbsp;$row->billing_company</td></tr>";

}*/

echo "<tr class='text'><th width='30%' class='pad' valign='top'>Offer No</th><td width='70%' valign='top'>&nbsp;$offercode-$pcode </td></tr>";
echo "<tr class='text'><th width='30%' class='pad' valign='top'>Date </th><td width='70%' valign='top'>&nbsp;".$this->date_format_india($row->date_ordered)." </td></tr>";
echo "<tr class='text'><th width='30%' class='pad' valign='top'>TAX %</th><td width='70%' valign='top'>&nbsp;$tax_percentage%</td></tr>";
echo "<tr class='text'><th class='pad' valign='top' valign='top'>PO No.</th><td valign='top'>&nbsp;".$po_number."</td></tr>";
echo "<tr class='text'><th class='pad' valign='top'>PO Date</th><td valign='top'>&nbsp;".$po_date."</td></tr>";
/*if($row->billing_fax_no!='')
{
echo "<tr class='text'><td class='pad' valign='top'>Fax No.</td><td valign='top'>: &nbsp;$row->billing_fax_no</td></tr>";		
}*/
}
else
{
echo "<tr class='text'><td colspan='2' class='redstar'> &nbsp; No record present in database</td></tr>";
}
echo "</table>";
}
/*ends*/
// This function shows shipping address
function order_shipping_address($pcode)
{
echo "<table width='100%' border='0' cellpadding='0' cellspacing='0' class='tblBorder'>";
echo "<tr class='pagehead'><td colspan='2' class='pad'>Shipping Address</td></tr>";
$sql = "select shipping_company, shipping_name, shipping_street_address, shipping_city, shipping_zip_code, shipping_state, shipping_country_name, shipping_telephone_no, shipping_fax_no from tbl_order where deleteflag='active' and orders_id='$pcode'";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql); 
if(mysqli_num_rows($rs)>0)
{
$row = mysqli_fetch_object($rs);	
/*	if($row->shipping_company!='')
{
echo "<tr class='text'><td width='30%' class='pad'>Company Name</td><td width='70%'>: &nbsp;$row->shipping_company</td></tr>";
}*/
echo "<tr class='text'><td width='30%' class='pad'>Name</td><td width='70%'>: &nbsp;$row->shipping_name </td></tr>";
echo "<tr class='text'><td class='pad' valign='top'>Street Address</td><td valign='top'>: &nbsp;".nl2br($row->shipping_street_address)."</td></tr>";
echo "<tr class='text'><td class='pad'>City</td><td>: &nbsp;$row->shipping_city</td></tr>";
echo "<tr class='text'><td class='pad'>State/Province</td><td>: &nbsp;".$this->StateName($row->shipping_state)."</td></tr>";
echo "<tr class='text'><td class='pad'>Country</td><td>: &nbsp;".$this->CountryName($row->shipping_country_name)."</td></tr>";
echo "<tr class='text'><td class='pad'>Zip/Postal Code</td><td>: &nbsp;$row->shipping_zip_code</td></tr>";
echo "<tr class='text'><td class='pad'>Telephone No.</td><td>: &nbsp;$row->shipping_telephone_no</td></tr>";
if($row->billing_fax_no!='')
{
echo "<tr class='text'><td class='pad'>Fax No.</td><td >: &nbsp;</td><td>$row->shipping_fax_no</td></tr>";
}
}
else
{
echo "<tr class='text'><td colspan='2' class='redstar'> &nbsp; No record present in database</td></tr>";
}
echo "</table>";
}

/// this function use to dispaly the payment information 
function OrderPaymentInfo($pcode)
{
$symbol		= $this->currencySymbol(1);
$currency1 	= $symbol[0];
$curValue 	= $symbol[1];
$sqlPayInfo = "select * from tbl_order where orders_id = '$pcode' and deleteflag = 'active'"; 
$rsPayInfo	= mysqli_query($GLOBALS["___mysqli_ston"],  $sqlPayInfo);
if(mysqli_num_rows($rsPayInfo)>0)
{
$rowPayInfo			= mysqli_fetch_object($rsPayInfo);
$payment_mode		= $rowPayInfo->payment_mode;
$payment_mode_orders_status		= $rowPayInfo->orders_status;
$sqlPayModule		= "select * from tbl_module_config_master where module_config_id = '$payment_mode' and deleteflag = 'active'";
$rsPayModule		= mysqli_query($GLOBALS["___mysqli_ston"],  $sqlPayModule);
$rowPayModule		= mysqli_fetch_object($rsPayModule);
$PayType			= $rowPayModule->module_config_name;
//echo $payment_mode."$$$$$";
if(strlen(trim($PayType))<=0 && $payment_mode == 15)
{
$PayType		= "Pay with a Gift Card";
}
else if(strlen(trim($PayType))<=0 && $payment_mode == 16)
{
$PayType		= "Pay with a Rewards Card";
}

else if(strlen(trim($PayType))<=0 && $payment_mode == 17)
{
$PayType		= "Cash On Delivery";
}

if($payment_mode == '4')
{
$sqlPPWP		= "select * from tbl_order_paypal_payment_info where order_id = '$pcode' and deleteflag = 'active'";
$rsPPWP			= mysqli_query($GLOBALS["___mysqli_ston"],  $sqlPPWP);
$rowPPWP		= mysqli_fetch_object($rsPPWP);
$additional_disc = $rowPPDD->additional_disc;
$totalAmount = $rowPPDD->total_order_cost;
$txn_id		 	= $rowPPWP->txn_id;
$pay_status		= $rowPPWP->payment_status;
$currency		= $rowPPWP->pay_currency;
}

if($payment_mode == '5')
{
$sqlAuth		= "select * from tbl_order_authorization where order_id = '$pcode' and deleteflag = 'active'";
$rsAuth			= mysqli_query($GLOBALS["___mysqli_ston"],  $sqlAuth);
$rowAuth		= mysqli_fetch_object($rsAuth);
$additional_disc = $rowPPDD->additional_disc;
$totalAmount 	= $rowAuth->total_amount;
$txn_id			= $rowAuth->txn_id;
$pay_status		= $rowAuth->payment_status;
$currency		= $rowAuth->pay_currency;
}

if($payment_mode == '9' || $payment_mode == '10' || $payment_mode == '11' )
{
$sqlAuth		= "select * from tbl_order_authorization_hdfc where order_id = '$pcode' and deleteflag = 'active'";
$rsAuth			= mysqli_query($GLOBALS["___mysqli_ston"],  $sqlAuth);
$rowAuth		= mysqli_fetch_object($rsAuth);
$totalAmount	= $rowAuth->total_amount;
$txn_id			= $rowAuth->txn_id;
$pay_status		= $rowAuth->payment_status;
$currency		= $rowAuth->pay_currency;
}
if($payment_mode == '6')
{
$sqlPPDD		= "select * from tbl_order_pp_api_dodirect where order_id = '$pcode' and deleteflag = 'active'";
$rsPPDD			= mysqli_query($GLOBALS["___mysqli_ston"],  $sqlPPDD);
$rowPPDD		= mysqli_fetch_object($rsPPDD);
$additional_disc = $rowPPDD->additional_disc;
$totalAmount = $rowPPDD->total_order_cost;
$txn_id			= $rowPPDD->txn_id;
$pay_status		= $rowPPDD->payment_status;
$currency		= $rowPPDD->pay_currency;
}

if($payment_mode == '17')
{
$sqlPPDD		= "select * from tbl_order where orders_id = '$pcode' and deleteflag = 'active'";
$rsPPDD			= mysqli_query($GLOBALS["___mysqli_ston"],  $sqlPPDD);
$rowPPDD		= mysqli_fetch_object($rsPPDD);
$additional_disc = $rowPPDD->additional_disc;
$totalAmount = $rowPPDD->total_order_cost-$rowPPDD->additional_disc;
//				$txn_id			= $rowPPDD->txn_id;

if($payment_mode_orders_status=="Delivered")
{
$pay_status		= "Received";//$rowPPDD->payment_status;
}
else
{
$pay_status		= "PENDING";//$rowPPDD->payment_status;
}
$currency		= "INR";//$rowPPDD->pay_currency;
}
if($payment_mode == '15')
{
$sqlPPDD		= "select * from tbl_order where orders_id = '$pcode' and deleteflag = 'active'";
$rsPPDD			= mysqli_query($GLOBALS["___mysqli_ston"],  $sqlPPDD);
$rowPPDD		= mysqli_fetch_object($rsPPDD);
$additional_disc = $rowPPDD->additional_disc;
$totalAmount = $rowPPDD->total_order_cost-$rowPPDD->additional_disc;
//				$txn_id			= $rowPPDD->txn_id;
if($payment_mode_orders_status=="Delivered")
{
$pay_status		= "RECEIVED";//$rowPPDD->payment_status;
}
else
{
$pay_status		= "RECEIVED";//$rowPPDD->payment_status;
}
$currency		= "INR";//$rowPPDD->pay_currency;
}
}

echo "<table width='100%' border='0' cellpadding='0' cellspacing='0' class='tblBorder'>";
echo "<tr class='pagehead'><td colspan='2' class='pad' >Payment Information </td></tr>";
if($rowPayInfo->cyber_amount > 0)
{
echo "<tr class='head'><td colspan='2' class='pad' >Payment Cyber Wallet Information </td></tr>";
echo "<tr class='text'><td width='32%' class='pad'>Payment </td><td>: </td><td width='68%'>  &nbsp; Cyber Wallet </td></tr>";
echo "<tr class='text'><td class='pad'>Payment Currency</td><td>: </td><td> &nbsp;INR</td></tr>";
echo "<tr class='text'><td class='pad'>Payment Amount  </td><td>: </td><td>"; 
echo " &nbsp;$currency1 ";
printf("%.0f",$rowPayInfo->cyber_amount); 
echo "  </td></tr>";
}
if($payment_mode != -1)
{
echo "<tr class='head'><td colspan='2' class='pad' >Payment Credit Card Information </td></tr>";
echo "<tr class='text'><td width='30%' class='pad'>Payment Type </td><td width='70%'>: &nbsp;$PayType </td></tr>";
echo "<tr class='text'><td class='pad'>Payment Currency</td><td>: &nbsp;$currency	</td></tr>";
echo "<tr class='text'><td class='pad'>Payment Amount  </td><td>:"; 
echo " &nbsp;$currency1 ";
printf("%.0f",$totalAmount); 
echo "  </td></tr>";
echo "<tr class='text'><td class='pad'>PaymentTransaction ID  </td><td valign='middle'>: &nbsp; # $txn_id </td></tr>";
echo "<tr class='text'><td class='pad'>Payment Status </td><td>: &nbsp;$pay_status </td></tr>";
}
echo "</table>";
}

/*****************************************************/
function OrderPaymentInfoInvoice($pcode)
{
$symbol		= $this->currencySymbol(1);
$currency1 	= $symbol[0];
$curValue 	= $symbol[1];
$sqlPayInfo = "select * from tbl_order where orders_id = '$pcode' and deleteflag = 'active'"; 
$rsPayInfo	= mysqli_query($GLOBALS["___mysqli_ston"],  $sqlPayInfo);
if(mysqli_num_rows($rsPayInfo)>0)
{
$rowPayInfo			= mysqli_fetch_object($rsPayInfo);
$payment_mode		= $rowPayInfo->payment_mode;
$payment_mode_orders_status		= $rowPayInfo->orders_status;
$sqlPayModule		= "select * from tbl_module_config_master where module_config_id = '$payment_mode' and deleteflag = 'active'";
$rsPayModule		= mysqli_query($GLOBALS["___mysqli_ston"],  $sqlPayModule);
$rowPayModule		= mysqli_fetch_object($rsPayModule);
$PayType			= $rowPayModule->module_config_name;
//echo $payment_mode."$$$$$";
if(strlen(trim($PayType))<=0 && $payment_mode == 15)
{
$PayType		= "Pay with a Gift Card";
}
else if(strlen(trim($PayType))<=0 && $payment_mode == 16)
{
$PayType		= "Pay with a Rewards Card";
}
else if(strlen(trim($PayType))<=0 && $payment_mode == 17)
{
$PayType		= "Cash On Delivery";
}
if($payment_mode == '4')
{
$sqlPPWP		= "select * from tbl_order_paypal_payment_info where order_id = '$pcode' and deleteflag = 'active'";
$rsPPWP			= mysqli_query($GLOBALS["___mysqli_ston"],  $sqlPPWP);
$rowPPWP		= mysqli_fetch_object($rsPPWP);
$additional_disc = $rowPPDD->additional_disc;
$totalAmount = $rowPPDD->total_order_cost;
$txn_id		 	= $rowPPWP->txn_id;
$pay_status		= $rowPPWP->payment_status;
$currency		= $rowPPWP->pay_currency;

}

if($payment_mode == '5')
{
$sqlAuth		= "select * from tbl_order_authorization where order_id = '$pcode' and deleteflag = 'active'";
$rsAuth			= mysqli_query($GLOBALS["___mysqli_ston"],  $sqlAuth);
$rowAuth		= mysqli_fetch_object($rsAuth);
$additional_disc = $rowPPDD->additional_disc;
$totalAmount 	= $rowAuth->total_amount;
$txn_id			= $rowAuth->txn_id;
$pay_status		= $rowAuth->payment_status;
$currency		= $rowAuth->pay_currency;
}

if($payment_mode == '9' || $payment_mode == '10' || $payment_mode == '11' )
{
$sqlAuth		= "select * from tbl_order_authorization_hdfc where order_id = '$pcode' and deleteflag = 'active'";
$rsAuth			= mysqli_query($GLOBALS["___mysqli_ston"],  $sqlAuth);
$rowAuth		= mysqli_fetch_object($rsAuth);
$totalAmount	= $rowAuth->total_amount;
$txn_id			= $rowAuth->txn_id;
$pay_status		= $rowAuth->payment_status;
$currency		= $rowAuth->pay_currency;
}

if($payment_mode == '6')
{
$sqlPPDD		= "select * from tbl_order_pp_api_dodirect where order_id = '$pcode' and deleteflag = 'active'";
$rsPPDD			= mysqli_query($GLOBALS["___mysqli_ston"],  $sqlPPDD);
$rowPPDD		= mysqli_fetch_object($rsPPDD);
$additional_disc = $rowPPDD->additional_disc;
$totalAmount = $rowPPDD->total_order_cost;
$txn_id			= $rowPPDD->txn_id;
$pay_status		= $rowPPDD->payment_status;
$currency		= $rowPPDD->pay_currency;
}

if($payment_mode == '17')
{
$sqlPPDD		= "select * from tbl_order where orders_id = '$pcode' and deleteflag = 'active'";
$rsPPDD			= mysqli_query($GLOBALS["___mysqli_ston"],  $sqlPPDD);
$rowPPDD		= mysqli_fetch_object($rsPPDD);
$additional_disc = $rowPPDD->additional_disc;
$totalAmount = $rowPPDD->total_order_cost-$rowPPDD->additional_disc;
//				$txn_id			= $rowPPDD->txn_id;
if($payment_mode_orders_status=="Delivered")
{
$pay_status		= "Received";//$rowPPDD->payment_status;
}
else
{
$pay_status		= "PENDING";//$rowPPDD->payment_status;
}
$currency		= "INR";//$rowPPDD->pay_currency;
}
if($payment_mode == '15')
{
$sqlPPDD		= "select * from tbl_order where orders_id = '$pcode' and deleteflag = 'active'";
$rsPPDD			= mysqli_query($GLOBALS["___mysqli_ston"],  $sqlPPDD);
$rowPPDD		= mysqli_fetch_object($rsPPDD);
$additional_disc = $rowPPDD->additional_disc;
$totalAmount = $rowPPDD->total_order_cost-$rowPPDD->additional_disc;
//				$txn_id			= $rowPPDD->txn_id;
if($payment_mode_orders_status=="Delivered")
{
$pay_status		= "RECEIVED";//$rowPPDD->payment_status;
}
else
{
$pay_status		= "RECEIVED";//$rowPPDD->payment_status;
}
$currency		= "INR";//$rowPPDD->pay_currency;
}
}
echo "<table width='100%' border='0' cellpadding='0' cellspacing='0' class='tblBorder'>";
echo "<tr class='pagehead'><td colspan='2' class='pad' ><strong>Payment Information </strong></td></tr>";
if($rowPayInfo->cyber_amount > 0)
{
echo "<tr class='head'><td colspan='2' class='pad' >Payment Cyber Wallet Information </td></tr>";
echo "<tr class='text'><td width='32%' class='pad'>Payment </td><td>: </td><td width='68%'>  &nbsp; Cyber Wallet </td></tr>";
echo "<tr class='text'><td class='pad'>Payment Currency</td><td>: </td><td> &nbsp;INR</td></tr>";
echo "<tr class='text'><td class='pad'>Payment Amount  </td><td>: </td><td>"; 
echo " &nbsp;$currency1 ";
printf("%.0f",$rowPayInfo->cyber_amount); 
echo "  </td></tr>";
}

if($payment_mode != -1)
{
//			echo "<tr class='head'><td colspan='2' class='pad' >Payment Credit Card Information </td></tr>";
echo "<tr class='text'><td width='30%' class='pad'>Payment Type </td><td width='70%'>: &nbsp;$PayType </td></tr>";
echo "<tr class='text'><td class='pad'>Payment Currency</td><td>: &nbsp;$currency	</td></tr>";
echo "<tr class='text'><td class='pad'>Payment Amount  </td><td>:"; 
echo " &nbsp;$currency1 ";
printf("%.0f",$totalAmount); 
echo "  </td></tr>";
echo "<tr class='text'><td class='pad'>PaymentTransaction ID  </td><td valign='middle'>: &nbsp; # $txn_id </td></tr>";
echo "<tr class='text'><td class='pad'>Payment Status </td><td>: &nbsp;$pay_status </td></tr>";
}

echo "</table>";
}

/*****************************************************/	

//generate delivery sticker

function OrderPaymentInfo_delivery_sticker($pcode,$invoice_no,$box_dimensions,$box_weight,$courier_company)
{
$symbol		= $this->currencySymbol(1);
$currency1 	= $symbol[0];
$curValue 	= $symbol[1];
$sqlPayInfo = "select * from tbl_order where orders_id = '$pcode' and deleteflag = 'active'"; 
$rsPayInfo	= mysqli_query($GLOBALS["___mysqli_ston"],  $sqlPayInfo);
if(mysqli_num_rows($rsPayInfo)>0)
{
$rowPayInfo			= mysqli_fetch_object($rsPayInfo);
$payment_mode		= $rowPayInfo->payment_mode;
$shipping_zip_code	= $rowPayInfo->shipping_zip_code;
$rout_codebluedart	= $this->bluedartroutingcode($shipping_zip_code);
$sqlPayModule		= "select * from tbl_module_config_master where module_config_id = '$payment_mode' and deleteflag = 'active'";
$rsPayModule		= mysqli_query($GLOBALS["___mysqli_ston"],  $sqlPayModule);
$rowPayModule		= mysqli_fetch_object($rsPayModule);
$PayType			= $rowPayModule->module_config_name;
$couponDiscount		= $rowPayInfo->coupon_discount;
///			$couponDiscount		= $rowPayInfo->coupon_discount;
//echo $payment_mode."$$$$$";
if(strlen(trim($PayType))<=0 && $payment_mode == 15)
{
$PayType		= "Pay with a Gift Card";
}
else if(strlen(trim($PayType))<=0 && $payment_mode == 16)
{
$PayType		= "Pay with a Rewards Card";
}
else if(strlen(trim($PayType))<=0 && $payment_mode == 17)
{
$PayType		= "Cash On Delivery";
}
if($payment_mode == '4')
{
$sqlPPWP		= "select * from tbl_order_paypal_payment_info where order_id = '$pcode' and deleteflag = 'active'";
$rsPPWP			= mysqli_query($GLOBALS["___mysqli_ston"],  $sqlPPWP);
$rowPPWP		= mysqli_fetch_object($rsPPWP);
$totalAmount 	= $rowPPWP->total_amount;
$txn_id		 	= $rowPPWP->txn_id;
$pay_status		= $rowPPWP->payment_status;
$currency		= $rowPPWP->pay_currency;
}
if($payment_mode == '5')
{
$sqlAuth		= "select * from tbl_order_authorization where order_id = '$pcode' and deleteflag = 'active'";
$rsAuth			= mysqli_query($GLOBALS["___mysqli_ston"],  $sqlAuth);
$rowAuth		= mysqli_fetch_object($rsAuth);
$totalAmount	= $rowAuth->total_amount;
$txn_id			= $rowAuth->txn_id;
$pay_status		= $rowAuth->payment_status;
$currency		= $rowAuth->pay_currency;
}
if($payment_mode == '9'  || $payment_mode == '10'  || $payment_mode == '11' )
{
$sqlAuth		= "select * from tbl_order_authorization_hdfc where order_id = '$pcode' and deleteflag = 'active'";
$rsAuth			= mysqli_query($GLOBALS["___mysqli_ston"],  $sqlAuth);
$rowAuth		= mysqli_fetch_object($rsAuth);
$totalAmount	= $rowAuth->total_amount;
$txn_id			= $rowAuth->txn_id;
$pay_status		= $rowAuth->payment_status;
$currency		= $rowAuth->pay_currency;
}

if($payment_mode == '6')
{
$sqlPPDD		= "select * from tbl_order_pp_api_dodirect where order_id = '$pcode' and deleteflag = 'active'";
$rsPPDD			= mysqli_query($GLOBALS["___mysqli_ston"],  $sqlPPDD);
$rowPPDD		= mysqli_fetch_object($rsPPDD);
$totalAmount	= $rowPPDD->total_amount;
$txn_id			= $rowPPDD->txn_id;
$pay_status		= $rowPPDD->payment_status;
$currency		= $rowPPDD->pay_currency;
}

if($payment_mode == '17')
{
$sqlPPDD		= "select * from tbl_order where orders_id = '$pcode' and deleteflag = 'active'";
$rsPPDD			= mysqli_query($GLOBALS["___mysqli_ston"],  $sqlPPDD);
$rowPPDD		= mysqli_fetch_object($rsPPDD);
$additional_disc = $rowPPDD->additional_disc;
$totalAmount	= $rowPPDD->total_order_cost;
//				$txn_id			= $rowPPDD->txn_id;
$pay_status		= "PENDING";//$rowPPDD->payment_status;
$currency		= "INR";//$rowPPDD->pay_currency;
}

if($payment_mode == '15' )
{
$sqlPPDD		= "select * from tbl_order where orders_id = '$pcode' and deleteflag = 'active'";
$rsPPDD			= mysqli_query($GLOBALS["___mysqli_ston"],  $sqlPPDD);
$rowPPDD		= mysqli_fetch_object($rsPPDD);
$additional_disc = $rowPPDD->additional_disc;
$totalAmount	= $rowPPDD->total_order_cost;
//				$txn_id			= $rowPPDD->txn_id;
$pay_status		= "RECEIVED";//$rowPPDD->payment_status;
$currency		= "INR";//$rowPPDD->pay_currency;
}
}

$pcodeAEB		= $_REQUEST['pcode'];
$awb			= "select * from tble_invoice where id = '$pcodeAEB'";
$rsAWB			= mysqli_query($GLOBALS["___mysqli_ston"],  $awb);
$rowAWB			= mysqli_fetch_object($rsAWB);
$sqlOrderTotal_R 			= "SELECT * FROM tbl_order_product where order_id ='$pcode' order by order_pros_id desc";
$rsOrderTotal_R  			=  mysqli_query($GLOBALS["___mysqli_ston"],  $sqlOrderTotal_R);
$shp_num_rows=mysqli_num_rows($rsOrderTotal_R);
//		$rowOrderTotal 				= mysqli_fetch_object($rsOrderTotal_R);
$shippingcost		   	= $rowPayInfo->shipping_method_cost;
//		$shippingcost		   	= $rowPayInfo->shipping_method_cost/$shp_num_rows;
$sqlOrderTotal 			= "SELECT * FROM tbl_order_product o_tbl, tble_invoice_items i_tbl WHERE o_tbl.pro_id = i_tbl.pro_id and order_id ='$pcode' and  i_tbl.i_id='$pcodeAEB' ";
$rsOrderTotal  =  mysqli_query($GLOBALS["___mysqli_ston"],  $sqlOrderTotal);
while($rowOrderTotal 	= mysqli_fetch_object($rsOrderTotal))
{
$additional_disc1=$rowOrderTotal->additional_disc+$additional_disc1;
$TotalOrder+= (($rowOrderTotal->pro_price)*$rowOrderTotal->pro_quantity);
}

//echo $rowPayInfo->additional_disc;
if($additional_disc1=='0')
{
$additional_disc1=$rowPayInfo->additional_disc;
}
$TotalOrder1	=$TotalOrder+$shippingcost-$couponDiscount;				
//	$TotalOrder1	=$TotalOrder+$taxValue+$shippingcost-$couponDiscount;				
//echo $courier_company; 
if($courier_company=="Blue dart pp" || $courier_company=="Blue dart cod")
{
$classname_blue="classname_blue";
echo "<table width='50%' border='1' cellspacing='0' cellpadding='0' align='center' height='400' bordercolor='#e4e4e4' class='bdr_left bdr_bottom bdr_right bdr_top '> ";
?>

<tr>
  <td class="" valign="top"><img src="http://kingschest.com/images/blog_logo.jpg" title="Kingschest" width="100" /></td>
  <td align="center" colspan="2" class="bdr_left bdr_top "><table width="100%" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td class="bdr_left bdr_bottom bdr_right bdr_top " width="200">TIN No. </td>
        <td class="bdr_left bdr_bottom bdr_top bdr_right ">07730421584</td>
      </tr>
      
      <!--  <tr>
<td class="bdr_left bdr_bottom bdr_right  ">CST</td>
<td class="bdr_left bdr_bottom bdr_right  ">07730421584</td>
</tr>
<tr>
<td class="bdr_left bdr_bottom bdr_right  ">VAT Reg No.</td>
<td class="bdr_left bdr_bottom bdr_right  ">07730421584</td>
</tr>
-->
      
    </table></td>
</tr>
<tr>
  <td  valign="top"><strong>Invoice No.</strong></td>
  <td align="center" colspan="2" valign="top"><span style="font-family:'basawa 3 of 9 MHR'; font-size:36px;">*<?php echo $invoice_no; ?>*</span></td>
</tr>
<?php 
}
else
{
$classname_blue="classname";
echo "<table width='30%' border='1' cellspacing='0' cellpadding='0' align='center' height='400' bordercolor='#e4e4e4' class='bdr_left bdr_bottom bdr_right bdr_top '> ";
?>
<tr>
  <td valign="top"><img src="http://kingschest.com/images/blog_logo.jpg" title="Kingschest" width="100" /></td>
  <td align="center" colspan="2"><span style="font-family:'3 of 9 Barcode'; font-size:40px">*<?php echo $invoice_no; ?>*</span><br />
    Invoice No.<?php echo $invoice_no; ?></td>
</tr>
<?php 

}
echo "<tr>";
echo "<td valign='top'><strong>AWB No.</strong></td>";
echo "<td nowrap='nowrap' colspan='3' align='center' valign='top'><span  class='$classname_blue'>*$rowAWB->awb_no*</span><br />
AWBNo. $rowAWB->awb_no  </td>";
echo "</tr>";
echo "<tr >";
echo "<td valign='top'>Order No#</td>";
echo "<td valign='top'>$rowPayInfo->orders_id</td>";
echo "</tr>";
echo "<tr >";
echo "<td valign='top'>Invoice No#</td>";
echo "<td valign='top'>".$invoice_no."</td>";
echo "</tr>";
echo "<tr >";
echo "<td valign='top'>Weight(kgs)</td>";
echo "<td valign='top'>". number_format($box_weight,2)."</td>";
echo "</tr>";
echo "<tr >";
echo "<td valign='top'>Dimensions(cms)</td>";
echo "<td valign='top'>".$box_dimensions."</td>";
echo "</tr>";
echo "<tr >";
echo "<td width='10%' valign='top'>Name</td>";
echo "<td nowrap='nowrap'  valign='top' >$rowPayInfo->shipping_name</td>";
echo "</tr>";
echo "<tr >";
echo "<td  valign='top'>Address</td>";
echo "<td  valign='top'> $rowPayInfo->shipping_street_address $rowPayInfo->shipping_city, <br>  $rowPayInfo->shipping_state,$rowPayInfo->shipping_zip_code ";
echo "<br><strong style='font-size: 18px;'>$rout_codebluedart</strong></td>";
echo "</tr>";
echo "<tr  valign='top'>";
echo "<td nowrap='nowrap'  valign='top'>Phone Number</td>";
echo "<td  valign='top'>$rowPayInfo->shipping_telephone_no</td>";
echo "</tr>";
$inv_no_item=substr($invoice_no,0,3);
if($courier_company=="Blue dart pp" || $courier_company=="Blue dart cod")
{
?>
<tr>
  <td colspan="3" class="tblBorder_invoice" ><?php $this->OrderItemsInfo_invoice_packing_slip($pcode,$inv_no_item);?></td>
</tr>
<?php
}
echo "<tr class='text'>";
echo "<td  >Order Amount</td>";
echo "<td >";
printf(" %.0f",$TotalOrder1-$additional_disc1);
echo"</td>";
echo "</tr>";
if($rowPayInfo->payment_mode==5  || $rowPayInfo->payment_mode==9  || $rowPayInfo->payment_mode==10  || $rowPayInfo->payment_mode==11 )
{
echo "<tr >";
echo "<td align='center' colspan='2' valign='top'>
<h3>Payment Received : Card/Netbanking</h3></td>";
echo "</tr>";
}

else if($rowPayInfo->payment_mode==15)
{
echo "<tr >";
echo "<td align='center' colspan='2' valign='top'>
<h3>Payment Received : Pay with Gift Card</h3></td>";
echo "</tr>";
}
else
{
echo "<tr>";	 
echo "<td align='center' colspan='2' valign='top'>
<h1 style='font-size: 24px;'>CASH ON DELIVERY</h1><h2 style='font-size: 22px;'>Amount to be collected : RS. ";
printf(" %.0f",$TotalOrder1-$additional_disc1);
echo"</h2></td>";
echo "</tr>";
}

/*echo "<tr class='text'>";
echo "<td>AWB/Tracking No.</td>";
echo "<td>$rowAWB->awb_no</td>";
echo "</tr>";*/
echo "<tr class='text'>";
if($rowPayInfo->payment_mode==0  || $rowPayInfo->payment_mode==9  || $rowPayInfo->payment_mode==10  || $rowPayInfo->payment_mode==11 || $rowPayInfo->payment_mode==15 )
{
echo "<td colspan='2' align='center'><strong>Return Address</strong> :Netcom Ventures Pvt. Ltd.<br>B-28 Okhla Ph-1, New Delhi-110020, <strong style='font-size: 18px;'>DEL/OKH</strong></td>";
}
else
{
echo "<td colspan='2' align='center'><strong>Return Address</strong> :Netcom Ventures Pvt. Ltd.<br>B-28 Okhla Ph-1, New Delhi,   <strong style='font-size: 18px;'>DEL/HPW	111116</strong></td>";
}
echo "</tr>";
echo "</table>";
}
//////////////////////////////
/////////// this function use to display the order Total amount info	
function OrderTotalInfo_invoice($pcode)
{
$symbol		= $this->currencySymbol(1);
$currency1 	= $symbol[0];
$curValue 	= $symbol[1];
$sqlOrderTotal = "select * from tbl_order where orders_id = '$pcode'";
$rsOrderTotal  =  mysqli_query($GLOBALS["___mysqli_ston"],  $sqlOrderTotal);
if(mysqli_num_rows($rsOrderTotal)>0)
{
$rowOrderTotal 	= mysqli_fetch_object($rsOrderTotal);
/*	if($rowOrderTotal->order_type1!=0)
{
$TotalOrder	   	= $rowOrderTotal->total_order_cost+$rowOrderTotal->shipping_method_cost;
//$TotalOrder	   	= $rowOrderTotal->total_order_cost;
}
else
{
$TotalOrder	   	= $rowOrderTotal->total_order_cost;
}*/
$TotalOrder	   	= $rowOrderTotal->total_order_cost;
$ship		   	= $rowOrderTotal->shipping_method_cost;
$shippingValue	   	= $ship;
$taxValue		   	= $rowOrderTotal->tax_cost;
//			$shippingValue 	= explode(" - ",$ship);
$subtotal      = $TotalOrder - $shippingValue-$taxValue ;
$subtotal		= $subtotal + $rowOrderTotal->coupon_discount;
$couponDiscount = $rowOrderTotal->coupon_discount;
}

echo "<table width='67%' border='0' cellpadding='5' cellspacing='0' >";
/*echo "<tr class='pagehead'>";
echo "<td colspan='2' class='pad' nowrap  align='left'>Order Total </td>";
echo "</tr>";*/
echo "<tr class='text'>";
echo "<td width='78%' class='pad' nowrap align='right'>Sub Total </td>";
echo "<td width='22%'  align='left'>: &nbsp; &nbsp; $currency1 ";
printf(" %.2f",$subtotal);
echo "</td>";
echo "</tr>";
echo "<tr class='text'>";
echo "<td width='78%' class='pad' nowrap align='right'>Tax </td>";
echo "<td width='22%'  align='left'>: &nbsp; &nbsp; $currency1 ";
printf(" %.2f",$taxValue);
echo "</td>";
echo "</tr>";
echo "<tr class='text'>";
echo "<td class='pad' nowrap align='right'>Shipping &amp; Handling </td>";
echo "<td  align='left'>: &nbsp; &nbsp; $currency1 ";
printf(" %.2f ",$ship);
echo "</td>";
echo "</tr>";
echo "<tr class='text'>";
echo "<td class='pad' nowrap align='right'><strong>Grand Total </strong></td>";
echo "<td align='left'><strong>: &nbsp; &nbsp; $currency1 "; 
printf(" %.2f",$TotalOrder);
echo "</strong></td>";
echo "</tr>";
echo "</table>";
}

/////////////////////

function GST_tax_amount_on_offer($orderid)
{
$sqlOrderTotal_GST = "SELECT sum( Pro_tax ) AS GST_amount
FROM `tbl_order_product` WHERE `order_id` =$orderid GROUP BY order_id";
$rsOrderTotal_GST  =  mysqli_query($GLOBALS["___mysqli_ston"],  $sqlOrderTotal_GST);
if(mysqli_num_rows($rsOrderTotal_GST)>0)
{
$rowOrderTotal_GST 	= mysqli_fetch_object($rsOrderTotal_GST);
$gst_amt=$rowOrderTotal_GST->GST_amount;
}
return $gst_amt;
}
/////////// this function use to display the order Total amount info	

function OrderTotalInfo_offer_letter($pcode)
{
$symbol		= $this->currencySymbol(1);
$currency1 	= $symbol[0];
$curValue 	= $symbol[1];
$sqlOrderTotal = "select * from tbl_order where orders_id = '$pcode'";
$rsOrderTotal  =  mysqli_query($GLOBALS["___mysqli_ston"],  $sqlOrderTotal);
if(mysqli_num_rows($rsOrderTotal)>0)
{
$rowOrderTotal 	= mysqli_fetch_object($rsOrderTotal);
$Price_type					= $rowOrderTotal->Price_type;//$rowOrderPro->GST_percentage;	
/*	if($rowOrderTotal->order_type1!=0)
{
$TotalOrder	   	= $rowOrderTotal->total_order_cost+$rowOrderTotal->shipping_method_cost;
//$TotalOrder	   	= $rowOrderTotal->total_order_cost;
}
else
{
$TotalOrder	   	= $rowOrderTotal->total_order_cost;
}*/
$sqlOrderPro = "select * from tbl_order_product where order_id ='$pcode' order by order_pros_id asc";
$rsOrderPro	 = mysqli_query($GLOBALS["___mysqli_ston"],  $sqlOrderPro);
$h=0;
$subtotal1=0;
while($rowOrderPro = mysqli_fetch_object($rsOrderPro))
{
$h++;
$OrderProID	 = $rowOrderPro->order_pros_id;
$proID	 	 = $rowOrderPro->pro_id;
$groupID 	 = $rowOrderPro->group_id;
$sqlAttr = "select * from tbl_group where group_id='$groupID'";
$rsAttr  = mysqli_query($GLOBALS["___mysqli_ston"],  $sqlAttr);
$i  	 = -1;
$k  	 = 0;
$cb 	 = 0;
$tx		 = 0;
$ta		 = 0;
if($cb != 0)
{
}
if($tx!=0)
{
}
if($ta != 0)
{
}
$ManufacturerID = $rowOrderPro->manufacturers_id;
$orPrice = $rowOrderPro->pro_price;
number_format($orPrice,2);
/*$GST_tax				=$rowOrderPro->Pro_tax;
echo "GSTSGSGST".$GST_percentage		=$this->GST_tax_amount_on_offer($pcode);//$rowOrderPro->GST_percentage;		
*/
$sql_dis="SELECT * FROM prowise_discount where orderid='".$pcode."' and proid='".$proID."'";
$sql_dis_dis=mysqli_query($GLOBALS["___mysqli_ston"],  $sql_dis);
$sql_dis_row=mysqli_fetch_object($sql_dis_dis);
$subtotal1=($orPrice*$rowOrderPro->pro_quantity)-$sql_dis_row->discount_amount;
$subtotal1_dis+=$sql_dis_row->discount_amount;
$discounted_price=$orPrice*$sql_dis_row->discount_percent/100;			
//added condition for zro GST in export offer as on dated 06-oct-2020 starts
if($Price_type!="Export_USD")
{
$per_product_GST_percentage=$rowOrderPro->GST_percentage;
$discounted_price_tax_amt=($orPrice-$discounted_price)*$rowOrderPro->GST_percentage/100;		
}
else
{
$per_product_GST_percentage="0";//$rowOrderPro->GST_percentage;
$discounted_price_tax_amt="0";//($orPrice-$discounted_price);			
}
//added condition for zro GST in export offer as on dated 06-oct-2020 ends	
	
$SumProPrice = ($orPrice-$discounted_price+$discounted_price_tax_amt)* $rowOrderPro->pro_quantity;//$proPrice * $rowOrderPro->pro_quantity;
$totalTax+= $discounted_price_tax_amt * $rowOrderPro->pro_quantity;//$rowOrderPro->Pro_tax * $rowOrderPro->pro_quantity;
$ProTotalPrice = ($discounted_price_tax_amt+$orPrice-$discounted_price)* $rowOrderPro->pro_quantity;//($rowOrderPro->pro_final_price) * $rowOrderPro->pro_quantity ;//remove tax changed by rumit
$freight_amount = $rowOrderPro->freight_amount;// * $rowOrderPro->pro_quantity;

"freight without GST value:".$freight_amount_with_gst = $rowOrderPro->freight_amount/1.18;// * $rowOrderPro->pro_quantity;

"<br>freight GST value:".$freight_gst_amount = $rowOrderPro->freight_amount-$freight_amount_with_gst;// * $rowOrderPro->pro_quantity;
$totalCost     = $totalCost + $ProTotalPrice;
/*		echo "<td valign='top' class='tblBorder_invoice_bottom ' nowrap='nowrap'>";
echo $Offer_Currency." "; 
echo number_format($ProTotalPrice,2);
echo "</td>";
echo "</tr>";
*/	
//		$discounted_price_tax_amt=($orPrice-$discounted_price)*$rowOrderPro->GST_percentage/100;				
}

//added condition for zro GST in export offer as on dated 06-oct-2020 starts
if($Price_type!="Export_USD")
{

$GST_tax_amt				= $this->GST_tax_amount_on_offer($pcode);//$rowOrderPro->GST_percentage;				
}
else
{
	$GST_tax_amt			= "0";//$this->GST_tax_amount_on_offer($pcode);//$rowOrderPro->GST_percentage;				
}
//added condition for zro GST in export offer as on dated 06-oct-2020 ends
$TotalOrder	   				= $rowOrderTotal->total_order_cost;
$ship		   				= $rowOrderTotal->shipping_method_cost;
$shippingValue				= $ship;
$taxValue					= $rowOrderTotal->tax_cost;
$tax_included				= $rowOrderTotal->tax_included;
$tax_perc					= $rowOrderTotal->taxes_perc;
$discount_perc				= $rowOrderTotal->discount_perc;
$discount_per_amt			= $rowOrderTotal->discount_per_amt;
$show_discount				=$rowOrderTotal->show_discount;
//			$shippingValue 	= explode(" - ",$ship);
//			$subtotal       = $TotalOrder - $shippingValue-$taxValue ;
//$subtotal       			= $TotalOrder;
$subtotal_after_discount    = $TotalOrder - $subtotal1_dis;
if($tax_included=='Excluded')
{
@$subtotal_tax       		= @$subtotal_after_discount*$tax_perc/100;
}
else
{
$subtotal_tax       		= $subtotal_after_discount;
}
if($tax_included=='Excluded')
{
$GrandTotalOrder			= $subtotal_after_discount + $subtotal_tax;
}
else
{
$GrandTotalOrder			= $subtotal_after_discount + 0;
}
//$subtotal_final		= $subtotal_tax;
$couponDiscount 			= $rowOrderTotal->coupon_discount;
}
echo "<table width='50%' border='0' cellpadding='5' cellspacing='0' >";
/*echo "<tr class='pagehead'>";
echo "<td colspan='2' class='pad' nowrap  align='left'>Order Total </td>";
echo "</tr>";*/
echo "<tr class='text'>";
echo "<td width='70%' class='pad tblBorder_invoice_bottom tblBorder_invoice_right tblBorder_invoice_left ' nowrap align='right'><strong>Sub Total :</strong> </td>";
echo "<td width='30%'  align='right' class='tblBorder_invoice_bottom ' nowrap='nowrap'> &nbsp; &nbsp;";
echo str_replace("backoffice","crm",$rowOrderTotal->Price_value)." ";
echo number_format($totalCost-$totalTax,2);
echo "</td>";
echo "</tr>";
//echo $show_discount;
if($show_discount=="Yes") {
echo "<tr class='text' style='display:none'>";
echo "<td  class='pad tblBorder_invoice_bottom tblBorder_invoice_right tblBorder_invoice_left' nowrap align='right'><strong>Discount :</strong></td>";
echo "<td   align='right'  class='tblBorder_invoice_bottom'  > &nbsp; &nbsp;";
echo str_replace("backoffice","crm",$rowOrderTotal->Price_value)." ";
echo number_format($subtotal1_dis,2);
echo "</td>";
echo "</tr>";
}
echo "<tr class='text'>";
echo "<td class='pad tblBorder_invoice_right tblBorder_invoice_bottom tblBorder_invoice_left' nowrap align='right'><strong>Freight Value :</strong></td>";
echo "<td align='right'  class='tblBorder_invoice_bottom' nowrap='nowrap' >&nbsp; &nbsp; ";
echo str_replace("backoffice","crm",$rowOrderTotal->Price_value)." ";
//echo number_format($freight_amount-$freight_gst_amount,2);
//print_r($_REQUEST);
if($_REQUEST["action"]=='edit')
{
?>
<!-- added on 23-may-2020 for edit freight value starts-->
<input type="text" name='freight_included' id='freight_included' value="<?php echo number_format($freight_amount-$freight_gst_amount,2);?>" class='form-control' required  style="width:70%; display:inline; padding:0; margin:0; height:auto" />
<!-- added on 23-may-2020 for edit freight value ends-->
<?php
}
else
{
	echo number_format($freight_amount-$freight_gst_amount,2);
}
echo "</td>";
echo "</tr>";
$Grand_subtotal1=$subtotal1;
if($tax_included=='Excluded' || $GST_tax_amt>0)
{
//	echo "</tr>";
echo "<tr class='text'>";
/*if($GST_tax_amt>0) // commented on  6-oct-2020 while adding condition on export offers
{*/
echo "<td class='pad tblBorder_invoice_bottom tblBorder_invoice_right tblBorder_invoice_left' nowrap align='right'><strong>Add IGST :</strong></td>";
//}
/*else 
{
echo "<td class='pad tblBorder_invoice_bottom tblBorder_invoice_right tblBorder_invoice_left' nowrap align='right'><strong>Add VAT/CST@ $tax_perc% :</strong></td>";
}*/
echo "<td align='right'  class='tblBorder_invoice_bottom'  > &nbsp; &nbsp; ";
if($totalTax>0)
{
echo str_replace("backoffice","crm",$rowOrderTotal->Price_value)." ";
echo number_format($totalTax+$freight_gst_amount,2);//total gst value rumit 
}
else
{
echo str_replace("backoffice","crm",$rowOrderTotal->Price_value)." ";
$subtotal_tax1=$subtotal_tax;
echo number_format($subtotal_tax1,2);
}//echo $subtotal1;
$Grand_subtotal1=$totalCost;//$subtotal1+$subtotal_tax1+$totalTax;
echo "</td>";
echo "</tr>";
}
/*	
echo "<tr class='text'>";
echo "<td class='pad' nowrap align='right'>Shipping &amp; Handling </td>";
echo "<td  align='left'>: &nbsp; &nbsp; $currency1 ";
printf(" %.2f ",$ship);
echo "</td>";
echo "</tr>";
*/
echo "<tr class='text'>";
echo "<td class='pad tblBorder_invoice_right tblBorder_invoice_left' nowrap align='right'><h4>Grand Total :</h4></td>";
echo "<td align='right'  nowrap='nowrap' ><h4> &nbsp; &nbsp; ";
echo str_replace("backoffice","crm",$rowOrderTotal->Price_value)." ";
//		printf(" %.2f",$TotalOrder);
if($Price_type!="Export_USD")
{
echo number_format($totalCost+$freight_amount,2);
}
else
{
echo number_format($totalCost+$freight_amount-$freight_gst_amount,2);	
}
echo "</h4></td>";
echo "</tr>";
echo "</table>";
}

//////////////////////////////// this function use to display the order Total amount info in proforma invoice	

function proforma_invoice_total($pcode)

{

$symbol		= $this->currencySymbol(1);

$currency1 	= $symbol[0];

$curValue 	= $symbol[1];

$sqlOrderTotal = "select * from tbl_order where orders_id = '$pcode'";

$rsOrderTotal  =  mysqli_query($GLOBALS["___mysqli_ston"],  $sqlOrderTotal);

if(mysqli_num_rows($rsOrderTotal)>0)

{

$rowOrderTotal 	= mysqli_fetch_object($rsOrderTotal);

/*	if($rowOrderTotal->order_type1!=0)

{

$TotalOrder	   	= $rowOrderTotal->total_order_cost+$rowOrderTotal->shipping_method_cost;

//$TotalOrder	   	= $rowOrderTotal->total_order_cost;

}

else

{

$TotalOrder	   	= $rowOrderTotal->total_order_cost;

}*/

$sqlOrderPro = "select * from tbl_order_product where order_id ='$pcode' order by order_pros_id asc";

$rsOrderPro	 = mysqli_query($GLOBALS["___mysqli_ston"],  $sqlOrderPro);

$h=0;

$subtotal1=0;

while($rowOrderPro = mysqli_fetch_object($rsOrderPro))

{

$h++;

$OrderProID	 = $rowOrderPro->order_pros_id;

$proID	 	 = $rowOrderPro->pro_id;

$groupID 	 = $rowOrderPro->group_id;

$orPrice 	 = $rowOrderPro->pro_price;

number_format($orPrice,2);

/*$GST_tax				=$rowOrderPro->Pro_tax;

echo "GSTSGSGST".$GST_percentage		=$this->GST_tax_amount_on_offer($pcode);//$rowOrderPro->GST_percentage;		

*/

$sql_dis="SELECT * FROM prowise_discount where orderid='".$pcode."' and proid='".$proID."'";

$sql_dis_dis=mysqli_query($GLOBALS["___mysqli_ston"],  $sql_dis);

$sql_dis_row=mysqli_fetch_object($sql_dis_dis);

$subtotal1=($orPrice*$rowOrderPro->pro_quantity)-$sql_dis_row->discount_amount;

$subtotal1_dis=$sql_dis_row->discount_amount;

$discounted_price=$orPrice*$sql_dis_row->discount_percent/100;			//discount amt per unit

$per_product_GST_percentage=$rowOrderPro->GST_percentage;//product gst %

$discounted_price_tax_amt=($orPrice-$discounted_price)*$rowOrderPro->GST_percentage/100;			

$SumProPrice = ($orPrice-$discounted_price+$discounted_price_tax_amt)* $rowOrderPro->pro_quantity;//$proPrice * $rowOrderPro->pro_quantity;

$totalTax+= $discounted_price_tax_amt * $rowOrderPro->pro_quantity;//$rowOrderPro->Pro_tax * $rowOrderPro->pro_quantity;

$ProTotalPrice = ($discounted_price_tax_amt+$orPrice-$discounted_price)* $rowOrderPro->pro_quantity;//($rowOrderPro->pro_final_price) * $rowOrderPro->pro_quantity ;//remove tax changed by rumit

$freight_amount = $rowOrderPro->freight_amount;// * $rowOrderPro->pro_quantity;
"freight without GST value:".$freight_amount_with_gst = $rowOrderPro->freight_amount/1.18;// * $rowOrderPro->pro_quantity;
"<br>freight GST value:".$freight_gst_amount = $rowOrderPro->freight_amount-$freight_amount_with_gst;// * $rowOrderPro->pro_quantity;

$totalCost     = $totalCost + $ProTotalPrice;

/*		echo "<td valign='top' class='tblBorder_invoice_bottom ' nowrap='nowrap'>";

echo $Offer_Currency." "; 

echo number_format($ProTotalPrice,2);

echo "</td>";

echo "</tr>";

*/	

//		$discounted_price_tax_amt=($orPrice-$discounted_price)*$rowOrderPro->GST_percentage/100;				

}

$GST_tax_amt				= $this->GST_tax_amount_on_offer($pcode);//$rowOrderPro->GST_percentage;				

$TotalOrder	   				= $rowOrderTotal->total_order_cost;

$ship		   				= $rowOrderTotal->shipping_method_cost;

$shippingValue				= $ship;

$taxValue					= $rowOrderTotal->tax_cost;

$tax_included				= $rowOrderTotal->tax_included;

$tax_perc					= $rowOrderTotal->taxes_perc;

$discount_perc				= $rowOrderTotal->discount_perc;

$discount_per_amt			= $rowOrderTotal->discount_per_amt;

$show_discount				=$rowOrderTotal->show_discount;

//			$shippingValue 	= explode(" - ",$ship);

//			$subtotal       = $TotalOrder - $shippingValue-$taxValue ;

//$subtotal       			= $TotalOrder;

$subtotal_after_discount    = $TotalOrder - $subtotal1_dis;

if($tax_included=='Excluded')

{

@$subtotal_tax       		= $subtotal_after_discount*$tax_perc/100;

}

else

{

$subtotal_tax       		= $subtotal_after_discount;

}

if($tax_included=='Excluded')

{

$GrandTotalOrder			= $subtotal_after_discount + $subtotal_tax;

}

else

{

$GrandTotalOrder			= $subtotal_after_discount + 0;

}

//$subtotal_final		= $subtotal_tax;

$couponDiscount 			= $rowOrderTotal->coupon_discount;

}

echo "<table width='50%' border='0' cellpadding='5' cellspacing='0' >";

/*echo "<tr class='pagehead'>";

echo "<td colspan='2' class='pad' nowrap  align='left'>Order Total </td>";

echo "</tr>";*/

echo "<tr class='text'>";

echo "<td width='45%' class='pad tblBorder_invoice_bottom tblBorder_invoice_right tblBorder_invoice_left ' nowrap align='right'><strong>Sub Total :</strong> </td>";

echo "<td width='30%'  align='right' class='tblBorder_invoice_bottom ' nowrap='nowrap'> &nbsp; &nbsp;";

echo str_replace("backoffice","crm",$rowOrderTotal->Price_value)." ";

echo number_format($totalCost-$totalTax,2);

echo "</td>";

echo "</tr>";

//echo $show_discount;

if($show_discount=="Yes") {

echo "<tr class='text' style='display:none'>";

echo "<td  class='pad tblBorder_invoice_bottom tblBorder_invoice_right tblBorder_invoice_left' nowrap align='right'><strong>Discount :</strong></td>";

echo "<td   align='right'  class='tblBorder_invoice_bottom'  > &nbsp; &nbsp;";

echo str_replace("backoffice","crm",$rowOrderTotal->Price_value)." ";

echo number_format($subtotal1_dis,2);

echo "</td>";

echo "</tr>";

}

echo "<tr class='text'>";

echo "<td class='pad tblBorder_invoice_right tblBorder_invoice_bottom tblBorder_invoice_left' nowrap align='right'><strong>Freight Value :</strong></td>";

echo "<td align='right'  class='tblBorder_invoice_bottom' nowrap='nowrap' >&nbsp; &nbsp; ";

echo str_replace("backoffice","crm",$rowOrderTotal->Price_value)." ";

echo number_format($freight_amount-$freight_gst_amount,2);

echo "</td>";

echo "</tr>";$Grand_subtotal1=$subtotal1;

if($tax_included=='Excluded' || $GST_tax_amt>0)

{

//	echo "</tr>";

echo "<tr class='text'>";

if($GST_tax_amt>0)

{

echo "<td class='pad tblBorder_invoice_bottom tblBorder_invoice_right tblBorder_invoice_left' nowrap align='right'><strong>Add IGST :</strong></td>";

}

else

{

echo "<td class='pad tblBorder_invoice_bottom tblBorder_invoice_right tblBorder_invoice_left' nowrap align='right'><strong>Add VAT/CST@ $tax_perc% :</strong></td>";

}

echo "<td align='right'  class='tblBorder_invoice_bottom'  > &nbsp; &nbsp; ";

if($totalTax>0)

{

echo str_replace("backoffice","crm",$rowOrderTotal->Price_value)." ";

echo number_format($totalTax+$freight_gst_amount,2);//total gst value rumit 

}

else

{

echo str_replace("backoffice","crm",$rowOrderTotal->Price_value)." ";

$subtotal_tax1=$subtotal_tax;

echo number_format($subtotal_tax1,2);

}//echo $subtotal1;

$Grand_subtotal1=$totalCost;//$subtotal1+$subtotal_tax1+$totalTax;

echo "</td>";

echo "</tr>";

}

/*	

echo "<tr class='text'>";

echo "<td class='pad' nowrap align='right'>Shipping &amp; Handling </td>";

echo "<td  align='left'>: &nbsp; &nbsp; $currency1 ";

printf(" %.2f ",$ship);

echo "</td>";

echo "</tr>";

*/echo "<tr class='text'>";

echo "<td class='pad tblBorder_invoice_right tblBorder_invoice_left' nowrap align='right'><h4>Grand Total :</h4></td>";

echo "<td align='right'  nowrap='nowrap' ><h4> &nbsp; &nbsp; ";

echo str_replace("backoffice","crm",$rowOrderTotal->Price_value)." ";

//		printf(" %.2f",$TotalOrder);

echo number_format($totalCost+$freight_amount,2);

echo "</h4></td>";

echo "</tr>";

echo "</table>";

}

///////////////////////PI freight edit function
function proforma_invoice_total_edit($pcode)

{

$symbol		= $this->currencySymbol(1);

$currency1 	= $symbol[0];

$curValue 	= $symbol[1];

$sqlOrderTotal = "select * from tbl_order where orders_id = '$pcode'";

$rsOrderTotal  =  mysqli_query($GLOBALS["___mysqli_ston"],  $sqlOrderTotal);

if(mysqli_num_rows($rsOrderTotal)>0)

{

$rowOrderTotal 	= mysqli_fetch_object($rsOrderTotal);

/*	if($rowOrderTotal->order_type1!=0)

{

$TotalOrder	   	= $rowOrderTotal->total_order_cost+$rowOrderTotal->shipping_method_cost;

//$TotalOrder	   	= $rowOrderTotal->total_order_cost;

}

else

{

$TotalOrder	   	= $rowOrderTotal->total_order_cost;

}*/

$sqlOrderPro = "select * from tbl_order_product where order_id ='$pcode' order by order_pros_id asc";

$rsOrderPro	 = mysqli_query($GLOBALS["___mysqli_ston"],  $sqlOrderPro);

$h=0;

$subtotal1=0;

while($rowOrderPro = mysqli_fetch_object($rsOrderPro))

{

$h++;

$OrderProID	 = $rowOrderPro->order_pros_id;

$proID	 	 = $rowOrderPro->pro_id;

$groupID 	 = $rowOrderPro->group_id;

$orPrice 	 = $rowOrderPro->pro_price;

number_format($orPrice,2);

/*$GST_tax				=$rowOrderPro->Pro_tax;

echo "GSTSGSGST".$GST_percentage		=$this->GST_tax_amount_on_offer($pcode);//$rowOrderPro->GST_percentage;		

*/

$sql_dis="SELECT * FROM prowise_discount where orderid='".$pcode."' and proid='".$proID."'";

$sql_dis_dis=mysqli_query($GLOBALS["___mysqli_ston"],  $sql_dis);

$sql_dis_row=mysqli_fetch_object($sql_dis_dis);

$subtotal1=($orPrice*$rowOrderPro->pro_quantity)-$sql_dis_row->discount_amount;

$subtotal1_dis=$sql_dis_row->discount_amount;

$discounted_price=$orPrice*$sql_dis_row->discount_percent/100;			//discount amt per unit

$per_product_GST_percentage=$rowOrderPro->GST_percentage;//product gst %

$discounted_price_tax_amt=($orPrice-$discounted_price)*$rowOrderPro->GST_percentage/100;			

$SumProPrice = ($orPrice-$discounted_price+$discounted_price_tax_amt)* $rowOrderPro->pro_quantity;//$proPrice * $rowOrderPro->pro_quantity;

$totalTax+= $discounted_price_tax_amt * $rowOrderPro->pro_quantity;//$rowOrderPro->Pro_tax * $rowOrderPro->pro_quantity;

$ProTotalPrice = ($discounted_price_tax_amt+$orPrice-$discounted_price)* $rowOrderPro->pro_quantity;//($rowOrderPro->pro_final_price) * $rowOrderPro->pro_quantity ;//remove tax changed by rumit

$freight_amount = $rowOrderPro->freight_amount;// * $rowOrderPro->pro_quantity;
"freight without GST value:".$freight_amount_with_gst = $rowOrderPro->freight_amount/1.18;// * $rowOrderPro->pro_quantity;
"<br>freight GST value:".$freight_gst_amount = $rowOrderPro->freight_amount-$freight_amount_with_gst;// * $rowOrderPro->pro_quantity;

$totalCost     = $totalCost + $ProTotalPrice;

/*		echo "<td valign='top' class='tblBorder_invoice_bottom ' nowrap='nowrap'>";

echo $Offer_Currency." "; 

echo number_format($ProTotalPrice,2);

echo "</td>";

echo "</tr>";

*/	

//		$discounted_price_tax_amt=($orPrice-$discounted_price)*$rowOrderPro->GST_percentage/100;				

}

$GST_tax_amt				= $this->GST_tax_amount_on_offer($pcode);//$rowOrderPro->GST_percentage;				

$TotalOrder	   				= $rowOrderTotal->total_order_cost;

$ship		   				= $rowOrderTotal->shipping_method_cost;

$shippingValue				= $ship;

$taxValue					= $rowOrderTotal->tax_cost;

$tax_included				= $rowOrderTotal->tax_included;

$tax_perc					= $rowOrderTotal->taxes_perc;

$discount_perc				= $rowOrderTotal->discount_perc;

$discount_per_amt			= $rowOrderTotal->discount_per_amt;

$show_discount				=$rowOrderTotal->show_discount;

//			$shippingValue 	= explode(" - ",$ship);

//			$subtotal       = $TotalOrder - $shippingValue-$taxValue ;

//$subtotal       			= $TotalOrder;

$subtotal_after_discount    = $TotalOrder - $subtotal1_dis;

if($tax_included=='Excluded')

{

@$subtotal_tax       		= $subtotal_after_discount*$tax_perc/100;

}

else

{

$subtotal_tax       		= $subtotal_after_discount;

}

if($tax_included=='Excluded')

{

$GrandTotalOrder			= $subtotal_after_discount + $subtotal_tax;

}

else

{

$GrandTotalOrder			= $subtotal_after_discount + 0;

}

//$subtotal_final		= $subtotal_tax;

$couponDiscount 			= $rowOrderTotal->coupon_discount;

}

echo "<table width='50%' border='0' cellpadding='5' cellspacing='0' >";

/*echo "<tr class='pagehead'>";

echo "<td colspan='2' class='pad' nowrap  align='left'>Order Total </td>";

echo "</tr>";*/

echo "<tr class='text'>";

echo "<td width='45%' class='pad tblBorder_invoice_bottom tblBorder_invoice_right tblBorder_invoice_left ' nowrap align='right'><strong>Sub Total :</strong> </td>";

echo "<td width='30%'  align='right' class='tblBorder_invoice_bottom ' nowrap='nowrap'> &nbsp; &nbsp;";

echo str_replace("backoffice","crm",$rowOrderTotal->Price_value)." ";

echo number_format($totalCost-$totalTax,2);

echo "</td>";

echo "</tr>";

//echo $show_discount;

if($show_discount=="Yes") {

echo "<tr class='text' style='display:none'>";

echo "<td  class='pad tblBorder_invoice_bottom tblBorder_invoice_right tblBorder_invoice_left' nowrap align='right'><strong>Discount :</strong></td>";

echo "<td   align='right'  class='tblBorder_invoice_bottom'  > &nbsp; &nbsp;";

echo str_replace("backoffice","crm",$rowOrderTotal->Price_value)." ";

echo number_format($subtotal1_dis,2);

echo "</td>";

echo "</tr>";

}

echo "<tr class='text'>";

echo "<td class='pad tblBorder_invoice_right tblBorder_invoice_bottom tblBorder_invoice_left' nowrap align='right'><strong>Freight Value :</strong></td>";

echo "<td align='right'  class='tblBorder_invoice_bottom' nowrap='nowrap' >&nbsp; &nbsp; ";

echo str_replace("backoffice","crm",$rowOrderTotal->Price_value)." ";
$total_freight_amt_show=$freight_amount-$freight_gst_amount;
?>

<!--changed dropdown from text field on 24-5-2019 as disscssed with Mr. Pankaj & sandeep-->

<input type="text" name='freight_included' id='freight_included' value="<?php echo number_format($total_freight_amt_show);?>" class='form-control' required dir="rtl" style="width:70%; display:inline; padding:0; margin:0; height:auto" />
<?php

echo "</td>";

echo "</tr>";$Grand_subtotal1=$subtotal1;

if($tax_included=='Excluded' || $GST_tax_amt>0)

{

//	echo "</tr>";

echo "<tr class='text'>";

if($GST_tax_amt>0)

{

echo "<td class='pad tblBorder_invoice_bottom tblBorder_invoice_right tblBorder_invoice_left' nowrap align='right'><strong>Add IGST :</strong></td>";

}

else

{

echo "<td class='pad tblBorder_invoice_bottom tblBorder_invoice_right tblBorder_invoice_left' nowrap align='right'><strong>Add VAT/CST@ $tax_perc% :</strong></td>";

}

echo "<td align='right'  class='tblBorder_invoice_bottom'  > &nbsp; &nbsp; ";

if($totalTax>0)

{

echo str_replace("backoffice","crm",$rowOrderTotal->Price_value)." ";

echo number_format($totalTax+$freight_gst_amount,2);//total gst value rumit 

}

else

{

echo str_replace("backoffice","crm",$rowOrderTotal->Price_value)." ";

$subtotal_tax1=$subtotal_tax;

echo number_format($subtotal_tax1,2);

}//echo $subtotal1;

$Grand_subtotal1=$totalCost;//$subtotal1+$subtotal_tax1+$totalTax;

echo "</td>";

echo "</tr>";

}

/*	

echo "<tr class='text'>";

echo "<td class='pad' nowrap align='right'>Shipping &amp; Handling </td>";

echo "<td  align='left'>: &nbsp; &nbsp; $currency1 ";

printf(" %.2f ",$ship);

echo "</td>";

echo "</tr>";

*/echo "<tr class='text'>";

echo "<td class='pad tblBorder_invoice_right tblBorder_invoice_left' nowrap align='right'><h4>Grand Total :</h4></td>";

echo "<td align='right'  nowrap='nowrap' ><h4> &nbsp; &nbsp; ";

echo str_replace("backoffice","crm",$rowOrderTotal->Price_value)." ";

//		printf(" %.2f",$TotalOrder);

echo number_format($totalCost+$freight_amount,2);

echo "</h4></td>";

echo "</tr>";

echo "</table>";}/////////// this function use to display the order Total amount info	

function OrderTotalInfo_sales_home_letter($pcode)

{

$symbol		= $this->currencySymbol(1);

$currency1 	= $symbol[0];

$curValue 	= $symbol[1];

$sqlOrderTotal = "select * from tbl_order where orders_id = '$pcode'";

$rsOrderTotal  =  mysqli_query($GLOBALS["___mysqli_ston"],  $sqlOrderTotal);

if(mysqli_num_rows($rsOrderTotal)>0)

{

$rowOrderTotal 	= mysqli_fetch_object($rsOrderTotal);

/*	if($rowOrderTotal->order_type1!=0)

{

$TotalOrder	   	= $rowOrderTotal->total_order_cost+$rowOrderTotal->shipping_method_cost;

//$TotalOrder	   	= $rowOrderTotal->total_order_cost;

}

else

{

$TotalOrder	   	= $rowOrderTotal->total_order_cost;

}*/

$sqlOrderPro = "select * from tbl_order_product where order_id ='$pcode' order by order_pros_id asc ";

$rsOrderPro	 = mysqli_query($GLOBALS["___mysqli_ston"],  $sqlOrderPro);

$h=0;

$subtotal1=0;

while($rowOrderPro = mysqli_fetch_object($rsOrderPro))

{

$h++;

$OrderProID	 = $rowOrderPro->order_pros_id;

$proID	 	 = $rowOrderPro->pro_id;

$groupID 	 = $rowOrderPro->group_id;

$sqlAttr = "select * from tbl_group where group_id='$groupID'";

$rsAttr  = mysqli_query($GLOBALS["___mysqli_ston"],  $sqlAttr);

$i  	 = -1;

$k  	 = 0;

$cb 	 = 0;

$tx		 = 0;

$ta		 = 0;

if($cb != 0)

{

}

if($tx!=0)

{

}

if($ta != 0)

{

}

$ManufacturerID = $rowOrderPro->manufacturers_id;

$orPrice = $rowOrderPro->pro_price;

number_format($orPrice,2);

$sql_dis="SELECT * FROM prowise_discount where orderid='".$pcode."' and proid='".$proID."'";

$sql_dis_dis=mysqli_query($GLOBALS["___mysqli_ston"],  $sql_dis);

$sql_dis_row=mysqli_fetch_object($sql_dis_dis);

$subtotal1+=($orPrice*$rowOrderPro->pro_quantity)-$sql_dis_row->discount_amount;

$subtotal1_dis+=$sql_dis_row->discount_amount;

}

$TotalOrder	   				= $rowOrderTotal->total_order_cost;

$ship		   				= $rowOrderTotal->shipping_method_cost;

$shippingValue				= $ship;

$taxValue					= $rowOrderTotal->tax_cost;

$tax_included				= $rowOrderTotal->tax_included;

$tax_perc					= $rowOrderTotal->taxes_perc;

$discount_perc				= $rowOrderTotal->discount_perc;

$discount_per_amt			= $rowOrderTotal->discount_per_amt;

$show_discount				=$rowOrderTotal->show_discount;

//			$shippingValue 	= explode(" - ",$ship);

//			$subtotal       = $TotalOrder - $shippingValue-$taxValue ;

//$subtotal       			= $TotalOrder;

$subtotal_after_discount    = $TotalOrder - $subtotal1_dis;

if($tax_included=='Excluded')

{

@$subtotal_tax       		= $subtotal_after_discount*$tax_perc/100;

}

else

{

$subtotal_tax       		= $subtotal_after_discount;

}

if($tax_included=='Excluded')

{

$GrandTotalOrder			= $subtotal_after_discount + $subtotal_tax;

}

else

{

$GrandTotalOrder			= $subtotal_after_discount + 0;

}

//$subtotal_final		= $subtotal_tax;

$couponDiscount 			= $rowOrderTotal->coupon_discount;

}

"<table width='50%' border='0' cellpadding='5' cellspacing='0' >";

/* "<tr class='pagehead'>";

"<td colspan='2' class='pad' nowrap  align='left'>Order Total </td>";

"</tr>";*/

"<tr class='text'>";

"<td width='70%' class='pad tblBorder_invoice_bottom tblBorder_invoice_right tblBorder_invoice_left ' nowrap align='right'><strong>Sub Total :</strong> </td>";

"<td width='30%'  align='right' class='tblBorder_invoice_bottom ' nowrap='nowrap'> &nbsp; &nbsp;";

str_replace("backoffice","crm",$rowOrderTotal->Price_value)." ";

number_format($subtotal1,2);

"</td>";

"</tr>";

// $show_discount;

if($show_discount=="Yes") {

"<tr class='text' style='display:none'>";

"<td  class='pad tblBorder_invoice_bottom tblBorder_invoice_right tblBorder_invoice_left' nowrap align='right'><strong>Discount :</strong></td>";

"<td   align='right'  class='tblBorder_invoice_bottom'  > &nbsp; &nbsp;";

str_replace("backoffice","crm",$rowOrderTotal->Price_value)." ";

number_format($subtotal1_dis,2);

"</td>";

"</tr>";

}

$Grand_subtotal1=$subtotal1;

if($tax_included=='Excluded')

{

"</tr>";

"<tr class='text'>";

"<td class='pad tblBorder_invoice_bottom tblBorder_invoice_right tblBorder_invoice_left' nowrap align='right'><strong>Add CST@ $tax_perc% :</strong></td>";

"<td align='right'  class='tblBorder_invoice_bottom'  > &nbsp; &nbsp; ";

str_replace("backoffice","crm",$rowOrderTotal->Price_value)." ";

$subtotal_tax1=$subtotal_tax;

number_format($subtotal_tax1,2);

//echo $subtotal1;

$Grand_subtotal1=$subtotal1+$subtotal_tax1;

"</td>";

"</tr>";

}

/*	

echo "<tr class='text'>";

echo "<td class='pad' nowrap align='right'>Shipping &amp; Handling </td>";

echo "<td  align='left'>: &nbsp; &nbsp; $currency1 ";

printf(" %.2f ",$ship);

echo "</td>";

echo "</tr>";

*/

"<tr class='text'>";

"<td class='pad tblBorder_invoice_right tblBorder_invoice_left' nowrap align='right'><h4>Grand Total :</h4></td>";

"<td align='right'  nowrap='nowrap' ><h4> &nbsp; &nbsp; ";

str_replace("backoffice","crm",$rowOrderTotal->Price_value)." ";

//		printf(" %.2f",$TotalOrder);

number_format($Grand_subtotal1,2);

"</h4></td>";

"</tr>";

"</table>";

return $Grand_subtotal1;

}
///////////////////////////////////////

function OrderItemsInfo_invoice($pcode)

{

$sql_dis_currency="SELECT Price_value FROM tbl_order where orders_id='".$pcode."'";

$sql_dis_dis_currency=mysqli_query($GLOBALS["___mysqli_ston"],  $sql_dis_currency);

$sql_dis_row_currency=mysqli_fetch_object($sql_dis_dis_currency);

$Offer_Currency=$sql_dis_row_currency->Price_value;

$Offer_Currency=str_replace("backoffice","crm",$Offer_Currency);$symbol		= $this->currencySymbol(1);

$currency1 	= $symbol[0];

$curValue 	= $symbol[1];

$totalCost  = 0;

echo "<table width='100%' border='0' cellpadding='5' cellspacing='0' class='tblBorder_invoice_right   tblBorder_invoice_left'>";

//echo "<tr class='pagehead'><td colspan='11' class='pad'>Item(s) Information </td></tr>";

echo "<tr class='head'>";

echo "<td class='pad tblBorder_invoice_bottom tblBorder_invoice_right' width='2%' nowrap='nowrap'>S.No</td>";

echo "<td width='10%' class='pad tblBorder_invoice_bottom tblBorder_invoice_right'>Product Name</td>";

echo "<td width='10%' class='pad tblBorder_invoice_bottom tblBorder_invoice_right'>Part No.</td>";

echo "<td width='30%' class='pad tblBorder_invoice_bottom tblBorder_invoice_right'>Description</td>";

echo "<td width='5%' class='tblBorder_invoice_bottom tblBorder_invoice_right'>Qty<strong></strong></td><td width='10%' class='tblBorder_invoice_bottom tblBorder_invoice_right' nowrap='nowrap'><strong>Unit Price</strong></td>";

$sql_dis="SELECT show_discount FROM prowise_discount where orderid='".$pcode."'";

$sql_dis_dis=mysqli_query($GLOBALS["___mysqli_ston"],  $sql_dis);

$sql_dis_row=mysqli_fetch_object($sql_dis_dis);

if($sql_dis_row->show_discount=="Yes") {

echo "<td width='10%' class='tblBorder_invoice_bottom tblBorder_invoice_right' nowrap='nowrap'>Discount(-)<strong></strong></td>";

$dis_td=1;

}

else

{

}

echo "<td nowrap='nowrap' width='10%' class='tblBorder_invoice_bottom ' nowrap='nowrap'><strong>Total</strong></td></tr>";

//		echo "<tr><td height='1px' colspan='11' class='tblBorder_invoice_bottom'>hf</td></tr>"; //blank line

$sqlOrderPro = "select * from tbl_order_product where order_id ='$pcode' order by order_pros_id asc ";

$rsOrderPro	 = mysqli_query($GLOBALS["___mysqli_ston"],  $sqlOrderPro);

$h=0;

while($rowOrderPro = mysqli_fetch_object($rsOrderPro))

{

$h++;

echo "<tr class='text'>";

echo "<td class='pad tblBorder_invoice_bottom tblBorder_invoice_right' >".$h."</td>";

echo "<td class='pad tblBorder_invoice_bottom tblBorder_invoice_right' valign='top'><strong>".$rowOrderPro->pro_name."</strong><br />";

$OrderProID	 = $rowOrderPro->order_pros_id;

$proID	 	 = $rowOrderPro->pro_id;

$groupID 	 = $rowOrderPro->group_id;

$sqlAttr = "select * from tbl_group where group_id='$groupID'";

$rsAttr  = mysqli_query($GLOBALS["___mysqli_ston"],  $sqlAttr);

$i  	 = -1;

$k  	 = 0;

$cb 	 = 0;

$tx		 = 0;

$ta		 = 0;

if($cb != 0)

{

}

if($tx!=0)

{

}

if($ta != 0)

{

}

echo "&nbsp;<span class='prdname red required '><strong>".$this->pro_text_ordered($proID,$pcode)."</strong></span>";		  

echo "</td>";

echo "<td valign='top' class='tblBorder_invoice_bottom tblBorder_invoice_right'> $rowOrderPro->pro_model </td>";

echo "<td valign='top' class='tblBorder_invoice_bottom tblBorder_invoice_right'>".str_replace("�"," ",$this->proidentrydesc($rowOrderPro->proidentry))."</td>";

echo "<td valign='top' class='tblBorder_invoice_bottom tblBorder_invoice_right'> $rowOrderPro->pro_quantity </td>";

$ManufacturerID = $rowOrderPro->manufacturers_id;

/*			$sqlManuF		= "select * from tbl_manufacturers where manufacturers_id = $ManufacturerID";

$rsManuF		= mysqli_query($GLOBALS["___mysqli_ston"],  $sqlManuF);

if(mysqli_num_rows($rsManuF)>0)

{

$rowManuF  = mysqli_fetch_object($rsManuF);

$ManuFname = $rowManuF->manufacturers_name;

}*/

$orPrice = $rowOrderPro->pro_price;

echo "<td valign='top' class='tblBorder_invoice_bottom tblBorder_invoice_right' nowrap='nowrap'> ";

echo $Offer_Currency." ";

echo number_format($orPrice,2);

echo "</td>";

$sql_dis="SELECT * FROM prowise_discount where orderid='".$pcode."' and proid='".$proID."'";

$sql_dis_dis=mysqli_query($GLOBALS["___mysqli_ston"],  $sql_dis);

$sql_dis_row=mysqli_fetch_object($sql_dis_dis);

if($sql_dis_row->show_discount=="Yes" || $dis_td) {

echo "<td valign='top' class='tblBorder_invoice_bottom tblBorder_invoice_right' nowrap='nowrap'>";

echo $Offer_Currency." "; 

echo number_format($sql_dis_row->discount_amount,2);

echo "<br />";

echo " (".$sql_dis_row->discount_percent."%)";

echo "</td>";

}

else

{

}

$SumProPrice = $proPrice * $rowOrderPro->pro_quantity;

$totalTax = $rowOrderPro->Pro_tax * $rowOrderPro->pro_quantity ;

$ProTotalPrice = ($rowOrderPro->pro_final_price) * $rowOrderPro->pro_quantity ;//remove tax changed by rumit

$totalCost     = $totalCost + $ProTotalPrice ;

echo "<td valign='top' class='tblBorder_invoice_bottom ' nowrap='nowrap'>";

echo $Offer_Currency." "; 

echo number_format($ProTotalPrice-$sql_dis_row->discount_amount,2);

echo "</td>";

echo "</tr>";

//			echo "<tr bgcolor='#F6F6F6'><td colspan='11' height='2'></td></tr>";

}

echo "</table>";

}

///////////////////////////////////////////////////////////////////////////////

function OrderItemsInfo_invoice1($pcode)
{
$sql_dis_currency="SELECT Price_value,Price_type FROM tbl_order where orders_id='".$pcode."'";
$sql_dis_dis_currency=mysqli_query($GLOBALS["___mysqli_ston"],  $sql_dis_currency);
$sql_dis_row_currency=mysqli_fetch_object($sql_dis_dis_currency);
$Price_type=$sql_dis_row_currency->Price_type;
$Offer_Currency=$sql_dis_row_currency->Price_value;
$Offer_Currency=str_replace("backoffice","crm",$Offer_Currency);
$symbol		= $this->currencySymbol(1);
$currency1 	= $symbol[0];
$curValue 	= $symbol[1];
$totalCost  = 0;
$sql_dis="SELECT show_discount FROM prowise_discount where orderid='".$pcode."'";
$sql_dis_dis=mysqli_query($GLOBALS["___mysqli_ston"],  $sql_dis);
$sql_dis_row=mysqli_fetch_object($sql_dis_dis);echo "<table width='100%' border='0' cellpadding='5' cellspacing='0' class='tblBorder_invoice_right   tblBorder_invoice_left'>";
//echo "<tr class='pagehead'><td colspan='11' class='pad'>Item(s) Information </td></tr>";
echo "<tr class='head'>";
echo "<td class='pad tblBorder_invoice_bottom tblBorder_invoice_right' width='2%' nowrap='nowrap'   >S.No</td>";
echo "<td width='10%' class='pad tblBorder_invoice_bottom tblBorder_invoice_right'>Product Name</td>";
echo "<td width='2%' class='pad tblBorder_invoice_bottom tblBorder_invoice_right'>UPC Code</td>";
echo "<td width='2%' class='pad tblBorder_invoice_bottom tblBorder_invoice_right'>HSN Code</td>";
echo "<td width='2%' class='pad tblBorder_invoice_bottom tblBorder_invoice_right'>Part No.</td>";
echo "<td width='40%' class='pad tblBorder_invoice_bottom tblBorder_invoice_right'>Description</td>";
echo "<td width='2%' class='tblBorder_invoice_bottom tblBorder_invoice_right' align='center'>Qty<br>A</td>
<td width='5%' class='tblBorder_invoice_bottom tblBorder_invoice_right' nowrap='nowrap' align='center' ><strong>Unit Price<br>B</strong></td>";//echo "SHOW dis:--".$sql_dis_row->show_discount;
if($sql_dis_row->show_discount=="Yes") {
echo "<td width='3%' class='tblBorder_invoice_bottom tblBorder_invoice_right' nowrap='nowrap' align='center'>Discount(-)<br>C</td>";
$dis_td=1;
}
else
{
}
echo "<td width='3%' class='pad tblBorder_invoice_bottom tblBorder_invoice_right' align='center'>Add IGST Value (%)<br>D</td>";
//echo "<td width='10%' class='pad tblBorder_invoice_bottom tblBorder_invoice_right' >GST %</td>";
if($sql_dis_row->show_discount=="Yes") {
echo "<td width='5%' class='tblBorder_invoice_bottom ' nowrap='nowrap' align='center'><strong>Sub Total</strong><br>=A*(B-C)</td></tr>";
}
else
{
echo "<td width='5%' class='tblBorder_invoice_bottom ' nowrap='nowrap' align='center'><strong>Sub Total</strong><br>=A*B</td></tr>";
}
//		echo "<tr><td height='1px' colspan='11' class='tblBorder_invoice_bottom'>hf</td></tr>"; //blank line
$sqlOrderPro = "select * from tbl_order_product where order_id ='$pcode' order by order_pros_id desc ";
$rsOrderPro	 = mysqli_query($GLOBALS["___mysqli_ston"],  $sqlOrderPro);
$h=0;
while($rowOrderPro = mysqli_fetch_object($rsOrderPro))
{
$h++;
echo "<tr class='text'>";
echo "<td class='pad tblBorder_invoice_bottom tblBorder_invoice_right' >".$h."</td>";
echo "<td class='pad tblBorder_invoice_bottom tblBorder_invoice_right' valign='middle'><strong>".$rowOrderPro->pro_name."</strong><br />";
$OrderProID	 = $rowOrderPro->order_pros_id;
$proID	 	 = $rowOrderPro->pro_id;
$groupID 	 = $rowOrderPro->group_id;

$hsn_code=$rowOrderPro->hsn_code;
$upc_code=$this->pro_upc_code($proID);//$rowOrderPro->hsn_code;
if($hsn_code==''){
$hsn_code="N/A";
}
echo "</td>";
echo "<td valign='middle' class='tblBorder_invoice_bottom tblBorder_invoice_right' width='2%'> $upc_code </td>";
echo "<td valign='middle' class='tblBorder_invoice_bottom tblBorder_invoice_right' width='2%'> $hsn_code </td>";
echo "<td valign='middle' class='tblBorder_invoice_bottom tblBorder_invoice_right' width='2%'> $rowOrderPro->pro_model </td>";
echo "<td valign='middle' class='tblBorder_invoice_bottom tblBorder_invoice_right'>".str_replace("�"," ",$this->proidentrydesc($rowOrderPro->proidentry))."</td>";
echo "<td valign='middle' class='tblBorder_invoice_bottom tblBorder_invoice_right' width='2%'> $rowOrderPro->pro_quantity </td>";
$ManufacturerID = $rowOrderPro->manufacturers_id;
/*			$sqlManuF		= "select * from tbl_manufacturers where manufacturers_id = $ManufacturerID";
$rsManuF		= mysqli_query($GLOBALS["___mysqli_ston"],  $sqlManuF);
if(mysqli_num_rows($rsManuF)>0)
{
$rowManuF  = mysqli_fetch_object($rsManuF);
$ManuFname = $rowManuF->manufacturers_name;
}*/
$orPrice = $rowOrderPro->pro_price;
echo "<td valign='middle' class='tblBorder_invoice_bottom tblBorder_invoice_right' nowrap='nowrap' > ";
echo $Offer_Currency." ";
echo $per_product_price=number_format($orPrice,2);
echo "</td>";
$sql_dis="SELECT * FROM prowise_discount where orderid='".$pcode."' and proid='".$proID."'";
$sql_dis_dis=mysqli_query($GLOBALS["___mysqli_ston"],  $sql_dis);
$sql_dis_row=mysqli_fetch_object($sql_dis_dis);
if($sql_dis_row->show_discount=="Yes" || $dis_td) {
echo "<td valign='middle' class='tblBorder_invoice_bottom tblBorder_invoice_right' nowrap='nowrap' width='3%'>";
echo $Offer_Currency." "; 
//	echo "ABC:=". number_format($sql_dis_row->discount_amount,2);
echo "".			$discounted_price=$orPrice*$sql_dis_row->discount_percent/100;			
echo "<br />";
echo " (".$sql_dis_row->discount_percent."%)";
echo "</td>";
}
else
{
}
echo "<td valign='middle' class='tblBorder_invoice_bottom tblBorder_invoice_right' nowrap='nowrap' width='3%'> ";
echo $Offer_Currency." ";
if($Price_type!="Export_USD")
{
//echo "aaaaa".$per_product_tax=$rowOrderPro->Pro_tax;
$rowOrderPro->GST_percentage;
}
else
{
	$rowOrderPro->GST_percentage="0";
}
echo "".$discounted_price_tax_amt=($orPrice-$discounted_price)*$rowOrderPro->GST_percentage/100;			
echo "<br>(".$per_product_GST_percentage=str_replace("%","",$rowOrderPro->GST_percentage)."%";
echo ")</td>";
/*echo "<td valign='top' class='tblBorder_invoice_bottom tblBorder_invoice_right' nowrap='nowrap'> ";
echo $Offer_Currency." ";
echo $per_product_GST_percentage=$rowOrderPro->GST_percentage;
echo "</td>";
*/	
$SumProPrice = ($orPrice-$discounted_price+$discounted_price_tax_amt)* $rowOrderPro->pro_quantity;//$proPrice * $rowOrderPro->pro_quantity;
$totalTax = $discounted_price_tax_amt * $rowOrderPro->pro_quantity;//$rowOrderPro->Pro_tax * $rowOrderPro->pro_quantity;
$ProTotalPrice = ($orPrice-$discounted_price)* $rowOrderPro->pro_quantity;//($rowOrderPro->pro_final_price) * $rowOrderPro->pro_quantity ;//remove tax changed by rumit
$totalCost     = $totalCost + $ProTotalPrice ;
echo "<td valign='middle' class='tblBorder_invoice_bottom ' nowrap='nowrap'>";
echo $Offer_Currency." "; 
echo number_format($ProTotalPrice,2);
echo "</td>";
echo "</tr>";
//			echo "<tr bgcolor='#F6F6F6'><td colspan='11' height='2'></td></tr>";
}
echo "</table>";
}

///////////////////////////////////////////////////////////////////////////////
function PerformaItemsInfo_invoice1($pcode)

{

$sql_dis_currency="SELECT Price_value FROM tbl_order where orders_id='".$pcode."'";

$sql_dis_dis_currency=mysqli_query($GLOBALS["___mysqli_ston"],  $sql_dis_currency);

$sql_dis_row_currency=mysqli_fetch_object($sql_dis_dis_currency);

$Offer_Currency=$sql_dis_row_currency->Price_value;

$Offer_Currency=str_replace("backoffice","crm",$Offer_Currency);

$symbol		= $this->currencySymbol(1);

$currency1 	= $symbol[0];

$curValue 	= $symbol[1];

$totalCost  = 0;

echo "<table width='100%' border='0' cellpadding='5' cellspacing='0' class='tblBorder_invoice_right   tblBorder_invoice_left'>";

//echo "<tr class='pagehead'><td colspan='11' class='pad'>Item(s) Information </td></tr>";

echo "<tr class='head'>";

echo "<td class='pad tblBorder_invoice_bottom tblBorder_invoice_right' width='2%' nowrap='nowrap'><strong>S.No</strong></td>";

echo "<td width='50%' class='pad tblBorder_invoice_bottom tblBorder_invoice_right' ><strong>Product Name</strong></td>";

echo "<td width='10%' class='pad tblBorder_invoice_bottom tblBorder_invoice_right' ><strong>HSN Code</strong></td>";echo "<td width='5%' class='tblBorder_invoice_bottom tblBorder_invoice_right' align='center'><strong>Qty</strong></td>

<td width='10%' class='tblBorder_invoice_bottom tblBorder_invoice_right' nowrap='nowrap' align='center' ><strong>Rate</strong></td>";

$sql_dis="SELECT show_discount FROM prowise_discount where orderid='".$pcode."'";

$sql_dis_dis=mysqli_query($GLOBALS["___mysqli_ston"],  $sql_dis);

$sql_dis_row=mysqli_fetch_object($sql_dis_dis);

if($sql_dis_row->show_discount=="Yes") {

echo "<td width='10%' class='tblBorder_invoice_bottom tblBorder_invoice_right' nowrap='nowrap' align='center' style='display:none'><strong>Discount</strong></td>";

$dis_td=1;

}

else

{

}

echo "<td width='10%' class='pad tblBorder_invoice_bottom tblBorder_invoice_right' align='center' style='display:none'><strong>Add IGST Value (%)</strong></td>";

//echo "<td width='10%' class='pad tblBorder_invoice_bottom tblBorder_invoice_right' >GST %</td>";

echo "<td nowrap='nowrap' width='10%' class='tblBorder_invoice_bottom ' nowrap='nowrap' align='center'><strong>Amount</strong></td></tr>";

//		echo "<tr><td height='1px' colspan='11' class='tblBorder_invoice_bottom'>hf</td></tr>"; //blank line

$sqlOrderPro = "select * from tbl_order_product where order_id ='$pcode' order by order_pros_id asc ";

$rsOrderPro	 = mysqli_query($GLOBALS["___mysqli_ston"],  $sqlOrderPro);

$h=0;

while($rowOrderPro = mysqli_fetch_object($rsOrderPro))

{

$h++;

echo "<tr class='text'>";

echo "<td class='pad tblBorder_invoice_bottom tblBorder_invoice_right' >".$h."</td>";

echo "<td class='pad tblBorder_invoice_bottom tblBorder_invoice_right' valign='top'><strong>".$rowOrderPro->pro_name."</strong><br />";

$OrderProID	 = $rowOrderPro->order_pros_id;

$proID	 	 = $rowOrderPro->pro_id;

$groupID 	 = $rowOrderPro->group_id;

$sqlAttr = "select * from tbl_group where group_id='$groupID'";

$rsAttr  = mysqli_query($GLOBALS["___mysqli_ston"],  $sqlAttr);

$i  	 = -1;

$k  	 = 0;

$cb 	 = 0;

$tx		 = 0;

$ta		 = 0;

if($cb != 0)

{

}

if($tx!=0)

{

}

if($ta != 0)

{

}

$hsn_code=$rowOrderPro->hsn_code;

if($hsn_code==''){

$hsn_code="N/A";

}

echo "</td>";

echo "<td valign='top' class='tblBorder_invoice_bottom tblBorder_invoice_right'> $hsn_code </td>";

//echo "<td valign='top' class='tblBorder_invoice_bottom tblBorder_invoice_right'> $rowOrderPro->pro_model </td>";

//echo "<td valign='top' class='tblBorder_invoice_bottom tblBorder_invoice_right'>".str_replace("�"," ",$this->proidentrydesc($rowOrderPro->proidentry))."</td>";

echo "<td valign='top' align='center' class='tblBorder_invoice_bottom tblBorder_invoice_right'> $rowOrderPro->pro_quantity </td>";

$ManufacturerID = $rowOrderPro->manufacturers_id;

/*			$sqlManuF		= "select * from tbl_manufacturers where manufacturers_id = $ManufacturerID";

$rsManuF		= mysqli_query($GLOBALS["___mysqli_ston"],  $sqlManuF);

if(mysqli_num_rows($rsManuF)>0)

{

$rowManuF  = mysqli_fetch_object($rsManuF);

$ManuFname = $rowManuF->manufacturers_name;

}*/

$orPrice = $rowOrderPro->pro_price;

$sql_dis="SELECT * FROM prowise_discount where orderid='".$pcode."' and proid='".$proID."'";

$sql_dis_dis=mysqli_query($GLOBALS["___mysqli_ston"],  $sql_dis);

$sql_dis_row=mysqli_fetch_object($sql_dis_dis);

if($sql_dis_row->show_discount=="Yes" || $dis_td) {

echo "<td valign='top' align='center' class='tblBorder_invoice_bottom tblBorder_invoice_right' nowrap='nowrap' style='display:none'>";

echo $Offer_Currency." "; 

//	echo "ABC:=". number_format($sql_dis_row->discount_amount,2);

echo "".			$discounted_price=$orPrice*$sql_dis_row->discount_percent/100;			

echo "<br />";

echo " (".$sql_dis_row->discount_percent."%)";

echo "</td>";

}

else

{
	$discounted_price=$orPrice*$sql_dis_row->discount_percent/100;			//added by rumit on 16-5-2019
}echo "<td valign='top' align='center' class='tblBorder_invoice_bottom tblBorder_invoice_right' nowrap='nowrap'> ";

echo $Offer_Currency." ";

echo $per_product_price=number_format($orPrice-$discounted_price,2);

echo "</td>";echo "<td valign='top' align='center' class='tblBorder_invoice_bottom tblBorder_invoice_right' nowrap='nowrap' style='display:none'> ";

echo $Offer_Currency." ";

//	echo $per_product_tax=$rowOrderPro->Pro_tax;

echo "".$discounted_price_tax_amt=($orPrice-$discounted_price)*$rowOrderPro->GST_percentage/100;			

echo "<br>(".$per_product_GST_percentage=str_replace("%","",$rowOrderPro->GST_percentage)."%";

echo ")</td>";

/*echo "<td valign='top' class='tblBorder_invoice_bottom tblBorder_invoice_right' nowrap='nowrap'> ";

echo $Offer_Currency." ";

echo $per_product_GST_percentage=$rowOrderPro->GST_percentage;

echo "</td>";

*/	

$SumProPrice = ($orPrice-$discounted_price+$discounted_price_tax_amt)* $rowOrderPro->pro_quantity;//$proPrice * $rowOrderPro->pro_quantity;

$totalTax = $discounted_price_tax_amt * $rowOrderPro->pro_quantity;//$rowOrderPro->Pro_tax * $rowOrderPro->pro_quantity;

$ProTotalPrice = ($orPrice-$discounted_price)* $rowOrderPro->pro_quantity;//($rowOrderPro->pro_final_price) * $rowOrderPro->pro_quantity ;//remove tax changed by rumit

$totalCost     = $totalCost + $ProTotalPrice ;

echo "<td valign='top' align='center' class='tblBorder_invoice_bottom ' nowrap='nowrap'>";

echo $Offer_Currency." "; 

echo number_format($ProTotalPrice,2);

echo "</td>";

echo "</tr>";

//			echo "<tr bgcolor='#F6F6F6'><td colspan='11' height='2'></td></tr>";

}

echo "</table>";

}
///////////////////////////////////////

//added on 21-may-2020
function quantity_slab_max_discount($proid,$pro_quantity)
{
$sql_max_dis_qty_wise="select max_discount_percent from tbl_pro_qty_max_discount_percentage where min_qty <=$pro_quantity AND max_qty>=$pro_quantity AND proid='$proid'";
	$sql_max_dis_query_qty_wise=mysqli_query($GLOBALS["___mysqli_ston"],$sql_max_dis_qty_wise);
	$sql_max_dis_data_qty_wise=mysqli_fetch_object($sql_max_dis_query_qty_wise);
	"<br>qty_wise_max_disc:".$max_dis_last_qty_wise=$sql_max_dis_data_qty_wise->max_discount_percent;
	
	return $max_dis_last_qty_wise;
}

function quantity_slab($proid)
{
		"<br>".$sql_qty_slab_pro_id = "select qty_slab from tbl_products where pro_id = '$proid' and deleteflag = 'active'";
		$rs_qty_slab_pro_id  = mysqli_query($GLOBALS["___mysqli_ston"],$sql_qty_slab_pro_id);
		$row_qty_slab_pro_id = mysqli_fetch_object($rs_qty_slab_pro_id);
		$quantity_slab	  = $row_qty_slab_pro_id->qty_slab;
	return $quantity_slab;
}
function OrderItemsInfo_invoice1_edit($pcode)

{

$sql_dis_currency="SELECT Price_value FROM tbl_order where orders_id='".$pcode."'";

$sql_dis_dis_currency=mysqli_query($GLOBALS["___mysqli_ston"],  $sql_dis_currency);

$sql_dis_row_currency=mysqli_fetch_object($sql_dis_dis_currency);

$Offer_Currency=$sql_dis_row_currency->Price_value;

$Offer_Currency=str_replace("backoffice","crm",$Offer_Currency);

$symbol		= $this->currencySymbol(1);

$currency1 	= $symbol[0];

$curValue 	= $symbol[1];

$totalCost  = 0;

echo "<table width='100%' border='0' cellpadding='5' cellspacing='0' class='tblBorder_invoice_right   tblBorder_invoice_left'>";

//echo "<tr class='pagehead'><td colspan='11' class='pad'>Item(s) Information </td></tr>";

echo "<tr class='head'>";
echo "<td  class='tblBorder_invoice_bottom tblBorder_invoice_right'>Delete</td>";
echo "<td class='pad tblBorder_invoice_bottom tblBorder_invoice_right' width='2%' nowrap='nowrap'   >S.No</td>";

echo "<td width='10%' class='pad tblBorder_invoice_bottom tblBorder_invoice_right' >Product Name</td>";

echo "<td width='10%' class='pad tblBorder_invoice_bottom tblBorder_invoice_right' >HSN Code</td>";

echo "<td width='10%' class='pad tblBorder_invoice_bottom tblBorder_invoice_right' >Part No.</td>";

echo "<td width='20%' class='pad tblBorder_invoice_bottom tblBorder_invoice_right' >Description</td>";

echo "<td width='5%' class='tblBorder_invoice_bottom tblBorder_invoice_right'>Qty<br>A</td>

<td width='10%' class='tblBorder_invoice_bottom tblBorder_invoice_right' nowrap='nowrap'><strong>Unit Price</strong><br>B</td>";

$sql_dis="SELECT show_discount FROM prowise_discount where orderid='".$pcode."'";

$sql_dis_dis=mysqli_query($GLOBALS["___mysqli_ston"],  $sql_dis);

$sql_dis_row=mysqli_fetch_object($sql_dis_dis);

//if($sql_dis_row->show_discount=="Yes") {

echo "<td width='10%' class='tblBorder_invoice_bottom tblBorder_invoice_right' nowrap='nowrap'>Discount(-)<br>C</td>";

/*$dis_td=1;

}

else

{

}*/

echo "<td width='10%' class='pad tblBorder_invoice_bottom tblBorder_invoice_right' >Add IGST Value(%)<br>D</td>";

//		echo "<td width='10%' class='pad tblBorder_invoice_bottom tblBorder_invoice_right' >GST %</td>";

echo "<td nowrap='nowrap' width='10%' class='tblBorder_invoice_bottom ' nowrap='nowrap'><strong>Sub Total</strong><br>=A*(B-C)</td></tr>";
$sqlOrderPro = "select * from tbl_order_product where order_id ='$pcode' order by order_pros_id desc ";
$rsOrderPro	 = mysqli_query($GLOBALS["___mysqli_ston"],  $sqlOrderPro);
$h=0;
while($rowOrderPro = mysqli_fetch_object($rsOrderPro))
{
$h++;
echo "<tr class='text'>";
?>
<td class='pad tblBorder_invoice_bottom tblBorder_invoice_right'><a href="offer_delivery_challan.php?action=delete&pcode=<?php echo $_REQUEST["pcode"];?>&ID=<?php echo $rowOrderPro->pro_id;?>&offercode=<?php echo $_REQUEST["offercode"];?>"  onclick="return del();"> <img src="images/x.gif"  border="0" title="Delete" /></a></td>
<?php
echo "<td class='pad tblBorder_invoice_bottom tblBorder_invoice_right' >".$h."</td>";
echo "<td class='pad tblBorder_invoice_bottom tblBorder_invoice_right' valign='middle'><strong>".$rowOrderPro->pro_name."</strong><br />";
$OrderProID	 = $rowOrderPro->order_pros_id;
$proID	 	 = $rowOrderPro->pro_id;
$groupID 	 = $rowOrderPro->group_id;
$sqlAttr = "select * from tbl_group where group_id='$groupID'";
$rsAttr  = mysqli_query($GLOBALS["___mysqli_ston"],  $sqlAttr);
$i  	 = -1;
$k  	 = 0;
$cb 	 = 0;
$tx		 = 0;
$ta		 = 0;
if($cb != 0)
{
}
if($tx!=0)
{
}
if($ta != 0)
{
}
$hsn_code=$rowOrderPro->hsn_code;
if($hsn_code=='')
{
$hsn_code="N/A";
}
echo "</td>";
echo "<td valign='middle' class='tblBorder_invoice_bottom tblBorder_invoice_right'> $hsn_code </td>";
echo "<td valign='middle' class='tblBorder_invoice_bottom tblBorder_invoice_right'> $rowOrderPro->pro_model </td>";
echo "<td valign='middle' class='tblBorder_invoice_bottom tblBorder_invoice_right'>".str_replace("�"," ",$this->proidentrydesc($rowOrderPro->proidentry))."</td>";
//echo "<td valign='top' class='tblBorder_invoice_bottom tblBorder_invoice_right'><input type='text' name='pro_qty[]' value='". $rowOrderPro->pro_quantity."' size='4'></td>" ;
echo "<td valign='middle' class='tblBorder_invoice_bottom tblBorder_invoice_right'>";
echo '<input type="text" name="pro_qty[]" value="'. $rowOrderPro->pro_quantity.'" size="4"  class="form-control">';
// $rowOrderPro->pro_quantity </td>";
$ManufacturerID = $rowOrderPro->manufacturers_id;
/*			$sqlManuF		= "select * from tbl_manufacturers where manufacturers_id = $ManufacturerID";
$rsManuF		= mysqli_query($GLOBALS["___mysqli_ston"],  $sqlManuF);
if(mysqli_num_rows($rsManuF)>0)
{
$rowManuF  = mysqli_fetch_object($rsManuF);
$ManuFname = $rowManuF->manufacturers_name;
}*/
$orPrice = $rowOrderPro->pro_price;
echo "<td valign='middle' class='tblBorder_invoice_bottom tblBorder_invoice_right' nowrap='nowrap'> ";
echo $Offer_Currency." ";
echo $per_product_price=number_format($orPrice,2);
echo "</td>";
$sql_dis="SELECT * FROM prowise_discount where orderid='".$pcode."' and proid='".$proID."'";
$sql_dis_dis=mysqli_query($GLOBALS["___mysqli_ston"],  $sql_dis);
$sql_dis_row=mysqli_fetch_object($sql_dis_dis);
//if($sql_dis_row->show_discount=="Yes" || $dis_td) {
echo "<td valign='middle' class='tblBorder_invoice_bottom tblBorder_invoice_right' nowrap='nowrap'>";
echo $Offer_Currency." "; 
echo number_format($sql_dis_row->discount_amount,2);
echo "<br />";
$discounted_price=$orPrice*$sql_dis_row->discount_percent/100;			
//$sql_dis_row->discount_percent;
echo '<input type="hidden" name="pro_price[]" value="'. $rowOrderPro->pro_price * $rowOrderPro->pro_quantity.'" >';
echo '<input type="hidden" name="pro_id[]" value="'. $rowOrderPro->pro_id.'" >';
echo '<input type="text" name="discount_percent[]" value="'. $sql_dis_row->discount_percent.'" size="4" class="form-control">%';
echo "</td>";
//}
/*else
{
echo '<input type="hidden" name="pro_price[]" value="'. $rowOrderPro->pro_price * $rowOrderPro->pro_quantity.'" >';
echo '<input type="hidden" name="pro_id[]" value="'. $rowOrderPro->pro_id.'" >';
}*/
echo "<td valign='middle' class='tblBorder_invoice_bottom tblBorder_invoice_right' nowrap='nowrap'> ";
echo $Offer_Currency." ";
//	echo $per_product_tax=$rowOrderPro->Pro_tax;
echo "".			$discounted_price_tax_amt=($orPrice-$discounted_price)*$rowOrderPro->GST_percentage/100;			
echo "<br>(".$per_product_GST_percentage=$rowOrderPro->GST_percentage;
echo ")</td>";
/*		echo "<td valign='top' class='tblBorder_invoice_bottom tblBorder_invoice_right' nowrap='nowrap'> ";
echo $Offer_Currency." ";
echo $per_product_GST_percentage=$rowOrderPro->GST_percentage;
echo "</td>";
*/			
$SumProPrice = ($orPrice-$discounted_price+$discounted_price_tax_amt)* $rowOrderPro->pro_quantity;//$proPrice * $rowOrderPro->pro_quantity;
$totalTax = $discounted_price_tax_amt * $rowOrderPro->pro_quantity;//$rowOrderPro->Pro_tax * $rowOrderPro->pro_quantity;
$ProTotalPrice = ($discounted_price_tax_amt+$orPrice-$discounted_price)* $rowOrderPro->pro_quantity;//($rowOrderPro->pro_final_price) * $rowOrderPro->pro_quantity ;//remove tax changed by rumit
$totalCost     = $totalCost + $ProTotalPrice ;
echo "<td valign='top' class='tblBorder_invoice_bottom ' nowrap='nowrap'>";
echo $Offer_Currency." "; 
echo number_format($ProTotalPrice,2);
echo "</td>";
//echo "<td><input type='submit' value='Delete' class='inputton' name='REM_NOW' onclick=\"return confirm('Are You Sure to Delete this product.\");' /><input type='idden' name='ID' value='$rowOrderPro->pro_id' /></td>";
//echo "<input type='idden' name='ID' value='$rowOrderPro->pro_id' />";
echo "</tr>";
//			echo "<tr bgcolor='#F6F6F6'><td colspan='11' height='2'></td></tr>";
}
echo "</table>";
}
/*performa invoice edit function*/

function OrderItemsInfo_invoice1_edit_performa($pcode)
{
$sql_dis_currency="SELECT Price_value FROM tbl_order where orders_id='".$pcode."'";
$sql_dis_dis_currency=mysqli_query($GLOBALS["___mysqli_ston"],  $sql_dis_currency);
$sql_dis_row_currency=mysqli_fetch_object($sql_dis_dis_currency);
$Offer_Currency=$sql_dis_row_currency->Price_value;
$Offer_Currency=str_replace("backoffice","crm",$Offer_Currency);
$symbol		= $this->currencySymbol(1);
$currency1 	= $symbol[0];
$curValue 	= $symbol[1];
$totalCost  = 0;
echo "<table width='100%' border='0' cellpadding='1' cellspacing='0' class='tblBorder_invoice_right1   tblBorder_invoice_left1'>";
//echo "<tr class='pagehead'><td colspan='11' class='pad'>Item(s) Information </td></tr>";
echo "<tr class='head'>";
echo "<th class='pad tblBorder_invoice_bottom tblBorder_invoice_right' width='2%' >S.No</td>";
echo "<th width='10%' class='pad tblBorder_invoice_bottom tblBorder_invoice_right' >Product Name</th>";
echo "<th width='5%' class='pad tblBorder_invoice_bottom tblBorder_invoice_right' >HSN Code</th>";
echo "<th width='5%' class='pad tblBorder_invoice_bottom tblBorder_invoice_right' >Part No.</th>";
//echo "<th width='30%' class='pad tblBorder_invoice_bottom tblBorder_invoice_right' >Description</th>";
echo "<th width='5%' class='tblBorder_invoice_bottom tblBorder_invoice_right'>Qty</th>
<th width='10%' class='tblBorder_invoice_bottom tblBorder_invoice_right' nowrap='nowrap'>Unit Price</th>
<th width='10%' class='tblBorder_invoice_bottom tblBorder_invoice_right' nowrap='nowrap'>Rate</th>";
$sql_dis="SELECT show_discount FROM prowise_discount where orderid='".$pcode."'";
$sql_dis_dis=mysqli_query($GLOBALS["___mysqli_ston"],  $sql_dis);
$sql_dis_row=mysqli_fetch_object($sql_dis_dis);
if($sql_dis_row->show_discount=="Yes") {
echo "<th width='10%' class='tblBorder_invoice_bottom tblBorder_invoice_right' nowrap='nowrap' style='display:none' >Discount(-)</th>";
$dis_td=1;
}
else
{
}
echo "<th width='10%' class='pad tblBorder_invoice_bottom tblBorder_invoice_right' style='display:none'>Add IGST Value(%)</th>";
//		echo "<td width='10%' class='pad tblBorder_invoice_bottom tblBorder_invoice_right' >GST %</td>";
echo "<th nowrap='nowrap' width='10%' class='tblBorder_invoice_bottom ' >Sub Total</th></tr>";
$sqlOrderPro = "select * from tbl_order_product where order_id ='$pcode' order by order_pros_id asc ";
$rsOrderPro	 = mysqli_query($GLOBALS["___mysqli_ston"],  $sqlOrderPro);
$h=0;
while($rowOrderPro = mysqli_fetch_object($rsOrderPro))
{
$h++;
echo "<tr class='text'>";
?>
<?php
echo "<td class='pad tblBorder_invoice_bottom tblBorder_invoice_right' >".$h."</td>";
echo "<td class='pad tblBorder_invoice_bottom tblBorder_invoice_right' valign='top'><strong>".$rowOrderPro->pro_name."</strong><br />";
$OrderProID	 = $rowOrderPro->order_pros_id;
$proID	 	 = $rowOrderPro->pro_id;
$groupID 	 = $rowOrderPro->group_id;
$pro_max_discount_allowed=$this->product_discount($proID);
$hsn_code=$rowOrderPro->hsn_code;
if($hsn_code=='')
{
$hsn_code="N/A";
}
echo "</td>";
echo "<td valign='top' class='tblBorder_invoice_bottom tblBorder_invoice_right'> $hsn_code </td>";
echo "<td valign='top' class='tblBorder_invoice_bottom tblBorder_invoice_right'> $rowOrderPro->pro_model </td>";
//echo "<td valign='top' class='tblBorder_invoice_bottom tblBorder_invoice_right'>".str_replace("�"," ",$this->proidentrydesc($rowOrderPro->proidentry))."</td>";
//echo "<td valign='top' class='tblBorder_invoice_bottom tblBorder_invoice_right'><input type='text' name='pro_qty[]' value='". $rowOrderPro->pro_quantity."' size='4'></td>" ;
echo "<td valign='top' class='tblBorder_invoice_bottom tblBorder_invoice_right'>";
echo '<input type="text" name="pro_qty[]" value="'. $rowOrderPro->pro_quantity.'" size="2" class="form-control">';
// $rowOrderPro->pro_quantity </td>";
//$ManufacturerID = $rowOrderPro->manufacturers_id;
/*			$sqlManuF		= "select * from tbl_manufacturers where manufacturers_id = $ManufacturerID";
$rsManuF		= mysqli_query($GLOBALS["___mysqli_ston"],  $sqlManuF);
if(mysqli_num_rows($rsManuF)>0)
{
$rowManuF  = mysqli_fetch_object($rsManuF);
$ManuFname = $rowManuF->manufacturers_name;
}*/
$orPrice = $rowOrderPro->pro_price;
echo "<td valign='top' align='center' class='tblBorder_invoice_bottom tblBorder_invoice_right' nowrap='nowrap'> ";
echo $Offer_Currency." ";
$per_product_price=number_format($orPrice,2);
echo  $per_product_price;
echo '<input type="hidden" name="pro_max_discount_allowed[]" value="'. $pro_max_discount_allowed.'" size="4" class="form-control">';
echo "</td>";

$sql_dis="SELECT * FROM prowise_discount where orderid='".$pcode."' and proid='".$proID."'";
$sql_dis_dis=mysqli_query($GLOBALS["___mysqli_ston"],  $sql_dis);
$sql_dis_row=mysqli_fetch_object($sql_dis_dis);
if($sql_dis_row->show_discount=="Yes" || $dis_td) {
echo "<td valign='top' class='tblBorder_invoice_bottom tblBorder_invoice_right' nowrap='nowrap' style='display:none'>";
echo $Offer_Currency." "; 
echo number_format($sql_dis_row->discount_amount,2);
echo "<br />";
$discounted_price=$orPrice*$sql_dis_row->discount_percent/100;			
//$sql_dis_row->discount_percent;
echo '<input type="hidden" name="pro_price1[]" value="'. $rowOrderPro->pro_price.'" >';
echo '<input type="hidden" name="pro_id[]" value="'. $rowOrderPro->pro_id.'" >';
echo '<input type="hidden" name="discount_percent[]" value="'. $sql_dis_row->discount_percent.'" size="4" class="form-control"><br>'.$sql_dis_row->discount_percent.'%';
echo "</td>";
}
else
{
	$discounted_price=$orPrice*$sql_dis_row->discount_percent/100;	
echo '<input type="hidden" name="pro_price2[]" value="'. $rowOrderPro->pro_price.'" >';
echo '<input type="hidden" name="pro_id[]" value="'. $rowOrderPro->pro_id.'" >';
}

echo "<td valign='top' align='center' class='tblBorder_invoice_bottom tblBorder_invoice_right' nowrap='nowrap'> ";
echo $Offer_Currency." ";
$per_product_price=number_format($orPrice-$discounted_price,2);
echo '<input type="text" name="pro_price[]" value="'. $per_product_price.'" size="10" class="form-control" style="display:inline-block;text-align: right;" >';
echo '<input type="hidden" name="pro_max_discount_allowed[]" value="'. $pro_max_discount_allowed.'" size="4" class="form-control">';
echo "</td>";

echo "<td valign='top' class='tblBorder_invoice_bottom tblBorder_invoice_right' nowrap='nowrap' style='display:none'> ";
echo $Offer_Currency." ";
//	echo $per_product_tax=$rowOrderPro->Pro_tax;
echo "".			$discounted_price_tax_amt=($orPrice-$discounted_price)*$rowOrderPro->GST_percentage/100;			
echo "<br>(".$per_product_GST_percentage=$rowOrderPro->GST_percentage;
echo ")</td>";
/*		echo "<td valign='top' class='tblBorder_invoice_bottom tblBorder_invoice_right' nowrap='nowrap'> ";
echo $Offer_Currency." ";
echo $per_product_GST_percentage=$rowOrderPro->GST_percentage;
echo "</td>";
*/			
$SumProPrice = ($orPrice-$discounted_price+$discounted_price_tax_amt)* $rowOrderPro->pro_quantity;//$proPrice * $rowOrderPro->pro_quantity;
$totalTax = $discounted_price_tax_amt * $rowOrderPro->pro_quantity;//$rowOrderPro->Pro_tax * $rowOrderPro->pro_quantity;
$ProTotalPrice = ($orPrice-$discounted_price)* $rowOrderPro->pro_quantity;//($rowOrderPro->pro_final_price) * $rowOrderPro->pro_quantity ;//remove tax changed by rumit
$totalCost     = $totalCost + $ProTotalPrice ;
echo "<td valign='top' class='tblBorder_invoice_bottom ' nowrap='nowrap'>";
echo $Offer_Currency." "; 
echo number_format($ProTotalPrice,2);
echo "</td>";
//echo "<td><input type='submit' value='Delete' class='inputton' name='REM_NOW' onclick=\"return confirm('Are You Sure to Delete this product.\");' /><input type='idden' name='ID' value='$rowOrderPro->pro_id' /></td>";
//echo "<input type='idden' name='ID' value='$rowOrderPro->pro_id' />";
echo "</tr>";
//			echo "<tr bgcolor='#F6F6F6'><td colspan='11' height='2'></td></tr>";
}
echo "</table>";
}
/*PI ends*/

///////////////////////////////////  Start Delivery Challan  //////////////////////////////////////////////

function OrderItemsInfo_Delivery_Challan($pcode)
{
$symbol		= $this->currencySymbol(1);
$currency1 	= $symbol[0];
$curValue 	= $symbol[1];
$totalCost  = 0;
echo "<table width='100%' border='0' cellpadding='5' cellspacing='2' class='bor_opt' style='border-collapse:collapse;'>";
//echo "<tr class='pagehead'><td colspan='11' class='pad'>Item(s) Information </td></tr>";
echo "<tr class='head'>";
echo "<td width='5%' align='center'>SR.No.</td>";
//echo "<td width='15%' '>Item Code</td>";
echo "<td >Product Description </td>";//added by rumit on 22july 2019 
//echo "<td width='15%' >Product Serial No. </td>";
echo "<td width='5%'>Qty.</td>";
echo "<td width='5%'>UOM</td>";
echo "<td width='10%' class='tblBorder_invoice_bottom'>Remarks</td>
</tr>";
//		echo "<tr><td height='1px' colspan='11' class='tblBorder_invoice_bottom'>hf</td></tr>"; //blank line
$sqlOrderPro = "select * from tbl_order_product where order_id ='$pcode' order by order_pros_id desc";
$rsOrderPro	 = mysqli_query($GLOBALS["___mysqli_ston"],  $sqlOrderPro);
$h=0;
while($rowOrderPro = mysqli_fetch_object($rsOrderPro))
{
$h++;
echo "<tr class='text'>";
echo "<td align='center'>".$h."</td>";
//echo "<td valign='top' >";
$OrderProID	 = $rowOrderPro->order_pros_id;
$proID	 	 = $rowOrderPro->pro_id;
$groupID 	 = $rowOrderPro->group_id;
/*$sqlAttr = "select * from tbl_group where group_id='$groupID'";
$rsAttr  = mysqli_query($GLOBALS["___mysqli_ston"],  $sqlAttr);

$i  	 = -1;

$k  	 = 0;

$cb 	 = 0;

$tx		 = 0;

$ta		 = 0;

if($cb != 0)

{

}

if($tx!=0)

{

}

if($ta != 0)

{

}*/
echo "<td valign='top' >".$this->customer_pro_desc_title($rowOrderPro->pro_model,$pcode).'<br>';
echo '<b>Item Type:</b>'.$this->product_type_class_name($this->product_type_class_id($proID)).'<br>
<b>Item Code:</b>'.$this->pro_text_ordered($proID,$pcode).'<br>
<b>Product S. No.: </b>'.$rowOrderPro->barcode;
"</td>";
//echo "<td valign='top'>".$rowOrderPro->barcode." </td>";
echo "<td valign='top'> $rowOrderPro->pro_quantity </td>";
echo "<td valign='top'> No.</td>";
echo "<td valign='top'> No Remark</td>";
echo "</tr>";
echo "<tr><td style='border-right:1px solid #fff; text-align:center'></td><td colspan='4'>";
$bom_items=$this->product_BOM_items($proID);
array_pop($bom_items);// removes last element of an array - because last array element is empty - added by Rumit on 23-Jun-2021
$bom_ctr=count($bom_items);
if($bom_ctr>0)
{
echo "<table border='0' width='100%' cellpadding='0' cellspacing='0' class='bor_opt' style='border-collapse:collapse;' >";
echo "<tr>
<td width='5%'><strong >Item Type</strong></td>
<td width='5%'><strong>UPC </strong></td>
<td ><strong>Item Name</strong></td>
<td width='5%'><strong>Qty</strong></td>
<td width='6%'><strong>UOM</strong></td>
<td width='10%'><strong>Remarks</strong></td>";

echo "</tr>";
for($i=0; $i< $bom_ctr; $i++ )
{
echo"<tr>";
/*if($this->product_type_class_id($bom_items[$i]->pro_id)>0)
{*/
echo"<td width='5%'>";
echo $this->product_type_class_name($this->product_type_class_id($bom_items[$i]->pro_id))."</td>";	
echo "<td width='5%'>".$bom_pro_id=$this->pro_upc_code($bom_items[$i]->pro_id)."</td>";
echo "<td >".$bom_pro_id=$this->product_name($bom_items[$i]->pro_id)."</td>";
echo "<td width='5%'>".$bom_pro_id=$bom_items[$i]->tbl_qty."</td>";
echo "<td width='6%'>No</td>";
echo "<td width='10%'>No Remark</td>";
//}
echo "</tr>";
}
echo "</table>";
}



echo "</td></tr>";

//			echo "<tr bgcolor='#F6F6F6'><td colspan='11' height='2'></td></tr>";

}

echo "</table>";

}

/////////////////////////////////////////// End Delivery Challan ////////////////////////////////////////
function OrderItemsInfo_Delivery_Challan_Mail($pcode)
{
$sqlOrderPro 	= "select * from tbl_order_product where order_id ='$pcode' order by order_pros_id asc";
$rsOrderPro	 	= mysqli_query($GLOBALS["___mysqli_ston"],  $sqlOrderPro);
$h=0;
while($rowOrderPro = mysqli_fetch_object($rsOrderPro))
{
$proID	 	 	= $rowOrderPro->pro_id;

$pro_desc_title	= $this->customer_pro_desc_title($rowOrderPro->pro_model,$pcode);
$item_code		= $this->pro_text_ordered($proID,$pcode);
$s_no			= $rowOrderPro->barcode;
	
 $favorites[] =  
  	  array( 
        "pro_desc_title" => $pro_desc_title,
		"item_code" => $item_code, 
        "s_no" => $s_no    
	);
}
	return $favorites;
}

///////////////////////////////////  Start warranty certificate //////////////////////////////////////////////

function OrderItemsInfo_warranty_certificate($pcode,$proidgc=0,$cert_id=0)
{
$symbol		= $this->currencySymbol(1);
$currency1 	= $symbol[0];
$curValue 	= $symbol[1];
$totalCost  = 0;
echo "<table width='100%' border='0' cellpadding='5' cellspacing='0' class='table-bordered'  >";
echo "<tr>";
echo "<th width='15%' align='center' style='text-align:center' >SR.No.</th>";
echo "<th width='55%'>Product Description </th>";//added by rumit on 22july 2019 
echo "<th width='15%'>Qty.</th>";
echo "<th width='15%'>UOM</th>";
echo "</tr>";
//		echo "<tr><td height='1px' colspan='11' class='tblBorder_invoice_bottom'>hf</td></tr>"; //blank line
//$sqlOrderPro = "select * from tbl_order_product where order_id ='$pcode' order by order_pros_id desc";

if($proidgc!='0' && $proidgc!='')
{
	//$proidgc1=explode(",",$proidgc);
	//$proidgc2=implode(",",$proidgc1);
	
	$search=" and g.pro_id IN ($proidgc) "; 
}

if($cert_id!='0' && $cert_id!='')
{
	//$proidgc1=explode(",",$proidgc);
	//$proidgc2=implode(",",$proidgc1);
	
	$search_cert=" and g.cert_gen_id = '$cert_id' "; 
}
 $sqlOrderPro = "SELECT o.order_id,o.order_pros_id,o.pro_id,o.pro_model,o.pro_name,o.pro_quantity,o.barcode,g.certificate_id,g.i_id,g.o_id,g.pro_id,g.generated_date,g.status FROM tbl_order_product o INNER JOIN tbl_warranty_certificate_generated_items g 
ON o.order_id=g.o_id 
and o.pro_id=g.pro_id 
and o.order_id = '$pcode' 
$search
$search_cert
order by o.order_pros_id desc";
$rsOrderPro	 = mysqli_query($GLOBALS["___mysqli_ston"],  $sqlOrderPro);
$h=0;
while($rowOrderPro = mysqli_fetch_object($rsOrderPro))
{
$h++;
echo "<tr class='text'>";
echo "<td align='center' >".$h."</td>";
//echo "<td valign='top' >";
$OrderProID	 = $rowOrderPro->order_pros_id;
$proID	 	 = $rowOrderPro->pro_id;

echo "<td valign='top'  style='word-break:break-word'>".$this->customer_pro_desc_title($rowOrderPro->pro_model,$pcode).'<br>
<b>Item Code</b>: '.$this->pro_text_ordered($proID,$pcode).'<br><b>Product S. No.</b>: '.$rowOrderPro->barcode." </td>";
echo "<td valign='top'> $rowOrderPro->pro_quantity </td>";
echo "<td valign='top'> No.</td>";
echo "</tr>";
}
echo "</table>";
}

/////////////////////////////////////////// End warranty certificate ////////////////////////////////////////

function delivery_offer_warranty($pcode)
{
$sql_warranty 	= "select delivery_offer_warranty from tbl_delivery_order where  O_Id IN ('$pcode') ";
	$rs_warranty	 	= mysqli_query($GLOBALS["___mysqli_ston"], $sql_warranty); 
	if(mysqli_num_rows($rs_warranty)>0)
	{
		$row_warranty  					= mysqli_fetch_object($rs_warranty);
		$delivery_offer_warranty		= $row_warranty->delivery_offer_warranty;
	//$Consignee		= $row_warranty->Consignee;

	}

return $delivery_offer_warranty;	
}

function get_certificate_id($pcode,$pro_id)
{
  $sql_warranty 	= "select certificate_id from tbl_warranty_certificate_generated where  o_id = '$pcode' and pro_id like '%$pro_id%' ";
	$rs_warranty	 	= mysqli_query($GLOBALS["___mysqli_ston"], $sql_warranty); 
	if(@mysqli_num_rows(@$rs_warranty)>0)
	{
		$row_warranty  					= mysqli_fetch_object($rs_warranty);
		$certificate_id		= $row_warranty->certificate_id;
	//$Consignee		= $row_warranty->Consignee;

	}

return $certificate_id;	
}
///////////////////////////////////  Start Delivery Order  //////////////////////////////////////////////
function OrderItemsInfo_Delivery_order($pcode)
{
$symbol		= $this->currencySymbol(1);
$currency1 	= $symbol[0];
$curValue 	= $symbol[1];
$totalCost  = 0;
echo "<table width='100%' border='0' cellpadding='5' cellspacing='2' class='bor_opt'>";
//echo "<tr class='pagehead'><td colspan='11' class='pad'>Item(s) Information </td></tr>";
echo "<tr class='head'>";
echo "<td width='3%' nowrap='nowrap'   >SR.No.</td>";
echo "<td width='32%' >Description of Goods</td>";
echo "<td width='15%' >Qty.</td>";
echo "<td width='15%' >UOM</td>";
echo "<td width='6%' class='tblBorder_invoice_bottom'>Remarks</td>
</tr>";
//		echo "<tr><td height='1px' colspan='11' class='tblBorder_invoice_bottom'>hf</td></tr>"; //blank line
$sqlOrderPro = "select * from tbl_do_products where OID ='$pcode' ";
$rsOrderPro	 = mysqli_query($GLOBALS["___mysqli_ston"],  $sqlOrderPro);
$h=0;
while($rowOrderPro = mysqli_fetch_object($rsOrderPro))
{
$h++;
echo "<tr class='text'>";
echo "<td >".$h."</td>";
echo "<td valign='top'><strong>".$rowOrderPro->pro_name."</strong><br />";
$OrderProID	 = $rowOrderPro->order_pros_id;
$proID	 	 = $rowOrderPro->pro_id;
$groupID 	 = $rowOrderPro->group_id;
$sqlAttr = "select * from tbl_group where group_id='$groupID'";
$rsAttr  = mysqli_query($GLOBALS["___mysqli_ston"],  $sqlAttr);
$i  	 = -1;
$k  	 = 0;
$cb 	 = 0;
$tx		 = 0;
$ta		 = 0;
if($cb != 0)
{
}
if($tx!=0)
{
}
if($ta != 0)
{
}
echo "&nbsp;<span class='prdname red required '><strong>".$this->pro_text_ordered($proID,$pcode)."</strong></span>";		  
echo "</td>";
echo "<td valign='top'> $rowOrderPro->pro_quantity </td>";
echo "<td valign='top'> No.</td>";
echo "<td valign='top'> No Remark</td>";
echo "</tr>";
//			echo "<tr bgcolor='#F6F6F6'><td colspan='11' height='2'></td></tr>";
}
echo "</table>";
}

/////////////////////////////////////////// End Delivery Order ////////////////////////////////////////
function pro_text_ordered($IdValue, $Order_id)
{
$sqlCountry = "select pro_text,pro_model from tbl_order_product where pro_id = '$IdValue' and order_id='$Order_id' and deleteflag = 'active' order by order_pros_id desc";
$rsCountry  = mysqli_query($GLOBALS["___mysqli_ston"],  $sqlCountry);
$rowCountry = mysqli_fetch_object($rsCountry);
$pro_text	= $rowCountry->pro_text."  (".$rowCountry->pro_model.") ";
//	$country 	= $IdValue;
if(trim(strlen($pro_text))>0)
{
return $pro_text;
}
else
{
echo "";
}
}
////////////////////////////////////////

function OrderPaymentInfo_delivery_sticker_bluedart($pcode,$invoice_no,$box_dimensions,$box_weight,$courier_company)
{
$symbol		= $this->currencySymbol(1);
$currency1 	= $symbol[0];
$curValue 	= $symbol[1];
$sqlPayInfo = "select * from tbl_order where orders_id = '$pcode' and deleteflag = 'active'"; 
$rsPayInfo	= mysqli_query($GLOBALS["___mysqli_ston"],  $sqlPayInfo);
if(mysqli_num_rows($rsPayInfo)>0)
{
$rowPayInfo			= mysqli_fetch_object($rsPayInfo);
$payment_mode		= $rowPayInfo->payment_mode;
$shipping_zip_code	= $rowPayInfo->shipping_zip_code;
$rout_codebluedart	= $this->bluedartroutingcode($shipping_zip_code);
$sqlPayModule		= "select * from tbl_module_config_master where module_config_id = '$payment_mode' and deleteflag = 'active'";
$rsPayModule		= mysqli_query($GLOBALS["___mysqli_ston"],  $sqlPayModule);
$rowPayModule		= mysqli_fetch_object($rsPayModule);
$PayType			= $rowPayModule->module_config_name;
$couponDiscount		= $rowPayInfo->coupon_discount;
///			$couponDiscount		= $rowPayInfo->coupon_discount;
//echo $payment_mode."$$$$$";
if(strlen(trim($PayType))<=0 && $payment_mode == 15)
{
$PayType		= "Pay with a Gift Card";
}
else if(strlen(trim($PayType))<=0 && $payment_mode == 16)
{
$PayType		= "Pay with a Rewards Card";
}
else if(strlen(trim($PayType))<=0 && $payment_mode == 17)
{
$PayType		= "Cash On Delivery";
}
if($payment_mode == '4')
{
$sqlPPWP		= "select * from tbl_order_paypal_payment_info where order_id = '$pcode' and deleteflag = 'active'";
$rsPPWP			= mysqli_query($GLOBALS["___mysqli_ston"],  $sqlPPWP);
$rowPPWP		= mysqli_fetch_object($rsPPWP);
$totalAmount 	= $rowPPWP->total_amount;
$txn_id		 	= $rowPPWP->txn_id;
$pay_status		= $rowPPWP->payment_status;
$currency		= $rowPPWP->pay_currency;
}

if($payment_mode == '5')
{
$sqlAuth		= "select * from tbl_order_authorization where order_id = '$pcode' and deleteflag = 'active'";
$rsAuth			= mysqli_query($GLOBALS["___mysqli_ston"],  $sqlAuth);
$rowAuth		= mysqli_fetch_object($rsAuth);
$totalAmount	= $rowAuth->total_amount;
$txn_id			= $rowAuth->txn_id;
$pay_status		= $rowAuth->payment_status;
$currency		= $rowAuth->pay_currency;
}

if($payment_mode == '9'  || $payment_mode == '10'  || $payment_mode == '11' )
{
$sqlAuth		= "select * from tbl_order_authorization_hdfc where order_id = '$pcode' and deleteflag = 'active'";
$rsAuth			= mysqli_query($GLOBALS["___mysqli_ston"],  $sqlAuth);
$rowAuth		= mysqli_fetch_object($rsAuth);
$totalAmount	= $rowAuth->total_amount;
$txn_id			= $rowAuth->txn_id;
$pay_status		= $rowAuth->payment_status;
$currency		= $rowAuth->pay_currency;
}

if($payment_mode == '6')
{
$sqlPPDD		= "select * from tbl_order_pp_api_dodirect where order_id = '$pcode' and deleteflag = 'active'";
$rsPPDD			= mysqli_query($GLOBALS["___mysqli_ston"],  $sqlPPDD);
$rowPPDD		= mysqli_fetch_object($rsPPDD);
$totalAmount	= $rowPPDD->total_amount;
$txn_id			= $rowPPDD->txn_id;
$pay_status		= $rowPPDD->payment_status;
$currency		= $rowPPDD->pay_currency;
}

if($payment_mode == '17')
{
$sqlPPDD		= "select * from tbl_order where orders_id = '$pcode' and deleteflag = 'active'";
$rsPPDD			= mysqli_query($GLOBALS["___mysqli_ston"],  $sqlPPDD);
$rowPPDD		= mysqli_fetch_object($rsPPDD);
$additional_disc = $rowPPDD->additional_disc;
$totalAmount	= $rowPPDD->total_order_cost;
//				$txn_id			= $rowPPDD->txn_id;
$pay_status		= "PENDING";//$rowPPDD->payment_status;
$currency		= "INR";//$rowPPDD->pay_currency;
}

if($payment_mode == '15' )
{
$sqlPPDD		= "select * from tbl_order where orders_id = '$pcode' and deleteflag = 'active'";
$rsPPDD			= mysqli_query($GLOBALS["___mysqli_ston"],  $sqlPPDD);
$rowPPDD		= mysqli_fetch_object($rsPPDD);
$additional_disc = $rowPPDD->additional_disc;
$totalAmount	= $rowPPDD->total_order_cost;
//				$txn_id			= $rowPPDD->txn_id;
$pay_status		= "RECEIVED";//$rowPPDD->payment_status;
$currency		= "INR";//$rowPPDD->pay_currency;
}

}
$pcodeAEB		= $_REQUEST['pcode'];
$awb			= "select * from tble_invoice where id = '$pcodeAEB'";
$rsAWB			= mysqli_query($GLOBALS["___mysqli_ston"],  $awb);
$rowAWB			= mysqli_fetch_object($rsAWB);
$sqlOrderTotal_R 			= "SELECT * FROM tbl_order_product where order_id ='$pcode' order by order_pros_id desc";
$rsOrderTotal_R  			=  mysqli_query($GLOBALS["___mysqli_ston"],  $sqlOrderTotal_R);
$shp_num_rows=mysqli_num_rows($rsOrderTotal_R);
//		$rowOrderTotal 				= mysqli_fetch_object($rsOrderTotal_R);
$shippingcost		   	= $rowPayInfo->shipping_method_cost;
//		$shippingcost		   	= $rowPayInfo->shipping_method_cost/$shp_num_rows;
$sqlOrderTotal 			= "SELECT * FROM tbl_order_product o_tbl, tble_invoice_items i_tbl WHERE o_tbl.pro_id = i_tbl.pro_id and order_id ='$pcode' and  i_tbl.i_id='$pcodeAEB'";
$rsOrderTotal  =  mysqli_query($GLOBALS["___mysqli_ston"],  $sqlOrderTotal);
while($rowOrderTotal 	= mysqli_fetch_object($rsOrderTotal))
{
$additional_disc1=$rowOrderTotal->additional_disc+$additional_disc1;
$TotalOrder+= (($rowOrderTotal->pro_price)*$rowOrderTotal->pro_quantity);
}
//echo $rowPayInfo->additional_disc;
if($additional_disc1=='0')
{
$additional_disc1=$rowPayInfo->additional_disc;
}
$TotalOrder1	=$TotalOrder+$shippingcost-$couponDiscount;				
//	$TotalOrder1	=$TotalOrder+$taxValue+$shippingcost-$couponDiscount;				
echo "<table width='90%' border='1' cellspacing='0' cellpadding='5' align='center' bordercolor='#CCCCCC'>";
echo $courier_company;
if($courier_company=="blue_dart_pp" || $courier_company=="Blue dart cod")
{
$classname_blue="classname_blue";
}
else
{
$classname_blue="classname";
}
echo $courier_company;
$sqlOrderPro = "select * from tbl_order_product where order_id ='$rowPayInfo->orders_id' order by order_pros_id desc";
$rsOrderPro	 = mysqli_query($GLOBALS["___mysqli_ston"],  $sqlOrderPro);
echo "<tr>";
echo "<th>S.No.</th>";
echo "<th>Reference Number</th>";
echo "<th>Attention</th>";
echo "<th>City/State</th>";
echo "<th>Contents</th>";
echo "<th>Weight(kgs)</th>";
echo "<th>Barcode - Airwaybill number</th>";
echo "</tr>";
$date_ordered="2012-05-28";
echo $sqlOrderPro1 = "select * from tbl_order where deleteflag ='active' and date_ordered='$date_ordered' ";
$rsOrderPro1	 = mysqli_query($GLOBALS["___mysqli_ston"],  $sqlOrderPro1);
while($rowOrderPro1 = mysqli_fetch_object($rsOrderPro1))
{
echo "<tr>";
echo "<td>1</td>";
echo "<td>$rowPayInfo->orders_id</td>";
echo "<td>$rowPayInfo->shipping_name</td>";
echo "<td nowrap='nowrap'> $rowPayInfo->shipping_city, $rowPayInfo->shipping_state; </td>";
echo "<td>";
while($rowOrderPro = mysqli_fetch_object($rsOrderPro))
{
echo $rowOrderPro->pro_name;
echo ",<br>";
}
echo "</td>";
echo "<td>". number_format($box_weight,2)."</td>";
echo "<td nowrap='nowrap'><span  class='$classname_blue'>* $rowAWB->awb_no *</span></td>";
echo "</tr>";
}
echo "</table>";
}
//this function use to display the shipping information 

function OrderShippingInfo($pcode)
{
$symbol		= $this->currencySymbol(1);
$currency1 	= $symbol[0];
$curValue 	= $symbol[1];
$sqlShipInfo = "select * from tbl_order where orders_id = '$pcode' and deleteflag = 'active'"; 
$rsShipInfo	 = mysqli_query($GLOBALS["___mysqli_ston"],  $sqlShipInfo);
if(mysqli_num_rows($rsShipInfo)>0)
{
$rowPayInfo    = mysqli_fetch_object($rsShipInfo);
$Shipping_mode = $rowPayInfo->shipping_method_cost;
$ship		   = explode(" - ", $Shipping_mode);
$sqlShipModule = "select * from tbl_module_config_master where module_config_id = '".$ship[0]."' and deleteflag = 'active'";
$rsShipModule  = mysqli_query($GLOBALS["___mysqli_ston"],  $sqlShipModule);
$rowShipModule = mysqli_fetch_object($rsShipModule);
$ShipType	   = $rowShipModule->module_config_name ;
}
echo "<table width='100%' border='0' cellpadding='0' cellspacing='0' class='tblBorder'>";
echo "<tr class='pagehead'><td colspan='2' class='pad'>Shipping Information </td></tr>";
echo "<tr class='text'><td width='30%' class='pad'>Shipping Method </td><td width='70%'>: &nbsp; &nbsp;$ShipType </td></tr>";
echo "<tr class='text'><td class='pad'>Shipping Cost</td><td>";
echo ": &nbsp; $currency1 ";
printf("%.0f",$ship[1]); 
echo " </td></tr>";
echo "</table>";

}

//////////////////////////////
/////////// this function use to display the order Total amount info	

function OrderTotalInfo($pcode)
{
$symbol		= $this->currencySymbol(1);
$currency1 	= $symbol[0];
$curValue 	= $symbol[1];
$sqlOrderTotal = "select * from tbl_order where orders_id = '$pcode'";
$rsOrderTotal  =  mysqli_query($GLOBALS["___mysqli_ston"],  $sqlOrderTotal);
if(mysqli_num_rows($rsOrderTotal)>0)
{
$rowOrderTotal 	= mysqli_fetch_object($rsOrderTotal);
$ship		   	= $rowOrderTotal->shipping_method_cost;
$shippingValue 	= explode(" - ",$ship);
$TotalOrder	   	= $rowOrderTotal->total_order_cost - $ship;
$additional_disc= $rowOrderTotal->additional_disc;
$subtotal       = $TotalOrder ;
$subtotal		= $subtotal + $rowOrderTotal->coupon_discount;
$couponDiscount = $rowOrderTotal->coupon_discount;
$TotalOrderFinal = $TotalOrder;
}
echo "<table width='100%' border='0' cellpadding='0' cellspacing='0' class='tblBorder'>";
echo "<tr class='pagehead'>";
echo "<td colspan='2' class='pad' nowrap>Order Total </td>";
echo "</tr>";
echo "<tr class='text'>";
echo "<td width='22%' class='pad' nowrap>Sub Total </td>";
echo "<td width='78%'> &nbsp; &nbsp; $currency1 ";
printf(" %.0f",$subtotal);
echo "</td>";
echo "</tr>";
if($couponDiscount!=0.00)
{
echo "<tr class='text'>";
echo "<td width='22%' class='pad' nowrap>Coupon(-) </td>";
echo "<td width='78%'> &nbsp; &nbsp; $currency1 ";
printf(" %.0f",$couponDiscount);
echo "</td>";
echo "</tr>";
}

if($additional_disc!=0.00)
{
echo "<tr class='text'>";
echo "<td width='22%' class='pad' nowrap>Additional Discount(-) </td>";
echo "<td width='78%'> &nbsp; &nbsp; $currency1 ";
printf(" %.0f",$additional_disc);
echo "</td>";
echo "</tr>";
}
if($ship!=0.00)
{
echo "<tr class='text'>";
echo "<td class='pad' nowrap>Shipping &amp; Handling(+) </td>";
echo "<td> &nbsp; &nbsp; $currency1 ";
printf(" %.0f ",$ship);
echo "</td>";
echo "</tr>";
}

echo "<tr class='text'>";
echo "<td class='pad' nowrap><strong>Grand Total </strong></td>";
echo "<td><strong> &nbsp; &nbsp; $currency1 "; 
printf(" %.0f",$TotalOrder-$rowOrderTotal->additional_disc+$ship);
echo "</strong></td>";
echo "</tr>";
echo "</table>";
}	

////////////////////////////////////////

function Get_acl_invoice_no($pcode)
{
$sql = "select Invoice_No from tbl_delivery_challan where O_Id='$pcode'";
$rs	 = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
return $row->Invoice_No;
}//added on 11-oct2019 to get manual invoice no in inventory manager module
function Get_invoice_no($pcode)
{
$sql = "select u_invoice_no from tbl_delivery_challan where O_Id='$pcode'";
$rs	 = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
return $row->u_invoice_no;
}

/////////////////////////////////////////////////////////////////////////////////////////////

function order_gen_info_invoice($pcode,$invoice_no)
{
$sql = "select * from tble_invoice where id='$invoice_no'";
$rs	 = mysqli_query($GLOBALS["___mysqli_ston"],  $sql); 
echo "<table width='100%' border='0' cellpadding='5' cellspacing='0' >";
if(mysqli_num_rows($rs)>0)
{
$sql_invoice = "select * from tble_invoice_items where i_id='$invoice_no'";
$rs_invoice	 = mysqli_query($GLOBALS["___mysqli_ston"],  $sql_invoice); 
$row = mysqli_fetch_object($rs);
$row_invoice = mysqli_fetch_object($rs_invoice);
echo "<tr class='pagehead'> <td width='68%' class='pad' valign='top' align='right'>Invoice Date :</td><td width='30%' valign='top'> &nbsp;". /*substr($row->I_date,0,10)*/date('d / m / y',strtotime($row->I_date))."</td> </tr>";
echo "<tr class='pagehead'><td width='68%' class='pad' valign='top' align='right'>Order No. :</td><td width='30%' valign='top'> &nbsp;". $row_invoice->o_id."</td> </tr>";
echo "<tr class='pagehead'><td width='68%' class='pad' valign='top' align='right'>Invoice No. :</td><td width='30%' valign='top'> &nbsp;". $row_invoice->i_no."</td> </tr>";
echo "<tr class='pagehead'> <td width='68%' class='pad' valign='top' align='right'>Shipped By :</td><td width='30%' valign='top'> &nbsp;".str_replace("_"," ",ucfirst($row->courier_company))."</td> </tr>";
if($row->courier_company=="Aramex")
{
echo "<tr class='pagehead'> <td width='68%' class='pad' valign='top' align='right'>Tracking No. :</td><td width='30%' valign='top'> &nbsp;<a href='http://www.aramex.com/track_results_multiple.aspx?ShipmentNumber=$row->awb_no' style='text-decoration:underline; color:#407700'>". $row->awb_no."</td> </tr>";

}

else if($row->courier_company=="blue_dart_cod" || $row->courier_company=="blue_dart_pp" )
{
echo "<tr class='pagehead'> <td width='68%' class='pad' valign='top' align='right'>Tracking No. :</td><td width='30%' valign='top'> &nbsp;<a href='http://www.bluedart.com/servlet/RoutingServlet?awb=$row->awb_no' style='text-decoration:underline; color:#407700'>". $row->awb_no."</td> </tr>";
}

else
{
echo "<tr class='pagehead'> <td width='68%' class='pad' valign='top' align='right'>Tracking No. :</td><td width='30%' valign='top'> &nbsp;<a href='#;' style='text-decoration:underline; color:#407700'>". $row->awb_no."</td> </tr>";

}
$_SESSION['stock_track_id']=$row->awb_no;
echo "<tr> <td colspan='2' height='2'></td></tr>";
}
else
{
echo "<tr class='text'><td colspan='2' class='redstar'> &nbsp; No record present in database</td></tr>";
}
echo "</table>";
}
////////////////////////////////////////

//added by rumit 17-08-2019 for demom stock remarks- removed duplicate remark of same pcode
function stock_remarks($ID)
{   
$sqlState = "select remarks from tbl_demo_stock where stock_id = '$ID' ";
$rsState  = mysqli_query($GLOBALS["___mysqli_ston"],  $sqlState);
$rowState = mysqli_fetch_object($rsState);
$remarks	  = $rowState->remarks;
return ucfirst($remarks);
}

function old_stock_chart($pcode){
$sql_t="Select stock_id,transf_stock_id,remarks from tbl_demo_stock where stock_id='$pcode' ";
			$rs_t=mysqli_query($GLOBALS["___mysqli_ston"],$sql_t);
			$rs_t_num=mysqli_num_rows($rs_t);
			if($rs_t_num>0)
			{
  $row_t=mysqli_fetch_object($rs_t);
   $transf_stock_id_get=$row_t->transf_stock_id;?>
<ul align="center">
  <li><img src="images/truck.png" height="50px" title="" /></li>
  <li><?php echo $this->stock_remarks($transf_stock_id_get);?></li>
</ul>
<?php
 $this->old_stock_chart($transf_stock_id_get);//recursion
			}
			//return $transf_stock_id_get;
			}

function admin_name($ID)
{   
$sqlState = "select * from tbl_admin where admin_id = '$ID' and deleteflag = 'active'";
$rsState  = mysqli_query($GLOBALS["___mysqli_ston"],  $sqlState);
$rowState = mysqli_fetch_object($rsState);
$name	  = $rowState->admin_fname." ".$rowState->admin_lname ;
return ucfirst($name);
}

function office_location_name($ID)
{   
$sqlState = "select * from tbl_contact_us_page_location where id = '$ID' and deleteflag = 'active'";
$rsState  = mysqli_query($GLOBALS["___mysqli_ston"],  $sqlState);
$rowState = mysqli_fetch_object($rsState);
$location_name	  = $rowState->location;
return ucfirst($location_name);
}

function stock_office_location_name($ID)
{   
$sqlState = "select * from tbl_location where id = '$ID' and deleteflag = 'active'";
$rsState  = mysqli_query($GLOBALS["___mysqli_ston"],  $sqlState);
$rowState = mysqli_fetch_object($rsState);
$location_name	  = $rowState->location;
return ucfirst($location_name);
}

function total_demo_stock($ID)
{   
$sqlState = "SELECT *, sum(qty) as product_stock_sum  from tbl_demo_stock where pro_id = '$ID' ";
$rsState  = mysqli_query($GLOBALS["___mysqli_ston"],  $sqlState);
$rowState = mysqli_fetch_object($rsState);
$product_stock_sum	  = $rowState->product_stock_sum;
return $product_stock_sum;
}function admin_abrv($ID)
{
$sqlState = "select admin_abrv from tbl_admin where admin_id = '$ID' and deleteflag = 'active'";
$rsState  = mysqli_query($GLOBALS["___mysqli_ston"],  $sqlState);
$rowState = mysqli_fetch_object($rsState);
if($rowState->admin_abrv!='0')
{
$admin_abrv	  = $rowState->admin_abrv;
}
else
{
$admin_abrv	  = "";
}
return $admin_abrv;
}

function admin_email($ID)
{
$sqlState = "select admin_email from tbl_admin where admin_id = '$ID' and deleteflag = 'active'";
$rsState  = mysqli_query($GLOBALS["___mysqli_ston"],  $sqlState);
$rowState = mysqli_fetch_object($rsState);
$name	  = $rowState->admin_email;
return $name;
}

////////////////////////////////////////

function customer_name($ID)
{
$sqlcust_name = "select fname,lname from tbl_comp where id = '$ID' and deleteflag = 'active'";
$rscust_name  = mysqli_query($GLOBALS["___mysqli_ston"],  $sqlcust_name);
$rowcust_name = mysqli_fetch_object($rscust_name);
$name	  = $rowcust_name->fname." ".$rowcust_name->lname ;
return ucwords($name);
}

function company_names($ID)
{
$sqlcust_name = "select comp_name from tbl_comp where id = '$ID' and deleteflag = 'active'";
$rscust_name  = mysqli_query($GLOBALS["___mysqli_ston"],  $sqlcust_name);
$rowcust_name = mysqli_fetch_object($rscust_name);
$name	  = $rowcust_name->comp_name;
return ucwords($name);
}

function company_number($ID)
{
$sqlcust_name = "select mobile_no from tbl_comp where id = '$ID' and deleteflag = 'active'";
$rscust_name  = mysqli_query($GLOBALS["___mysqli_ston"],  $sqlcust_name);
$rowcust_name = mysqli_fetch_object($rscust_name);
$name	  = $rowcust_name->mobile_no;
return ucwords($name);
}

////////////////////////////////////////

function designation_name($ID)
{
	if(is_numeric($ID))
{
$sqldesignation_name = "select designation_name from tbl_designation where designation_id = '$ID' and deleteflag = 'active'";
$rsdesignation_name  = mysqli_query($GLOBALS["___mysqli_ston"],  $sqldesignation_name);
$rowdesignation_name = mysqli_fetch_object($rsdesignation_name);
$designation_name	 = $rowdesignation_name->designation_name;	
}

else
{
$designation_name 	= $ID;
}

return ucfirst($designation_name);
}

////////////////////////////////////////

function admin_role_id($ID)
{
$sqladmin_accs = "select admin_role_id from tbl_admin where admin_id = '$ID' and deleteflag = 'active'";
$rsadmin_accs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sqladmin_accs);
$rowadmin_accs = mysqli_fetch_object($rsadmin_accs);
$admin_role_id	 = $rowadmin_accs->admin_role_id;	
return $admin_role_id;
}//individual permisson functions starts 23-05-2019
function admin_add_del_export_permision($ID,$page_id)
{   
$sqladmin_accs = "select assign_perm from tbl_admin_access_in_module where admin_id = '$ID' and page_id='$page_id' and deleteflag = 'active'";
$rsadmin_accs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sqladmin_accs);
$rowadmin_accs = mysqli_fetch_object($rsadmin_accs);
$assign_perm  = $rowadmin_accs->assign_perm;	
return $assign_perm;
}function website_page_id($page_name)
{   
$sql_page_id = "select page_id from tbl_website_page where page_name LIKE '$page_name' and deleteflag = 'active'";
$rsadmin_page_id  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql_page_id);
$rowadmin_page_id = mysqli_fetch_object($rsadmin_page_id);
$page_id	 = $rowadmin_page_id->page_id;	
return $page_id;
}
function indiv_permission_sel($page_id,$admin_role,$pcode,$perm_name)
{
	   
$sql_page_check_individual="select * from tbl_admin_access_in_module where page_id='".$page_id."' and admin_role_id='".$admin_role."' and admin_id='".$pcode."' and assign_perm='".$perm_name."'";
$rs_page_check_individual=mysqli_query($GLOBALS["___mysqli_ston"],  $sql_page_check_individual);	
$rs_num=mysqli_num_rows($rs_page_check_individual);	
	
return $rs_num;
}
//individual permisson functions ends 23-05-2019

function admin_sub_team_lead($ID)

{   

$sqladmin_accs = "select sub_team_lead from tbl_team where team_id = '$ID' and deleteflag = 'active'";

$rsadmin_accs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sqladmin_accs);

$rowadmin_accs = mysqli_fetch_object($rsadmin_accs);

$sub_team_lead	 = $rowadmin_accs->sub_team_lead;	

if($sub_team_lead=='' || $sub_team_lead=='0')

{

$sub_team_lead="0";

}
return $sub_team_lead;
}

function admin_sub_team_lead2($ID)
{   
$sqladmin_accs = "select sub_team_lead2 from tbl_team where team_id = '$ID' and deleteflag = 'active'";
$rsadmin_accs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sqladmin_accs);
$rowadmin_accs = mysqli_fetch_object($rsadmin_accs);
$sub_team_lead = $rowadmin_accs->sub_team_lead2;	
if($sub_team_lead=='' || $sub_team_lead=='0')
{
$sub_team_lead="0";
}
return $sub_team_lead;
}

function admin_sub_team_lead3($ID)
{   
$sqladmin_accs = "select sub_team_lead3 from tbl_team where team_id = '$ID' and deleteflag = 'active'";
$rsadmin_accs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sqladmin_accs);
$rowadmin_accs = mysqli_fetch_object($rsadmin_accs);
$sub_team_lead = $rowadmin_accs->sub_team_lead3;	
if($sub_team_lead=='' || $sub_team_lead=='0')
{
$sub_team_lead="0";
}
return $sub_team_lead;
}

function admin_sub_team_lead_admin_id($ID)
{   
$sqladmin_accs = "select sub_team_lead from tbl_admin_team_members where sub_team_lead = '$ID' and deleteflag = 'active'";
$rsadmin_accs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sqladmin_accs);
$rowadmin_accs = mysqli_fetch_object($rsadmin_accs);
$sub_team_lead	 = $rowadmin_accs->sub_team_lead;	
if($sub_team_lead=='' || $sub_team_lead=='0')
{
$sub_team_lead="0";
}
return $sub_team_lead;
}

function admin_sub_team_member1($ID)
{   
$sqladmin_accs = "select sub_team_member1 from tbl_admin_team_members where sub_team_lead = '$ID' and deleteflag = 'active'";
$rsadmin_accs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sqladmin_accs);
while($rowadmin_accs = mysqli_fetch_object($rsadmin_accs))
{
$sub_team_member1[]	 = $rowadmin_accs->sub_team_member1;	
}
if($sub_team_member1=='' || $sub_team_member1=='0')
{
$sub_team_member1="0";
}
return $sub_team_member1;
}

function admin_sub_team_member2($ID)
{   
$sqladmin_accs = "select sub_team_member1 from tbl_admin_team_members where sub_team_lead = '$ID' and deleteflag = 'active'";
$rsadmin_accs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sqladmin_accs);
$rowadmin_accs = mysqli_fetch_object($rsadmin_accs);
$sub_team_member2	 = $rowadmin_accs->sub_team_member2;	
if($sub_team_member2=='' || $sub_team_member2=='0')
{
$sub_team_member2="0";
}
return $sub_team_member2;
}

function admin_sub_team_member3($ID)
{   
$sqladmin_accs = "select sub_team_member3 from tbl_admin_team_members where sub_team_lead = '$ID' and deleteflag = 'active'";
$rsadmin_accs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sqladmin_accs);
$rowadmin_accs = mysqli_fetch_object($rsadmin_accs);
$sub_team_member3	 = $rowadmin_accs->sub_team_member3;
if($sub_team_member3=='' || $sub_team_member3=='0')
{
$sub_team_member3="0";
}
return $sub_team_member3;
}

function admin_team_lead($ID)

{   

$sqladmin_accs = "select admin_team_lead from tbl_admin where admin_id = '$ID' and deleteflag = 'active'";

$rsadmin_accs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sqladmin_accs);

$rowadmin_accs = mysqli_fetch_object($rsadmin_accs);

$admin_team_lead	 = $rowadmin_accs->admin_team_lead;	

if($admin_team_lead=='' || $admin_team_lead=='0')

{

$admin_team_lead="0";

}

return $admin_team_lead;

}
function admin_teams($ID)

{   

$sqladmin_accs ="SELECT GROUP_CONCAT(DISTINCT(admin_team)) as admin_team FROM `tbl_admin` WHERE admin_team_lead='$ID' and deleteflag = 'active'";

$rsadmin_accs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sqladmin_accs);

$rowadmin_accs = mysqli_fetch_object($rsadmin_accs);

$admin_teams	 = $rowadmin_accs->admin_team;	

return $admin_teams;

}
////////////////////////////////////////

function admin_team($ID)

{

$sqladmin_accs = "select admin_team from tbl_admin where admin_id = '$ID' and deleteflag = 'active'";

$rsadmin_accs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sqladmin_accs);

$rowadmin_accs = mysqli_fetch_object($rsadmin_accs);

$admin_team	 = $rowadmin_accs->admin_team;	

return $admin_team;

}// function for product name 

function product_name($id)

{   

$sql = "select pro_title from tbl_products where pro_id = '$id' ";

$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);

$row = mysqli_fetch_object($rs);

$pro_title	  = $row->pro_title;

return $pro_title;

}
// function for product name 

function product_entry_id($id)

{   

$sql = "select pro_id from tbl_products_entry where pro_id_entry = '$id'";

$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);

$row = mysqli_fetch_object($rs);

$pro_title	  = $row->pro_id;

return $pro_title;

}
function product_part($id)

{   

$sql = "select model_no from tbl_products_entry where pro_id = '$id'";

$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);

$row = mysqli_fetch_object($rs);

$pro_title	  = $row->model_no;

return $pro_title;

}
function product_part_srf($id)

{   

$sql = "select model_no from tbl_products_entry where pro_id_entry = '$id'";

$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);

$row = mysqli_fetch_object($rs);

$pro_title	  = $row->model_no;

return $pro_title;

}
function product_price($id)

{   

//status & deleteflag added by rumit 18may2018 before it shows 32000 price which is inactive

//$sql = "select pro_price_entry from tbl_products_entry where status='active' and deleteflag='active' and pro_id = '$id' ";
$sql = "select pro_price_entry from tbl_products_entry where status='active' and deleteflag='active' and pro_id = '$id' and price_list='pvt'";

$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);

$row = mysqli_fetch_object($rs);

$pro_title	  = $row->pro_price_entry;

return $pro_title;

}

//added on 07-aug-2020
function product_price_with_price_type($id,$Price_type)
{   
//status & deleteflag added by rumit 18may2018 before it shows 32000 price which is inactive
$sql = "select pro_price_entry from tbl_products_entry where status='active' and deleteflag='active' and pro_id = '$id' and price_list='$Price_type'";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
$pro_title	  = $row->pro_price_entry;
return $pro_title;
}

function product_price_quick_offer($id,$price_list)

{   

//status & deleteflag added by rumit 18may2018 before it shows 32000 price which is inactive

$sql = "select pro_price_entry from tbl_products_entry where status='active' and deleteflag='active' and pro_id = '$id' and price_list='$price_list'";

$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);

$row = mysqli_fetch_object($rs);

$pro_title	  = $row->pro_price_entry;

return $pro_title;

}
function product_price_srf($id)

{   

$sql = "select pro_price_entry from tbl_products_entry where pro_id_entry = '$id'";

$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);

$row = mysqli_fetch_object($rs);

$pro_title	  = $row->pro_price_entry;

return $pro_title;

}

// function for product stock

function product_stock($id)

{   

$sql = "select ware_house_stock from tbl_products where pro_id = '$id' and deleteflag = 'active'";

$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);

$row = mysqli_fetch_object($rs);

$pro_title	  = $row->ware_house_stock;

return $pro_title;

}
function product_discount($id)

{   

$sql = "select pro_max_discount from tbl_products where pro_id = '$id' and deleteflag = 'active'";

$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);

$row = mysqli_fetch_object($rs);

$product_discount	  = $row->pro_max_discount;

return $product_discount;

}
function product_acl_code($id)

{   

$sql = "select model_no from tbl_products_entry where pro_id = '$id' and deleteflag = 'active'";

$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);

$row = mysqli_fetch_object($rs);

"MODEL NO BY FUNCTION ".$pro_title	  = $row->model_no;

return $pro_title;

}
function product_img($id)

{   

$sql = "select pro_image_small from tbl_products where pro_id = '$id' and deleteflag = 'active'";

$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);

$row = mysqli_fetch_object($rs);

$pro_title	  = $row->pro_image_small;

return $pro_title;

}
function performa_invoice_id($id)

{   

$sql = "SELECT pi_id,O_Id FROM `tbl_performa_invoice` where O_Id = '$id' and deleteflag = 'active'";

$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);

$row = mysqli_fetch_object($rs);

$pi_id	  = $row->pi_id;

return $pi_id;

}
function performa_invoice_status($pi_id)

{

$sql = "SELECT pi_status FROM `tbl_performa_invoice` where pi_id = '$pi_id' and save_send='yes' and deleteflag = 'active'";

$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);

$row = mysqli_fetch_object($rs);

$pi_status	  = $row->pi_status;

return $pi_status;

}
/*****Enquiry module started****************/
//enquiry generated id
function enqiry_details($ID)
{
$sqlState = "select enq_id,Cus_msg,Cus_Name,Cus_email,Cus_mob,city,state,acc_manager,ref_source,cust_segment,Enq_Date,hot_productnote,hot_productnoteother,product_category from tbl_web_enq_edit where ID = '$ID' and deleteflag = 'active'";
$rsState  = mysqli_query($GLOBALS["___mysqli_ston"],  $sqlState);
$rowState = mysqli_fetch_object($rsState);
$details	  = $rowState;
return $details;
}

function enqiry_lead_source_edited($ID)
{
$sqlref_source = "select ref_source from tbl_web_enq_edit where ID = '$ID' and deleteflag = 'active'";
$rsref_source  = mysqli_query($GLOBALS["___mysqli_ston"],  $sqlref_source);
$rowref_source = mysqli_fetch_object($rsref_source);
$enq_ref_source	  = $rowref_source->ref_source;
return $enq_ref_source;
}

function get_enquiry_edited_id($id)
{
$sql = "select ID from tbl_web_enq_edit where enq_id = '$id' and deleteflag = 'active'";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
$ID	  = $row->ID;
return $ID;
}

function get_eid_generation_date($id)
{
$sql = "select Enq_Date from tbl_web_enq_edit where enq_id = '$id' and deleteflag = 'active'";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
$ID	  = $row->Enq_Date;
return $ID;
}

function get_eid_add_date($id)
{
$sql = "select Enq_Date from tbl_web_enq where id = '$id' and deleteflag = 'active'";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
$ID	  = $row->Enq_Date;
return $ID;
}

function get_eid_DO_NO($id)
{
$sql = "select DO_ID,PO_NO,D_Order_Date,PO_Date from tbl_delivery_order where O_Id = '$id' ";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
$po_do_details	  = $row;
return $po_do_details;
}//added on august-2020


function get_DO_ID($id)
{
$sql = "select DO_ID from tbl_delivery_order where O_Id = '$id' ";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
$DO_ID	  = $row->DO_ID;
return $DO_ID;
}//added on august-2020


function get_delivery_order_date($id)
{
$sql = "select D_Order_Date from tbl_delivery_order where O_Id = '$id' ";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
$D_Order_Date	  = $row->D_Order_Date;
return $D_Order_Date;
}

function get_invoice_date($id)
{   
$sql = "select I_date from tble_invoice where o_id = '$id' ";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
$I_date	  = $row->I_date;
return $I_date;

}

//added on august-2020
function get_follow_up_date($id)
{   
$sql = "select follow_up_date from tbl_order where orders_id = '$id' ";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
$follow_up_date	  = $row->follow_up_date;
return $follow_up_date;

}

function cutomer_GSTno($orderid)
{
$sql = "select Buyer_CST from tbl_delivery_order where O_Id = '$orderid' ";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
$Buyer_CST	  = $row->Buyer_CST;
return $Buyer_CST;
}

function consignee_GSTno($orderid)
{
$sql = "select Con_CST from tbl_delivery_order where O_Id = '$orderid' ";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
$Con_CST	  = $row->Con_CST;
return $Con_CST;
}

function buyer_mobile($orderid)
{
$sql = "select Buyer_Mobile from tbl_delivery_order where O_Id = '$orderid' ";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
$Buyer_Mobile	  = $row->Buyer_Mobile;
return $Buyer_Mobile;
}

function buyer_email($orderid)
{
$sql = "select Buyer_Email from tbl_delivery_order where O_Id = '$orderid' ";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
$Buyer_Email	  = $row->Buyer_Email;
return $Buyer_Email;
}

function buyer_contact_person($orderid)
{
$sql = "select Buyer_Name from tbl_delivery_order where O_Id = '$orderid' ";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
$buyer_contact_person	  = $row->Buyer_Name;
return $buyer_contact_person;
}

//added on 26-feb-2020
function buyer_address($orderid)
{
$sql = "select Cus_Com_name,Buyer,Buyer_Name,Buyer_Email,Buyer_Mobile,Buyer_CST from tbl_delivery_order where O_Id = '$orderid' ";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
$Buyer= $row->Cus_Com_name.' 
'.$row->Buyer.'
Name: '.$row->Buyer_Name.'
Mobile: '.$row->Buyer_Mobile.'
Email: '.$row->Buyer_Email.'
GST No: '.$row->Buyer_CST;
return  ($Buyer);
}

function consignee_address($orderid)
{
$sql = "select Con_Com_name,Consignee,Con_Name,Con_Mobile,Con_Email,Con_CST from tbl_delivery_order where O_Id = '$orderid' ";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
$consignee= $row->Con_Com_name.' 
'.$row->Consignee.'
Name: '.$row->Con_Name.'
Mobile: '.$row->Con_Mobile.'
Email: '.$row->Con_Email.'
GST No: '.$row->Con_CST;
return  ($consignee);
}

function customer_notification($orderid)
{
$sql = "select customer_notification from tbl_delivery_challan_comment where order_id = '$orderid' order by delivery_challan_comment_id desc";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],$sql);
$row = mysqli_fetch_object($rs);
$customer_notification= $row->customer_notification;
return  ($customer_notification);
}

function Con_Email($orderid)
{
$sql = "select Con_Email from tbl_delivery_order where O_Id = '$orderid' ";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],$sql);
$row = mysqli_fetch_object($rs);
$Con_Email	  = $row->Con_Email;
if($Con_Email=='')
{
	$Con_Email="N/a";
}

return $Con_Email;

}

function consignee_mobile($orderid)
{
$sql = "select Con_Mobile from tbl_delivery_order where O_Id = '$orderid' ";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],$sql);
$row = mysqli_fetch_object($rs);
$Con_Mobile	  = $row->Con_Mobile;
if($Con_Mobile=='')
{
	$Con_Mobile="N/a";
}
return $Con_Mobile;
}

function consignee_contact_person($orderid)
{
$sql = "select Con_Name from tbl_delivery_order where O_Id = '$orderid' ";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],$sql);
$row = mysqli_fetch_object($rs);
$Con_Name	  = $row->Con_Name;
if($Con_Name=='')
{
	$Con_Name="N/a";
}
return $Con_Name;
}

function tax_per($orderid)
{
$sql = "select Tax_Per from tbl_delivery_order where O_Id = '$orderid' ";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
$Tax_Per	  = $row->Tax_Per;
return $Tax_Per;
}

function PO_no($orderid)
{
$sql = "select PO_NO from tbl_delivery_order where O_Id = '$orderid' ";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
$PO_NO	  = $row->PO_NO;
return $PO_NO;
}

function PO_date_delivery_order($orderid)

{

$sql = "select PO_Date from tbl_delivery_order where O_Id = '$orderid' ";

$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);

$row = mysqli_fetch_object($rs);

//$PO_Date	  = $this->getDateformate($row->PO_Date,'ymd','mdy','-');;//commented by rumit for date format change on 30-07-2019

$PO_Date	  = $row->PO_Date;

//return $this->getDateformate($row_product_vendor_price->Date,'ymd','mdy','-');

return $PO_Date;

}
function get_offer_id_enq_lead_id($id)

{

$sql = "select orders_id from tbl_order where lead_id = '$id' and deleteflag = 'active'";

$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);

$row = mysqli_fetch_object($rs);

$orders_id	  = $row->orders_id;

return $orders_id;

}
function get_lead_id_by_enquiry_edited_id($id)

{

$sql = "select ID from tbl_lead where enq_id = '$id' and deleteflag = 'active'";

$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);

$row = mysqli_fetch_object($rs);

$ID	  = $row->ID;

return $ID;

}
function get_enq_id($id)

{

$sql = "select enq_id from tbl_lead where id = '$id' and deleteflag = 'active'";

$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);

$row = mysqli_fetch_object($rs);

$enq_id	  = $row->enq_id;

return $enq_id;

}

function get_alphanumeric_id_enq_lead_id($id)
{
	if($id!='' && $id!='0')
	{
$sql = "select orders_id,offercode from tbl_order where lead_id = '$id' and deleteflag = 'active'";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
$offercode	  = $row->offercode.'-'.$row->orders_id;
	}
	else
	{
$offercode	  = "N/A";		
	}
return $offercode;
}

//created on 08-july-2020 to get enq_id from lead_id 
function get_enq_id_from_lead_id($lead_id)
{   
$sql = "select enq_id from tbl_web_enq_edit where lead_id = '$lead_id' and deleteflag = 'active'";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
$ID	  =$row->enq_id;
return $ID;
}

function get_lead_id_from_offer($id)

{   

$sql = "select lead_id from tbl_order where orders_id = '$id' and deleteflag = 'active'";

$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);

$row = mysqli_fetch_object($rs);

$lead_id	  =$row->lead_id;

return $lead_id;

}
function get_alphanumeric_id_enq_order_id($id)

{   

$sql = "select orders_id,offercode from tbl_order where orders_id = '$id' and deleteflag = 'active'";

$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);

$row = mysqli_fetch_object($rs);

$offercode	  = $row->offercode.'-'.$row->orders_id;

return $offercode;

}

function get_offercode($id)

{   

$sql = "select offercode from tbl_order where orders_id = '$id' and deleteflag = 'active'";

$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);

$row = mysqli_fetch_object($rs);

$offercode	  = $row->offercode;

return $offercode;
}
function get_offer_date_enq_lead_id($id)

{   

$sql = "select date_ordered from tbl_order where lead_id = '$id' and deleteflag = 'active'";

$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);

$row = mysqli_fetch_object($rs);

$date_ordered	  = $row->date_ordered;

return $date_ordered;

}

//added on 07-aug-2020
function get_offer_date_order_id($id)
{   
$sql = "select date_ordered from tbl_order where orders_id = '$id' and deleteflag = 'active'";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
$date_ordered	  = $row->date_ordered;
return $date_ordered;
}

/*****Enquiry module ends****************/

function product_qty($id)

{   

$sql = "select ware_house_stock from tbl_products where pro_id = '$id' and deleteflag = 'active'";

$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);

$row = mysqli_fetch_object($rs);

$pro_title	  = $row->ware_house_stock;

return $pro_title;

}
////////////////////////////////////////////////////////////////////////////////////////////
function FieldValue($tableName, $fieldFetch, $query = " 1 = 1 ")

{   

$sql = "select * from $tableName where $query";

$rs	 = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);

if(mysqli_num_rows($rs)>0)

{

$row = mysqli_fetch_object($rs);

return $row->$fieldFetch;

}

else

{

return -999;

}

}
//////////////////////// File uploading Code ///////////////////////////////////////////////
function fileUpload($destnation, $filename, $codeupname="case")

{   

//echo $destnation; exit;

if($_FILES[$filename]['name'] != "")

{

$unique_id_query = strtoupper(substr(md5(uniqid(rand(), true)), 0 ,6));

$unique_add      = $unique_id_query;

$unique_name     = $destnation.$codeupname.$unique_add;

//		$unique_name     = $destnation;

if($_FILES[$filename]["error"] > 0)

{

return -1;		// file error 

}

else

{

$uploadedfile = $_FILES[$filename]['tmp_name'];

//$destination1 = $_FILES[$filename]['name'];

$destination1 = $unique_name.$_FILES[$filename]['name']; // for add unique name with code

$path		  = "../".$destination1;

$Result 	  = move_uploaded_file($uploadedfile, $path);

if(!$Result)

{

return -1;

}

else

{

return $destination1; // returning image name and path

}

}

}

else

{

return -1; // no file persent 

}

}
function get_company_list()

{   

$sql = "select * from tbl_comp group by comp_name";

$rs	 = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);

while($row=mysqli_fetch_object($rs))

{

echo "<option value='".$row->comp_name."' ";

if(($_REQUEST['cusomers_company'])  AND  ($_REQUEST['cusomers_company']==$row->comp_name)) { echo "selected"; }

echo " >".$row->comp_name."</option>";

}

}function get_desi_list()

{   

$sql = "select * from tbl_comp group by designation_id";

$rs	 = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);

while($row=mysqli_fetch_object($rs))

{

echo "<option value='".$row->designation_id."' ";

if(($_REQUEST['cusomers_desg'])  AND  ($_REQUEST['cusomers_desg']==$row->designation_id)) { echo "selected"; }

echo " >".$row->designation_id."</option>";

}
}function get_state_list()

{   

$sql = "select * from tbl_comp group by state";

$rs	 = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);

while($row=mysqli_fetch_object($rs))

{

echo "<option value='".$row->state."' ";

if(($_REQUEST['cusomers_sate'])  AND  ($_REQUEST['cusomers_sate']==$row->state)) { echo "selected"; }

echo " >".$row->state."</option>";

}

}function get_city_list()

{   

$sql = "select * from tbl_comp where state='".$_REQUEST['cusomers_sate']."' group by city ";

$rs	 = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);

while($row=mysqli_fetch_object($rs))

{

echo "<option value='".$row->city."' ";

if(($_REQUEST['cusomers_city'])  AND  ($_REQUEST['cusomers_city']==$row->city)) { echo "selected"; }

echo " >".$row->city."</option>";

}
}////////////////////////////////////////////////////////////////////////////////////////////////////

function fetchCategoryTree($parent = 0, $spacing = '', $user_tree_array = '') {

  if (!is_array($user_tree_array))

  $user_tree_array = array();

  $sql = "SELECT `id`, `comp_name`, `parent_id` FROM `tbl_comp` WHERE 1 AND `parent_id` = $parent ORDER BY id ASC";

  $query = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);

  if (mysqli_num_rows($query) > 0) {

    while ($row = mysqli_fetch_object($query)) {

      $user_tree_array[] = array("id" => $row->id, "comp_name" => $spacing . $row->comp_name);

      $user_tree_array = $this->fetchCategoryTree($row->id, $spacing . '&nbsp;&nbsp;&raquo;', $user_tree_array);

   }

 }

  return $user_tree_array;
}
function fetchCategoryTreeList($parent = 0, $user_tree_array = '') {

    if (!is_array($user_tree_array))

    $user_tree_array = array();
  $sql = "SELECT `id`, `comp_name`, `parent_id` FROM `tbl_comp` WHERE 1 AND `parent_id` = $parent ORDER BY id ASC";

  $query = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);

  if (mysqli_num_rows($query) > 0) {

     $user_tree_array[] = "<ul>";

    while ($row = mysqli_fetch_object($query)) {

	  $user_tree_array[] = "<li>". $row->comp_name."</li>";

      $user_tree_array = $this->fetchCategoryTreeList($row->id, $user_tree_array);

    }

	$user_tree_array[] = "</ul>";

  }

  return $user_tree_array;

}
function company_tree()

{   
$sqlcat="Select id,parent_id,comp_name,co_extn_id,co_division,co_city,office_type, address, create_date from tbl_comp where deleteflag = 'active'  and india_mart_co='no'  order by comp_name asc, create_date desc";

$rs_cat=mysqli_query($GLOBALS["___mysqli_ston"],  $sqlcat);

while($row_cat=mysqli_fetch_object($rs_cat))

{

$categories[]=$row_cat;

}
$items= $categories;

$childs = array();

foreach($items as $item)

   $childs[$item->parent_id][] = $item;

foreach($items as $item) if (isset($childs[$item->id]))

   $item->childs = $childs[$item->id];

$tree = $childs[0];

return $tree;

}
function company_tree_latest($limit=15)

{   

$sqlcat="Select id,comp_name,co_extn_id,co_division,co_city,office_type from tbl_comp where deleteflag = 'active' and india_mart_co='no' order by id desc limit 0,$limit";

$rs_cat=mysqli_query($GLOBALS["___mysqli_ston"],  $sqlcat);

while($row_cat=mysqli_fetch_object($rs_cat))

{

$categories[]=$row_cat;

}

$items= $categories;//show all latest companies wheteher it is main or sub co.  22/5/2019

return $items;
}

/*
function get_order_status($orderID)
{   
$sql = "select orders_id, follow_up_date, order_type, date_ordered, orders_status, orders_date_finished from tbl_order where deleteflag='active' and orders_id='$orderID'";
$rs	 = mysqli_query($GLOBALS["___mysqli_ston"],  $sql); 
if(mysqli_num_rows($rs)>0)
{
$row = mysqli_fetch_object($rs);
$orders_status	  = $row->orders_status;
return $orders_status;
}
}*/
function get_order_status($orderID)
{   
$sql = "select orders_status from tbl_order where deleteflag='active' and orders_id='$orderID'";
$rs	 = mysqli_query($GLOBALS["___mysqli_ston"],  $sql); 
if(mysqli_num_rows($rs)>0)
{
$row = mysqli_fetch_object($rs);
$orders_status	  = $row->orders_status;
return $orders_status;
}
}

// function for date format global added  by rumit 25-08-2019 

function date_format_india_with_time($date)

{

return  $date_formate_with_time=date("d/M/Y - H:i:s", strtotime($date));

}
function date_format_india($date)

{

return $date_formate=date("d/M/Y", strtotime($date));
}
//get invoice generated date added on 23-08-2019  by rumit

function get_invoice_date_by_order_id($id)

{   

$sql = "select I_date from tble_invoice where o_id = '$id' ";

$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);

$row = mysqli_fetch_object($rs);

$I_date	  = date("Y-m-d", strtotime($row->I_date));

return $I_date;
}
//function for subcompany showing in company Manager 24th-25 june 2019

function showSubCompany($cat_id, $dashes, $edit_perm, $del_perm)

	{   

		$dashes.= '&nbsp;&nbsp;&nbsp;';

		if(strlen(trim($order))<=0)

		{

			$order		= "asc";

		}

		if(strlen(trim($order_by))<=0)

		{

			$order_by	= 'comp_name';

		}
 		$sql_sub_cat	= "SELECT distinct id,parent_id,fname,comp_name,lname,mobile_no,create_date,status,city,acc_manager,co_extn_id,co_division,co_city,office_type,key_customer from tbl_comp where parent_id=$cat_id and deleteflag='active' order by ".$order_by." ".$order;

		$rsSub = mysqli_query($GLOBALS["___mysqli_ston"],  $sql_sub_cat);

		if(mysqli_num_rows($rsSub) >= 1)

		{

			while($rowSub = mysqli_fetch_array($rsSub))

			{

?>
<tr  class="text"  style="background-color:#fff" >
  <td class="pad" valign="middle" width="5%"><input type="checkbox" name="comp_id[]" id="comp_name" value="<?php echo $rowSub['id'];?>" />
    <?php //echo $rowSub['id'];?><?php if($rowSub['key_customer']=='1'){?>
                    <img src="images/retail_cust.png" title="Retail" border="0"  />
					<?php }?>
<?php if($rowSub['key_customer']=='2'){?>
                    <img src="images/sub_key.png" title="Sub Key Customer" border="0"  />
					<?php }?>       
<?php if($rowSub['key_customer']=='3'){?>
                    <img src="images/key_customer.png" title="Key Customer" border="0"  />
					<?php }?> </td>
  <td class="pad" width="20%"><?php echo $dashes."&raquo; ". stripslashes($this->company_name_return($rowSub['id'])); ?>
    <?php //echo $this->company_extn_name($rowSub['co_extn_id']); ?>
    <?php if($rowSub['co_division']!='0' && $rowSub['co_division']!=''){echo "/".$rowSub['co_division'];}?>
    <?php if($rowSub['co_city']!='0' && $rowSub['co_city']!='') {echo "/".$rowSub['co_city'];}?>
    <?php //echo "Count:-".lead_count_comp($rowSub['id']);?></td>
  <td  class="pad" align="center"><?php echo $this->CityName($rowSub['city']);?></td>
  <td align="left" valign="middle"><?php echo ucfirst($rowSub['fname']) ; ?> <?php echo ucfirst($rowSub['lname']) ; ?></td>
  <td  class="pad" align="left"><?php if($rowSub['acc_manager']!=''){ echo $this->account_manager_name_full($rowSub['acc_manager']);} else { echo "N/A";}?></td>
  <td class="pad" valign="middle" ><?php echo ucfirst(stripslashes($rowSub['mobile_no'])) ; ?></td>
  <td align="center" valign="middle"><?php echo date("d/M/Y",strtotime($rowSub['create_date'])) ; ?></td>
  <?php if($_SESSION["AdminLoginID_SET"]==4) { ?>
  <td align="center" valign="middle"><?php 	if($rowSub['status'] == "active")	{?>
    <img src="images/green.gif" title="Active" border="0"  /> &nbsp; &nbsp; <a href="index.php?pagename=company_manager&action=ChangeStatus&pcode=<?php echo $rowSub['id'];?>&pageno=<?php echo $page;?>&records=<?php echo $max_results;?>"><img src="images/red_light.gif" title="Inactive" border="0"  /></a>
    <?php

	}

	// if($rowSub['status'] == "inactive")

	else

	{

?>
    <a href="index.php?pagename=company_manager&action=ChangeStatus&pcode=<?php echo $rowSub['id'];?>&pageno=<?php echo $page;?>&records=<?php echo $max_results;?>"><img src="images/green_light.gif" title="Active" border="0"  /></a> &nbsp; &nbsp; <img src="images/red.gif" title="Inactive" border="0"  />
    <?php		

	}?></td>
  <?php } ?>
  <td align="center" valign="middle"><?php if($edit_perm=='1'){?>
    <a href="index.php?pagename=add_company_new&action=edit&pcode=<?php echo $rowSub['id'];?>"> <img src="images/e.gif" border="0"  alt="Edit"/></a>
    <?php } else {?>
    <a href="#;"> <img src="images/protect.png" border="0"  alt="Edit"/></a>
    <?php }

if($del_perm=='1')

{

?>
    &nbsp; &nbsp; <a href="index.php?pagename=company_manager&action=delete&pcode=<?php echo $rowSub['id'];?>" onclick='return del();'> <img src="images/x.gif" border="0" alt="Delete" onclick="return confirm('Are You Sure to Delete this Company?')" /></a>
    <?php }

	else

		{

			?>
    <?php /*?>   &nbsp; &nbsp; <a href="#;" onclick='return del();'> <img src="images/protect.png" border="0" alt="Delete" onclick="return confirm('Are You Sure to Delete this Company?')" /></a><?php */?>
    <?php //}
	} ?></td>
  <td align="center" valign="middle"><a href="index.php?pagename=company_contact_person&action=view&pcode=<?php echo $rowSub['id'];?>" title="View More Company Persons Name" target="_blank"><img src="images/new-user2.png" border="0"  alt="View Company Persons Name" width="45" align="middle"/></a></td>
  <td align="center" valign="middle">&nbsp; <a href="index.php?pagename=lead_history&action=view&pcode=<?php echo $rowSub['id'];?>" title="View Company Leads/Offers/Orders History" target="_blank"> <img src="images/history.svg" border="0"  alt="View Company Lead History" width="30" align="middle" /></a></td>
</tr>
<?php 

			}

 		}

	}

//selling product qty for stock report added on 23-09-2019	

function outgoing_selling_qty($ProID,$item_code,$month_search=0,$year_search=0,$datevalid_from=0,$datevalid_to=0)

{

	if($ProID!= '')		

		{

			$searchproid = "  and tbl_order_product.pro_model ='$ProID' ";

		}

		else

		{

		$searchproid = "";
		}

		if($item_code!= '')		

		{

			$searchitemcode = " and tbl_order_product.pro_model = '$item_code' ";

		}

		else

		{

			$searchitemcode = "";

		}
if($datevalid_from!='0' && $datevalid_to!='0' && $datevalid_from!='' && $datevalid_to!='')

	{

$date_range_search=" AND (date( tbl_order.date_ordered ) BETWEEN '$datevalid_from' AND '$datevalid_to')";
	}

	if($year_search!='' && $year_search!='0')

	{

	//$orders_status='Pending';

	$year_search_search=" AND YEAR( tbl_order.date_ordered ) IN ( $year_search )";

	}

if($month_search!='' && $month_search!='0')

	{

	//$orders_status='Pending';

	$month_search_search=" AND MONTH( tbl_order.date_ordered ) IN ( $month_search )";

}

$searhRecord =  $year_search_search.$month_search_search.$date_range_search.$searchproid.$searchitemcode;
$sql = "select tbl_order_product.pro_id as 'Product ID', tbl_order_product.pro_name as 'Product Name', count(tbl_order_product.pro_id) as 'Selling Count', sum(tbl_order_product.pro_quantity) as 'selling_qty' from tbl_order_product INNER JOIN tbl_order on tbl_order.orders_id = tbl_order_product.order_id where tbl_order.deleteflag = 'active' and tbl_order.orders_status!='Failed' and tbl_order.orders_status!='offer Cancelled' and tbl_order.orders_status!='On Hold' and tbl_order.orders_status!='Pending' and tbl_order_product.deleteflag = 'active' $searhRecord  group by tbl_order_product.pro_id order by count(tbl_order_product.pro_id) desc";

$rs	 = mysqli_query($GLOBALS["___mysqli_ston"],$sql);

$row=mysqli_fetch_object($rs);

if($row->selling_qty=='' || $row->selling_qty=='0')

{

	$selling_qty="0";

}

else

{

$selling_qty=$row->selling_qty;

}

return $selling_qty;

}
//incoming product qty for stock report added on 23-09-2019	

function incoming_qty($modelno)

{

$sql ="select count(Prodcut_Qty) as 'Incoming Count', sum(Prodcut_Qty) as 'incoming_qty' from vendor_po_final WHERE Model_No = '$modelno'";

$rs	 = mysqli_query($GLOBALS["___mysqli_ston"],$sql);

$row=mysqli_fetch_object($rs);

if($row->incoming_qty=='' || $row->incoming_qty=='0')

{

	$incoming_qty="0";

}

else

{

$incoming_qty=$row->incoming_qty;

}

return $incoming_qty;

}
//added on 22-10-2019

//incoming qty for tbl_inventory starts

function incoming_inventory_stock($modelno,$month_search=0,$year_search=0,$datevalid_from=0,$datevalid_to=0)

{

if($datevalid_from!='0' && $datevalid_to!='0' && $datevalid_from!='' && $datevalid_to!='')

	{

$date_range_search=" AND (date( date_ordered ) BETWEEN '$datevalid_from' AND '$datevalid_to')";

	}

	if($year_search!='' && $year_search!='0')

	{

	//$orders_status='Pending';

	$year_search_search=" AND YEAR( date_ordered ) IN ( $year_search )";

	}
if($month_search!='' && $month_search!='0')

	{

	//$orders_status='Pending';

	$month_search_search=" AND MONTH( date_ordered ) IN ( $month_search )";

}
$searhRecord =  $year_search_search.$month_search_search.$date_range_search.$searchproid;	

$sql ="	SELECT sum(pro_quantity) as incoming_stock FROM `tbl_inventory` WHERE `pro_model` LIKE '%$modelno%' AND stock_type='Incoming' 

  $searhRecord order by date_ordered desc";

$rs	 = mysqli_query($GLOBALS["___mysqli_ston"],$sql);

$row=mysqli_fetch_object($rs);

{

$sum = $row->incoming_stock;

}

if($sum=='' || $sum=='0')

{

	$incoming_qty="0";

}

else

{
$incoming_qty=$sum;

}

return $incoming_qty;

}
//ends incoming stock 

//outgoing qty for tbl_inventory starts

function outgoing_inventory_stock($modelno,$month_search=0,$year_search=0,$datevalid_from=0,$datevalid_to=0)

{

if($datevalid_from!='0' && $datevalid_to!='0' && $datevalid_from!='' && $datevalid_to!='')

	{

$date_range_search=" AND (date( date_ordered ) BETWEEN '$datevalid_from' AND '$datevalid_to')";

	}
	if($year_search!='' && $year_search!='0')

	{

	//$orders_status='Pending';

	$year_search_search=" AND YEAR( date_ordered ) IN ( $year_search )";

	}
if($month_search!='' && $month_search!='0')

	{

//$orders_status='Pending';

$month_search_search=" AND MONTH( date_ordered ) IN ( $month_search )";

}

$searhRecord =  $year_search_search.$month_search_search.$date_range_search.$searchproid;	

 $sql ="SELECT sum(pro_quantity) as outgoing_stock FROM `tbl_inventory` WHERE TRIM(`pro_model`) =  '$modelno' AND stock_type='outgoing' 

  $searhRecord order by date_ordered desc";

$rs	 = mysqli_query($GLOBALS["___mysqli_ston"],$sql);

$row=mysqli_fetch_object($rs);

{

	$sum = $row->outgoing_stock;

}

if($sum=='' || $sum=='0')

{

	$outgoing_stock="0";

}

else

{

$outgoing_stock=$sum;

}

return $outgoing_stock;

}
//ends outgoing stock 

//order total Starts

function order_amount($pcode)

{

$sqlOrderTotal = "select * from tbl_order where orders_id = '$pcode'";

$rsOrderTotal  =  mysqli_query($GLOBALS["___mysqli_ston"],  $sqlOrderTotal);

if(mysqli_num_rows($rsOrderTotal)>0)

{

$rowOrderTotal 	= mysqli_fetch_object($rsOrderTotal);

$sqlOrderPro = "select pro_id,pro_price,order_pros_id,pro_quantity,GST_percentage,freight_amount from tbl_order_product where order_id ='$pcode' order by order_pros_id asc";

$rsOrderPro	 = mysqli_query($GLOBALS["___mysqli_ston"],  $sqlOrderPro);

$h=0;

$subtotal1=0;

while($rowOrderPro = mysqli_fetch_object($rsOrderPro))

{

$h++;

$OrderProID	 = $rowOrderPro->order_pros_id;

$proID	 	 = $rowOrderPro->pro_id;

$orPrice = $rowOrderPro->pro_price;

number_format($orPrice,2);

$sql_dis					= "SELECT * FROM prowise_discount where orderid='".$pcode."' and proid='".$proID."'";

$sql_dis_dis				= mysqli_query($GLOBALS["___mysqli_ston"],  $sql_dis);

$sql_dis_row				= mysqli_fetch_object($sql_dis_dis);

$subtotal1					= ($orPrice*$rowOrderPro->pro_quantity)-$sql_dis_row->discount_amount;

$subtotal1_dis+=$sql_dis_row->discount_amount;

$discounted_price			= $orPrice*$sql_dis_row->discount_percent/100;			

$per_product_GST_percentage	= $rowOrderPro->GST_percentage;

$discounted_price_tax_amt	= ($orPrice-$discounted_price)*$rowOrderPro->GST_percentage/100;			

$SumProPrice 				= ($orPrice-$discounted_price+$discounted_price_tax_amt)* $rowOrderPro->pro_quantity;//$proPrice * $rowOrderPro->pro_quantity;

$totalTax+= $discounted_price_tax_amt * $rowOrderPro->pro_quantity;//$rowOrderPro->Pro_tax * $rowOrderPro->pro_quantity;

$ProTotalPrice 				= ($discounted_price_tax_amt+$orPrice-$discounted_price)* $rowOrderPro->pro_quantity;//($rowOrderPro->pro_final_price) * $rowOrderPro->pro_quantity ;//remove tax changed by rumit

$freight_amount 			= $rowOrderPro->freight_amount;// * $rowOrderPro->pro_quantity;

$freight_amount_with_gst 	= $rowOrderPro->freight_amount/1.18;// * $rowOrderPro->pro_quantity;

$freight_gst_amount 		= $rowOrderPro->freight_amount-$freight_amount_with_gst;// * $rowOrderPro->pro_quantity;

$totalCost     				= $totalCost + $ProTotalPrice;

}
$GST_tax_amt				= $this->GST_tax_amount_on_offer($pcode);//$rowOrderPro->GST_percentage;				

$TotalOrder	   				= $rowOrderTotal->total_order_cost;

$ship		   				= $rowOrderTotal->shipping_method_cost;

$shippingValue				= $ship;

$taxValue					= $rowOrderTotal->tax_cost;

$tax_included				= $rowOrderTotal->tax_included;

$tax_perc					= $rowOrderTotal->taxes_perc;

$discount_perc				= $rowOrderTotal->discount_perc;

$discount_per_amt			= $rowOrderTotal->discount_per_amt;

$show_discount				=$rowOrderTotal->show_discount;

$subtotal_after_discount    = $TotalOrder - $subtotal1_dis;

if($tax_included=='Excluded')

{

$subtotal_tax       		= $subtotal_after_discount*$tax_perc/100;

}

else

{

$subtotal_tax       		= $subtotal_after_discount;

}

if($tax_included=='Excluded')

{

$GrandTotalOrder			= $subtotal_after_discount + $subtotal_tax;

}

else

{

$GrandTotalOrder			= $subtotal_after_discount + 0;

}

//$subtotal_final		= $subtotal_tax;

$couponDiscount 			= $rowOrderTotal->coupon_discount;

}

number_format($totalCost-$totalTax,2);

//echo $show_discount;

if($show_discount=="Yes") {

 number_format($subtotal1_dis,2);

}

number_format($freight_amount-$freight_gst_amount,2);

$Grand_subtotal1=$subtotal1;

if($tax_included=='Excluded' || $GST_tax_amt>0)

{

if($totalTax>0)

{

number_format($totalTax+$freight_gst_amount,2);//total gst value rumit 

}

else

{

$subtotal_tax1=$subtotal_tax;

number_format($subtotal_tax1,2);

}//echo $subtotal1;

$Grand_subtotal1=$totalCost;//$subtotal1+$subtotal_tax1+$totalTax;

}

return ($totalCost+$freight_amount);

}
//order total ends

//whatsapp api by creating function 19-dec2019

function whatsapp_msg($phoneno,$message)

{

/**************whatsapp starts*/

//echo "whatsapp msg ::".$whatsaap_enq_ids=implode("\r\n",$emailary[$acc_manager]);

$data = [

    'phone' => $phoneno, // Receivers phone

    'body' => $message, // Message

];

$json = json_encode($data); // Encode data to JSON

// URL for request POST /message

$url =        'https://api.chat-api.com/instance85376/sendMessage?token=86wkjz6mc9kyttpp'; //added on 18-dec-2019

// Make a POST request

$options = stream_context_create(['http' => [

        'method'  => 'POST',

        'header'  => 'Content-type: application/json',

        'content' => $json

    ]

]);

// Send a request

$result = file_get_contents($url, false, $options);

//print_r($result);

/****************************************************/

}
function warranty_name($warranty_id)

{

if($warranty_id!='0')

{

$sql = "select warranty_name from tbl_warranty_master where warranty_id = '$warranty_id' and deleteflag = 'active' and warranty_status='active'";

$rs  = mysqli_query($GLOBALS["___mysqli_ston"],$sql);

$row = mysqli_fetch_object($rs);

$warranty_name	  = $row->warranty_name;

}

else

{

	$warranty_name	  = "Nil";

}

return $warranty_name;

}
function calibration_name($calibration_id)

{

if($calibration_id!='0')

{

$sql = "select calibration_name from tbl_calibration_master where calibration_id = '$calibration_id' and deleteflag = 'active' and calibration_status='active'";

$rs  = mysqli_query($GLOBALS["___mysqli_ston"],$sql);

$row = mysqli_fetch_object($rs);

$calibration_name	  = $row->calibration_name;

}

else

{

	$calibration_name	  = "Nil";

}

return $calibration_name;

}
function get_offer_calibration($order_id)

{

$sql = "select offer_calibration from tbl_order where orders_id = '$order_id' and deleteflag = 'active' ";

$rs  = mysqli_query($GLOBALS["___mysqli_ston"],$sql);

$row = mysqli_fetch_object($rs);

$offer_calibration	  = $row->offer_calibration;

return $offer_calibration;

}
function get_offer_pro_calibration($pcode)

{

  	$sql_o_p 	= "select pro_id from tbl_order_product where deleteflag='active' and order_id = '$pcode'";

	$rs_o_p	 	= mysqli_query($GLOBALS["___mysqli_ston"], $sql_o_p); 

	if(mysqli_num_rows($rs_o_p)>0)

	{

		while($row_o_p = mysqli_fetch_object($rs_o_p))

		{

	 	$pro_calibration_id[] 	   = $row_o_p->pro_id;

		}

	}
$pro_calibration_id=$pro_calibration_id;//$this->get_offer_pro_calibration($pcode);

$pro_calibration_ids=implode(",",$pro_calibration_id);

  	$sql_o_p_c 	= "select pro_calibration  from tbl_products where deleteflag='active' and pro_id IN ($pro_calibration_ids) order by pro_calibration desc";

	$rs_o_p_c	 	= mysqli_query($GLOBALS["___mysqli_ston"], $sql_o_p_c); 

	if(mysqli_num_rows($rs_o_p_c)>0)

	{

		$row_o_p_c = mysqli_fetch_object($rs_o_p_c);

		{

	 	$offer_calibration 	   = $row_o_p_c->pro_calibration;

		}

	}

return $offer_calibration;

}
function notification_send_check($order_id,$notification_type,$module_name)

{

$sql = "select notification_id,date_of_sending,comment_by from tbl_notification where order_id = '$order_id' and notification_type='$notification_type' and module_name='$module_name' and deleteflag = 'active' order by notification_id desc ";

$rs  = mysqli_query($GLOBALS["___mysqli_ston"],$sql);

$row = mysqli_fetch_object($rs);

$notification_id	= $row->notification_id;

$date_of_sending	= $row->date_of_sending;

$comment_by	  		= $row->comment_by;

return array("notification_id"=>$notification_id,"date_of_sending"=>$date_of_sending,"comment_by"=>$comment_by);

}
//service offer gst tax funcion added on 12 march 2020

function GST_tax_amount_on_service_offer($orderid)

{

$sqlOrderTotal_GST = "SELECT sum( Pro_tax ) AS GST_amount FROM `tbl_order_service_product` WHERE `service_order_id` = '$orderid' GROUP BY order_id";

$rsOrderTotal_GST  =  mysqli_query($GLOBALS["___mysqli_ston"],  $sqlOrderTotal_GST);

if(mysqli_num_rows($rsOrderTotal_GST)>0)

{

$rowOrderTotal_GST = mysqli_fetch_object($rsOrderTotal_GST);

$gst_amt		   = $rowOrderTotal_GST->GST_amount;

}

return $gst_amt;

}
/////////////////////

function ServiceOrderTotalInfo_offer_letter($pcode)

{

$symbol		= $this->currencySymbol(1);

$currency1 	= $symbol[0];

$curValue 	= $symbol[1];

$sqlOrderTotal = "select * from tbl_order_service where service_orders_id = '$pcode'";

$rsOrderTotal  =  mysqli_query($GLOBALS["___mysqli_ston"],  $sqlOrderTotal);

if(mysqli_num_rows($rsOrderTotal)>0)

{

$rowOrderTotal 	= mysqli_fetch_object($rsOrderTotal);

$sqlOrderPro = "select * from tbl_order_service_product  where service_order_id ='$pcode' order by service_order_pros_id asc";

$rsOrderPro	 = mysqli_query($GLOBALS["___mysqli_ston"],  $sqlOrderPro);

$h=0;

$subtotal1=0;

while($rowOrderPro = mysqli_fetch_object($rsOrderPro))

{

$h++;

$OrderProID	 = $rowOrderPro->order_pros_id;

$proID	 	 = $rowOrderPro->pro_id;

$groupID 	 = $rowOrderPro->group_id;

$ManufacturerID = $rowOrderPro->manufacturers_id;

$orPrice = $rowOrderPro->pro_price;

number_format($orPrice,2);

$sql_dis="SELECT * FROM prowise_discount where orderid='".$pcode."' and proid='".$proID."'";

$sql_dis_dis=mysqli_query($GLOBALS["___mysqli_ston"],  $sql_dis);

$sql_dis_row=mysqli_fetch_object($sql_dis_dis);

$subtotal1=($orPrice*$rowOrderPro->pro_quantity)-$sql_dis_row->discount_amount;

$subtotal1_dis+=$sql_dis_row->discount_amount;

$discounted_price=$orPrice*$sql_dis_row->discount_percent/100;			

$per_product_GST_percentage=$rowOrderPro->GST_percentage;

$discounted_price_tax_amt=($orPrice-$discounted_price)*$rowOrderPro->GST_percentage/100;			

$SumProPrice = ($orPrice-$discounted_price+$discounted_price_tax_amt)* $rowOrderPro->pro_quantity;//$proPrice * $rowOrderPro->pro_quantity;

$totalTax+= $discounted_price_tax_amt * $rowOrderPro->pro_quantity;//$rowOrderPro->Pro_tax * $rowOrderPro->pro_quantity;

$ProTotalPrice = ($discounted_price_tax_amt+$orPrice-$discounted_price)* $rowOrderPro->pro_quantity;//($rowOrderPro->pro_final_price) * $rowOrderPro->pro_quantity ;//remove tax changed by rumit

$freight_amount = $rowOrderPro->freight_amount;// * $rowOrderPro->pro_quantity;

"freight without GST value:".$freight_amount_with_gst = $rowOrderPro->freight_amount/1.18;// * $rowOrderPro->pro_quantity;

"<br>freight GST value:".$freight_gst_amount = $rowOrderPro->freight_amount-$freight_amount_with_gst;// * $rowOrderPro->pro_quantity;

$totalCost     = $totalCost + $ProTotalPrice;

}
$GST_tax_amt				= $this->GST_tax_amount_on_service_offer($pcode);//$rowOrderPro->GST_percentage;				

$TotalOrder	   				= $rowOrderTotal->total_order_cost;

$ship		   				= $rowOrderTotal->shipping_method_cost;

$shippingValue				= $ship;

$taxValue					= $rowOrderTotal->tax_cost;

$tax_included				= $rowOrderTotal->tax_included;

$tax_perc					= $rowOrderTotal->taxes_perc;

$discount_perc				= $rowOrderTotal->discount_perc;

$discount_per_amt			= $rowOrderTotal->discount_per_amt;

$show_discount				=$rowOrderTotal->show_discount;

$subtotal_after_discount    = $TotalOrder - $subtotal1_dis;

if($tax_included=='Excluded')

{

$subtotal_tax       		= $subtotal_after_discount*$tax_perc/100;

}

else

{

$subtotal_tax       		= $subtotal_after_discount;

}

if($tax_included=='Excluded')

{

$GrandTotalOrder			= $subtotal_after_discount + $subtotal_tax;

}

else

{

$GrandTotalOrder			= $subtotal_after_discount + 0;

}

//$subtotal_final		= $subtotal_tax;

$couponDiscount 			= $rowOrderTotal->coupon_discount;

}

echo "<table width='50%' border='0' cellpadding='5' cellspacing='0' >";

echo "<tr class='text'>";

echo "<td width='70%' class='pad tblBorder_invoice_bottom tblBorder_invoice_right tblBorder_invoice_left ' nowrap align='right'><strong>Sub Total :</strong> </td>";

echo "<td width='30%'  align='right' class='tblBorder_invoice_bottom ' nowrap='nowrap'> &nbsp; &nbsp;";

echo str_replace("backoffice","crm",$rowOrderTotal->Price_value)." ";

echo number_format($totalCost-$totalTax,2);

echo "</td>";

echo "</tr>";

//echo $show_discount;

if($show_discount=="Yes") {

echo "<tr class='text' style='display:none'>";

echo "<td  class='pad tblBorder_invoice_bottom tblBorder_invoice_right tblBorder_invoice_left' nowrap align='right'><strong>Discount :</strong></td>";

echo "<td   align='right'  class='tblBorder_invoice_bottom'  > &nbsp; &nbsp;";

echo str_replace("backoffice","crm",$rowOrderTotal->Price_value)." ";

echo number_format($subtotal1_dis,2);

echo "</td>";

echo "</tr>";

}

echo "<tr class='text'>";

echo "<td class='pad tblBorder_invoice_right tblBorder_invoice_bottom tblBorder_invoice_left' nowrap align='right'><strong>Freight Value :</strong></td>";

echo "<td align='right'  class='tblBorder_invoice_bottom' nowrap='nowrap' >&nbsp; &nbsp; ";

echo str_replace("backoffice","crm",$rowOrderTotal->Price_value)." ";

echo number_format($freight_amount-$freight_gst_amount,2);

echo "</td>";

echo "</tr>";

$Grand_subtotal1=$subtotal1;

if($tax_included=='Excluded' || $GST_tax_amt>0)

{

//	echo "</tr>";

echo "<tr class='text'>";

if($GST_tax_amt>0)

{

echo "<td class='pad tblBorder_invoice_bottom tblBorder_invoice_right tblBorder_invoice_left' nowrap align='right'><strong>Add IGST :</strong></td>";

}

else

{

echo "<td class='pad tblBorder_invoice_bottom tblBorder_invoice_right tblBorder_invoice_left' nowrap align='right'><strong>Add VAT/CST@ $tax_perc% :</strong></td>";

}

echo "<td align='right'  class='tblBorder_invoice_bottom'  > &nbsp; &nbsp; ";

if($totalTax>0)

{

echo str_replace("backoffice","crm",$rowOrderTotal->Price_value)." ";

echo number_format($totalTax+$freight_gst_amount,2);//total gst value rumit 

}

else

{

echo str_replace("backoffice","crm",$rowOrderTotal->Price_value)." ";

$subtotal_tax1=$subtotal_tax;

echo number_format($subtotal_tax1,2);

}//echo $subtotal1;

$Grand_subtotal1=$totalCost;//$subtotal1+$subtotal_tax1+$totalTax;

echo "</td>";

echo "</tr>";

}

echo "<tr class='text'>";

echo "<td class='pad tblBorder_invoice_right tblBorder_invoice_left' nowrap align='right'><h4>Grand Total :</h4></td>";

echo "<td align='right'  nowrap='nowrap' ><h4> &nbsp; &nbsp; ";

echo str_replace("backoffice","crm",$rowOrderTotal->Price_value)." ";

echo $total_price_value=number_format($totalCost+$freight_amount,2);

echo "</h4></td>";

echo "</tr>";

echo "</table>";
$ArrayData['total_order_service_cost'] = $totalCost+$freight_amount;

$this->editRecord('tbl_order_service',$ArrayData,'service_orders_id',$pcode);
}

/////////////////////
///////////////////////////////////////////////////////////////////////////////

//added on 12-march-2020 for service orders

function ServiceOrderItemsInfo_invoice1($pcode)

{

$sql_dis_currency="SELECT Price_value FROM tbl_order_service where service_orders_id='".$pcode."'";

$sql_dis_dis_currency=mysqli_query($GLOBALS["___mysqli_ston"],  $sql_dis_currency);

$sql_dis_row_currency=mysqli_fetch_object($sql_dis_dis_currency);

$Offer_Currency=$sql_dis_row_currency->Price_value;

$Offer_Currency=str_replace("backoffice","crm",$Offer_Currency);

$symbol		= $this->currencySymbol(1);

$currency1 	= $symbol[0];

$curValue 	= $symbol[1];

$totalCost  = 0;

$sql_dis="SELECT show_discount FROM prowise_discount_service where serviceorderid='".$pcode."'";

$sql_dis_dis=mysqli_query($GLOBALS["___mysqli_ston"],  $sql_dis);

$sql_dis_row=mysqli_fetch_object($sql_dis_dis);

echo "<table width='100%' border='0' cellpadding='5' cellspacing='0' class='tblBorder_invoice_top tblBorder_invoice_right   tblBorder_invoice_left'>";

echo "<tr class='head'>";

echo "<td class='pad tblBorder_invoice_bottom tblBorder_invoice_right' width='2%' nowrap='nowrap'   >S.No</td>";

echo "<td width='10%' class='pad tblBorder_invoice_bottom tblBorder_invoice_right' >Product Name</td>";

echo "<td width='10%' class='pad tblBorder_invoice_bottom tblBorder_invoice_right' >HSN Code</td>";

echo "<td width='10%' class='pad tblBorder_invoice_bottom tblBorder_invoice_right' >Part No.</td>";

echo "<td width='20%' class='pad tblBorder_invoice_bottom tblBorder_invoice_right' >Description</td>";

echo "<td width='5%' class='tblBorder_invoice_bottom tblBorder_invoice_right' align='center'>Qty<br>A</td>

<td width='10%' class='tblBorder_invoice_bottom tblBorder_invoice_right' nowrap='nowrap' align='center' ><strong>Unit Price<br>B</strong></td>";

//echo "SHOW dis:--".$sql_dis_row->show_discount;

if($sql_dis_row->show_discount=="Yes") {

echo "<td width='10%' class='tblBorder_invoice_bottom tblBorder_invoice_right' nowrap='nowrap' align='center'>Discount(-)<br>C</td>";

$dis_td=1;

}

else

{

}

echo "<td width='10%' class='pad tblBorder_invoice_bottom tblBorder_invoice_right' align='center'>Add IGST Value (%)<br>D</td>";

if($sql_dis_row->show_discount=="Yes") {

echo "<td nowrap='nowrap' width='10%' class='tblBorder_invoice_bottom ' nowrap='nowrap' align='center'><strong>Sub Total</strong><br>=A*(B-C)</td></tr>";

}

else

{

echo "<td nowrap='nowrap' width='10%' class='tblBorder_invoice_bottom ' nowrap='nowrap' align='center'><strong>Sub Total</strong><br>=A*B</td></tr>";

}

//updated on 23-10-2019

//echo $sqlOrderProa = "select * from tbl_order_service_product where service_order_id ='$pcode' and pro_sort>0";

$sqlOrderProa 	= "select * from tbl_order_service_product where service_order_id ='$pcode' ";

$rsOrderProa	= mysqli_query($GLOBALS["___mysqli_ston"],$sqlOrderProa);

$numpro_sort	= mysqli_num_rows($rsOrderProa); //exit;

if($numpro_sort>0)

{

$sqlOrderPro = "select * from tbl_order_service_product where service_order_id ='$pcode' order by pro_sort asc";

/*}

else

{

$sqlOrderPro = "select * from tbl_order_service_product where service_order_id ='$pcode' order by order_pros_id desc ";

}*/

//echo $sqlOrderPro ;

$rsOrderPro	 = mysqli_query($GLOBALS["___mysqli_ston"],  $sqlOrderPro);

$h=0;

while($rowOrderPro = mysqli_fetch_object($rsOrderPro))

{

$h++;

echo "<tr class='text'>";

echo "<td class='pad tblBorder_invoice_bottom tblBorder_invoice_right' >".$h."</td>";

echo "<td class='pad tblBorder_invoice_bottom tblBorder_invoice_right' valign='middle'><strong>".$rowOrderPro->pro_name."</strong><br />";

$OrderProID	 = $rowOrderPro->order_pros_id;

$proID	 	 = $rowOrderPro->pro_id;

$groupID 	 = $rowOrderPro->group_id;

$hsn_code	 = $rowOrderPro->hsn_code;

if($hsn_code=='')

{

$hsn_code="N/A";

}

echo "</td>";

echo "<td valign='middle' class='tblBorder_invoice_bottom tblBorder_invoice_right'> $hsn_code </td>";

echo "<td valign='middle' class='tblBorder_invoice_bottom tblBorder_invoice_right'> $rowOrderPro->pro_model </td>";

echo "<td valign='middle' class='tblBorder_invoice_bottom tblBorder_invoice_right'>".str_replace("ï¿½"," ",$this->proidentrydesc($rowOrderPro->proidentry))."</td>";

echo "<td valign='middle' class='tblBorder_invoice_bottom tblBorder_invoice_right'> $rowOrderPro->pro_quantity </td>";

$ManufacturerID = $rowOrderPro->manufacturers_id;

/*			$sqlManuF		= "select * from tbl_manufacturers where manufacturers_id = $ManufacturerID";

$rsManuF		= mysqli_query($GLOBALS["___mysqli_ston"],  $sqlManuF);

if(mysqli_num_rows($rsManuF)>0)

{

$rowManuF  = mysqli_fetch_object($rsManuF);

$ManuFname = $rowManuF->manufacturers_name;

}*/

$orPrice = $rowOrderPro->pro_price;

echo "<td valign='middle' class='tblBorder_invoice_bottom tblBorder_invoice_right' nowrap='nowrap'> ";

echo $Offer_Currency." ";

echo $per_product_price=number_format($orPrice,2);

echo "</td>";

$sql_dis="SELECT * FROM prowise_discount_service where serviceorderid='".$pcode."' and proid='".$proID."'";

$sql_dis_dis=mysqli_query($GLOBALS["___mysqli_ston"],  $sql_dis);

$sql_dis_row=mysqli_fetch_object($sql_dis_dis);

if($sql_dis_row->show_discount=="Yes" || $dis_td) {

echo "<td valign='middle' class='tblBorder_invoice_bottom tblBorder_invoice_right' nowrap='nowrap'>";

echo $Offer_Currency." "; 

//	echo "ABC:=". number_format($sql_dis_row->discount_amount,2);

echo "".			$discounted_price=$orPrice*$sql_dis_row->discount_percent/100;			

echo "<br />";

echo " (".$sql_dis_row->discount_percent."%)";

echo "</td>";

}

else

{

}

echo "<td valign='middle' class='tblBorder_invoice_bottom tblBorder_invoice_right' nowrap='nowrap'> ";

echo $Offer_Currency." ";

echo "".$discounted_price_tax_amt=($orPrice-$discounted_price)*$rowOrderPro->GST_percentage/100;			

echo "<br>(".$per_product_GST_percentage=str_replace("%","",$rowOrderPro->GST_percentage)."%";

echo ")</td>";

$SumProPrice = ($orPrice-$discounted_price+$discounted_price_tax_amt)* $rowOrderPro->pro_quantity;//$proPrice * $rowOrderPro->pro_quantity;

$totalTax = $discounted_price_tax_amt * $rowOrderPro->pro_quantity;//$rowOrderPro->Pro_tax * $rowOrderPro->pro_quantity;

$ProTotalPrice = ($orPrice-$discounted_price)* $rowOrderPro->pro_quantity;//($rowOrderPro->pro_final_price) * $rowOrderPro->pro_quantity ;//remove tax changed by rumit

$totalCost     = $totalCost + $ProTotalPrice ;

echo "<td valign='middle' class='tblBorder_invoice_bottom ' nowrap='nowrap'>";

echo $Offer_Currency." "; 

echo number_format($ProTotalPrice,2);

echo "</td>";

echo "</tr>";

}

}

echo "</table>";

}

/*********ends*********************/
function ServiceOrderItemsInfo_invoice1_edit($pcode)

{

$sql_dis_currency="SELECT Price_value FROM tbl_order_service where service_orders_id='".$pcode."'";

$sql_dis_dis_currency=mysqli_query($GLOBALS["___mysqli_ston"],  $sql_dis_currency);

$sql_dis_row_currency=mysqli_fetch_object($sql_dis_dis_currency);

$Offer_Currency=$sql_dis_row_currency->Price_value;

$Offer_Currency=str_replace("backoffice","crm",$Offer_Currency);

$symbol		= $this->currencySymbol(1);

$currency1 	= $symbol[0];

$curValue 	= $symbol[1];

$totalCost  = 0;

echo "<table width='100%' border='0' cellpadding='5' cellspacing='0' class='tblBorder_invoice_right   tblBorder_invoice_left'>";

//echo "<tr class='pagehead'><td colspan='11' class='pad'>Item(s) Information </td></tr>";

echo "<tr class='head'>";

echo "<td  class='tblBorder_invoice_bottom tblBorder_invoice_right'><input type='submit' name='delete' value='Delete' class='inputton' style='margin-left:0px; padding:2px; font-size:11px;margin-bottom: 0px;'></td>";

echo "<td class='pad tblBorder_invoice_bottom tblBorder_invoice_right' width='2%' nowrap='nowrap'   >S.No</td>";

echo "<td width='10%' class='pad tblBorder_invoice_bottom tblBorder_invoice_right' >Product Name</td>";

echo "<td width='10%' class='pad tblBorder_invoice_bottom tblBorder_invoice_right' >HSN Code</td>";

echo "<td width='10%' class='pad tblBorder_invoice_bottom tblBorder_invoice_right' >Part No.</td>";

echo "<td width='20%' class='pad tblBorder_invoice_bottom tblBorder_invoice_right' >Description</td>";

echo "<td width='5%' class='tblBorder_invoice_bottom tblBorder_invoice_right'>Qty<br>A</td>

<td width='2%' class='tblBorder_invoice_bottom tblBorder_invoice_right'>Display Order</td>

<td width='10%' class='tblBorder_invoice_bottom tblBorder_invoice_right' nowrap='nowrap'><strong>Unit Price</strong><br>B</td>";

$sql_dis="SELECT show_discount FROM prowise_discount_service where serviceorderid='".$pcode."'";

$sql_dis_dis=mysqli_query($GLOBALS["___mysqli_ston"],  $sql_dis);

$sql_dis_row=mysqli_fetch_object($sql_dis_dis);

//if($sql_dis_row->show_discount=="Yes") {

echo "<td width='10%' class='tblBorder_invoice_bottom tblBorder_invoice_right' nowrap='nowrap'>Discount(-)<br>C</td>";

echo "<td width='10%' class='pad tblBorder_invoice_bottom tblBorder_invoice_right' >Add IGST Value(%)<br>D</td>";

echo "<td nowrap='nowrap' width='10%' class='tblBorder_invoice_bottom ' nowrap='nowrap'><strong>Sub Total</strong><br>=A*(B-C)</td></tr>";

//updated on23-10-2019

//echo $sqlOrderProa = "select * from tbl_order_service_product where service_order_id ='$pcode' and pro_sort>0";

$sqlOrderProa = "select * from tbl_order_service_product where service_order_id ='$pcode'";

$rsOrderProa	 = mysqli_query($GLOBALS["___mysqli_ston"],$sqlOrderProa);

$numpro_sort=mysqli_num_rows($rsOrderProa); //exit;

if($numpro_sort>0)

{

$sqlOrderPro = "select * from tbl_order_service_product where service_order_id ='$pcode' order by pro_sort asc";

}

else

{

$sqlOrderPro = "select * from tbl_order_service_product where service_order_id ='$pcode' order by service_order_pros_id desc ";

}

//echo $sqlOrderPro ;

//$sqlOrderPro = "select * from tbl_order_product where order_id ='$pcode' order by order_pros_id desc ";

$rsOrderPro	 = mysqli_query($GLOBALS["___mysqli_ston"],  $sqlOrderPro);

$h=0;

while($rowOrderPro = mysqli_fetch_object($rsOrderPro))

{

$h++;

echo "<tr class='text'>";

?>
<td class='pad tblBorder_invoice_bottom tblBorder_invoice_right'><input type="checkbox" value="<?php echo $rowOrderPro->pro_id;?>" name="pro_del[]" /></td>
<?php

echo "<td class='pad tblBorder_invoice_bottom tblBorder_invoice_right' >".$h."</td>";

echo "<td class='pad tblBorder_invoice_bottom tblBorder_invoice_right' valign='middle'><strong>".$rowOrderPro->pro_name."</strong><br />";

$OrderProID	 = $rowOrderPro->order_pros_id;

$proID	 	 = $rowOrderPro->pro_id;

$groupID 	 = $rowOrderPro->group_id;

$hsn_code	 = $rowOrderPro->hsn_code;

if($hsn_code=='')

{

$hsn_code="N/A";

}

echo "</td>";

echo "<td valign='middle' class='tblBorder_invoice_bottom tblBorder_invoice_right'> $hsn_code </td>";

echo "<td valign='middle' class='tblBorder_invoice_bottom tblBorder_invoice_right'> $rowOrderPro->pro_model </td>";

echo "<td valign='middle' class='tblBorder_invoice_bottom tblBorder_invoice_right'>".str_replace("ï¿½"," ",$this->proidentrydesc($rowOrderPro->proidentry))."</td>";

echo "<td valign='middle' class='tblBorder_invoice_bottom tblBorder_invoice_right'>";

echo '<input type="text" name="pro_qty[]" value="'. $rowOrderPro->pro_quantity.'" size="4"  class="form-control">';

echo "</td>";

echo "<td valign='middle' class='tblBorder_invoice_bottom tblBorder_invoice_right' align='center'>";

echo '<input type="text" name="pro_sort[]" value="'. $rowOrderPro->pro_sort.'" size="2"   class="form-control">';//added on 23-10-2019

echo "</td>";

$orPrice = $rowOrderPro->pro_price;

echo "<td valign='middle' class='tblBorder_invoice_bottom tblBorder_invoice_right' nowrap='nowrap'> ";

echo $Offer_Currency." ";

echo $per_product_price=number_format($orPrice,2);

echo "</td>";

$sql_dis="SELECT * FROM prowise_discount_service where serviceorderid='".$pcode."' and proid='".$proID."'";

$sql_dis_dis=mysqli_query($GLOBALS["___mysqli_ston"],  $sql_dis);

$sql_dis_row=mysqli_fetch_object($sql_dis_dis);

//if($sql_dis_row->show_discount=="Yes" || $dis_td) {

echo "<td valign='middle' class='tblBorder_invoice_bottom tblBorder_invoice_right' nowrap='nowrap'>";

echo $Offer_Currency." "; 

echo number_format($sql_dis_row->discount_amount,2);

echo "<br />";

$discounted_price=$orPrice*$sql_dis_row->discount_percent/100;			

//$sql_dis_row->discount_percent;

echo '<input type="hidden" name="pro_price[]" value="'. $rowOrderPro->pro_price * $rowOrderPro->pro_quantity.'" >';

echo '<input type="hidden" name="pro_id[]" value="'. $rowOrderPro->pro_id.'" >';

echo '<input type="text" name="discount_percent[]" value="'. $sql_dis_row->discount_percent.'" size="4" class="form-control">%';

echo "</td>";

echo "<td valign='middle' class='tblBorder_invoice_bottom tblBorder_invoice_right' nowrap='nowrap'> ";

echo $Offer_Currency." ";

//	echo $per_product_tax=$rowOrderPro->Pro_tax;

echo "".			$discounted_price_tax_amt=($orPrice-$discounted_price)*$rowOrderPro->GST_percentage/100;			

echo "<br>(".$per_product_GST_percentage=$rowOrderPro->GST_percentage;

echo ")</td>";

$SumProPrice = ($orPrice-$discounted_price+$discounted_price_tax_amt)* $rowOrderPro->pro_quantity;//$proPrice * $rowOrderPro->pro_quantity;

$totalTax = $discounted_price_tax_amt * $rowOrderPro->pro_quantity;//$rowOrderPro->Pro_tax * $rowOrderPro->pro_quantity;

$ProTotalPrice = ($discounted_price_tax_amt+$orPrice-$discounted_price)* $rowOrderPro->pro_quantity;//($rowOrderPro->pro_final_price) * $rowOrderPro->pro_quantity ;//remove tax changed by rumit

$totalCost     = $totalCost + $ProTotalPrice ;

echo "<td valign='middle' class='tblBorder_invoice_bottom ' nowrap='nowrap'>";

echo $Offer_Currency." "; 

echo number_format($ProTotalPrice,2);

echo "</td>";

echo "</tr>";

}
echo "</table>";

}/////////////service order order page info

function service_order_billing_address($pcode)
{
echo "<table width='100%' border='0' cellpadding='0' cellspacing='0' class='table table-striped table-bordered'>";
echo "<tr class='pagehead'><td colspan='2' class='pad'>Billing Address</td></tr>";
//echo "<tr> <td colspan='2' height='2'></td></tr>";
$sql = "select customers_contact_no,customers_id,billing_name, billing_company, billing_street_address, billing_city, billing_zip_code, billing_state, billing_country_name, billing_telephone_no, billing_fax_no from tbl_order_service where deleteflag='active' and service_orders_id='$pcode'";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql); 
if(mysqli_num_rows($rs)>0)

{
$row = mysqli_fetch_object($rs);

$customers_id=$row->customers_id;

$company_name=$this->company_name($row->customers_id);

$customer_GST_no=$this->cutomer_GSTno($pcode);

/*if($row->billing_company!='')

{

echo "<tr class='text'><td width='30%' class='pad' valign='top'>Company Name</td><td width='70%' valign='top'>: &nbsp;$row->billing_company</td></tr>";
}*/

echo "<tr class='text'><th width='30%' class='pad' valign='top'>Company Name</th><td width='70%' valign='top'>$company_name </td></tr>";

echo "<tr class='text'><th width='30%' class='pad' valign='top'>Company GST</th><td width='70%' valign='top'>$customer_GST_no </td></tr>";

echo "<tr class='text'><th width='30%' class='pad' valign='top'>Buyer/ Customer Name</th><td width='70%' valign='top'>$row->billing_name </td></tr>";

echo "<tr class='text'><th class='pad' valign='top' valign='top'>Address</th><td valign='top'>".nl2br($row->billing_street_address)."</td></tr>";

echo "<tr class='text'><th class='pad' valign='top'>City</th><td valign='top'>$row->billing_city</td></tr>";

echo "<tr class='text'><th class='pad' valign='top'>State/Province</th><td valign='top'>".$this->StateName($row->billing_state)."</td></tr>";

echo "<tr class='text'><th class='pad' valign='top'>Country</th><td valign='top'>".$this->CountryName($row->billing_country_name)."</td>";

echo "<tr class='text'><th class='pad' valign='top'>Zip/Postal Code</th><td valign='top'>$row->billing_zip_code</td></tr>";

if($row->billing_telephone_no!='')

{

echo "<tr class='text'><th class='pad' valign='top'>Telephone No.</th><td valign='top'>$row->billing_telephone_no</td></tr>";

}

echo "<tr class='text'> <th class='pad' valign='top'>Contact No.</th><td valign='top'>$row->customers_contact_no</td></tr>";

/*if($row->billing_fax_no!='')

{

echo "<tr class='text'><td class='pad' valign='top'>Fax No.</td><td valign='top'>: &nbsp;$row->billing_fax_no</td></tr>";		

}*/

}

else

{

echo "<tr class='text'><td colspan='2' class='redstar'> &nbsp; No record present in database</td></tr>";

}

echo "</table>";

}
// This function shows shipping address

function service_order_shipping_address($pcode)

{

echo "<table width='100%' border='0' cellpadding='0' cellspacing='0' class='tblBorder'>";

echo "<tr class='pagehead'><td colspan='2' class='pad'>Shipping Address</td></tr>";

$sql = "select shipping_company, shipping_name, shipping_street_address, shipping_city, shipping_zip_code, shipping_state, shipping_country_name, shipping_telephone_no, shipping_fax_no from tbl_order_service where deleteflag='active' and service_orders_id='$pcode'";

$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql); 

if(mysqli_num_rows($rs)>0)

{

$row = mysqli_fetch_object($rs);	

/*	if($row->shipping_company!='')

{

echo "<tr class='text'><td width='30%' class='pad'>Company Name</td><td width='70%'>: &nbsp;$row->shipping_company</td></tr>";

}*/

echo "<tr class='text'><td width='30%' class='pad'>Name</td><td width='70%'>: &nbsp;$row->shipping_name </td></tr>";

echo "<tr class='text'><td class='pad' valign='top'>Street Address</td><td valign='top'>: &nbsp;".nl2br($row->shipping_street_address)."</td></tr>";

echo "<tr class='text'><td class='pad'>City</td><td>: &nbsp;$row->shipping_city</td></tr>";

echo "<tr class='text'><td class='pad'>State/Province</td><td>: &nbsp;".$this->StateName($row->shipping_state)."</td></tr>";

echo "<tr class='text'><td class='pad'>Country</td><td>: &nbsp;".$this->CountryName($row->shipping_country_name)."</td></tr>";

echo "<tr class='text'><td class='pad'>Zip/Postal Code</td><td>: &nbsp;$row->shipping_zip_code</td></tr>";

echo "<tr class='text'><td class='pad'>Telephone No.</td><td>: &nbsp;$row->shipping_telephone_no</td></tr>";

if($row->billing_fax_no!='')

{

echo "<tr class='text'><td class='pad'>Fax No.</td><td >: &nbsp;</td><td>$row->shipping_fax_no</td></tr>";

}

}

else

{

echo "<tr class='text'><td colspan='2' class='redstar'> &nbsp; No record present in database</td></tr>";

}

echo "</table>";

}function service_PO_date_delivery_order($orderid)

{

	   

$sql = "select PO_Date from tbl_delivery_order_service where service_order_id = '$orderid' ";

$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);

$row = mysqli_fetch_object($rs);

//$PO_Date	  = $this->getDateformate($row->PO_Date,'ymd','mdy','-');;//commented by rumit for date format change on 30-07-2019

$PO_Date	  = $row->PO_Date;

//return $this->getDateformate($row_product_vendor_price->Date,'ymd','mdy','-');

return $PO_Date;
}function Service_PO_no($orderid)

{

	   

$sql = "select PO_NO from tbl_delivery_order_service where  DO_Status='active' and service_order_id = '$orderid' ";

$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);

$row = mysqli_fetch_object($rs);

$PO_NO	  = $row->PO_NO;

return $PO_NO;
}
function service_tax_per($orderid)

{

	   

$sql = "select Tax_Per from tbl_delivery_order_service where service_order_id = '$orderid' ";

$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);

$row = mysqli_fetch_object($rs);

$Tax_Per	  = $row->Tax_Per;

return $Tax_Per;

}function service_order_po_info($pcode)

{

echo "<table width='100%' border='0' cellpadding='0' cellspacing='0' class='table table-striped table-bordered'>";

echo "<tr class='pagehead'><td colspan='2' class='pad'>PO details</td></tr>";

//echo "<tr> <td colspan='2' height='2'></td></tr>";

$sql = "select offercode, time_order_serviceed, customers_contact_no,customers_id,billing_name, billing_company, billing_street_address, billing_city, billing_zip_code, billing_state, billing_country_name, billing_telephone_no, billing_fax_no from tbl_order_service where deleteflag='active' and service_orders_id='$pcode'";

$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql); 

if(mysqli_num_rows($rs)>0)

{

$row 				= mysqli_fetch_object($rs);

$customers_id		= $row->customers_id;

$offercode			= $row->offercode;

$company_name		= $this->company_name($row->customers_id);

$customer_GST_no	= $this->cutomer_GSTno($pcode);

$tax_percentage		= $this->service_tax_per($pcode);

$po_number			= $this->Service_PO_no($pcode);

$po_date			= $this->date_format_india($this->service_PO_date_delivery_order($pcode));
/*if($row->billing_company!='')

{

echo "<tr class='text'><td width='30%' class='pad' valign='top'>Company Name</td><td width='70%' valign='top'>: &nbsp;$row->billing_company</td></tr>";
}*/
echo "<tr class='text'><th width='30%' class='pad' valign='top'>Offer No</th><td width='70%' valign='top'>&nbsp;$offercode-$pcode </td></tr>";

echo "<tr class='text'><th width='30%' class='pad' valign='top'>Date </th><td width='70%' valign='top'>&nbsp;".$this->date_format_india($row->time_order_serviceed)." </td></tr>";

echo "<tr class='text'><th width='30%' class='pad' valign='top'>TAX %</th><td width='70%' valign='top'>&nbsp;$tax_percentage%</td></tr>";

echo "<tr class='text'><th class='pad' valign='top' valign='top'>PO No.</th><td valign='top'>&nbsp;".$po_number."</td></tr>";

echo "<tr class='text'><th class='pad' valign='top'>PO Date</th><td valign='top'>&nbsp;".$po_date."</td></tr>";

/*if($row->billing_fax_no!='')

{

echo "<tr class='text'><td class='pad' valign='top'>Fax No.</td><td valign='top'>: &nbsp;$row->billing_fax_no</td></tr>";		

}*/

}

else

{

echo "<tr class='text'><td colspan='2' class='redstar'> &nbsp; No record present in database</td></tr>";

}

echo "</table>";

}

/*ends*///get service offer id from previos order's order_id 

function get_service_offer_id($orderid)

{   

$sqlState = "select service_orders_id from tbl_order_service where order_id = '$orderid' and deleteflag = 'active'";

$rsState  = mysqli_query($GLOBALS["___mysqli_ston"],  $sqlState);

$rowState = mysqli_fetch_object($rsState);

$service_offer_id	  = $rowState->service_orders_id;

return $service_offer_id;

}
function get_contract_end_date($service_order_id)

{   

$sqlState 			= "select PO_Due_Date from tbl_delivery_order_service where service_order_id = '$service_order_id' ";

$rsState  			= mysqli_query($GLOBALS["___mysqli_ston"],  $sqlState);

$rowState 			= mysqli_fetch_object($rsState);

$contract_end_date	= $rowState->PO_Due_Date;

if($contract_end_date=='' || $contract_end_date='0')

{

	$contract_end_date="N/A";

}

else

{

	$contract_end_date	= $this->date_format_india($rowState->PO_Due_Date);

}
return $contract_end_date;

}
function get_service_type($service_order_id)

{   

$sqlState 			= "select service_type from tbl_delivery_order_service where service_order_id = '$service_order_id' ";

$rsState  			= mysqli_query($GLOBALS["___mysqli_ston"],  $sqlState);

$rowState 			= mysqli_fetch_object($rsState);

$service_type		= str_replace("_"," ",$rowState->service_type);
return $service_type;

}
function service_payment_terms_name($ref_source)

{
$sql = "select service_order_payment_terms_name from tbl_service_order_payment_terms_master where service_order_payment_terms_id = '$ref_source' and deleteflag = 'active' and service_order_payment_terms_status='active'";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);

$row = mysqli_fetch_object($rs);

$service_payment_terms_name	  = $row->service_order_payment_terms_name;

/*	if($service_order_payment_terms_name=='')

{

$service_order_payment_terms_name="N/A";

}*/

return $service_payment_terms_name;

}//created on 28march2020




function supply_payment_terms_name($ref_source)

{
$sql = "select supply_order_payment_terms_name from tbl_supply_order_payment_terms_master where supply_order_payment_terms_id = '$ref_source' and deleteflag = 'active' and supply_order_payment_terms_status='active'";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);

$row = mysqli_fetch_object($rs);

$supply_payment_terms_name	  = $row->supply_order_payment_terms_name;

/*	if($supply_order_payment_terms_name=='')

{

$supply_order_payment_terms_name="N/A";

}*/

return $supply_payment_terms_name;

}//created on 16Sept2021



function servicecutomer_GSTno($serviceorderid)

{

	   

$sql = "select Buyer_CST from tbl_delivery_order_service where service_order_id = '$serviceorderid' ";

$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);

$row = mysqli_fetch_object($rs);

$Buyer_CST	  = $row->Buyer_CST;

return $Buyer_CST;

}function buyer_contact_person_service($service_order_id)

{

	   

$sql = "select Buyer_Name from tbl_delivery_order_service where service_order_id = '$service_order_id' ";

$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);

$row = mysqli_fetch_object($rs);

$buyer_contact_person	  = $row->Buyer_Name;
return $buyer_contact_person;

}
function buyer_mobile_service($service_order_id)

{

	   

$sql = "select Buyer_Mobile from tbl_delivery_order_service where service_order_id = '$service_order_id' ";

$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);

$row = mysqli_fetch_object($rs);

$Buyer_Mobile	  = $row->Buyer_Mobile;

return $Buyer_Mobile;

}function buyer_email_service($service_order_id)

{

	   

$sql = "select Buyer_Email from tbl_delivery_order_service where service_order_id = '$service_order_id' ";

$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);

$row = mysqli_fetch_object($rs);

$Buyer_Email	  = $row->Buyer_Email;

return $Buyer_Email;

}function Con_Email_service($service_order_id)

{

	   

$sql = "select Con_Email from tbl_delivery_order_service where service_order_id = '$service_order_id' ";

$rs  = mysqli_query($GLOBALS["___mysqli_ston"],$sql);

$row = mysqli_fetch_object($rs);

$Con_Email	  = $row->Con_Email;

if($Con_Email=='')

{

	$Con_Email="N/a";

}

return $Con_Email;

}
function consignee_mobile_service($service_order_id)

{

	   

$sql = "select Con_Mobile from tbl_delivery_order_service where service_order_id = '$service_order_id' ";

$rs  = mysqli_query($GLOBALS["___mysqli_ston"],$sql);

$row = mysqli_fetch_object($rs);

$Con_Mobile	  = $row->Con_Mobile;

if($Con_Mobile=='')

{

	$Con_Mobile="N/a";

}

return $Con_Mobile;

}
//added on 31-mar-2020

function buyer_address_service($service_order_id)

{

$sql = "select Cus_Com_name,Buyer,Buyer_Name,Buyer_Email,Buyer_Mobile,Buyer_CST from tbl_delivery_order_service where service_order_id = '$service_order_id'  ";

$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);

$row = mysqli_fetch_object($rs);

$Buyer= $row->Cus_Com_name.' 

'.$row->Buyer.'

Name: '.$row->Buyer_Name.'

Mobile: '.$row->Buyer_Mobile.'

Email: '.$row->Buyer_Email.'

GST No: '.$row->Buyer_CST;

return  ($Buyer);

}
function consignee_address_service($service_order_id)

{

$sql = "select Con_Com_name,Consignee,Con_Name,Con_Mobile,Con_Email,Con_CST from tbl_delivery_order_service where service_order_id = '$service_order_id'  ";

$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);

$row = mysqli_fetch_object($rs);

$consignee= $row->Con_Com_name.' 

'.$row->Consignee.'

Name: '.$row->Con_Name.'

Mobile: '.$row->Con_Mobile.'

Email: '.$row->Con_Email.'

GST No: '.$row->Con_CST;

return  ($consignee);

}
///////////////////////////////////  Start Service Delivery Challan  //////////////////////////////////////////////
function OrderItemsInfo_Delivery_Challan_Service($service_orders_id)

{

$symbol		= $this->currencySymbol(1);

$currency1 	= $symbol[0];

$curValue 	= $symbol[1];

$totalCost  = 0;

echo "<table width='100%' border='0' cellpadding='5' cellspacing='2' class='bor_opt' style='border-collapse:collapse;'>";

//echo "<tr class='pagehead'><td colspan='11' class='pad'>Item(s) Information </td></tr>";

echo "<tr class='head'>";

echo "<td width='5%' align='center'>SR.No.</td>";

//echo "<td width='15%' '>Item Code</td>";

echo "<td >Product Description </td>";//added by rumit on 22july 2019 

//echo "<td width='15%' >Product Serial No. </td>";

echo "<td width='5%'>Qty.</td>";

echo "<td width='5%'>UOM</td>";

echo "<td width='10%' class='tblBorder_invoice_bottom'>Remarks</td>

</tr>";

//		echo "<tr><td height='1px' colspan='11' class='tblBorder_invoice_bottom'>hf</td></tr>"; //blank line

$sqlOrderPro = "select * from tbl_order_service_product where service_order_id ='$service_orders_id'  order by service_order_pros_id asc";

$rsOrderPro	 = mysqli_query($GLOBALS["___mysqli_ston"],  $sqlOrderPro);

$h=0;

while($rowOrderPro = mysqli_fetch_object($rsOrderPro))

{

$h++;

echo "<tr class='text'>";

echo "<td align='center'>".$h."</td>";

//echo "<td valign='top' >";

$OrderProID	 = $rowOrderPro->service_order_pros_id;

$proID	 	 = $rowOrderPro->pro_id;

$groupID 	 = $rowOrderPro->group_id;

$sqlAttr = "select * from tbl_group where group_id='$groupID'";

$rsAttr  = mysqli_query($GLOBALS["___mysqli_ston"],  $sqlAttr);
$i  	 = -1;
$k  	 = 0;
$cb 	 = 0;
$tx		 = 0;
$ta		 = 0;
if($cb != 0)
{
}
if($tx!=0)
{
}
if($ta != 0)
{
}
//echo "<span class='prdname red required '><strong>".$this->pro_text_ordered($proID,$service_orders_id)."</strong></span></td>";		  
//echo "<br>Product S. No.: ".$rowOrderPro->barcode."</td>";
echo "<td valign='top' >".$this->customer_pro_desc_title_service($rowOrderPro->pro_model,$service_orders_id).'<br><b>Item Code:</b>'.$rowOrderPro->pro_model.'<br><b>Product S. No.: </b>'.$rowOrderPro->barcode." </td>";

//echo "<td valign='top'>".$rowOrderPro->barcode." </td>";

echo "<td valign='top'> $rowOrderPro->pro_quantity </td>";
echo "<td valign='top'> No.</td>";
echo "<td valign='top'> No Remark</td>";
echo "</tr>";
//			echo "<tr bgcolor='#F6F6F6'><td colspan='11' height='2'></td></tr>";
}
echo "</table>";
}//ends service delivery challan
function customer_pro_desc_title_service($model_no,$service_order_id)

{

$sql = "select Description from tbl_do_products_service where ItemCode = '$model_no' and service_order_id ='$service_order_id' ";

$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);

$row = mysqli_fetch_object($rs);

$customer_pro_desc_title	  = $row->Description;

return $customer_pro_desc_title;

}
//30-03-2020

function pro_text_ordered_service($IdValue, $service_order_id)

{

$sqlCountry = "select pro_text,pro_model from tbl_do_products_service where pro_id = '$IdValue' and service_order_id='$service_order_id' and deleteflag = 'active' order by service_order_pros_id desc";

$rsCountry  = mysqli_query($GLOBALS["___mysqli_ston"],  $sqlCountry);

$rowCountry = mysqli_fetch_object($rsCountry);

$pro_text	= $rowCountry->pro_text."(".$rowCountry->pro_model.")";

//	$country 	= $IdValue;

if(trim(strlen($pro_text))>0)

{

return $pro_text;

}

else

{

echo "";

}

}
/*performa invoice service edit function*/
function OrderItemsInfo_invoice1_edit_performa_service($service_order_id)

{

	$sql_dis_currency="SELECT Price_value FROM tbl_order_service where service_orders_id='".$service_order_id."'";

$sql_dis_dis_currency=mysqli_query($GLOBALS["___mysqli_ston"],  $sql_dis_currency);

$sql_dis_row_currency=mysqli_fetch_object($sql_dis_dis_currency);

$Offer_Currency=$sql_dis_row_currency->Price_value;

$Offer_Currency=str_replace("backoffice","crm",$Offer_Currency);

$symbol		= $this->currencySymbol(1);

$currency1 	= $symbol[0];

$curValue 	= $symbol[1];

$totalCost  = 0;

echo "<table width='100%' border='0' cellpadding='1' cellspacing='0' class='tblBorder_invoice_right1   tblBorder_invoice_left1'>";

//echo "<tr class='pagehead'><td colspan='11' class='pad'>Item(s) Information </td></tr>";

echo "<tr class='head'>";

echo "<th class='pad tblBorder_invoice_bottom tblBorder_invoice_right' width='2%' >S.No</td>";

echo "<th width='10%' class='pad tblBorder_invoice_bottom tblBorder_invoice_right' >Product Name</th>";

echo "<th width='5%' class='pad tblBorder_invoice_bottom tblBorder_invoice_right' >HSN Code</th>";

echo "<th width='5%' class='pad tblBorder_invoice_bottom tblBorder_invoice_right' >Part No.</th>";

echo "<th width='5%' class='tblBorder_invoice_bottom tblBorder_invoice_right'>Qty</th>

<th width='10%' class='tblBorder_invoice_bottom tblBorder_invoice_right' nowrap='nowrap'>Unit Price</th>

<th width='10%' class='tblBorder_invoice_bottom tblBorder_invoice_right' nowrap='nowrap'>Rate</th>";

$sql_dis="SELECT show_discount FROM prowise_discount_service where serviceorderid='".$service_order_id."'";

$sql_dis_dis=mysqli_query($GLOBALS["___mysqli_ston"],  $sql_dis);

$sql_dis_row=mysqli_fetch_object($sql_dis_dis);

if($sql_dis_row->show_discount=="Yes") {

echo "<th width='10%' class='tblBorder_invoice_bottom tblBorder_invoice_right' nowrap='nowrap' style='display:none' >Discount(-)</th>";

$dis_td=1;

}

else

{

}

echo "<th width='10%' class='pad tblBorder_invoice_bottom tblBorder_invoice_right' style='display:none'>Add IGST Value(%)</th>";

//		echo "<td width='10%' class='pad tblBorder_invoice_bottom tblBorder_invoice_right' >GST %</td>";

echo "<th nowrap='nowrap' width='10%' class='tblBorder_invoice_bottom ' >Sub Total</th></tr>";

$sqlOrderPro = "select * from tbl_order_service_product where service_order_id ='$service_order_id' order by service_order_pros_id asc ";

$rsOrderPro	 = mysqli_query($GLOBALS["___mysqli_ston"],  $sqlOrderPro);

$h=0;

while($rowOrderPro = mysqli_fetch_object($rsOrderPro))

{

$h++;

echo "<tr class='text'>";

?>
<?php

echo "<td class='pad tblBorder_invoice_bottom tblBorder_invoice_right' >".$h."</td>";

echo "<td class='pad tblBorder_invoice_bottom tblBorder_invoice_right' valign='top'><strong>".$rowOrderPro->pro_name."</strong><br />";

$OrderProID	 = $rowOrderPro->order_pros_id;

$proID	 	 = $rowOrderPro->pro_id;

$groupID 	 = $rowOrderPro->group_id;
$pro_max_discount_allowed=$this->product_discount($proID);

$hsn_code=$rowOrderPro->hsn_code;
if($hsn_code=='')
{
$hsn_code="N/A";
}

echo "</td>";

echo "<td valign='top' class='tblBorder_invoice_bottom tblBorder_invoice_right'> $hsn_code </td>";

echo "<td valign='top' class='tblBorder_invoice_bottom tblBorder_invoice_right'> $rowOrderPro->pro_model </td>";

//echo "<td valign='top' class='tblBorder_invoice_bottom tblBorder_invoice_right'>".str_replace("ï¿½"," ",$this->proidentrydesc($rowOrderPro->proidentry))."</td>";

//echo "<td valign='top' class='tblBorder_invoice_bottom tblBorder_invoice_right'><input type='text' name='pro_qty[]' value='". $rowOrderPro->pro_quantity."' size='4'></td>" ;

echo "<td valign='top' class='tblBorder_invoice_bottom tblBorder_invoice_right'>";

echo '<input type="text" name="pro_qty[]" value="'. $rowOrderPro->pro_quantity.'" size="2" class="form-control">';

// $rowOrderPro->pro_quantity </td>";

//$ManufacturerID = $rowOrderPro->manufacturers_id;

/*			$sqlManuF		= "select * from tbl_manufacturers where manufacturers_id = $ManufacturerID";

$rsManuF		= mysqli_query($GLOBALS["___mysqli_ston"],  $sqlManuF);

if(mysqli_num_rows($rsManuF)>0)

{

$rowManuF  = mysqli_fetch_object($rsManuF);

$ManuFname = $rowManuF->manufacturers_name;

}*/

$orPrice = $rowOrderPro->pro_price;
echo "<td valign='top' align='center' class='tblBorder_invoice_bottom tblBorder_invoice_right' nowrap='nowrap'> ";

echo $Offer_Currency." ";

$per_product_price=number_format($orPrice,2);

echo  $per_product_price;

echo '<input type="hidden" name="pro_max_discount_allowed[]" value="'. $pro_max_discount_allowed.'" size="4" class="form-control">';

echo "</td>";
$sql_dis="SELECT * FROM prowise_discount_service where serviceorderid='".$service_order_id."' and proid='".$proID."'";

$sql_dis_dis=mysqli_query($GLOBALS["___mysqli_ston"],  $sql_dis);

$sql_dis_row=mysqli_fetch_object($sql_dis_dis);

if($sql_dis_row->show_discount=="Yes" || $dis_td) {

echo "<td valign='top' class='tblBorder_invoice_bottom tblBorder_invoice_right' nowrap='nowrap' style='display:none'>";

echo $Offer_Currency." "; 

echo number_format($sql_dis_row->discount_amount,2);

echo "<br />";

$discounted_price=$orPrice*$sql_dis_row->discount_percent/100;			

//$sql_dis_row->discount_percent;

echo '<input type="hidden" name="pro_price1[]" value="'. $rowOrderPro->pro_price.'" >';

echo '<input type="hidden" name="pro_id[]" value="'. $rowOrderPro->pro_id.'" >';

echo '<input type="hidden" name="discount_percent[]" value="'. $sql_dis_row->discount_percent.'" size="4" class="form-control"><br>'.$sql_dis_row->discount_percent.'%';

echo "</td>";

}

else

{

	$discounted_price=$orPrice*$sql_dis_row->discount_percent/100;	

echo '<input type="hidden" name="pro_price2[]" value="'. $rowOrderPro->pro_price.'" >';

echo '<input type="hidden" name="pro_id[]" value="'. $rowOrderPro->pro_id.'" >';

}
echo "<td valign='top' align='center' class='tblBorder_invoice_bottom tblBorder_invoice_right' nowrap='nowrap'> ";

echo $Offer_Currency." ";

$per_product_price=number_format($orPrice-$discounted_price,2);

echo '<input type="text" name="pro_price[]" value="'. $per_product_price.'" size="10" class="form-control" style="display:inline-block;text-align: right;" >';

echo '<input type="hidden" name="pro_max_discount_allowed[]" value="'. $pro_max_discount_allowed.'" size="4" class="form-control">';

echo "</td>";
echo "<td valign='top' class='tblBorder_invoice_bottom tblBorder_invoice_right' nowrap='nowrap' style='display:none'> ";

echo $Offer_Currency." ";

//	echo $per_product_tax=$rowOrderPro->Pro_tax;

echo "".			$discounted_price_tax_amt=($orPrice-$discounted_price)*$rowOrderPro->GST_percentage/100;			

echo "<br>(".$per_product_GST_percentage=$rowOrderPro->GST_percentage;

echo ")</td>";

/*		echo "<td valign='top' class='tblBorder_invoice_bottom tblBorder_invoice_right' nowrap='nowrap'> ";

echo $Offer_Currency." ";

echo $per_product_GST_percentage=$rowOrderPro->GST_percentage;

echo "</td>";

*/			

$SumProPrice = ($orPrice-$discounted_price+$discounted_price_tax_amt)* $rowOrderPro->pro_quantity;//$proPrice * $rowOrderPro->pro_quantity;

$totalTax = $discounted_price_tax_amt * $rowOrderPro->pro_quantity;//$rowOrderPro->Pro_tax * $rowOrderPro->pro_quantity;

$ProTotalPrice = ($orPrice-$discounted_price)* $rowOrderPro->pro_quantity;//($rowOrderPro->pro_final_price) * $rowOrderPro->pro_quantity ;//remove tax changed by rumit

$totalCost     = $totalCost + $ProTotalPrice ;

echo "<td valign='top' class='tblBorder_invoice_bottom ' nowrap='nowrap'>";

echo $Offer_Currency." "; 

echo number_format($ProTotalPrice,2);

echo "</td>";

//echo "<td><input type='submit' value='Delete' class='inputton' name='REM_NOW' onclick=\"return confirm('Are You Sure to Delete this product.\");' /><input type='idden' name='ID' value='$rowOrderPro->pro_id' /></td>";

//echo "<input type='idden' name='ID' value='$rowOrderPro->pro_id' />";

echo "</tr>";

//			echo "<tr bgcolor='#F6F6F6'><td colspan='11' height='2'></td></tr>";

}

echo "</table>";
	}

/*PI ends*/
////////////////////////serviec order total PI 01-april-2020///////////////////////////////////////////////////////

function PerformaItemsInfo_invoice1_service($service_order_id)
{
$sql_dis_currency="SELECT Price_value FROM tbl_order_service where service_orders_id='".$service_order_id."'";$sql_dis_dis_currency=mysqli_query($GLOBALS["___mysqli_ston"],  $sql_dis_currency);
$sql_dis_row_currency=mysqli_fetch_object($sql_dis_dis_currency);
$Offer_Currency=$sql_dis_row_currency->Price_value;
$Offer_Currency=str_replace("backoffice","crm",$Offer_Currency);
$symbol		= $this->currencySymbol(1);
$currency1 	= $symbol[0];
$curValue 	= $symbol[1];
$totalCost  = 0;
echo "<table width='100%' border='0' cellpadding='5' cellspacing='0' class='tblBorder_invoice_right   tblBorder_invoice_left'>";
//echo "<tr class='pagehead'><td colspan='11' class='pad'>Item(s) Information </td></tr>";
echo "<tr class='head'>";
echo "<td class='pad tblBorder_invoice_bottom tblBorder_invoice_right' width='2%' nowrap='nowrap'><strong>S.No</strong></td>";
echo "<td width='50%' class='pad tblBorder_invoice_bottom tblBorder_invoice_right' ><strong>Product Name</strong></td>";
echo "<td width='10%' class='pad tblBorder_invoice_bottom tblBorder_invoice_right' ><strong>HSN Code</strong></td>";echo "<td width='5%' class='tblBorder_invoice_bottom tblBorder_invoice_right' align='center'><strong>Qty</strong></td>
<td width='10%' class='tblBorder_invoice_bottom tblBorder_invoice_right' nowrap='nowrap' align='center' ><strong>Rate</strong></td>";
$sql_dis="SELECT show_discount FROM prowise_discount_service where serviceorderid='".$service_order_id."'";
$sql_dis_dis=mysqli_query($GLOBALS["___mysqli_ston"],  $sql_dis);
$sql_dis_row=mysqli_fetch_object($sql_dis_dis);
if($sql_dis_row->show_discount=="Yes") {
echo "<td width='10%' class='tblBorder_invoice_bottom tblBorder_invoice_right' nowrap='nowrap' align='center' style='display:none'><strong>Discount</strong></td>";
$dis_td=1;
}
else
{
}
echo "<td width='10%' class='pad tblBorder_invoice_bottom tblBorder_invoice_right' align='center' style='display:none'><strong>Add IGST Value (%)</strong></td>";
//echo "<td width='10%' class='pad tblBorder_invoice_bottom tblBorder_invoice_right' >GST %</td>";
echo "<td nowrap='nowrap' width='10%' class='tblBorder_invoice_bottom ' nowrap='nowrap' align='center'><strong>Amount</strong></td></tr>";
//		echo "<tr><td height='1px' colspan='11' class='tblBorder_invoice_bottom'>hf</td></tr>"; //blank line
$sqlOrderPro = "select * from tbl_order_service_product where service_order_id ='$service_order_id' order by service_order_pros_id asc ";
$rsOrderPro	 = mysqli_query($GLOBALS["___mysqli_ston"],  $sqlOrderPro);
$h=0;
while($rowOrderPro = mysqli_fetch_object($rsOrderPro))
{
$h++;
echo "<tr class='text'>";
echo "<td class='pad tblBorder_invoice_bottom tblBorder_invoice_right' >".$h."</td>";
echo "<td class='pad tblBorder_invoice_bottom tblBorder_invoice_right' valign='top'><strong>".$rowOrderPro->pro_name."</strong><br />";
$OrderProID	 = $rowOrderPro->service_order_pros_id;
$proID	 	 = $rowOrderPro->pro_id;
$groupID 	 = $rowOrderPro->group_id;
$sqlAttr = "select * from tbl_group where group_id='$groupID'";
$rsAttr  = mysqli_query($GLOBALS["___mysqli_ston"],  $sqlAttr);
$i  	 = -1;
$k  	 = 0;
$cb 	 = 0;
$tx		 = 0;
$ta		 = 0;
if($cb != 0)
{
}
if($tx!=0)
{
}
if($ta != 0)
{
}
$hsn_code=$rowOrderPro->hsn_code;
if($hsn_code==''){
$hsn_code="N/A";
}
echo "</td>";
echo "<td valign='top' class='tblBorder_invoice_bottom tblBorder_invoice_right'> $hsn_code </td>";
//echo "<td valign='top' class='tblBorder_invoice_bottom tblBorder_invoice_right'> $rowOrderPro->pro_model </td>";
//echo "<td valign='top' class='tblBorder_invoice_bottom tblBorder_invoice_right'>".str_replace("ï¿½"," ",$this->proidentrydesc($rowOrderPro->proidentry))."</td>";
echo "<td valign='top' align='center' class='tblBorder_invoice_bottom tblBorder_invoice_right'> $rowOrderPro->pro_quantity </td>";
$ManufacturerID = $rowOrderPro->manufacturers_id;
/*			$sqlManuF		= "select * from tbl_manufacturers where manufacturers_id = $ManufacturerID";
$rsManuF		= mysqli_query($GLOBALS["___mysqli_ston"],  $sqlManuF);
if(mysqli_num_rows($rsManuF)>0)
{
$rowManuF  = mysqli_fetch_object($rsManuF);
$ManuFname = $rowManuF->manufacturers_name;
}*/
$orPrice = $rowOrderPro->pro_price;
$sql_dis="SELECT * FROM prowise_discount_service where serviceorderid='".$service_order_id."' and proid='".$proID."'";
$sql_dis_dis=mysqli_query($GLOBALS["___mysqli_ston"],  $sql_dis);
$sql_dis_row=mysqli_fetch_object($sql_dis_dis);
if($sql_dis_row->show_discount=="Yes" || $dis_td) {
echo "<td valign='top' align='center' class='tblBorder_invoice_bottom tblBorder_invoice_right' nowrap='nowrap' style='display:none'>";
echo $Offer_Currency." "; 
//	echo "ABC:=". number_format($sql_dis_row->discount_amount,2);
echo "".			$discounted_price=$orPrice*$sql_dis_row->discount_percent/100;			
echo "<br />";
echo " (".$sql_dis_row->discount_percent."%)";
echo "</td>";
}
else
{

	$discounted_price=$orPrice*$sql_dis_row->discount_percent/100;			//added by rumit on 16-5-2019

}echo "<td valign='top' align='center' class='tblBorder_invoice_bottom tblBorder_invoice_right' nowrap='nowrap'> ";
echo $Offer_Currency." ";
echo $per_product_price=number_format($orPrice-$discounted_price,2);
echo "</td>";echo "<td valign='top' align='center' class='tblBorder_invoice_bottom tblBorder_invoice_right' nowrap='nowrap' style='display:none'> ";
echo $Offer_Currency." ";
//	echo $per_product_tax=$rowOrderPro->Pro_tax;
echo "".$discounted_price_tax_amt=($orPrice-$discounted_price)*$rowOrderPro->GST_percentage/100;			
echo "<br>(".$per_product_GST_percentage=str_replace("%","",$rowOrderPro->GST_percentage)."%";
echo ")</td>";
/*echo "<td valign='top' class='tblBorder_invoice_bottom tblBorder_invoice_right' nowrap='nowrap'> ";
echo $Offer_Currency." ";
echo $per_product_GST_percentage=$rowOrderPro->GST_percentage;
echo "</td>";
*/	
$SumProPrice = ($orPrice-$discounted_price+$discounted_price_tax_amt)* $rowOrderPro->pro_quantity;//$proPrice * $rowOrderPro->pro_quantity;
$totalTax = $discounted_price_tax_amt * $rowOrderPro->pro_quantity;//$rowOrderPro->Pro_tax * $rowOrderPro->pro_quantity;
$ProTotalPrice = ($orPrice-$discounted_price)* $rowOrderPro->pro_quantity;//($rowOrderPro->pro_final_price) * $rowOrderPro->pro_quantity ;//remove tax changed by rumit
$totalCost     = $totalCost + $ProTotalPrice ;
echo "<td valign='top' align='center' class='tblBorder_invoice_bottom ' nowrap='nowrap'>";
echo $Offer_Currency." "; 
echo number_format($ProTotalPrice,2);
echo "</td>";
echo "</tr>";
//			echo "<tr bgcolor='#F6F6F6'><td colspan='11' height='2'></td></tr>";
}
echo "</table>";
}

///////////////////////////////////////
//PI freight edit function serviceorder

function proforma_invoice_total_edit_service($service_order_id)

{

$symbol		= $this->currencySymbol(1);

$currency1 	= $symbol[0];

$curValue 	= $symbol[1];

$sqlOrderTotal = "select * from tbl_order_service where service_orders_id = '$service_order_id'";

$rsOrderTotal  =  mysqli_query($GLOBALS["___mysqli_ston"],  $sqlOrderTotal);

if(mysqli_num_rows($rsOrderTotal)>0)

{

$rowOrderTotal 	= mysqli_fetch_object($rsOrderTotal);

$sqlOrderPro = "select * from tbl_order_service_product where service_order_id ='$service_order_id' order by service_order_pros_id asc";

$rsOrderPro	 = mysqli_query($GLOBALS["___mysqli_ston"],  $sqlOrderPro);

$h=0;

$subtotal1=0;

while($rowOrderPro = mysqli_fetch_object($rsOrderPro))

{

$h++;

$OrderProID	 = $rowOrderPro->service_order_pros_id;

$proID	 	 = $rowOrderPro->pro_id;

$groupID 	 = $rowOrderPro->group_id;

$orPrice 	 = $rowOrderPro->pro_price;

number_format($orPrice,2);
$sql_dis="SELECT * FROM prowise_discount_service where serviceorderid='".$service_order_id."' and proid='".$proID."'";

$sql_dis_dis=mysqli_query($GLOBALS["___mysqli_ston"],  $sql_dis);

$sql_dis_row=mysqli_fetch_object($sql_dis_dis);

$subtotal1=($orPrice*$rowOrderPro->pro_quantity)-$sql_dis_row->discount_amount;

$subtotal1_dis=$sql_dis_row->discount_amount;

$discounted_price=$orPrice*$sql_dis_row->discount_percent/100;			//discount amt per unit

$per_product_GST_percentage=$rowOrderPro->GST_percentage;//product gst %

$discounted_price_tax_amt=($orPrice-$discounted_price)*$rowOrderPro->GST_percentage/100;			

$SumProPrice = ($orPrice-$discounted_price+$discounted_price_tax_amt)* $rowOrderPro->pro_quantity;//$proPrice * $rowOrderPro->pro_quantity;

$totalTax+= $discounted_price_tax_amt * $rowOrderPro->pro_quantity;//$rowOrderPro->Pro_tax * $rowOrderPro->pro_quantity;

$ProTotalPrice = ($discounted_price_tax_amt+$orPrice-$discounted_price)* $rowOrderPro->pro_quantity;//($rowOrderPro->pro_final_price) * $rowOrderPro->pro_quantity ;//remove tax changed by rumit

$freight_amount = $rowOrderPro->freight_amount;// * $rowOrderPro->pro_quantity;

"freight without GST value:".$freight_amount_with_gst = $rowOrderPro->freight_amount/1.18;// * $rowOrderPro->pro_quantity;

"<br>freight GST value:".$freight_gst_amount = $rowOrderPro->freight_amount-$freight_amount_with_gst;// * $rowOrderPro->pro_quantity;

$totalCost     = $totalCost + $ProTotalPrice;

//		$discounted_price_tax_amt=($orPrice-$discounted_price)*$rowOrderPro->GST_percentage/100;				

}

$GST_tax_amt				= $this->GST_tax_amount_on_offer($service_order_id);//$rowOrderPro->GST_percentage;				

$TotalOrder	   				= $rowOrderTotal->total_order_cost;

$ship		   				= $rowOrderTotal->shipping_method_cost;

$shippingValue				= $ship;

$taxValue					= $rowOrderTotal->tax_cost;

$tax_included				= $rowOrderTotal->tax_included;

$tax_perc					= $rowOrderTotal->taxes_perc;

$discount_perc				= $rowOrderTotal->discount_perc;

$discount_per_amt			= $rowOrderTotal->discount_per_amt;

$show_discount				=$rowOrderTotal->show_discount;

$subtotal_after_discount    = $TotalOrder - $subtotal1_dis;
if($tax_included=='Excluded')
{
$subtotal_tax       		= $subtotal_after_discount*$tax_perc/100;
}
else
{
$subtotal_tax       		= $subtotal_after_discount;
}
if($tax_included=='Excluded')
{
$GrandTotalOrder			= $subtotal_after_discount + $subtotal_tax;
}
else
{
$GrandTotalOrder			= $subtotal_after_discount + 0;
}
//$subtotal_final		= $subtotal_tax;
$couponDiscount 			= $rowOrderTotal->coupon_discount;
}
echo "<table width='50%' border='0' cellpadding='5' cellspacing='0' >";
/*echo "<tr class='pagehead'>";
echo "<td colspan='2' class='pad' nowrap  align='left'>Order Total </td>";
echo "</tr>";*/
echo "<tr class='text'>";
echo "<td width='45%' class='pad tblBorder_invoice_bottom tblBorder_invoice_right tblBorder_invoice_left ' nowrap align='right'><strong>Sub Total :</strong> </td>";
echo "<td width='30%'  align='right' class='tblBorder_invoice_bottom ' nowrap='nowrap'> &nbsp; &nbsp;";
echo str_replace("backoffice","crm",$rowOrderTotal->Price_value)." ";
echo number_format($totalCost-$totalTax,2);
echo "</td>";
echo "</tr>";
//echo $show_discount;
if($show_discount=="Yes") {
echo "<tr class='text' style='display:none'>";
echo "<td  class='pad tblBorder_invoice_bottom tblBorder_invoice_right tblBorder_invoice_left' nowrap align='right'><strong>Discount :</strong></td>";
echo "<td   align='right'  class='tblBorder_invoice_bottom'  > &nbsp; &nbsp;";
echo str_replace("backoffice","crm",$rowOrderTotal->Price_value)." ";
echo number_format($subtotal1_dis,2);
echo "</td>";
echo "</tr>";
}echo "<tr class='text'>";
echo "<td class='pad tblBorder_invoice_right tblBorder_invoice_bottom tblBorder_invoice_left' nowrap align='right'><strong>Freight Value :</strong></td>";
echo "<td align='right'  class='tblBorder_invoice_bottom' nowrap='nowrap' >&nbsp; &nbsp; ";
echo str_replace("backoffice","crm",$rowOrderTotal->Price_value)." ";

$total_freight_amt_show=$freight_amount-$freight_gst_amount;

?>

<!--changed dropdown from text field on 24-5-2019 as disscssed with Mr. Pankaj & sandeep-->

<input type="text" name='freight_included' id='freight_included' value="<?php echo number_format($total_freight_amt_show);?>" class='form-control' required dir="rtl" style="width:70%; display:inline; padding:0; margin:0; height:auto" />
<?php

echo "</td>";

echo "</tr>";

$Grand_subtotal1=$subtotal1;

if($tax_included=='Excluded' || $GST_tax_amt>0)

{

//	echo "</tr>";

echo "<tr class='text'>";

if($GST_tax_amt>0)

{

echo "<td class='pad tblBorder_invoice_bottom tblBorder_invoice_right tblBorder_invoice_left' nowrap align='right'><strong>Add IGST :</strong></td>";

}

else

{

echo "<td class='pad tblBorder_invoice_bottom tblBorder_invoice_right tblBorder_invoice_left' nowrap align='right'><strong>Add VAT/CST@ $tax_perc% :</strong></td>";

}

echo "<td align='right'  class='tblBorder_invoice_bottom'  > &nbsp; &nbsp; ";

if($totalTax>0)

{

echo str_replace("backoffice","crm",$rowOrderTotal->Price_value)." ";

echo number_format($totalTax+$freight_gst_amount,2);//total gst value rumit 

}

else

{

echo str_replace("backoffice","crm",$rowOrderTotal->Price_value)." ";

$subtotal_tax1=$subtotal_tax;

echo number_format($subtotal_tax1,2);

}//echo $subtotal1;

$Grand_subtotal1=$totalCost;//$subtotal1+$subtotal_tax1+$totalTax;

echo "</td>";

echo "</tr>";

}
echo "<tr class='text'>";

echo "<td class='pad tblBorder_invoice_right tblBorder_invoice_left' nowrap align='right'><h4>Grand Total :</h4></td>";

echo "<td align='right'  nowrap='nowrap' ><h4> &nbsp; &nbsp; ";

echo str_replace("backoffice","crm",$rowOrderTotal->Price_value)." ";

//		printf(" %.2f",$TotalOrder);

echo number_format($totalCost+$freight_amount,2);

echo "</h4></td>";

echo "</tr>";

echo "</table>";

}/////////// this function use to display the order Total amount info in proforma invoice service

function proforma_invoice_total_service($service_order_id)

{

$symbol		= $this->currencySymbol(1);

$currency1 	= $symbol[0];

$curValue 	= $symbol[1];

$sqlOrderTotal = "select * from tbl_order_service where service_orders_id = '$service_order_id'";

$rsOrderTotal=  mysqli_query($GLOBALS["___mysqli_ston"],  $sqlOrderTotal);

if(mysqli_num_rows($rsOrderTotal)>0)

{

$rowOrderTotal 	= mysqli_fetch_object($rsOrderTotal);

$sqlOrderPro = "select * from tbl_order_service_product where service_order_id ='$service_order_id' order by service_order_pros_id asc";

$rsOrderPro	 = mysqli_query($GLOBALS["___mysqli_ston"],  $sqlOrderPro);

$h=0;

$subtotal1=0;

while($rowOrderPro = mysqli_fetch_object($rsOrderPro))
{
$h++;
$OrderProID	 = $rowOrderPro->order_pros_id;
$proID	 	 = $rowOrderPro->pro_id;
$groupID 	 = $rowOrderPro->group_id;
$orPrice 	 = $rowOrderPro->pro_price;
number_format($orPrice,2);
/*$GST_tax				=$rowOrderPro->Pro_tax;
echo "GSTSGSGST".$GST_percentage		=$this->GST_tax_amount_on_service_offer($service_order_id);//$rowOrderPro->GST_percentage;		
*/
$sql_dis="SELECT * FROM prowise_discount_service where serviceorderid='".$service_order_id."' and proid='".$proID."'";
$sql_dis_dis=mysqli_query($GLOBALS["___mysqli_ston"],  $sql_dis);
$sql_dis_row=mysqli_fetch_object($sql_dis_dis);
$subtotal1=($orPrice*$rowOrderPro->pro_quantity)-$sql_dis_row->discount_amount;
$subtotal1_dis=$sql_dis_row->discount_amount;
$discounted_price=$orPrice*$sql_dis_row->discount_percent/100;			//discount amt per unit
$per_product_GST_percentage=$rowOrderPro->GST_percentage;//product gst %
$discounted_price_tax_amt=($orPrice-$discounted_price)*$rowOrderPro->GST_percentage/100;			
$SumProPrice = ($orPrice-$discounted_price+$discounted_price_tax_amt)* $rowOrderPro->pro_quantity;//$proPrice * $rowOrderPro->pro_quantity;
$totalTax+= $discounted_price_tax_amt * $rowOrderPro->pro_quantity;//$rowOrderPro->Pro_tax * $rowOrderPro->pro_quantity;
$ProTotalPrice = ($discounted_price_tax_amt+$orPrice-$discounted_price)* $rowOrderPro->pro_quantity;//($rowOrderPro->pro_final_price) * $rowOrderPro->pro_quantity ;//remove tax changed by rumit
$freight_amount = $rowOrderPro->freight_amount;// * $rowOrderPro->pro_quantity;
"freight without GST value:".$freight_amount_with_gst = $rowOrderPro->freight_amount/1.18;// * $rowOrderPro->pro_quantity;
"<br>freight GST value:".$freight_gst_amount = $rowOrderPro->freight_amount-$freight_amount_with_gst;// * $rowOrderPro->pro_quantity;
$totalCost     = $totalCost + $ProTotalPrice;
}
$GST_tax_amt				= $this->GST_tax_amount_on_service_offer($service_order_id);//$rowOrderPro->GST_percentage;				
$TotalOrder	   				= $rowOrderTotal->total_order_cost;
$ship		   				= $rowOrderTotal->shipping_method_cost;
$shippingValue				= $ship;
$taxValue					= $rowOrderTotal->tax_cost;
$tax_included				= $rowOrderTotal->tax_included;
$tax_perc					= $rowOrderTotal->taxes_perc;
$discount_perc				= $rowOrderTotal->discount_perc;
$discount_per_amt			= $rowOrderTotal->discount_per_amt;
$show_discount				=$rowOrderTotal->show_discount;
$subtotal_after_discount    = $TotalOrder - $subtotal1_dis;
if($tax_included=='Excluded')
{
$subtotal_tax       		= $subtotal_after_discount*$tax_perc/100;
}
else
{
$subtotal_tax       		= $subtotal_after_discount;
}
if($tax_included=='Excluded')
{
$GrandTotalOrder			= $subtotal_after_discount + $subtotal_tax;
}
else
{
$GrandTotalOrder			= $subtotal_after_discount + 0;
}
$couponDiscount 			= $rowOrderTotal->coupon_discount;
}
echo "<table width='50%' border='0' cellpadding='5' cellspacing='0' >";
echo "<tr class='text'>";
echo "<td width='45%' class='pad tblBorder_invoice_bottom tblBorder_invoice_right tblBorder_invoice_left ' nowrap align='right'><strong>Sub Total :</strong> </td>";
echo "<td width='30%'  align='right' class='tblBorder_invoice_bottom ' nowrap='nowrap'> &nbsp; &nbsp;";
echo str_replace("backoffice","crm",$rowOrderTotal->Price_value)." ";
echo number_format($totalCost-$totalTax,2);
echo "</td>";
echo "</tr>";
//echo $show_discount;
if($show_discount=="Yes") {
echo "<tr class='text' style='display:none'>";
echo "<td  class='pad tblBorder_invoice_bottom tblBorder_invoice_right tblBorder_invoice_left' nowrap align='right'><strong>Discount :</strong></td>";
echo "<td   align='right'  class='tblBorder_invoice_bottom'  > &nbsp; &nbsp;";
echo str_replace("backoffice","crm",$rowOrderTotal->Price_value)." ";
echo number_format($subtotal1_dis,2);
echo "</td>";
echo "</tr>";
}
echo "<tr class='text'>";
echo "<td class='pad tblBorder_invoice_right tblBorder_invoice_bottom tblBorder_invoice_left' nowrap align='right'><strong>Freight Value :</strong></td>";
echo "<td align='right'  class='tblBorder_invoice_bottom' nowrap='nowrap' >&nbsp; &nbsp; ";
echo str_replace("backoffice","crm",$rowOrderTotal->Price_value)." ";
echo number_format($freight_amount-$freight_gst_amount,2);
echo "</td>";
echo "</tr>";
$Grand_subtotal1=$subtotal1;
if($tax_included=='Excluded' || $GST_tax_amt>0)
{
echo "<tr class='text'>";
if($GST_tax_amt>0)
{
echo "<td class='pad tblBorder_invoice_bottom tblBorder_invoice_right tblBorder_invoice_left' nowrap align='right'><strong>Add IGST :</strong></td>";
}
else
{
echo "<td class='pad tblBorder_invoice_bottom tblBorder_invoice_right tblBorder_invoice_left' nowrap align='right'><strong>Add VAT/CST@ $tax_perc% :</strong></td>";
}
echo "<td align='right'  class='tblBorder_invoice_bottom'  > &nbsp; &nbsp; ";
if($totalTax>0)
{
echo str_replace("backoffice","crm",$rowOrderTotal->Price_value)." ";
echo number_format($totalTax+$freight_gst_amount,2);//total gst value rumit 
}
else
{
echo str_replace("backoffice","crm",$rowOrderTotal->Price_value)." ";
$subtotal_tax1=$subtotal_tax;
echo number_format($subtotal_tax1,2);
}//echo $subtotal1;
$Grand_subtotal1=$totalCost;//$subtotal1+$subtotal_tax1+$totalTax;
echo "</td>";
echo "</tr>";
}
echo "<tr class='text'>";
echo "<td class='pad tblBorder_invoice_right tblBorder_invoice_left' nowrap align='right'><h4>Grand Total :</h4></td>";
echo "<td align='right'  nowrap='nowrap' ><h4> &nbsp; &nbsp; ";
echo str_replace("backoffice","crm",$rowOrderTotal->Price_value)." ";
//		printf(" %.2f",$TotalOrder);
echo number_format($totalCost+$freight_amount,2);
echo "</h4></td>";
echo "</tr>";
echo "</table>";
}

///// added on 15-july-2020
function enq_stage($EID,$order_id)
{
$sql = "select enq_stage from tbl_web_enq_edit where ID = '$EID' and order_id = '$order_id' and deleteflag='active'";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
$enq_stage	  = $row->enq_stage;
return $enq_stage;
}

///// added on 23-july-2020
function enq_stage_name($ID)
{
$sql = "select stage_name from tbl_stage_master where stage_id = '$ID' and deleteflag='active'";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
$stage_name	  = $row->stage_name;
return $stage_name;
}

///// added on 27-july-2020
function sales_funnel_probablity($pro_id,$probablity)
{
$sql = "SELECT   count(tbl_order.offer_probability) as probcount from tbl_order_product INNER JOIN tbl_order ON tbl_order_product.order_id=tbl_order.orders_id 
  where tbl_order_product.pro_id='$pro_id' and tbl_order.offer_probability='$probablity'
group by  tbl_order.offer_probability
ORDER BY `tbl_order_product`.`pro_id` ASC";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
$probcount	  = $row->probcount;
return $probcount;
}

function get_customers_id_by_order_id($ID)
{
$sql = "select customers_id from tbl_order where orders_id = '$ID' and deleteflag='active'";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
$customers_id	  = $row->customers_id;
return $customers_id;
}

// indian money format added on 25-aug-2020
function moneyFormatIndia($num){

$explrestunits = "" ;
$num = preg_replace('/,+/', '', $num);
$words = explode(".", $num);
$des = "00";
if(count($words)<=2){
    $num=$words[0];
    if(count($words)>=2){$des=$words[1];}
    if(strlen($des)<2){$des="$des";}else{$des=substr($des,0,2);}
}
if(strlen($num)>3){
    $lastthree = substr($num, strlen($num)-3, strlen($num));
    $restunits = substr($num, 0, strlen($num)-3); // extracts the last three digits
    $restunits = (strlen($restunits)%2 == 1)?"0".$restunits:$restunits; // explodes the remaining digits in 2's formats, adds a zero in the beginning to maintain the 2's grouping.
    $expunit = str_split($restunits, 2);
    for($i=0; $i<sizeof($expunit); $i++){
        // creates each of the 2's group and adds a comma to the end
        if($i==0)
        {
            $explrestunits .= (int)$expunit[$i].","; // if is first value , convert into integer
        }else{
            $explrestunits .= $expunit[$i].",";
        }
    }
    $thecash = $explrestunits.$lastthree;
} else {
    $thecash = $num;
}
return "$thecash.$des"; // writes the final format where $currency is the currency symbol.

}
function pro_application_id($IdValue)
{
	if(is_numeric($IdValue))
	{
		$sqlApplication_pro_id = "select pro_id from tbl_index_g2 where match_pro_id_g2 = '$IdValue' and deleteflag = 'active'";
		$rsApplication_pro_id  = mysqli_query($GLOBALS["___mysqli_ston"],$sqlApplication_pro_id);
		$rowApplication_pro_id = mysqli_fetch_object($rsApplication_pro_id);
		$product_appliaction_id	  = $rowApplication_pro_id->pro_id;
	}
	else
	{
		$product_appliaction_id 	= $IdValue;
	}
	return $product_appliaction_id;
}function ApplicationTax($IdValue)
{
	if(is_numeric($IdValue))
	{
		$sqlApplication = "select tax_class_id from tbl_application where application_id = '$IdValue' and deleteflag = 'active'";
		$rsApplication  = mysqli_query($GLOBALS["___mysqli_ston"],$sqlApplication);
		$rowApplication = mysqli_fetch_object($rsApplication);
		$tax_class_id	  = $rowApplication->tax_class_id;
	}
	else
	{
		$tax_class_id 	= $IdValue;
	}
	return $tax_class_id;
}//added on 9-oct-2020
function offer_total($pcode)
{
$symbol			= $this->currencySymbol(1);
$currency1 		= $symbol[0];
$curValue 		= $symbol[1];
$sqlOrderTotal 	= "select * from tbl_order where orders_id = '$pcode'";
$rsOrderTotal  	=  mysqli_query($GLOBALS["___mysqli_ston"],  $sqlOrderTotal);
if(mysqli_num_rows($rsOrderTotal)>0)
{
$rowOrderTotal 	= mysqli_fetch_object($rsOrderTotal);
$Price_type					= $rowOrderTotal->Price_type;//$rowOrderPro->GST_percentage;	
$sqlOrderPro = "select * from tbl_order_product where order_id ='$pcode' order by order_pros_id asc";
$rsOrderPro	 = mysqli_query($GLOBALS["___mysqli_ston"],  $sqlOrderPro);
$h=0;
$subtotal1=0;
while($rowOrderPro = mysqli_fetch_object($rsOrderPro))
{
$h++;
$OrderProID	 = $rowOrderPro->order_pros_id;
$proID	 	 = $rowOrderPro->pro_id;
$groupID 	 = $rowOrderPro->group_id;

$orPrice = $rowOrderPro->pro_price;
number_format($orPrice,2);
/*$GST_tax				=$rowOrderPro->Pro_tax;
echo "GSTSGSGST".$GST_percentage		=$this->GST_tax_amount_on_offer($pcode);//$rowOrderPro->GST_percentage;		
*/
$sql_dis="SELECT * FROM prowise_discount where orderid='".$pcode."' and proid='".$proID."'";
$sql_dis_dis=mysqli_query($GLOBALS["___mysqli_ston"],  $sql_dis);
$sql_dis_row=mysqli_fetch_object($sql_dis_dis);
$subtotal1=($orPrice*$rowOrderPro->pro_quantity)-$sql_dis_row->discount_amount;
$subtotal1_dis+=$sql_dis_row->discount_amount;
$discounted_price=$orPrice*$sql_dis_row->discount_percent/100;			
//added condition for zro GST in export offer as on dated 06-oct-2020 starts
if($Price_type!="Export_USD")
{
$per_product_GST_percentage=$rowOrderPro->GST_percentage;
$discounted_price_tax_amt=($orPrice-$discounted_price)*$rowOrderPro->GST_percentage/100;		
}
else
{
$per_product_GST_percentage="0";//$rowOrderPro->GST_percentage;
$discounted_price_tax_amt="0";//($orPrice-$discounted_price);			
}
//added condition for zro GST in export offer as on dated 06-oct-2020 ends	
	
$SumProPrice = ($orPrice-$discounted_price+$discounted_price_tax_amt)* $rowOrderPro->pro_quantity;//$proPrice * $rowOrderPro->pro_quantity;
$totalTax+= $discounted_price_tax_amt * $rowOrderPro->pro_quantity;//$rowOrderPro->Pro_tax * $rowOrderPro->pro_quantity;
$ProTotalPrice = ($discounted_price_tax_amt+$orPrice-$discounted_price)* $rowOrderPro->pro_quantity;//($rowOrderPro->pro_final_price) * $rowOrderPro->pro_quantity ;//remove tax changed by rumit
$freight_amount = $rowOrderPro->freight_amount;// * $rowOrderPro->pro_quantity;

"freight without GST value:".$freight_amount_with_gst = $rowOrderPro->freight_amount/1.18;// * $rowOrderPro->pro_quantity;

"<br>freight GST value:".$freight_gst_amount = $rowOrderPro->freight_amount-$freight_amount_with_gst;// * $rowOrderPro->pro_quantity;
$totalCost     = $totalCost + $ProTotalPrice;
}

//added condition for zro GST in export offer as on dated 06-oct-2020 starts
if($Price_type!="Export_USD")
{

$GST_tax_amt				= $this->GST_tax_amount_on_offer($pcode);//$rowOrderPro->GST_percentage;				
}
else
{
	$GST_tax_amt			= "0";//$this->GST_tax_amount_on_offer($pcode);//$rowOrderPro->GST_percentage;				
}
//added condition for zro GST in export offer as on dated 06-oct-2020 ends
$TotalOrder	   				= $rowOrderTotal->total_order_cost;
$ship		   				= $rowOrderTotal->shipping_method_cost;
$shippingValue				= $ship;
$taxValue					= $rowOrderTotal->tax_cost;
$tax_included				= $rowOrderTotal->tax_included;
$tax_perc					= $rowOrderTotal->taxes_perc;
$discount_perc				= $rowOrderTotal->discount_perc;
$discount_per_amt			= $rowOrderTotal->discount_per_amt;
$show_discount				=$rowOrderTotal->show_discount;
//			$shippingValue 	= explode(" - ",$ship);
//			$subtotal       = $TotalOrder - $shippingValue-$taxValue ;
//$subtotal       			= $TotalOrder;
$subtotal_after_discount    = $TotalOrder - $subtotal1_dis;
if($tax_included=='Excluded')
{
@$subtotal_tax       		= @$subtotal_after_discount*$tax_perc/100;
}
else
{
$subtotal_tax       		= $subtotal_after_discount;
}
if($tax_included=='Excluded')
{
$GrandTotalOrder			= $subtotal_after_discount + $subtotal_tax;
}
else
{
$GrandTotalOrder			= $subtotal_after_discount + 0;
}
//$subtotal_final		= $subtotal_tax;
$couponDiscount 			= $rowOrderTotal->coupon_discount;
}
number_format($totalCost-$totalTax,2);
number_format($freight_amount-$freight_gst_amount,2);

$Grand_subtotal1=$subtotal1;
if($tax_included=='Excluded' || $GST_tax_amt>0)
{

str_replace("backoffice","crm",$rowOrderTotal->Price_value)." ";
$subtotal_tax1=$subtotal_tax;
number_format($subtotal_tax1,2);
echo $Grand_subtotal1=$totalCost;//$subtotal1+$subtotal_tax1+$totalTax;

}
}function sanitize_from_word( $content )
{
    // Convert microsoft special characters
    $replace = array(
        "‘" => "'",
        "’" => "'",
        "”" => '"',
        "“" => '"',
        "–" => "-",
        "—" => "-",
        "…" => "&#8230;"
    );

    foreach($replace as $k => $v)
    {
        $content = str_replace($k, $v, $content);
    }

    // Remove any non-ascii character
    $content = preg_replace('/[^\x20-\x7E]*/','', $content);
//echo "AAss";
    return $content;
}

//new added on 17-dec-2020 for website product name using in FAq section or alarm WhatsaAPp 
function web_product_name($id)
{
		$sql = "select pro_title from tbl_products_1 where pro_id = '$id' and deleteflag = 'active'";
		$rs  = mysqli_query($GLOBALS["___mysqli_ston"],$sql);
		$row = mysqli_fetch_object($rs);
		$pro_title	  = $row->pro_title;
		return $pro_title;
}function vendor_config_mail_id($type)
{
		$sql = "SELECT config_mail_id FROM `tbl_vendor_confirm_mail` where vendor_product_type='$type' and vendor_master_type='master' 
		and status='active'
		and deleteflag='active'
		ORDER BY `tbl_vendor_confirm_mail`.`vendor_product_type` ASC";
		$rs  = mysqli_query($GLOBALS["___mysqli_ston"],$sql);
		$row = mysqli_fetch_object($rs);
		$config_mail_id	  = $row->config_mail_id;
		return $config_mail_id;
}//added by rumit on 18-nov-2020 -wfh
function product_desc_by_pro_id($pro_id)
{
$sql = "select pro_desc_entry from tbl_products_entry where pro_id = '$pro_id' and deleteflag = 'active' and status='active'";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
$pro_desc_entry	  = $row->pro_desc_entry;
return $pro_desc_entry;
}//added by rumit on 21-Jan-2021 
function max_upc_code()
{

$sql	= "select max(cast(upc_code as unsigned) ) AS max_upc_code from tbl_products";
$result = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
    
		if(mysqli_num_rows($result) >= 1)
		{        
			$rows_sub = mysqli_fetch_array($result);
			{            
			$upc_code_max=$rows_sub['max_upc_code'];
			}
		}
		return $upc_code_max;

}

//added on 27-jan-2021 for print label of consignee using delivery challan
function consignee_company_name($orderid)
{
$sql = "select Con_Com_name  from tbl_delivery_order where O_Id = '$orderid' ";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
$consignee= $row->Con_Com_name;
return  $consignee;
}

//added on 27-jan-2021 for print label of consignee using delivery challan
function consignee_address_by_do($orderid)
{
$sql = "select Consignee from tbl_delivery_order where O_Id = '$orderid' ";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
$Consignee	  = $row->Consignee;
return $Consignee;
}
//added on 28-jan-2021 for showing IN PQV estimated delivery date & qty 
function pqv_incoming_stock_qty_with_date($model_no)
{
$sql = "select vpf.Prodcut_Qty, vpf.E_Date, vpf.delivery from vendor_po_order vpo INNER JOIN vendor_po_final vpf ON vpo.ID = vpf.PO_ID where vpo.Confirm_Purchase='inactive' and vpf.upc_code='$model_no' and vpf.E_Date >=  (CURDATE() - INTERVAL 8 DAY) order by vpf.E_Date ";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
while($row = mysqli_fetch_object($rs))
{
echo "<strong>Qty:</strong> ".$Prodcut_Qty	 = $row->Prodcut_Qty."<br>";
if($row->E_Date!='' )
	{
echo "<strong>Incoming Stock Date:</strong> ".$E_date	 = $this->date_format_india($row->E_Date . ' + 8 days')."<br>";//$row->E_Date."<br>";
	}

	/*if($this->date_format_india($row->delivery)!='01/Jan/1970')
	{
echo "<strong>Delivery:</strong> ".$delivery	  	  = $this->date_format_india($row->delivery . ' + 8 days')."<br>";
	}
	else
	{
echo "<strong>Delivery:</strong> ".$delivery	  	  = $row->delivery."<br>";		
	}*/
/*if($Prodcut_Qty>0)
{
$incoming_stock_date="Qty: ".$Prodcut_Qty.'<br> Delivery :'.$delivery.'<br>'.$E_date;
}
else
{
	$incoming_stock_date="N/A";
}
*/
//return $incoming_stock_date;
}
}

//added by rumit on 1-Feb-2021 
function pro_upc_code($pro_id)
{

$sql	= "select upc_code from tbl_products where pro_id='$pro_id' ";
$result = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
    
		if(mysqli_num_rows($result) >= 1)
		{        
			$rows_sub = mysqli_fetch_object($result);
			{            
			$upc_code=$rows_sub->upc_code;
			}
		}
		return $upc_code;

}//added by rumit on 16-Feb-2021 

function po_acl_item_code($pro_id)
{

$sql	= "select ACL_Item_Code from vendor_product_list where ID='$pro_id' ";
$result = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
    
		if(mysqli_num_rows($result) >= 1)
		{        
			$rows_sub = mysqli_fetch_object($result);
			{            
			$ACL_Item_Code=$rows_sub->ACL_Item_Code;
			}
		}
		return $ACL_Item_Code;

}
//added by rumit on 16-Feb-2021 
function po_product_list_desc($pro_id)
{

$sql	= "select Product_List from vendor_product_list where ID='$pro_id' ";
$result = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
    
		if(mysqli_num_rows($result) >= 1)
		{        
			$rows_sub = mysqli_fetch_object($result);
			{            
			$Product_List=$rows_sub->Product_List;
			}
		}
		return $Product_List;

}
//added by rumit on 16-Feb-2021 
function po_product_price($pro_id)
{

$sql	= "select Prodcut_Price from vendor_product_list where ID='$pro_id' ";
$result = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
    
		if(mysqli_num_rows($result) >= 1)
		{        
			$rows_sub = mysqli_fetch_object($result);
			{            
			$Prodcut_Price=$rows_sub->Prodcut_Price;
			}
		}
		return $Prodcut_Price;

}



//added by rumit on 15-July-2021 
function po_product_vendor($pro_id)
{

$sql	= "select Vendor_List from vendor_product_list where ID='$pro_id' ";
$result = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
    
		if(mysqli_num_rows($result) >= 1)
		{        
			$rows_sub = mysqli_fetch_object($result);
			{            
			$Vendor_List=$rows_sub->Vendor_List;
			}
		}
		return $Vendor_List;

}

//created on 15july 2021
function po_product_vendor_by_model_no($model_no)
{

echo "<br>".$sql	= "select DISTINCT(Vendor_List) from vendor_product_list where ACL_Item_Code like '%".trim($model_no)."%'  and Vendor_List!='0' ";
$result = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
    
		if(mysqli_num_rows($result) >= 1)
		{        
			$rows_sub = mysqli_fetch_object($result);
			{            
echo "Vendor code ---".			$Vendor_List=$rows_sub->Vendor_List;
			}
		}
		return $Vendor_List;

}


function po_product_vendor_item_code($pro_id)
{

$sql	= "select Vendor_Item_Code from vendor_product_list where pro_id='$pro_id' limit 0,1 "; 
$result = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
    
		if(mysqli_num_rows($result) > 0)
		{        
			$rows_sub = mysqli_fetch_object($result);
			{            
			//print_r($rows_sub);
			
			$Vendor_Item_Code=$rows_sub->Vendor_Item_Code;
			
			if($rows_sub->Vendor_Item_Code=='')
			{
		$Vendor_Item_Code="0";
			}
			}
			
			if($Vendor_Item_Code=='0')
			{
			$Vendor_Item_Code=$this->product_part($pro_id);
			}
			
		}
		
		
		
//		echo "<br>".$Vendor_Item_Code;
		return $Vendor_Item_Code;

}

function po_acl_item_code_by_pro_id($pro_id)
{

$sql	= "select ACL_Item_Code from vendor_product_list where pro_id='$pro_id' ";
$result = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
    
		if(mysqli_num_rows($result) >= 1)
		{        
			$rows_sub = mysqli_fetch_object($result);
			{            
			$ACL_Item_Code=$rows_sub->ACL_Item_Code;
			}
		}
		return $ACL_Item_Code;

}



function po_product_price_by_pro_id($pro_id)
{

$sql	= "select Prodcut_Price from vendor_product_list where pro_id='$pro_id' ";
$result = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
    
		if(mysqli_num_rows($result) >= 1)
		{        
			$rows_sub = mysqli_fetch_object($result);
			{            
			$Prodcut_Price=$rows_sub->Prodcut_Price;
			}
		}
		return $Prodcut_Price;

}


function po_product_list_desc_by_pro_id($pro_id)
{

$sql	= "select Product_List from vendor_product_list where pro_id='$pro_id' ";
$result = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
    
		if(mysqli_num_rows($result) >= 1)
		{        
			$rows_sub = mysqli_fetch_object($result);
			{            
			$Product_List=$rows_sub->Product_List;
			}
		}
		return $Product_List;

}

function vendor_master_currency($vendor_id)
{

$sql	= "select Currency from vendor_master where ID='$vendor_id' ";
$result = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
    
		if(mysqli_num_rows($result) >= 1)
		{        
			$rows_sub = mysqli_fetch_object($result);
			{            
			$Currency=$rows_sub->Currency;
			}
		}
		return $Currency;

}

//25-feb-2021
function vendor_payment_terms_name($cust_segment_id)
{
	
if(!is_numeric($cust_segment_id))
{
/*$sql = "select vendor_payment_terms_name from tbl_vendor_payment_terms_master where vendor_payment_terms_id = '$cust_segment_id' and deleteflag = 'active' and vendor_payment_terms_status='active'";*/
$vendor_payment_terms_name=$cust_segment_id;
} 
else
{
 $sql = "select vendor_payment_terms_name from tbl_vendor_payment_terms_master where vendor_payment_terms_id = '$cust_segment_id' and deleteflag = 'active' and vendor_payment_terms_status='active'";

$rs  = mysqli_query($GLOBALS["___mysqli_ston"],$sql);
$row = mysqli_fetch_object($rs);
$vendor_payment_terms_name	  = $row->vendor_payment_terms_name;
}

/*	if($cust_segment_name=='')

{

echo $cust_segment_name="N/A";

}*/

return $vendor_payment_terms_name;
}
//25-feb-2021
function vendor_payment_terms_from_po($po_id)
{
	
if(!is_numeric($po_id))
{
/*$sql = "select vendor_payment_terms_name from tbl_vendor_payment_terms_master where vendor_payment_terms_id = '$po_id' and deleteflag = 'active' and vendor_payment_terms_status='active'";*/
$vendor_payment_terms_name=$po_id;
} 
else
{
 $sql = "select Payment_Terms from vendor_po_final where PO_ID = '$po_id' ";

$rs  = mysqli_query($GLOBALS["___mysqli_ston"],$sql);
$row = mysqli_fetch_object($rs);
$Payment_Terms	  = $row->Payment_Terms;
}

/*	if($cust_segment_name=='')

{

echo $cust_segment_name="N/A";

}*/

return $Payment_Terms;
}

//17-feb-2021
function vendor_payment_terms_days($vendor_payment_terms_id)
{

if(!is_numeric($vendor_payment_terms_id))
{
/*$sql = "select vendor_payment_terms_name from tbl_vendor_payment_terms_master where vendor_payment_terms_id = '$vendor_payment_terms_id' and deleteflag = 'active' and vendor_payment_terms_status='active'";*/
$vendor_payment_terms_name=$vendor_payment_terms_id;
} 
else
{
 $sql = "select vendor_payment_terms_description from tbl_vendor_payment_terms_master where vendor_payment_terms_id = '$vendor_payment_terms_id' and deleteflag = 'active' and vendor_payment_terms_status='active'";

$rs  = mysqli_query($GLOBALS["___mysqli_ston"],$sql);
$row = mysqli_fetch_object($rs);
$vendor_payment_terms_description	  = $row->vendor_payment_terms_description;
}
/*	if($cust_segment_name=='')
{
echo $cust_segment_name="N/A";
}*/

return $vendor_payment_terms_description;
}


//added by rumit on 23-Jun-2021 fro BOM items
function product_BOM_items($pro_id)
{

echo "BOM function Query".$sql	= "select pro_id,p_pro_id,tbl_qty from tbl_group_products_entry where p_pro_id='$pro_id' order by id DESC ";
$result = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
    
/*		if(mysqli_num_rows($result) >= 0)
		{        */
			while($rows_sub[] = mysqli_fetch_object($result));
			{            
//			$Prodcut_Price=$rows_sub->Prodcut_Price;
			}
	//	}
		return $rows_sub;

}


function product_type_class_name($product_type_class_id)
{
$sqlState = "select product_type_class_name from tbl_product_type_class_master where product_type_class_id = '$product_type_class_id' and deleteflag = 'active'";
$rsState  = mysqli_query($GLOBALS["___mysqli_ston"],  $sqlState);
$rowState = mysqli_fetch_object($rsState);
$product_type_class_name	  = $rowState->product_type_class_name;
return $product_type_class_name;
}


function product_type_class_id($id)

{   

$sql = "select product_type_class_id from tbl_products where pro_id = '$id' ";

$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);

$row = mysqli_fetch_object($rs);

$product_type_class_id	  = $row->product_type_class_id;

return $product_type_class_id;

}


function OrderItemsInfo_Delivery_Challan_BOM_Items($pcode)
{
$symbol		= $this->currencySymbol(1);
$currency1 	= $symbol[0];
$curValue 	= $symbol[1];
$totalCost  = 0;
echo "<table width='100%' border='0' cellpadding='5' cellspacing='2' class='bor_opt' style='border-collapse:collapse;'>";
//echo "<tr class='pagehead'><td colspan='11' class='pad'>Item(s) Information </td></tr>";
echo "<tr class='head'>";
echo "<td width='5%' align='center'>SR.No.</td>";
//echo "<td width='15%' '>Item Code</td>";
echo "<td >Product Description </td>";//added by rumit on 22july 2019 
//echo "<td width='15%' >Product Serial No. </td>";
echo "<td width='5%'>Qty.</td>";
echo "<td width='5%'>UOM</td>";
echo "<td width='10%' class='tblBorder_invoice_bottom'>Remarks</td>
</tr>";
//		echo "<tr><td height='1px' colspan='11' class='tblBorder_invoice_bottom'>hf</td></tr>"; //blank line
$sqlOrderPro = "select * from tbl_order_product where order_id ='$pcode' order by order_pros_id desc";
$rsOrderPro	 = mysqli_query($GLOBALS["___mysqli_ston"],  $sqlOrderPro);
$h=0;
while($rowOrderPro = mysqli_fetch_object($rsOrderPro))
{
$h++;
echo "<tr class='text'>";
echo "<td align='center'>".$h."</td>";
//echo "<td valign='top' >";
$OrderProID	 = $rowOrderPro->order_pros_id;
$proID	 	 = $rowOrderPro->pro_id;
$groupID 	 = $rowOrderPro->group_id;
/*$sqlAttr = "select * from tbl_group where group_id='$groupID'";
$rsAttr  = mysqli_query($GLOBALS["___mysqli_ston"],  $sqlAttr);

$i  	 = -1;

$k  	 = 0;

$cb 	 = 0;

$tx		 = 0;

$ta		 = 0;

if($cb != 0)

{

}

if($tx!=0)

{

}

if($ta != 0)

{

}*/
echo "<td valign='top' >".$this->customer_pro_desc_title($rowOrderPro->pro_model,$pcode).'<br>';
echo '<b>Item Type:</b>'.$this->product_type_class_name($this->product_type_class_id($proID)).'<br>
<b>Item Code:</b>'.$this->pro_text_ordered($proID,$pcode).'<br>
<b>Product S. No.: </b>'.$rowOrderPro->barcode;
"</td>";
//echo "<td valign='top'>".$rowOrderPro->barcode." </td>";
echo "<td valign='top'> $rowOrderPro->pro_quantity </td>";
echo "<td valign='top'> No.</td>";
echo "<td valign='top'> No Remark</td>";
echo "</tr>";
echo "<tr><td style='border-right:1px solid #fff; text-align:center'></td><td colspan='4'>";
$bom_items=$this->product_BOM_items($proID);
array_pop($bom_items);// removes last element of an array - because last array element is empty - added by Rumit on 23-Jun-2021
$bom_ctr=count($bom_items);
if($bom_ctr>0)
{
echo "<table border='0' width='100%' cellpadding='0' cellspacing='0' class='bor_opt' style='border-collapse:collapse;' >";
echo "<tr>
<td width='5%'><strong >Item Type</strong></td>
<td width='5%'><strong>UPC </strong></td>
<td ><strong>Item Name</strong></td>
<td width='5%'><strong>Qty</strong></td>
<td width='6%'><strong>UOM</strong></td>
<td width='10%'><strong>Remarks</strong></td>";

echo "</tr>";
for($i=0; $i< $bom_ctr; $i++ )
{
echo"<tr>";
/*if($this->product_type_class_id($bom_items[$i]->pro_id)>0)
{*/
echo"<td width='5%'>";
echo $this->product_type_class_name($this->product_type_class_id($bom_items[$i]->pro_id))."</td>";	
echo "<td width='5%'>".$bom_pro_id=$this->pro_upc_code($bom_items[$i]->pro_id)."</td>";
echo "<td >".$bom_pro_id=$this->product_name($bom_items[$i]->pro_id)."</td>";
echo "<td width='5%'>".$bom_pro_id=$bom_items[$i]->tbl_qty."</td>";
echo "<td width='6%'>No</td>";
echo "<td width='10%'>No Remark</td>";
//}
echo "</tr>";
}
echo "</table>";
}



echo "</td></tr>";

//			echo "<tr bgcolor='#F6F6F6'><td colspan='11' height='2'></td></tr>";

}

echo "</table>";

}


function vendor_payment_id_by_payment_term_name($vendor_payment_terms_name)
{

$sql = "select vendor_payment_terms_id from tbl_vendor_payment_terms_master where vendor_payment_terms_name = '$vendor_payment_terms_name' and deleteflag = 'active' and vendor_payment_terms_status='active'";

$rs  = mysqli_query($GLOBALS["___mysqli_ston"],$sql);
$row = mysqli_fetch_object($rs);
$vendor_payment_terms_id	  = $row->vendor_payment_terms_id;

return $vendor_payment_terms_id;
}



//added by rumit on 20-Jul-2021 fro BOM items in uto PO for BOM Items
function product_BOM_pro_id($pro_id)
{

echo "BOM function Query".$sql	= "select  pro_id  from tbl_group_products_entry where p_pro_id='$pro_id' order by id DESC ";
$result = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
    
		/*if(mysqli_num_rows($result) >= 0)
		{        
			while($rows_sub = mysqli_fetch_object($result))
			{
			"<br>PROID :".$rows_sub_pro_id[]=$rows_sub->pro_id;
			}
		}*/
		$str = '';
while($row = mysqli_fetch_array($result)){
   $str .= $row['pro_id'].",";
}
$data = rtrim($str, ",");
//echo $data;

		return $data;

}




//added on 19aug-2021 for PO approval emails 
/////////////////////////////////////////////////////////////////////////////////////////////
function po_approval_email_config($valueType)
{
$sql_mail = "select * from tbl_po_email_configuration where deleteflag='active'";
$rs_mail  =  mysqli_query($GLOBALS["___mysqli_ston"],$sql_mail);
if(mysqli_num_rows($rs_mail)>0)
{
$row_mail = mysqli_fetch_object($rs_mail);
if($valueType == "admin")
{
return $row_mail->admin_email;
}

else if($valueType == "import_po_email")
{
return $row_mail->import_po_email;
}
else if($valueType == "import_po_email_2")
{
return $row_mail->import_po_email_2;
}
else if($valueType == "export_po_email")
{
return $row_mail->export_po_email;
}
else if($valueType == "export_po_email_2")
{
return $row_mail->export_po_email_2;
}

else if($valueType == "sales_head_email")
{
return $row_mail->sales_head_email;
}

else

{

return "error";

}

}

else 

{
	return "error";

}	

}


function vendor_purchase_type($VID)
{
$sql = "select purchase_type from vendor_master where ID = '$VID' and deleteflag = 'active' and status='active'";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
$purchase_type	  = $row->purchase_type;
return $purchase_type;
}


function vendor_price_basis($VID)
{
$sql = "select Price_Basis from vendor_master where ID = '$VID' and deleteflag = 'active' and status='active'";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
$Price_Basis	  = $row->Price_Basis;
return $Price_Basis;
}

function vendor_po_status($po_id)
{
$sql = "select po_type from vendor_po_order where ID = '$po_id' ";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
$po_type	  = $row->po_type;
return $po_type;
}


function vendor_id_from_po_id($po_id)
{
$sql = "select Sup_Ref from vendor_po_final where PO_ID = '$po_id' ";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],$sql);
$row = mysqli_fetch_object($rs);
$vendor_id_from_po_id	  = $row->Sup_Ref;

return $vendor_id_from_po_id;
}


function supply_delivery_terms_name($ref_source)
{
$sql = "select supply_order_delivery_terms_name from tbl_supply_order_delivery_terms_master where supply_order_delivery_terms_id = '$ref_source' and deleteflag = 'active' and supply_order_delivery_terms_status='active'";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
$supply_delivery_terms_name	  = $row->supply_order_delivery_terms_name;
/*	if($supply_order_delivery_terms_name=='')
{
$supply_order_delivery_terms_name="N/A";
}*/
return $supply_delivery_terms_name;
}//created on 16Sept2021


function po_awb_invoice_path($po_id)
{
$sql = "select I_img2,I_img3 from vendor_po_invoice where po_id = '$po_id' ";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],$sql);
$row = mysqli_fetch_assoc($rs);
$awb_path[]	  = $row;

return $awb_path;
}







//auto po for DP added on 05-oct-2021
function auto_po_for_do($do_order_id)
{

echo "<br>".$sql_invoice			= "SELECT * FROM tble_invoice WHERE o_id ='$do_order_id'";  //exit;
$rs_invoice_qty	= mysqli_query($GLOBALS["___mysqli_ston"], $sql_invoice);
echo "<br>NUM ROWS:====".$invoice_num_rows=$this->getCountRow($rs_invoice_qty); //exit;
if($invoice_num_rows==0) //lessthan one
{
echo "<br>".$sql_reorder_qty			= "SELECT o.orders_id, o.orders_status,tdp.ItemCode,tdp.Quantity
FROM tbl_order o 
INNER JOIN tbl_delivery_order tdo 
ON o.orders_id=tdo.O_Id
INNER JOIN tbl_do_products tdp 
ON o.orders_id=tdp.OID 
where tdp.OID=$do_order_id";  //exit;
$rs_reorder_qty	= mysqli_query($GLOBALS["___mysqli_ston"], $sql_reorder_qty);

while($row_reorder_qty	= mysqli_fetch_object($rs_reorder_qty))
{
echo  "<br>PRO ID -----:".$this->product_entry_desc_bymodel($row_reorder_qty->ItemCode);
echo  "<br>ware_house_stockssssss:".$ware_house_stock	=$this->product_stock($this->product_entry_desc_bymodel($row_reorder_qty->ItemCode));//$row_reorder_qty->ware_house_stock;
echo  "<br>Do Quantity--reorder_qty:".$reorder_qty		=$row_reorder_qty->Quantity;//$row_reorder_qty->reorder_qty;
if($ware_house_stock <= $reorder_qty)
{
echo   "<br>Model :".$model_no[]						=$row_reorder_qty->ItemCode;//$row_reorder_qty->model_no;
echo   "<br>PROID :".$reorder_qty_pro_id[]				=$this->product_entry_desc_bymodel($row_reorder_qty->ItemCode);//$row_reorder_qty->pro_id;
echo   "<br>ware_house_stockkkkk:".$ware_house_stocks[] =$ware_house_stock;//$row_reorder_qty->ware_house_stock;
echo   "<br>reorder_qty:".$reorder_qtys[]				=$row_reorder_qty->Quantity;//$row_reorder_qty->reorder_qty;
//echo "<br>ware_house_stock :".$ware_house_stock[]=$ware_house_stock;
//echo "<br>reorder_qty :".$reorder_qty[]=$reorder_qty;
 }


//print_r($reorder_qty_pro_id);    //exit;
$pro_count=count($reorder_qty_pro_id);
//exit;
//print_r($reorder_qty_pro_id[$i]);  //exit;
//echo "aaaaaaaaaa";
//$new_ven_ids = array();

/*if($ware_house_stock < $reorder_qty)//ordered quantity if warehouse stock less than ordered quantity
{*/
for($i=0; $i <$pro_count;$i++)
		{
$get_pro_id					= $reorder_qty_pro_id[$i];
$get_VPI					= $this->po_product_vendor_by_model_no($model_no[$i]); 
$new_ven_ids[] 				= $this->po_product_vendor_by_model_no($model_no[$i]);
$get_pro_id_entry			= $this->get_pro_id_entry_by_pro_id($reorder_qty_pro_id[$i]);
$get_pro_upc_code			= $this->pro_upc_code($reorder_qty_pro_id[$i]);
$get_ware_house_stock		= $ware_house_stocks[$i];
// "<br>reorder_qty ".$get_reorder_qty		=$reorder_qtys[$i];
$get_reorder_qty			= $reorder_qtys[$i]-$ware_house_stocks[$i];

//exit;
//echo  "<br>Vendor:".$get_VPIimplode					= implode(",",$this->po_product_vendor($reorder_qty_pro_id[$i]));//exit;
if($get_VPI>0)
{
$vendor_array[$get_VPI][]=array("VPI" =>$get_VPI,
"get_pro_id_entry" =>$get_pro_id_entry,
"get_pro_upc_code" =>$get_pro_upc_code,
"get_ware_house_stock" =>$get_ware_house_stock,
"get_reorder_qty" =>$get_reorder_qty,
"get_pro_id" =>$get_pro_id);
}
		}



//}
//exit;
//print_r($new_ven_ids);
//exit;
$new_vendor_ids				=implode(",",$new_ven_ids);
$explode_new_vendor_ids		=explode(",",$new_vendor_ids);

//print_r($explode_new_vendor_ids);
echo "CTR------------".count($vendor_array);

$final_vendors=array_reverse(array_unique($explode_new_vendor_ids));
print_r($vendor_array);
//exit;

	for($j=0;$j< count($final_vendors) ;$j++)
	{
			echo "<br>-------final_vendors---------".$final_vendors[$j];
print_r($vendor_array[$final_vendors[$j]]);
$dataArraypo["VPI"]				= $final_vendors[$j];//$VPI;
$dataArraypo["Confirm_Purchase"]= "inactive";//$VPI;
$dataArraypo["po_type"]= "5"; //draft-1, 0- normal , 5-do order for filter
$dataArraypo["flag"]= "4"; //draft-1, 0- normal , for filter
$dataArraypo["order_id"]= $do_order_id; //draft-1, 0- normal , for filter
	
$result 						= $this->insertRecord_auto('vendor_po_order',$dataArraypo);
echo "<br>PO ID generatedsssss:  ".$o_id							= mysqli_insert_id($GLOBALS["___mysqli_ston"]);
if(count($vendor_array[$final_vendors[$j]])>0)
{

for($k=0;$k< count($vendor_array[$final_vendors[$j]]) ;$k++)
		{
		//print_r($vendor_array[$final_vendors[$j]][$k]);			
//echo "VPI:::: ".$vendor_array[$final_vendors[$j]][$k]["VPI"];
		  "Vendor:".$get_VPI					=$vendor_array[$final_vendors[$j]][$k]["VPI"];
		  "<br>pro ID".$get_pro_id				=$vendor_array[$final_vendors[$j]][$k]["get_pro_id"];
		  "<br>pro ID entry".$get_pro_id_entry	=$vendor_array[$final_vendors[$j]][$k]["get_pro_id_entry"];
		  "<br>UPC_code ".$get_pro_upc_code		=$vendor_array[$final_vendors[$j]][$k]["get_pro_upc_code"];
		  "<br>UPC_code ".$get_reorder_qty		=$vendor_array[$final_vendors[$j]][$k]["get_reorder_qty"];

			$dataArray["Vendor_List"]			= $vendor_array[$final_vendors[$j]][$k]["VPI"];//$VPI;
			$dataArray["O_ID"]					= $o_id;
			$dataArray["VPI"]					= $vendor_array[$final_vendors[$j]][$k]["VPI"];
			$dataArray["Product_Name"]			= addslashes($this->product_name($get_pro_id));//addslashes($_REQUEST["Product_Name"][$i]);
			$dataArray["pro_id_entry"]			= $vendor_array[$final_vendors[$j]][$k]["get_pro_id_entry"];
			$dataArray["pro_id"]				= $vendor_array[$final_vendors[$j]][$k]["get_pro_id"];
			$dataArray["upc_code"]				= $vendor_array[$final_vendors[$j]][$k]["get_pro_upc_code"];
			$dataArray["hsn_code"]				= addslashes($this->ProHsn_code($vendor_array[$final_vendors[$j]][$k]["get_pro_id"])); //exit;//$hsn_code;
			$result = $this->insertRecord_auto('vendor_po_item',$dataArray); //exit;
/********************		//save data in vendor po final table***********************************/
$buyer="B28, Okhla Industrial Area,
Phase - 1,
New Delhi - 110020
Tel: +91-11-41860000
Fax: +91-11-41860066
Email: sales@stanlay.com
GST: 07AAACA0859J1ZQ ";
$consignee="ASIAN CONTEC LIMITED
B28, Okhla Industrial Area,
Phase - 1,
New Delhi - 110020";
$notify="Name: Piyush 
Tel: 011-41860000
Email : cord@stanlay.com";

echo $sql_product_vendor_price="select C_Name, AddressName, Contact_1, Contact_2, Country, Number,Payment_Terms, Fax,gst_no, Email from vendor_master where ID='".$vendor_array[$final_vendors[$j]][$k]["VPI"]."'";
		$rs_product_vendor_price=mysqli_query($GLOBALS["___mysqli_ston"], $sql_product_vendor_price);
		$row_product_vendor_price=mysqli_fetch_object($rs_product_vendor_price);

$exporter=$row_product_vendor_price->C_Name.",\n"
.$row_product_vendor_price->AddressName.",\n"
.$row_product_vendor_price->Country.",\n"
.'Contact: '.$row_product_vendor_price->Contact_1.","
//.$row_product_vendor_price->Contact_2.",\n"
.'Tel: #'.$row_product_vendor_price->Number.",\n"
.'Mobile:'.$row_product_vendor_price->Fax.",\n"
.'Email:'. $row_product_vendor_price->Email.",\n"
.'GST no: '.$row_product_vendor_price->gst_no;

echo "<br>Payment Terms:::: ".$payment_terms_id=$this->vendor_payment_id_by_payment_term_name($row_product_vendor_price->Payment_Terms);
		$dataArray_po["PO_ID"] 				= $o_id;
		$dataArray_po["buyer"] 				= addslashes($buyer);
		$dataArray_po["Date"] 				= date("Y-m-d");
		$dataArray_po["E_Date"] 			= "Immediate";//$_REQUEST['Delivery'];//$_REQUEST['EDA'];
		$dataArray_po["Sup_Ref"] 			= addslashes($vendor_array[$final_vendors[$j]][$k]["VPI"]);
		$dataArray_po["Destination"] 		= "New Delhi";
		$dataArray_po["Dispatch"] 			= "By Air Freight";//addslashes($_REQUEST['Dispatch']);
		$dataArray_po["Consignee"] 			= addslashes($consignee);
		$dataArray_po["Notify"] 			= addslashes($notify);
		$dataArray_po["Exporter"] 			= addslashes($exporter);
		$dataArray_po["Term_Delivery"] 		= "Please Ship through Our UPS A/c 3R23X3";
		$dataArray_po["Payment_Terms"] 		= $payment_terms_id;//"1";//addslashes($_REQUEST['Payment_Terms']);
		$dataArray_po["Delivery"] 			= date("Y-m-d");//addslashes($_REQUEST['Delivery']);
		$dataArray_po["Freight"] 			= "Please Ship through UPS";//addslashes($_REQUEST['Freight']);
		$dataArray_po["Warranty"] 			= "1 year against manufacturing defect";//addslashes($_REQUEST['Warranty']);
		$dataArray_po["ORDER_Acknowledgement"] = "Please send a signed scanned copy of this order as SOA for our records";//addslashes($_REQUEST['ORDER_Acknowledgement']);
		
		$dataArray_po["Discount"] 			= "0";//$_REQUEST['Discount'];
		$dataArray_po["Packing_Charge"] 	= "0.00";//$_REQUEST['Packing_Charge'];
		$dataArray_po["Excise_Duty"] 		= "0";//$_REQUEST['Excise_Duty'];
		$dataArray_po["Education_Cess"] 	= "0";//$_REQUEST['Education_Cess'];
		$dataArray_po["HSEC"] 				= "0";//$_REQUEST['HSEC'];
		$dataArray_po["freight_type"] 		= "Extra";//$_REQUEST['freight_type'];
		$dataArray_po["tax_type"] 			= "IGST";//$_REQUEST['tax_type'];
		$dataArray_po["Freight_Value"] 		= "0.00";//$_REQUEST['Freight_Value'];
		$dataArray_po["Tax_Value"] 			= "18";//$_REQUEST['Tax_Value'];
		$dataArray_po["Tax_C_Form"]="No";
		$dataArray_po["Handling_Value"] 	= "0";//$_REQUEST['Handling_Value'];
		$dataArray_po["Bank_Value"] 		= "0";//$_REQUEST['Bank_Value'];
		$dataArray_po["Product_Desc"] 		= $this->po_product_list_desc_by_pro_id($vendor_array[$final_vendors[$j]][$k]["get_pro_id"]);//$_REQUEST['Product_Desc'];//addslashes($arr[$j]);
	if($vendor_array[$final_vendors[$j]][$k]["get_reorder_qty"]>0)
	{
		$dataArray_po["Prodcut_Qty"] 		= $vendor_array[$final_vendors[$j]][$k]["get_reorder_qty"];//$_REQUEST['Prodcut_Qty'];
	}
	else
	{
		$dataArray_po["Prodcut_Qty"] 		= "1";//$_REQUEST['Prodcut_Qty'];
	}
//			$dataArray_po["Prodcut_Qty"] 	= "1";//$_REQUEST['Prodcut_Qty'];
			$dataArray_po["Prodcut_Price"] 	= $this->po_product_price_by_pro_id($vendor_array[$final_vendors[$j]][$k]["get_pro_id"]);//$_REQUEST['Prodcut_Price'];
			$dataArray_po["Model_No"] 		= $this->po_acl_item_code_by_pro_id($vendor_array[$final_vendors[$j]][$k]["get_pro_id"]);
			$dataArray_po["Flag"] 			= $this->vendor_master_currency($vendor_array[$final_vendors[$j]][$k]["VPI"]);//$_REQUEST['Flag'][0];
			$dataArray_po["upc_code"]		= $vendor_array[$final_vendors[$j]][$k]["get_pro_upc_code"];
			$dataArray_po["hsn_code"]		= addslashes($this->ProHsn_code($vendor_array[$final_vendors[$j]][$k]["get_pro_id"])); //exit;//$hsn_code;$upc_code;
			$dataArray_po["Vendor_Item_Code"]= $this->po_product_vendor_item_code($vendor_array[$final_vendors[$j]][$k]["get_pro_id"]);//$Vendor_Item_Code; //exit;//$hsn_code;
			$dataArray_po["CF_S_NO"]		= "";//$this->po_product_vendor_item_code($get_pro_id);//$Vendor_Item_Code; //exit;//$hsn_code;
			$dataArray_po["CF_NO"]			= "";//$this->po_product_vendor_item_code($get_pro_id);//$Vendor_Item_Code; //exit;//$hsn_code;
			$dataArray_po["CF_Date"]		= "";//$this->po_product_vendor_item_code($get_pro_id);//$Vendor_Item_Code; //exit;//$hsn_code;
			$dataArray_po["CF_Value"]		= "";//$this->po_product_vendor_item_code($get_pro_id);//$Vendor_Item_Code; //exit;//$hsn_code;
			$dataArray_po["CF_Pay_Ins"]		= "";//$this->po_product_vendor_item_code($get_pro_id);//$Vendor_Item_Code; //exit;//$hsn_code;
			
			$result    =  $this->insertRecord_auto('vendor_po_final', $dataArray_po); 
		}
		$dataArraypoe["flag"]= "1"; //draft-1, 0- normal , for filter
	
//		$resultoe 				= $this->editRecord('vendor_po_order',$dataArraypoe);
		$resultoe    =  $this->editRecord('vendor_po_order', $dataArraypoe, "ID", $o_id);
echo 		$row_del   =  "DELETE from vendor_po_order where flag='4'";//$this->editRecord('vendor_po_order', $dataArraypoe, "ID", $o_id);
		$resultdel=mysqli_query($GLOBALS["___mysqli_ston"],  $row_del);
}
	}?>
<!--</table>-->
    <?php }
}

//print_r($get_VPIimplode);
//exit;
}


function product_category_by_edited_enq_id($ID)
{
echo $sql = "select product_category from tbl_web_enq_edit where order_id = '$ID' and deleteflag = 'active'  ";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
$product_category	  = $row->product_category;
return $product_category;
}

function order_customers_name($orders_id)
{
if(is_numeric($ID))
	{
		$sqlCountry = "select customers_name from tbl_order where orders_id = '$orders_id' and deleteflag = 'active'";
		$rsCountry  	= mysqli_query($GLOBALS["___mysqli_ston"], $sqlCountry);
		$rowCountry 	= mysqli_fetch_object($rsCountry);
		$customers_name		= $rowCountry->customers_name;
		
	}
		return ucfirst($customers_name);
}

function Get_DO_PO_path($ID) {
$sql = "select PO_path from tbl_delivery_order where O_Id = '$ID'";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
$PO_path	  = $row->PO_path;
return $PO_path;
}

function TES_Total_Target($tes_id) {
$sql_tes_total	= "select sum(sub_total) as tes_total from tbl_tes where tes_id =".$tes_id." ";
	$rs_tes_total		= mysqli_query($GLOBALS["___mysqli_ston"],$sql_tes_total);
	$data_tes_row_total = mysqli_fetch_object($rs_tes_total); 
$TES_Total=$data_tes_row_total->tes_total;

return $TES_Total;
}





//function for get quarterly value  of an acc manager for Target summary

function qtr_target_achived1($acc_manager=0,$qtr_start_date_show,$qtr_end_date_show,$enq_source_search=0)
{
if($enq_source_search!='' && $enq_source_search!='0')
	{
		$enq_source_search_search=" AND  ref_source='$enq_source_search'";
	}

if($acc_manager!='' && $acc_manager!='0')
	{
	//$orders_status='Pending';
	$acc_manager_search=" and order_by='$acc_manager'";
//	$acc_manager_search_follow_up="and o.order_by=$acc_manager";
	}
	
    "<br><br>".$sql = "SELECT  
invoice_id, I_date,
delivery_challan_id,
customers_id,
D_Order_Date,
SUM(Quantity * Price) as total_price
from target_summary
where 
  orders_status IN('Confirmed','Order Closed')
$acc_manager_search
AND ( I_date BETWEEN '$qtr_start_date_show' AND '$qtr_end_date_show' ) 
$enq_source_search_search
$hvc_search_filter  

order by orders_id desc";	
$rs = mysqli_query($GLOBALS["___mysqli_ston"],$sql);

if(mysqli_num_rows($rs)>0)
	{
  $row = mysqli_fetch_object($rs);
	$q1_target=$row->total_price;
	}
return $q1_target;
}



//view

function qtr_target_achived($acc_manager=0,$qtr_start_date_show,$qtr_end_date_show,$enq_source_search=0)
{
if($enq_source_search!='' && $enq_source_search!='0')
	{
		$enq_source_search_search=" AND  l.ref_source='$enq_source_search'";
	}

if($acc_manager!='' && $acc_manager!='0')
	{
	//$orders_status='Pending';
	$acc_manager_search=" and o.order_by='$acc_manager'";
//	$acc_manager_search_follow_up="and o.order_by=$acc_manager";
	}
	
"<br><br>".$sql = "SELECT 
 o.Price_type, 
tble_invoice.id, tble_invoice.I_date,
tbl_delivery_challan.id,
o.Price_type,
o.total_order_cost, o.customers_id,
tdo.D_Order_Date,
SUM(tdp.Quantity * tdp.Price) as total_price
from tbl_order o ,
tbl_delivery_order tdo ,
tbl_do_products tdp ,
tbl_admin a,
tbl_lead l, 
tbl_delivery_challan,
tble_invoice
where 
o.orders_id=tdp.OID 
and o.order_by=a.admin_id 
and l.id=o.lead_id  
and tdo.O_Id = tdp.OID  
and tdo.O_Id = tbl_delivery_challan.O_Id 
AND tble_invoice.o_id=tdo.O_Id 
and o.orders_status IN('Confirmed','Order Closed')
$acc_manager_search
AND ( tble_invoice.I_date BETWEEN '$qtr_start_date_show' AND '$qtr_end_date_show' ) 
$enq_source_search_search
$hvc_search_filter  
order by o.orders_id desc";	
$rs = mysqli_query($GLOBALS["___mysqli_ston"],$sql);

if(mysqli_num_rows($rs)>0)
	{
  $row = mysqli_fetch_object($rs);
	$q1_target=$row->total_price;
	}
return $q1_target;
}

//function for get quarterly value  of an acc manager for Target summary
function target_by_enq_source($acc_manager=0,$qtr_start_date_show,$qtr_end_date_show)
{
if($acc_manager!='' && $acc_manager!='0')
	{
	//$orders_status='Pending';
	$acc_manager_search=" and o.order_by='$acc_manager'";
//	$acc_manager_search_follow_up="and o.order_by=$acc_manager";
	}
//enq_source_abbrv	
$sql = "SELECT
tble_invoice.id, tble_invoice.I_date,  
tbl_delivery_challan.id,
l.ref_source, 
o.total_order_cost, 
o.customers_id,
tdo.D_Order_Date,
SUM(tdp.Quantity * tdp.Price) as total_price
from tbl_order o ,
tbl_delivery_order tdo ,
tbl_do_products tdp ,
tbl_admin a,
tbl_lead l, 
tbl_delivery_challan,
tble_invoice

where 
o.orders_id=tdp.OID and 
o.order_by=a.admin_id 
and l.id=o.lead_id  
and  tdo.O_Id = tdp.OID
and tdo.O_Id = tbl_delivery_challan.O_Id   
AND tble_invoice.o_id=tdo.O_Id 
and o.orders_status IN('Confirmed','Order Closed')
$acc_manager_search
AND ( tble_invoice.I_date BETWEEN '$qtr_start_date_show' AND '$qtr_end_date_show' ) 
group by l.ref_source 
$hvc_search_filter  
order by total_price desc";	
$rs = mysqli_query($GLOBALS["___mysqli_ston"],$sql);

  while($row[] = mysqli_fetch_object($rs))
  {
	$target_by_enq_source=$row;
  }
return $target_by_enq_source;
}


//function for get quarterly value  of an acc manager for Target summary

function top_selling_products_tes_summary($acc_manager=0,$qtr_start_date_show,$qtr_end_date_show)
{


if($acc_manager!='' && $acc_manager!='0')
	{
	$acc_manager_search=" and tdo.Prepared_by='$acc_manager' ";
	}
$sql = "SELECT
tbl_delivery_challan.id, 
o.orders_id,
top.pro_id, 
top.pro_model,
tdo.Prepared_by,
o.orders_id, 
tdp.ItemCode,
tdp.Description,
tdp.Quantity,
tdp.Price, 
tdo.D_Order_Date 
from tbl_delivery_order tdo , tbl_do_products tdp ,tbl_order o ,tbl_order_product top, 
tbl_delivery_challan
where 
o.orders_id=top.order_id 
AND o.orders_id=tdp.OID  
and tdp.OID=tdo.O_Id
and tdo.O_Id = tbl_delivery_challan.O_Id  
and o.orders_status IN('Confirmed','Order Closed')
$acc_manager_search
AND ( tdo.D_Order_Date BETWEEN '$qtr_start_date_show' AND '$qtr_end_date_show' )
Group By tdp.ItemCode ORDER BY `tdp`.`Quantity` DESC";	
$rs = mysqli_query($GLOBALS["___mysqli_ston"],$sql);
  while($row[] = mysqli_fetch_object($rs))
  {
	$top_selling_products_tes_summary=$row;
  }
return $top_selling_products_tes_summary;
}


//products requiring attention
function products_requiring_attention_less_target_sale($acc_manager=0,$qtr_start_date_show,$qtr_end_date_show)
{
if($acc_manager!='' && $acc_manager!='0')
{
	$acc_manager_search=" and tdo.Prepared_by='$acc_manager' ";
//	$acc_manager_search_follow_up="and o.order_by=$acc_manager";
	}
 $sql="SELECT tes_id, comp_name, pro_id, pro_name, quantity from tbl_tes tes WHERE tes.pro_id NOT IN ( SELECT tes.pro_id from tbl_delivery_order tdo , tbl_do_products tdp ,tbl_order o ,tbl_order_product top ,tbl_tes tes where o.orders_id=top.order_id AND o.orders_id=tdp.OID and tdp.OID=tdo.O_Id and top.pro_id=tes.pro_id $acc_manager_search
AND ( tdo.D_Order_Date BETWEEN '$qtr_start_date_show' AND '$qtr_end_date_show' ) 
 HAVING sum(tes.quantity) < sum(tdp.Quantity) ORDER BY `tdp`.`Quantity` DESC) ";
$rs = mysqli_query($GLOBALS["___mysqli_ston"],$sql);
  while($row[] = mysqli_fetch_object($rs))
  {
	$top_selling_products_tes_summary=$row;
  }
return $top_selling_products_tes_summary;
}



function products_quantity_sold($acc_manager=0,$pro_id,$qtr_start_date_show,$qtr_end_date_show )
{
if($acc_manager!='' && $acc_manager!='0')
{
	$acc_manager_search=" and tdo.Prepared_by='$acc_manager' ";
//	$acc_manager_search_follow_up="and o.order_by=$acc_manager";
	} 

$sql="SELECT tbl_delivery_challan.id,(top.pro_quantity) as tot_qty from tbl_delivery_order tdo , tbl_do_products tdp ,tbl_order o ,tbl_order_product top ,tbl_tes tes , 
tbl_delivery_challan where o.orders_id=top.order_id 
                                                   AND o.orders_id=tdo.O_Id
                                                   and tdo.DO_ID=tdp.ID 
                                                   and top.pro_id=tes.pro_id
												   and tdo.O_Id = tbl_delivery_challan.O_Id  
												    and top.pro_id='$pro_id'
													and o.orders_status IN('Confirmed','Order Closed')
AND ( tdo.D_Order_Date BETWEEN '$qtr_start_date_show' AND '$qtr_end_date_show' and top.pro_id='$pro_id' ) 
GROUP BY top.pro_id 
ORDER BY `tdp`.`Quantity` DESC ";

$rs = mysqli_query($GLOBALS["___mysqli_ston"],$sql);
$row = mysqli_fetch_object($rs);
if($row->tot_qty!='')
{
	
	$pro_quantity	  = $row->tot_qty;
}
else
{
		$pro_quantity	  = 0;
}

return $pro_quantity;
}


//added on 20thjan 2022 to get product name/dec by model id
function product_tes_target_value($tes_id,$pro_id)
{
$sql = "SELECT pro_name, sum(quantity) as pro_total_qty, price, sum(sub_total) as pro_sub_total FROM `tbl_tes` where tes_id='$tes_id' and pro_id= '$pro_id' ";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
if($row->pro_sub_total!=NULL)
{
$pro_sub_total	  = $row->pro_sub_total;
}
else
{
	$pro_sub_total	  = "N/A";
}
return $pro_sub_total;
}


function top_selling_products_tes_summary2($acc_manager=0,$qtr_start_date_show,$qtr_end_date_show)
{
if($acc_manager!='' && $acc_manager!='0')
	{
	//$orders_status='Pending';
	$acc_manager_search=" and tdo.Prepared_by='$acc_manager' ";
//	$acc_manager_search_follow_up="and o.order_by=$acc_manager";
	}

$sql = "SELECT 
tbl_delivery_challan.id,
tdo.D_Order_Date,tdp.Description, tdp.ItemCode,  SUM(tdp.Quantity) as tot_qty, tdp.Price, tdp.OID
from tbl_delivery_order tdo, tbl_do_products tdp , tbl_delivery_challan
where 
tdo.O_Id=tdp.OID
and tdp.OID=tdo.O_Id 
and tdo.O_Id = tbl_delivery_challan.O_Id  
$acc_manager_search

AND ( tdo.D_Order_Date BETWEEN '$qtr_start_date_show' AND '$qtr_end_date_show' )
GROUP BY tdp.ItemCode
ORDER BY SUM(tdp.Quantity *  tdp.Price) DESC";	
$rs = mysqli_query($GLOBALS["___mysqli_ston"],$sql);
  while($row[] = mysqli_fetch_object($rs))
  {
	$top_selling_products_tes_summary=$row;
  }
return $top_selling_products_tes_summary;
}

function product_tes_target_quantity($tes_id,$pro_id)
{
$sql = "SELECT pro_name, sum(quantity) as pro_total_qty, price, sum(sub_total) as pro_sub_total FROM `tbl_tes` where tes_id='$tes_id' and pro_id= '$pro_id' ";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
if($row->pro_total_qty!=NULL)
{
$pro_total_qty	  = $row->pro_total_qty;
}
else
{
	$pro_total_qty	  = "N/A";
}
return $pro_total_qty;
}

function pro_id_by_itemcode($model_no,$order_id)
{
$sql = "select pro_id from tbl_order_product where pro_model = '$model_no' and order_id ='$order_id' ";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
$pro_id	  = $row->pro_id;
return $pro_id;
}

//get TES id from account manager ID & its financial Year added on 1-feb-2022
function get_tes_id($acc_manager=0,$financial_year=1)
{
$sql = "select ID from tbl_tes_manager where account_manager = '$acc_manager' and financial_year ='$financial_year' and status='active' and deleteflag='active' ";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
$tes_id	  = $row->ID;
return $tes_id;
}

function invoice_counts($acc_manager=0,$qtr_start_date_show,$qtr_end_date_show)
{
if($acc_manager!='' && $acc_manager!='0')
	{
	//$orders_status='Pending';
	$acc_manager_search=" and o.order_by='$acc_manager' ";
//	$acc_manager_search_follow_up="and o.order_by=$acc_manager";
	}
   "<br>".$sql = 
"SELECT count(distinct  o.orders_id) as no_of_invoices, tble_invoice.I_date, tdo.D_Order_Date, tdp.Description from tbl_order o , tbl_delivery_order tdo , tbl_do_products tdp , tbl_admin a, tbl_lead l, tbl_delivery_challan, tble_invoice where o.orders_id=tdp.OID and o.order_by=a.admin_id and l.id=o.lead_id and tdo.O_Id = tdp.OID and tdo.O_Id = tbl_delivery_challan.O_Id AND tble_invoice.o_id=tdo.O_Id and o.orders_status IN('Confirmed','Order Closed') $acc_manager_search AND ( tble_invoice.I_date BETWEEN '$qtr_start_date_show' AND '$qtr_end_date_show' ) ORDER BY `tdo`.`D_Order_Date` ASC";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
$no_of_invoices	  = $row->no_of_invoices;
return $no_of_invoices;
}


function task_events_count($comp_id=0,$acc_manager=0,$evttxt='0' )
{
if($acc_manager!='' && $acc_manager!='0')
	{
	//$orders_status='Pending';
	$acc_manager_search=" and account_manager='$acc_manager' ";
//	$acc_manager_search_follow_up="and o.order_by=$acc_manager";
	}
	
	
	if($comp_id!='' && $comp_id!='0')
	{
	//$orders_status='Pending';
	$comp_id_search=" and customer='$comp_id' ";
//	$acc_manager_search_follow_up="and o.order_by=$acc_manager";
	}
	
	if($evttxt!='' && $evttxt!='0')
	{
	//$orders_status='Pending';
	$evttxt_search=" and evttxt='$evttxt' ";
//	$acc_manager_search_follow_up="and o.order_by=$acc_manager";
	}
	
	
"<br>".$sql = 
"SELECT count(distinct  id) as no_of_events from events where  1=1 $acc_manager_search 
$comp_id_search $evttxt_search ";
$rs  = mysqli_query($GLOBALS["___mysqli_ston"],  $sql);
$row = mysqli_fetch_object($rs);
$no_of_events	  = $row->no_of_events;
return $no_of_events;
}

/////////////////////ends//////////
}
$s = new myclass();
$s->conn();
?>