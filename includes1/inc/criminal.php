<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td class="next_content"><h2>About the Drink Wheel</h2>
        <p>The Intoximeters Inc. "Drink Wheel"1 is a form that you can fill out. Upon completion we will instantly compute your estimated blood/breath alcohol concentration ("BAC") based on the information that you have provided and return that estimate to you. It is presented as a public service to Intoximeters web site visitors. Its primary purpose is to provide useful information about the responsible use of alcohol.</p>
      <h2>Why is it called a "Drink Wheel"?</h2>
      <p>We call it the "Drink Wheel" because it is based on various paper and cardboard BAC calculators that are given out in alcohol awareness programs, some of which are in the form of a wheel that you can spin around to calculate your estimated BAC based on what and how much you have had to drink.</p>
	  <form method="POST" action="http://www.intox.com/wheel/drinkwheelresults.asp" target="_blank">
      <div class="bgColor">
          
				 <table width="100%" border="0" cellspacing="0" cellpadding="5">
				 
				 
  <tr>
    <td align="center" valign="top" class="txt1">DUI Drink Wheel</td>
  </tr>
  <tr>
    <td align="center" valign="top">On-Line BrAC Calculator</td>
  </tr>
   <tr> 
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>I have had <input type="text" value="1"  name="Number" maxlength="3" class="textfieldSm" /> 
	
	<select name="DrinkType" size="1" class="selectList">
       <option selected value="US beer(s)">US beer(s)</option>
       <option value="Light beer(s)">Light beer(s)</option>
       <option value="Reduced alcohol beer(s)">Reduced alcohol beer(s)</option>
       <option value="Pint(s) of beer">Pint(s) of beer</option>
       <option value="Malt liquor(s)">Malt liquor(s)</option>
       <option value="Glass(es) of table wine">Glass(es) of table wine</option>
       <option value="Glass(es) of Champagne">Glass(es) of Champagne</option>
       <option value="Glass(es) of Fortified/Dessert wine">Glass(es) of Fortified/Dessert wine</option>
       <option value="Bloody Mary(s)">Bloody Mary(s)</option>
       <option value="Gin and Tonic(s)">Gin and Tonic(s)</option>
       <option value="Highball(s)">Highball(s)</option>
       <option value="Irish Coffee(s)">Irish Coffee(s)</option>
       <option value="On the Rocks">On the Rocks</option>
       <option value="Pina Colada(s)">Pina Colada(s)</option>
       <option value="Screwdriver(s)">Screwdriver(s)</option>
       <option value="Tom Collins">Tom Collins</option>
       <option value="Whiskey Sour(s)">Whiskey Sour(s)</option>
       <option value="Margarita(s)">Margarita(s)</option>
       <option value="Airline miniature(s)">Airline miniature(s)</option>
       <option value="Gimlet(s)">Gimlet(s)</option>
       <option value="Old-fashioned(s)">Old-fashioned(s)</option>
       <option value="Mint Julep(s)">Mint Julep(s)</option>
       <option value="Black Russian(s)">Black Russian(s)</option>
       <option value="Dry Martini(s)">Dry Martini(s)</option>
       <option value="Manhattan(s)">Manhattan(s)</option>
       <option value="Rob Roy(s)">Rob Roy(s)</option>
       <option value="Double(s) on the Rocks">Double(s) on the Rocks</option>
       <option value="Frozen Daquiri(s)">Frozen Daquiri(s)</option>
      </select>
</td>
  </tr>
  <tr>
    <td>over a period of 
      <input type="text" class="textfieldSm" maxlength="3" name="Hours" value="1" />
      hour(s)<a HREF="http://www.intox.com/credits.asp#Drink Wheel"><sup>2</sup></a>.</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>I am<input type="radio" name="Sex" value="male" />       Male <input  type="radio" name="Sex" value="female" />      Female (Explanation of gender differences in Blood </td>
  </tr>
  <tr>
    <td>Alcohol Concentration)and I weigh
      <input type="text" class="textfieldSm" maxlength="3" name="Weight" value="150" />
      <input name="MassUnits" value="pounds" checked type="radio" />                       
      Pounds        
      <input type="radio" name="MassUnits" value="kilos"/>
      Kilograms and </td>
  </tr>
  <tr>
    <td>I live in <select name="Country" size="1">
       <option selected value="United States">United States</option>
       <option value="Australia">Australia</option>
       <option value="Austria">Austria</option>
       <option value="Belgium">Belgium</option>
       <option value="Canada">Canada</option>
       <option value="Cyprus">Cyprus</option>
       <option value="Finland">Finland</option>
       <option value="France">France</option>
       <option value="Korea">Korea</option>
       <option value="Hong Kong">Hong Kong</option>
       <option value="Netherlands">Netherlands</option>
       <option value="New Zealand">New Zealand</option>
       <option value="Norway">Norway</option>
       <option value="Portugal">Portugal</option>
       <option value="South Africa">South Africa</option>
       <option value="Spain">Spain</option>
       <option value="Sweden">Sweden</option>
       <option value="Taiwan">Taiwan</option>
       <option value="United Kingdom">United Kingdom</option>
       <option value="United States">United States</option>
      </select> (so that the result is displayed in the appropriate units).</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="81%"><input type="image" src="images/btn.gif" /></td>
    <td width="19%"><a href="./cmspage.php?PageDataID=3"><img src="images/reset.gif" border="0" />
    </a></td>
  </tr>
</table></td>
  </tr>
  
</table>

      </div>
	  </form>
      <h2>Disclaimer</h2>
      <p>It would be extremely foolish for us to pretend that our "Drink Wheel" can tell you what your BAC actually is, first because it would open us up to an incredible amount of potential liability and second if it really did work accurately there would be no need for anyone to buy the instruments that we make and sell.</p>
      <p>A person's actual BAC is dependent on many complex factors, including their physical condition (body composition, health etc...) and what they have recently ingested (including food, water, medications and other drugs). This site includes a more detailed discussion of the Pharmacology and Disposition of alcohol in humans. </p>
      <p>The results that are generated are rough estimates of an average healthy person's BAC assuming typical beverage sizes, recipes and alcohol content. The BAC estimates generated by the Drink Wheel should not be used to infer anyone's fitness to work, drive or perform any other task or duty.</p></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
