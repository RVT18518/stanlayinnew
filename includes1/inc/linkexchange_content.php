<?php 
	if($_REQUEST["action"]=="MailSend")
	{
		$to  	 =	$front->fetchGeneral_config("admin");
		$from    =	$_REQUEST["email"];// false mail id 
		$subject = 	"Stanlay: Link Exchange from website";
		$headers = 	"MIME-Version: 1.0\r\n".
					"Content-type: text/html; charset=iso-8859-1\r\n".
					"From: <".$from.">\r\n".
					"Date: ".date("r")."\r\n".
					"Subject: ".$subject."\r\n";
		ob_start();                       // start output buffer 2
		include("includes1/exchangelink_form_format.php");        // fill ob2
		$message  = ob_get_contents();    // read ob2 ("b")
		ob_end_clean();
		//echo $message; exit;
		$result1 = mail($to, $subject, $message, $headers);
		if($result1 == 1)
		{
			$PAGE_PERMALINK	= stripslashes($front->getContentpage('85' ,'permalink'));
			$url			= str_replace(" ","_",$PAGE_PERMALINK)."_"."85.html";
			$front->pageLocation($url);
			// $front->pageLocation("enquiry_thanks.php");
		}
		else
		{
			$PAGE_PERMALINK	= stripslashes($front->getContentpage('87' ,'permalink'));
			$url			= str_replace(" ","_",$PAGE_PERMALINK)."_"."87.html";
			$front->pageLocation($url);
		}
	}
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr><td><h1><?php echo  $front->CmsContent("71", "pages_name"); ?></h1></td></tr>
	<tr><td>&nbsp;</td></tr>
	<tr><td><?php echo stripslashes($front->CmsContent("71", "pages_html_text")); ?></td></tr>
<?php
	if($_REQUEST["action"] == "error")
	{
?>
	<tr><td class="required">Your message could not send. Please try after some time.</td></tr>
	<tr><td>&nbsp;</td></tr>
<?php
	}
?>
	<tr>
		<td>
			<form id="frx1" name="frx1" enctype="multipart/form-data" method="post" action="cmspage.php?PageDataID=71&action=MailSend">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td width="35%" align="right" class="highlight_brdrbtm"><strong><span class="required">*</span>Company Name:</strong></td>
					<td width="65%" class="highlight_brdrbtm"><input name="company" id="company" size="40" maxlength="50" class="inputbox" /></td>
				</tr>
				<tr>
					<td width="35%" align="right" ><strong><span class="required">*</span> Contact Name:</strong></td>
					<td width="65%" ><input name="name" id="name" size="40" maxlength="50" class="inputbox" /></td>
				</tr>
				<tr>
					<td width="35%" align="right" class="highlight_brdrbtm"><strong><span class="required">*</span> Phone:</strong></td>
					<td width="65%" class="highlight_brdrbtm">
						<table cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td><input name="phone" id="phone" size="20" maxlength="20" class="inputbox" /></td>
								<td>&nbsp;<strong>Ext.</strong>&nbsp;&nbsp;<input name="ext" id="ext" size="10" maxlength="20" class="inputbox" /></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td width="35%" align="right"><strong><span class="required">*</span> E-Mail:</strong></td>
					<td width="65%"><input name="email" id="email" size="40" maxlength="50" class="inputbox" /></td>
				</tr>
				<tr>
					<td width="35%" align="right" class="highlight_brdrbtm"><strong><span class="required">*</span> Your URL(Your link Address):</strong></td>
					<td width="65%" class="highlight_brdrbtm"><input name="your_url" id="your_url" size="40" maxlength="50" class="inputbox" /></td>
				</tr>
				<tr>
					<td width="35%" align="right"><strong><span class="required">*</span> Location of our reciprocal link(where our link will be):</strong></td>
					<td width="65%" ><input name="location" id="location" size="40" maxlength="50" class="inputbox" /></td>
				</tr>				
				<tr>
					<td align="right" valign="top" class="highlight_brdrbtm"><strong> Any additional comments:</strong></td>
					<td valign="top" class="highlight_brdrbtm"><textarea name="comments" id="comments" rows="6" cols="50" value="" class="inputbox"></textarea></td>
				</tr>
				
				<tr>
					<td align="right">&nbsp;</td>
					<td>
						<table border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td align="left" width="110"><input type="image" id="submit" name="submit" src="images/button_sendto.gif" alt="" border="0" /></td>
							<td align="left"><input type="reset" value="" class="enquiry_btn" /></td>
						</tr>
						</table>					</td>
				</tr>
			</table>
			</form>
		</td>
	</tr>
</table>
<script language="JavaScript" type="text/javascript">
 var frmvalidator = new Validator("frx1");
 frmvalidator.addValidation("name","req","Please enter Name.");
 frmvalidator.addValidation("company","req","Please enter Company.");
 frmvalidator.addValidation("phone","req","Please enter Phone.");
 frmvalidator.addValidation("email","req","Please enter E-Mail.");
 frmvalidator.addValidation("email","email","Please enter valid E-Mail.");
 frmvalidator.addValidation("your_url","req","Please enter Your URL.");
 frmvalidator.addValidation("location","req","Please enter Location of our reciprocal link.");
</script>