<link href="css/style.css" rel="stylesheet" type="text/css" />

<style type="text/css">
td.hover_effect table tr td {
	border-bottom: 1px solid #e7e7e7
}
td.hover_effect1 table tr td {
	border-bottom: 1px solid #e7e7e7
}
td.hover_effect table tr:hover {
	background-color: #ddfdd1
}
td.hover_effect1 table tr:hover {
	background-color: #fdddd1
}
.malorm-bottom p {
		text-align:left!important;
		width:100%!important;
		text-align:justify!important;
		font-size:13px!important;
		font-weight:normal;
		color:#333
 }
.menuH ul a
{
    padding: 0 10px;
    line-height: 30px;
    font-size: 0.9em;
    font-weight: normal;
    color: #0066CC;
    text-align: left;
}
</style>

<?php

	$admin_role_id	= $s->admin_role_id($_SESSION["AdminLoginID_SET"]);
	$_SESSION['AdminLoginID_SET_1']=$_SESSION['AdminLoginID_SET'];
	if($_REQUEST["ordered_by"]>0)
		{
			//print_r($_REQUEST);
			 $_SESSION['AdminLoginID_SET_1']=$_REQUEST['ordered_by'];
			 
	if($_REQUEST["datevalid_from1"]!= '')
		{
			$fromDate 	  = $_REQUEST["datevalid_from1"]; //exit;
			$datevalid_from = "";
		}
	else
		{
			$fromDate = '';
		}
	if($_REQUEST["datevalid_to1"] != '')
		{
			echo $toDate	  	  = $_REQUEST["datevalid_to1"]; //exit;
			$datevalid_to = "";
		}
	else
		{
			$toDate	= "";
		}
	if($fromDate!="" && $toDate!="")
		$date_filter="o.date_ordered BETWEEN '$fromDate' AND '$toDate' and";
	else
		$date_filter="";
		}

?>

<script type="text/javascript">
	window.onload = function(){
		g_globalObject = new JsDatePick({
			useMode:2,
			target:"datevalid_from1",
			dateFormat:"%Y-%m-%d"
			/*selectedDate:{ This is an example of what the full configuration offers.
				day:5,		 For full documentation about these settings please see the full version of the code.
				month:9,
				year:2006
			},
			yearsRange:[1978,2020],
			limitToToday:false,
			cellColorScheme:"beige",
			dateFormat:"%m-%d-%Y",
			imgPath:"img/",
			weekStartDay:1*/
		});
		
		g_globalObject = new JsDatePick({
			useMode:2,
			target:"datevalid_to1",
			dateFormat:"%Y-%m-%d"
			/*selectedDate:{				This is an example of what the full configuration offers.
				day:5,						For full documentation about these settings please see the full version of the code.
				month:9,
				year:2006
			},
			yearsRange:[1978,2020],
			limitToToday:false,
			cellColorScheme:"beige",
			dateFormat:"%m-%d-%Y",
			imgPath:"img/",
			weekStartDay:1*/
		});

	};
</script>




<div class="page-container">	
   <div class="content">
	   <div class="mother-grid-inner">
           
<!--inner block start here-->
<div class="inner-block" style="padding:10px">
<!--market updates updates-->
	 <div class="market-updates">
			
            <div class="col-md-4 market-update-gd">
				<div class="market-update-block clr-block-1">
					<div class="col-md-8 market-update-left">
						<a href="index.php?pagename=sales_manager"><h3>Sales <br /> Manager</h3>
						<p>Create Offer, Lead & Track your <br />Sales Progress. </p></a>
					</div>
					<div class="col-md-4 market-update-right">
						 <img src="images/newicon/sales.png" width="80" /> 
					</div>
				  <div class="clearfix"> </div>
				</div>
			</div>
			
            
            
			<div class="col-md-4 market-update-gd">
				<div class="market-update-block clr-block-3">
					<div class="col-md-8 market-update-left">
						<a href=""><h3>Warehousing<br /> 
						Manager</h3>
						<p>Update Incoming &amp; Outgoing Stock, Manage Warehouse Stock.</p></a>
					</div>
					<div class="col-md-4 market-update-right">
						 <img src="images/newicon/warehousing.png" width="80" /> 
					</div>
				  <div class="clearfix"> </div>
				</div>
			</div>
            <div class="col-md-4 market-update-gd">
				<div class="market-update-block clr-block-2">
				 <div class="col-md-8 market-update-left">
					<a href="warranty_support"><h3>Warranty <br />Support</h3>
						<p>Service Request Form,  <br /> track service request status.</p></a>
				  </div>
					<div class="col-md-4 market-update-right">
						 <img src="images/newicon/warranty-icon.png" width="80" /> 
					</div>
				  <div class="clearfix"> </div>
				</div>
			</div>
            
		   <div class="clearfix"> </div>
		</div>
        
        
    <div class="market-updates" style="margin-top:40px;">
			
            <div class="col-md-4 market-update-gd">
				<div class="market-update-block clr-block-4">
					<div class="col-md-8 market-update-left">
						<a href="index.php?pagename=sales_manager"><h3>Tendering<br />&nbsp;</h3>
						<p>Mange Tender, Upload Tender Submission, Manage SD/EMD. </p></a>
					</div>
					<div class="col-md-4 market-update-right">
						 <img src="images/newicon/knowledgebank.png" width="80" /> 
					</div>
				  <div class="clearfix"> </div>
				</div>
			</div>
			
            <div class="col-md-4 market-update-gd">
				<div class="market-update-block clr-block-5">
				 <div class="col-md-8 market-update-left">
					<a href="index.php?pagename=kc_home"><h3>Knowledge<br /> Center</h3>
						<p>Search Datasheet, CS, Manuals <br /> & Useful Documents.</p></a>
				  </div>
					<div class="col-md-4 market-update-right">
						 <img src="images/newicon/open-knowledge-icon.png" width="80" /> 
					</div>
				  <div class="clearfix"> </div>
				</div>
			</div>
            
			<div class="col-md-4 market-update-gd">
				<div class="market-update-block clr-block-6">
					<div class="col-md-8 market-update-left">
			<a href="view_stock.php" target="_blank">
            <h3>PQV<br /><br /></h3>
			<p>View Product Stock/Pricing, <br />Product Item Code.</p>
            </a>
					</div>
					<div class="col-md-4 market-update-right">
						 <img src="images/newicon/view-product.png" width="80" /> 
					</div>
				  <div class="clearfix"> </div>
				</div>
			</div>
            
            
		   <div class="clearfix"> </div>
		</div>
        
        
        <div class="market-updates" style="margin-top:40px;">
			
            <div class="col-md-4 market-update-gd">
				<div class="market-update-block clr-block-7">
					<div class="col-md-8 market-update-left">
						<a href="index.php?pagename=sales_manager"><h3>Purchase<br /> Dept</h3>
						<p>Vendor Managment & Purchase <br />Order, Vendor Invoice.</p></a>
					</div>
					<div class="col-md-4 market-update-right">
						 <img src="images/newicon/purchase-icon.png" width="80" /> 
					</div>
				  <div class="clearfix"> </div>
				</div>
			</div>
			
            <div class="col-md-4 market-update-gd">
				<div class="market-update-block clr-block-8">
				 <div class="col-md-8 market-update-left">
					<a href=""><h3>Accounts <br />Payable</h3>
						<p>Vendor Payment Managment. <br /> Customer Payment Followups.</p></a>
				  </div>
					<div class="col-md-4 market-update-right">
						 <img src="images/newicon/cash-payment-icon.png" width="80" /> 
					</div>
				  <div class="clearfix"> </div>
				</div>
			</div>
            
			<div class="col-md-4 market-update-gd">
				<div class="market-update-block clr-block-9">
					<div class="col-md-8 market-update-left">
						<a href=""><h3>Human<br /> Resource</h3>
						<p>Attendance &amp; OD Mgt, Leave <br />Application &amp; Leave Records Mgt.</p></a>
					</div>
					<div class="col-md-4 market-update-right">
						 <img src="images/newicon/Subscribe-HR-Features-For-Finance-Large.png" width="80" /> 
					</div>
				  <div class="clearfix"> </div>
				</div>
			</div>
            
            
            
            <div class="col-md-4 market-update-gd" style="margin-top:40px;">
				<div class="market-update-block clr-block-2">
				 <div class="col-md-8 market-update-left">
					<a href=""><h3>Dispatch <br />Cordination</h3>
						<p>Schedule and Dispatch Works<br />Track &amp; Cordination of Dispatch</p></a>
				  </div>
					<div class="col-md-4 market-update-right">
						 <img src="images/newicon/delivery.png" width="80" /> 
					</div>
				  <div class="clearfix"> </div>
				</div>
			</div>
            
		   <div class="clearfix"> </div>
		</div>
        
        
<!--market updates end here-->
<div class="main-page-charts" style="margin-top:30px">
   <div class="main-page-chart-layer1">
		<div class="col-md-6 chart-layer1-left"> 
			<div class="glocy-chart">
			<div class="span-2c">  
                        <h3 class="tlt">Month Sales Analytics 2016-17</h3>
                        <canvas id="bar" height="300" width="400" style="width: 400px; height: 300px;"></canvas>
                        
						 <?php include('../includes1/monthly_sales.php'); ?>
						 
                    </div> 			  		   			
			</div>
		</div>
		<div class="col-md-6 chart-layer1-right"> 
			<div class="user-marorm">
			
			<div class="malorm-bottom">
				 <h2>News & Alerts</h2>
                 <marquee behavior="alternate"  direction="down" height="317" scrollamount="1" scrolldelay="1" style="overflow:scroll">
                 <?php
            $sql_cat_pro_all	 = "SELECT * from tbl_news order by ID desc limit 0,10";
			$rs_cat_pro_all  	 = mysqli_query($GLOBALS["___mysqli_ston"],$sql_cat_pro_all);
			while($rs_cat_pro_num_all  = mysqli_fetch_object($rs_cat_pro_all)) {
			?>
				 <h4><?php echo $rs_cat_pro_num_all->News_Title; ?></h4>
                 <p>
				 <?php $val=explode(" ",$rs_cat_pro_num_all->News_Details);
				$i=0;
				while($i<=30)
				   {
					   	echo " ".$val[$i];
						$i++;
				   }if(sizeof($val)>30) { echo ". "; }
				    ?>
                    <a href="<?php echo $css_path; ?>news/<?php echo str_replace(" ","-",strtolower($rs_cat_pro_num_all->News_Title)); ?>-<?php echo $rs_cat_pro_num_all->ID; ?>.php" style="font-weight:bold; color:#F00; float:right; padding-right:15px; text-decoration:none" onmouseover="this.color:#333" onmouseout="this.color:#F00">Read More...</a>
                    
                    
                    </p>
                 
                 
     
		

                    <p style="text-align:left!important; width:100%; border-bottom:2px dotted #CCC; padding-bottom:25px; margin-bottom:25px"> </p>
      
      
      <?php } ?>
				</marquee>
			</div>
		   </div>
		</div>
	 <div class="clearfix"> </div>
  </div>
 </div>
<!--mainpage chit-chating-->

<div class="chit-chat-layer1">
	<div class="col-md-12 chit-chat-layer1-left">
               <div class="work-progres">
                            <div class="chit-chat-heading">
                                  Recent Orders
                            </div>
                            <div class="table-responsive">
                                <table class="table table-hover">
                                  <thead>
                                    <tr>
                                      <th>Offer Ref</th>
                                      <th>Customer Name</th>
                                      <th>Contact Person</th>
                                      <th>Offer Value</th>
                                      <th>A/c Manager</th>                                   
                                      <th>Order Status</th>                                   
                                  </tr>
                              </thead>
                              
                              <tbody>
                              <?php include('../includes1/your_opportunities_new.php'); ?>
                              
                              
                      </table>
                  </div>
             </div>
      </div>
      
     <div class="clearfix"> </div>
</div>

</div>
<!--COPY rights end here-->
</div>
</div>
<!--slider menu-->
    
	<div class="clearfix"> </div>
</div>
