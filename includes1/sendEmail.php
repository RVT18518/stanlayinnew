
<link href="../css/style.css" rel="stylesheet" type="text/css" />
<?php 
	$result = $_REQUEST["result"];
	if($_REQUEST['action'] == "EmailMailed")
	{
		$customer = $_REQUEST["customer"];
		$i = 0;
		if($customer=="all")
		{
			$rs_customer  =	$s->getData_without_condition("tbl_customer");
			while($row_customer = mysqli_fetch_object($rs_customer))
			{
				$to		  	  = $row_customer->customers_email_address;
				$from	  	  = $_POST["from_email"];
				$subject 	  = stripslashes($_REQUEST["subject1"]);
				$headers 	  = "MIME-Version: 1.0\r\n".
								"Content-type: text/html; charset=iso-8859-1\r\n".
								"From: <".$from.">\r\n".
								"Date: ".date("r")."\r\n".
								"Subject: ".$subject."\r\n";
				ob_start();                       // start output buffer 2
				include("sendEmailFormat.php");        // fill ob2
				$message  	= ob_get_contents();    // read ob2 ("b")
				ob_end_clean(); 
				//echo $message;	
				$result 	= mail($to, $subject, $message,$headers);
				if($result)
				{
					$i++;
				}
			}
		}
		else
		{
			$to		  	  = $customer;
			$from	  	  = $_POST["from_email"];
			$subject 	  = stripslashes($_REQUEST["subject1"]);
			$headers 	  = "MIME-Version: 1.0\r\n".
							"Content-type: text/html; charset=iso-8859-1\r\n".
							"From: <".$from.">\r\n".
							"Date: ".date("r")."\r\n".
							"Subject: ".$subject."\r\n";
			ob_start();                       // start output buffer 2
			include("sendEmailFormat.php");       // fill ob2
			$message  = ob_get_contents();    // read ob2 ("b")
			ob_end_clean();
			//echo $message;
			$result=mail($to, $subject, $message,$headers);
			if($result)
			{
				$i++;
			}
		}
			$s->pageLocation("index.php?pagename=sendEmail&action=EmaillingDone&result=$i");

	}

?>
<form name="frx1" id="frx1" action="index.php?pagename=sendEmail&action=EmailMailed" method="post" enctype="multipart/form-data">
<table width="100%"  align="center" cellpadding="0" cellspacing="0">

	<tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="24%" class="pageheadTop">Send Email</td>
            <td width="76%" class="headLink"></td>
          </tr>
      </table></td>
    </tr>
	<tr>
      <td class="pHeadLine"></td>
    </tr>
	<tr>
      <td>&nbsp;</td>
    </tr>
<tr>
<td>
<?php
	if($_REQUEST['action']=="EmaillingDone")
	{
		if($result>0)
		{
			echo "<p class='success'>Email Send to $result  Customers Successfully</p><br />";	
		}
		else 
		{
			echo "<p class='error'>Error ! Email Sending Fails</p><br />";	
		}
	}
?>
</td>
</tr>	
	<tr><td>
	<table width="100%" cellpadding="0" cellspacing="0" class="tblBorder">
	<tr>
	  <td colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
	  <td class="pagehead"> Email Details </td>
	</tr>

      </table></td>
	  </tr>
	<tr class="text">
	  <td width="15%"  class="pad" valign="top"> Customers <span class="redstar"> * </span></td>
	<td width="85%"><select name="customer" id="customer" class="inpuTxtSelect" >
	<option value="0"> Select Customer </option>
	<option value="all"> All Customers </option>
	<?php
		$rs_cust =	$s->getData_without_condition("tbl_customer");
		while($row_cust = mysqli_fetch_object($rs_cust))
		{
	?>
<option value="<?php echo $row_cust->customers_email_address;?>"> <?php echo ucfirst($row_cust->customers_salutation).". ".ucfirst($row_cust->customers_firstname)." ".ucfirst($row_cust->customers_lastname)." [\"".$row_cust->customers_email_address."\"]";?></option>
	<?php
		}
	?>
	</select></td></tr>

	<tr class="text">
	  <td class="pad" nowrap valign="top">From Email Address <span class="redstar"> * </span></td>
	  <td><input name="from_email" type="text" class="inpuTxtSelect" size="40" id="from_email" value="<?php echo $s->fetchGeneral_config('info');?>"></td>
	  </tr>
	<tr class="text">
	  <td class="pad" valign="top">Subject<span class="redstar"> *</span> </td>
	  <td><input name="subject1" type="text"  size="75" id="subject1" class="inpuTxtSelect" value=""></td>
	  </tr>
	<tr class="text">
	  <td class="pad" valign="top">Message </td><td>
	  <?php
					//$sBasePath 			  = $_SERVER['PHP_SELF'] ;
					//$sBasePath 			  = substr( $sBasePath, 0, strpos( $sBasePath, "file" ) ) ;
					$sBasePath  = "../fckeditor/";
					$oFCKeditor 		  = new FCKeditor('msz') ;
					$oFCKeditor->BasePath = $sBasePath;
					$oFCKeditor->Value	  =  $content; 
					$oFCKeditor->Width    = '80%' ;
					$oFCKeditor->Height   = '400' ;
					$oFCKeditor->Create() ;
		?>	
	
	  </td></tr>


	<tr class="text"><td class="pad" valign="top"></td><td><input name="save"  type="submit" class="inputton" id="save" value="Send"></td>
	</tr> <tr class="text"><td class="redstar pad" colspan="2"> * Required Fields </td></tr>
	</table>
		</td></tr>
</table>
</form>
<script language="JavaScript" type="text/javascript">
 var frmvalidator = new Validator("frx1");
 frmvalidator.addValidation("customer","dontselect='0'","Please select the customer.");
 frmvalidator.addValidation("from_email","req","Please enter From Email.");
 frmvalidator.addValidation("from_email","email","Please enter right From Email.");
 frmvalidator.addValidation("subject1","req","Please enter  subject ");
 //frmvalidator.addValidation("msz","req","Please enter Message");
 //frmvalidator.addValidation("msz","minlen=10","Please enter Minimum 10 letters");
</script>

