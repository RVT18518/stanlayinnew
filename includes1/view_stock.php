<?php include_once("../includes1/function_lib.php"); include_once("session_check.php");
	$admin_id=$_SESSION["AdminLoginID_SET"]; 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
  <title>Backoffice</title>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="js/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#datepicker" ).datepicker();
  } );

  $( function() {
    $( "#datepicker_to" ).datepicker();
  } );

  </script>
  </head>
  <link href="css1/style.css" rel="stylesheet" type="text/css" />
  </head><?php
	$data_action 			= $_REQUEST["action"];
	$CateID		 			= $_REQUEST["CateID"];
	$pcode		 			= $_REQUEST["pcode"];
	$manufacturers_id 		= $_REQUEST["manufacturers_id"];
	$process_status_search 	= $_REQUEST["process_status"];
	
	
	if(!isset($_GET['pageno']))
	{ 
    	$page = 1; 
	} 
	else 
	{ 
    	$page = $_GET['pageno']; 
	} 
	if(!isset($_GET['records']))
	{ 
    	$max_results = 10000; 
	} 
	else 
	{ 
    	$max_results = $_GET['records']; 
	} 
	$from = (($page * $max_results) - $max_results);  
	
	
	
	
	/*if($_REQUEST["status_search"]=="")
	{
		$status_search 			= "pvt";
	}
	else
	{*/
	$status_search 			= $_REQUEST["status_search"];
//	}
	$temp					=1;
	$app_cat_id				= $_REQUEST["app_cat_id"];
	/*echo "<pre>";
	print_r($_REQUEST);*/
	
/**************************************************************************/		
?>
  <style type="text/css">
.text_red {
	background-color:#eee;
	color:#333;
	border-bottom:1px solid #fff!important;
	border-top:1px solid #666!important;
}
</style>

  <table  width="90%" align="center"  border="0" cellpadding="0"  cellspacing="0" class="pagecontent" bgcolor="#FFFFFF">
    <tr>
      <td ><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
          <td width="36%" class="pageheadTop">Product Stock Manager:</td>
          <td width="64%" class="headLink"></td>
        </tr>
        </table></td>
    </tr>
    <tr>
      <td class="pHeadLine"></td>
    </tr>
    <tr>
      <td valign="top"><!-- td valign="top" width="20%" class="pad"><?php //include("products_left_category.php"); ?></td-->
        
        <table width="100%" border="0" cellpadding="3" cellspacing="0" >
          <tr>
            <td valign="top" colspan="9"><table width="100%" border="0" cellpadding="5" cellspacing="0" class="tblBorder1">
                <tr class='pagehead'>
                <td  valign="middle" class="pad" colspan='7'>Products Warehouse Stock Report:</td>
                <td align="right"  class="headLink" colspan="5"><?php 
	if(isset($_SESSION["ELqueryX"]))
	{
?>
                    <ul>
                    <li><a href="export_product_data.php" >Export to Excel</a></li>
                  </ul>
                    <?php		
	}
	else
	{
?>
                    <ul>
                    <li><a href="index.php?pagename=bestseller_report&error=xlsError" >Export to Excel</a></li>
                  </ul>
                    <?php 
	}
?></td>
              </tr>
                <tr>
                <td colspan="8"><form name="search_filter_stock" id="search_filter_stock" action="view_stock.php?action=search_type" method="post" style="display:inline">
                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                    <tr class="text">
                      <td width="100%"  align="right"> Date from</td>
                      <td><input type='text'  name='datepicker' id='datepicker' value="<?php echo $_REQUEST["datepicker"];?>" /></td>
                      <td nowrap="nowrap">Date to </td>
                      <td><input type='text'  name='datepicker_to' id='datepicker_to' value="<?php echo $_REQUEST["datepicker_to"];?>" /></td>
                      <td><select name="app_cat_id" class="inpuTxt" id="app_cat_id" onchange="this.form.submit()">
                          <option value="">Select Product Group</option>
                          <?php
		$query   	= "deleteflag='active' and application_status='active' order by application_name";
		$rs_appli   = $s->selectwhere('tbl_application',$query);
		while($row_appli = mysqli_fetch_object($rs_appli))
		{
		?>
                          <option  value="<?php echo $row_appli->application_id; ?>"
	   <?php
		   if($row_appli->application_id == $app_cat_id)
		   {
				echo "selected='selected'";
		   }
	   ?> ><?php echo stripslashes($row_appli->application_name); ?></option>
                          <?php } ?>
                        </select></td>
                      <td><select name="status_search" class="inpuTxt" id="status_search" style="width:150px" onchange="this.form.submit()">
                          <option value="">All</option>
                          <option value="Export_USD" <?php if( $status_search =="Export_USD"){ echo "selected='selected'";}?>>Export_USD</option>
                          <option value="govt" <?php if( $status_search =="govt"){ echo "selected='selected'";}?>>Govt</option>
                          <option value="pvt" <?php if( $status_search =="pvt"){ echo "selected='selected'";}?>>Pvt</option>
                        </select></td>
                      <td><input type="submit" value="Search" name="search" id="search" class="inputton" /></td>
                  </form>
                    <form name="frx1" id="frx1" action="view_stock.php?action=search_type" method="post" style="display:inline">
                    <input type="submit" class="inputton" value="View All" name="view_all" />
                  </form></td>
              </tr>
              </table></td>
          </tr>
          <tr class="head">
            <td width="5%" class="pad" align="center">S. No.</td>
            <td width="10%" class="pad">Product Name</td>
            <td width="10%" class="pad">Product Item Code</td>
            <td width="15%" class="pad">List Type</td>
            <td width="10%" class="pad" align="left">Warehouse Stock</td>
            <td width="10%" class="pad" align="left">Product Price</td>
            <td width="10%" class="pad" align="left">Max Discount(%)</td>
            <td width="10%" class="pad" align="left">Last Modified</td>
            <td width="10%" class="pad" align="left">View Old Stock</td>
          </tr>
          <?php
//$search = "pvt";
/*****************************************************************************************************************/	
//	if($_REQUEST["status_search"]!='' || $_REQUEST["app_cat_id"]!='')
	//{
		
		
	 $search = $_REQUEST["status_search"];
	 /*if($search=="")
	 {
		 $search="pvt";
	 }*/
	 $search_c = $_REQUEST["app_cat_id"];
	 $search_from = $_REQUEST["datepicker"];
	 $datepicker_to = $_REQUEST["datepicker_to"];
	 
	 if($search!='')

{
	
	$search_search="and tbl_products_entry.price_list='$search'";
}
	
	
	 if($search_c!='')

{
	$search_c_search=" 	 and tbl_products_entry.app_cat_id='$search_c'";
}
	
	
	if($search_from!='' && $datepicker_to!='')
	{
		
$date_range="AND (date( tbl_products_entry.last_modified ) BETWEEN '$search_from' AND '$datepicker_to')";
	}
	

			$sql = "SELECT 
			tbl_products.pro_id,
			tbl_products.pro_title,
			tbl_products.ware_house_stock,
			tbl_products.pro_max_discount,
			tbl_products_entry.pro_price_entry,
			tbl_products_entry.last_modified,
			tbl_products_entry.price_list,
			tbl_products_entry.model_no
			FROM tbl_products_entry
			left join tbl_products on tbl_products.pro_id=tbl_products_entry.pro_id
			WHERE tbl_products.deleteflag = 'active' AND tbl_products.STATUS = 'active' AND tbl_products_entry.deleteflag = 'active' AND tbl_products_entry.STATUS = 'active' AND tbl_products_entry.price_list!='' $search_search $search_c_search $date_range ORDER BY tbl_products.pro_title ASC LIMIT $from, $max_results";
			
			$rsPro  = mysqli_query($GLOBALS["___mysqli_ston"],$sql);
			
		//$searchRecord="status='active'";
		//$rsPro = $s->getData_withPages('tbl_products',$order_by, $order,$from,$max_results,$searchRecord);
	//}
	
//	    $sql_export = "select * from tbl_comp $searchRecord order by $order_by $order";
//export to excel query
	  $_SESSION["ELqueryX"] = $sql;
	
	if($_REQUEST['view_all']=="View All") {
		
		$sql = "SELECT 
		tbl_products.pro_id,
			tbl_products.pro_title,
			tbl_products.ware_house_stock,
			tbl_products.ware_house_stock,
			tbl_products.pro_max_discount,
			tbl_products_entry.pro_price_entry,
			tbl_products_entry.last_modified,
			tbl_products_entry.price_list,
			tbl_products_entry.model_no
			FROM tbl_products_entry
			left join tbl_products on tbl_products.pro_id=tbl_products_entry.pro_id
			WHERE tbl_products.deleteflag = 'active' AND tbl_products.STATUS = 'active' AND tbl_products_entry.deleteflag = 'active' AND tbl_products_entry.STATUS = 'active' $date_range ORDER BY tbl_products.pro_title,tbl_products_entry.price_list='pvt' ASC";
			
			$rsPro  = mysqli_query($GLOBALS["___mysqli_ston"],$sql);
			
	}
/*****************************************************************************************************************/

//echo $sql;
/*if(mysqli_num_rows($rsPro)<1 && $page > 1)
{
		$s->pageLocation("view_stock.php?pageno=1&orderby=$order_by&order=$order"); 
}*/
//echo mysqli_num_rows($rsPro);
if(mysqli_num_rows($rsPro)>0)
{

	while($rowPro = mysqli_fetch_object($rsPro))
	{
		
			if($rowPro->ware_house_stock<=0)
			  {
				$text_class="text_red";			
			  }
			  else
			   {
				   $text_class="";
			   }
				
?>
          <tr class="text <?php if($i%2==0) { echo 'text1"';  } else { echo 'text2'; } $i++; ?>" onmouseover="bgr_color(this, '#EAB9BA')" onmouseout="bgr_color(this, '')">
            <td align="center" valign="top" class="pad " style="border-bottom:1px solid #eee"><?php echo $temp++; ?></td>
            <td valign="top" class="pad " style="border-bottom:1px solid #eee" ><?php echo $rowPro->pro_title;?></td>
            <td valign="top" class="pad " style="border-bottom:1px solid #eee" ><?php echo $rowPro->model_no;?></td>
            <td align="left" valign="top" class="pad " style="border-bottom:1px solid #eee"><?php echo stripslashes($rowPro->price_list); ?></td>
            <td align="left" valign="top" class="pad " style="border-bottom:1px solid #eee"><?php echo stripslashes($rowPro->ware_house_stock); ?></td>
            <td align="left" valign="top" class="pad " style="border-bottom:1px solid #eee"><?php echo stripslashes($rowPro->pro_price_entry); ?></td>
            <td align="left" valign="top" class="pad " style="border-bottom:1px solid #eee"><?php if($rowPro->price_list=='govt') { echo "0"; } else echo stripslashes($rowPro->pro_max_discount); ?></td>
            <td align="left" valign="top" class="pad " style="border-bottom:1px solid #eee"><?php $last_modified_date=substr($rowPro->last_modified,0,10);
				    if($last_modified_date!='')
					{
				   echo $nice_date = date('d-M-Y', strtotime( $last_modified_date));
					}
					else
					{
						echo "N/A";
					}?></td>
            <td align="left" valign="top" class="pad " style="border-bottom:1px solid #eee"><a href="view_stock_previous.php?pcode=<?php echo $rowPro->pro_id;?>" target="_blank">View Old Stock</a></td>
          </tr>
          <?php
				
		}
?>
          <tr class='head headLink'>
            <td colspan="9" align="right" ><table width="100%" cellpadding="1" cellspacing="0" border="0" style="display:none">
                <tr>
                <td  align="left"><!--<input type="submit" value="Send to Manager"  name="EmailUpload" class="inputton" onclick="return check_del();" />--></td>
                <td  align="right"  style="display:none"><?php	
							
	if($manufacturers_id != "" || $_REQUEST["process_status"]!= "ALL")
		{	
			if($status_search != "ALL")
			{
		 	$product_search3=" and status ='$status_search'";
			}

			$product_search="  $product_search3" ;							
			//$rsPro = $s->getData_withPages('tbl_products',$order_by, $order,$from,$max_results,$product_search);				
		 	$total_pages_sql = "SELECT 
			tbl_products.pro_id,
			tbl_products.pro_title,
			tbl_products.ware_house_stock,
			tbl_products.pro_max_discount,
			tbl_products_entry.pro_price_entry,
			tbl_products_entry.last_modified,
			tbl_products_entry.price_list,
			tbl_products_entry.model_no
			FROM tbl_products_entry
			left join tbl_products on tbl_products.pro_id=tbl_products_entry.pro_id
			WHERE tbl_products.deleteflag = 'active' AND tbl_products.STATUS = 'active' AND tbl_products_entry.deleteflag = 'active' AND tbl_products_entry.STATUS = 'active' AND tbl_products_entry.price_list!='' $search_search $search_c_search $date_range ORDER BY tbl_products.pro_title ASC ";
			$total_page_rs=mysqli_query($GLOBALS["___mysqli_ston"],$total_pages_sql);
			$total_pages=mysqli_num_rows($total_page_rs);
			
		}
		else
		{
			$total_pages = $s->getTotal_pages('tbl_products',$order_by, $order,$max_results,$searchRecord);
		}
			if($page > 1)
			{ 
				$prev = ($page - 1); 
			echo "<ul><li><a href='view_stock.php?action=search_type&status_search=$status_search&orderby=$order_by&order=$order&pageno=$prev&records=$max_results'> Previous</a></li></ul>"; 
			} 
?></td>
                <td width="43"  align="right" style="display:none"><select name="pages_select" onchange="OnSelectPages();">
                    <?php
			for($i = 1; $i <= $total_pages; $i++)
			{ 
?>
                    <option <?php if($page==$i){ echo "selected='selected'";} ?> 
value="view_stock.php?action=search_type&status_search=<?php echo $status_search;?>&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $i;?>&records=<?php echo $max_results;?>"> <?php echo $i;?></option>
                    <?php
			} 
?>
                  </select></td>
                <td width="59"  align="left" style="display:none"><?php		
			if($page < $total_pages)
			{ 
				$next = ($page + 1); 
				echo "<ul><li><a href='view_stock.php?action=search_type&status_search=$status_search&orderby=$order_by&order=$order&pageno=$next&records=$max_results'>Next </a></li></ul>";
			} 
?></td>
              </tr>
              </table></td>
          </tr>
          <tr class='pagehead'>
            <td  valign="middle" class="pad" colspan='7'></td>
            <td align="right">&nbsp;</td>
            <td align="right"><?php /*?>Records View &nbsp;
                    <select name="records" onchange="OnSelect();"  >
                      <option <?php if($max_results==25){ echo "selected='selected'";} ?> 
value="view_stock.php?action=search&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=25">25</option>
                      <option <?php if($max_results==50){ echo "selected='selected'";} ?>
value="view_stock.php?action=search&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=50">50</option>
                      <option <?php if($max_results==100){ echo "selected='selected'";} ?>
value="view_stock.php?action=search&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=100">100</option>
                      <option <?php if($max_results==500){ echo "selected='selected'";} ?>
value="view_stock.php?action=search&orderby=<?php echo $order_by;?>&order=<?php echo $order;?>&pageno=<?php echo $page;?>&records=500">500</option>
                    </select><?php */?></td>
          </tr>
          <tr>
            <td colspan="9" bgcolor="#333333" align="right" style="display:none"><?php  $temp_p=round($top_record_fpo/50)+1;  ?>
              <form name="pading_fpo_frm" method="post">
                <input type="hidden" name="status_search" value="<?php echo $_REQUEST['status_search']; ?>" />
                <input type="hidden" name="app_cat_id" value="<?php echo $_REQUEST['app_cat_id']; ?>" />
                <select name="pading_fpo" onchange="this.form.submit()">
                  <?php for($pageno=1;$pageno<=$temp_p;$pageno++) { ?>
                  <option value="<?php echo $pageno-1; ?>" <?php if($pageno==$_REQUEST['pading_fpo']+1) { echo "selected"; } ?>><?php echo $pageno; ?></option>
                  <?php } ?>
                </select>
              </form></td>
          </tr>
          <?php
	}
	else
	{
?>
          <tr class='text'>
            <td colspan='9' align="center" class="redstar"><?php echo no_record?></td>
          </tr>
          <?php	
	}
?>
        </table></td>
    </tr>
  </table>
  <script type="text/javascript">

	window.onload = function(){

		g_globalObject = new JsDatePick({

			useMode:2,

			target:"follow_up_date",

			dateFormat:"%Y-%m-%d"

			/*selectedDate:{				This is an example of what the full configuration offers.

				day:5,						For full documentation about these settings please see the full version of the code.

				month:9,

				year:2006

			},

			yearsRange:[1978,2020],

			limitToToday:false,

			cellColorScheme:"beige",

			dateFormat:"%m-%d-%Y",

			imgPath:"img/",

			weekStartDay:1*/

		});

		g_globalObject1 = new JsDatePick({

			useMode:2,

			target:"follow_up_date1",

			dateFormat:"%m-%d-%Y"

			/*selectedDate:{				This is an example of what the full configuration offers.

				day:5,						For full documentation about these settings please see the full version of the code.

				month:9,

				year:2006

			},

			yearsRange:[1978,2020],

			limitToToday:false,

			cellColorScheme:"beige",

			dateFormat:"%m-%d-%Y",

			imgPath:"img/",

			weekStartDay:1*/

		});
	};

</script>
</html>