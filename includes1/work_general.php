
<link href="css/style.css" rel="stylesheet" type="text/css" />

<?php 
	$data_action = $_REQUEST["action"];
	$pcode		 = $_REQUEST["pcode"];
	if($data_action=="edit")
	{
		$rs  			 = $s->getData_with_condition('tbl_projects','id',$pcode);
		$row 			 = mysqli_fetch_object($rs);
		$pro_name		 = $row->project_name;
		$url	         = $row->project_url;
		$short_desc		 = stripslashes($row->short_desc);
		$desc			 = $row->long_desc;
		$status	 		 = $row->status;
		$data_action     = "update";
	}
	if($data_action == "add_new")
	{
		$data_action = "insert";
	}	
if($_REQUEST['action']=='update' )
{
		$file_array["project_name"]  	= addslashes($_REQUEST["project_name"]);	
		$file_array["project_url"]  	= addslashes($_REQUEST["pro_url"]);	
		$file_array["short_desc"]  		= addslashes($_REQUEST["sort_desc"]);
		$file_array["long_desc"]  		= addslashes($_REQUEST["description"]);
		$file_array["status"]  			= $_REQUEST["status"];

}
if($_REQUEST['action']=='update')
{
	$result = $s->editRecord('tbl_projects',$file_array,'id',$pcode);
	$s->pageLocation("index.php?pagename=general_work&subpagename=work_general&action=edit&pcode=$pcode");
}
?>
<form name="frx1" id="frx1" action="index.php?pagename=work_general&action=<?php echo $data_action;?>&pcode=<?php echo $pcode;?>" method="post" enctype="multipart/form-data">
<table width="100%"  align="center" cellpadding="0" cellspacing="0">

	
	<tr><td>
	<table width="100%" cellpadding="0" cellspacing="0" class="tblBorder">
	<tr>
	  <td colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
	  <td class="pagehead">  Work Details</td>
	</tr>

      </table></td>
	  </tr>
	<tr class="text">
	  <td width="12%"  class="pad">Project Name <span class="redstar">* </span></td>
	<td width="88%"><input name="project_name" type="text" class="inpuTxt" id="project_name" value="<?php  echo stripslashes($pro_name); ?>" /></td></tr>

	<tr class="text">
	  <td class="pad" valign="top" nowrap>Project Url</td>
	  <td><input  type ="text" name="pro_url" class="inpuTxtText" id="pro_url" value="<?php  echo stripslashes($url); ?>">	     [e.g. - http://www.google.com ]</td>
	</tr>

	<tr class="text">
	  <td class="pad" valign="top" nowrap>Short Description</td>
	  <td><?php
					//$sBasePath = $_SERVER['PHP_SELF'] ;
					//$sBasePath = substr( $sBasePath, 0, strpos( $sBasePath, "file" ) ) ;
					$sBasePath  = "../fckeditor/";
					$oFCKeditor 		  = new FCKeditor('sort_desc') ;
					$oFCKeditor->BasePath = $sBasePath;
					$oFCKeditor->Value		=  stripslashes($short_desc); 
					$oFCKeditor->Width  = '80%' ;
					$oFCKeditor->Height = '200' ;
					$oFCKeditor->Create() ;
				?>	 </td>
	  </tr>
	<tr class="text">
	  <td class="pad" valign="top" nowrap> Description</td>
	  <td><?php
					//$sBasePath = $_SERVER['PHP_SELF'] ;
					//$sBasePath = substr( $sBasePath, 0, strpos( $sBasePath, "file" ) ) ;
					$sBasePath  = "../fckeditor/";
					$oFCKeditor 		  = new FCKeditor('description') ;
					$oFCKeditor->BasePath = $sBasePath;
					$oFCKeditor->Value		=  stripslashes($desc); 
					$oFCKeditor->Width  = '80%' ;
					$oFCKeditor->Height = '400' ;
					$oFCKeditor->Create() ;
				?>	 </td>
	  </tr>

	  <tr class="text"><td class="pad"> Status</td><td><select name="status" class="inpuTxt" id="status">
	<option value="active"   <?php  if( $status == "active"){ echo "selected='selected'";}?> >Active</option>
	<option value="inactive" <?php  if( $status == "inactive"){ echo "selected='selected'";}?>>Inactive</option>
	</select></td></tr>
	<tr class="text"><td class="pad"></td><td><input name="save"  type="submit" class="inputton" id="save" value="Save"></td>
	</tr>
	<tr class="text"><td class="redstar pad" colspan="2"> * Required Fields </td></tr>  
	</table>
		</td></tr>
</table>
</form>
<script language="JavaScript" type="text/javascript">
 var frmvalidator = new Validator("frx1");
 frmvalidator.addValidation("project_name","req","Please enter Project Name");
</script>
