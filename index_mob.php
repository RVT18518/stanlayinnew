<?php include("includes/modulefunction.php");
	  include("includes/function_lib.php");
	  
		$PAGE_TITLE 		= stripslashes($front->fetchGeneral_config("webtitle"));
		$PAGE_DESC		 	= stripslashes($front->fetchGeneral_config("webtitlecont"));
		$PAGE_META_DESC 	= stripslashes($front->fetchGeneral_config("meta_desc"));
		$PAGE_META_CONTENT 	= stripslashes($front->fetchGeneral_config("meta_content"));
		
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1"> 
<title><?php echo $PAGE_TITLE;?></title>
<meta name="description" content="<?php echo $PAGE_META_DESC;?>" />
<meta name="keywords" content="<?php echo $PAGE_META_CONTENT;?>" />
<meta name="Robots" content="index, follow" />
<meta name="googlebot" content="noodp" />
<meta name="Robots" content="all" />
<meta name="revisit-after" content="1 days" />
<meta name="Author" content="www.stanlay.in, YBP" />
<link rel="shortcut icon" href="<?php echo $css_path; ?>favicon.ico" type="image/x-icon" />
<link rel="icon" href="<?php echo $css_path; ?>favicon.ico" type="image/x-icon" />

<!-- Latest compiled and minified CSS -->
<link href="<?php echo $css_path; ?>stylesheet/stanlay_main_style.css" type="text/css" rel="stylesheet" />
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-77333643-1', 'auto');
  ga('send', 'pageview');
</script>
<meta name="google-site-verification" content="AvvrgPzucnt7FTFd-6oRfR9k7KJCLCPXKmGeOeMoKVg" />
</head>
<body>
<div id="wrapper">
  <?php include('header.php'); ?>
  <div id="bottom_main" style="margin-top:10px;"> 
    <!-- MENU START FROM HARE -->
   <?php include('menuboth.php'); ?>
    <!-- MENU END HARE -->
    <div id="sliderFrame">
      <div id="slider">
        <?php
	 $sql_fpt = "select * from tbl_category where parent_id = '0' and sort_order='9'  and deleteflag = 'active' and cate_status='active' order by sort_order asc limit 0,10";
		$rs_fpt  = mysqli_query($con,  $sql_fpt);
		$row_fpt = mysqli_fetch_object($rs_fpt);
			?>
        <a href="#"> <img src="<?php echo $css_path; ?><?php echo $row_fpt->banner_image; ?>" alt="<?php echo 
			str_replace("</p>","",(str_replace("<p>","",$row_fpt->short_desc))); ?>" width="100%" height="380" /> </a>
        <?php
             
             $sql_fpt = "select * from tbl_category where parent_id = '0' and (sort_order between 2 AND 11)  and deleteflag = 'active' and cate_status='active' order by sort_order asc limit 0,10";
		$rs_fpt  = mysqli_query($con,  $sql_fpt);
		while($row_fpt = mysqli_fetch_object($rs_fpt)) {
			?>
        <a href="#"> <img src="<?php echo $css_path; ?><?php echo $row_fpt->banner_image; ?>" alt="<?php echo 
			str_replace("</p>","",(str_replace("<p>","",$row_fpt->short_desc))); ?>"  width="100%"  height="380" /> </a>
        <?php
		}
	 ?>
      </div>
    </div>
  </div>
  <div id="bottom_main">
    <div id="bottom_main_right">
      <div class="category_tab">
        <?php
	 $sql_fpt = "select cate_id, cat_permalink, cate_name from tbl_category where parent_id = '0' and deleteflag = 'active' and cate_status='active' order by sort_order asc limit 0,11";
		$rs_fpt  = mysqli_query($con,  $sql_fpt);
		while($row_fpt = mysqli_fetch_object($rs_fpt)) {
			?>
        <a href="<?php echo $css_path.$row_fpt->cat_permalink;  ?>/" class="tab_button"><?php echo $row_fpt->cate_name;  ?></a>
        <?php
		}
	 
	 ?>
      </div>
      <div class="bottom_middle_text">
        <h1 style="font-size:12px; color:#F00; text-decoration:underline">About Asian Contec Ltd:</h1>
        <div id="sample"><?php echo $PAGE_DESC; ?> </div>
        <span style="float:right; cursor:pointer; color:#F00" onclick="show_text()" id="hide_more" title="Click to more...">[+] More</span> <span style="float:right; cursor:pointer; color:#F00" onclick="show_text1()" id="show_more" title="Click to less...">[-] Less</span> </div>
      <div class="product_display_head">New Products:</div>
      <div class="product_display">
        <?php
			$sql_sub_cat_pro	 = "
			select tbl_products_1.pro_title, tbl_products_1.pro_short_desc, tbl_products_1.pro_id
			from tbl_products_1 
			INNER JOIN tbl_mapnew_pro
			ON tbl_products_1.pro_id=tbl_mapnew_pro.match_pro_id_g2
			where tbl_products_1.cate_id!='0' AND tbl_products_1.deleteflag = 'active' AND tbl_products_1.status='active' order by tbl_mapnew_pro.match_id_g2  desc limit 0,6";
			$rs_sub_cat_pro  	 = mysqli_query($con,  $sql_sub_cat_pro);
			while($rs_cat_pro_num_sub  = mysqli_fetch_object($rs_sub_cat_pro)) {
			
		  ?>
        <div class="product_box_cat">
          <div class="heading_products"><?php echo $rs_cat_pro_num_sub->pro_title; ?></div>
          <div class="product_box_cat_immagine"> <a href="<?php echo $s->Pro_URL($rs_cat_pro_num_sub->pro_id); ?>"> <img src="<?php echo $css_path; ?>uploads/pro_image/small/<?php echo $s->get_PROImages($rs_cat_pro_num_sub->pro_id); ?>" alt="<?php echo $rs_cat_pro_num_sub->pro_title; ?>" width="290" height="277" /> </a> </div>
          <div class="product_box_content">
            <p style="height:70px">
              <?php  $val=explode(" ",$rs_cat_pro_num_sub->pro_short_desc);
				$i=0;
				while($i<=27)
				   {
					   	echo " ".$val[$i];
						$i++;
				   }if(sizeof($val)>29) { echo "..."; }
				    ?>
            </p>
          </div>
          <div class="product_display_box_view_cat"> <a href="<?php echo $s->Pro_URL($rs_cat_pro_num_sub->pro_id); ?>"> <img src="<?php echo $css_path; ?>images/view-more.png" onmouseover="this.src='<?php echo $css_path; ?>images/view-more-h.png'" onmouseout="this.src='<?php echo $css_path; ?>images/view-more.png'" border="0" height="30" width="144" /> </a> </div>
        </div>
        <?php } ?>
        <div class="product_display_head">Recently Viewed Products:</div>
        <?php  $s->recent_viewed(); ?>
      </div>
    </div>
    <?php include('left.php'); ?>
  </div>
</div>
<?php include('footer.php');  ?>
<link href="<?php echo $css_path1; ?>themes/8/js-image-slider.css" rel="stylesheet" type="text/css" />
<script src="<?php echo $css_path1; ?>themes/8/js-image-slider.js" type="text/javascript"></script> 
<link rel="stylesheet" href="<?php echo $css_path1; ?>css/bootstrap.min.css">
<!-- jQuery library -->
<script src="<?php echo $css_path1; ?>js/jquery.min.js"></script>
<!-- Latest compiled JavaScript -->
<script src="<?php echo $css_path1; ?>js/bootstrap.min.js"></script>

</body>
</html>