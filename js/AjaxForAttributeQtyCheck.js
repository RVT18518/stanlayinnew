// JavaScript Document
function GetXmlHttpObject(handler)
{  
	var objXMLHttp=null;   
	if (window.XMLHttpRequest)   
	{       
	objXMLHttp=new XMLHttpRequest();   
	}   
	else if (window.ActiveXObject)   
	{       objXMLHttp=new ActiveXObject("Microsoft.XMLHTTP") ;  
	}   
	return objXMLHttp;
}
function stateChanged()
{   
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")   
	{           
	document.getElementById("txtResult").innerHTML= xmlHttp.responseText;   
	}  
	 else 
	 {           //alert(xmlHttp.status);   
	 }
}// Will populate data based on input
function htmlData(url, qStr,qtyString)
{  
	if (url.length==0)  
	{      
	document.getElementById("txtResult").innerHTML="";      
	return;  
	}  
	xmlHttp=GetXmlHttpObject();   
	if (xmlHttp==null)  
	{      
	 alert ("Browser does not support HTTP Request");      
	 return;  
	}   
	url=url+"?"+qStr+qtyString;   
	
	xmlHttp.onreadystatechange=stateChanged;   
	xmlHttp.open("GET",url,true) ;  
	xmlHttp.send(null);
}
function dbSelectAttr(AttrName, ProTranX, ProID, GropuAttrID)
{
	var AtName; 
	var field, fieldValue,dbquery='',qtyString='';
	if(AttrName != 0)
	{
		AtName = AttrName.split(',');
		for(i=0; AtName[i]!=null; i++)
		{
			field 		= (AtName[i]);
			AttrVal 	= document.getElementById(AtName[i]).value;
			fieldValue 	= AttrVal.split(',');
			dbquery 	= dbquery + field + "=" + fieldValue[0]+" and ";
			qtyString	= qtyString + "&" + field + "=" + AttrVal;
		}
		dbquery 	= dbquery + "pro_group_id=" + ProTranX;
	}
	else
	{
		dbquery 	= dbquery + "=0&pro_group_id=" + ProTranX;	
	}
	qtyString	= qtyString + "&ProID=" + ProID + "&GropuAttrID=" + GropuAttrID + "&ProTranX=" + ProTranX;
	htmlData('AjaxforAttrCheck.php','dbquery='+ dbquery,qtyString);
	
}
