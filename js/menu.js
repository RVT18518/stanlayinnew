// JavaScript Document
function mmLoadMenus() 
{
	if (window.mm_menu_1205180357_0)return;
//Configuration Menu     - ID = 0;
	window.mm_menu_1205180357_0 = new Menu("root",150,23,"",12,"#FFFFFF","#FFFFFF","#58595B","#ED3237","left","middle",2,0,1000,-5,7,true,false,true,5,true,true);
// submenu Option
	mm_menu_1205180357_0.addMenuItem("General&nbsp;Configuration","location='index.php?pagename=general_configuration&action=edit'");
  	mm_menu_1205180357_0.addMenuItem("Company&nbsp;Manager","location='index.php?pagename=company_manager'");
  	mm_menu_1205180357_0.addMenuItem("Employee&nbsp;Manager","location='index.php?pagename=admin_manager'");
	mm_menu_1205180357_0.addMenuItem("Designation&nbsp;Manager","location='index.php?pagename=manage_designation'");
  	mm_menu_1205180357_0.addMenuItem("Product&nbsp;SEO&nbsp;Manager","location='index.php?pagename=manager_product_meta'");
	mm_menu_1205180357_0.addMenuItem("Product&nbsp;Display&nbsp;Manager","location='index.php?pagename=manager_product_sort'");
	mm_menu_1205180357_0.addMenuItem("Application&nbsp;Manager","location='index.php?pagename=manage_application'");
// Submenue property
  	mm_menu_1205180357_0.hideOnMouseOut=true;
  	mm_menu_1205180357_0.bgColor='#000000';
  	mm_menu_1205180357_0.menuBorder=0;
  	mm_menu_1205180357_0.menuLiteBgColor='#0D409A';
  	mm_menu_1205180357_0.menuBorderBgColor='#0D409A';

//Catalog Menu     - ID = 01;
	window.mm_menu_1205180357_01 = new Menu("root", 110, 23,"",12,"#FFFFFF","#FFFFFF","#58595B","#ED3237","left","middle",2,0,1000,-5,7,true,false,true,5,true,true);
// submenu Option
  	mm_menu_1205180357_01.addMenuItem("CMS","location='index.php?pagename=cms_manager'");
	mm_menu_1205180357_01.addMenuItem("News&nbsp;Events","location='index.php?pagename=manage_news_event'");
	//mm_menu_1205180357_01.addMenuItem("Video&nbsp;Manager","location='index.php?pagename=manage_corporate_videos'");
	mm_menu_1205180357_01.addMenuItem("Database&nbsp;Backup","location='index.php?pagename=database_backup'");
	mm_menu_1205180357_01.addMenuItem("Server&nbsp;Info","location='index.php?pagename=serverinfo'");

// Submenue property
  	mm_menu_1205180357_01.hideOnMouseOut=true;
  	mm_menu_1205180357_01.bgColor='#000000';
  	mm_menu_1205180357_01.menuBorder=0;
  	mm_menu_1205180357_01.menuLiteBgColor='#0D409A';
  	mm_menu_1205180357_01.menuBorderBgColor='#0D409A';
	
//Module Menu     - ID = 04;
	window.mm_menu_1205180357_04 = new Menu("root",110,23,"",12,"#FFFFFF","#FFFFFF","#58595B","#ED3237","left","middle",2,0,1000,-5,7,true,false,true,5,true,true);
// submenu Option
  	mm_menu_1205180357_04.addMenuItem("Payment&nbsp;Module","location='index.php?pagename=payment_module'");
  	//mm_menu_1205180357_04.addMenuItem("Shipping&nbsp;Module","location='index.php?pagename=shipping_module'");
//	mm_menu_1205180357_02.addMenuItem("Gift&nbsp;Rap","location='#'");
	
// Submenue property
  	mm_menu_1205180357_04.hideOnMouseOut=true;
  	mm_menu_1205180357_04.bgColor='#000000';
  	mm_menu_1205180357_04.menuBorder=0;
  	mm_menu_1205180357_04.menuLiteBgColor='#0D409A';
  	mm_menu_1205180357_04.menuBorderBgColor='#0D409A';

////My Account    - ID = 08;
	window.mm_menu_1205180357_02 = new Menu("root",120,23,"",12,"#FFFFFF","#FFFFFF","#58595B","#ED3237","left","middle",2,0,1000,-5,7,true,false,true,5,true,true);
// submenu Option
  	mm_menu_1205180357_02.addMenuItem("My&nbsp;Account&nbsp;Details","location='index.php?pagename=my_details_change&action=edit'");
  	mm_menu_1205180357_02.addMenuItem("Change&nbsp;Password","location='index.php?pagename=change_password'");

//// Submenue property
 	mm_menu_1205180357_02.hideOnMouseOut=true;
 	mm_menu_1205180357_02.bgColor='#000000';
  	mm_menu_1205180357_02.menuBorder=0;
  	mm_menu_1205180357_02.menuLiteBgColor='#0D409A';
 	mm_menu_1205180357_02.menuBorderBgColor='#0D409A';

//Product Menu     - ID = 04;
	window.mm_menu_1205180357_05 = new Menu("root",125,23,"",12,"#FFFFFF","#FFFFFF","#58595B","#ED3237","left","middle",2,0,1000,-5,7,true,false,true,5,true,true);
// submenu Option
  	mm_menu_1205180357_05.addMenuItem("Product&nbsp;Manager","location='index.php?pagename=manage_product'");
	mm_menu_1205180357_05.addMenuItem("Product&nbsp;Entry","location='index.php?pagename=manage_product_entry'");
  	mm_menu_1205180357_05.addMenuItem("Category&nbsp;Manager","location='index.php?pagename=manage_category'");
//	mm_menu_1205180357_02.addMenuItem("Gift&nbsp;Rap","location='#'");


mm_menu_1205180357_0.writeMenus();
}