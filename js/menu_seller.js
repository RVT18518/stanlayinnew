// JavaScript Document
function mmLoadMenus() 
{
	if (window.mm_menu_1205180357_0)return;
//Configuration Menu     - ID = 0;
	window.mm_menu_1205180357_0 = new Menu("root",150,23,"",12,"#FFFFFF","#000000","#6B7184","#A1ABC7","left","middle",2,0,1000,-5,7,true,false,true,5,true,true);
// submenu Option
	mm_menu_1205180357_0.addMenuItem("General&nbsp;Configuration","location='index.php?pagename=general_configuration&action=edit'");
  	/*mm_menu_1205180357_0.addMenuItem("Company&nbsp;Details","location='index.php?pagename=company_details&action=edit'");
  	mm_menu_1205180357_0.addMenuItem("Admin&nbsp;Manager","location='index.php?pagename=admin_manager'");
	mm_menu_1205180357_0.addMenuItem("Seller&nbsp;Manager","location='index.php?pagename=manage_seller'");
  	mm_menu_1205180357_0.addMenuItem("Role&nbsp;Manager","location='index.php?pagename=role_manager'");*/
  //	mm_menu_1205180357_0.addMenuItem("Shipping&nbsp;Origin&nbsp;Manager","location='index.php?pagename=shipping_origin_manager'");
	//mm_menu_1205180357_0.addMenuItem("Free&nbsp;Shipping&nbsp;Manager","location='index.php?pagename=free_shipping_manager'");
// Submenue property
  	mm_menu_1205180357_0.hideOnMouseOut=true;
  	mm_menu_1205180357_0.bgColor='#000000';
  	mm_menu_1205180357_0.menuBorder=0;
  	mm_menu_1205180357_0.menuLiteBgColor='#0D409A';
  	mm_menu_1205180357_0.menuBorderBgColor='#0D409A';

//Catalog Menu     - ID = 01;
	window.mm_menu_1205180357_01 = new Menu("root",150,23,"",12,"#FFFFFF","#000000","#6B7184","#A1ABC7","left","middle",2,0,1000,-5,7,true,false,true,5,true,true);
// submenu Option
	
	mm_menu_1205180357_01.addMenuItem("Product&nbsp;Manager ","location='index.php?pagename=manager_product'");
	//mm_menu_1205180357_01.addMenuItem("Category&nbsp;Manager ","location='index.php?pagename=manage_category'");
  	//mm_menu_1205180357_01.addMenuItem("Group&nbsp;Attribute&nbsp;Manager","location='index.php?pagename=manage_group_attribute'");
  	//mm_menu_1205180357_01.addMenuItem("Attribute&nbsp;Manager","location='index.php?pagename=manage_attribute'");
	//mm_menu_1205180357_01.addMenuItem("Coupon&nbsp;Manager","location='index.php?pagename=manage_coupon'");
	mm_menu_1205180357_01.addMenuItem("Special&nbsp;Manager","location='index.php?pagename=manage_product_special&managerTypeCheck=PRO'");
  	//mm_menu_1205180357_01.addMenuItem("Manufacturer&nbsp;Manager","location='index.php?pagename=manage_manu'");
	
   	//mm_menu_1205180357_01.addMenuItem("Banner&nbsp;Manager","location='index.php?pagename=manage_banner'");
	
// Submenue property
  	mm_menu_1205180357_01.hideOnMouseOut=true;
  	mm_menu_1205180357_01.bgColor='#A1ABC7';
  	mm_menu_1205180357_01.menuBorder=0;
  	mm_menu_1205180357_01.menuLiteBgColor='#0D409A';
  	mm_menu_1205180357_01.menuBorderBgColor='#0D409A';

//Module Menu     - ID = 02;
	window.mm_menu_1205180357_02 = new Menu("root",110,23,"",12,"#FFFFFF","#000000","#6B7184","#A1ABC7","left","middle",2,0,1000,-5,7,true,false,true,5,true,true);
// submenu Option
  	mm_menu_1205180357_02.addMenuItem("Payment&nbsp;Module","location='index.php?pagename=payment_module'");
  	//mm_menu_1205180357_02.addMenuItem("Shipping&nbsp;Module","location='index.php?pagename=shipping_module'");
//	mm_menu_1205180357_02.addMenuItem("Gift&nbsp;Rap","location='#'");
	
// Submenue property
  	mm_menu_1205180357_02.hideOnMouseOut=true;
  	mm_menu_1205180357_02.bgColor='#A1ABC7';
  	mm_menu_1205180357_02.menuBorder=0;
  	mm_menu_1205180357_02.menuLiteBgColor='#0D409A';
  	mm_menu_1205180357_02.menuBorderBgColor='#0D409A';
	
//Customers/Order Menu     - ID = 03;
	window.mm_menu_1205180357_03 = new Menu("root",130,23,"",12,"#FFFFFF","#000000","#6B7184","#A1ABC7","left","middle",2,0,1000,-5,7,true,false,true,5,true,true);
// submenu Option
  	//mm_menu_1205180357_03.addMenuItem("Customer&nbsp;Manager","location='index.php?pagename=manage_customer'");//
  	mm_menu_1205180357_03.addMenuItem("Order&nbsp;Manager","location='index.php?pagename=order_manager'");
// Submenue property
  	mm_menu_1205180357_03.hideOnMouseOut=true;
  	mm_menu_1205180357_03.bgColor='#A1ABC7';
  	mm_menu_1205180357_03.menuBorder=0;
  	mm_menu_1205180357_03.menuLiteBgColor='#0D409A';
  	mm_menu_1205180357_03.menuBorderBgColor='#0D409A';

//Location/Order Menu     - ID = 04;
	window.mm_menu_1205180357_04 = 	new Menu("root",110,23,"",12,"#FFFFFF","#000000","#6B7184","#A1ABC7","left","middle",2,0,1000,-5,7,true,false,true,5,true,true);
// submenu Option
  	mm_menu_1205180357_04.addMenuItem("Country&nbsp;Manager","location='index.php?pagename=manage_country'");
	//mm_menu_1205180357_04.addMenuItem("Zones","location='index.php?pagename=zones_manager'");
  	//mm_menu_1205180357_04.addMenuItem("Tax&nbsp;Zone","location='index.php?pagename=tax_zone_manager'");
  	//mm_menu_1205180357_04.addMenuItem("Tax&nbsp;Class","location='index.php?pagename=tax_class_manager'");
  	//mm_menu_1205180357_04.addMenuItem("Tax&nbsp;Rate","location='index.php?pagename=tax_rate_manager'");
// Submenue property
  	mm_menu_1205180357_04.hideOnMouseOut=true;
  	mm_menu_1205180357_04.bgColor='#A1ABC7';
  	mm_menu_1205180357_04.menuBorder=0;
  	mm_menu_1205180357_04.menuLiteBgColor='#0D409A';
  	mm_menu_1205180357_04.menuBorderBgColor='#0D409A';

//Localization     - ID = 05;
	window.mm_menu_1205180357_05 = new Menu("root",120,23,"",12,"#FFFFFF","#000000","#6B7184","#A1ABC7","left","middle",2,0,1000,-5,7,true,false,true,5,true,true);
// submenu Option
  	//mm_menu_1205180357_05.addMenuItem("Languages","location='index.php?pagename=manager_languages'");
  	mm_menu_1205180357_05.addMenuItem("Currencies","location='index.php?pagename=currency'");
  	//mm_menu_1205180357_05.addMenuItem("Order&nbsp;Status&nbsp;Master","location='index.php?pagename=order_status_manager'");
// Submenue property
  	mm_menu_1205180357_05.hideOnMouseOut=true;
  	mm_menu_1205180357_05.bgColor='#A1ABC7';
  	mm_menu_1205180357_05.menuBorder=0;
  	mm_menu_1205180357_05.menuLiteBgColor='#0D409A';
  	mm_menu_1205180357_05.menuBorderBgColor='#0D409A';

//Reports    - ID = 06;
	window.mm_menu_1205180357_06 = new Menu("root",170,23,"",12,"#FFFFFF","#000000","#6B7184","#A1ABC7","left","middle",2,0,1000,-5,7,true,false,true,5,true,true);
// submenu Option
  	mm_menu_1205180357_06.addMenuItem("Order&nbsp;Report","location='index.php?pagename=order_report'");
  	//mm_menu_1205180357_06.addMenuItem("Product&nbsp;Report","location='index.php?pagename=product_report'");
	//mm_menu_1205180357_06.addMenuItem("Customer&nbsp;Report","location='index.php?pagename=customer_Report'");
  	mm_menu_1205180357_06.addMenuItem("Stock&nbsp;Report","location='index.php?pagename=stock_report'");
  	/*mm_menu_1205180357_06.addMenuItem("Coupon&nbsp;Report","location='index.php?pagename=coupon_report'");
	mm_menu_1205180357_06.addMenuItem("Best&nbsp;Seller","location='index.php?pagename=bestseller_report'");
	mm_menu_1205180357_06.addMenuItem("Product&nbsp;Reviews&nbsp;Report","location='index.php?pagename=product_review_report'");
	mm_menu_1205180357_06.addMenuItem("Customers&nbsp;Reviews&nbsp;Report","location='index.php?pagename=customers_review_report'");*/
// Submenue property
  	mm_menu_1205180357_06.hideOnMouseOut=true;
  	mm_menu_1205180357_06.bgColor='#A1ABC7';
  	mm_menu_1205180357_06.menuBorder=0;
  	mm_menu_1205180357_06.menuLiteBgColor='#0D409A';
  	mm_menu_1205180357_06.menuBorderBgColor='#0D409A';

//Tools    - ID = 07;
	window.mm_menu_1205180357_07 = new Menu("root",120,23,"",12,"#FFFFFF","#000000","#6B7184","#A1ABC7","left","middle",2,0,1000,-5,7,true,false,true,5,true,true);
// submenu Option
	mm_menu_1205180357_07.addMenuItem("Coupon&nbsp;Manager","location='index.php?pagename=manage_coupon'");
  	//mm_menu_1205180357_07.addMenuItem("CMS","location='index.php?pagename=cms_manager'");
  	//mm_menu_1205180357_07.addMenuItem("Database&nbsp;Backup","location='index.php?pagename=database_backup'");
	//mm_menu_1205180357_07.addMenuItem("Newsletter&nbsp;Users","location='index.php?pagename=NewsLetter_Subscriber_Manager'");
	//mm_menu_1205180357_07.addMenuItem("Newsletter&nbsp;Manager","location='index.php?pagename=manage_newsletter'");
	//mm_menu_1205180357_07.addMenuItem("Policy&nbsp;Manager","location='index.php?pagename=manage_policy'");
	//mm_menu_1205180357_07.addMenuItem("Who's&nbsp;Online","location='index.php?pagename=#'");//manage_website_page
	//mm_menu_1205180357_07.addMenuItem("Website&nbsp;PHP&nbsp;Pages","location='index.php?pagename=manage_website_page'");
	//mm_menu_1205180357_07.addMenuItem("Server&nbsp;Info","location='index.php?pagename=serverinfo'");
	//mm_menu_1205180357_07.addMenuItem("Send&nbsp;Email","location='index.php?pagename=sendEmail'");
  //	mm_menu_1205180357_07.addMenuItem("Newsletter&nbsp;Manager","location='#'");

// Submenue property
  	mm_menu_1205180357_07.hideOnMouseOut=true;
  	mm_menu_1205180357_07.bgColor='#A1ABC7';
  	mm_menu_1205180357_07.menuBorder=0;
  	mm_menu_1205180357_07.menuLiteBgColor='#0D409A';
  	mm_menu_1205180357_07.menuBorderBgColor='#0D409A';
	
//My Account    - ID = 08;
	window.mm_menu_1205180357_08 = new Menu("root",120,23,"",12,"#FFFFFF","#000000","#6B7184","#A1ABC7","left","middle",2,0,1000,-5,7,true,false,true,5,true,true);
// submenu Option
  	mm_menu_1205180357_08.addMenuItem("My&nbsp;Account&nbsp;Details","location='index.php?pagename=my_details_change&action=edit'");
  	mm_menu_1205180357_08.addMenuItem("Change&nbsp;Password","location='index.php?pagename=change_password'");

// Submenue property
  	mm_menu_1205180357_08.hideOnMouseOut=true;
  	mm_menu_1205180357_08.bgColor='#A1ABC7';
  	mm_menu_1205180357_08.menuBorder=0;
  	mm_menu_1205180357_08.menuLiteBgColor='#0D409A';
  	mm_menu_1205180357_08.menuBorderBgColor='#0D409A';	

mm_menu_1205180357_0.writeMenus();
}