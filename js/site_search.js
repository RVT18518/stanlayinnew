/*
	README - Important
	
	The form action right now is set to a semi-absolute path assuming that reflects how my local web environment is set up.  This is NOT how the
	pages will be set up in production.
	
	When the site is moved to production, edit this file to change the form action to something more absolute:
	i.e.	/search/search_results.asp
*/

// JavaScript Document
inc_site_search_js =
'<table width="160" border="0" cellspacing="0" cellpadding="0" class="sitesearch"><tr><td width="10" height="10" valign="top"><img src="img/search_top_lft_corner.gif" width="10" height="10" /></td><td width="140" height="10" valign="top"></td><td width="10" height="10" valign="top"><img src="img/search_top_rgt_corner.gif" width="10" height="10" /></td></tr><tr><td valign="top">&nbsp;</td><td valign="top"><span class="sitesearchheader">Site Search</span></td><td valign="top">&nbsp;</td></tr>'
+ '<form action="search/search_results.asp" method="post" name="searchBox" id="searchBox">'
+ '<tr><td valign="top">&nbsp;</td><td valign="top">'
+ '<input type="text" name="q" class="sitesearchbox" /></td>'
+ '<input type="hidden" name="fs" value="Y" /></td>'
+ '<td valign="top">&nbsp;</td></tr>'
+ '<tr><td valign="top">&nbsp;</td><td align="right" valign="top"><input name="Search" type="submit" name="sa" value="Search" class="searchbutton" /></td>'
+ '<td valign="top">&nbsp;</td></tr><tr><td><img src="img/search_bot_lft_corner.gif" width="10" height="10" /></td><td bgcolor="#e6e6e6"></td><td><img src="img/search_bot_rgt_corner.gif" width="10" height="10" /></td>'
+ '</form>'
//+ '<script type="text/javascript" src="http://www.google.com/coop/cse/brand?form=searchbox_005405679198301218471%3Auxhrq0s9vsm&lang=en"></script>'
+ '</tr></table>';
// end_var_declaration
document.write(inc_site_search_js);