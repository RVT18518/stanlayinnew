<!DOCTYPE html>
<html lang="en">
<head>
        <title>Live Demo - Multi-select Dropdown List with Checkbox using jQuery</title>
              <link rel="stylesheet" type="text/css" href="multiselect/jquery.multiselect.css"/>
<style type="text/css">
ul,li { margin:0; padding:0; list-style:none;}
.label { color:#000; font-size:16px;}
</style>
</head>
<body>
        

<div class="demo-title"><h4>DEMO BY <span class="one">CODEX</span><span class="two">WORLD</span>: Multi-select Dropdown List with Checkbox using jQuery</h4></div>
        <div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="">
                <h2>jQuery MultiSelect Basic Uses</h2>
                <select name="langOpt[]" multiple id="langOpt">
                <option value="C++">C++</option>
                <option value="C#">C#</option>
                <option value="Java">Java</option>
                <option value="Objective-C">Objective-C</option>
                <option value="JavaScript">JavaScript</option>
                <option value="Perl">Perl</option>
                <option value="PHP">PHP</option>
                <option value="Ruby on Rails">Ruby on Rails</option>
                <option value="Android">Android</option>
                <option value="12">Asian adminiOS</option>
                <option value="HTML">HTML</option>
                <option value="XML">XML</option>
                </select>
                
                <h2>jQuery MultiSelect With Search Option</h2>
                <select name="langOpt2[]" multiple id="langOpt2">
                <option value="C++">C++</option>
                <option value="C#">C#</option>
                <option value="Java">Java</option>
                <option value="Objective-C">Objective-C</option>
                <option value="JavaScript">JavaScript</option>
                <option value="Perl">Perl</option>
                <option value="PHP">PHP</option>
                <option value="Ruby on Rails">Ruby on Rails</option>
                <option value="Android">Android</option>
                <option value="iOS">iOS</option>
                <option value="HTML">HTML</option>
                <option value="XML">XML</option>
                </select>
                
                <h2>jQuery MultiSelect With Select All Option</h2>
                <select name="langOpt3[]" multiple id="langOpt3">
                <option value="C++">C++</option>
                <option value="C#">C#</option>
                <option value="Java">Java</option>
                <option value="Objective-C">Objective-C</option>
                <option value="JavaScript">JavaScript</option>
                <option value="Perl">Perl</option>
                <option value="PHP">PHP</option>
                <option value="Ruby on Rails">Ruby on Rails</option>
                <option value="Android">Android</option>
                <option value="iOS">iOS</option>
                <option value="HTML">HTML</option>
                <option value="XML">XML</option>
                </select>
                
                <h2>jQuery MultiSelect With Optgroup</h2>
                <select name="langOptgroup[]" multiple id="langOptgroup">
                    <optgroup label="Programming Languages">
                        <option value="C++ / C#">C++ / C#</option>
                        <option value="Java">Java</option>
                        <option value="Objective-C">Objective-C</option>
                    </optgroup>
                    <optgroup label="Client-side scripting Languages">
                        <option value="JavaScript">JavaScript</option>
                    </optgroup>
                    <optgroup label="Server-side scripting Languages">
                        <option value="Perl">Perl</option>
                        <option value="PHP">PHP</option>
                        <option value="Ruby on Rails">Ruby on Rails</option>
                    </optgroup>
                    <optgroup label="Mobile Platforms">
                        <option value="Android">Android</option>
                        <option value="iOS">iOS</option>
                    </optgroup>
                    <optgroup label="Document Markup Languages">
                        <option value="HTML">HTML</option>
                        <option value="XML">XML</option>
                    </optgroup>
                </select>
            </div>
    	</div>
  	</div>
</div>
    	
        	<!-- JavaScript -->
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
     	
        <!-- Place this tag in your head or just before your close body tag. -->
        <!--<script src="https://apis.google.com/js/platform.js" async defer></script>-->
    	<script src="multiselect/jquery.multiselect.js"></script>
<script>
$('#langOpt').multiselect({
    columns: 1,
    selectedOptions: 'PHP',
    placeholder: 'Select Languages'
});

$('#langOpt2').multiselect({
    columns: 1,
    placeholder: 'Select Languages',
    search: true
});

$('#langOpt3').multiselect({
    columns: 1,
    placeholder: 'Select Languages',
    search: true,
    selectAll: true
});

$('#langOptgroup').multiselect({
    columns: 4,
    placeholder: 'Select Languages',
    search: true,
    selectAll: true
});
</script>
</body>
</html>