<section class="news-hp animate-box" data-animate-effect="fadeInUp animated-fast">
  <div class="container clearfix">
    <div class="row">
      <div class="head"><?php echo $heading_section7 = stripslashes($front->section_heading("heading_section7"));?><!--News At Stanlay--></div>
      <div class="col-md-5">
        <ul>
          <?php
			$sql_news_featured	 = "
			select * from tbl_news order by ID desc limit 0,1";
			$rs_news_featured  	 = mysqli_query($GLOBALS["___mysqli_ston"],$sql_news_featured);
			while($row_news_featured  = mysqli_fetch_object($rs_news_featured)) {
				$feat_image=$row_news_featured->image_path;
				if($feat_image=='')
				{
					$feat_image='images/news-image-holder.jpg';
				}
 ?>
          <li class="clearfix"> <a href="<?php echo $css_path; ?>news/<?php echo str_replace(" ","-",strtolower($row_news_featured->News_Title)); ?>-<?php echo $row_news_featured->ID; ?>.php"><img class="lazy" src="<?php echo $css_path_images;?>/images/blank.png" data-src="<?php echo $css_path_images.$feat_image; ?>" width="500" height="379" alt=""></a>
            <div class="news-date">
              <?php $news_date_featured=@strtotime($row_news_featured->News_Date); echo $news_publish_date_featured=@date('d M Y',$news_date_featured);?>
            </div>
            <div class="news-heading"><a href="<?php echo $css_path; ?>news/<?php echo str_replace(" ","-",strtolower($row_news_featured->News_Title)); ?>-<?php echo $row_news_featured->ID; ?>.php"><?php echo $row_news_featured->News_Title;?></a></div>
          </li>
          <?php }?>
        </ul>
      </div>
      <div class="col-md-7">
        <ul id="scroller">
          <?php
			$sql_news	 = "
			select * from tbl_news order by ID desc limit 1,10";
			$rs_news  	 = mysqli_query($GLOBALS["___mysqli_ston"],$sql_news);
			$n=0;
			while($row_news  = mysqli_fetch_object($rs_news)) { 
			$n++;
			$feat_image_n=$row_news->image_path;
				if($feat_image_n=='')
				{
					$feat_image_n='images/news-image-holder.jpg';
				}
				$feat_image_n;
			?>
          <li class="clearfix"><a href="<?php echo $css_path; ?>news/<?php echo str_replace(" ","-",strtolower($row_news->News_Title)); ?>-<?php echo $row_news->ID; ?>.php"> 
            <?php /*if($n==1) {
				 echo '<img class="lazy" data-src="'.$css_path_images.$feat_image_n.'" width="25%" height="" alt="" class="thumb"></a>';
				 }*/?>
            <div class="news-date">
              <?php $news_date=@strtotime($row_news->News_Date); echo $news_publish_date=@date('d M Y',$news_date);?>
            </div>
            <div class="news-heading"><?php echo $row_news->News_Title;?></div></a>
          </li>
          <?php }?>
        </ul>
      </div>
    </div>
    <div class="text-center clear"> <a href="<?php echo $css_path; ?>news.php" class="btn-knowmore">View all</a> </div>
  </div>
</section>