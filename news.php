<?php include('header-news.php'); ?>
<style>
h1.news_heading2 {
   /* font-size: 15px;*/
    color: #fff;
    padding: 10px 10px 10px 5px;
    background-color: #000;
  /*  margin: 20px 5px 5px;*/
    border: 1px solid #ccc;
    border-radius: 5px;
}

h1.news_heading2 select {
    font-family:inherit;
    font-size: 13px;
    float: right;
    color: #333;
}
</style>
<body class="homepage">
<!--header menu-->
<?php include('header-menu.php');  ?>
<!--/header menu--> 
<div class="page-banner"> <img src="https://www.stanlay.in/images/page-banner.jpg" alt="">
  <ul class="breadcrumb">
    <li><a href="<?php echo BASE_URL;?>">Home</a></li>
    <li>News</li>
  </ul>
  <h1>News at Stanlay </h1>
</div>

<div class="page-content">

<?php  if($_REQUEST['nid']) { ?>
       <?php /*?> <h1 class="news_heading"><?php echo $N_Title; ?></h1>
        <div class="news_content"><?php echo $N_content; ?></div>
        <p class="news_date">Posted On: <?php echo $N_Date; ?> | Posted by: Stanlay Team</p><?php */?>

<div class="container">
  <div class="news-date"> <?php echo @$N_Date; ?> </div>
    <h2><strong><?php echo @$N_Title; ?></strong></h2>
    <?php echo @$N_content; ?>
  </div>

<?php }?>

  <div class="container">
  <h1 class="news_heading2">
          <?php 
	 
			$sql_cat_pro_all	 = "SELECT * from tbl_news where 1 order by ID";
			$rs_cat_pro_all  	 = mysqli_query($GLOBALS["___mysqli_ston"],$sql_cat_pro_all);
			
			$total_rows=mysqli_num_rows($rs_cat_pro_all);
			$j=1;
			?>
          <label for="news_filter">
          Stanlay News &amp; Updates:
          <form name="news_filter" action="" method="post" style="display:inherit;">
            <select name="filter_by_page" style="margin-left:10px;margin-top: 10px;" onChange="this.form.submit()">
              <?php 
				for($ij=1;$ij<=$total_rows;$ij=$ij+10) { 
				?>
              <option value="<?php echo $j; ?>" <?php if($j== @$_REQUEST['filter_by_page']) { echo "selected"; } ?>><?php echo $j; $j++; ?></option>
              <?php } ?>
            </select>
            <select name="filter_by_date" onChange="this.form.submit()" style="margin-top: 10px;">
              <option value="order by ID desc" <?php if(@$_REQUEST['filter_by_date']=='order by ID desc') { echo "selected"; } ?>>Latest on top</option>
              <option value="order by ID asc" <?php if(@$_REQUEST['filter_by_date']=='order by ID asc') { echo "selected"; } ?>>Oldest on the top</option>
              <option value="order by News_Title asc" <?php if(@$_REQUEST['filter_by_date']=='order by News_Title asc') { echo "selected"; } ?>>A-Z on the top</option>
              <option value="order by News_Title desc" <?php if(@$_REQUEST['filter_by_date']=='order by News_Title desc') { echo "selected"; } ?>>Z-A on the top</option>
            </select>
            &nbsp;
            </label>
          </form>
        </h1>
    <ul class="news-list">
    <?php
	if(@$_REQUEST['filter_by_page']!='')
			  {
				  $start_p=(@$_REQUEST['filter_by_page']-1)*10;
				  $end_p=10;
				  $limit="limit $start_p,$end_p";
			  }
			  else
			    {
					$limit="limit 0,30";
				}
	
	if(@$_REQUEST['filter_by_date']!='')	{ $orderby= @$_REQUEST['filter_by_date']; }
			else { $orderby="order by ID desc";  }
    		 $sql_cat_pro_all	 = "SELECT * from tbl_news where ID!='".$_REQUEST['nid']."' $orderby $limit";
			$rs_cat_pro_all  	 = mysqli_query($GLOBALS["___mysqli_ston"],$sql_cat_pro_all);
			while($rs_cat_pro_num_all  = mysqli_fetch_object($rs_cat_pro_all)) 
			{
            ?>
      <li> 
<div class="news-date"> <?php  echo @date('d M Y',strtotime($rs_cat_pro_num_all->News_Date));?> </div>
        <h4><a href="<?php echo $css_path; ?>news/<?php echo str_replace(" ","-",strtolower($rs_cat_pro_num_all->News_Title)); ?>-<?php echo $rs_cat_pro_num_all->ID; ?>.php"> <?php echo $rs_cat_pro_num_all->News_Title; ?></a></h4>
      </li>
	  <?php }?>
	  <!--<li> 
        <div class="news-date"> 26 Oct 2017 </div>
        <h4><a href="#;">IDS Italy Stream C GPR, With Massive array of 34 antennas in two polarization (VV + HH)</a></h4>
      </li>
	  
      <li> 
        <div class="news-date"> 23 Oct 2017 </div>
        <h4><a href="#;">Cscope MXL4 Precision Pipe &amp; Cable Locator</a></h4>
      </li>
      <li> 
        <div class="news-date"> 19 May 2017 </div>
        <h4><a href="#;">Cscope launches its latest XL4 series</a></h4>
      </li>-->
    </ul>
    <p>&nbsp;</p>
  </div>
</div>
<?php include('sidebar-contact.php');  ?>
<?php include('get-in-touch.php');  ?>
<?php include('browse-by-category.php');  ?>
<!--done-->
<?php include('footer-top-categories.php');  ?>
<!--done-->
<?php include('footer.php');  ?>
</body>
</html>
<?php
/*
######## Your Website Content Ends here Product Page#########
if (!is_dir($cache_folder)) { //create a new folder if we need to
    mkdir($cache_folder);
}

if(!$ignore){
    $fp = fopen($cache_file, 'w');  //open file for writing
   fwrite($fp, ob_get_contents()); //write contents of the output buffer in Cache file
    fclose($fp); //Close file pointer
}
ob_end_flush(); //Flush and turn off output buffering
*/
?>