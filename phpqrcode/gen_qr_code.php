<?php    
/*
 * PHP QR Code encoder
 *
 * Exemplatory usage
 *
 * PHP QR Code is distributed under LGPL 3
 * Copyright (C) 2010 Dominik Dzienia <deltalab at poczta dot fm>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */
    
    //set it to writable location, a place for temp generated PNG files
    $PNG_TEMP_DIR = dirname(__FILE__).DIRECTORY_SEPARATOR.'temp'.DIRECTORY_SEPARATOR;
    //html PNG location prefix
    $PNG_WEB_DIR = 'temp/';
    include "qrlib.php";    
   
    //ofcourse we need rights to create temp dir
    if (!file_exists($PNG_TEMP_DIR))
        mkdir($PNG_TEMP_DIR);
    
    $filename = $PNG_TEMP_DIR.'test.png';
    
    //processing form input
    //remember to sanitize user input in real-life solution !!!
//    $errorCorrectionLevel = 'L';//default
    $errorCorrectionLevel = 'H';
/*    if (isset($_REQUEST['level']) && in_array($_REQUEST['level'], array('L','M','Q','H')))
        $errorCorrectionLevel = $_REQUEST['level'];    */

    $matrixPointSize = 2;
 /*   if (isset($_REQUEST['size']))
        $matrixPointSize = min(max((int)$_REQUEST['size'], 1), 10);*/
   // if (isset($_REQUEST['data'])) { 
    
        //it's very important!
//        if (trim($_REQUEST['data']) == '')
  //          die('data cannot be empty! <a href="?">back</a>');
            
        // user data
		
	for ($i = 1; $i <= 10; $i++) 
		{
		//echo "AAAAAAAAAAAAAAAAAA:::  ".$i."<br>";
		//$data="Product name Zebra Scanner ZT230 300dpi=".$i;
		$data="https://www.stanlay.in=";
        $filename = $PNG_TEMP_DIR.$data.'-1-'.md5($data.'|'.$errorCorrectionLevel.'|'.$matrixPointSize).'.png';
        QRcode::png($data, $filename, $errorCorrectionLevel, $matrixPointSize, 2);    
		
	   echo '<img src="'.$PNG_WEB_DIR.basename($filename).'" /><hr/>';  
//	   $i++;
		}
		
  /* $filename2 = $PNG_TEMP_DIR.'proname-2-'.md5($_REQUEST['data2'].'|'.$errorCorrectionLevel.'|'.$matrixPointSize).'.png';
        QRcode::png($_REQUEST['data2'], $filename2, $errorCorrectionLevel, $matrixPointSize, 2);    

     $filename3 = $PNG_TEMP_DIR.'proname-3-'.md5($_REQUEST['data3'].'|'.$errorCorrectionLevel.'|'.$matrixPointSize).'.png';
      QRcode::png($_REQUEST['data3'], $filename3, $errorCorrectionLevel, $matrixPointSize, 2);    		
  */      
 /*   } else {    
   
        //default data
        echo 'You can provide data in GET parameter: <a href="?data=like_that">like that</a><hr/>';    
        QRcode::png('PHP QR Code :)', $filename, $errorCorrectionLevel, $matrixPointSize, 2);    
    }  */  
        
    //display generated file
//    echo '<img src="'.$PNG_WEB_DIR.basename($filename).'" /><hr/>';  
/*	echo '<img src="'.$PNG_WEB_DIR.basename($filename2).'" /><hr/>';  
	echo '<img src="'.$PNG_WEB_DIR.basename($filename3).'" /><hr/>'; */ 
    
    //config form
    /*  echo '<form action="index.php" method="post">
        Data1:&nbsp;<input name="data" value="'.(isset($_REQUEST['data'])?htmlspecialchars($_REQUEST['data']):'PHP QR Code :)').'" />&nbsp;<br>
	    Data2:&nbsp;<input name="data2" value="'.(isset($_REQUEST['data2'])?htmlspecialchars($_REQUEST['data2']):'PHP QR Code 2:)').'" />&nbsp;<br>
		Data3:&nbsp;<input name="data3" value="'.(isset($_REQUEST['data3'])?htmlspecialchars($_REQUEST['data3']):'PHP QR Code 3:)').'" />&nbsp;
		
        ECC:&nbsp;<select name="level" style="display:none">
            <option value="L"'.(($errorCorrectionLevel=='L')?' selected':'').'>L - smallest</option>
            <option value="M"'.(($errorCorrectionLevel=='M')?' selected':'').'>M</option>
            <option value="Q"'.(($errorCorrectionLevel=='Q')?' selected':'').'>Q</option>
            <option value="H"'.(($errorCorrectionLevel=='H')?' selected':'').'>H - best</option>
        </select>&nbsp;
        Size:&nbsp;<select name="size" style="display:none">';
        
    for($i=1;$i<=10;$i++)
        echo '<option value="'.$i.'"'.(($matrixPointSize==$i)?' selected':'').'>'.$i.'</option>';
        
    echo '</select>&nbsp;
        <input type="submit" value="GENERATE"></form><hr/>';<?php */ 
        
    // benchmark
  //  QRtools::timeBenchmark();    
?>