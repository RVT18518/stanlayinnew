<?php include('header-product.php'); ?>



<body>

<!--header menu-->

<?php include('header-menu.php');  ?>

<!--/header menu-->

<div class="page-banner"> <img src="<?php echo $css_path_images; ?>images/page-banner.jpg" alt="">

  <ul class="breadcrumb">

    <li><a href="<?php echo $css_path; ?>">Home</a></li>

    <li><a href="#">Categories</a></li>

    <li><a href="<?php echo $front->Cat_URL($pro_cate_id); ?>"><?php echo $front->p_category_name('cate_name',$pro_cate_id);?></a></li>

    <li><a href="<?php echo $front->Cat_URL1($pro_cate_id); ?>"><?php echo $front->category_name($pro_cate_id);?></a></li>

    <li><?php echo $pro_name;?></li>

  </ul>

  <h1><?php echo $pro_name;?></h1>

</div>

<div class="page-content">

  <div class="container-fluid">

    <div class="row bg-light content-expand">

      <div class="col-md-6 prd-gallery">

        <?php   //    $pro_id=$pro_id;

	  //if(!$_REQUEST['i_id']) $_REQUEST['i_id']=$front->get_PROImages_id($pro_id);

	  

	  

	  ?>

        <?php /*?> <img id="zoom_01" src="<?php echo $css_path;?>uploads/pro_image/zoom/<?php echo $front->Get_pro_images($pro_id); ?>" data-zoom-image="<?php echo $css_path;?>uploads/pro_image/zoom/<?php echo $front->Get_pro_images($pro_id); ?>" alt="<?php echo $front->product_name($pro_id); ?>" width="100%" /><?php */?>

        <?php 

				$front->Get_all_pro_images($pro_id);

			?>

        <!--<a href="images/gallery-large.jpg" rel="prettyPhoto[pp_gal]"  title="Picture Description"><img src="images/gallery-thumb.jpg" alt="" /></a>

<a href="images/gallery-large.jpg" rel="prettyPhoto[pp_gal]"  title="Picture Description"><img src="images/gallery-thumb.jpg" alt="" /></a>

<a href="images/gallery-large.jpg" rel="prettyPhoto[pp_gal]"  title="Picture Description"><img src="images/gallery-thumb.jpg" alt="" /></a>

<a href="images/gallery-large.jpg" rel="prettyPhoto[pp_gal]"  title="Picture Description"><img src="images/gallery-thumb.jpg" alt="" /></a>

<a href="images/gallery-large.jpg" rel="prettyPhoto[pp_gal]"  title="Picture Description"><img src="images/gallery-thumb.jpg" alt="" /></a>

<a href="images/gallery-large.jpg" rel="prettyPhoto[pp_gal]"  title="Picture Description"><img src="images/gallery-thumb.jpg" alt="" /></a>--> 

      </div>

      <div class="col-md-6">

        <h3>Product Overview</h3>

        <p> <?php echo $pro_short_desc;?> 

      </div>

    </div>

  </div>

  <div class="container-fluid">

    <div class="row">

      <?php 

		

		//$text = $pro_description;//"<p>this is the first paragraph</p><p>this is the first paragraph</p>";

////$text = str_replace('</p>', '', $text);

//$array = explode('<p>', $text);

//echo "<pre>";

//		print_r($array); //exit;

		//echo $pro_description;?>

      <!--<div class="col-md-6 padtpbtm30">

        <p class="highlight18"></p>

      </div>-->

      <div class="col-md-12 padtpbtm30">

        <p><?php echo $pro_description;?></p>

      </div>

    </div>

  </div>

</div>

<section class="newly-launch">

  <div class="container"> 

    <!-- tabs -->

    <ul class="tabs">

      <li class="active" rel="tab1"><span>Specifications</span></li>

      <li rel="tab2"><span>Features</span></li>

      <li rel="tab3"><span>Video</span></li>

      <li rel="tab4"><span>Product Info Downloads</span></li>

    </ul>

    <div class="tab_container">

      <h2 class="d_active tab_drawer_heading" rel="tab1">Specifications</h2>

      <div id="tab1" class="tab_content">

        <div class="pad20-all">

          <h3>Product Specification</h3>

        </div>

        <div class="table-responsive">

          <table class="table prd-specification">

            <!--<thead>

              <tr>

                <th><strong>Mode/Feature</strong></th>

              </tr>

            </thead>-->

            <tbody>

              <tr>

                <td><?php echo $pro_specification;?></td>

              </tr>

              <!--<tr>

                <td>Radio</td>

                <td>10,000-30,000</td>

                <td>12μA</td>

                <td>2.0m</td>

              </tr>

              <tr>

                <td>Generator</td>

                <td>32,100-33,400</td>

                <td>2μA</td>

                <td>3.0m</td>

              </tr>

              <tr>

                <td>CAT 33 XD Depth Measurement- Line</td>

                <td></td>

                <td></td>

                <td>0.2 to 3.0m</td>

              </tr>

              <tr>

                <td>CAT 33 XD Depth Measurement- Sonde</td>

                <td></td>

                <td></td>

                <td>0.85 to 4.5m</td>

              </tr>

              <tr>

                <td>Power</td>

                <td>50-500</td>

                <td>7mA</td>

                <td>3.0m</td>

              </tr>-->

            </tbody>

          </table>

        </div>

      </div>

      <!-- #tab1 -->

      <h2 class="d_active tab_drawer_heading" rel="tab2">Features</h2>

      <div id="tab2" class="tab_content pad20-all">

        <h3>Features</h3>

        <div class="row clearfix feature"> <?php echo $pro_features;?> 

          <!--<div class="col-md-6"><img src="images/feature-img.jpg"></div>

          <div class="col-md-6">

            <h4><strong>Power Mode</strong></h4>

            <p>The POWER mode is the quickest way to detect most buried power cables by detecting the 50/60 Hz signal created by the electrical current itself, just using the CAT 33XD pipe & cable locating receiver on a stand alone basis.</p>

          </div>--> 

        </div>

        <div>&nbsp;</div>

        <!--<div class="row clearfix feature">

          <div class="col-md-6">

            <h4><strong>Power Mode</strong></h4>

            <p>The POWER mode is the quickest way to detect most buried power cables by detecting the 50/60 Hz signal created by the electrical current itself, just using the CAT 33XD pipe & cable locating receiver on a stand alone basis.</p>

          </div>

          <div class="col-md-6"><img src="images/feature-img.jpg"></div>

        </div>-->

        <div>&nbsp;</div>

        <!--<div class="row clearfix feature">

          <div class="col-md-6"><img src="images/feature-img.jpg"></div>

          <div class="col-md-6">

            <h4><strong>LCD Display</strong></h4>

            <p>Multi Segment LCD display with bar graph for easy locating, pin pointing and direction/orientation of buried utility.</p>

            <p>LCD displays important operating parameters such as :</p>

            <ul>

              <li>Signal strength</li>

              <li>Battery condition</li>

              <li>Confirmation of selected mode</li>

              <li>Depth (applicable to CAT 33 XD model) when used with the SGV signal generator.</li>

            </ul>

          </div>

        </div>--> 

      </div>

      <!-- #tab2 -->

      <h2 class="d_active tab_drawer_heading" rel="tab3">Video</h2>

      <div id="tab3" class="tab_content">

        <div class="pad20-all clearfix">

          <h3>Video</h3>

          <div class="col-md-4 col-sm-4">

            <iframe width="" height="200" src="https://www.youtube.com/embed/69z_el7_7rE" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

          </div>

          <div class="col-md-4 col-sm-4">

            <iframe width="" height="200" src="https://www.youtube.com/embed/69z_el7_7rE" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

          </div>

          <div class="col-md-4 col-sm-4">

            <iframe width="" height="200" src="https://www.youtube.com/embed/69z_el7_7rE" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

          </div>

        </div>

      </div>

      <!-- #tab3 -->

      <h2 class="d_active tab_drawer_heading" rel="tab4">Product Info Downloads</h2>

      <div id="tab4" class="tab_content">

        <div class="pad20-all">

          <h3>Product Info Downloads</h3>

          <p>Product Information files contain the essential technical detail you need to make informed decisions on any particular product. Simply click on a title to download a PDF copy of that file. If you wish a hard copy of a brochure, technical advice on any product or a demonstration then please contact us. We're always happy to help!</p>

          

          <!-- <span class="download_details_left_left">Datasheet : </span>  -->

          

          <?php if($datasheet_down!='') { ?>

          <div class="col-md-6 download-box"> <img src="<?php echo $css_path;?>images/download-thumb.jpg" style="display:none;"> <span>Product Datasheet</span> <a href="<?php echo $css_path;?><?php echo $datasheet_down;?>" title="<?php echo $datasheet_tit1;?>" target="_blank"> Product Datasheet</a> </div>

          <?php } ?>

          <?php if($manuals_down!='') { ?>

          <div class="col-md-6 download-box"> <img src="<?php echo $css_path;?>images/download-thumb.jpg"> <span>User Manual</span> <a href="<?php echo $css_path;?><?php echo $manuals_down;?>" title="<?php echo $datasheet_tit2;?>" target="_blank" >User Manual</a> </div>

          <?php } ?>

          <?php if($manuals_down1!='') { ?>

          <div class="col-md-6 download-box"> <img src="<?php echo $css_path;?>images/download-thumb.jpg"> <span>User Manual</span> <a href="<?php echo $css_path;?><?php echo $manuals_down1;?>" title="<?php echo $datasheet_tit3;?>" target="_blank" >User Manual</a> </div>

          <?php } ?>

          <?php if($manuals_down2!='') { ?>

          <div class="col-md-6 download-box"> <img src="<?php echo $css_path;?>images/download-thumb.jpg"> <span>Quick User Guide</span> <a href="<?php echo $css_path;?><?php echo $manuals_down2;?>" title="<?php echo $datasheet_tit4;?>" target="_blank" class="download_details_left_l" >Quick User Guide</a> </div>

          <?php } ?>

          <?php if($manuals_down3!='') { ?>

          <div class="col-md-6 download-box"> <img src="<?php echo $css_path;?>images/download-thumb.jpg"> <span>Case Studies</span> <a href="<?php echo $css_path;?><?php echo $manuals_down3;?>" title="<?php echo $datasheet_tit5;?>" target="_blank">Case Studies</a> </div>

          <?php } ?>

          <?php if($manuals_down4!='') { ?>

          <div class="col-md-6 download-box"> <img src="<?php echo $css_path;?>images/download-thumb.jpg"> <span>Case Studies</span> <a href="<?php echo $css_path;?><?php echo $manuals_down4;?>" title="<?php echo $datasheet_tit6;?>" target="_blank" >Case Studies</a> </div>

          <?php } ?>

          

          <!--<div class="col-md-6 download-box"> <img src="<?php //echo $css_path;?>images/download-thumb.jpg"> <span>Product Datasheet</span> <a href="#" class="download-icon">Download</a> </div>

          <div class="col-md-6 download-box"> <img src="images/download-thumb.jpg"> <span>Product Datasheet</span> <a href="#" class="download-icon">Download</a> </div>--> 

        </div>

      </div>

      <!-- #tab4 --> 

    </div>

    <!-- .tab_container --> 

  </div>

  <!-- /tabs -->

  </div>

</section>

<section class="top-cat-hp bg-light">

  <div class="container">

    <div class="row">

      <h2>Related Products</h2>

       <?php  $sql_related="select tbl_index_g21.match_pro_id_g2,tbl_products_1.pro_id,tbl_products_1.pro_image,tbl_products_1.pro_title,tbl_products_1.pro_permalink_n from tbl_index_g21,tbl_products_1 where tbl_index_g21.match_pro_id_g2=tbl_products_1.pro_id and tbl_index_g21.deleteflag='active' and tbl_products_1.pro_group_id = 0 and tbl_products_1.status='active' and tbl_index_g21.pro_id=$pro_id";

	   $rs_related_post=mysqli_query($con,  $sql_related);



	   

	    ?>

      <div id="related-products" class="carousel slide" data-ride="carousel">

        <div class="carousel-inner">

          <div class="item active">

            <div class="container clearfix">

            

            <?php

			$r=0;

				   while($row_related_post=mysqli_fetch_object($rs_related_post))

				   {

					   

				$r++;	   

					   /*echo "<pre>";

					   print_r($row_related_post);*/

					   ?>

              <div class="col-md-4 col-sm-4">

                <div class="related-box"><a href="<?php echo $css_path.$front->Pro_URL($row_related_post->pro_id);?>"><img src="<?php echo $css_path_images.'uploads/pro_image/small/'.$front->Get_pro_images($row_related_post->pro_id);?>" alt="<?php echo $row_related_post->pro_title;?>" border="0"  style=" width:320px; height:178px "   /></a>

                  <h3><a href="<?php echo $css_path.$front->Pro_URL($row_related_post->pro_id);?>"><?php echo $row_related_post->pro_title;?></a></h3>

                </div>

              </div>

                            <?php 

               if($r%3==0)

	  {

		  echo ' </div ></div >';

		  echo ' <div class="item "><div class="container clearfix">';

	  }

		}

		?>



           

            </div>

          </div>

          

        </div>

        <div class="control-panel"> <a class="left carousel-control" href="#related-products" data-slide="prev"> &nbsp;</a> <a class="right carousel-control" href="#related-products" data-slide="next"> &nbsp;</a> </div>

      </div>

    </div>

  </div>

</section>



<!-- MENU END HARE -->



</div>

<?php include('get-in-touch.php');  ?>

<?php include('browse-by-category.php');  ?>

<!--done-->

<?php include('footer-top-categories.php');  ?>

<!--done-->

<?php include('footer.php');  ?>

</body>

</html>