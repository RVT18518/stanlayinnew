<?php include('header-product.php'); ?>
<style>
.table-width100per{width:100%;white-space: initial!important;/*this is used for mobile no wrap to remove scroll*/}
.youtube {
    background-color: #000;
    margin-bottom: 30px;
    position: relative;
    padding-top: 56.25%;
    overflow: hidden;
    cursor: pointer;
}
.youtube img {
    width: 100%;
    top: -16.84%;
    left: 0;
    opacity: 0.7;
}
.youtube .play-button {
    width: 90px;
    height: 60px;
    background-color: #333;
    box-shadow: 0 0 30px rgba( 0,0,0,0.6 );
    z-index: 1;
    opacity: 0.8;
    border-radius: 6px;
}
.youtube .play-button:before {
    content: "";
    border-style: solid;
    border-width: 15px 0 15px 26.0px;
    border-color: transparent transparent transparent #fff;
}
.youtube img,
.youtube .play-button {
    cursor: pointer;
}
.youtube img,
.youtube iframe,
.youtube .play-button,
.youtube .play-button:before {
    position: absolute;
}
.youtube .play-button,
.youtube .play-button:before {
    top: 50%;
    left: 50%;
    transform: translate3d( -50%, -50%, 0 );
}
.youtube iframe {
    height: 100%;
    width: 100%;
    top: 0;
    left: 0;
}

 .faqHeader {
        font-size: 27px;
        margin: 20px;
    }


.panel-default>.panel-heading+.panel-collapse .panel-body {
    background: #fff;
    color: inherit!important;
}
    .panel-heading [data-toggle="collapse"]:after {
        font-family: 'Glyphicons Halflings';
        content: "\e072"; /* "play" icon */
        float: right;
        color: #ed3338;/*#F58723;*/
        font-size: 18px;
        line-height: 22px;
        /* rotate "play" icon from > (right arrow) to down arrow */
        -webkit-transform: rotate(-90deg);
        -moz-transform: rotate(-90deg);
        -ms-transform: rotate(-90deg);
        -o-transform: rotate(-90deg);
        transform: rotate(-90deg);
    }

    .panel-heading [data-toggle="collapse"].collapsed:after {
        /* rotate "play" icon from > (right arrow) to ^ (up arrow) */
        -webkit-transform: rotate(90deg);
        -moz-transform: rotate(90deg);
        -ms-transform: rotate(90deg);
        -o-transform: rotate(90deg);
        transform: rotate(90deg);
        color: #454444;
    }
	
	
	
	.accordion-toggle {
    border-bottom: 0px solid #ccc;
    cursor: pointer;
    margin: 0;
    padding: 10px 0;
    position: relative;
    color: red;
}



.accordion-toggle.active:after, .accordion-toggle:before {
    position: absolute;
    top: 17px;
    width: 0;
    height: 0;
    /* border-left: 5px solid transparent; */
    /* border-right: 5px solid transparent; */
    right: 0;
    content: "";
}



.accordion-toggle:before {
    border-top: 0px solid #000;
}


.panel-group .panel {
    margin-bottom: 3px;
  /*  border-radius: 4px;*/
}


.alert-warning {
    color: #8a6d3b;
    background-color: #fcf8e3;
    border-color: #faebcc;
}


.alert-dismissable, .alert-dismissible {
    padding-right: 35px;
}

.alert {
    padding: 15px;
    margin-bottom: 20px;
    border: 1px solid transparent;
    border-radius: 4px;
}
</style>

 <link rel="stylesheet" type="text/css" href="<?php echo BASE_URL;?>font-awesome/css/font-awesome.min.css" />
<script type="text/javascript" src="<?php echo BASE_URL;?>js/jquery-1.10.2.min.js"></script>
<body>
<!--header menu-->
<?php include('header-menu.php'); 
/*https://stanlay.in/web/underground-locating-equipment/utility-mapping-gpr/ids-opera-duo-gpr/ demo link with all features anteena software etc/*/
if($pro_banner_image=='')
{
	$pro_banner_image="images/page-banner.jpg";
}
else
{
	$pro_banner_image=$pro_banner_image;
}
//echo "CID". $pro_cate_id;
 ?>
<!--/header menu-->
<div class="page-banner"> <img class="lazy img-responsive" src="<?php echo $css_path_images;?>images/blank.png" data-src="<?php echo $css_path_images.$pro_banner_image; ?>" alt="" >
  <div class="product_pagehead category-header">
    <h1><?php echo $pro_name;?></h1>
    <ul class="breadcrumb">
      <li><a href="<?php echo $css_path; ?>">Home</a></li>
      <li><a href="<?php echo $css_path.$front->Cat_URL($pro_cate_id); ?>"><?php echo $front->p_category_name('cate_name',$pro_cate_id);?></a></li>
      <li><a href="<?php echo $css_path.$front->Cat_URL1($pro_cate_id); ?>"><?php echo $front->category_name($pro_cate_id);?></a></li>
      <li><?php echo $pro_name;?></li>
    </ul>
  </div>
</div>
<div class="page-content">
  <div class="container">
    <div class="row content-expand">
      <div class="col-md-12 prd-gallery" align="center">
        <?php  
 	        $pro_id=$pro_id;
	  		$sql_cat_pro	 = "SELECT * from tbl_products_1 where pro_id='$pro_id' order by sort_order";
			$rs_cat_pro  	 = mysqli_query($GLOBALS["___mysqli_ston"],$sql_cat_pro);
			if(mysqli_num_rows($rs_cat_pro)<=0) {
			echo "<script>window.location='https://www.stanlay.in/404.php'</script>";
			}
			
			$rs_cat_pro_num  = @mysqli_fetch_object($rs_cat_pro);
			
			$seo_title			= @$rs_cat_pro_num->seo_title;
			$meta_content		= @$rs_cat_pro_num->meta_content;
			$meta_desc			= @$rs_cat_pro_num->meta_desc;
			$pro_name			= @$rs_cat_pro_num->pro_title;
			$heading_2			= @$rs_cat_pro_num->heading_2;			
			$pro_description	= @$rs_cat_pro_num->pro_details;
			$pro_specification	= @$rs_cat_pro_num->specification;
			$pro_features		= @$rs_cat_pro_num->features;
			$pro_software		= @$rs_cat_pro_num->software;
			$Anteena			= @$rs_cat_pro_num->transducer_selection;
			$FAQ				= "1";//@$rs_cat_pro_num->Transducers;
			$Transducers		= @$rs_cat_pro_num->Transducers;
			$pro_video			= @$rs_cat_pro_num->embed_video;
			$pro_video1			= @$rs_cat_pro_num->embed_video1;
			$pro_video2			= @$rs_cat_pro_num->embed_video2;
			
			$pro_cate_id		= @$rs_cat_pro_num->cate_id;
			$Blog_id			= @$rs_cat_pro_num->Blog_id;

			$sql_cat_pro	 = "SELECT * from tbl_products_downloads where pro_id='$pro_id'";
			$rs_cat_pro  	 = @mysqli_query($GLOBALS["___mysqli_ston"],$sql_cat_pro);
			$rs_cat_pro_num  = @mysqli_fetch_object($rs_cat_pro);
			
			$datasheet_tit1	= @$rs_cat_pro_num->pro_tech_specs_title;
			$datasheet_tit2	= @$rs_cat_pro_num->pro_user_manuals_title;
			$datasheet_tit3	= @$rs_cat_pro_num->pro_user_manuals_title1;
			$datasheet_tit4	= @$rs_cat_pro_num->pro_user_manuals_title3;
			$datasheet_tit5	= @$rs_cat_pro_num->pro_user_manuals_title4;
			$datasheet_tit6	= @$rs_cat_pro_num->Case_Studies_title;
			$datasheet_down	= @$rs_cat_pro_num->pro_tech_specs;
			$manuals_down	= @$rs_cat_pro_num->pro_user_manuals;
			$manuals_down1	= @$rs_cat_pro_num->pro_user_manuals1;
			$manuals_down2	= @$rs_cat_pro_num->pro_user_manuals3;
			$manuals_down3	= @$rs_cat_pro_num->pro_user_manuals4;
			$manuals_down4	= @$rs_cat_pro_num->Case_Studies;
	  ?>
        <ul id="image-gallery" class="gallery list-unstyled cS-hidden">
          <?php 
				$front->Get_all_pro_images($pro_id);
			?>
        </ul>
      </div>
       <!--comment to hide short desc-->
       <div class="clear"></div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <!--<div class="col-md-6 padtpbtm30">
        <p class="highlight18"></p>
      </div>--> 
      <!--commnetd by rumit to hide pro description extra-->
      <div class="container category-header">
        <h2><?php echo $heading_2;?> </h2> 
      </div>
      <div class="col-md-12 padtpbtm30 table-responsive">
        <p>
				  <?php $pro_description1=preg_replace('/font-family.+?;/', "", $pro_description);?>
                  <?php $pro_description2=preg_replace('/margin.+?;/', "", $pro_description1);?>
                  <?php $pro_description3=preg_replace('/text-indent.+?;/', "", $pro_description2);?>
                  <?php $pro_description4=preg_replace('/font-size.+?;/', "", $pro_description3);?>
                  <?php $pro_description5=preg_replace('/width.+?;/', "", $pro_description4);?>
                  <?php $pro_description6=str_replace("../uploads/","../../../uploads/", $pro_description5);?>
                  <?php $pro_description7=str_replace("style=","style=", $pro_description6);?>
                  <?php //$pro_description8=str_replace("MsoNormalTable","table-responsive table-width100per", $pro_description7);?>                 <?php  echo $pro_description8=preg_replace('/<table/', "<table class='table-responsive table-striped table-width100per '", $pro_description7);?>
                  <br>
		<?php //echo str_replace("../uploads/","../../../uploads/",$pro_description);?><a href="#;" class="abtn-letus" data-toggle="modal" data-target="#myModal" onclick="ga('send', 'event', 'Request-A-Quote', 'Click', 'Request-A-Quote-product-button');"><img src="<?php echo $css_path;?>images/request-a-quote.png" width="175px;"  alt=""></a></p>
      </div>
    </div>
  </div>
</div>
<section class="newly-launch">
  <div class="container"> 
    
    <!-- tabs -->
    
    <ul class="tabs">
      <?php if($pro_specification!='') { ?>
      <li class="active" rel="tab1"><span>Specifications</span></li>
      <?php }
	  if($pro_features!='') { ?>
      <li <?php if($pro_specification!='') { } else {echo 'class="active"'; }?>rel="tab2"><span>Features</span></li>
      <?php }
	   if($pro_video!='' || $pro_video1!='' || $pro_video2!='') { ?>
      <li rel="tab3"><span>Video</span></li>
      <?php }
  
	   if($FAQ!='') { ?>
<!--       data-toggle="modal" data-target="#myModalFAQ"-->
      <li rel="tab9" ><span>FAQ</span>1</li>
      <?php } ?>
      <?php if($pro_software!='') { ?>
      <li rel="tab4"><span>Software</span></li>
      <?php } ?>
      <?php if($Anteena!='') { ?>
      <li rel="tab5"><span>Anteena</span></li>
      <?php } ?>
      <?php if($Transducers!='') { ?>
      <li rel="tab6"><span>Transducers</span></li>
      <?php } ?>
      <?php if($Blog_id!='') { ?>
      <li rel="tab8"><span>Related Article</span></li>
      <?php } ?>
      <li rel="tab7"><span>Product Info Downloads</span></li>
    </ul>
    <div class="tab_container">
    <?php if($pro_specification!='') { ?> 
      <div class="headmob tab_drawer_heading" rel="tab1">Specifications</div>
      <div id="tab1" class="tab_content">
        <div class="pad20-all">
          <div class="tab-head">Product Specification</div>
        </div>
        <div class="table-responsive">
          <table class="table prd-specification">
            <tbody>
              <tr>
                <td><?php 				$pro_specification1=preg_replace('/font-family.+?;/', "", $pro_specification);?>
                  <?php 				$pro_specification2=preg_replace('/margin.+?;/', "", $pro_specification1);?>
                  <?php 				$pro_specification3=preg_replace('/text-indent.+?;/', "", $pro_specification2);?>
                  <?php 				$pro_specification4=preg_replace('/font-size.+?;/', "", $pro_specification3);?>
                  <?php 				$pro_specification5=preg_replace('/width.+?;/', "", $pro_specification4);?>
                  <?php 				$pro_specification6=preg_replace('/<table/', "<table class='table-striped table-width100per table-hover'", $pro_specification5);?>
                   <?php echo  $pro_specification7=str_replace("../uploads/","/uploads/",$pro_specification6);?></td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
       <?php } ?>
      <!-- #tab1 -->
            <?php if($pro_features!='') { ?>
      <div class="headmob tab_drawer_heading" rel="tab2">Features</div>
      <div id="tab2" class="tab_content pad20-all">
        <div class="tab-head">Features</div>
        <div class="row clearfix feature"> <?php /*$pro_features1=str_replace("../uploads/","/uploads/",$pro_features);
echo		$pro_features2=str_replace('/<table/', "<table class='table-striped table-width100per '",$pro_features1); */?>
				  <?php	$pro_features1=preg_replace('/font-family.+?;/', "", $pro_features);?>
                  <?php	$pro_features2=preg_replace('/margin.+?;/', "", $pro_features1);?>
                  <?php	$pro_features3=preg_replace('/text-indent.+?;/', "", $pro_features2);?>
                  <?php	$pro_features4=preg_replace('/font-size.+?;/', "", $pro_features3);?>
                  <?php	$pro_features5=preg_replace('/width.+?;/', "", $pro_features4);?>
                  <?php	$pro_features6=preg_replace('/<table/', "<table class='table-striped table-width100per '", $pro_features5);
				  		$pro_features7=str_replace("../uploads/","/uploads/",$pro_features6);
				   echo $pro_features8=str_replace("http://","https://",$pro_features7); ?>
 </div>
        <div>&nbsp;</div>
        <div>&nbsp;</div>
      </div>
      <?php } ?>
      <!-- #tab2 -->
      <?php if($pro_video!='' || $pro_video1!='' || $pro_video2!='') {  ?>
      <div class="headmob d_active tab_drawer_heading" rel="tab3">Video</div>
      <div id="tab3" class="tab_content">
        <div class="pad20-all clearfix">
          <div class="tab-head">Video</div>
          <?php if($pro_video!='') {  ?> 
          <div class="col-md-4 col-sm-4">
                   <?php $play_video=str_replace("https://www.youtube.com/embed/","",preg_replace('/^([^?]*).*$/', '$1',$pro_video));// changes done on 21-oct-2020 to reduce load time on googplepage speed test - embed video slows down the loading score ?>
          <!-- (1) video wrapper -->
<div class="youtube" data-embed="<?php echo $play_video;?>"> 
     <!-- (2) the "play" button -->
    <div class="play-button"></div> 
 </div>
          <?php /*?>  <iframe width="" height="200" src="<?php echo $pro_video; ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe><?php */?>
          </div>
          <?php }
		  if($pro_video1!='') {  ?> 
           <div class="col-md-4 col-sm-4">
            <?php $play_video=str_replace("https://www.youtube.com/embed/","",preg_replace('/^([^?]*).*$/', '$1',$pro_video1));// changes done on 21-oct-2020 to reduce load time on googplepage speed test - embed video slows down the loading score ?>
            <!-- (1) video wrapper -->
            <div class="youtube" data-embed="<?php echo $play_video;?>"> 
              <!-- (2) the "play" button -->
              <div class="play-button"></div>
            </div>
            <?php /*?>  <iframe width="" height="200" src="<?php echo $pro_video; ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe><?php */?>
          </div>
          <?php }
		  ?>
          <?php if($pro_video2!='') {  ?> 
          <div class="col-md-4 col-sm-4">
            <?php $play_video=str_replace("https://www.youtube.com/embed/","",preg_replace('/^([^?]*).*$/', '$1',$pro_video2));// changes done on 21-oct-2020 to reduce load time on googplepage speed test - embed video slows down the loading score ?>
            <!-- (1) video wrapper -->
            <div class="youtube" data-embed="<?php echo $play_video;?>"> 
              <!-- (2) the "play" button -->
              <div class="play-button"></div>
            </div>
            <?php /*?>  <iframe width="" height="200" src="<?php echo $pro_video; ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe><?php */?>
          </div>
                    <?php }
		  ?>
        </div>
      </div>
      <!-- #tab3 -->
      <?php }?>
      <?php if($pro_software!='') { ?>
      <div class="headmob d_active tab_drawer_heading" rel="tab4">Software</div>
      <div id="tab4" class="tab_content pad20-all">
        <div class="tab-head">Software</div>
        <div class="row clearfix feature"> <?php echo $pro_software1=str_replace("../uploads/","/uploads/",$pro_software);?> </div>
        <div>&nbsp;</div>
        <div>&nbsp;</div>
      </div>
      <!-- #tab4 -->
      <?php }?>
      <?php if($Anteena!='') { ?>
      <div class="headmob tab_drawer_heading" rel="tab5">Anteena</div>
      <div id="tab5" class="tab_content pad20-all">
        <div class="tab-head">Anteena</div>
        <div class="row clearfix feature"> <?php echo $Anteena;?> </div>
        <div>&nbsp;</div>
        <div>&nbsp;</div>
      </div>
      <!-- #tab5 -->
      <?php }?>
      <?php if($Transducers!='') { ?>
      <div class="headmob tab_drawer_heading" rel="tab6">Transducers</div>
      <div id="tab6" class="tab_content pad20-all">
        <div class="tab-head">Transducers</div>
        <div class="row clearfix feature"> <?php echo $Transducers;?> </div>
        <div>&nbsp;</div>
        <div>&nbsp;</div>
      </div>
      <!-- #tab6 -->
      <?php }?>
      
       <?php if($FAQ!='') { ?>
      <!--mobile tabs-->  <!--data-toggle="modal" data-target="#myModalFAQ"-->
      <div class="headmob tab_drawer_heading" rel="tab9">FAQ</div>
      <div id="tab9" class="tab_content pad20-all">
        <div class="tab-head">FAQ</div>
        <div class="row clearfix feature">
             <div class="containers">
       <div class="alert alert-warning alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        Do you have a question which we can answer for you? <a href="#;" data-toggle="modal" data-target="#myModalFAQ">Click here</a>  if Yes
    </div>
    <div class="panel-group" id="accordion">

  <?php 
  	  		$sql_faq	 = "SELECT faq_ques,faq_ans,name from tbl_faq where faq_ans!='' and pro_id=$pro_id and status='active' and deleteflag='active' ";
			$rs_faq  	 = mysqli_query($GLOBALS["___mysqli_ston"],$sql_faq);
			$faq_num=@mysqli_num_rows(@$rs_faq);
			if(@mysqli_num_rows(@$rs_faq)>0) {?>
        <div class="faqHeader">Frequently Asked Questions <span style="font-size:12px;">(<?php echo $faq_num;?> answered questions)</span> </div>
            <?php
			$i=0;
			while($row_faq  = @mysqli_fetch_object($rs_faq))
			{
			$i++;
			$faq_ques			= $row_faq->faq_ques;
			$faq_ans			= nl2br($row_faq->faq_ans);
			$faq_name			= explode(" ",$row_faq->name);
   ?>      
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $i;?>"><?php echo $faq_ques;?></a>
                </h4>
            </div>
            <div id="collapse<?php echo $i;?>" class="panel-collapse collapse ">
                <div class="panel-body">
             <?php echo $faq_ans;?>
             <p><strong>Person name:</strong> <?php echo $faq_name[0];?></p>
                </div>
            </div>
        </div>
<?php 
	 }
	  }
	  
	  ?> 
        </div>
</div>      
         </div>
        <div>&nbsp;</div>
        <div>&nbsp;</div>
      </div>
      <!-- #tab8 -->
      <?php }?>
      <div class="headmob tab_drawer_heading" rel="tab7">Product Info Downloads</div>
      <div id="tab7" class="tab_content">
        <div class="pad20-all">
          <div class="tab-head">Product Info Downloads</div>
          <p>Product Information files contain the essential technical detail you need to make informed decisions on any particular product. Simply click on a title to download a PDF copy of that file. If you wish a hard copy of a brochure, technical advice on any product or a demonstration then please contact us. We're always happy to help!</p>
          <div style="max-width:100%; overflow-x:auto;">
            <div class="DatalTable">
              <div class="DatalTableBody">
                <div class="DataTableRow">
                  <div class="DataTableCell" style="background: #ed3237; color:#fff;"><strong>Title</strong></div>
                  <div class="DataTableCell" style="background: #ed3237; color:#fff;"><strong>File Type</strong></div>
                  <div class="DataTableCell" style="background: #ed3237; color:#fff;"><strong>File Size</strong></div>
                  <div class="DataTableCell" style="background: #ed3237; color:#fff;"><strong>Last Uploaded</strong></div>
                </div>
                <?php if($datasheet_down!='') {
 				  $path=$_SERVER['DOCUMENT_ROOT']."/".$datasheet_down;	
					 ?>
                <div class="DataTableRow">
                  <div class="DataTableCell"><a href="<?php echo $css_path_images;?><?php echo $datasheet_down;?>" title="<?php echo $datasheet_tit1;?>" target="_blank">Product Datasheet</a></div>
                  <div class="DataTableCell"><?php echo mime_content_type($path);?></div>
                  <div class="DataTableCell">
                    <?php   $file_size=filesize($path);
				   echo $front->format_size($file_size);?>
                  </div>
                  <div class="DataTableCell"><?php echo date ("F d Y H:i:s", filemtime($path));?></div>
                </div>
                <?php } ?>
                <?php if($manuals_down!='') { 
				  $path=$_SERVER['DOCUMENT_ROOT']."/".$manuals_down;	?>
                <div class="DataTableRow">
                  <div class="DataTableCell"><a href="<?php echo $css_path_images.$manuals_down;?>" title="<?php echo $datasheet_tit2;?>" target="_blank" >User Manual</a></div>
                  <div class="DataTableCell"><?php echo mime_content_type($path);?></div>
                  <div class="DataTableCell">
                    <?php 
				   $file_size=filesize($path);
				  echo $front->format_size($file_size);?>
                  </div>
                  <div class="DataTableCell"><?php echo date ("F d Y H:i:s", filemtime($path));?></div>
                </div>
                <?php } ?>
                <?php if($manuals_down1!='') {
			 $path=$_SERVER['DOCUMENT_ROOT']."/".$manuals_down1;  
			   ?>
                <div class="DataTableRow">
                  <div class="DataTableCell"><a href="<?php echo $css_path_images.$manuals_down1;?>" title="<?php echo $datasheet_tit3;?>" target="_blank" >User Manual</a></div>
                  <div class="DataTableCell"><?php echo mime_content_type($path);?></div>
                  <div class="DataTableCell">
                    <?php 
				   $file_size=filesize($path);
				  echo $front->format_size($file_size);?>
                  </div>
                  <div class="DataTableCell"><?php echo date ("F d Y H:i:s", filemtime($path));?></div>
                </div>
                <?php } ?>
                <?php if($manuals_down2!='') {
			  $path=$_SERVER['DOCUMENT_ROOT']."/".$manuals_down2;   
			   ?>
                <div class="DataTableRow">
                  <div class="DataTableCell"><a href="<?php echo $css_path_images.$manuals_down2;?>" title="<?php echo $datasheet_tit4;?>" target="_blank" class="download_details_left_l" >Quick User Guide</a> </div>
                  <div class="DataTableCell"><?php echo mime_content_type($path);?></div>
                  <div class="DataTableCell">
                    <?php 
				   $file_size=filesize($path);
				  echo $front->format_size($file_size);?>
                  </div>
                  <div class="DataTableCell"><?php echo date ("F d Y H:i:s", filemtime($path));?></div>
                </div>
                <?php } ?>
                <?php if($manuals_down3!='') {
			 $path=$_SERVER['DOCUMENT_ROOT']."/".$manuals_down3;  
			   ?>
                <div class="DataTableRow">
                  <div class="DataTableCell"> <a href="<?php echo $css_path_images.$manuals_down3;?>" title="<?php echo $datasheet_tit5;?>" target="_blank">Case Studies</a></div>
                  <div class="DataTableCell"><?php echo mime_content_type($path);?></div>
                  <div class="DataTableCell">
                    <?php 
				   $file_size=filesize($path);
				  echo $front->format_size($file_size);?>
                  </div>
                  <div class="DataTableCell"><?php echo date ("F d Y H:i:s", filemtime($path));?></div>
                </div>
                <?php } ?>
                <?php if($manuals_down4!='') {
			   $path=$_SERVER['DOCUMENT_ROOT']."/".$manuals_down4;
			   ?>
                <div class="DataTableRow">
                  <div class="DataTableCell"> <a href="<?php echo $css_path_images.$manuals_down4;?>" title="<?php echo $datasheet_tit6;?>" target="_blank" >Case Studies</a></div>
                  <div class="DataTableCell"><?php echo mime_content_type($path);?></div>
                  <div class="DataTableCell">
                    <?php 
				   $file_size=filesize($path);
				  echo $front->format_size($file_size);?>
                  </div>
                  <div class="DataTableCell"><?php echo date ("F d Y H:i:s", filemtime($path));?></div>
                </div>
                <?php } ?>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- #tab8 -->
      <?php if($Blog_id!='') { ?>
      <div class="headmob tab_drawer_heading" rel="tab8">Related Articles</div>
      <div id="tab8" class="tab_content pad20-all">
        <div class="tab-head">Related Articles</div>
        <?php 
	   	$blog_id_all=explode("-",$Blog_id);
	   	foreach($blog_id_all as $keys)
			  {
	    $sqlArticle = "select post_content,post_name,post_title,ID from wp_posts where ID='".$keys."' and post_status='publish'";	
		$rsArticles	 = mysqli_query($GLOBALS["___mysqli_ston"],$sqlArticle);
		if(mysqli_num_rows($rsArticles)>0)
			{
				while($rowArticle = mysqli_fetch_object($rsArticles))
				{  ?>
        <div class="DataTableRow">
          <div class="DataTableCell" style="width: 99.1%;background-color: #eee;padding: .5% 4px .5% .5%;font-weight: 700;color: #000; margin-top: 0;"><?php echo $rowArticle->post_title;  ?></div>
        </div>
        <div class="DataTableRow">
          <div class="DataTableCell">
            <?php 
			 $blog_cont = utf8_encode(str_replace("http://","https://",$rowArticle->post_content));
                $val=explode(" ",strip_tags($blog_cont));
                    $i=0;
                    while($i<=145)
                       {   echo @$val[$i]." ";
					       $i++;
                       }   
					   /*echo $sql_feat_img="SELECT guid FROM wp_postmeta INNER JOIN wp_posts ON wp_postmeta.post_id=wp_posts.ID WHERE post_id=$rowArticle->ID and meta_key = '_wp_attached_file' ";
					   $rs_feat_img=mysqli_query($GLOBALS["___mysqli_ston"],$sql_feat_img);
					   $row_feat_img=mysqli_fetch_object($rs_feat_img);
					   echo $row_feat_img->guid;
					   */?>...</div>
        </div>
        <div class="DataTableRow">
          <div class="DataTableCell" style="border:none"><a href="https://www.stanlay.in/blog/<?php echo $rowArticle->post_name;  ?>/" class="read_more_link"  title="<?php echo $rowArticle->post_title;  ?>" target="_blank">Read Full Story [+]</a></div>
        </div>
        <?php  }  } } ?>
      </div>
      <?php } //echo $pro_features;?>
    </div>
    <div>&nbsp;</div>
    <div>&nbsp;</div>
  </div>
  <!-- #tab4 -->
  </div>
  <!-- .tab_container -->
  </div>
  <!-- /tabs -->
  </div>
</section>
<?php  $sql_related="select tbl_index_g21.match_pro_id_g2,tbl_products_1.pro_id,tbl_products_1.pro_image,tbl_products_1.pro_title,tbl_products_1.pro_permalink_n from tbl_index_g21,tbl_products_1 where tbl_index_g21.match_pro_id_g2=tbl_products_1.pro_id and tbl_index_g21.deleteflag='active' and tbl_products_1.pro_group_id = 0 and tbl_products_1.status='active' and tbl_index_g21.pro_id=$pro_id";
   $rs_related_post=mysqli_query($GLOBALS["___mysqli_ston"],$sql_related);
   $num_count=@mysqli_num_rows($rs_related_post);
if($num_count>0)
{
 ?>
<section class="top-cat-hp bg-light">
  <div class="container">
    <div class="row">
      <h2><?php echo $pro_heading 		= stripslashes($front->section_heading("pro_heading"));?><!--Related Products--></h2>
      <div id="related-products" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
          <div class="item active">
            <div class="container clearfix">
              <?php
			$r=0;
 while($row_related_post=@mysqli_fetch_object($rs_related_post))
				   {
				$r++;	   
	   ?>
              <div class="col-md-4 col-sm-4">
                <div class="related-box"><a href="<?php echo $css_path.$front->Pro_URL($row_related_post->pro_id);?>">
                <img class="lazy img-responsive" src="<?php echo $css_path_images;?>/images/blank.png" data-src="<?php echo $css_path_images.'uploads/pro_image/small/'.$front->Get_pro_images($row_related_post->pro_id);?>" alt="<?php echo $row_related_post->pro_title;?>" border="0"  /></a>
                  <h3><a href="<?php echo $css_path.$front->Pro_URL($row_related_post->pro_id);?>"><?php echo $row_related_post->pro_title;?></a></h3>
                </div>
              </div>
<?php 
 if($r%3==0)
	  {
if($r!=$num_count)
		  {
		  echo ' </div ></div >';
		  echo ' <div class="item "><div class="container clearfix">';
		  }
		    }
	}
?>
            </div>
          </div>
        </div>
        <div class="control-panel"> <a class="left carousel-control" href="#related-products" data-slide="prev"> &nbsp;</a> <a class="right carousel-control" href="#related-products" data-slide="next"> &nbsp;</a> </div>
      </div>
    </div>
  </div>
</section>
<?php }?>
<!-- MENU END HARE -->
<!-- Modal -->
<style type="text/css">
.modal-header-faq {
    min-height: 16.43px;
    padding: 5px;
    border-bottom: 1px solid #e5e5e5;
	    background-color: #716b6b;
}
.modal-footer-faq {
    padding: 2px;
    text-align: right;
    border-top: 1px solid #e5e5e5;
	    background-color: #716b6b;
}

.close-faq {
    float: right;
    font-size: 19px;
    font-weight: 400;
    line-height: 1;
    color: #333;
    text-shadow: 0 1px 0 #fff;
    opacity: .5;
}
  
 button.close-faq {
    -webkit-appearance: none;
    padding: 0;
    cursor: pointer;
    background: 0 0;
    border: 0;
/*	margin-top: -20px;*/
	margin-top: -216px;
}


@media ( max-width: 768px) {  
button.close-faq {

	margin-top: -100px;
	margin-right: -10px;
}


.panel-title {
    margin-top: 0;
    margin-bottom: 0;
    font-size: 14px;
    color: inherit;
}
 }

.no_border td, th {
    
 border: solid 1px #fff;
    
}

/*.no_line_height li, td, th {line-height:10px;}*/
 </style>
<div class="modal fade" id="myModalFAQ" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
   <!--   <div class="modal-header-faq text-center"  >
        <h4 class="modal-title w-100 font-weight-bold" ></h4>
        <button type="button" class="close-faq" data-dismiss="modal" aria-label="Close" > <span aria-hidden="true">&times;</span> </button>
      </div>-->
      <div class="modal-body mx-3">
      <form  action="<?php echo $css_path; ?>mail-faq.php" name="TagPopup_Form" id="TagPopup_Form" method="post" >
        <table width="100%" border="0"  class="no_border">
          <tr>
            <td width="60%"><strong>Ask A Question</strong> </td>
            <td rowspan="7" style="vertical-align:top"><img src="/images/faqsmall.png"  align="right" > <button type="button" class="close-faq" data-dismiss="modal" aria-label="Close" > <span aria-hidden="true">&times;</span></td>
          </tr>
<?php
$page_link=$_SERVER["REQUEST_URI"];
$page_link_exp=explode("/",$_SERVER["REQUEST_URI"]);

if(@$pro_id!='' && @$pro_id!='0')
{
$contact_link_id="Product: ".$pro_name;//$pro_id;
}
else{
$contact_link_id="Page Name: HOME ";
}
?>
<input type="hidden" name="page_link" value="<?php echo "https://www.stanlay.in".$page_link;///$_SERVER['REQUEST_URI'];?>" class="form-control input-sm" />
            <input type="hidden" name="page_name" value="<?php echo $contact_link_id;///$_SERVER['REQUEST_URI'];?>" class="form-control input-sm" />
                        <input type="hidden" name="pro_id" value="<?php echo $pro_id;///$_SERVER['REQUEST_URI'];?>" class="form-control input-sm" />
                      <tr>
            <td><input type="text" id="faq_name" name="faq_name" class="form-control input-sm" placeholder="Name" autocomplete="2" required></td>
          </tr>
          <tr>
            <td><input type="email" id="faq_email" name="faq_email" class="form-control validate" placeholder="Email" autocomplete="2" required></td>
          </tr>
          <tr>
            <td><input type="text" id="faq_mobile" name="faq_mobile" class="form-control validate" placeholder="Mobile" autocomplete="2" required></td>
          </tr>
          
          <!--     <tr>
    <td> <input type="text" id="faq_mobile" name="faq_mobile" class="form-control validate" placeholder="Mobile" autocomplete="2" required></td>
    </tr>
-->
          <tr>
            <td><img src="<?php echo BASE_URL;?>captcha_faq.php" width="95px;" />
              <input name="captcha_faq" type="number"  class="form-control " placeholder="Enter Captcha" required  ></td>
          </tr>
          <tr>
            <td colspan="2"><textarea type="text" id="faq_question" name="faq_question" class="md-textarea form-control" rows="4" placeholder="Question" required ></textarea></td>
          </tr>
          <tr>
            <td><input type="submit" class="btn btn-login" value="Submit" /></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><strong>Privacy Disclaimer:</strong></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td colspan="2" style="font-size:9px; color:#999;line-height:10px;"><ul class="no_line_height">
                <li>We will only publish your first name. Your Last name will not published to maintain your privacy.</li>
                <li>Your mobile no will not be revealed or published to maintain your privacy. It will only be maintained in our database. You may receive product or information updates from time to time on WhatsApp. </li>
                <li>Your email ID will not be revealed or published to maintain your privacy. It will only be maintained in our database. You may receive product or information updates from time to time on email. </li>
                <li>Should you post your question, it is assumed that you agree to the above. </li>
              </ul></td>
          </tr>
        </table>
        </form>
        <?php /*?> <div class="form-group mb-5">
        
          <input type="text" id="faq_name" name="faq_name" class="form-control input-sm" placeholder="Name" autocomplete="2">
<!--          <label data-error="wrong" data-success="right" for="form34" >Your name</label>-->
        </div>

        <div class="form-group mb-5">
         
          <input type="email" id="faq_email" name="faq_email" class="form-control validate" placeholder="Email" autocomplete="2">
<!--          <label data-error="wrong" data-success="right" for="form29">Your email</label>-->
        </div>

        <div class="form-group mb-5">
         
          <input type="text" id="faq_mobile" name="faq_mobile" class="form-control validate" placeholder="Mobile" autocomplete="2">
     <!--     <label data-error="wrong" data-success="right" for="form32">Subject</label>-->
        </div>

        <div class="form-group">
          
          <textarea type="text" id="faq_question" name="faq_question" class="md-textarea form-control" rows="4" placeholder="Question"></textarea>
     <!--     <label data-error="wrong" data-success="right" for="form8">Your Question</label>-->
        </div><?php */?>
        
        <!--<img src="http://localhost/stanlay.in/images/FAQ-temp.jpg" >--> 
      </div>
      <!--<div class="modal-footer-faq d-flex justify-content-center"  >
        <button class="btn btn-warning">Send </button>
      </div>--> 
    </div>
  </div>
</div>
<!--<div class="text-center">
  <a href="" class="btn btn-default btn-rounded mb-4" data-toggle="modal" data-target="#modalContactForm">Launch
    Modal Contact Form</a>
</div>-->
</div>
<?php include('sidebar-contact.php');  ?>
<?php include('get-in-touch.php');  ?>
<?php include('browse-by-category.php');  ?>
<!--done-->
<?php include('footer-top-categories.php');  ?>
<!--done-->
<?php include('footer.php');  ?>
<script id="rendered-js" >

<!-- Medium Quality: https://img.youtube.com/vi/{video-id}/mqdefault.jpg (320×180 pixels)-->
<!-- High Quality: http://img.youtube.com/vi/G0wGs3useV8/hqdefault.jpg (480×360 pixels)-->
<!-- Standard Definition (SD): http://img.youtube.com/vi/G0wGs3useV8/sddefault.jpg (640×480 pixels)-->
<!-- Maximum Resolution: http://img.youtube.com/vi/G0wGs3useV8/maxresdefault.jpg (1920×1080 pixels)-->
( function() {

    var youtube = document.querySelectorAll( ".youtube" );
    
    for (var i = 0; i < youtube.length; i++) {
        
        var source = "https://img.youtube.com/vi/"+ youtube[i].dataset.embed +"/hqdefault.jpg";
        
        var image = new Image();
                image.src = source;
                image.addEventListener( "load", function() {
                    youtube[ i ].appendChild( image );
                }( i ) );
        
                youtube[i].addEventListener( "click", function() {

                    var iframe = document.createElement( "iframe" );

                            iframe.setAttribute( "frameborder", "0" );
                            iframe.setAttribute( "allowfullscreen", "" );
                            iframe.setAttribute( "src", "https://www.youtube.com/embed/"+ this.dataset.embed +"?rel=0&showinfo=0&autoplay=1" );

                            this.innerHTML = "";
                            this.appendChild( iframe );
                } );    
    };
    
} )();
    </script>
</body>
</html>
<?php
######## Your Website Content Ends here Product Page#########
if (!is_dir($cache_folder)) { //create a new folder if we need to
    mkdir($cache_folder);
}
if(!$ignore){
   $fp = fopen($cache_file, 'w');  //open file for writing
   fwrite($fp, ob_get_contents()); //write contents of the output buffer in Cache file
   fclose($fp); //Close file pointer
}
ob_end_flush(); //Flush and turn off output buffering
?>