<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require_once "phpmailer/vendor/autoload.php";

//PHPMailer Object
$mail = new PHPMailer(true); //Argument true in constructor enables exceptions

//From email address and name
$mail->From = "cord@stanlay.in";
$mail->FromName = "Stanlay CRM table";

//To address and name
$mail->addAddress("rumit@stanlay.com", "Rumit Verma");
$mail->addAddress("rumit@example.com"); //Recipient name is optional

//Address to which recipient will reply
$mail->addReplyTo("cord@stanlay.com", "Reply");

//CC and BCC
$mail->addCC("jb@stanlay.com");
//$mail->addBCC("bcc@example.com");

//Send HTML or Plain Text email
$mail->isHTML(true);

$mail->Subject = "Subject Dispatch confirmation";
$mail->Body = "<table width='100%' border='0'>
  <tr>
    <td>A</td>
    <td>B</td>
  </tr>
  <tr>
    <td>C</td>
    <td>D</td>
  </tr>
  <tr>
    <td>E</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>F</td>
  </tr>
</table>
";
$mail->AltBody = "This is the plain text version of the email content";

try {
    $mail->send();
    echo "Message has been sent successfully";
} catch (Exception $e) {
    echo "Mailer Error: " . $mail->ErrorInfo;
}





