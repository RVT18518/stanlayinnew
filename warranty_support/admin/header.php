
<style>
	table tr td {
			border-right:1px solid #CCC!important;
			border-bottom:1px solid #CCC!important;
	}
		table tr th {
			border-right:1px solid #CCC!important;
			border-bottom:1px solid #CCC!important;
			padding:10px;
	}

	table tr td {
			font-size:12px!important;
	}
	
	.back_button {
  background: #3498db;
  background-image: -webkit-linear-gradient(top, #3498db, #2980b9);
  background-image: -moz-linear-gradient(top, #3498db, #2980b9);
  background-image: -ms-linear-gradient(top, #3498db, #2980b9);
  background-image: -o-linear-gradient(top, #3498db, #2980b9);
  background-image: linear-gradient(to bottom, #3498db, #2980b9);
  -webkit-border-radius: 28;
  -moz-border-radius: 28;
  border-radius: 28px;
  font-family: Arial;
  color: #ffffff;
  font-size: 15px;
  padding: 8px 15px 8px 15px;
  text-decoration: none;
}
.back_button a{
  color: #ffffff;
  border:none!important;
}
.back_button:hover a{
  color: #fff000;
  border:none!important;
}
.back_button:hover {
  background: #3cb0fd;
  background-image: -webkit-linear-gradient(top, #3cb0fd, #3498db);
  background-image: -moz-linear-gradient(top, #3cb0fd, #3498db);
  background-image: -ms-linear-gradient(top, #3cb0fd, #3498db);
  background-image: -o-linear-gradient(top, #3cb0fd, #3498db);
  background-image: linear-gradient(to bottom, #3cb0fd, #3498db);
  text-decoration: none;
}
.back_button_ac {
  background: #339966;
  text-decoration: none;
}


.submit_but {
  background: #3498db;
  background-image: -webkit-linear-gradient(top, #3498db, #2980b9);
  background-image: -moz-linear-gradient(top, #3498db, #2980b9);
  background-image: -ms-linear-gradient(top, #3498db, #2980b9);
  background-image: -o-linear-gradient(top, #3498db, #2980b9);
  background-image: linear-gradient(to bottom, #3498db, #2980b9);
  -webkit-border-radius: 28;
  -moz-border-radius: 28;
  border-radius: 28px;
  font-family: Arial;
  color: #ffffff;
  font-size: 20px;
  padding: 10px 20px 10px 20px;
  text-decoration: none;
  cursor:pointer
}

.submit_but:hover {
  background: #3cb0fd;
  background-image: -webkit-linear-gradient(top, #3cb0fd, #3498db);
  background-image: -moz-linear-gradient(top, #3cb0fd, #3498db);
  background-image: -ms-linear-gradient(top, #3cb0fd, #3498db);
  background-image: -o-linear-gradient(top, #3cb0fd, #3498db);
  background-image: linear-gradient(to bottom, #3cb0fd, #3498db);
  text-decoration: none;
  outline:none
}
</style>
<header style="background-color:rgba(204,204,204,0.3)">
  <section class="upper-nav">
    <div class="row">
     
    </div>
  </section>
  <nav id="main-nav" class="row">
    <div id="logo" class="three columns"> <a aria-label="Home" role="button" href="#" class="main-logo"> <img src="../../images/logo.png" /> </a> </div>
    <div id="site-links" class="columns"> <a id="optimizely-demo-button" href="#" class="button demo lightgray" style="margin-top:9px; font-size:25px">RMA REPAIR</a> <?php time(); ?> </div>
  </nav>
</header>