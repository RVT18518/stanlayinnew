<?php
	include("../../includes1/function_lib.php");
	include("session_check.php");
	$admin_id=$_SESSION["AdminLoginID_SET"];
	
	if(!isset($_SESSION["AdminLoginID_SET"]))
		{
			$s->pageLocation("admin_login.php");
		}
		$home = "back_button_ac";
		
		if($_REQUEST['p_rec']=='active') {
			$dataArray['p_rec'] = 'active';
			$dataArray['p_rec_date'] = date("Y-m-d h:i:sa");
			$s->editRecord('tbl_srf', $dataArray,"S_ID",$_REQUEST['id']); 
		}
		
		if($_REQUEST['p_del']=='active') {
			$dataArray['del_status'] = 'inactive';
			$s->editRecord('tbl_srf', $dataArray,"S_ID",$_REQUEST['id']); 
		}
		
		
		
	?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Warranty Dashboard</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="styles.css">

</head>

<body>
<?php include('header.php'); ?>


<main class="templates">
  <section class="section--white">
    <?php include('left.php'); ?>
    
    
   
    
    
      
   
         <table width="97.3%" cellpadding="0" cellspacing="0" style="margin:auto">
       <tr>
        	<td colspan="7" style="padding:10px; font-size:18px; font-weight:bold; background-color:rgba(0,204,102,0.1); text-transform:uppercase">RMA Requests Pending:: Product Not Received</td>
        </tr>
        	<tr>
        	<th>RMA#</th>
            <th>Company Name</th>
            <th>Person Name</th>
            <th>Email</th>
            <th>Product</th>
            <th>Confirm Product <br /> Received</th>
            <th>Remove</th>
        </tr>
        <?php 
			$i=1;
			$sql_rma = "select * from tbl_srf where p_rec='inactive' && del_status='active'";
			$row_rma = mysqli_query($GLOBALS["___mysqli_ston"],$sql_rma);
			while($rs_rma  = mysqli_fetch_object($row_rma)) {
		?>
       	<tr>
        	<td><?php echo $rs_rma->RMA_NO; ?></td>
        	<td><?php echo $rs_rma->cname; ?></td>
        	<td><?php echo $rs_rma->name; ?></td>
        	<td><?php echo $rs_rma->email; ?></td>
        	<td><?php echo $s->tbl_order_product_all($rs_rma->order_id); ?></td>
        	<td style="text-align:center">
            	<a href="index.php?p_rec=active&id=<?php echo $rs_rma->S_ID; ?>" style="text-decoration:none; border-bottom:none" title="Click to updated, product has been received and process repair" onclick="return confirm('Are you sure to move into pending repairs!');">
                	<img src="icon-active.gif" alt="Active to Received" />
                </a>
               
               
                </td>
                
                <td style="text-align:center">
            	<a href="index.php?p_del=active&id=<?php echo $rs_rma->S_ID; ?>" style="text-decoration:none; border-bottom:none" title="Remove" onclick="return confirm('Are you sure to remove pending repairs!');">
                	<img src="dash_remove_icon.png" alt="click to remove" />
                </a>
               
               
                </td>
                
                <?php } ?>
        </tr>
        </table>
      
    
      
      
        
       
  
     
    </div>
  </section>
   <?php include('footer.php'); ?>
</main>
    
</body>
</html>