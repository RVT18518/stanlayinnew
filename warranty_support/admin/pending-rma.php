<?php 
	include("../../includes1/function_lib.php");
	include("session_check.php");
	$admin_id=$_SESSION["AdminLoginID_SET"];
	
	if(!isset($_SESSION["AdminLoginID_SET"]))
		{
			$s->pageLocation("admin_login.php");
		}
		
		
		if($_REQUEST['Confirm_Resolved_Repair']=='active') {
			$dataArray['Confirm_Resolved_Repair'] = 'active';
				$s->editRecord('tbl_srf', $dataArray,"S_ID",$_REQUEST['id']); 
		}
		
		
	?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Warranty Pending RMA</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="styles.css">

</head>

<body>
<?php 
	$pram = "back_button_ac";
include('header.php'); ?>


<main class="templates">
  <section class="section--white">
    <?php include('left.php'); ?>
    
    
   
    
    
      
   
         <table width="97.3%" cellpadding="0" cellspacing="0" style="margin:auto">
       <tr>
        	<td colspan="9" style="padding:10px; font-size:18px; font-weight:bold; background-color:rgba(0,204,102,0.1); text-transform:uppercase">RMA Requests Pending Investigation</td>
        </tr>
        
        <tr>
        	<th>RMA#</th>
            <th>Company Name</th>
            <th>Person Name</th>
            <th>Email</th>
            <th>Product</th>
            <th>Problem Faced</th>
            <th>Date <br /> of SRF</th>
            <th>Expected Date <br /> of Dispatch</th>
            <th>Process Repair</th>
            <th>Confirm <br />Resolved Repair</th>
        </tr>
        
        <?php 
			$i=1;
			$sql_rma = "select * from tbl_srf where p_rec='active' && Confirm_Resolved_Repair='inactive'";
			$row_rma = mysqli_query($GLOBALS["___mysqli_ston"],$sql_rma);
			while($rs_rma  = mysqli_fetch_object($row_rma)) {
		?>
       	<tr>
        	<td><?php echo $rs_rma->RMA_NO; ?></td>
        	<td><?php echo $rs_rma->cname; ?></td>
        	<td><?php echo $rs_rma->name; ?></td>
        	<td><?php echo $rs_rma->email; ?></td>
        	<td>
			
			<?php echo ucwords(strtolower($s->tbl_order_product_all($rs_rma->S_No))); ?></td>
        	<td><?php echo $rs_rma->problem_facing; ?></td>
        	
          <td><?php echo $rs_rma->SRF_Place_Date; ?></td>
          <td><?php echo $rs_rma->edd; ?></td>
          <td style="text-align:center">
            	<a href="process-repair.php?id=<?php echo $rs_rma->S_ID; ?>" style="text-decoration:none; border-bottom:none" title="Click to stat repair process!">
                	<img src="process.png" alt="Active to Received" width="50" />
                </a>
               
               
                </td>
          <td style="text-align:center">
            	<a href="pending-rma.php?Confirm_Resolved_Repair=active&id=<?php echo $rs_rma->S_ID; ?>" style="text-decoration:none; border-bottom:none" title="Click to confirm resolved repair." onclick="return confirm('Are you sure to move into resolved repairs!');">
                	<img src="icon-active.gif" alt="Active to resolved" />
                </a>
               
               
                </td>
                
                

                <?php } ?>
        </tr>
        </table>
      
    
      
      
        
       
  
     
    </div>
  </section>
   <?php include('footer.php'); ?>
</main>
    
</body>
</html>