<?php 
	include("../../includes1/function_lib.php");
	include("session_check.php");
	$admin_id=$_SESSION["AdminLoginID_SET"];
	
	if(!isset($_SESSION["AdminLoginID_SET"]))
		{
			$s->pageLocation("admin_login.php");
		}
		
		
		
		
	?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Warranty Resolved Repairs RMA</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="styles.css">

</head>

<body>
<?php 
	$rmares = "back_button_ac";
include('header.php'); ?>


<main class="templates">
  <section class="section--white">
    <?php include('left.php'); ?>
    
    
   
    
    
      
   
         <table width="97.3%" cellpadding="0" cellspacing="0" style="margin:auto">
       <tr>
        	<td colspan="7" style="padding:10px; font-size:18px; font-weight:bold; background-color:rgba(0,204,102,0.1); text-transform:uppercase">RMA Requests Pending Investigation</td>
           <td style="padding:10px; font-size:18px; font-weight:bold; background-color:rgba(0,204,102,0.1); text-transform:uppercase">
           	Change No of Result<form action="">
           	<select onchange="this.form.submit();" name="select_range">
            	<option value="20" <?php if($_REQUEST['select_range']==20) { echo 'selected'; } ?>>20</option>
                <option value="50" <?php if($_REQUEST['select_range']==50) { echo 'selected'; } ?>>50</option>
                <option value="100" <?php if($_REQUEST['select_range']==100) { echo 'selected'; } ?>>100</option>
                <option value="500" <?php if($_REQUEST['select_range']==500) { echo 'selected'; } ?>>500</option>
                <option value="1000" <?php if($_REQUEST['select_range']==1000) { echo 'selected'; } ?>>1000</option>
                <option value="5000" <?php if($_REQUEST['select_range']==5000) { echo 'selected'; } ?>>5000</option>
            </select>
            </form>
           </td>
        </tr>
        	<tr>
        	<th>RMA#</th>
            <th>Company Name</th>
            <th>Person Name</th>
            <th>Email</th>
            <th>Product</th>
            <th>Problem Faced</th>
            <th>View Details</th>
            <th>Expected Date <br /> of Dispatch</th>

        </tr>
        <?php 
			$i=1;
			$sql_rma = "select * from tbl_srf  where del_status='active' AND Confirm_Resolved_Repair='active' order by S_ID desc limit 0,20";
			if($_REQUEST['select_range']) {
				$sql_rma = "select * from tbl_srf  where del_status='active' AND Confirm_Resolved_Repair='active' order by S_ID desc limit 0,".$_REQUEST['select_range'];
			}
			$row_rma = mysqli_query($GLOBALS["___mysqli_ston"],$sql_rma);
			while($rs_rma  = mysqli_fetch_object($row_rma)) {
		?>
       	<tr>
        	<td><?php echo $rs_rma->RMA_NO; ?></td>
        	<td><?php echo $rs_rma->cname; ?></td>
        	<td><?php echo $rs_rma->name; ?></td>
        	<td><?php echo $rs_rma->email; ?></td>
        	<td><?php echo $s->tbl_order_product_all($rs_rma->order_id); ?></td>
        	<td><?php echo $rs_rma->pfaced; ?></td>
        	<td style="text-align:center">
            	<a href="view-repair.php?id=<?php echo $rs_rma->S_ID; ?>" style="text-decoration:none; border-bottom:none" title="Click to updated, product has been received and process repair">
                	<img src="icon-schedule.png" alt="Active to Received" width="25" />
                </a>
               
               
                </td>
          <td><?php echo $rs_rma->edd; ?></td>
          
        
                
                

                <?php } ?>
        </tr>
        </table>
      
    
      
      
        
       
  
     
    </div>
  </section>
   <?php include('footer.php'); ?>
</main>
    
</body>
</html>