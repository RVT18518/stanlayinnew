<?php include("../../includes1/modulefunction.php"); include("../../includes1/function_lib.php");  
	include("session_check.php");
	$admin_id=$_SESSION["AdminLoginID_SET"];
	
	if(!isset($_SESSION["AdminLoginID_SET"]))
		{
			$s->pageLocation("admin_login.php");
		}
	?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Warranty Support: Thankyou for Service Request</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="styles.css">
</head>

<body>
<?php include('header.php'); ?>


<main class="templates">
  <section class="section--white" style="width:97.5%; margin:auto;">
   
    
    <?php include('left.php'); ?>
    
    
      
     
         
  <h1 class="headline" style="margin-left:15px">Thank you <?php echo $_REQUEST['name']; ?>, </h1>
        <p class="sub-title" style="font-weight:bold; font-size:18px; margin-left:18px">
        Your Warranty Support form has been Submitted Successfully.
        Our Customer support devision will cantact you soon.
       
        
        
      <?php 
       
       echo $msg1 = '
	   
	   <p class="sub-title" style="font-weight:bold; font-size:15px; margin-left:18px"><span style="color:#ff0000;font-size:19px">*</span> Any Product being shipped back to our works should have RMA # displayed on or  &nbsp;&nbsp;&nbsp;included in the returned products packaging.</p>
	   
	   <table width="97.2%" border="1" style="margin:auto">
       <tr>
        	<th colspan="2" style="padding:10px; font-size:18px; background-color:rgba(0,204,102,0.1); text-transform:uppercase">Tracing Details</th>
        </tr>
       	<tr>
        	<td>Name: </td>
            <td>'.$_REQUEST['name'].'</td>
        </tr>
        <tr>
        	<td>Company Name: </td>
            <td>'.$_REQUEST['cname'].'</td>
        </tr>
        <tr>
        	<td>Return Address: </td>
            <td>'.$_REQUEST['address'].'</td>
        </tr><tr>
        	<td>Email ID: </td>
            <td>'.$_REQUEST['email'].'</td>
        </tr>
        <tr>
        	<td>Problme you are facing: </td>
            <td>'.$_REQUEST['problem_facing'].'</td>
        </tr>
        
         <tr>
        	<td>RMA Number: </td>
            <td>'.$_REQUEST['RMA_NO'].'</td>
        </tr>
         <tr>
        	<td>Stanlay Order ID: </td>
            <td>'.$_REQUEST['order_id'].'</td>
        </tr>';
		?>
        
          <?php 
       
        $msg = '
	   	<table>
   
	<p align="center"><a aria-label="Home" role="button" href="http://www.stanlay.in/" class="main-logo"> <img src="http://www.stanlay.in/images/logo.png" /> </a> </p>
    <p align="center"> <a id="optimizely-demo-button" href="#" class="button demo lightgray" style="margin-top:9px; font-size:25px">Online Service Request Form</a> </p>
	   
	   
	   
	   <p class="sub-title" style="font-weight:bold; font-size:15px"><span style="color:#ff0000;font-size:19px">*</span> Any Product being shipped back to our works should have RMA # displayed on or <br />  &nbsp;&nbsp;&nbsp;included in the returned products packaging.</p>
	   
	  
	   
	   <table width="100%" border="1">
       <tr>
        	<th colspan="2" style="padding:10px; font-size:18px; background-color:rgba(0,204,102,0.1); text-transform:uppercase">Tracing Details</th>
        </tr>
       	<tr>
        	<td>Name: </td>
            <td>'.$_REQUEST['name'].'</td>
        </tr>
        <tr>
        	<td>Company Name: </td>
            <td>'.$_REQUEST['cname'].'</td>
        </tr>
        <tr>
        	<td>Return Address: </td>
            <td>'.$_REQUEST['address'].'</td>
        </tr><tr>
        	<td>Email ID: </td>
            <td>'.$_REQUEST['email'].'</td>
        </tr>
        <tr>
        	<td>Problme you are facing: </td>
            <td>'.$_REQUEST['problem_facing'].'</td>
        </tr>
        
         <tr>
        	<td>RMA Number: </td>
            <td>'.$_REQUEST['RMA_NO'].'</td>
        </tr>
         <tr>
        	<td>Stanlay Order ID: </td>
            <td>'.$_REQUEST['order_id'].'</td>
        </tr>
		 <tr><td colspan="2">
		<p><br />
Regards,<br />
'.$_REQUEST['name'].'</p></td>
        </tr>';
		?>
        
        
   <tr>
   <td colspan="2" style="text-align:center!important"><input type="button" value="Print" onclick="window.print();" /></td>
   </tr>
       </table>
       
       
       
       </p>

  </section>
   <?php include('footer.php'); ?>
</main>



<?php

		$dataArray["name"] 				= htmlspecialchars($_REQUEST['name'],ENT_QUOTES);
		$dataArray["cname"] 			= htmlspecialchars($_REQUEST['cname'],ENT_QUOTES);
		$dataArray["address"] 			= htmlspecialchars($_REQUEST['address'],ENT_QUOTES);
		$dataArray["email"] 			= htmlspecialchars($_REQUEST['email'],ENT_QUOTES);
		$dataArray["problem_facing"] 	= htmlspecialchars($_REQUEST['problem_facing'],ENT_QUOTES);
		$dataArray["RMA_NO"] 			= htmlspecialchars($_REQUEST['RMA_NO'],ENT_QUOTES);
		$dataArray["order_id"] 			= htmlspecialchars($_REQUEST['order_id'],ENT_QUOTES);
		
		$result    	=  $s->editRecord('tbl_srf', $dataArray,"RMA_NO",$_REQUEST['RMA_NO']); 
		

		// To send HTML mail, the Content-type header must be set
		$to 		= "dsr@stanlay.com";
		$subject 	= "New Warranty Support Enquiry";
		$headers  	= 'MIME-Version: 1.0' . "\r\n";
		$headers 	.= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		
		
		// Additional headers
		$headers 	.= 'To: Stanlay SRF <sales@stanlay.com>' . "\r\n";
		$headers 	.= 'From: '.$dataArray["cname"].' <'.$dataArray["email"].'>' . "\r\n";
		
		
		// Mail it
		mail($to, $subject, $msg, $headers);
		mail($_REQUEST['email'], $subject, $msg, $headers);


?>
    
</body>
</html>