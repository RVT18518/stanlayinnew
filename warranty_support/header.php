<header>
  <section class="upper-nav">
    <div class="row">
      <div class="upper-nav-links">
        <ul>
          <li><a href="http://stanlay.in/">Home</a></li>
          <li><a href="http://stanlay.in/warranty_support/">Support Dashboard</a></li>
          <li><a href="http://stanlay.in/contact-us.php">Contact Us</a></li>
          <li class="login-left"><a href="#">+91-11-4186 0000</a></li>
        </ul>
      </div>
    </div>
  </section>
  <nav id="main-nav" class="row">
    <div id="logo" class="three columns"> <a aria-label="Home" role="button" href="https://www.stanlay.in/" class="main-logo"> <img src="http://www.stanlay.in/images/logo.png" /> </a> </div>
    <div id="site-links" class="columns"> <a id="optimizely-demo-button" href="#" class="button demo lightgray" style="margin-top:9px; font-size:25px">Online Service Request Form</a> </div>
  </nav>
</header>